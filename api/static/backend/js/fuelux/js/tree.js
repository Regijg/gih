var TreeView = function () {

    return {
        //main function to initiate the module
        init: function () {

            var DataSourceTree = function (options) {
                this._data  = options.data;
                this._delay = options.delay;
            };

            DataSourceTree.prototype = {

                data: function (options, callback) {
                    var self = this;

                    setTimeout(function () {
                        var data = $.extend(true, [], self._data);

                        callback({ data: data });

                    }, this._delay)
                }
            };
			
            var treeDataSource = new DataSourceTree({
                data: [
                    { name: 'Office Bandung <div class="tree-actions"><i class="fa fa-plus"></i><i class="fa fa-trash-o"></i><i class="fa fa-refresh"></i></div>', type: 'folder', additionalParameters: { id: 'F11', data:[
							{ name: 'Test Sub Folder 1', type: 'folder', additionalParameters: { id: 'FF1' } },
							{ name: 'Test Sub Folder 2', type: 'folder', additionalParameters: { id: 'FF2' } },
							{ name: 'Test Item 2 in Folder 1', type: 'item', additionalParameters: { id: 'FI2' } }
						  ] } },
                    { name: 'Office Jakarta<div class="tree-actions"><i class="fa fa-plus"></i><i class="fa fa-trash-o"></i><i class="fa fa-refresh"></i></div>', type: 'folder', additionalParameters: { id: 'F12' } }
                ],
                delay: 400
            });

            $('#FlatTree4').tree({
                selectable: false,
                dataSource: treeDataSource
            });


        }

    };

}();