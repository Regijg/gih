$(document).ready(function () {
    // Select2===============================================================
    $(".select2, .select2-multi").select2().change(function (e) {
        $('#select2Form').formValidation('form-ppob', 'sub_produk');
    });
    $('.select-search-hide').select2({
        minimumResultsForSearch: -1
    });

    $('#form-ppob').bootstrapValidator({
        fields: {
            sub_produk: {
                validators: {
                    notEmpty: {
                        message: 'tidak boleh kosong'
                    }
                }
            },
            no_pel: {
                validators: {
                    notEmpty: {
                        message: 'No. Telepon/pelanggan digunakan Dan tidak boleh kosong'
                    },
                    digits: {
                        message: 'No. Telepon/pelanggan Harus Angka'
                    }
                }
            }
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // You can get the form instance
        var $form = $(e.target);

        // and the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data
        $.ajax({
            url: $form.attr('action'),
            type: 'POST',
            data: $form.serialize(),
            dataType: "json",
            beforeSend: function(){
                loadingbar();
            },
            success: function (data) {
                loadingbar_close();
                if(data.status){
                    $('#page-form').slideUp();
                    $('#page-inquiry').html(data.page).slideDown('slow');
                }else{
                    $('#alert-info').html(data.messages);
                }
                // ... Process the result ...
            }
        });

        //loadingbar();
        //bv.defaultSubmit();
    });
});