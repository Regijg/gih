$(document).ready(function () {
    // Select2===============================================================
    $(".select2, .select2-multi").select2();
    $('.select-search-hide').select2({
        minimumResultsForSearch: -1
    });

    //datepicker=============================================================
    $.fn.datepicker.dates['id'] = {
        days: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"],
        daysShort: ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab", "Min"],
        daysMin: ["Mg", "Sn", "Sl", "Rb", "Km", "Jm", "Sb", "Mg"],
        months: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "Desember"],
        monthsShort: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Des"],
        today: "Hari Ini",
        clear: "Clear"
    };

    $('.date').datepicker({
        language: 'id',
        format: "yyyy-mm-dd",
        autoclose: true
    });

    //Ckeditor===============================================================
    editorPage(); // for editor konten
    function editorPage() {
        $('textarea.editor').each(function () {
            CKEDITOR.replace($(this).attr('id'), {
                toolbar: [
                    ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript'],
                    ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
                    {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi'], items: ['NumberedList', 'BulletedList']},
                    ['Undo', 'Redo'],
                    {name: 'links', items: ['Link', 'Unlink']},
                    {name: 'styles', items: ['Font', 'FontSize', 'Format']},
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                    {name: 'insert', items: ['Image', 'Table']},
                    {name: 'tools', items: ['Maximize']}
                ]
            });
        });

    }

    //Form Wizard===========================================================
    jQuery('#basicWizard').bootstrapWizard({
        onTabShow: function (tab, navigation, index) {
            tab.prevAll().addClass('done');
            tab.nextAll().removeClass('done');
            tab.removeClass('done');

            var $total = navigation.find('li').length;
            var $current = index + 1;

            if ($current >= $total) {
                $('#basicWizard').find('.wizard .next').addClass('hide');
                $('#basicWizard').find('.wizard .finish').removeClass('hide');
            } else {
                $('#basicWizard').find('.wizard .next').removeClass('hide');
                $('#basicWizard').find('.wizard .finish').addClass('hide');
            }
        }
    });
});


