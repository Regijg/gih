$(document).ready(function () {
     // Select2===============================================================
    $(".select2, .select2-multi").select2();
    $('.select-search-hide').select2({
        minimumResultsForSearch: -1
    });
    
   //========================DatePicker===========================
    $.fn.datepicker.dates['id'] = {
        days: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"],
        daysShort: ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab", "Min"],
        daysMin: ["Mg", "Sn", "Sl", "Rb", "Km", "Jm", "Sb", "Mg"],
        months: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "Desember"],
        monthsShort: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Des"],
        today: "Hari Ini",
        clear: "Clear"
    };
    
    $('.date-years').datepicker({
        language: 'id',
        format: "yyyy-mm-dd",
        autoclose: true,
        endDate: '0d',
        startView: 2
    }).on('changeDate', function (e) {
        $('#form-book').bootstrapValidator('revalidateField', 'pax_birthdate_1');
        $('#form-book').bootstrapValidator('revalidateField', 'pax_birthdate_2');
        $('#form-book').bootstrapValidator('revalidateField', 'pax_birthdate_3');
    });
   
    $(".date-start-train").datepicker({
        language: 'id',
        todayHighlight: true,
        autoclose: true,
        startDate: '0d',
        format: 'yyyy-mm-dd'
    }).on('changeDate', function (e) {
        $(".date-end-train").datepicker({autoclose: true}).datepicker('setStartDate', e.date).focus();
        $('#formSearch_train').bootstrapValidator('revalidateField', 'start');
    });
    $(".date-end-train").datepicker({
        language: 'id',
        autoclose: true,
        startDate: '0d',
        format: 'yyyy-mm-dd'
    }).on('changeDate', function (e) {
        $('#formSearch_train').bootstrapValidator('revalidateField', 'end');
    });
    
    //TYPEAHEAD====================================================
    
    function obtainer_train(query, cb) {
        var filteredList = $.grep(suggestionsArray_train, function (item, index) {
            return item.match(query);
        });

        mapped = $.map(filteredList, function (item) {
            return {value: item};
        });
        cb(mapped);
    }
    
    $('.typeahead-depart-train').typeahead({
        hint: false,
        highlight: false,
        minLength: 0
    }, {
        name: "noname",
        displayKey: "value",
        matcher : function (item) {
            return true;
        },
        source: obtainer_train
    }).on('blur', function (e) {
        $('#formSearch_train').bootstrapValidator('revalidateField', 'from');
    });

    $('.typeahead-depart-train').on("focus", function () {
        ev = $.Event("keydown");
        ev.keyCode = ev.which = 40;
        $(this).trigger(ev);
        return true;
    });
    
    $('.typeahead-return-train').typeahead({
        hint: false,
        highlight: false,
        minLength: 0
    }, {
        name: "noname",
        displayKey: "value",
        matcher : function (item) {
            return true;
        },
        source: obtainer_train
    }).on('blur', function (e) {
        $('#formSearch_train').bootstrapValidator('revalidateField', 'to');
    });

    $('.typeahead-return-train').on("focus", function () {
        ev = $.Event("keydown");
        ev.keyCode = ev.which = 40;
        $(this).trigger(ev);
        return true;
    });
    
    //VALIDATION FORM SEARCH TRAIN================================
    $('#formSearch_train').bootstrapValidator({
        fields: {
            from: {
                validators: {
                    notEmpty: {
                        message: 'Stasiun Keberangkatan diperlukan dan tidak boleh kosong'
                    },
                    different: {
                        field: 'to',
                        message: 'Kota Keberangkatan Tidak Boleh sama'
                    }
                }
            },
            to: {
                validators: {
                    notEmpty: {
                        message: 'Stasiun Tujuan diperlukan dan tidak boleh kosong'
                    },
                    different: {
                        field: 'from',
                        message: 'Kota Tujuan Tidak Boleh sama'
                    }
                }
            },
            start: {
                validators: {
                    notEmpty: {
                        message: 'Tanggal Berangkat diperlukan dan tidak boleh kosong'
                    },
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'Format tanggal salah'
                    }
                }
            },
            end: {
                validators: {
                    notEmpty: {
                        message: 'Tanggal Pulang diperlukan dan tidak boleh kosong'
                    },
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'Format tanggal salah'
                    }
                }
            }
        }
    }).on('success.form.bv', function (e, data) {
        // Prevent form submission
        e.preventDefault();

        // You can get the form instance
        var $form = $(e.target);

        // and the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Check Count Passengers
        var total_pax = parseInt($('#formSearch_train *[name="adult"]').val()) +
                        parseInt($('#formSearch_train *[name="infant"]').val()) +
                        parseInt($('#formSearch_train *[name="child"]').val());
        if(total_pax < 4){
            loadingbar();
            bv.defaultSubmit();
        }else{
            $('#formSearch_train .alert-pax').html('*Maksimal jumlah penumpang secara akumulatif adalah 4').removeClass('hidden');
        }
    });
    //VALIDATION FORM SEARCH TRAIN================================
    
    //VALIDATION FORM DETAIL TRAIN================================
    $('#form-book').bootstrapValidator({
        fields: {
            customer_name: {
                validators: {
                    notEmpty: {
                        message: 'nama diperlukan dan tidak boleh kosong'
                    },
                    alpha_space: {
                        message: 'nama hanya menggunakan alpha karakter'
                    }
                }
            },
            customer_email: {
                validators: {
                    notEmpty: {
                        message: 'email diperlukan dan tidak boleh kosong'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            },
            customer_phone: {
                validators: {
                    notEmpty: {
                        message: 'No. Telepon Seluler digunakan dan tidak boleh kosong'
                    },
                    digits: {
                        message: 'No. Telepon Harus Angka'
                    }
                }
            },
            pax_name_first_1: {
                validators: {
                    notEmpty: {
                        message: 'Nama depan Passanger diperlukan dan tidak boleh kosong'
                    },
                    alpha: {
                        message: 'hanya menggunakan alpha karakter'
                    }
                }
            },
            pax_name_last_1: {
                validators: {
                    notEmpty: {
                        message: 'Nama belakang Passenger diperlukan dan tidak boleh kosong'
                    },
                    alpha: {
                        message: 'hanya menggunakan alpha karakter'
                    }
                }
            },
            pax_id_no_1: {
                validators: {
                    notEmpty: {
                        message: 'No. Identitas Passenger diperlukan dan tidak boleh kosong'
                    },
                    alpha_numeric: {
                        message: 'hanya menggunakan alpha numeric karakter'
                    }
                }
            },
            pax_birthdate_1: {
                validators: {
                    notEmpty: {
                        message: 'Tanggal lahir Passenger diperlukan dan tidak boleh kosong'
                    },
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'Format tanggal salah'
                    }
                }
            },
            contact_no_1: {
                validators: {
                    notEmpty: {
                        message: 'No. Kontak digunakan dan tidak boleh kosong'
                    },
                    digits: {
                        message: 'No. kontak Harus Angka'
                    }
                }
            },
            pax_name_first_2: {
                validators: {
                    notEmpty: {
                        message: 'Nama depan Passenger diperlukan dan tidak boleh kosong'
                    },
                    alpha: {
                        message: 'hanya menggunakan alpha karakter'
                    }
                }
            },
            pax_name_last_2: {
                validators: {
                    notEmpty: {
                        message: 'Nama belakang Passenger diperlukan dan tidak boleh kosong'
                    },
                    alpha: {
                        message: 'hanya menggunakan alpha karakter'
                    }
                }
            },
            pax_id_no_2: {
                validators: {
                    notEmpty: {
                        message: 'No Identitas Passenger diperlukan dan tidak boleh kosong'
                    },
                    alpha_numeric: {
                        message: 'hanya menggunakan alpha numeric karakter'
                    }
                }
            },
            pax_birthdate_2: {
                validators: {
                    notEmpty: {
                        message: 'Tanggal lahir Passenger diperlukan dan tidak boleh kosong'
                    },
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'Format tanggal salah'
                    }
                }
            },
            contact_no_2: {
                validators: {
                    notEmpty: {
                        message: 'No. kontak digunakan dan tidak boleh kosong'
                    },
                    digits: {
                        message: 'No. kontak Harus Angka'
                    }
                }
            },
            pax_name_first_3: {
                validators: {
                    notEmpty: {
                        message: 'Nama depan Passenger diperlukan dan tidak boleh kosong'
                    },
                    alpha: {
                        message: 'hanya menggunakan alpha karakter'
                    }
                }
            },
            pax_name_last_3: {
                validators: {
                    notEmpty: {
                        message: 'Nama belakang Passenger diperlukan dan tidak boleh kosong'
                    },
                    alpha: {
                        message: 'hanya menggunakan alpha karakter'
                    }
                }
            },
            pax_id_no_3: {
                validators: {
                    notEmpty: {
                        message: 'No identitas Passenger diperlukan dan tidak boleh kosong'
                    },
                    alpha_numeric: {
                        message: 'hanya menggunakan alpha numeric karakter'
                    }
                }
            },
            pax_birthdate_3: {
                validators: {
                    notEmpty: {
                        message: 'Tanggal lahir Passenger diperlukan dan tidak boleh kosong'
                    },
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'Format tanggal salah'
                    }
                }
            },
            contact_no_3: {
                validators: {
                    notEmpty: {
                        message: 'No. kontak digunakan Dan tidak boleh kosong'
                    },
                    digits: {
                        message: 'No. kontak Harus Angka'
                    }
                }
            },
            pax_name_first_4: {
                validators: {
                    notEmpty: {
                        message: 'Nama depan Passenger diperlukan dan tidak boleh kosong'
                    }
                }
            },
            pax_name_last_4: {
                validators: {
                    notEmpty: {
                        message: 'Nama belakang Passenger diperlukan dan tidak boleh kosong'
                    },
                    alpha: {
                        message: 'hanya menggunakan alpha karakter'
                    }
                }
            },
            pax_id_no_4: {
                validators: {
                    notEmpty: {
                        message: 'No Identitas Passenger diperlukan dan tidak boleh kosong'
                    },
                    alpha_numeric: {
                        message: 'hanya menggunakan alpha numeric karakter'
                    }
                }
            },
            pax_birthdate_4: {
                validators: {
                    notEmpty: {
                        message: 'Tanggal lahir Passenger diperlukan dan tidak boleh kosong'
                    },
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'Format tanggal salah'
                    }
                }
            },
            contact_no_4: {
                validators: {
                    notEmpty: {
                        message: 'No. kontak digunakan Dan tidak boleh kosong'
                    },
                    digits: {
                        message: 'No. kontak Harus Angka'
                    }
                }
            },
            pax_name_first_5: {
                validators: {
                    notEmpty: {
                        message: 'Nama depan Passenger diperlukan dan tidak boleh kosong'
                    },
                    alpha: {
                        message: 'hanya menggunakan alpha karakter'
                    }
                }
            },
            pax_name_last_5: {
                validators: {
                    notEmpty: {
                        message: 'Nama belakang Passenger diperlukan dan tidak boleh kosong'
                    },
                    alpha: {
                        message: 'hanya menggunakan alpha karakter'
                    }
                }
            },
            pax_id_no_5: {
                validators: {
                    notEmpty: {
                        message: 'No Identitas Passenger diperlukan dan tidak boleh kosong'
                    },
                    alpha_numeric: {
                        message: 'hanya menggunakan alpha numeric karakter'
                    }
                }
            },
            pax_birthdate_5: {
                validators: {
                    notEmpty: {
                        message: 'Tanggal lahir Passenger diperlukan dan tidak boleh kosong'
                    },
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'Format tanggal salah'
                    }
                }
            },
            contact_no_5: {
                validators: {
                    notEmpty: {
                        message: 'No. kontak digunakan Dan tidak boleh kosong'
                    },
                    digits: {
                        message: 'No. kontak Harus Angka'
                    }
                }
            }
        }
    }).on('success.form.bv', function (e, data) {
        // Prevent form submission
        e.preventDefault();

        // You can get the form instance
        var $form = $(e.target);

        // and the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        loadingbar();
        bv.defaultSubmit();

    });
});