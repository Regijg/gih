$(document).ready(function() {
    // Select2===============================================================
    $(".select2, .select2-multi").select2();
    $('.select-search-hide').select2({
        minimumResultsForSearch: -1
    });

    //datepicker=============================================================
    $.fn.datepicker.dates['id'] = {
        days: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"],
        daysShort: ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab", "Min"],
        daysMin: ["Mg", "Sn", "Sl", "Rb", "Km", "Jm", "Sb", "Mg"],
        months: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "Desember"],
        monthsShort: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Des"],
        today: "Hari Ini",
        clear: "Clear"
    };
    
    // ================ datepicket set for brithday ======================
    $('.date-years').datepicker({
        language: 'id',
        format: "yyyy-mm-dd",
        autoclose: true,
        endDate: '0d',
        startView: 2
    }).on('changeDate', function (e) {
        $('#form-book').bootstrapValidator('revalidateField', 'pax_birthdate_1');
    });
    
    // ================ datepicket set for search flight ======================
    $(".start").datepicker({
        language: 'id',
        todayHighlight: true,
        autoclose: true,
        startDate: '0d',
        format: 'yyyy-mm-dd'
    }).on('changeDate', function(e) {
        $(".end").datepicker({autoclose: true}).datepicker('setStartDate', e.date).focus();
        $('#formSearch_flight').bootstrapValidator('revalidateField', 'start');
    });
    $(".end").datepicker({
        language: 'id',
        autoclose: true,
        startDate: '0d',
        format: 'yyyy-mm-dd'
    }).on('changeDate', function(e) {
       $('#formSearch_flight').bootstrapValidator('revalidateField', 'end');
    });
    
    // ================ datepicket set for report filter ======================
    $(".start-filter").datepicker({
        language: 'id',
        todayHighlight: true,
        autoclose: true,
        format: 'yyyy-mm-dd'
    }).on('changeDate', function(e) {
        $(".end-filter").datepicker({autoclose: true}).datepicker('setStartDate', e.date).focus();
        $('.report-filter').bootstrapValidator('revalidateField', 'start_date');
    });
    $(".end-filter").datepicker({
        language: 'id',
        todayHighlight: true,
        autoclose: true,
        format: 'yyyy-mm-dd'
    }).on('changeDate', function(e) {
       $('.report-filter').bootstrapValidator('revalidateField', 'end_date');
    });

    //TYPEAHEAD GET AIRPORT===================================================
    function obtainer(query, cb) {
        var filteredList = $.grep(suggestionsArray, function (item, index) {
            return item.match(query);
        });

        mapped = $.map(filteredList, function (item) {
            return {value: item};
        });
        cb(mapped);
    }

    $('.typeahead-depart').typeahead({
        hint: false,
        highlight: false,
        minLength: 0
    }, {
        name: "noname",
        displayKey: "value",
        matcher : function (item) {
            return true;
        },
        source: obtainer
    }).on('blur', function (e) {
        $('#formSearch_flight').bootstrapValidator('revalidateField', 'from');
    });

    $('.typeahead-depart').on("focus", function () {
        ev = $.Event("keydown");
        ev.keyCode = ev.which = 40;
        $(this).trigger(ev);
        return true;
    });


    $('.typeahead-return').typeahead({
        hint: false,
        highlight: false,
        minLength: 0
    }, {
        name: "noname",
        displayKey: "value",
        matcher : function (item) {
            return true;
        },
        source: obtainer
    }).on('blur', function (e) {
        $('#formSearch_flight').bootstrapValidator('revalidateField', 'to');
    });
    
    $('.typeahead-return').on("focus", function () {
        ev = $.Event("keydown");
        ev.keyCode = ev.which = 40;
        $(this).trigger(ev);
        return true;
    });
    
    //Validation form Filter Report=======================================================
    $('.report-filter').bootstrapValidator({
        fields: {
            start_date: {
                validators: {
                    notEmpty: {
                        message: 'tidak boleh kosong'
                    },
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'Format tanggal salah'
                    }
                }
            },
            end_date: {
                validators: {
                    notEmpty: {
                        message: 'tidak boleh kosong'
                    },
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'Format tanggal salah'
                    }
                }
            }
        }
    });
    
    //Validation form=======================================================
    $('#formSearch_flight').bootstrapValidator({
        fields: {
            from: {
                validators: {
                    notEmpty: {
                        message: 'Kota Keberangkatan diperlukan dan tidak boleh kosong'
                    },
                    different: {
                        field: 'to',
                        message: 'Kota Keberangkatan Tidak Boleh sama'
                    }
                }
            },
            to: {
                validators: {
                    notEmpty: {
                        message: 'Kota Tujuan diperlukan dan tidak boleh kosong'
                    },
                    different: {
                        field: 'from',
                        message: 'Kota Tujuan Tidak Boleh sama'
                    }
                }
            },
            start: {
                validators: {
                    notEmpty: {
                        message: 'Tanggal Tujuan diperlukan dan tidak boleh kosong'
                    },
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'Format tanggal salah'
                    }
                }
            },
            end: {
                validators: {
                    notEmpty: {
                        message: 'Tanggal Pulang diperlukan dan tidak boleh kosong'
                    },
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'Format tanggal salah'
                    }
                }
            }
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // You can get the form instance
        var $form = $(e.target);

        // and the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Check Count Passengers
        var total_pax = parseInt($('#formSearch_flight *[name="adult"]').val()) +
                        parseInt($('#formSearch_flight *[name="infant"]').val()) +
                        parseInt($('#formSearch_flight *[name="child"]').val());
        if(total_pax < 7){
            loadingbar();
            bv.defaultSubmit();
        }else{
            $('#formSearch_flight .alert-pax').html('*Maksimal jumlah penumpang secara akumulatif adalah 7').removeClass('hidden');
        }
    });
    
    //VALIDATION FORM DETAIL FLIGHT================================
    $('#form-book').bootstrapValidator({
        fields: {
            customer_name: {
                validators: {
                    notEmpty: {
                        message: 'nama diperlukan dan tidak boleh kosong'
                    },
                    alpha_space: {
                        message: 'nama hanya menggunakan alpha karakter'
                    }
                }
            },
            customer_email: {
                validators: {
                    notEmpty: {
                        message: 'email pelaggan diperlukan dan tidak boleh kosong'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    }
                }
            },
            customer_phone: {
                validators: {
                    notEmpty: {
                        message: 'No. Telepon digunakan Dan tidak boleh kosong'
                    },
                    digits: {
                        message: 'No. Telepon Harus Angka'
                    }
                }
            },
            pax_name_first_1: {
                validators: {
                    notEmpty: {
                        message: 'Nama depan Passanger diperlukan dan tidak boleh kosong'
                    },
                    alpha: {
                        message: 'hanya menggunakan alpha karakter'
                    }
                }
            },
            pax_name_last_1: {
                validators: {
                    notEmpty: {
                        message: 'Nama belakang Passenger diperlukan dan tidak boleh kosong'
                    },
                    alpha: {
                        message: 'hanya menggunakan alpha karakter'
                    }
                }
            },
            pax_id_no_1: {
                validators: {
                    alpha_numeric: {
                        message: 'hanya menggunakan alpha numeric karakter'
                    }
                }
            },
            pax_country_pasport_1: {
                validators: {
                    notEmpty: {
                        message: 'Negara Asal diperlukan dan tidak boleh kosong'
                    }
                }
            },
            pax_birthdate_1: {
                validators: {
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'Format tanggal salah'
                    }
                }
            },
            contact_no_1: {
                validators: {
                    digits: {
                        message: 'No. Telepon Harus Angka'
                    }
                }
            },
            pax_name_first_2: {
                validators: {
                    notEmpty: {
                        message: 'Nama depan Passanger diperlukan dan tidak boleh kosong'
                    },
                    alpha: {
                        message: 'hanya menggunakan alpha karakter'
                    }
                }
            },
            pax_name_last_2: {
                validators: {
                    notEmpty: {
                        message: 'Nama belakang Passenger diperlukan dan tidak boleh kosong'
                    },
                    alpha: {
                        message: 'hanya menggunakan alpha karakter'
                    }
                }
            },
            pax_id_no_2: {
                validators: {
                    alpha_numeric: {
                        message: 'hanya menggunakan alpha numeric karakter'
                    }
                }
            },
            pax_country_pasport_2: {
                validators: {
                    notEmpty: {
                        message: 'Negara Asal diperlukan dan tidak boleh kosong'
                    }
                }
            },
            pax_birthdate_2: {
                validators: {
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'Format tanggal salah'
                    }
                }
            },
            contact_no_2: {
                validators: {
                    digits: {
                        message: 'No. Telepon Harus Angka'
                    }
                }
            },
            pax_name_first_3: {
                validators: {
                    notEmpty: {
                        message: 'Nama depan Passanger diperlukan dan tidak boleh kosong'
                    },
                    alpha: {
                        message: 'hanya menggunakan alpha karakter'
                    }
                }
            },
            pax_name_last_3: {
                validators: {
                    notEmpty: {
                        message: 'Nama belakang Passenger diperlukan dan tidak boleh kosong'
                    },
                    alpha: {
                        message: 'hanya menggunakan alpha karakter'
                    }
                }
            },
            pax_id_no_3: {
                validators: {
                    alpha_numeric: {
                        message: 'hanya menggunakan alpha numeric karakter'
                    }
                }
            },
            pax_country_pasport_3: {
                validators: {
                    notEmpty: {
                        message: 'Negara Asal diperlukan dan tidak boleh kosong'
                    }
                }
            },
            pax_birthdate_3: {
                validators: {
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'Format tanggal salah'
                    }
                }
            },
            contact_no_3: {
                validators: {
                    digits: {
                        message: 'No. Telepon Harus Angka'
                    }
                }
            },
            pax_name_first_4: {
                validators: {
                    notEmpty: {
                        message: 'Nama depan Passanger diperlukan dan tidak boleh kosong'
                    },
                    alpha: {
                        message: 'hanya menggunakan alpha karakter'
                    }
                }
            },
            pax_name_last_4: {
                validators: {
                    notEmpty: {
                        message: 'Nama belakang Passenger diperlukan dan tidak boleh kosong'
                    },
                    alpha: {
                        message: 'hanya menggunakan alpha karakter'
                    }
                }
            },
            pax_id_no_4: {
                validators: {
                    alpha_numeric: {
                        message: 'hanya menggunakan alpha numeric karakter'
                    }
                }
            },
            pax_country_pasport_4: {
                validators: {
                    notEmpty: {
                        message: 'Negara Asal diperlukan dan tidak boleh kosong'
                    }
                }
            },
            pax_birthdate_4: {
                validators: {
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'Format tanggal salah'
                    }
                }
            },
            contact_no_4: {
                validators: {
                    digits: {
                        message: 'No. Telepon Harus Angka'
                    }
                }
            },
            pax_name_first_5: {
                validators: {
                    notEmpty: {
                        message: 'Nama depan Passanger diperlukan dan tidak boleh kosong'
                    },
                    alpha: {
                        message: 'hanya menggunakan alpha karakter'
                    }
                }
            },
            pax_name_last_5: {
                validators: {
                    notEmpty: {
                        message: 'Nama belakang Passenger diperlukan dan tidak boleh kosong'
                    },
                    alpha: {
                        message: 'hanya menggunakan alpha karakter'
                    }
                }
            },
            pax_id_no_5: {
                validators: {
                    alpha_numeric: {
                        message: 'hanya menggunakan alpha numeric karakter'
                    }
                }
            },
            pax_country_pasport_5: {
                validators: {
                    notEmpty: {
                        message: 'Negara Asal diperlukan dan tidak boleh kosong'
                    }
                }
            },
            pax_birthdate_5: {
                validators: {
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'Format tanggal salah'
                    }
                }
            },
            contact_no_5: {
                validators: {
                    digits: {
                        message: 'No. Telepon Harus Angka'
                    }
                }
            }
        }
    }).on('success.form.bv', function (e) {
        // Prevent form submission
        e.preventDefault();

        // You can get the form instance
        var $form = $(e.target);

        // and the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        loadingbar();
        bv.defaultSubmit();
    });
});