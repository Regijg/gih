$(document).ready(function () {
    // Select2===============================================================
    $(".select2, .select2-multi").select2();
    $('.select-search-hide').select2({
        minimumResultsForSearch: -1
    });

    //datepicker=============================================================
    $.fn.datepicker.dates['id'] = {
        days: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu", "Minggu"],
        daysShort: ["Min", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab", "Min"],
        daysMin: ["Mg", "Sn", "Sl", "Rb", "Km", "Jm", "Sb", "Mg"],
        months: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "Desember"],
        monthsShort: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Des"],
        today: "Hari Ini",
        clear: "Clear"
    };

    $("a[data-rel^='prettyPhoto']").prettyPhoto();

    $(".check-in").datepicker({
        language: 'id',
        todayHighlight: true,
        autoclose: true,
        startDate: '0d',
        format: 'yyyy-mm-dd'
    }).on('changeDate', function (e) {
        var newDate = new Date(e.date);
        newDate.setDate(newDate.getDate() + 1);
        $(".check-out").datepicker({autoclose: true}).datepicker('setStartDate', newDate).focus();
        $('#seach-hotel').bootstrapValidator('revalidateField', 'checkin');
    });

    $(".check-out").datepicker({
        language: 'id',
        autoclose: true,
        startDate: '0d',
        format: 'yyyy-mm-dd'
    }).on('changeDate', function (e) {
        $('#seach-hotel').bootstrapValidator('revalidateField', 'checkout');
    });

    $('.typeahead-city').typeahead({
        hint: true,
        highlight: true,
        minLength: 2,
        limit: 10
    }, {
        source: function (q, cb) {
            return $.ajax({
                dataType: 'json',
                type: 'GET',
                url: base_url + 'hotel/get_city/' + q,
                chache: false,
                beforeSend: function () {
                    $('.loading-search').removeClass('glyphicon-map-marker');
                    $('.loading-search').addClass('glyphicon-cog fa-spin');
                },
                success: function (data) {
                    $('.loading-search').addClass('glyphicon-map-marker');
                    $('.loading-search').removeClass('glyphicon-cog fa-spin');
                    var matches, substrRegex;
                    matches = [];
                    substrRegex = new RegExp(q, 'i');

                    $.each(data, function (i, str) {
                        if (substrRegex.test(str)) {
                            matches.push({value: str});
                        }
                    });
                    cb(matches);
                }
            });
        }
    }).on('blur', function (e) {
        $('#seach-hotel').bootstrapValidator('revalidateField', 'city');
    });

    //VALIDATION FORM BOOK HOTEL================================
    $('#seach-hotel').bootstrapValidator({
        fields: {
            city: {
                validators: {
                    notEmpty: {
                        message: 'Tujuan menginap diperlukan dan tidak boleh kosong!'
                    }, stringLength: {
                        min: 3,
                        message: 'Minimal Karakter 3'
                    }
                }
            },
            checkin: {
                validators: {
                    notEmpty: {
                        message: 'Tanggal Check-In diperlukan dan tidak boleh kosong'
                    },
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'Format tanggal salah'
                    }
                }
            },
            checkout: {
                validators: {
                    notEmpty: {
                        message: 'Tanggal Check-Out diperlukan dan tidak boleh kosong'
                    },
                    date: {
                        format: 'YYYY-MM-DD',
                        message: 'Format tanggal salah'
                    }
                }
            }
        }
    }).on('success.form.bv', function (e, data) {
        // Prevent form submission
        e.preventDefault();

        // You can get the form instance
        var $form = $(e.target);

        // and the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        loadingbar();
        bv.defaultSubmit();
    });
    //VALIDATION FORM SEARCH HOTEL================================

    //VALIDATION FORM BOOK HOTEL================================
    $('#book-hotel').bootstrapValidator({
        fields: {
            guest_name: {
                validators: {
                    notEmpty: {
                        message: 'Nama Tamu diperlukan dan tidak boleh kosong!'
                    },
                    alpha_space: {
                        message: 'nama hanya menggunakan alpha karakter'
                    }
                }
            },
            email_cust: {
                validators: {
                    notEmpty: {
                        message: 'Alamat Email diperlukan dan tidak boleh kosong'
                    },
                    emailAddress: {
                        message: 'Format email tidak valid'
                    }
                }
            },
            mobile_cust: {
                validators: {
                    notEmpty: {
                        message: 'No. Telepon diperlukan dan tidak boleh kosong'
                    },
                    digits: {
                        message: 'No. Telepon Harus Angka'
                    }
                }
            },
            margin: {
                validators: {
                    digits: {
                        message: 'Nominal Margin Harus Angka'
                    }
                }
            }
        }
    }).on('success.form.bv', function (e, data) {
        // Prevent form submission
        e.preventDefault();

        // You can get the form instance
        var $form = $(e.target);

        // and the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        loadingbar();
        bv.defaultSubmit();
    });
    //VALIDATION FORM BOOK HOTEL================================

});