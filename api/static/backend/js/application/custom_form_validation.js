$(document).ready(function () {
   /* TRIGER 'alpha' */ 
   $.fn.bootstrapValidator.validators.alpha = {
        validate: function(validator, $field, options) {
            var value = $field.val();
            if (value === '') {
                return true;
            }
            
            var letters = /^[a-zA-Z]+$/; 
            if (!value.match(letters)) {
                return false;
            }
            
            return true;
        }
    };
    
    /* TRIGER 'alpha_numeric' */
    $.fn.bootstrapValidator.validators.alpha_numeric = {
        validate: function(validator, $field, options) {
            var value = $field.val();
            if (value === '') {
                return true;
            }
            
            var letters = /^[A-z0-9]+$/i; 
            if (!value.match(letters)) {
                return false;
            }
            
            return true;
        }
    };
    
    /* TRIGER 'alpha_space' */ 
   $.fn.bootstrapValidator.validators.alpha_space = {
        validate: function(validator, $field, options) {
            var value = $field.val();
            if (value === '') {
                return true;
            }
            
            var letters = /^[a-zA-Z\s]+$/; 
            if (!value.match(letters)) {
                return false;
            }
            
            return true;
        }
    };
    
    /* TRIGER 'no_space' */ 
   $.fn.bootstrapValidator.validators.no_space = {
        validate: function(validator, $field, options) {
            var value = $field.val();
            if (value === '') {
                return true;
            }
            
            var letters = /^\S{3,}$/; 
            if (!value.match(letters)) {
                return false;
            }
            
            return true;
        }
    };
});