

//dataTable default
$('#dataTable').dataTable({responsive: true});
//dataTable for Report
$('.dataTable-report').dataTable({
    responsive: true,
    bPaginate: false,
    aaSorting: [[0, "DESC"]],
    bFilter: false, bInfo: false
});