<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class search extends CI_Controller
{
	function __construct()
    {
        parent::__construct();
		$this->load->library('webservice/flight');
    }
	
	function search_detail($airline, $akses_kode,$client_app,$agent_id = NULL)
	{
		$postDat = json_decode($this->converter->decode($this->input->post('data')),true);
		#$postDat = json_decode($this->converter->decode($data),true);
		$data	= $this->flight->price_search($airline, $postDat, $akses_kode, $agent_id,$client_app);
		$json	= json_encode($data);
		echo $json;
	}

	function search_detail_ntaminus(){
		$parameter 	= json_decode($this->input->post("parameter"), true);
		$session 	= json_decode($this->input->post("session"), true);
		$data		= $this->ntaminus->detail($parameter, $session);
		$json		= json_encode($data);
		echo $json;
	}

	function book()
	{
		$airlines	= $this->input->post('airlines');
		$sessiondata= json_decode($this->input->post('session'),true);
		$psgr		= json_decode($this->input->post('psgr'),true);
		$cust		= json_decode($this->input->post('cust'),true);
		
		$data	= $this->flight->book_airlines($airlines, $sessiondata, $psgr, $cust);
		$json	= json_encode($data);
		echo $json;
	}

	function book_ntaminus(){
		$parameter 	= json_decode($this->input->post("parameter"), true);
		$session 	= json_decode($this->input->post("session"), true);
		
		$trip 		= $this->input->post("trip");
		$data		= $this->ntaminus->book($parameter, $session, $trip);
		$json		= json_encode($data);
		echo $json;
	}
	
	function get_real_price_garuda(){
		$this->load->library('airlines/garuda');
		
		$search_post 	= json_decode($this->input->post('search_post'), true);
		$arr_class 		= json_decode($this->input->post('arr_class'), true);
		$idAcc			= $this->input->post("idAcc");
		
		$arr_price = $this->garuda->get_price($search_post['dep'], $search_post['arr'], $search_post['date'], $search_post['date'], $search_post['adult'], $search_post['child'], $search_post['infant'], $search_post['roundtrip'], $arr_class, $idAcc);
		
		echo json_encode($arr_price);
	}
}

?>
