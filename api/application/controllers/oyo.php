<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Oyo extends CI_Controller {

	
	public function getData($country) {
		
		// CEK TOTAL DATA OYO HOTEL FROM DB
		$cekData = $this->mgeneral->getWhere(array('country'=>$country),"properti");
		if(count($cekData)==0):
			$offset = 0;
			$exec = "lanjut";
		else:
			$offset = $cekData[0]->offset+100;
			if($cekData[0]->offset >= $cekData[0]->total):
				$exec = "stop";
			else:
				$exec = "lanjut";
			endif;
		endif;
		echo $offset."<br>";
		echo $exec."<br>";
		echo $cekData[0]->offset." > ".$cekData[0]->total;
		
		if($exec!="stop"):
			//CURL GET DATA OYO HOTEL
			$curl = curl_init();
			curl_setopt_array($curl, array(
			  CURLOPT_URL => "http://api.oyorooms.com/b2b_reseller/api/v2/hotels?offset=".$offset."&limit=100&countryCode=".$country,
			  CURLOPT_RETURNTRANSFER => true,
			  CURLOPT_ENCODING => "",
			  CURLOPT_MAXREDIRS => 10,
			  CURLOPT_TIMEOUT => 30,
			  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  CURLOPT_CUSTOMREQUEST => "GET",
			  CURLOPT_POSTFIELDS => "",
			  CURLOPT_HTTPHEADER => array(
			    "Cache-Control: no-cache",
			    "Content-Type: application/json",
			    "Postman-Token: 4afb2d22-6e20-a348-4a00-3b81be938bbe,33c50e40-8199-4946-bfc0-30f3a5d539b8",
			    "cache-control: no-cache",
			    "x-access-token: dnNKQ0t5c2drSkd5cW0zTmpMd1E6WDVUeGNucTYxMTdnMUZRZGFyMk0="
			  ),
			));
			$response = curl_exec($curl);
			$err = curl_error($curl);
			curl_close($curl);

			//PROCESS DATA OYO HOTEL
			if ($err) {
			  echo "cURL Error #:" . $err;
			} else {
			  $data = json_decode($response,true);

			  	//SAVE PROPERTI INFO
			  	$saveProperti = array('country'=>$data['hotels'][0]['countryCode'],
			  						  'offset'=>$data['offset'],
			  						  'total'=>$data['total'],
			  						  'exec_date'=>date('Y-m-d'));
			  	if(count($cekData)==0):
					$this->mgeneral->save($saveProperti,"properti");
				else:
					$this->mgeneral->update(array('country'=>$country),$saveProperti,"properti");
				endif;
			  	

				//EXEC HOTEL DATA TO DATABASE
			  	foreach ($data['hotels'] as $dt) {
			  		$saveData = $dt;
			  		$galeriImg = $dt['images'];
			  		unset($saveData['images']);
			  		$saveData['amenities'] 		= json_encode($saveData['amenities']);
			  		$saveData['policies'] 		= json_encode($saveData['policies']);
			  		$saveData['restrictions'] 	= json_encode($saveData['restrictions']);
			  		$saveData['street']			= $saveData['address']['street'];
			  		$saveData['city']			= $saveData['address']['city'];
			  		$saveData['state']			= $saveData['address']['state'];
			  		$saveData['country']		= $saveData['address']['country'];
			  		$saveData['zipcode']		= $saveData['address']['zipcode'];
			  		$saveData['latitude']		= $saveData['address']['latitude'];
			  		$saveData['longitude']		= $saveData['address']['longitude'];
			  		$saveData['update_date']	= date('Y-m-d h:i:s');
			  		unset($saveData['address']);

			  		$cekHotel = $this->mgeneral->getWhere(array('id'=>$saveData['id']),"hotel");
			  		if(count($cekHotel)==0):
			  			$this->mgeneral->save($saveData,'hotel');
			  		else:
			  			$this->mgeneral->update(array('id'=>$saveData['id']),$saveData,'hotel');
			  		endif;

			  		$this->mgeneral->delete(array('hotel_id'=>$saveData['id']),'hotel_image');
			  		foreach($galeriImg as $g){
			  			$saveImg = array('hotel_id'=>$saveData['id'],
			  							 'image'	=>$g);
			  			$this->mgeneral->save($saveImg,'hotel_image');
			  		}
			  		echo $saveData['id']."<br>";
			  	}
			}
		endif;

	}
}

/* End of file tester.php */
/* Location: ./application/controllers/tester.php */