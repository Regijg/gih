<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cron extends CI_Controller
{
	function __construct()
    {
        parent::__construct();
		$this->load->library('bank/bca');
		$this->load->library('bank/mandiri');
		#$this->load->library('bank/mandiri_corp');
    }
	
	public function expire_free_trial()
	{
		$sql = "SELECT * FROM `mstr_partner` WHERE `date_added` <= ( CURDATE() - INTERVAL 3 DAY ) AND skema_bisnis_id = '1'";
		$data_expire = $this->mgeneral->post_query_sql($sql);
		
		foreach($data_expire as $d):
			$varData = array('partner_status'=>'tidak_aktif');
			$this->mgeneral->update(array('partner_id'=>$d->partner_id),$varData,"mstr_partner");
			
			echo "Expire Agent Id = ".$d->partner_id."<Br>";
		endforeach;
		
		if(count($data_expire) == 0):
			echo "tidak ada yang expire";
		endif;
		
	}
	
	public function status_booking() {
        # Date Now
        $nowBaru = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . ' - 3 minute'));
		
        #Cek CTL
		$sql	= "select * from dt_flight 
								where (dtf_status='book' or dtf_status='payment_hold' or dtf_status='pending') 
								and (depart_expired_date <= '$nowBaru' or depart_expired_date IS NULL) 
								and (return_expired_date <= '$nowBaru' or return_expired_date IS NULL)
								UNION ALL
select * from dt_flight where  (dtf_status='book' or dtf_status='payment_hold' or dtf_status='pending') and LENGTH(depart_book_code)<'4' and  LENGTH(return_book_code)<'4' 
								UNION ALL
select * from dt_flight where  (dtf_status='book' or dtf_status='payment_hold' or dtf_status='pending') and (LENGTH(depart_book_code)<'4' or  LENGTH(return_book_code)<'4') and dtf_roundtrip='oneway' ";
        $data_ctl = $this->mgeneral->post_query_sql($sql);
		
		if(empty($data_ctl)) {
            echo 'CTL : 0';
        }else{
			foreach ($data_ctl as $data) {
				$data_updt 	= array('dtf_status' => "ctl", 'depart_book_status' => "ctl", 'return_book_status' => "ctl");
				$id 		= $data->dtf_id;
				$this->mgeneral->update(array('dtf_id'=>$id),$data_updt,'dt_flight');
				echo "$id = Expired (CTL)<br>";
			}
		}
        echo "<br>";
		
		$this->cek_user_garuda();
	}
	
	function expire_deposit_pending(){
		$pending		= $this->mgeneral->getWhere(array('status'=>'pending'),'dt_topup_deposit');
		if(empty($pending)):
			echo 'No Req Topup canceled';		
		else:
			foreach($pending as $row){
				$now			= date('Y-m-d H:i:s');
				$temp_date 		= strtotime(date("Y-m-d H:i:s", strtotime($row->transfer_savedate)) . " +1 days");
				$expire_date 	= date("Y-m-d H:i:s",$temp_date);
				if($now > $expire_date){
					$this->mgeneral->update(array('dt_topup_id'=>$row->dt_topup_id),array('status'=>'cancel'),'dt_topup_deposit');
					echo 'cancel success for Topup id :'.$row->dt_topup_id.'<br/>';
				}
				
			}
		endif;
	}
	
	function deposit_pending(){
		$cekReqDep	= $this->mgeneral->getWhere(array('status' => 'pending'),'dt_topup_deposit');
		
		if(!empty($cekReqDep)):
			$this->getMutasi(); #cek mutasi ketika ada deposit pending
			foreach($cekReqDep as $row):
				$dateCek	= str_replace('-','',$row->transfer_date);
				$nameCek	= explode("(",$row->transfer_from);
				$bankCek	= explode("(",$row->transfer_to);
				$bankCekFin = explode("/",$bankCek[1]);
				$akun_idCek	= $this->mgeneral->getValue('bank_akun_id',array('bank_akun_holder'	=> trim($bankCek[0]),
																			 'bank_akun_no'		=> trim($bankCekFin[0])),'mstr_bank_akun');
				$sql = 'select * from dt_mutasi_bank where bank_akun_id="'.$akun_idCek.'" and bank_mutation_datebank="'.$dateCek.'" and bank_mutation_amount="'.((int)str_replace('.', '', $row->transfer_amount)).'" and bank_mutation_status="0" and bank_mutation_text like "%'.rtrim($nameCek[0]).'%"';
				echo "<br>";
				# Cek Mutasi
				$MutasiCek	= $this->mgeneral->post_query_sql($sql);
				# Cek Mutasi
				
				if(!empty($MutasiCek)):
					# Update Mutasi Bank
					$this->mgeneral->update(array('bank_mutation_id' => $MutasiCek[0]->bank_mutation_id),array('bank_mutation_status' => '1','payment_for'=>"deposit",'payment_id'=>$row->dt_topup_id),'dt_mutasi_bank');
					$this->mgeneral->update(array('dt_topup_id' => $row->dt_topup_id),array('status' => 'success'),'dt_topup_deposit');
					# Update Mutasi Bank
					
					$post_data['status'] = 'approve';
					$post_data['approval_message'] = $MutasiCek[0]->bank_mutation_text;
					
					# Proses Add Deposit
						# Get Data Partner
						$dataPartner	= $this->mgeneral->getWhere(array('partner_id' => $row->partner_id),'mstr_partner');															 						$amount			= ((int)str_replace('.', '', $row->transfer_amount));
						$curDeposit		= $dataPartner[0]->current_deposit;
						$lastDeposit	= $curDeposit+$amount;
						
						# Update Deposit History						 
						$this->mgeneral->save(array( 'partner_id' 			=> $row->partner_id,
													 'payment_date'			=> date('Y-m-d H:i:s'),
													 'payment_amount'		=> $amount,
													 'payment_last_deposit'	=> $lastDeposit,
													 'payment_type'			=> 'debet',
													 'payment_referensi'	=> $MutasiCek[0]->bank_mutation_id,
													 'payment_status'		=> 'ok',
													 'payment_message'		=> 'Penambahan Deposit sebesar '.((int)str_replace('.', '', $row->transfer_amount)).''),'dt_deposit_history');
						# Update Deposit Partner
						$sql = "UPDATE mstr_partner SET current_deposit = current_deposit+" . $amount . " WHERE partner_id = '".$row->partner_id."' ";
						$this->db->query ($sql);
					# Proses Add Deposit
				else:
					echo "Mutasi belum terdeteksi <br>";
				endif;
				
			endforeach;
			
		else:
			echo "tidak ada list cek deposit pending<br>";
		endif;

	}
	
	function getMutasi()
	{
		echo "<pre>";
		$stardate	= date('Y-m-d');
		$enddate	= date('Y-m-d');
		
		$bankAkun = $this->mgeneral->getAll("mstr_bank_akun");
		#$bankAkun = $this->mgeneral->getWhere(array('bank_akun_id'=>"1"),"mstr_bank_akun");
		foreach($bankAkun as $ba):
			
			echo "<br><br>Save Mutasi Rekening ".$ba->bank_name."(".$ba->bank_akun_id.") : <br>";
			
			switch(strtoupper($ba->bank_name)):
				
				case "BCA";
					
					$bca_data = $this->bca->get_mutasi($ba->bank_akun_id,$account_no,$account_holder,$stardate,$enddate);
					foreach($bca_data['mutasi'] as $db):
						
						$amount		= explode(".",$db['mutasi_amount']);
						$amount		= str_replace(",","",$amount[0]);
						$dateMutasi	= explode("/",$db['mutasi_date']);
						
						if($db['mutasi_type']=="DB"):
							$status = "1";
						else:
							$status = "0";
						endif;
						
						$varData = array('bank_akun_id'				=> $ba->bank_akun_id,
										 'bank_mutation_date'		=> date('Y-m-d H:i:s'),
										 'bank_mutation_datebank'	=> $dateMutasi[2].$dateMutasi[1].$dateMutasi[0],
										 'bank_mutation_amount'		=> $amount,
										 'bank_mutation_type'		=> $db['mutasi_type'],
										 'bank_mutation_text'		=> preg_replace('/\s+/', ' ', $db['mutasi_note']),
										 'bank_mutation_status'		=> $status);
										 
						$cekData = $this->mgeneral->post_query_sql("SELECT * FROM `dt_mutasi_bank` WHERE `bank_akun_id` = '".$ba->bank_akun_id."' AND bank_mutation_datebank = '".$dateMutasi[2].$dateMutasi[1].$dateMutasi[0]."' AND bank_mutation_amount = '".$amount."' AND bank_mutation_type = '".$db['mutasi_type']."' AND replace(bank_mutation_text,' ','') = '".str_replace(" ","",preg_replace('/\s+/', ' ', $db['mutasi_note']))."'");
						
						if(count($cekData)==0 && $amount > 10):
							$this->mgeneral->save($varData,"dt_mutasi_bank");
						endif;
						
						print_r($varData);
						echo "<br><br>";
						
					endforeach;
					
				break;
				
				case "MANDIRI";
				
					$mandiri_data = $this->mandiri->get_mutasi($ba->bank_akun_id,$account_no,$account_holder,$stardate,$enddate); #mandiri personal
					#$mandiri_data = $this->mandiri_corp->get_mutasi($ba->bank_akun_id,$account_no,$account_holder,$stardate,$enddate); #mandiri corporate
					
					foreach($mandiri_data['mutasi'] as $dm):
						
						#mandiri personal
						$amount		= explode(",",$dm['mutasi_amount']);
						$amount		= str_replace(".","",$amount[0]);
						$dateMutasi	= explode("/",$dm['mutasi_date']);
						
						#mandiri_corp
						/*$amount		= explode(".",$dm['mutasi_amount']);
						$amount		= str_replace(",","",$amount[0]);
						$dateMutasi	= explode("/",$dm['mutasi_date']);*/
						
						if($dm['mutasi_type']=="DB"):
							$status = "1";
						else:
							$status = "0";
						endif;
						
						$varData2 = array('bank_akun_id'			=> $ba->bank_akun_id,
										 'bank_mutation_date'		=> date('Y-m-d H:i:s'),
										 'bank_mutation_datebank'	=> $dateMutasi[2].$dateMutasi[1].$dateMutasi[0],
										 'bank_mutation_amount'		=> $amount,
										 'bank_mutation_type'		=> $dm['mutasi_type'],
										 'bank_mutation_text'		=> preg_replace('/\s+/', ' ', $dm['mutasi_note']),
										 'bank_mutation_status'		=> $status);
										 
						$cekData2 = $this->mgeneral->post_query_sql("SELECT * FROM `dt_mutasi_bank` WHERE `bank_akun_id` = '".$ba->bank_akun_id."' AND bank_mutation_datebank = '".$dateMutasi[2].$dateMutasi[1].$dateMutasi[0]."' AND bank_mutation_amount = '".$amount."' AND bank_mutation_type = '".$dm['mutasi_type']."' AND replace(bank_mutation_text,' ','') = '".str_replace(" ","",preg_replace('/\s+/', ' ', $dm['mutasi_note']))."'");
						
						if(count($cekData2)==0 && $amount > 10):
							$this->mgeneral->save($varData2,"dt_mutasi_bank");
						endif;
						
						print_r($varData2);
						echo "<br><br>";
						
					endforeach;
				
				break;
				
			endswitch;
			
		endforeach;
		
	}
}

?>
