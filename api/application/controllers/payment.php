<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment extends CI_Controller {
	public $arr_product 		= array("topup", "flight", "train", "ecommerce");
	public $arr_payment_type 	= array("deposit", "TF", "BK", "VC", "MY", "CK", "BT");
	public $api_key 			= "2222";
	public $url					= "https://api-palapa.appsku.tech/";
	public $api_url				= "https://api-palapa.appsku.tech/ws";
	public $ecommerce_url		= "https://apitoko-palapa.appsku.tech/api/";
	public $duitku_url			= "http://sandbox.duitku.com/webapi/api/merchant/inquiry"; #https://passport.duitku.com/webapi/api/merchant/inquiry
	public $duitku_merchant_code = "D4763";
	public $duitku_api_key 		= "c667e9d25c0ef5dde0b1a40bb4111508";
	#public $ecommerce_header 	= array("key : e11f56378dfc85b937b56ff473c34cb557e30f00");
	function __construct()
	{
		parent::__construct();
		$this->load->library('curl');
		$this->load->library('api/wsdeposit');
	}
	
	/*function view_pay()
	{
		$data["amount"] = "100000";
		$bank = $this->mgeneral->getWhere(array('status'=>"on"),"sys_bank_akun");
		$data['bank']	= $bank;
		$this->load->view("payment/transfer2", $data);
	}*/
	
	public function create_order(){
		$product 		= $_GET["product"];
		$transaction_id = $_GET["transaction_id"];
		$amount 		= $_GET["amount"];
		$payment_type	= $_GET["payment_type"];
		$partner_id		= $_GET["partner_id"];

		if(in_array($product, $this->arr_product) && in_array($payment_type, $this->arr_payment_type)){
			if($product == "flight"){
				$this->order_flight($product, $transaction_id, $amount, $payment_type, $partner_id);
			}
			else if($product == "train"){
				$this->order_train($product, $transaction_id, $amount, $payment_type, $partner_id);
			}
			else if($product == "ecommerce"){
				$this->order_ecommerce($product, $transaction_id, $amount, $payment_type, $partner_id);
			}
			else{
				$this->order_topup($product, $transaction_id, $amount, $payment_type, $partner_id);
			}
		}
		else if(!in_array($product, $this->arr_product)){
			$data['reason'] = "Produk tidak terdaftar";
			$this->load->view("payment/error", $data);
		}
		else{
			$data['reason'] = "Payment type tidak terdaftar";
			$this->load->view("payment/error", $data);
		}
	}

	private function order_flight($product, $transaction_id, $amount, $payment_type, $partner_id){
		$param_get_product	= array("api_key" 	=> $this->api_key,
									"format"	=> "",
									"ws"		=> "report",
									"call"		=> "flight_detail",
									"book_id"	=> $transaction_id);
		$get_product = $this->curl->post_custom($this->api_url, "", $param_get_product);
		if($get_product['rest_no'] == "0"){
			$get_product = json_decode($get_product['result'], true);
		}

		if($get_product['rest_no'] == "0"){
			$amount = $get_product['data']['total_fares'] + $get_product['data']['margin'];
			if($get_product['data']['return']):
				$kdBook		= $get_product['data']["schedule"]['depart']['booking_code']." dan ".$get_product['data']["schedule"]['return']['booking_code'];
			else:
				$kdBook		= $get_product['data']["schedule"]['depart']['booking_code'];
			endif;			
			$message		= "Pembelian tiket pesawat kode booking ".$kdBook;

			$rand_no = $this->getUnikNo();
			$order_id = $this->save_order($product, $transaction_id, $amount, $payment_type, $partner_id, $message);

			if($payment_type == "TF"){
				
				$amount = substr($amount, 0, -3).$rand_no;
				$this->mgeneral->update(array("id" => $order_id), array("amount" => $amount, "unique_fare" => $rand_no), "payment_order");
				$status = true;
					
				if($status == true){
					$data["amount"] = $amount;
					$bank = $this->mgeneral->getWhere(array('status'=>"on"),"sys_bank_akun");
					$data['bank']	= $bank;
					$this->load->view("payment/transfer", $data);	
				}
				else{
					$this->mgeneral->update(array("id" => $order_id), array("status" => "cancel"), "payment_order");
					$data["reason"] = "terjadi gangguan pada sistem payment";
					$this->load->view("payment/error", $data);	
				}
			}
			else if($payment_type == "deposit"){
				$actionPayment	= cek_deposit_dan_bayar($this->converter->decode($partner_id), $amount, $transaction_id, $message);
				if($actionPayment["error_no"] == "0"){
					$this->mgeneral->update(array("id" => $order_id), array("status" => "used"), "payment_order");

					$param_payment	= array("api_key" 	=> $this->api_key,
											"format"	=> "",
											"ws"		=> "flight",
											"call"		=> "ws_payment_with_gateway",
											"book_id"	=> $transaction_id);
					$result = $this->curl->post_custom($this->api_url, "", $param_payment);
					$data['message'] = "Payment berhasil, silahkan cek data transaksi";
					$this->load->view("payment/result", $data);
				}
				else{
					$this->mgeneral->update(array("id" => $order_id), array("status" => "cancel"), "payment_order");
					$data['reason'] = $actionPayment['error_msg'];
					$this->load->view("payment/error", $data);
				}
			}
			else{

				$merchantCode 		= $this->duitku_merchant_code;
			    $merchantKey 		= $this->duitku_api_key;
			    $paymentAmount 		= $amount;
			    $paymentMethod 		= $payment_type; // WW = duitku wallet, VC = Credit Card, MY = Mandiri Clickpay, BK = BCA KlikPay
			    $merchantOrderId 	= $order_id; //$this->converter->encode($order_id) from merchant, unique
			    $productDetails 	= $message;
			    $email 				= $get_product['data']['customer']['customer_email']; // your customer email
			    $phoneNumber 		= $get_product['data']['customer']['customer_phone']; // your customer phone number (optional)
			    $additionalParam 	= $partner_id; // optional
			    $callbackUrl 		= $this->url . 'payment/callback?from=duitku'; // url for callback
			    $returnUrl 			= $this->url . 'payment/result'; // url for redirect

			    $signature = md5($merchantCode . $merchantOrderId . $paymentAmount . $merchantKey);

			    $req_params = array(
			        'merchantCode' 		=> $merchantCode,
			        'paymentAmount' 	=> $paymentAmount,
			        'paymentMethod' 	=> $paymentMethod,
			        'merchantOrderId' 	=> $merchantOrderId,
			        'productDetails' 	=> $productDetails,
			        'additionalParam' 	=> $additionalParam,
			        'email' 			=> $email,
	    			'phoneNumber' 		=> $phoneNumber,
			        'callbackUrl' 		=> $callbackUrl,
			        'returnUrl' 		=> $returnUrl,
			        'signature' 		=> $signature
			    );

				$api_request = $this->curl->post_custom($this->duitku_url, "", $req_params, '', FALSE);
				if($api_request['rest_no'] == "0"){
					$api_request = json_decode($api_request['result'], 1);
				}

				if ( ! empty($api_request['reference']) && ! empty($api_request['paymentUrl']))
				{
					// Updating RESPONSE data
		        	redirect($api_request['paymentUrl']);
				}
				else
				{
					$this->mgeneral->update(array("id" => $order_id), array("status" => "cancel"), "payment_order");
		        	$data['reason'] = "Terdapat Kesalahan dari Request API" . $api_request['statusMessage'];
					$this->load->view("payment/error", $data);
				}
			}
		}
		else{
			$data['reason'] = $get_product['reason'];
			$this->load->view("payment/error", $data);
		}
	}

	private function order_train($product, $transaction_id, $amount, $payment_type, $partner_id){
		$param_get_product	= array("api_key" 	=> $this->api_key,
									"format"	=> "",
									"ws"		=> "report",
									"call"		=> "train_detail",
									"book_id"	=> $transaction_id);
		$get_product = $this->curl->post_custom($this->api_url, "", $param_get_product);
		if($get_product['rest_no'] == "0"){
			$get_product = json_decode($get_product['result'], true);
		}

		if($get_product['rest_no'] == "0"){
			$amount = $get_product['data']['total_fares'];
			if($get_product['data']['return']):
				$kdBook		= $get_product['data']['depart']['book_details']['book_code']." dan ".$get_product['data']['return']['book_details']['book_code'];
			else:
				$kdBook		= $get_product['data']['depart']['book_details']['book_code'];
			endif;			
			$message		= "Pembelian tiket kereta kode booking ".$kdBook;

			if($payment_type == "TF"){
				
				$rand_no = $this->getUnikNo();
				$amount = substr($amount, 0, -3).$rand_no;
			
				$order_id = $this->save_order($product, $transaction_id, $amount, $payment_type, $partner_id, $message);
				$this->mgeneral->update(array("id" => $order_id), array("amount" => $amount, "unique_fare" => $rand_no), "payment_order");
				$status = true;
									
				if($status == true){
					$data["amount"] = $amount;
					$bank = $this->mgeneral->getWhere(array('status'=>"on"),"sys_bank_akun");
					$data['bank']	= $bank;
					$this->load->view("payment/transfer", $data);	
				}
				else{
					$this->mgeneral->update(array("id" => $order_id), array("status" => "cancel"), "payment_order");
					$data["reason"] = "terjadi gangguan pada sistem payment";
					$this->load->view("payment/error", $data);	
				}
			}
			else if($payment_type == "deposit"){
				$order_id = $this->save_order($product, $transaction_id, $amount, $payment_type, $partner_id, $message);
				$actionPayment	= cek_deposit_dan_bayar($this->converter->decode($partner_id), $amount, $transaction_id, $message);
				if($actionPayment["error_no"] == "0"){
					$this->mgeneral->update(array("id" => $order_id), array("status" => "used"), "payment_order");

					$param_get_product	= array("api_key" 	=> $this->api_key,
												"format"	=> "",
												"ws"		=> "train",
												"call"		=> "ws_payment_with_gateway",
												"book_id"	=> $transaction_id);
					$this->curl->post_custom($this->api_url, "", $param_get_product);

					$data['message'] = "Payment berhasil, silahkan cek data transaksi";
					$this->load->view("payment/result", $data);
				}
				else{
					$this->mgeneral->update(array("id" => $order_id), array("status" => "cancel"), "payment_order");
					$data['reason'] = $actionPayment['error_msg'];
					$this->load->view("payment/error", $data);
				}
			}
			else{
				$order_id = $this->save_order($product, $transaction_id, $amount, $payment_type, $partner_id, $message);
				$req_url = $this->data['config']['payment']['duitku']['url'];

				$merchantCode 		= $this->duitku_merchant_code;
			    $merchantKey 		= $this->duitku_api_key;
			    $paymentAmount 		= $amount;
			    $paymentMethod 		= $payment_type; // WW = duitku wallet, VC = Credit Card, MY = Mandiri Clickpay, BK = BCA KlikPay
			    $merchantOrderId 	= $order_id; // from merchant, unique
			    $productDetails 	= $message;
			    $email 				= $get_product['data']['customer']['customer_email']; // your customer email
			    $phoneNumber 		= $get_product['data']['customer']['customer_phone']; // your customer phone number (optional)
			    $additionalParam 	= $partner_id; // optional
			    $callbackUrl 		= $this->url . 'payment/callback?from=duitku'; // url for callback
			    $returnUrl 			= $this->url . 'payment/result'; // url for redirect

			    $signature = md5($merchantCode . $merchantOrderId . $paymentAmount . $merchantKey);

			    $req_params = array(
			        'merchantCode' 		=> $merchantCode,
			        'paymentAmount' 	=> $paymentAmount,
			        'paymentMethod' 	=> $paymentMethod,
			        'merchantOrderId' 	=> $merchantOrderId,
			        'productDetails' 	=> $productDetails,
			        'additionalParam' 	=> $additionalParam,
			        'email' 			=> $email,
	    			'phoneNumber' 		=> $phoneNumber,
			        'callbackUrl' 		=> $callbackUrl,
			        'returnUrl' 		=> $returnUrl,
			        'signature' 		=> $signature
			    );
			    
				$api_request = $this->curl->post_custom($this->duitku_url, "", $req_params, '', FALSE);
				if($api_request['rest_no'] == "0"){
					$api_request = json_decode($api_request['result'], 1);
				}
				
				if ( ! empty($api_request['reference']) && ! empty($api_request['paymentUrl']))
				{
					// Updating RESPONSE data
		        	redirect($api_request['paymentUrl']);
				}
				else
				{
		        	$this->mgeneral->update(array("id" => $order_id), array("status" => "cancel"), "payment_order");
		        	$data['reason'] = "Terdapat Kesalahan dari Request API" . $api_request['statusMessage'];
					$this->load->view("payment/error", $data);
				}
			}
		}
		else{
			$data['reason'] = $get_product['reason'];
			$this->load->view("payment/error", $data);
		}
	}

	private function order_topup($product, $transaction_id, $amount, $payment_type, $partner_id){
		
		$message		= "Topup request";
		$rand_no = $this->getUnikNo();
		$order_id = $this->save_order($product, $payment_type, $amount, $payment_type, $partner_id, $message);

		$param_get_product	= array("api_key" 		=> $this->api_key,
									"format"		=> "",
									"ws"			=> "deposit",
									"call"			=> "topup_request",
									"payment_code"	=> $payment_type,
									"transfer_date"	=> date("Y-m-d"),
									"partner_id"	=> $partner_id,
									"amount"		=> $amount);
		$get_product = $this->curl->post_custom($this->api_url, "", $param_get_product);
		if($get_product['rest_no'] == "0"){
			$get_product = json_decode($get_product['result'], true);
		}
		
		if($get_product['rest_no'] == "0"){
			$this->mgeneral->update(array("id" => $order_id), array("transaction_id" => $get_product['data']['topup_id']), "payment_order");
			if($payment_type == "TF"){
				
				$amount = substr($amount, 0, -3).$rand_no;
				
				$this->mgeneral->update(array("id" => $order_id), array("amount" => $amount, "unique_fare" => $rand_no), "payment_order");
				$this->mgeneral->update(array("dt_topup_id" => $this->converter->decode($get_product['data']['topup_id'])), array("transfer_amount" => $amount), "deposit_topup");
				$status = true;
									
				if($status == true){
					$data["amount"] = $amount;
					$bank = $this->mgeneral->getWhere(array('status'=>"on"),"sys_bank_akun");
					$data['bank']	= $bank;
					$this->load->view("payment/transfer", $data);	
				}
				else{
					$this->mgeneral->update(array("id" => $order_id), array("status" => "cancel"), "payment_order");
					$data["reason"] = "terjadi gangguan pada sistem payment";
					$this->load->view("payment/error", $data);	
				}
			}
			else{
				$partner_data = $this->mgeneral->getWhere(array("partner_id" => $this->converter->decode($partner_id)), "user_partner");

				$req_url = $this->data['config']['payment']['duitku']['url'];

				$merchantCode 		= $this->duitku_merchant_code;
			    $merchantKey 		= $this->duitku_api_key;
			    $paymentAmount 		= $amount;
			    $paymentMethod 		= $payment_type; // WW = duitku wallet, VC = Credit Card, MY = Mandiri Clickpay, BK = BCA KlikPay
			    $merchantOrderId 	= $order_id; // $this->converter->encode($order_id)from merchant, unique
			    $productDetails 	= $message;
			    $email 				= $partner_data[0]->email; // your customer email
			    $phoneNumber 		= $partner_data[0]->mobile_number; // your customer phone number (optional)
			    $additionalParam 	= $partner_id; // optional
			    $callbackUrl 		= $this->url . 'payment/callback?from=duitku'; // url for callback
			    $returnUrl 			= $this->url . 'payment/result'; // url for redirect

			    $signature = md5($merchantCode . $merchantOrderId . $paymentAmount . $merchantKey);

			    $req_params = array(
			        'merchantCode' 		=> $merchantCode,
			        'paymentAmount' 	=> $paymentAmount,
			        'paymentMethod' 	=> $paymentMethod,
			        'merchantOrderId' 	=> $merchantOrderId,
			        'productDetails' 	=> $productDetails,
			        'additionalParam' 	=> $additionalParam,
			        'email' 			=> $email,
	    			'phoneNumber' 		=> $phoneNumber,
			        'callbackUrl' 		=> $callbackUrl,
			        'returnUrl' 		=> $returnUrl,
			        'signature' 		=> $signature
			    );
				
				$api_request = $this->curl->post_custom($this->duitku_url, "", $req_params);
				
				if($api_request['rest_no'] == "0"){
					$api_request = json_decode($api_request['result'], 1);
				}
				
				if ( ! empty($api_request['reference']) && ! empty($api_request['paymentUrl']))
				{
					// Updating RESPONSE data
		        	redirect($api_request['paymentUrl']);
				}
				else
				{
					$data['reason'] = "Terdapat Kesalahan dari Request API" . $api_request['statusMessage'];
					$this->load->view("payment/error", $data);
				}
			}
		}
		else{
			$data['reason'] = $get_product['reason'];
			$this->load->view("payment/error", $data);
		}
	}

	private function order_ecommerce($product, $transaction_id, $amount, $payment_type, $partner_id){
		$partner_data = $this->mgeneral->getWhere(array("partner_id" => $this->converter->decode($partner_id)), "user_partner");

		$data_ecommerce = $this->read_order($transaction_id);

		if($data_ecommerce['rest_no'] == '0'){
			
			$statusTrx = $data_ecommerce['status'];
			
			if($data_ecommerce['rest_no'] == 0 && $statusTrx != 'paid'){
				$amount = $data_ecommerce['total'];
				$message		= "Pembelian toko invoice ".$data_ecommerce['invoice'];
				
				$rand_no = $this->getUnikNo();
				$order_id = $this->save_order($product, $transaction_id, $amount, $payment_type, $partner_id, $message, $data_ecommerce);

				if($payment_type == "TF"){
					$formatAmount = $this->format_amount_unik($rand_no,$amount);	
					$amount = $formatAmount['amount_akhir'];
					$this->mgeneral->update(array("id" => $order_id), array("amount" => $amount, "unique_fare" => $rand_no,'selisih_amount'=>$formatAmount['selisih']), "payment_order");
					$status = true;
											
					if($status == true){
						$data["amount"] = $amount;
						$data['selisih']= $formatAmount['selisih'];
						$bank = $this->mgeneral->getWhere(array('status'=>"on"),"sys_bank_akun");
						$data['bank']	= $bank;
						$this->load->view("payment/transfer", $data);	
					}
					else{
						$this->mgeneral->update(array("id" => $order_id), array("status" => "cancel"), "payment_order");
						$data["reason"] = "terjadi gangguan pada sistem payment";
						$this->load->view("payment/error", $data);	
					}
				}
				else if($payment_type == "deposit"){
					$actionPayment	= cek_deposit_dan_bayar($this->converter->decode($partner_id), $amount, $transaction_id, $message);
					if($actionPayment["error_no"] == "0"){
						$this->mgeneral->update(array("id" => $order_id), array("status" => "used"), "payment_order");
						$resultHit = $this->update_order($transaction_id,$payment_type,'paid');
						$data['message'] = "Payment berhasil, silahkan cek data transaksi";
						$this->load->view("payment/result", $data);
					}
					else{
						$this->mgeneral->update(array("id" => $order_id), array("status" => "cancel"), "payment_order");
						$data['reason'] = $actionPayment['error_msg'];
						$this->load->view("payment/error", $data);
					}
				}
				else{
					$req_url = $this->data['config']['payment']['duitku']['url'];

					$merchantCode 		= $this->duitku_merchant_code;
				    $merchantKey 		= $this->duitku_api_key;
				    $paymentAmount 		= $amount;
				    $paymentMethod 		= $payment_type; // WW = duitku wallet, VC = Credit Card, MY = Mandiri Clickpay, BK = BCA KlikPay
				    $merchantOrderId 	= $order_id; // $this->converter->encode($order_id) from merchant, unique
				    $productDetails 	= $message;
				    $email 				= $get_product['data']['customer']['customer_email']; // your customer email
				    $phoneNumber 		= $get_product['data']['customer']['customer_phone']; // your customer phone number (optional)
				    $additionalParam 	= $partner_id; // optional
				    $callbackUrl 		= $this->url . 'payment/callback?from=duitku'; // url for callback
				    $returnUrl 			= $this->url . 'payment/result'; // url for redirect

				    $signature = md5($merchantCode . $merchantOrderId . $paymentAmount . $merchantKey);

				    $req_params = array(
				        'merchantCode' 		=> $merchantCode,
				        'paymentAmount' 	=> $paymentAmount,
				        'paymentMethod' 	=> $paymentMethod,
				        'merchantOrderId' 	=> $merchantOrderId,
				        'productDetails' 	=> $productDetails,
				        'additionalParam' 	=> $additionalParam,
				        'email' 			=> $email,
		    			'phoneNumber' 		=> $phoneNumber,
				        'callbackUrl' 		=> $callbackUrl,
				        'returnUrl' 		=> $returnUrl,
				        'signature' 		=> $signature
				    );

					$api_request = $this->curl->post_custom($this->duitku_url, "", $req_params);
					if($api_request['rest_no'] == "0"){
						$api_request = json_decode($api_request['result'], 1);
					}

					if ( ! empty($api_request['reference']) && ! empty($api_request['paymentUrl']))
					{
						// Updating RESPONSE data
			        	redirect($api_request['paymentUrl']);
					}
					else
					{
			        	$data['reason'] = "Terdapat Kesalahan dari Request API" . $api_request['statusMessage'];
						$this->load->view("payment/error", $data);
					}
				}
			}
			else{
				$data['reason'] = "gagal mendapatkan data atau sudah dibayar";
				$this->load->view("payment/error", $data);
			}
		}
		else{
			$data['reason'] = "gagal mendapatkan data";
			$this->load->view("payment/error", $data);
		}
	}
	
	private function save_order($product, $transaction_id, $amount, $payment_type, $partner_id, $message, $transaction_data = NULL){
		$arr_save 	= array("product" 			=> $product,
							"transaction_id"	=> $transaction_id,
							"amount"			=> $amount,
							"payment_type"		=> $payment_type,
							"partner_id"		=> $this->converter->decode($partner_id),
							"message"			=> $message,
							"transaction_data"	=> json_encode($transaction_data));
		$id = $this->mgeneral->save($arr_save, "payment_order");

		return $id;
	}

	public function callback_micropay(){
		
		$auth = base64_encode('12004:'.hash('sha256', '123456'));
		$header = array('Authorization: Basic '.$auth, 'Content-Type: application/x-www-form-urlencoded');
		$param['grant_type'] = "12004";
		$param['scope'] = "Write";
		$result = $this->curl->post_micropay('https://103.28.56.98:8081/ech/dev/auth/token',$header, $param);
		$dataToken = json_decode($result['result'],true);

		$param2['merchant_id'] = "12004";
		$param2['trx_date'] = date('Y-m-d H:i:s');
		$param2['payment_code'] = "123";
		$param2['amount'] = "100000";
		$param2['description'] = "Pembayaran invoice 123";
		$param2['expired_date'] = date('Y-m-d');
		$param2['customer_institution_code'] = "";
		$param2['customer_phone_number'] = "";
		$header2 = array('Authorization: '.$dataToken['token_type'].' '.$dataToken['access_token'],'Content-Type: application/x-www-form-urlencoded');
		$result2 = $this->curl->post_micropay('https://103.28.56.98:8081/ech/dev/openPayment/addBill',$header2, $param2);
		$dataBill = json_decode($result2['result'],true);
		
		if($dataBill['response_code'] = "00"):
			echo "Transaksi Berhasil";
		else:
			echo "Trransaksi gagal #".$dataBill['response_message'];
		endif;
	}

	public function callback(){
		
		#save log respon from duitku
		$postData	= json_encode($this->input->post());
		$varSave	= array('action'	=> "payment",
							'post'		=> $postData,
							'result'	=> $_GET['from']);
		$logId = $this->mgeneral->save($varSave,"log_payment");
		
		if($_GET['from'] == "duitku"){
			
			$apiKey = $this->duitku_api_key;
			$merchantCode = isset($_POST['merchantCode']) ? $_POST['merchantCode'] : null; 
			$amount = isset($_POST['amount']) ? $_POST['amount'] : null; 
			$merchantOrderId = isset($_POST['merchantOrderId']) ? $_POST['merchantOrderId'] : null; 
			$productDetail = isset($_POST['productDetail']) ? $_POST['productDetail'] : null; 
			$additionalParam = isset($_POST['additionalParam']) ? $_POST['additionalParam'] : null; 
			$paymentMethod = isset($_POST['paymentCode']) ? $_POST['paymentCode'] : null; 
			$resultCode = isset($_POST['resultCode']) ? $_POST['resultCode'] : null; 
			$merchantUserId = isset($_POST['merchantUserId']) ? $_POST['merchantUserId'] : null; 
			$reference = isset($_POST['reference']) ? $_POST['reference'] : null; 
			$signature = isset($_POST['signature']) ? $_POST['signature'] : null;
			
			if ( ! empty($merchantCode) && ! empty($amount) && ! empty($merchantOrderId) && ! empty($signature))
			{
			    $params = $merchantCode . $amount . $merchantOrderId . $apiKey;
			    $calcSignature = md5($params);

			    if ($signature == $calcSignature)
			    {
			    	$order_id = $merchantOrderId;
			    }
			}
			
		}else if($_GET['from'] == "micropay"){

			$merchant_id = isset($_POST['merchant_id']) ? $_POST['merchant_id'] : null; 
			$payment_date = isset($_POST['payment_date']) ? $_POST['payment_date'] : null;
			$local_payment_date = isset($_POST['local_payment_date']) ? $_POST['local_payment_date'] : null;
			$payment_code = isset($_POST['payment_code']) ? $_POST['payment_code'] : null;
			$bill_amount = isset($_POST['bill_amount']) ? $_POST['bill_amount'] : null;
			$institution_code = isset($_POST['institution_code']) ? $_POST['institution_code'] : null;
			$customer_phone_number = isset($_POST['customer_phone_number']) ? $_POST['customer_phone_number'] : null;

			$result = array('response_code' => '0000', 'response_message'=> 'Transaction Success!' );
			echo json_encode($result);

		}else if($_GET['from'] == "cron"){
			$order_id = $this->converter->decode($_POST['order_id']);
		}

		if(isset($order_id)){
			$get_data = $this->mgeneral->getWhere(array("id" => $order_id, "status" => "free"), "payment_order");

			if(count($get_data) > 0){
				$this->mgeneral->update(array("id" => $order_id), array("status" => "used"), "payment_order");
				if($get_data[0]->product == "flight"){
					$param_payment	= array("api_key" 	=> $this->api_key,
											"format"	=> "",
											"ws"		=> "flight",
											"call"		=> "ws_payment_with_gateway",
											"book_id"	=> $get_data[0]->transaction_id,
											'reference'	=> $reference);
					$resultHit = $this->curl->post_custom($this->api_url, "", $param_payment);
				}
				else if($get_data[0]->product == "train"){
					$param_payment	= array("api_key" 	=> $this->api_key,
											"format"	=> "",
											"ws"		=> "train",
											"call"		=> "ws_payment_with_gateway",
											"book_id"	=> $get_data[0]->transaction_id,
											'reference'	=> $reference);
					$resultHit = $this->curl->post_custom($this->api_url, "", $param_payment);
				}
				else if($get_data[0]->product == "ecommerce"){
					
					$resultHit = $this->update_order($get_data[0]->transaction_id,$get_data[0]->payment_type,'paid');
				}
				else{
					$param_payment	= array("api_key" 	=> $this->api_key,
											"format"	=> "",
											"ws"		=> "deposit",
											"call"		=> "confirm_deposit",
											"topup_id"	=> $get_data[0]->transaction_id,
											'reference'	=> $reference);
					$resultHit = $this->curl->post_custom($this->api_url, "", $param_payment);
				}
				
				$varSave	= array('result'	=> json_encode($resultHit));
				$this->mgeneral->update(array('log_payment_id'=>$logId),$varSave,"log_payment");
			}
			else{
				$data['reason'] = "gagal mendapatkan data";
				$this->load->view("payment/error", $data);
			}
		}
	}

	public function endtrx()
	{
		$this->load->view("payment/end");
	}
	
	public function result(){
		$data['message'] = "Payment berhasil, silahkan cek data transaksi";
		$this->load->view("payment/result", $data);
	}
	
	public function tes(){
		$data['message'] = "<p>silahkan cek data transaksi</p>";
		$this->load->view("payment/result", $data);
	}
	
	public function format_amount_unik($unik_number,$amount)
	{
		$DigitAkhir	= substr($amount,-3,3);
		$DigitAwal 	= substr($amount,0,-3);
		$fUnikNo	= "1".$unik_number;
		$selisih	= $fUnikNo-$DigitAkhir;
		$amountakhir= ($DigitAwal+1)."".$unik_number;
		 
		$dataBalikan = array('amount_akhir'	=> $amountakhir,
							 'selisih'		=> $selisih);
		
		return $dataBalikan;
	}

	private function read_order($id)
	{
		$sql = "SELECT `mall_order`.*, SHA1(CONCAT(`mall_order`.`id`, 'rMP')) AS `id`, SHA1(CONCAT(`mall_order`.`user_id`, 'rMP')) AS `user_id`, (SELECT SUM(`mall_transaction_item`.`price`) FROM `mall_transaction_item` WHERE `mall_transaction_item`.`transaction_id` = (SELECT `mall_transaction`.`id` FROM `mall_transaction` WHERE `mall_transaction`.`order_id` = `mall_order`.`id` LIMIT 1)) AS `total_price`,
					(SELECT SUM(`mall_transaction_item`.`discount`) FROM `mall_transaction_item` WHERE `mall_transaction_item`.`transaction_id` = (SELECT `mall_transaction`.`id` FROM `mall_transaction` WHERE `mall_transaction`.`order_id` = `mall_order`.`id` LIMIT 1)) AS `total_discount`,
					(SELECT SUM(`mall_transaction_item`.`quantity`) FROM `mall_transaction_item` WHERE `mall_transaction_item`.`transaction_id` = (SELECT `mall_transaction`.`id` FROM `mall_transaction` WHERE `mall_transaction`.`order_id` = `mall_order`.`id` LIMIT 1)) AS `total_quantity`,
					(SELECT SUM(`mall_transaction`.`shipping_cost`) FROM `mall_transaction` WHERE `mall_transaction`.`order_id` = `mall_order`.`id`) AS `total_shipping_cost`
					FROM `mall_order` where SHA1(CONCAT(`mall_order`.`id`, 'rMP')) = '".$id."'";
		$data = $this->db->query($sql)->result();

		if(count($data)!=0):
			$total = ($data[0]->total_price-$data[0]->total_discount)+$data[0]->total_shipping_cost;
			$result = array('rest_no'=>0,"invoice"=>$data[0]->invoice_code,'total'=>$total,'status'=>$data[0]->payment_status);
		else:
			$result = array('rest_no'=>"500","reason"=>"transaksi tidak ditemukan");
		endif;

		return $result;
	}

	private function update_order($id,$payment_code,$payment_status)
	{
		$sql = "SELECT `mall_order`.*, `mall_transaction_item`.quantity, `mall_transaction_item`.product_data, `mall_transaction_item`.variant_id
				FROM `mall_order`
				LEFT JOIN `mall_transaction` ON `mall_order`.id = `mall_transaction`.order_id
				LEFT JOIN `mall_transaction_item` ON `mall_transaction`.id = `mall_transaction_item`.transaction_id
				where SHA1(CONCAT(`mall_order`.`id`, 'rMP')) = '".$id."'";
		$trx_data = $this->db->query($sql)->result();

		if(count($trx_data)!=0):

			if($trx_data[0]->payment_status!="paid"):

				$sql1 = "UPDATE `mall_order` SET `payment_status` = '".$payment_status."', `payment_code` = '".$payment_code."' where SHA1(CONCAT(`mall_order`.`id`, 'rMP')) = '".$id."'";
				$this->db->query($sql1);
				//echo $sql1."<br>";

				foreach($trx_data as $trx):
					$product_data = json_decode($trx->product_data,true);
					$sql2 = "UPDATE `mall_product` SET `product_stock` = (`product_stock`-".$trx->quantity.") WHERE SHA1(CONCAT(`mall_product`.`id`, 'rMP')) = '".$product_data['id']."'";
					$this->db->query($sql2);
					//echo $sql2."<br>";

					if($trx->variant_id!=""):
						$sql3 = "UPDATE `mall_product_variant` SET `stock` = (`stock`-".$trx->quantity.") WHERE SHA1(CONCAT(`mall_product_variant`.`id`, 'rMP')) = '".$trx->variant_id."'";
						$this->db->query($sql3);
						//echo $sql3."<br>";
					endif;
					
				endforeach;
			endif;

		endif;

		return array('rest_no'=>0);
	}
	
	private function getUnikNo()
	{
		$lastNumber = $this->db->query("select * from payment_order where payment_type ='TF' AND date LIKE '".date('Y-m-d')."%' AND unique_fare IS NOT NULL order by date desc limit 1")->result_array(); 
		
		if(count($lastNumber)==0):
			$unikNo = "1";
		else:
			$unikNo = $lastNumber[0]['unique_fare']+1;
		endif;
		
		if($unikNo < 1000):
			if(strlen($unikNo)!=3):
				$lengtKurang = 3-strlen($unikNo);
				$fUnik = "";
				for($a=0;$a<$lengtKurang;$a++):
					$fUnik.="0";
				endfor;
				$fUnik .= $unikNo;
				$unikNo = $fUnik; 
			endif;
		else:
			$unikNo = "001";
		endif;
		
		return $unikNo;
	}
}