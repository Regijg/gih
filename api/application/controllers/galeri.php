<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Galeri extends CI_Controller
{        
	function img()
	{
		$controler	= "galeri/img";
		$uri		= str_replace($controler."/","",uri_string());
		$subdomain	= explode("/",$uri);
		
		if(uri_string()!=$controler):
			#$imagePath = "http://".$subdomain[0].".expedia.com".str_replace($subdomain[0],"",$uri);
			$imagePath = "http://images.trvl-media.com".str_replace($subdomain[0],"",$uri);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $imagePath); 
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$result = curl_exec($ch);
			curl_close($ch);
			
			header("Content-type: image/jpeg");
			echo $result;
		endif;
	}
	
	function cgvblitz($img){
		$imagePath = "https://www.cgvblitz.com/en/assets/images/cover/".$img;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $imagePath); 
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		$result = curl_exec($ch);
		curl_close($ch);
		
		header("Content-type: image/jpeg");
		echo $result;
	}

	function xxicineplex($img){
		$imagePath = "https://mtix.21cineplex.com/images/movie/".$img;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $imagePath); 
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		$result = curl_exec($ch);
		curl_close($ch);
		
		header("Content-type: image/jpeg");
		echo $result;
	}
}
