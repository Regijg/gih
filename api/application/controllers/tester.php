<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tester extends CI_Controller {

	
	public function testAT() {
		$post_data['akses_kode']   = "WEBAT2";
	   	$post_data['app']     = "hotel_v5";
	    $post_data['action']  = "hotel_search";
	    $post_data['city'] = "Jakarta";
	    $post_data['ci'] = "2019-03-27";
	    $post_data['co'] = "2019-03-28";
	    $post_data['room'] = "1";
	    $post_data['adult'] = "1";
	    $post_data['child'] = "0";
	    $post_data['hotel_name'] = "";
	    $url = "https://agent.aeroticket.com/service/v2";
	    
	    foreach ($post_data as $key => $value) {
	        $post_items[] = $key . "=" . $value;
	    }
	    $post_string = implode("&", $post_items);

	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, $url);
	    curl_setopt($ch, CURLOPT_HEADER, false);
	    curl_setopt($ch, CURLOPT_POST, true);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
	    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, '100');
	    curl_setopt($ch, CURLOPT_USERAGENT, "AT2-JSN");
	    curl_setopt($ch, CURLOPT_TIMEOUT, 600);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	    $result_post = curl_exec($ch);
		$result = json_decode($result_post);
		
	    print_r($result);
	}

	public function ws() {
		$this->load->view('tester/api', $data);
	}

	public function encode($key) {
		echo $this->converter->encode($key);
	}

	public function decode($key) {
		echo $this->converter->decode($key);
	}
	
	public function class_logreg($action) {
		
		$data['ws']		= "logreg";
		$data['call'] 	= $action;
		
		switch ($action) {
			case 'register_process':
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'logreg');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => "first_name", 'val' => "");
				$data['field'][] = array('name' => "last_name", 'val' => "");
				$data['field'][] = array('name' => "phone_number", 'val' => "");
				$data['field'][] = array('name' => "mobile_number", 'val' => "");
				$data['field'][] = array('name' => "address", 'val' => "");
				$data['field'][] = array('name' => "country", 'val' => "");
				$data['field'][] = array('name' => "post_code", 'val' => "");
				$data['field'][] = array('name' => "business_name", 'val' => "");
				$data['field'][] = array('name' => "business_address", 'val' => "");
				$data['field'][] = array('name' => "business_scheme", 'val' => "",'msg'=>"business_scheme_id");
				$data['field'][] = array('name' => "email", 'val' => "");
				$data['field'][] = array('name' => "password", 'val' => "");
				$data['field'][] = array('name' => "password_confirm", 'val' => "");
				$data['field'][] = array('name' => "branch_id", 'val' => "", "msg"=>"di front end di hidden");
				break;
			case 'login_process':
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'logreg');
				$data['field'][] = array('name' => 'call','val'=>$action);
				$data['field'][] = array('name' => 'email', 'val' =>'');
				$data['field'][] = array('name' => 'password', 'val' =>'');
				break;
			case 'forgot_password':
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'logreg');
				$data['field'][] = array('name' => 'call','val'=>$action);
				$data['field'][] = array('name' => 'email', 'val' =>'');
				break;
			case 'profile_update':
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'logreg');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => 'partner_id', 'val' =>'');
				$data['field'][] = array('name' => "first_name", 'val' => "");
				$data['field'][] = array('name' => "last_name", 'val' => "");
				$data['field'][] = array('name' => "phone_number", 'val' => "");
				$data['field'][] = array('name' => "mobile_number", 'val' => "");
				$data['field'][] = array('name' => "address", 'val' => "");
				$data['field'][] = array('name' => "country", 'val' => "");
				$data['field'][] = array('name' => "post_code", 'val' => "");
				$data['field'][] = array('name' => "business_name", 'val' => "");
				$data['field'][] = array('name' => "business_address", 'val' => "");
				break;
			case 'password_change':
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'logreg');
				$data['field'][] = array('name' => 'call','val'=>$action);
				$data['field'][] = array('name' => 'partner_id', 'val' =>'');
				$data['field'][] = array('name' => 'old_password', 'val' =>'');
				$data['field'][] = array('name' => 'new_password', 'val' =>'');
				$data['field'][] = array('name' => 'new_password_confirm', 'val' =>'');
				break;
			case 'business_scheme':
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'logreg');
				$data['field'][] = array('name' => 'call','val'=>$action);
				break;
			case 'add_corporate_user':
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'logreg');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => "first_name", 'val' => "");
				$data['field'][] = array('name' => "last_name", 'val' => "");
				$data['field'][] = array('name' => "phone_number", 'val' => "");
				$data['field'][] = array('name' => "mobile_number", 'val' => "");
				$data['field'][] = array('name' => "address", 'val' => "");
				$data['field'][] = array('name' => "country", 'val' => "");
				$data['field'][] = array('name' => "post_code", 'val' => "");
				$data['field'][] = array('name' => "business_name", 'val' => "");
				$data['field'][] = array('name' => "business_address", 'val' => "");
				$data['field'][] = array('name' => "email", 'val' => "");
				$data['field'][] = array('name' => "password", 'val' => "");
				$data['field'][] = array('name' => "password_confirm", 'val' => "");
				$data['field'][] = array('name' => "partner_level", 'val' => "", "msg" => "user_issue, user_book");
				$data['field'][] = array('name' => 'partner_id', 'val' =>'');
				$data['field'][] = array('name' => 'corporate_id', 'val' =>'');
				break;
			case 'confirm_registration':
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'logreg');
				$data['field'][] = array('name' => 'call','val'=>$action);
				$data['field'][] = array('name' => 'partner_id', 'val' =>'');
				break;
			case 'get_branch':
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'logreg');
				$data['field'][] = array('name' => 'call','val'=>$action);
				$data['field'][] = array('name' => 'branch_id', 'val' =>'');
				break;
			case 'send_referal':
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'logreg');
				$data['field'][] = array('name' => 'call','val'=>$action);
				$data['field'][] = array('name' => 'branch_id', 'val' =>'');
				$data['field'][] = array('name' => "email", 'val' => "");
				break;
			case 'update_status_corporate_user':
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'logreg');
				$data['field'][] = array('name' => 'call','val'=>$action);
				$data['field'][] = array('name' => 'partner_id', 'val' =>'');
				$data['field'][] = array('name' => 'status', 'val' =>'', "msg" => "aktif, tidak_aktif");
				break;
		}
		$this->load->view('tester/form_api', $data);
	}
	
	public function class_flight($action) {
		$data['ws'] = 'flight';
		$data['call'] = $action;
		
		switch ($action) {
			case 'ws_getairlines':
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'flight');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				break;
			case 'ws_getairport':
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'flight');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => 'country', 'val' => 'INDONESIA');
				break;
			case 'ws_search':
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'flight');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => 'airline_id', 'val' => '');
				$data['field'][] = array('name' => 'dep', 'val' => '', 'msg' => 'departure airport, contoh: CGK [airport_code]');
				$data['field'][] = array('name' => 'arr', 'val' => '', 'msg' => 'arrival airport, contoh: DPS [airport_code]');
				$data['field'][] = array('name' => 'date', 'val' => date('Y-m-d'));
				$data['field'][] = array('name' => 'adult', 'val' => '1');
				$data['field'][] = array('name' => 'child', 'val' => '0');
				$data['field'][] = array('name' => 'infant', 'val' => '0');
				break;
			case 'ws_getallclass':
                $data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'flight');
                $data['field'][] = array('name' => 'call', 'val' => $action);
                $data['field'][] = array('name' => 'scReferenceId', 'val' => '');
                break;
			case 'ws_faredetail':
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'flight');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => 'scReferenceId', 'val' => '');
				$data['field'][] = array('name' => 'fno', 'val' => '');
				$data['field'][] = array('name' => 'class_id', 'val' => '');
				$data['field'][] = array('name' => 'scReferenceId_2', 'val' => '');
				$data['field'][] = array('name' => 'fno_2', 'val' => '');
				$data['field'][] = array('name' => 'class_id_2', 'val' => '');
				break;
			case 'ws_book':
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'flight');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => 'partner_id', 'val' => '');
				$data['field'][] = array('name' => 'fareReferenceId', 'val' => '');
				$data['field'][] = array('name' => 'customer_name', 'val' => '');
				$data['field'][] = array('name' => 'customer_phone', 'val' => '');
				$data['field'][] = array('name' => 'customer_email', 'val' => '');
				$data['field'][] = array('name' => 'margin', 'val' => '');
				$data['field'][] = array('name' => 'favorite', 'val' => '','msg'=>"true/false");
				for ($i=1; $i <= 4; $i++) {
					$data['field'][] = array('name' => 'pax_type_'.$i, 'val' => '');
					$data['field'][] = array('name' => 'pax_title_'.$i, 'val' => '');
					$data['field'][] = array('name' => 'pax_first_name_'.$i, 'val' => '');
					$data['field'][] = array('name' => 'pax_last_name_'.$i, 'val' => '');
					$data['field'][] = array('name' => 'pax_id_no_'.$i, 'val' => '');
					$data['field'][] = array('name' => 'pax_birthdate_'.$i, 'val' => '');
					$data['field'][] = array('name' => 'pax_pasport_'.$i, 'val' => '');
					$data['field'][] = array('name' => 'pax_expired_pasport_'.$i, 'val' => '');
					$data['field'][] = array('name' => 'pax_country_pasport_'.$i, 'val' => '');
				}
				break;
			case 'ws_payment':
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'flight');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => 'book_id', 'val' => '');
				break;
			case 'ws_cekpayment':
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'flight');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => 'book_id', 'val' => '');
			break;
			case 'ws_getrealprice':
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'flight');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => 'scReferenceId', 'val' => '');
				$data['field'][] = array('name' => 'schedule_id', 'val' => '');
			break;
			case 'ws_getbalance':
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'flight');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => 'airline_id', 'val' => '');
			break;
			case "ws_getfavorite":
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'flight');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => 'partner_id', 'val' => '');
				$data['field'][] = array('name' => 'name', 'val' => '');
				break;
		}

		$this->load->view('tester/form_api', $data);
	}
	
	public function class_data($action) {
		$data['ws'] = "data";
		$data['call'] = $action;
		
		switch ($action) {
			case 'pdf_ticket':
			case 'html_ticket':
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'data');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => "product", 'val' => '', 'msg' => 'flight');
				$data['field'][] = array('name' => "type", 'val' => "ticket", 'msg' => 'ticket or invoice');
				$data['field'][] = array('name' => "book_id", 'val' => '');
				/*$data['field'][] = array('name' => "travel_name", 'val' => '');
				$data['field'][] = array('name' => "travel_addr", 'val' => '');
				$data['field'][] = array('name' => "travel_contact", 'val' => '');*/
				break;
			case 'email_ticket':
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'data');
				$data['field'][] = array('name' => "call", 'val' => $action);
				$data['field'][] = array('name' => "product", 'val' => '');
				$data['field'][] = array('name' => "type", 'val' => "ticket", 'msg' => 'ticket or invoice');
				$data['field'][] = array('name' => "book_id", 'val' => "", 'msg' => 'ex:YOk9ijycU8n6P9pTTczAy8Y6HmRFLUIQp5Z4mu6sn3U');
				$data['field'][] = array('name' => "email", 'val' => "", 'msg' => "separated with comma if email address more than one.");
				/*$data['field'][] = array('name' => "travel_name", 'val' => '');
				$data['field'][] = array('name' => "travel_addr", 'val' => '');
				$data['field'][] = array('name' => "travel_contact", 'val' => '');*/
				break;
			case 'slider':
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'data');
				$data['field'][] = array('name' => "call", 'val' => $action);
				break;
			case "news":
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'data');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => 'page', 'val' => '');
				$data['field'][] = array('name' => 'per_page', 'val' => '');
				break;
		}

		$this->load->view('tester/form_api', $data);
	}
	
	public function class_ppob($action) {
		$data['ws'] = "ppob";
		$data['call'] = $action;
		
		switch ($action) {
			case 'ws_produk':
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'ppob');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				break;
			case 'ws_inquiry':
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'ppob');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => 'produk_code', 'val' => '');
				$data['field'][] = array('name' => 'id_pelanggan', 'val' => '','msg'=>"no telp / id pelanggan");
				$data['field'][] = array('name' => 'partner_id', 'val' => '');
				break;
			case 'ws_payment':
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'ppob');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => 'pay_code', 'val' => '');
				$data['field'][] = array('name' => 'partner_id', 'val' => '');
				$data['field'][] = array('name' => 'favorite', 'val' => '', 'msg'=>"yes/no");
				break;
			case 'ws_struk':
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'ppob');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => 'pay_code', 'val' => '');
				break;
			case "ws_favorite":
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'ppob');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => 'partner_id', 'val' => '');
				$data['field'][] = array('name' => 'page', 'val' => '');
				$data['field'][] = array('name' => 'per_page', 'val' => '');
				break;
		}

		$this->load->view('tester/form_api', $data);
	}

	public function class_train($action) {
        $data['ws'] = 'train';
        $data['call'] = $action;
        $data['url'] = base_url('wstrain');

        switch ($action) {
            case 'ws_getstation':
                $data['field'][] = array('name' => 'api_key', 'val' => '');
                $data['field'][] = array('name' => 'format','val'=>'');
                $data['field'][] = array('name' => 'ws','val'=>'train');
                $data['field'][] = array('name' => 'call', 'val' => $action);
                $data['field'][] = array('name' => 'country', 'val' => 'INDONESIA');
                break;
            case 'ws_search':
                $data['field'][] = array('name' => 'api_key', 'val' => '');
                $data['field'][] = array('name' => 'format','val'=>'');
                $data['field'][] = array('name' => 'ws','val'=>'train');
                $data['field'][] = array('name' => 'call', 'val' => $action);
                $data['field'][] = array('name' => 'roundtrip', 'val' => "", 'msg' => 'oneway / return');
                $data['field'][] = array('name' => 'dep', 'val' => '', 'msg' => 'departure station, contoh: BD [station_code]');
                $data['field'][] = array('name' => 'arr', 'val' => '', 'msg' => 'arrival station, contoh: YK [station_code]');
                $data['field'][] = array('name' => 'depart_date', 'val' => date('Y-m-d'));
                $data['field'][] = array('name' => 'return_date', 'val' => date('Y-m-d'));
                $data['field'][] = array('name' => 'adult', 'val' => '1');
                #$data['field'][] = array('name' => 'child', 'val' => '0');
                $data['field'][] = array('name' => 'infant', 'val' => '0');
                break;
            case 'ws_faredetail':
                $data['field'][] = array('name' => 'api_key', 'val' => '');
                $data['field'][] = array('name' => 'format','val'=>'');
                $data['field'][] = array('name' => 'ws','val'=>'train');
                $data['field'][] = array('name' => 'call', 'val' => $action);
                $data['field'][] = array('name' => 'scReferenceId', 'val' => '');
                $data['field'][] = array('name' => 'schedule_id', 'val' => '');
                $data['field'][] = array('name' => 'sub_class', 'val' => '');
                $data['field'][] = array('name' => 'schedule_id2', 'val' => '');
                $data['field'][] = array('name' => 'sub_class2', 'val' => '');
                $data['field'][] = array('name' => 'partner_id', 'val' => '');
                break;
            case 'ws_book':
                $data['field'][] = array('name' => 'api_key', 'val' => '');
                $data['field'][] = array('name' => 'format','val'=>'');
                $data['field'][] = array('name' => 'ws','val'=>'train');
                $data['field'][] = array('name' => 'call', 'val' => $action);
                $data['field'][] = array('name' => 'fareReferenceId', 'val' => '');
                $data['field'][] = array('name' => 'partner_id', 'val' => '');
                $data['field'][] = array('name' => 'customer_name', 'val' => '');
                $data['field'][] = array('name' => 'customer_phone', 'val' => '');
                $data['field'][] = array('name' => 'customer_email', 'val' => '');
				$data['field'][] = array('name' => 'favorite', 'val' => '','msg'=>"true/false");
                for ($i=1; $i <= 3; $i++) {
                    $data['field'][] = array('name' => 'pax_type_'.$i, 'val' => '');
                    $data['field'][] = array('name' => 'pax_name_'.$i, 'val' => '');
                    $data['field'][] = array('name' => 'pax_id_no_'.$i, 'val' => '');
                    $data['field'][] = array('name' => 'pax_birthdate_'.$i, 'val' => '');
                    $data['field'][] = array('name' => 'pax_phone_'.$i, 'val' => '');
                }
                break;
            case 'ws_seatmap':
                $data['field'][] = array('name' => 'api_key', 'val' => '');
                $data['field'][] = array('name' => 'format','val'=>'');
                $data['field'][] = array('name' => 'ws','val'=>'train');
                $data['field'][] = array('name' => 'call', 'val' => $action);
                $data['field'][] = array('name' => 'book_id', 'val' => '');
                $data['field'][] = array('name' => 'type', 'val' => '', 'msg' => 'depart / return');
                break;
            case 'ws_manualseat':
                $data['field'][] = array('name' => 'api_key', 'val' => '');
                $data['field'][] = array('name' => 'format','val'=>'');
                $data['field'][] = array('name' => 'ws','val'=>'train');
                $data['field'][] = array('name' => 'call', 'val' => $action);
                $data['field'][] = array('name' => 'book_id', 'val' => '');
                $data['field'][] = array('name' => 'type', 'val' => '', 'msg' => 'depart / return');
                $data['field'][] = array('name' => 'wagon_code', 'val' => '');
                $data['field'][] = array('name' => 'wagon_no', 'val' => '');
                $data['field'][] = array('name' => 'seat', 'val' => '', 'msg' => 'separated with comma (,) if seats more than one.');
                break;
            case 'ws_payment':
                $data['field'][] = array('name' => 'api_key', 'val' => '');
                $data['field'][] = array('name' => 'format','val'=>'');
                $data['field'][] = array('name' => 'ws','val'=>'train');
                $data['field'][] = array('name' => 'call', 'val' => $action);
                $data['field'][] = array('name' => 'book_id', 'val' => '');
                break;
			
			case "ws_getfavorite":
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'train');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => 'partner_id', 'val' => '');
				$data['field'][] = array('name' => 'name', 'val' => '');
				break;
        }

        $this->load->view('tester/form_api', $data);
    }

    public function class_report($action) {
		$data['ws'] = "report";
		$data['call'] = $action;
	
		switch ($action) {
			case "train":
			case "flight":
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'report');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => "partner_id", 'val' => '');
				$data['field'][] = array('name' => "start_date", 'val' => '');
				$data['field'][] = array('name' => "end_date", 'val' => '');
				$data['field'][] = array('name' => "keyword", 'val' => '', "msg" => "tanggal booking atau kode booking");
				$data['field'][] = array('name' => 'page', 'val' => '');
				$data['field'][] = array('name' => 'per_page', 'val' => '');
				break;
			case "ppob":
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'report');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => "partner_id", 'val' => '');
				$data['field'][] = array('name' => "start_date", 'val' => '');
				$data['field'][] = array('name' => "end_date", 'val' => '');
				$data['field'][] = array('name' => "keyword", 'val' => '', "msg" => "id_pelanggan atau nama pelanggan");
				$data['field'][] = array('name' => 'page', 'val' => '');
				$data['field'][] = array('name' => 'per_page', 'val' => '');
				break;
			case "train_detail":
			case "flight_detail":
			case "ppob_detail":
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'report');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => "book_id", 'val' => '');
				break;
			case "deposit":
			case "transaction":
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'report');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => "partner_id", 'val' => '');
				$data['field'][] = array('name' => 'start_date', 'val' => '','msg'=>"Format : YYYY-MM-DD");
				$data['field'][] = array('name' => 'end_date', 'val' => '','msg'=>"Format : YYYY-MM-DD");
				break;
			case "get_corporate_user":
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'report');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => "partner_id", 'val' => '');
				$data['field'][] = array('name' => "corporate_id", 'val' => '');
				$data['field'][] = array('name' => 'page', 'val' => '');
				$data['field'][] = array('name' => 'per_page', 'val' => '');
				break;
			case "get_branch_member":
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'report');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => "partner_id", 'val' => '');
				$data['field'][] = array('name' => "branch_id", 'val' => '');
				$data['field'][] = array('name' => 'page', 'val' => '');
				$data['field'][] = array('name' => 'per_page', 'val' => '');
				break;
		}
		$this->load->view('tester/form_api', $data);
	}

	public function class_bank($action) {
		$data['ws'] = "bank";
		$data['call'] = $action;
	
		switch ($action) {
			case "bank_list":
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'bank');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => "partner_id", 'val' => "");
				break;
			case "bank_add":
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'bank');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => "partner_id", 'val' => "");
				$data['field'][] = array('name' => "bank_name", 'val' => "");
				$data['field'][] = array('name' => "account_no", 'val' => "");
				$data['field'][] = array('name' => "account_holder", 'val' => "");
				break;
			case "bank_update":
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'bank');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => "partner_id", 'val' => "");
				$data['field'][] = array('name' => "bank_partner_id", 'val' => "");
				$data['field'][] = array('name' => "bank_name", 'val' => "");
				$data['field'][] = array('name' => "account_no", 'val' => "");
				$data['field'][] = array('name' => "account_holder", 'val' => "");
				break;
			case "bank_delete":
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'bank');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => "partner_id", 'val' => "");
				$data['field'][] = array('name' => "bank_partner_id", 'val' => "");
				break;
		}
		$this->load->view('tester/form_api', $data);
	}

	public function class_deposit($action) {
		$data['ws'] = "deposit";
		$data['call'] = $action;
	
		switch ($action) {
			case "bank_topup_list":
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'deposit');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				break;
			case "topup_request":
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'deposit');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => "partner_id", 'val' => '');
				$data['field'][] = array('name' => 'transfer_date', 'val' => '','msg'=>"Format : YYYY-MM-DD");
				$data['field'][] = array('name' => 'payment_code', 'val' => '');
				$data['field'][] = array('name' => 'bank_topup_id', 'val' => '');
				$data['field'][] = array('name' => 'bank_partner_id', 'val' => '');
				$data['field'][] = array('name' => 'amount', 'val' => '');
				break;
			case "topup_data":
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'deposit');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => 'partner_id', 'val' => '');
				$data['field'][] = array('name' => 'page', 'val' => '');
				$data['field'][] = array('name' => 'per_page', 'val' => '');
				break;
			case "check_deposit":
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'deposit');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => "partner_id", 'val' => '');
				break;
			case "payment_gateway":
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'deposit');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				break;
			case "confirm_deposit":
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'deposit');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => "topup_id", 'val' => '');
				break;
		}
		$this->load->view('tester/form_api', $data);
	}

	public function class_hotel($action){
		
		$data['ws'] = 'hotel';
		$data['call'] = $action;

		switch ($action) {
			case "ws_srKeyword";
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'hotel');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => 'word', 'msg' => 'tulis nama kota atau poin of interest.');
			break;
			case "ws_findHotel";
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'hotel');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => 'check_in', 'val' => date('Y-m-d'));
				$data['field'][] = array('name' => 'check_out', 'val' => date('Y-m-d'));
				$data['field'][] = array('name' => 'rooms', 'val' => '1');
				$data['field'][] = array('name' => 'adults', 'val' => '1');
				$data['field'][] = array('name' => 'children', 'val' => '0');
				$data['field'][] = array('name' => 'lattitude', 'val' => '');
				$data['field'][] = array('name' => 'longitude', 'val' => '');
			break;
			case "ws_nextHotel";
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'hotel');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => 'findReferenceId', 'val' => '');
				$data['field'][] = array('name' => 'page', 'val' => '');
			break;
			case "ws_detailHotel";
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'hotel');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => 'findReferenceId', 'val' => '');
				$data['field'][] = array('name' => 'hotelId', 'val' => '');
			break;
			case "ws_selRoom";
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'hotel');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => 'detReferenceId', 'val' => '');
				$data['field'][] = array('name' => 'room_code', 'val' => '');
			break;
			case "ws_payment";
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'hotel');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				// $data['field'][] = array('name' => 'detReferenceId', 'val' => '');
				$data['field'][] = array('name' => 'firstName', 'val' => '');
				$data['field'][] = array('name' => 'lastName', 'val' => '');
				$data['field'][] = array('name' => 'email', 'val' => '');
				$data['field'][] = array('name' => 'phone', 'val' => '');
				$data['field'][] = array('name' => 'countryCode', 'val' => '+62');
				$data['field'][] = array('name' => 'single', 'val' => '');
				$data['field'][] = array('name' => 'double', 'val' => '');
				$data['field'][] = array('name' => 'extra', 'val' => '');
				$data['field'][] = array('name' => 'adults', 'val' => '');
				$data['field'][] = array('name' => 'children', 'val' => '');
				$data['field'][] = array('name' => 'rooms', 'val' => '');
				$data['field'][] = array('name' => 'checkin', 'val' => '');
				$data['field'][] = array('name' => 'checkout', 'val' => '');
				$data['field'][] = array('name' => 'hotelId', 'val' => '');
				$data['field'][] = array('name' => 'roomCategoryId', 'val' => '');
				$data['field'][] = array('name' => 'billToAffiliate', 'val' => 'false');
				// for($a=1;$a<=3;$a++):
				// 	$data['field'][] = array('name' => 'guest_room_'.$a, 'val' => '');
				// endfor;
			break;
			case "ws_bookInfo";
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'hotel');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => 'book_id', 'val' => '');
			break;
			
		}
		
		$this->load->view('tester/form_api', $data);
	}

	function jali(){
		$this->load->library("train/train_indotama");
		#print_r($this->train_indotama->get_station());
		$param_depart   = array("dep"           => "BD",
                                "arr"           => "GMR",
                                "depart_date"   => "2017-08-01",
                                "adult"			=> '2',
                                "infant"		=> '1');

		#echo json_encode($this->train_indotama->search($param_depart));
		
		$value 	= $this->converter->decode('Du9TU8Asbp1UnMvIcBJlKxQt7m72TwO9cVJd0gbPRxsql0FchUzX6JeIHCPsU7wvaryTzWD6Dw6-71wpiQGcYHDQMfQS9e3AdPH-GyaEsIDWMJ4tny66gqMsrhzZ1DEj');
		$param 	= array("value"			=> $value,
						"train_number"	=> "77D",
						"train_name"	=> "ARGO GOPAR",
						"sub_class"		=> "A");

		echo json_encode($this->train_indotama->detail($param));
		
	}

	public function class_upload($action) {
		$data['ws']	= "logreg";
		$data['call'] = $action;
	
		switch ($action) {
			case "upload_corporate_logo":
				$data['field'][] = array('name' => 'api_key', 'val' => '');
				$data['field'][] = array('name' => 'format','val'=>'');
				$data['field'][] = array('name' => 'ws','val'=>'logreg');
				$data['field'][] = array('name' => 'call', 'val' => $action);
				$data['field'][] = array('name' => "partner_id", 'val' => '');
				$data['field'][] = array('name' => "corporate_id", 'val' => '');
				break;
		}
		$this->load->view('tester/form_upload', $data);
	}
}

/* End of file tester.php */
/* Location: ./application/controllers/tester.php */