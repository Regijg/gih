<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Paymentdev extends CI_Controller {
	
	public $arr_product 		= array("topup", "flight", "train", "ecommerce");
	public $arr_payment_type 	= array("deposit", "TF", "BK", "VC", "MY", "CK", "BT");
	public $api_key 			= "2222";
	public $url					= "https://gih.appsku.tech/api/";
	public $api_url				= "https://gih.appsku.tech/api/ws";
	public $duitku_url			= "http://sandbox.duitku.com/webapi/api/merchant/inquiry"; #
	public $duitku_merchant_code = "D4676";
	public $duitku_api_key 		= "23b432d038667578e3a36b8e88f27f7f";
	
	function __construct()
	{
		parent::__construct();
		$this->load->library('curl');
	}

	public function ts()
	{
		$vcode="FLY";//CODE PRODUCT
		echo $vcode;
		//include "https://gih-indonesia.com/crt/q_create_invoice.php";
		file_get_contents('https://gih-indonesia.com/crt/q_create_invoice.php');
		echo $inv_number;
		$booking_code=substr($inv_number,6,15);
		echo $booking_code;
	}

	public function index(){
		
		$transaction_id = $_GET["transaction_id"];
		$amount 		= $_GET["amount"];
		$payment_type	= $_GET["pay"];
		//$partner_id		= $_GET["partner_id"];

		$param_get_product	= array("api_key" 	=> $this->api_key,
									"format"	=> "",
									"ws"		=> "report",
									"call"		=> "flight_detail",
									"book_id"	=> $transaction_id);
		$get_product = $this->post($param_get_product,$this->api_url);
		$get_product = json_decode($get_product, true);
		//print_r($get_product);
		if($get_product['rest_no'] == "0"){
			$amount = $get_product['data']['total_fares'] + $get_product['data']['margin'];
			
			if($get_product['data']['return']):
				$kdBook		= $get_product['data']["schedule"]['depart']['booking_code']." dan ".$get_product['data']["schedule"]['return']['booking_code'];
			else:
				$kdBook		= $get_product['data']["schedule"]['depart']['booking_code'];
			endif;			
			$message		= "Pembelian tiket pesawat kode booking ".$kdBook;

			//$order_id = $this->save_order($product, $transaction_id, $amount, $payment_type, $partner_id, $message);

				$merchantCode 		= $this->duitku_merchant_code;
			    $merchantKey 		= $this->duitku_api_key;
			    $paymentAmount 		= $amount;
			    $paymentMethod 		= $payment_type; // WW = duitku wallet, VC = Credit Card, MY = Mandiri Clickpay, BK = BCA KlikPay
			    $merchantOrderId 	= $get_product['data']['book_id']; //$this->converter->encode($order_id) from merchant, unique
			    $productDetails 	= $message;
			    $email 				= $get_product['data']['customer']['customer_email']; // your customer email
			    $phoneNumber 		= $get_product['data']['customer']['customer_phone']; // your customer phone number (optional)
			    $additionalParam 	= $partner_id; // optional
			    $callbackUrl 		= $this->url . 'paymentdev/callback?from=duitku'; // url for callback
			    $returnUrl 			= $this->url . 'paymentdev/result'; // url for redirect

			    $signature = md5($merchantCode . $merchantOrderId . $paymentAmount . $merchantKey);

			    $req_params = array(
			        'merchantCode' 		=> $merchantCode,
			        'paymentAmount' 	=> $paymentAmount,
			        'paymentMethod' 	=> $paymentMethod,
			        'merchantOrderId' 	=> $merchantOrderId,
			        'productDetails' 	=> $productDetails,
			        'additionalParam' 	=> $additionalParam,
			        'email' 			=> $email,
	    			'phoneNumber' 		=> $phoneNumber,
			        'callbackUrl' 		=> $callbackUrl,
			        'returnUrl' 		=> $returnUrl,
			        'signature' 		=> $signature
			    );
			   	
				$api_request = $this->post($req_params,$this->duitku_url);
				$api_request = json_decode($api_request, 1);

				if ( ! empty($api_request['reference']) && ! empty($api_request['paymentUrl']))
				{
					// Updating RESPONSE data
		        	redirect($api_request['paymentUrl']);
				}
				else
				{
					//$this->mgeneral->update(array("id" => $order_id), array("status" => "cancel"), "payment_order");
		        	$data['reason'] = "Terdapat Kesalahan dari Request API" . $api_request['statusMessage'];
					$this->load->view("payment/error", $data);
				}
			
		}
		else{
			$data['reason'] = $get_product['reason'];
			$this->load->view("payment/error", $data);
		}
	}
	
	private function save_order($product, $transaction_id, $amount, $payment_type, $partner_id, $message, $transaction_data = NULL){
		$arr_save 	= array("product" 			=> $product,
							"transaction_id"	=> $transaction_id,
							"amount"			=> $amount,
							"payment_type"		=> $payment_type,
							"partner_id"		=> $this->converter->decode($partner_id),
							"message"			=> $message,
							"transaction_data"	=> json_encode($transaction_data));
		$id = $this->mgeneral->save($arr_save, "payment_order");

		return $id;
	}

	public function callback(){
		
		#save log respon from duitku
		$postData	= json_encode($this->input->post());
		$varSave	= array('action'	=> "payment",
							'post'		=> $postData,
							'result'	=> $_GET['from']);
		$logId = $this->mgeneral->save($varSave,"log_payment");
		
		if($_GET['from'] == "duitku"){
			
			$apiKey = $this->duitku_api_key;
			$merchantCode = isset($_POST['merchantCode']) ? $_POST['merchantCode'] : null; 
			$amount = isset($_POST['amount']) ? $_POST['amount'] : null; 
			$merchantOrderId = isset($_POST['merchantOrderId']) ? $_POST['merchantOrderId'] : null; 
			$productDetail = isset($_POST['productDetail']) ? $_POST['productDetail'] : null; 
			$additionalParam = isset($_POST['additionalParam']) ? $_POST['additionalParam'] : null; 
			$paymentMethod = isset($_POST['paymentCode']) ? $_POST['paymentCode'] : null; 
			$resultCode = isset($_POST['resultCode']) ? $_POST['resultCode'] : null; 
			$merchantUserId = isset($_POST['merchantUserId']) ? $_POST['merchantUserId'] : null; 
			$reference = isset($_POST['reference']) ? $_POST['reference'] : null; 
			$signature = isset($_POST['signature']) ? $_POST['signature'] : null;
			
			if ( ! empty($merchantCode) && ! empty($amount) && ! empty($merchantOrderId) && ! empty($signature))
			{
			    $params = $merchantCode . $amount . $merchantOrderId . $apiKey;
			    $calcSignature = md5($params);

			    if ($signature == $calcSignature)
			    {
			    	$order_id = $merchantOrderId;
			    }
			}
			
		}
		else if($_GET['from'] == "cron"){
			$order_id = $this->converter->decode($_POST['order_id']);
		}

		if(isset($order_id)){
			$get_data = $this->mgeneral->getWhere(array("id" => $order_id, "status" => "free"), "payment_order");

			if(count($get_data) > 0){
				$this->mgeneral->update(array("id" => $order_id), array("status" => "used"), "payment_order");
				$param_payment	= array("api_key" 	=> $this->api_key,
											"format"	=> "",
											"ws"		=> "flight",
											"call"		=> "ws_payment",
											"book_id"	=> $get_data[0]->transaction_id,
											'reference'	=> $reference);
				$resultHit = $this->curl->post_custom($this->api_url, "", $param_payment);
				
				$varSave	= array('result'	=> json_encode($resultHit));
				$this->mgeneral->update(array('log_payment_id'=>$logId),$varSave,"log_payment");
			}
			else{
				$data['reason'] = "gagal mendapatkan data";
				$this->load->view("payment/error", $data);
			}
		}
	}

	public function endtrx()
	{
		$this->load->view("payment/end");
	}
	
	public function result(){
		//$data['message'] = "Payment berhasil, silahkan cek data transaksi";
		//$this->load->view("payment/result", $data);
		redirect("https://gih-indonesia.com/payment/statuspaymentflysty.php");
	}
	
	public function tes(){
		$data['message'] = "<p>silahkan cek data transaksi</p>";
		$this->load->view("payment/result", $data);
	}

	function post($post_data,$url)
	{
		foreach ($post_data as $key => $value) {
            $post_items[] = $key . '=' . $value;
        }
        $data = implode('&', $post_items);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, '100');
		curl_setopt($ch, CURLOPT_TIMEOUT, 300);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		$result = curl_exec($ch);
		#print_r($result);
		if($result === false):
			$data	= json_encode(array("rest_no"	=> "500",
							"reason"	=> "Server sedang mengalami ganguan, silahkan coba lagi."));
		else:
			$data	= $result;
		endif;
		
		return $data;
		curl_close($ch);
	}
}