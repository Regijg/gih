<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Garuda_gateway extends CI_Controller {
    
    function submit_post(){
        $data   = json_decode($this->input->post("data"), true);
        $url    = $data['url'];
        $params = json_encode($data['param']);
        $auth   = $data['auth'];
        $result = $this->curl_post($url, $params, $auth);
        // print_r($result);
        echo json_encode($result);
    }

    function submit_get(){
        $data   = json_decode($this->input->post("data"), true);
        $url    = $data['url'];
        $params = $data['param'];
        $auth   = $data['auth'];
        $result = $this->curl_get($url, $auth);
        echo json_encode($result);
    }
    
    function curl_get($url, $auth){
        
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Accept: application/json';
        $headers[] = 'Connection: Keep-Alive';
        $headers[] = 'Authorization: Basic '.$auth;
        $user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.19 (KHTML, like Gecko) Chrome/1.0.154.53 Safari/525.19';
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, '300');
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $result = curl_exec($ch);
        
        if($result === false)
        {
            $resultData['error_no']     = curl_errno($ch);
            $resultData['error_msg']    = curl_error($ch);
        }
        else
        {
            $resultData['error_no']     = "0";
            $resultData['result']       = json_decode($result, true);
        }
        
        return $resultData;
    }
    
    function curl_post($url, $data, $auth){
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Accept: application/json';
        $headers[] = 'Connection: Keep-Alive';
        $headers[] = 'Authorization: Basic '.$auth;
        $user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.19 (KHTML, like Gecko) Chrome/1.0.154.53 Safari/525.19';
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://dev-ags.garuda-indonesia.com/altea/');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, '300');
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);
        
        if($result === false)
        {
            $resultData['error_no']     = curl_errno($ch);
            $resultData['error_msg']    = curl_error($ch);
        }
        else
        {
            $resultData['error_no']     = "0";
            $resultData['result']       = json_decode($result, true);
        }
        
        return $resultData;
    }
}

/* End of file btrav.php */
/* Location: ./application/controllers/btrav.php */