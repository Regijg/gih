<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Poolflight extends CI_Controller
{
	var $maintenance	= "no";

	function __construct()
	{
		parent::__construct();
		#$this->load->library('webservice/flight');
		$this->load->library('ws/wsflight');
	}

	function index()
    {
		$this->benchmark->mark('code_start');
		if($this->maintenance=="no"): #start cek maintenance

			$hak_akses = $this->cekAkses($this->input->post());
			
			if($hak_akses['rest_no']==0):

				#get basic parameter
				$api_key				= $this->post('api_key');
				$call					= strtolower($this->post('call'));
				$parameter				= $this->input->post(); #get all post parameter
				$parameter['client']	= $hak_akses['client'];
				$idLog 					= $this->saveLog($api_key,$_SERVER['REMOTE_ADDR'],$call,$parameter); #save log akses

				#show respons dan tampilkan data dulu baru proses
				ob_end_clean();
				header("Connection: close\r\n");
				ignore_user_abort(true); 
				ob_start();
				
					@apache_setenv('no-gzip', 1);
					@ini_set('zlib.output_compression', 0);
					@ini_set('implicit_flush', 1);
					for ($i = 0; $i < ob_get_level(); $i++) { ob_end_flush(); }
					ob_implicit_flush(1);
					echo json_encode(array("identification_id" => $this->converter->encode($idLog)));
				
				$size = ob_get_length();
				header("Content-Length: $size");
				ob_end_flush();     
				flush();          
				ob_end_clean();

				#POST TO LIBRARIES
				#$result	= $this->flight->index($call,$parameter);
				$result	= $this->wsflight->index($call,$parameter);


				#cek apakah result dari webservice kosong ?
				if($result==""):
						$result	= array('rest_no'	=> "00",
										'reason'	=> "Tidak ada respon dari server, silahkan coba lagi.");
				endif;

			else:

				$result	= $hak_akses;

			endif;


		else: #jika sedang maintenance

			$result	= array('rest_no'	=> "100",
							'reason'	=> "Sistem sedang dalam perbaikan, silahkan hubungi customer service kami untuk info lebih lanjut.");

		endif; #end cek maintenance
		$this->benchmark->mark('code_end');
		/*$updateLog	= array('result'		=> json_encode($result),
							'error_no'		=> $result['rest_no'],
							'elapsed_time'	=> $this->benchmark->elapsed_time('code_start', 'code_end'),
							'memory_usage'	=> $this->memory_usage());*/
		$updateLog	= array('result'		=> json_encode($result),
							'respon_code'		=> $result['rest_no']);
		$this->mgeneral->update(array('log_id'=>$idLog),$updateLog,"log_access");
		/*if ($class != 'ticket') {
		
			header('Content-Type: application/json');
			echo json_encode($result);

			//update log result
			$updateLog	= array('result'		=> json_encode($result),
								'error_no'		=> $result['rest_no'],
								'elapsed_time'	=> $this->benchmark->elapsed_time('code_start', 'code_end'),
								'memory_usage'	=> $this->memory_usage());
			$this->mgeneral->update(array('log_id'=>$idLog),$updateLog,"log_access");
		
		} else {
			if ($function == 'html' OR $function == 'pdf') {
				echo $result;
			} else {
				$this->output
				    ->set_content_type('application/json')
				    ->set_output(json_encode($result));
			}
		}*/
	}

	function test(){
		echo "test";
	}

	function getData($id){
		$log_data = $this->mgeneral->getWhere(array("log_id" => $this->converter->decode($id)), "log_access");
		if(count($log_data) > 0 && $log_data[0]->result != NULL){
			header('Content-Type: application/json');
			echo $log_data[0]->result;
		}
		else if(count($log_data) > 0){
			echo '{"message":"process"}';
		}
		else{
			echo '{"message":"not found"}';
		}
	}

	private function saveLog($api_key,$ip,$call,$param)
	{
		/*$dataLog	= array('api_key'		=> $api_key,
							'IP'			=> $ip,
							'class'			=> $ws,
							'action'		=> $call,
							'post'			=> json_encode($param));*/
		$dataLog	= array('api_key'		=> $api_key,
							'IP'			=> $ip,
							'produk'		=> "flight",
							'function'		=> $call,
							'post'			=> json_encode($param));
		$idLog	= $this->mgeneral->save($dataLog,"log_access");
		return $idLog;
	}

	private function cekAkses($parameter)
	{
		#cek client data
		$client = $this->mgeneral->getWhere(array('api_key'=>$parameter['api_key']),"mstr_api_access");

		if(count($client)!=0):

			#cek ip client
			#if($_SERVER['REMOTE_ADDR'] == $client[0]->client_ip):
				$result = array('rest_no'		=> "0",
								'client'		=> array('client_id'	=> $client[0]->api_id,
														 'access_type'	=> $client[0]->access_type,
														 'partner_id'	=> $client[0]->partner_id,
														 'client_email'	=> $client[0]->client_email,
														 'client_app'	=> $client[0]->app)
								);
			#else:
			#	$result = array('err_no'	=> "102",
			#					'err_msg'	=> "Akses tidak dikenali");
			#endif;


		else:

			$result = array('rest_no'	=> "101",
							'reason'	=> "Client tidak dikenal");

		endif;

		return $result;

	}

	private function post($name)
	{
		$value	= $this->input->post($name);
		return $value;
	}

	private function memory_usage()
	{
        $mem_usage = memory_get_usage(true);

        if ($mem_usage < 1024)
            $mem_usage2 = $mem_usage." bytes";
        elseif ($mem_usage < 1048576)
            $mem_usage2 = round($mem_usage/1024,2)." kilobytes";
        else
            $mem_usage2 = round($mem_usage/1048576,2)." megabytes";

      return $mem_usage2;
    }
}

?>
