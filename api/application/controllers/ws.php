<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ws extends CI_Controller {
	
	var $maintenance	= "no";

	function __construct()
	{
		parent::__construct();
		$this->load->library('ws/wslogreg');
		$this->load->library('ws/wsflight');
		$this->load->library('ws/wsdata');
		$this->load->library('ws/wsreport');
		$this->load->library('ws/wshotel2');
		
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		$method = $_SERVER['REQUEST_METHOD'];
		if($method == "OPTIONS") {
			die();
		}
	}
	
	function index()
    {
		$this->benchmark->mark('code_start');
		
			if($this->maintenance=="no"): #start cek maintenance
				
				$hak_akses	= $this->cekAkses($this->input->post());
				$api_key	= $this->input->post('api_key');
				$ws			= strtolower($this->input->post('ws'));
				$call		= strtolower($this->input->post('call'));
				$format		= strtolower($this->input->post('format'));
				$param		= $this->input->post();
				$param['client']	= $hak_akses['client'];
				$idLog 		= $this->saveLog($api_key,$_SERVER['REMOTE_ADDR'],$ws,$call,$param); #save log akses
				if($hak_akses['rest_no']==0):

					switch($ws):
						case "logreg":	$result = $this->wslogreg->index($call,$param); break;
						case "flight":	$result = $this->wsflight->index($call,$param); break;
						case "data":	$result = $this->wsdata->index($call,$param); break;
						case "hotel":	$result = $this->wshotel2->index($call,$param); break;
						case "report":	$result = $this->wsreport->index($call,$param); break;
						default : 
							$result	= array("rest_no" => 1,"reason" => "ws tidak ditemukan");
						break;
					endswitch;
					
				else:
					$result	= $hak_akses;
				endif;
				
			endif;	
		
		$this->benchmark->mark('code_end');
		
		if (($call == 'html_ticket' OR $call == 'pdf_ticket') AND $result['rest_no'] == "0") :
			echo $result['data']['ticket'];
		else:
			if($format == "mobile"):
				echo $this->converter->mobile_api_format($result);
			elseif($format == "xml"):
				echo $this->converter->xml_api_format($result);
			else:
				header('Content-Type: application/json');
				echo json_encode($result);
			endif;
		endif;
		
		//update log result
		$updateLog	= array('result'		=> json_encode($result),
							'respon_code'		=> $result['rest_no']);				
		$this->mgeneral->update(array('log_id'=>$idLog),$updateLog,"log_access");
	}
	
	private function saveLog($api_key,$ip,$ws,$call,$param)
	{
		$dataLog	= array('api_key'		=> $api_key,
							'IP'			=> $ip,
							'produk'			=> $ws,
							'function'		=> $call,
							'post'			=> json_encode($param));
		$idLog	= $this->mgeneral->save($dataLog,"log_access");
		return $idLog;
	}
	
	private function cekAkses($parameter)
	{
		#cek client data
		$client = $this->mgeneral->getWhere(array('api_key'=>$parameter['api_key']),"mstr_api_access");

		if(count($client)!=0):

			#cek ip client
			#if($_SERVER['REMOTE_ADDR'] == $client[0]->client_ip):
				/*
				$result = array('rest_no'		=> "0",
								'client'		=> array('client_id'	=> $client[0]->client_id,
														 'client_name'	=> $client[0]->client_name,
														 'client_email'	=> $client[0]->client_email,
														 'client_app'	=> $client[0]->app)
								);
				*/
				$result = array('rest_no'		=> "0",
								'client'		=> array('client_id'	=> $client[0]->api_id,
														 'access_type'	=> $client[0]->access_type,
														 'partner_id'	=> $client[0]->partner_id,
														//  'client_email'	=> $client[0]->client_email,
														 'client_app'	=> $client[0]->app_name)
								);
			#else:
			#	$result = array('err_no'	=> "102",
			#					'err_msg'	=> "Akses tidak dikenali");
			#endif;


		else:

			$result = array('rest_no'	=> "101",
							'reason'	=> "Client tidak dikenal");

		endif;

		return $result;

	}
	
	private function memory_usage()
	{
        $mem_usage = memory_get_usage(true);

        if ($mem_usage < 1024)
            $mem_usage2 = $mem_usage." bytes";
        elseif ($mem_usage < 1048576)
            $mem_usage2 = round($mem_usage/1024,2)." kilobytes";
        else
            $mem_usage2 = round($mem_usage/1048576,2)." megabytes";

      return $mem_usage2;
    }
}
