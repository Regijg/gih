<?php
$dt_train 	= $kereta['dt_train'];
$passenger 	= $kereta['dt_train_pax'];
$schedule  	= $kereta['dt_train_schedule'];

	foreach($passenger as $p):
		$seatD .= $p->pax_depart_wagon_code."-".$p->pax_depart_wagon_no."/".$p->pax_depart_seat." "; 
		$seatR .= $p->pax_return_wagon_code."-".$p->pax_return_wagon_no."/".$p->pax_return_seat." ";
	endforeach;
?>
<!DOCTYPE html>
<html>
<head>
	<title>Struk Pembelian Tiket Kereta Api</title>
</head>
<body style="font-family:'Courier New', Courier, monospace">
	<table cellpadding="0" cellspacing="0" frame="box" width="100%" style="border: 0px; overflow: hidden;">
		<!-- Header Comment -->
		<tr>
			<td align="left" style="border-bottom: 1px solid #CCCCCC;"><img src="<?php echo base_url().'asset/image/icon-kai/logo-kai.png'; ?>" style="margin: 10px 0 10px 25px;" width="120px;"></td>
			<td align="right" style="border-bottom: 1px solid #CCCCCC;">
                <h4><?=$param['travel_name']?></h4>
                <p style="margin-top:-10px; font-size:12px;"><?=$param['travel_addr']?><br /><?=$param['travel_contact']?></p>
            </td>
		</tr>
        <tr>
        	<td colspan="2"><p align="center">STRUK PEMBELIAN TIKET KERETA API</p></td>
        </tr>
		<!-- /Header Comment -->
        <!-- Schedule Data -->
        <tr>
        	<td colspan="2" style="padding:0 20px 0 20px;">
            	
                <table style="border:1px #999999 solid; padding:10px 10px 10px 10px;" cellpadding="0" cellspacing="0" width="100%">
                	<tr>
                    	<td width="40%" align="center">
                        	
                            <table style="border-right:1px #999999 solid;" width="100%">
                            	<tr>
                                	<td colspan="3" align="center">
                                    	<img src="http://phpqrcode.sourceforge.net/examples/example_001_simple_png_output.php">
                                    </td>
                                </tr>
                                <tr>
                                	<td width="30%">No. KA</td><td width="1%">:</td>
                                    <td><?=$dt_train[0]->depart_train_no?></td>
                                </tr>
                                <?php $tglBayar = explode(" ",$dt_train[0]->dtt_issued_date);?>
                                <tr>
                                	<td>Tgl. Pembayaran</td><td width="1%">:</td>
                                    <td><?=$this->converter->setTanggalNama($tglBayar[0])?></td>
                                </tr>
                                <tr>
                                	<td>Jam Pembayaran</td><td width="1%">:</td>
                                    <td><?=$tglBayar[1]?></td>
                                </tr>
                            </table>
                            
                        </td>
                        <td colspan="3" align="left">
                        	
                            <table width="100%" style="padding:20px 0 0 10px;">
                            	<tr>
                                	<td width="30%">Kode Booking</td><td width="1%">:</td>
                                    <td><?=$dt_train[0]->depart_book_code?></td>
                                </tr>
                                <tr>
                                	<td>Nama</td><td width="1%">:</td>
                                    <td><?=$passenger[0]->pax_name?></td>
                                </tr>
                                <tr>
                                	<td>Nama KA</td><td width="1%">:</td>
                                    <td><?=$dt_train[0]->depart_train_name?></td>
                                </tr>
                                <tr>
                                	<td>Tgl. Keberangkatan</td><td width="1%">:</td>
                                    <td><?=$this->converter->setTanggalNama($schedule[0]->sch_departure_date)?></td>
                                </tr>
                                <tr>
                                	<td>Stasiun/Jam Asal</td><td width="1%">:</td>
                                    <td><?=$this->converter->set_codeToStation($dt_train[0]->dtt_from)?> / <?=$schedule[0]->sch_etd?></td>
                                </tr>
                                <tr>
                                	<td>Stasiun/Jam Tiba</td><td width="1%">:</td>
                                    <td><?=$this->converter->set_codeToStation($dt_train[0]->dtt_to)?> / <?=$schedule[0]->sch_eta?></td>
                                </tr>
                                <tr>
                                	<td>Kelas</td><td width="1%">:</td>
                                    <td><?=$schedule[0]->sch_class_name?></td>
                                </tr>
                            </table>
                            <br>
                        </td>
                    </tr>
                    <tr>
        				<td style="border-top:1px #999999 solid; padding:10px 0 10px 0;" colspan="2">
                        No. Tempat Duduk : <?=$seatD?>
                        </td>
                    </tr>
                    <tr>
        				<td style="border-top:1px #999999 solid; padding-top:10px;" colspan="2">
                        	
                            <table width="100%">
                            	<tr>
                                	<td align="left">Penumpang</td>
                                    <td align="center">Jumlah</td>
                                    <td align="right">Harga (Rp)</td>
                                    <td align="right">Jumlah (Rp)</td>
                                </tr>
                                <tr>
                                	<td align="left">Dewasa + Bayi</td>
                                    <td align="center"><?=$dt_train[0]->dtt_adult+$dt_train[0]->dtt_infant?></td>
                                    <td align="right"><?=number_format($schedule[0]->sch_normal_price, 0, ",", ".")?></td>
                                    <td align="right"><?=number_format($schedule[0]->sch_normal_price, 0, ",", ".")?></td>
                                </tr>
                                <tr>
                                	<td align="left">Biaya Administrasi</td>
                                    <td align="center"></td>
                                    <td align="right"><?=number_format($schedule[0]->sch_extra_fee, 0, ",", ".")?></td>
                                    <td align="right"><?=number_format($schedule[0]->sch_extra_fee, 0, ",", ".")?></td>
                                </tr>
                                <tr>
                                	<td align="left">Diskon Channel</td>
                                    <td align="center"></td>
                                    <td align="right"><?=number_format($schedule[0]->sch_discount, 0, ",", ".")?></td>
                                    <td align="right"><?=number_format($schedule[0]->sch_discount, 0, ",", ".")?></td>
                                </tr>
                                <tr>
                                	<td style="padding-top:10px;" align="left" colspan="3">Harga Total</td>
                                    <td style="padding-top:10px;" align="right"><?=number_format($schedule[0]->sch_ticket_price, 0, ",", ".")?></td>
                                </tr>
                            </table>
                            
                        </td>
                    </tr>
                </table>
            	
            </td>
        </tr>
        <!-- /Schedule Data -->
 </table>
        
        <?php if ($dt_train[0]->dtt_roundtrip == 'return'): ?>
        	
        <table cellpadding="0" cellspacing="0" frame="box" width="100%" style="border: 0px; overflow: hidden;">
            <!-- Header Comment -->
            <tr>
                <td align="left" style="border-bottom: 1px solid #CCCCCC;"><img src="<?php echo base_url().'asset/image/icon-kai/logo-kai.png'; ?>" style="margin: 10px 0 10px 25px;" width="120px;"></td>
                <td align="right" style="border-bottom: 1px solid #CCCCCC;">
                    <h4><?=$param['travel_name']?></h4>
                    <p style="margin-top:-10px; font-size:12px;"><?=$param['travel_addr']?><br /><?=$param['travel_contact']?></p>
                </td>
            </tr>
            <tr>
                <td colspan="2"><p align="center">STRUK PEMBELIAN TIKET KERETA API</p></td>
            </tr>
            <!-- /Header Comment -->
            <!-- Schedule Data -->
            <tr>
                <td colspan="2" style="padding:0 20px 0 20px;">
                    
                    <table style="border:1px #999999 solid; padding:10px 10px 10px 10px;" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td width="40%" align="center">
                                
                                <table style="border-right:1px #999999 solid;" width="100%">
                                    <tr>
                                        <td colspan="3" align="center">
                                            <img src="http://phpqrcode.sourceforge.net/examples/example_001_simple_png_output.php">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="30%">No. KA</td><td width="1%">:</td>
                                        <td><?=$dt_train[0]->return_train_no?></td>
                                    </tr>
                                    <?php $tglBayar = explode(" ",$dt_train[0]->dtt_issued_date);?>
                                    <tr>
                                        <td>Tgl. Pembayaran</td><td width="1%">:</td>
                                        <td><?=$this->converter->setTanggalNama($tglBayar[0])?></td>
                                    </tr>
                                    <tr>
                                        <td>Jam Pembayaran</td><td width="1%">:</td>
                                        <td><?=$tglBayar[1]?></td>
                                    </tr>
                                </table>
                                
                            </td>
                            <td colspan="3" align="left">
                                
                                <table width="100%" style="padding:20px 0 0 10px;">
                                    <tr>
                                        <td width="30%">Kode Booking</td><td width="1%">:</td>
                                        <td><?=$dt_train[0]->return_book_code?></td>
                                    </tr>
                                    <tr>
                                        <td>Nama</td><td width="1%">:</td>
                                        <td><?=$dt_train[0]->customer_name?></td>
                                    </tr>
                                    <tr>
                                        <td>Nama KA</td><td width="1%">:</td>
                                        <td><?=$dt_train[0]->return_train_name?></td>
                                    </tr>
                                    <tr>
                                        <td>Tgl. Keberangkatan</td><td width="1%">:</td>
                                        <td><?=$this->converter->setTanggalNama($schedule[1]->sch_departure_date)?></td>
                                    </tr>
                                    <tr>
                                        <td>Stasiun/Jam Asal</td><td width="1%">:</td>
                                        <td><?=$this->converter->set_codeToStation($dt_train[1]->dtt_from)?> / <?=$schedule[1]->sch_etd?></td>
                                    </tr>
                                    <tr>
                                        <td>Stasiun/Jam Tiba</td><td width="1%">:</td>
                                        <td><?=$this->converter->set_codeToStation($dt_train[1]->dtt_to)?> / <?=$schedule[1]->sch_eta?></td>
                                    </tr>
                                    <tr>
                                        <td>Kelas</td><td width="1%">:</td>
                                        <td><?=$schedule[1]->sch_class_name?></td>
                                    </tr>
                                </table>
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-top:1px #999999 solid; padding:10px 0 10px 0;" colspan="2">
                            No. Tempat Duduk : <?=$seatR?>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-top:1px #999999 solid; padding-top:10px;" colspan="2">
                                
                                <table width="100%">
                                    <tr>
                                        <td align="left">Penumpang</td>
                                        <td align="center">Jumlah</td>
                                        <td align="right">Harga (Rp)</td>
                                        <td align="right">Jumlah (Rp)</td>
                                    </tr>
                                    <tr>
                                        <td align="left">Dewasa + Bayi</td>
                                        <td align="center"><?=$dt_train[0]->dtt_adult+$dt_train[0]->dtt_infant?></td>
                                        <td align="right"><?=number_format($schedule[1]->sch_normal_price, 0, ",", ".")?></td>
                                        <td align="right"><?=number_format($schedule[1]->sch_normal_price, 0, ",", ".")?></td>
                                    </tr>
                                    <tr>
                                        <td align="left">Biaya Administrasi</td>
                                        <td align="center"></td>
                                        <td align="right"><?=number_format($schedule[1]->sch_extra_fee, 0, ",", ".")?></td>
                                        <td align="right"><?=number_format($schedule[1]->sch_extra_fee, 0, ",", ".")?></td>
                                    </tr>
                                    <tr>
                                        <td align="left">Diskon Channel</td>
                                        <td align="center"></td>
                                        <td align="right"><?=number_format($schedule[1]->sch_discount, 0, ",", ".")?></td>
                                        <td align="right"><?=number_format($schedule[1]->sch_discount, 0, ",", ".")?></td>
                                    </tr>
                                    <tr>
                                        <td style="padding-top:10px;" align="left" colspan="3">Harga Total</td>
                                        <td style="padding-top:10px;" align="right"><?=number_format($schedule[1]->sch_ticket_price, 0, ",", ".")?></td>
                                    </tr>
                                </table>
                                
                            </td>
                        </tr>
                    </table>
                    
                </td>
            </tr>
            <!-- /Schedule Data -->
            
         </table>
            
        <?php endif; ?>
</body>
</html>