<?php
$dt_train 	= $kereta['dt_train'];
$passenger 	= $kereta['dt_train_pax'];
$schedule  	= $kereta['dt_train_schedule'];
$travel     = $kereta['travel'];

	foreach($passenger as $p):
		$seatD .= $p->pax_depart_wagon_code."-".$p->pax_depart_wagon_no."/".$p->pax_depart_seat." "; 
		$seatR .= $p->pax_return_wagon_code."-".$p->pax_return_wagon_no."/".$p->pax_return_seat." ";
	endforeach;
?>
<page style="font-family: Courier, Arial, Helvetica; font-size: 14px" pagegroup="new">

    <page_header>
        <table border="0" cellpadding="0" cellspacing="0">
            <tr valign="middle">
                <td style="width: 390px; height: 40px;">
                <?php if($travel['business_logo']!=""):?>
                    <img src="<?=$travel['business_logo']?>" style="height: 60px; margin: 10px 0 10px 10px;">
                <?php else: ?>
                    <img src="<?php echo base_url().'asset/image/icon-kai/logo-kai.png'; ?>" style="margin: 10px 0 10px 25px;" width="120px;">
                <?php endif; ?>
               	</td>
                <td style="width: 345px;" align="right">
                    <h4><?=$travel['business_name']?></h4>
                    <p style="margin-top:-10px; font-size:12px;"><?=$travel['business_address']?><br /><?=$travel['business_phone']?></p>
                </td>
            </tr>
        </table>
		<hr />
        <p align="center" style="margin-top:0px;">STRUK PEMBELIAN TIKET KERETA API
        <?php if($travel['business_logo']!=""): $padtop = "210"; ?>
            <br /><img src="<?php echo base_url().'asset/image/icon-kai/logo-kai.png'; ?>" style="margin: 10px 0 10px 25px;" width="120px;">
        <?php else: $padtop = "150"; 
		endif; ?>
        </p>
    </page_header>
    <div style="padding:<?=$padtop?>px 1px 1px 1px; width:99.7%;">
    
        <table style="border:1px #999999 solid; padding:10px 10px 10px 10px;" cellpadding="0" cellspacing="0" width="890px;">
	        <tr>
                <td width="300px;">
                    
                    <table>
                        <tr>
                            <td colspan="3" align="center">
                                <qrcode value="<?php echo $dt_train[0]->depart_book_code; ?>" ec="H" style="width:32mm;border:none;"></qrcode>
                            </td>
                        </tr>
                        <tr>
                            <td height="25px;" width="30%">No. KA</td><td width="1%">:</td>
                            <td><?=$dt_train[0]->depart_train_no?></td>
                        </tr>
                        <?php $tglBayar = explode(" ",$dt_train[0]->dtt_issued_date);?>
                        <tr>
                            <td height="25px;">Tgl. Pembayaran</td><td width="1%">:</td>
                            <td><?=$this->converter->setTanggalNama($tglBayar[0],"2")?></td>
                        </tr>
                        <tr>
                            <td height="25px;">Jam Pembayaran</td><td width="1%">:</td>
                            <td><?=$tglBayar[1]?></td>
                        </tr>
                    </table>
                    
                </td>
                <td width="420px;" align="left">
                    
                    <table width="100%" style="border-left:1px #999999 solid;padding:0px 0 0 20px;">
                        <tr>
                            <td height="25px;" width="30%">Kode Booking</td><td width="1%">:</td>
                            <td><?=$dt_train[0]->depart_book_code?></td>
                        </tr>
                        <tr>
                            <td height="25px;">Nama</td><td width="1%">:</td>
                            <td><?=$passenger[0]->pax_name?></td>
                        </tr>
                        <tr>
                            <td height="25px;">Nama KA</td><td width="1%">:</td>
                            <td><?=$dt_train[0]->depart_train_name?></td>
                        </tr>
                        <tr>
                            <td height="25px;">Tgl. Keberangkatan</td><td width="1%">:</td>
                            <td><?=$this->converter->setTanggalNama($schedule[0]->sch_departure_date)?></td>
                        </tr>
                        <tr>
                            <td height="25px;">Stasiun/Jam Asal</td><td width="1%">:</td>
                            <td><?=$this->converter->set_codeToStation($dt_train[0]->dtt_from)?> / <?=$schedule[0]->sch_etd?></td>
                        </tr>
                        <tr>
                            <td height="25px;">Stasiun/Jam Tiba</td><td width="1%">:</td>
                            <td><?=$this->converter->set_codeToStation($dt_train[0]->dtt_to)?> / <?=$schedule[0]->sch_eta?></td>
                        </tr>
                        <tr>
                            <td height="25px;">Kelas</td><td width="1%">:</td>
                            <td><?=$schedule[0]->sch_class_name?></td>
                        </tr>
                    </table>
                    <br>
                </td>
            </tr>
            <tr>
                <td style="border-top:1px #999999 solid; padding:20px 0 20px 0;" colspan="2">
                No. Tempat Duduk: <?=$seatD?>
                </td>
            </tr>
            <tr>
                <td style="border-top:1px #999999 solid; padding-top:10px;" colspan="2">
                    
                    <table width="100%">
                        <tr>
                            <td width="200px;" height="25px;" align="left">Penumpang</td>
                            <td width="100px;" align="center">Jumlah</td>
                            <td width="200px;" align="right">Harga (Rp)</td>
                            <td width="200px;" align="right">Jumlah (Rp)</td>
                        </tr>
                        <tr>
                            <td height="25px;" align="left">Dewasa + Bayi</td>
                            <td align="center"><?=$dt_train[0]->dtt_adult+$dt_train[0]->dtt_infant?></td>
                            <td align="right"><?=number_format($schedule[0]->sch_normal_price, 0, ",", ".")?></td>
                            <td align="right"><?=number_format($schedule[0]->sch_normal_price, 0, ",", ".")?></td>
                        </tr>
                        <tr>
                            <td height="25px;" align="left">Biaya Administrasi</td>
                            <td align="center"></td>
                            <td align="right"><?=number_format($schedule[0]->sch_extra_fee, 0, ",", ".")?></td>
                            <td align="right"><?=number_format($schedule[0]->sch_extra_fee, 0, ",", ".")?></td>
                        </tr>
                        <tr>
                            <td height="25px;" align="left">Diskon Channel</td>
                            <td align="center"></td>
                            <td align="right"><?=number_format($schedule[0]->sch_discount, 0, ",", ".")?></td>
                            <td align="right"><?=number_format($schedule[0]->sch_discount, 0, ",", ".")?></td>
                        </tr>
                        <tr>
                            <td height="25px;" style="padding-top:10px;" align="left" colspan="3">Harga Total</td>
                            <td style="padding-top:10px;" align="right"><?=number_format($schedule[0]->sch_ticket_price, 0, ",", ".")?></td>
                        </tr>
                    </table>
                    
                </td>
            </tr>
        </table>
        
	</div>
    
</page>

<?php if ($dt_train[0]->dtt_roundtrip == 'return'): ?>
<page style="font-family: Courier, Arial, Helvetica; font-size: 14px" pagegroup="new">

    <page_header>
        <table border="0" cellpadding="0" cellspacing="0">
            <tr valign="middle">
                <td style="width: 390px; height: 40px;"><img src="<?php echo base_url().'asset/image/icon-kai/logo-kai.png'; ?>" style="margin: 10px 0 10px 25px;" width="120px;"></td>
                <td style="width: 345px;" align="right">
                	<h4><?=$param['travel_name']?></h4>
                    <p style="margin-top:-10px; font-size:12px;"><?=$param['travel_addr']?><br /><?=$param['travel_contact']?></p>
                </td>
            </tr>
        </table>
		<hr />
        <p align="center" style="margin-top:0px;">STRUK PEMBELIAN TIKET KERETA API</p>
    </page_header>
    <div style="padding:150px 1px 1px 1px; width:99.7%;">
    
        <table style="border:1px #999999 solid; padding:10px 10px 10px 10px;" cellpadding="0" cellspacing="0" width="890px;">
	        <tr>
                <td width="300px;">
                    
                    <table>
                        <tr>
                            <td colspan="3" align="center">
                                <qrcode value="<?php echo $dt_train[0]->return_book_code; ?>" ec="H" style="width:32mm;border:none;"></qrcode>
                            </td>
                        </tr>
                        <tr>
                            <td height="25px;" width="30%">No. KA</td><td width="1%">:</td>
                            <td><?=$dt_train[0]->return_train_no?></td>
                        </tr>
                        <?php $tglBayar = explode(" ",$dt_train[0]->dtt_issued_date);?>
                        <tr>
                            <td height="25px;">Tgl. Pembayaran</td><td width="1%">:</td>
                            <td><?=$this->converter->setTanggalNama($tglBayar[0],"2")?></td>
                        </tr>
                        <tr>
                            <td height="25px;">Jam Pembayaran</td><td width="1%">:</td>
                            <td><?=$tglBayar[1]?></td>
                        </tr>
                    </table>
                    
                </td>
                <td width="420px;" align="left">
                    
                    <table width="100%" style="border-left:1px #999999 solid;padding:0px 0 0 20px;">
                        <tr>
                            <td height="25px;" width="30%">Kode Booking</td><td width="1%">:</td>
                            <td><?=$dt_train[0]->return_book_code?></td>
                        </tr>
                        <tr>
                            <td height="25px;">Nama</td><td width="1%">:</td>
                            <td><?=$dt_train[0]->customer_name?></td>
                        </tr>
                        <tr>
                            <td height="25px;">Nama KA</td><td width="1%">:</td>
                            <td><?=$dt_train[0]->return_train_name?></td>
                        </tr>
                        <tr>
                            <td height="25px;">Tgl. Keberangkatan</td><td width="1%">:</td>
                            <td><?=$this->converter->setTanggalNama($schedule[1]->sch_departure_date)?></td>
                        </tr>
                        <tr>
                            <td height="25px;">Stasiun/Jam Asal</td><td width="1%">:</td>
                            <td><?=$this->converter->set_codeToStation($dt_train[1]->dtt_from)?> / <?=$schedule[1]->sch_etd?></td>
                        </tr>
                        <tr>
                            <td height="25px;">Stasiun/Jam Tiba</td><td width="1%">:</td>
                            <td><?=$this->converter->set_codeToStation($dt_train[1]->dtt_to)?> / <?=$schedule[1]->sch_eta?></td>
                        </tr>
                        <tr>
                            <td height="25px;">Kelas</td><td width="1%">:</td>
                            <td><?=$schedule[1]->sch_class_name?></td>
                        </tr>
                    </table>
                    <br>
                </td>
            </tr>
            <tr>
                <td style="border-top:1px #999999 solid; padding:20px 0 20px 0;" colspan="2">
                No. Tempat Duduk : <?=$seatR?>
                </td>
            </tr>
            <tr>
                <td style="border-top:1px #999999 solid; padding-top:10px;" colspan="2">
                    
                    <table width="100%">
                        <tr>
                            <td width="200px;" height="25px;" align="left">Penumpang</td>
                            <td width="100px;" align="center">Jumlah</td>
                            <td width="200px;" align="right">Harga (Rp)</td>
                            <td width="200px;" align="right">Jumlah (Rp)</td>
                        </tr>
                        <tr>
                            <td height="25px;" align="left">Dewasa + Bayi</td>
                            <td align="center"><?=$dt_train[0]->dtt_adult+$dt_train[0]->dtt_infant?></td>
                            <td align="right"><?=number_format($schedule[1]->sch_normal_price, 0, ",", ".")?></td>
                            <td align="right"><?=number_format($schedule[1]->sch_normal_price, 0, ",", ".")?></td>
                        </tr>
                        <tr>
                            <td height="25px;" align="left">Biaya Administrasi</td>
                            <td align="center"></td>
                            <td align="right"><?=number_format($schedule[1]->sch_extra_fee, 0, ",", ".")?></td>
                            <td align="right"><?=number_format($schedule[1]->sch_extra_fee, 0, ",", ".")?></td>
                        </tr>
                        <tr>
                            <td height="25px;" align="left">Diskon Channel</td>
                            <td align="center"></td>
                            <td align="right"><?=number_format($schedule[1]->sch_discount, 0, ",", ".")?></td>
                            <td align="right"><?=number_format($schedule[1]->sch_discount, 0, ",", ".")?></td>
                        </tr>
                        <tr>
                            <td height="25px;" style="padding-top:10px;" align="left" colspan="3">Harga Total</td>
                            <td style="padding-top:10px;" align="right"><?=number_format($schedule[1]->sch_ticket_price, 0, ",", ".")?></td>
                        </tr>
                    </table>
                    
                </td>
            </tr>
        </table>
        
	</div>
    
</page>
<?php endif; ?>