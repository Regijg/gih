<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Hotel Voucher</title>
</head>
<body>
	
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr valign="middle">
            <td style="width:370px;"><br><p style="font-size:16px; padding-left:20px;">Itinerary : <b># <?=$booking[0]->itinerary_id?></b></p><br></td>
            <td style="width:370px;" align="right">
                &nbsp;
            </td>
        </tr>
        <tr valign="middle">
            <td style="border-top:0.7px dotted #CCCCCC; border-right:0.7px dotted #CCCCCC; border-bottom:0.7px dotted #CCCCCC;background-color:#FEFEFC;">
                
               <table style="width:740px; margin:10px 0 20px 10px;">
                    <tr>
                        <td width="100" style="height:15px;">Nama Tamu</td><td><b><?=$booking[0]->nama_pemesan?></b></td>
                    </tr>
                    <tr>
                        <td style="height:15px;">No Telepon</td><td><b><?=$booking[0]->phone_pemesan?></b></td>
                    </tr>
                    <tr>
                        <td style="height:15px;">Email</td><td><b><?=$booking[0]->email_pemesan?></b></td>
                    </tr>
                </table>
                
            </td>
            <td width="50%" style="background-color:#FEFEFC;border-top:0.7px dotted #CCCCCC;border-bottom:0.7px dotted #CCCCCC;" align="center">
                
                  <p>Reservation and payment by <br /><img src="<?=$this->config->item('path_static');?>/img/expedia.png" align="absmiddle" /></p>
                
            </td>
        </tr>
        <tr valign="middle">
            <td colspan="2" style="padding:0 10px 0 10px;">
                <br><br>
                <table style="width:100%; margin-top:10px; border:0.7px #CCCCCC solid; border-radius:3px;">
                    <tr>
                        <td width="25%" style="height:30px; padding:0 0 0 5px;border-bottom:0.7px #CCCCCC dotted;">Nama Hotel</td>
                        <td width="25%" style="border-bottom:0.7px #CCCCCC dotted;border-right:0.1px #CCCCCC dotted;"><b><?=$booking[0]->nama_hotel?></b></td>
                        <td width="25%" style="padding:0 0 0 5px;border-bottom:0.7px #CCCCCC dotted;">Kamar</td>
                        <td width="25%" style="border-bottom:0.7px #CCCCCC dotted;"><b><?=$booking[0]->jumlah_kamar?></b></td>
                    </tr>
                    <tr>
                        <td width="100" style="height:30px; padding:0 0 0 5px;border-bottom:0.7px #CCCCCC dotted;">Alamat</td>
                        <td width="247" style="border-bottom:0.7px #CCCCCC dotted;border-right:0.1px #CCCCCC dotted;"><b><?=$booking[0]->alamat_hotel?></b></td>
                        <td width="100" style="padding:0 0 0 5px;border-bottom:0.7px #CCCCCC dotted;">Tipe Kamar</td>
                        <td width="247" style="border-bottom:0.7px #CCCCCC dotted;"><b><?=$booking[0]->tipe_kamar?></b></td>
                    </tr>
                    <tr>
                        <td width="100" style="height:30px; padding:0 0 0 5px;border-bottom:0.7px #CCCCCC dotted;">Check In</td>
                        <td width="247" style="border-bottom:0.7px #CCCCCC dotted;border-right:0.1px #CCCCCC dotted;"><b><?=$booking[0]->tanggal_cekin?></b></td>
                        <td width="100" valign="top" style="padding:0 0 0 5px;" rowspan="2">Permintaan</td>
                        <td width="247" valign="top" rowspan="2"><?=$detail[0]->special_req?></td>
                    </tr>
                    <tr>
                        <td style="height:30px; padding:0 0 0 5px;">Check Out</td>
                        <td style="border-right:0.1px #CCCCCC dotted;"><b><?=$booking[0]->tanggal_cekout?></b></td>
                    </tr>
                </table>
                <br><br>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding:10px 10px 10px 10px;">
                
                <table cellpadding="0" cellspacing="0" style="width:740px; font-size:10px; background-color:#FFFFFB; border-radius:3px; padding:5px 10px 5px 10px; border:0.5px dashed #F9F9F9">
                <tr valign="middle"><td colspan="2" style="height:30px;">SYARAT & KETENTUAN HOTEL</td></tr>
                <tr><td>Harga TIDAK termasuk biaya layanan hotel yang berlaku, biaya untuk pengeluaran opsional (seperti makanan kecil di minibar atau panggilan telepon), atau biaya tambahan wajib. Hotel akan mengkalkulasi pengeluaran ini, biaya, dan biaya tambahan saat check-out.</td></tr>
                <?php if($booking[0]->cancel_policy!=""): ?>
                    <tr>
                        <td width="200px">
                            <bR /><p>KETENTUAN PEMBATALAN : </p>
                            <div style="width:700px"><?=$booking[0]->cancel_policy?></div>
                        </td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <td colspan="2" align="center">
                        <br>Untuk informasi lebih lanjut hubungi kami di:<br /><br>
                    </td>
                </tr>
                </table>
                
            </td>
        </tr>
    </table>
	
</body>
</html>