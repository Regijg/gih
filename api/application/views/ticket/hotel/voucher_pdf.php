	
	<page style="font-family:Arial, Helvetica, sans-serif; font-size:11px" pagegroup="new">
    	<div style="border:0.7px solid #EEE; padding:1px 1px 1px 1px;">
        
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr valign="middle">
                    <td style="width:370px;">
                    	<img src="<?=$this->config->item('path_static');?>/img/expedia.png" align="absmiddle" height="25" style="margin:5px 0 5px 10px"/>
                   </td>
                    <td style="width:370px;" align="right">
                    	&nbsp;
                    </td>
                </tr>
                <tr valign="middle">
                    <td colspan="2" style="border-top:0.7px dotted #CCCCCC; border-bottom:0.7px dotted #CCCCCC;background-color:#FEFEFC;">
                        
                        <p style="font-size:16px; margin:15px 0 0 10px;"><b><?=$booking[0]->nama_hotel?></b></p>
                        <p style="margin:2px 0 10px 10px; font-size:12px;"><?=$this->converter->setTanggalNama($booking[0]->tanggal_cekin)?> - <?=$this->converter->setTanggalNama($booking[0]->tanggal_cekout)?> | Itinerary : <b># <?=$booking[0]->itinerary_id?></b></p><br />
                        
                    </td>
                </tr>
                <tr valign="middle">
            		<td colspan="2" style="padding:0 10px 0 10px;">
                    	
                    	<table style="width:740px; margin-top:10px; border:0.7px #CCCCCC solid; border-radius:3px;">
                    		<tr>
                            	<td width="100" rowspan="5" style="border-right:0.1px #CCCCCC dotted;">
                                	<img src="<?=$booking[0]->img_hotel?>" style="height:75px; width:75px; margin-left:10px;">
                                </td>
                                <td width="100" style="height:30px; padding:0 0 0 5px;border-bottom:0.7px #CCCCCC dotted;">Nama Hotel</td>
                                <td width="510" style="border-bottom:0.7px #CCCCCC dotted;"><b><?=$booking[0]->nama_hotel?></b></td>
                            </tr>
                            <tr>
                                <td width="100" style="height:30px; padding:0 0 0 5px;border-bottom:0.7px #CCCCCC dotted;">Alamat</td>
                                <td width="510" style="border-bottom:0.7px #CCCCCC dotted;"><b><?=$booking[0]->alamat_hotel?></b></td>
                            </tr>
                            <tr>
                                <td width="100" style="height:30px; padding:0 0 0 5px;border-bottom:0.7px #CCCCCC dotted;">Check In</td>
                                <td width="510" style="border-bottom:0.7px #CCCCCC dotted;"><b><?=$this->converter->setTanggalNama($booking[0]->tanggal_cekin)?></b></td>
                            </tr>
                            <tr>
                                <td width="100" style="height:30px; padding:0 0 0 5px;border-bottom:0.7px #CCCCCC dotted;">Check Out</td>
                                <td width="510" style="border-bottom:0.7px #CCCCCC dotted;"><b><?=$this->converter->setTanggalNama($booking[0]->tanggal_cekout)?></b></td>
                            </tr>
                            <tr>
                                <td width="100" style="height:30px; padding:0 0 0 5px;">Kamar</td>
                                <td width="510"><b><?=$booking[0]->jumlah_kamar?> <?=$booking[0]->tipe_kamar?></b></td>
                            </tr>
                        </table>
                  		<br><br>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding:10px 10px 10px 10px;">
                        
                        <table cellpadding="0" cellspacing="0" style="width:740px; font-size:10px; background-color:#FFFFFB; border-radius:3px; padding:5px 10px 5px 10px; border:0.5px dashed #F9F9F9">
                        <tr valign="middle"><td colspan="2" style="height:30px;">SYARAT & KETENTUAN HOTEL</td></tr>
                        <tr><td>Harga TIDAK termasuk biaya layanan hotel yang berlaku, biaya untuk pengeluaran opsional (seperti makanan kecil di minibar atau panggilan telepon), atau biaya tambahan wajib. Hotel akan mengkalkulasi pengeluaran ini, biaya, dan biaya tambahan saat check-out.</td></tr>
                        <?php if($booking[0]->cancel_policy!=""): ?>
                        	<tr>
                            	<td width="200px">
                                	<bR /><p>KETENTUAN PEMBATALAN : </p>
                            		<div style="width:700px"><?=$booking[0]->cancel_policy?></div>
                                </td>
                            </tr>
						<?php endif; ?>
                        <tr>
                            <td colspan="2" align="center">
                            	<?php
									/*$profile_data	= $this->mgeneral->getWhere(array('type_info'=>profile),"mstr_konten");
									$dataList		= json_decode($profile_data[0]->info_konten,true);
									#print_r($dataList);*/
								?>
                                <br>Untuk informasi lebih lanjut hubungi kami di :<br />
								<?=$dataList['alamat']?><br />
                                Email : <?=$dataList['email']?> - Phone : <?=$dataList['phone']?><br>
                            </td>
                        </tr>
                        </table>
                        
                    </td>
                </tr>
            </table>
            
        </div>
    </page>