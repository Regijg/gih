<tr>
    <td colspan="2" align="left">
        <p>
            - YOUR AIRLINE TICKET IS ELECTRONICALLY STORED IN OUR SYSTEM AND IS SUBJECT TO CONDITION OF CONTRACT.
            <br> - PLEASE BRING THIS RECEIPT AND YOUR IDENTITY CARD ON YOUR TRAVEL IN CASE REQUIRED BY AIRPORT/CHECK-IN COUNTER/CUSTOMS AND IMMIGRATION OFFICIALS AS PROOF OF PURCHASE.
            <br> - THE FARE ABOVE IS SUBJECT TO THE APPLICABLE CONDITIONS.
            <br> - CHECK-IN COUNTERS WILL BE CLOSED 45 MINUTES PRIOR TO DEPARTURE. YOU HAVE TO BE AT THE BOARDING GATE AT LEAST 30 MINUTES BEFORE FLIGHT DEPARTS OR WE WILL LEAVE WITHOUT YOU TO AVOID UNNECESSARY DELAYS.
            <br>
            <br> NOTICE :
            <br> - CARRIAGE AND OTHER SERVICES PROVIDED BY THE CARRIER ARE SUBJECT TO THE CONDITIONS OF CARRIAGE WHICH ARE HEREBY INCORPORATED BY REFERENCE. THESE CONDITIONS MAY BE OBTAINED FROM THE ISSUING CARRIER
            <br>
            <br> DANGEROUS GOODS
            <br> - FOR SAFETY REASON. DANGEROUS ARTICLES SUCH AS COMPRESSED GASES/ FLAMMABLE /NON FLAMMABLE/POISONOUS /CORROSIVES /ACIDS /ALKALIS AND WET CELL BATTERIES/ETILOGIC AGENTS /BACTERIA/VIRUSES ETC/ EXPLOSIVES MUNITIONS/FIREWORKS /FLARES /RADIO ACTIVE /OXIDIZING MATERIALS OR OTHER DANGEROUS GOODS ARTICLE MUST NOT BE CARRIED IN PASSENGERS BAGGAGE
            <br>
        </p>
    </td>
</tr>
<tr>
    <td colspan="2">
        <h3>ELECTRONIC TICKET RECEIPT</h3></td>
</tr>
<tr>
    <td colspan="2">CONDITIONS OF CONTRACTS DOMESTIC REGULATION
        <br>
    </td>
</tr>
<tr>
    <td valign="top" width="10px">1.</td>
    <td>Carriage hereunder is subject to the regulations of the Air Transport Ordonantie of The Republic of Indonesia Stbl. 1939/100, the Law of the Republic of Indonesia No. 15, 1992 concerning Civil Aviation, the Government Regulation of the Republic of Indonesia No. 40, 1995 and to the applicable conditions of carriage,
        <br> tariffs, time tables (except the times of departure and arrival stated therein) and other regulations of the carrier, which form an inseparable part hereof and
        <br> which are available for inspection at the carrier's booking offices.
    </td>
</tr>
<tr>
    <td valign="top">2.</td>
    <td>
        This passenger's ticket is valid only for the person named hereon and is not transferable. The passenger agrees that the carrier reserves the right to check if necessary, whether this ticket is utilized by the rightful person. If anyone other than the person named on the ticket travels or endeavors to travel on this ticket, the carrier is entitled to refuse such transportation and the right of transportation on this ticket by the person entitled on the carriage will lapse.
    </td>
</tr>
<tr>
    <td valign="top">3. </td>
    <td>
        The carrier reserves the right to substitute other carriers for the execution of the contract and to alter agreed stopping places.
    </td>
</tr>
<tr>
    <td valign="top">4. </td>
    <td>
        Checked baggage carried hereunder will only be delivered to the passenger on production of the baggage claim tag.
    </td>
</tr>
<tr>
    <td valign="top">5. </td>
    <td>
        a. The carrier is liable for the damage and loss on the passenger's baggage subject to the Government Regulation of the Republic of Indonesia No. 40, 1995 and the conditions of the carriage of the carrier.
        <br> b. A baggage is regarded received in good order and condition by the passenger unless the passenger claims otherwise upon receipt of his/her baggage.
        <br> c. All claims are subject to proof of amount of actual loss. The liability for lost or damaged baggage is limited to IDR 100.000,- per kg.
        <br> d. The Carrier assumes no liability for fragile/ perishable articles and lives animals if carried as baggage.
        <br> e. The Carrier assumes no liability for valuable goods such as money, jewellery, electronic devices, medicines, documents and valuable papers etc.,
        <br> kept in the baggage.
    </td>
</tr>
<tr>
    <td valign="top">6. </td>
    <td>
        No agent, employee or representative of the carrier has authority to alter or waive either wholly or partly any provision of the applicable conditions of carriage, tariffs, time tables and other regulations of the carrier The passenger named hereon is insured with P.T Asuransi Kerugian Jasa Raharja in accordance with Act No. 33/1964 juncto its implementation regulations
    </td>
</tr>
