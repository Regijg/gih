<tr valign="top">
    <td width="50%">
        <ol>
            <li>Seluruh tarif penerbangan Indonesia AirAsia adalah untuk satu kali jalan dan belum termasuk: airport tax, asuransi dan biaya administrasi.</li>
            <li>Seluruh kursi yang sudah dibeli tidak dapat ditukarkan kembali.</li>
            <li>Indonesia AirAsia adalah perusahaan penerbangan dengan konsep biaya rendah dan penerbangan langsung (tanpa transit), sehingga tidak ada pelayanan penerbangan atau transfer bagasi. Apabila melakukan penerbangan lanjutan, maka penumpang bertanggung-jawab sepenuhnya terhadap pemesanan penerbangan lanjutan tersebut. Dengan alasan apapun Indonesia AirAsia tidak bertanggung-jawab terhadap terjadinya kegagalan dalam melaksanakan penerbangan lanjutan tersebut.</li>
            <li>Setiap kursi yang sudah dibeli berlaku untuk penumpang yang namanya tercantum didalam dokumen penerbangan dan hanya berlaku untuk tanggal dan nomor penerbangan yang tercantum di dalam dokumen penerbangan.</li>
            <li>Indonesia AirAsia saat ini adalah penerbangan tanpa menggunakan tiket. Yang diperlukan hanyalah nomor pemesanan atau cukup menyebutkan nama anda. Yang paling penting untuk diperhatikan adalah anda menunjukkan kartu identitas atau paspor pada waktu melapor di bandara (pada saat check-in dan pada saat masuk ke ruang tunggu keberangkatan).</li>
            <li>
                Check-in counter Indonesia AirAsia dibuka 2 (dua) jam dan ditutup 45 (empat puluh lima) menit sebelum jadwal keberangkatan.
                <ul style="list-style:lower-alpha">
                    <li>Penumpang diminta untuk melapor di check-in counter sekurang-kurangnya 2 (dua) jam sebelum jadwal keberangkatan dan berada di ruang tunggu keberangkatan 30 (tiga puluh) menit sebelum jadwal keberangkatan.</li>
                    <li>Penumpang yang melapor setelah check-in counter ditutup tidak diijinkan untuk naik ke pesawat dan kursi yang sudah dibeli dianggap hangus.</li>
                </ul>
            </li>
            <li>Setiap penumpang hanya diijinkan membawa 1 (satu) barang bawaan kedalam kabin pesawat (maksimum berat 7 kilogram dan maksimum ukuran 56cm x 36cm x 23cm). Setiap kelebihan barang bawaan harus dilaporkan ke check-in counter sebagai bagasi tercatat.</li>
            <li>Biaya bagasi adalah biaya yang hanya dikenakan untuk Bagasi Tercatat, dimana pembayarannya akan dikenai potongan harga apabila dilakukan bersamaan pada saat pemesanan kursi atau pembayaran dengan harga penuh apabila dilakukan di konter check-in bandara. Pembayaran pertama kali dilakukan untuk minimal 15 kg untuk setiap Bagasi Tercatat dan akan dikenakan tambahan biaya untuk setiap kelipatan 5 kg. Setiap penumpang dengan Bagasi Tercatat yang beratnya melebihi 15 kg atau yang beratnya melebihi dari biaya bagasi yang telah dibayarkan pada saat pemesanan kursi akan dikenakan biaya kembali per kg di konter check-in bandara.</li>
            <li>Indonesia AirAsia akan mengangkut penumpang dan bagasinya sesuai dengan tanggal dan waktu penerbangan yang telah dipesan oleh penumpang tetapi tidak menjamin ketepatan sepenuhnya. Indonesia AirAsia dapat melakukan perubahan tanpa pemberitahuan sebelumnya.</li>
            <li>Apabila terjadi keadaan di luar kemampuan yang menyebabkan terjadinya penundaan ataupun pembatalan penerbangan, Indonesia AirAsia akan berusaha memindahkan penumpang ke penerbangan lainnya dan biaya-biaya tambahan yang timbul menjadi tanggung-jawab penumpang sepenuhnya.</li>
            <li>Setiap penumpang dilarang membawa makanan dan minuman kedalam pesawat. Makanan dan minuman dengan harga yang terjangkau tersedia didalam pesawat untuk dijual.</li>
            <li>Pengaturan kursi Disesuaikan dengan ketersediaan kursi yang ada, Anda dapat membayar Permintaan Pengaturan Kursi Lebih Awal (Advance Seat Request/ASR) pada saat pemesanan untuk pengaturan kursi. Mohon merujuk pada informasi mengenai biaya untuk ASR. Ketika ASR telah dibeli, kami berhak untuk menentukan atau memindahkan kursi Anda kapanpun, bahkan ketika Anda telah memasuki pesawat. Hal ini dapat disebabkan karena alasan operasional, keselamatan atau keamanan. Kami tidak menjamin adanya pengaturan ulang kursi secara khusus, baik itu di gang kabin, dekat jendela, di jalur pintu keluar atau yang lainnya. Kami akan, bagaimanapun juga, berusaha untuk dapat memberikan kursi yang sesuai dengan pengaturan kursi yang telah Anda bayarkan.</li>
            <li>
                Setelah dilakukan pembelian kursi, tidak diijinkan untuk melakukan perubahan, kecuali:
                <ul style="list-style:lower-alpha">
                    <li>
                        Perubahan dilakukan lebih dari 48 (empat puluh delapan) jam sebelum jadwal keberangkatan:
                        <ul style="list-style:decimal">
                            <li>dikenakan biaya sebesar untuk setiap penumpang dan setiap rute penerbangan; serta</li>
                            <li>harus dibayarkan pada waktu perubahan tersebut dilakukan</li>
                        </ul>
                    </li>
                    <li>
                        Dalam kurun waktu 48 (empat puluh delapan) jam sebelum jadwal keberangkatan,
                        <ul style="list-style:decimal">
                            <li>tidak diijinkan untuk melakukan perubahan penerbangan; dan</li>
                            <li>hanya diijinkan untuk perubahan nama penumpang dengan membayar biaya perubahan nama minimum 6 (enam) jam sebelum jadwal keberangkatan.</li>
                        </ul>
                    </li>
                    <li>
                        Selisih tarif (hanya berlaku jika ada perubahan penerbangan).
                        <ul style="list-style:decimal">
                            <li>Jika pembelian kursi yang baru merupakan kursi dengan kelas yang lebih rendah dari tarif penerbangan sebelumnya, maka selisih tarif tidak akan dikembalikan.</li>
                            <li>Jika pembelian kursi yang baru merupakan kursi dengan kelas yang lebih tinggi dari tarif penerbangan sebelumnya maka selisih tarif harus dibayar oleh penumpang.</li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>Setiap penumpang yang membutuhkan bantuan khusus medis harus melaporkan kepada Indonesia AirAsia pada waktu dilakukan pemesanan. Penerbangan Indonesia AirAsia tidak dilengkapi dengan peralatan khusus medis dan berhak menolak untuk mengangkut penumpang tersebut.</li>
            <li>
                Setiap penumpang yang mengandung (ibu hamil) harus melapor kepada Indonesia AirAsia pada waktu dilakukan pemesanan dan pada waktu melapor di check-in counter.
                <ul style=" list-style:lower-alpha">
                    <li>Usia kehamilan sampai dengan 28 (dua puluh delapan) minggu dapat diberangkatkan tanpa surat keterangan dokter dan penumpang tersebut diwajibkan untuk menandatangani Pernyataan Tanggung Jawab Terbatas (Limited Liability Statement).</li>
                    <li>Usia kehamilan antara 28 (dua puluh delapan) sampai dengan 34 (tiga puluh empat) minggu dapat diberangkatkan dengan surat keterangan dokter yang masa berlakunya tidak kurang dari 7 (tujuh) hari dari tanggal keberangkatan penumpang, yang menyatakan bahwa penumpang tersebut dapat melakukan penerbangan dan penumpang tersebut diwajibkan untuk menandatangani Pernyataan Tanggung Jawab Terbatar Limited Liability Statement).</li>
                    <li>Indonesia Airasia tidak dapat memberangkatkan penumpang dengan usia kehamilan 35 (tiga puluh lima) minggu atau lebih.</li>
                </ul>
            </li>
            <li>Indonesia AirAsia tidak dapat memberangkatkan penumpang bayi dengan usia kurang dari 8 (delapan) hari kecuali ada surat keterangan dokter.</li>
            <li>Setiap penerbangan Indonesia AirAsia adalah penerbangan bebas asap rokok.</li>
            <li>Setiap tarif, jadwal dan rute penerbangan adalah yang berlaku pada saat diumumkan. Indonesia AirAsia berhak untuk melakukan perubahan syarat-syarat dan ketentuan umum, tarif dan jadwal penerbangan tanpa pemberitahuan sebelumnya.</li>
            <li>Terdapat beberapa barang atau benda ketika bepergian menggunakan transportasi udara yang dapat menimbulkan gangguan pada kesehatan, keselamatan, barang dan lingkungan, oleh karena itu demi keselamatan tamu kami, karyawan dan barang, barang-barang tersebut dilarang untuk dibawa baik sebagai bagasi kabin maupun sebagai check-in bagasi. Jika anda menginginkan untuk membawa barang-barang yang dimaksud, mohon menghubungi Indonesia AirAsia untuk bantuan lebih lanjut.</li>
            <li>Berpedoman pada standard keselamatan internasional yang diterapkan oleh seluruh maskapai penerbangan, terdapat beberapa barang yang dikategorikan sebagai Barang Berbahaya yang dilarang untuk dibawa ke dalam kabin pesawat tetapi harus dimasukkan sebagai check-in bagasi. Daftar Barang Berbahaya dapat ditemukan di check-in counter kami, mohon menghubungi karyawan kami jika anda memiliki barang-barang yang dimaksud sebagai bagasi kabin anda. Apabila anda membawa Barang Berbahaya di dalam bagasi kabin anda, barang tersebut akan disita oleh pihak Keamanan Bandara. Indonesia AirAsia tidak akan bertanggungjawab terhadap barang-barang yang disita oleh pihak Keamanan Bandara.</li>
            <li>
                Berdasarkan kebijksanaan perusahaan kami, barang-barang di bawah ini juga dilarang untuk dibawa, baik sebagai bagasi kabin maupun check-in bagasi:
                <ul style="list-style:lower-alpha">
                    <li>Tumbuhan Hidup</li>
                    <li>Buah-buahan</li>
                    <li>Senjata api dan amunisi</li>
                    <li>Organ, embrio atau contoh jaringan dari makhluk hidup</li>
                    <li>Binatang</li>
                    <li>Kursi roda yang dioperasikan dengan alat elektronik/baterai</li>
                    <li>Daftar ini belum sepenuhnya lengkap, oleh karena itu Indonesia AirAsia berhak setiap waktu menambah daftar barang-barang yang dilarang untuk dibawa jika kami rasa barang-barang tersebut menimbulkan resiko yang berbahaya.</li>
                </ul>
                Catatan: Mohon untuk merujuk pada Syarat-syarat dan Kondisi pada Pasal 8.1 mengenai Barang-barang yang tidak dapat diterima sebagai Bagasi atau dibawa didalam Bagasi. Mohon untuk meminta penjelasan mengenai Syarat-syarat dan Kondisi tersebut di bandara dan counter penjualan kami atau situs www.airasia.com.
            </li>
            <li>Indonesia AirAsia membebankan biaya untuk peralatan olah raga. Biaya tersebut adalah Rp 125.000,- (seratus dua puluh lima ribu rupiah) dengan berat maksimum 15(lima belas)kg. Setiap kelebihan beban akan dikenakan biaya kelebihan beban.</li>
            <li>
                Saat pemesanan telah dilakukan, Indonesia AirAsia dapat merubah jadwal dan/atau membatalkan, mengakhiri, mengalihkan, menunda penjadwalan ulang atau mengalami keterlambatan bila Indonesia AirAsia menilai alasan tersebut adalah wajar dikarenakan keadaan di luar pengendalian Indonesia AirAsia karena alasan-alasan keamanan atau komersial. Bilamana dalam keadaan pembatalan penerbangan ini, Indonesia AirAsia akan:
                <ul style="list-style:lower-alpha">
                    <li>Mengangkut penumpang pada kesempatan paling awal pada penerbangan terjadwal Indonesia AirAsia yang lain yang masih tersedia tempat tanpa biaya tambahan dan bilamana perlu memperpanjang masa berlaku pemesanan; atau</li>
                    <li>Bila penumpang memilih untuk bepergian di waktu lain, menyimpan nilai tarif penumpang dalam sebuah rekening kredit untuk perjalanan penumpang selanjutnya sejauh penumpang memesan dalam kurun waktu 3 (tiga) bulan setelahnya</li>
                </ul>
            </li>
            <li>Apabila terjadi ketidaksesuaian antara pernyataan di dalam Syarat &amp; Ketentuan Umum PT. Indonesia AirAsia dengan Syarat-Syarat Perjanjian Peraturan Dalam Negeri, maka yang menjadi acuan adalah Syarat-Syarat</li>
        </ol>
    </td>
    <td>
        <ol>
            <li>All Indonesia AirAsia fares are 1-way and excludes airport tax, insurance surcharge and administration fees.</li>
            <li>All bookings sold are non-refundable.</li>
            <li>Indonesia Indonesia AirAsia is a low cost point-to-point airline which does not provide flight connections or baggage transfer. It is your responsibility when making bookings to allow time for baggage collection and re-check. We shall not be liable for your failure to meet any connecting flights.</li>
            <li>Each booking sold is valid for the passenger named, and valid for travel on the date and flight as specified.</li>
            <li>Indonesia AirAsia now is a ticketless airline. All you need is a booking number or mention your name. Most importantly, bring along your identification card or passport for Check-In purposes.</li>
            <li>
                Indonesia AirAsias check-in counters are open 2 (two) hours before and closes 45 (forty five) minutes before the scheduled departure.
                <ul style="list-style:lower-alpha">
                    <li>Passengers are advised to check-in at least 2 (two) hours in advance, and be at the boarding gate 30 (thirty) minutes prior to the departure time.</li>
                    <li>Passengers who present themselves after check-in has closed will not be allowed to board the flight and will forfeit their booking.</li>
                </ul>
            </li>
            <li>Each passenger is allowed 1 (one) Unchecked Hand Luggage (max. weight 7kgs and max. size 56cm x 36cm x 23cm) into the cabin. Any excess has to be checked in as excess baggage.</li>
            <li>A baggage fee is charged for the carriage of Checked Baggage, which will be charged at a discounted rate if purchased at time of booking or at a full rate at the Airport Check-in counters. A minimum of 15kg of Checked Baggage may be purchased at first instance and then in increments of 5kg. Any passenger checking in baggage which exceeds 15kgs or the amount purchased at time of booking will be charged on a per kg basis at the Airport Check-in counters.</li>
            <li>Indonesia AirAsia will try to carry you and your Baggage in accordance with the date and time of the flight(s) specified, but does not guarantee it will be able to do so. Schedules may change without notice.</li>
            <li>Where due to circumstances beyond our control and the flight is delayed or cancelled, wheter you have checked in or not, Indonesia AirAsia will try to assist you to get your destination but will not be responsible for paying any cost or expenses you may incur as a result of the delay or cancellation.</li>
            <li>Passengers are not allowed to consume their own food on board. Reasonably priced snacks and refreshments are available for purchase on board.</li>
            <li>Seat assignment - Subject to availability you may pay a fee for an advance seat request (ASR) at the time of reservation for an advance seat assignment. Please refer to our fee schedule for the fee for an ASR. Where an ASR is purchased, we reserve our right to assign or reassign seats at any time, even after boarding of the aircraft. This may be necessary for operational, safety or security reasons. We do not guarantee any specific seat reassignments, whether for an aisle, window, exit row, or other type of seat. We will, however, make reasonable efforts to honour paid seat assignments.</li>
            <li>
                Once booking is paid for, changes are not allowed. Unless the following rules apply:
                <ul style="list-style:lower-alpha">
                    <li>
                        More than 48 (forty eight) hours before the original scheduled time of departure
                        <ul style="list-style:decimal">
                            <li>A change fee will be imposed per passenger per sector</li>
                            <li>The change fee must be paid immediately upon changes made.</li>
                        </ul>
                    </li>
                    <li>
                        Within 48 (forty eight) hours before the original scheduled time of departure
                        <ul style="list-style:decimal">
                            <li>Flight changes are not allowed</li>
                            <li>Name change is allowed with a name change fee per passenger outside six (6) hours prior to scheduled departure</li>
                        </ul>
                    </li>
                    <li>
                        Difference of airfare (applies for flight changes only)
                        <ul style="list-style:decimal">
                            <li>If the new booking is in a lower fare class than the original booking, the difference will not be refunded</li>
                            <li>If the new booking is in a higher fare class than the original booking, the fare difference must be paid.</li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>Passengers who require special medical assistance must advise Indonesia AirAsia at the point of booking. Indonesia AirAsia is not equipped for medical evacuation and reserves the right to decline such passengers.</li>
            <li>
                It is the responsibility of all pregnant women to advise Indonesia AirAsia of their condition at the point of booking and at the check-in counter
                <ul style=" list-style:lower-alpha">
                    <li>Pregnancy up to 27 weeks (inclusive): we will accept without a doctors certificate subject to the passenger signing a Limited Liability Statement.</li>
                    <li>Pregnancy between 28 weeks to 34 weeks (inclusive): passenger shall produce a doctors certificate confirming the number of weeks of pregnancy and that she is fit to travel which certificate shall have been issued not more than seven (7) days before the scheduled flight departure date. The passenger will be required to sign a Limited Liability Statement.</li>
                    <li>35 (thirty five) weeks and over Indonesia AirAsia will not carry women during this period of pregnancy under any circumstances.</li>
                </ul>
            </li>
            <li>Infants aged less than 8 (eight) days will not be permitted to travel on board Indonesia AirAsia unless with a doctors certification.</li>
            <li>No smoking is permitted on any of our flights.</li>
            <li>All fares, flight schedules and routes published are correct at the time of printing. Indonesia AirAsia reserves the right to revise any part of the terms and conditions, fare and flight schedules without notice.</li>
            <li>There are certain articles or substances which when transported by air, are capable of posing a significant risk to health, safety, property and environment, therefore without risking the safety of our guests, staff and property, these items are prohibited either as unchecked baggage or checked baggage. If you are intending to carry any items that are out of the ordinary, kindly contact Indonesia AirAsia for further assistance.</li>
            <li>In line with international safety standards as practised by all airlines, there are some items that are classfied as Risk Items which are prohibited into the aircraft cabin and must be checked-in. A display of the Risk Items is available at our check-in counters, kindly advise our staff if you have such items in your unchecked bagggae. Any risk items that are carried in your unchecked baggage will be confiscated by the Airport Authorities. Indonesia AirAsia will not accept any responsibility nor liability for any items confiscated by the Airport Authorities.</li>
            <li>
                As per our company policy, the following items are also prohibited, as unchecked or as checked baggage:
                <ul style="list-style:lower-alpha">
                    <li>Live Plants</li>
                    <li>Fruits</li>
                    <li>Firearms and ammunition</li>
                    <li>Organs, embryos or tissue samples</li>
                    <li>Pets</li>
                    <li>Elecrical/battery operated wheelchairs</li>
                    <li>This list is not exhaustive and Indonesia AirAsia may at anytime add more prohibited items if we feel its carriage can pose a significant risk</li>
                </ul>
                Note: Please refer to our full Terms and Conditions Article 8.1 under Items Unacceptable as Baggage or to be Carried Inside Baggage. Kindly request for such Terms and Conditions from any of our airport and sales office counters or log on to www.airasia.com
            </li>
            <li>Indonesia AirAsia has a sports equipment fee. We have a fixed rate of Rp 125.000,-(one hundred and twenty five thousand rupiah) for maximum 15(fifteen)kilos. For over and above 15(fifteen)kilos, an excess baggage fee shall be applied.</li>
            <li>
                At any time after a booking has been made, Indonesia AirAsia may change the schedule and/or cancel, terminate, divert, postpone reschedule or delay any flight where we reasonably consider this to be justified by circumstances beyond our control or for reasons of safety or commercial reasons. In the event of such flight cancellation, Indonesia AirAsia shall at our option, either:
                <ul style="list-style:lower-alpha">
                    <li>carry you at the earliest opportunity on another of our scheduled services on which space is available without additional charge and, where necessary, extend the validity of your booking; or</li>
                    <li>should you choose to travel at another time, retain the value of your fare in a credit account for your future travel provided that you must re-book within three (3) months therefrom.</li>
                </ul>
            </li>
            <li>In the event of inconsistencies between the provisions of PT. Indonesia AirAsia General Terms and Conditions and Conditions of Contract Domestic Regulation, Conditions of Contract Domestic Regulation shall prevail.</li>
        </ol>
    </td>
</tr>
<tr valign="top">
    <td>
        <h4>SYARAT-SYARAT PERJANJIAN PERATURAN DALAM NEGERI</h4>
        <ol>
            <li>Perjanjian Pengangkutan ini tunduk kepada ketentuan-ketentuan Ordonansi Pengangkutan Udara Indonesia (Stbl. 1939/100), Undang-Undang Republik Indonesia No.15 Tahun 1992 tentang Penerbangan juncto Peraturan Pemerintah Republik Indonesia No. 40 tahun 1995 serta kepada persyaratan yang berlaku mengenai Pengangkutan, tarif, peraturan dinas (kecuali waktu keberangkatan dan waktu kedatangan yang tersebut didalamnya) dan peraturan lain dari pengangkut, yang merupakan bagian yang tidak dapat dipisahkan dari perjanjian ini dan yang dapat diperiksa di kantor pemesanan pengangkut.</li>
            <li>Tiket penumpang ini hanya dapat dipergunakan oleh orang yang namanya tertera didalamnya dan tidak dapat dipergunakan oleh orang lain. Penumpang menyetujui bahwa apabila diperlukan pengangkut dapat memeriksa apakah tiket dipergunakan oleh yang berhak. Jika tiket ini dipergunakan atau dicoba untuk dipergunakan oleh seseorang yang lain daripada yang namanya tercantum di dalam tiket ini, maka pengangkut berhak untuk menolak untuk memgangkut orang tersebut serta hak pengangkutan dengan tiket ini oleh yang berhak menjadi batal.</li>
            <li>Pengangkut berhak untuk menyerahkan penyelenggaraan perjanjian pengangkutan ini kepada perusahaan pengangkutan lain dan untuk mengubah tempat-tempat perhentian yang telah disetujui.</li>
            <li>Bagasi tercatat yang diangkut berdasarkan perjanjian ini, hanya akan diserahkan kepada penumpang yang jika tanda bukti klaim bagasinya dikembalikan kepada pengangkut.</li>
            <li>
                Pengangkut bertanggung jawab atas kerusakan dan kerugian-kerugian yang timbul pada bagasi penumpang berdasarkan pada syarat-syarat dan batas-batas yang ditentukan di dalam Peraturan Pemerintah Republik Indonesia No. 40 tahun 1995 dan syarat-syarat umum pengangkutan dari pengangkut.
                <ul style="list-style:lower-alpha">
                    <li>Bagasi dianggap telah diterima dalam kondisi dan keadaan yang lengkap dan baik pada saat penerimaan kecuali apabila penumpang mengajukan protes pada saat penerimaan barang tersebut.</li>
                    <li>Semua tuntutan ganti kerugian harus dapat dibuktikan besarnya kerugian yang secara nyata diderita. Tanggung jawab terbatas untuk kehilangan dan kerusakan bagasi ditetapkan sejumlah setinggi-tingginya IDR 100.000,- (seratus ribu rupiah) per kilogram.</li>
                    <li>Pengangkut udara tidak bertanggung jawab terhadap kerusakan barang-barang pecah belah/cepat busuk dan binatang hidup jika diangkut sebagai bagasi.</li>
                    <li>Pengangkut udara tidak bertanggung jawab terhadap barang-barang berharga seperti uang, perhiasan, barang elektronik, obat-obatan, dokumen serta surat berharga atau sejenisnya jika dimasukkan kedalam bagasi.</li>
                </ul>
            </li>
            <li>Tidak ada agen, pegawai atau wakil pengangkut yang memiliki hak untuk mengubah atau membatalkan syarat-syarat pengangkutan, tarif, jadwal dan peraturan lain dari pengangkut yang berlaku, baik sebagian maupun seluruhnya. Penumpang yang namanya tercantum di dalam tiket ini diasuransikan pada PT. Asuransi Kerugian Jasa Raharja berdasarkan Undang-Undang No. 33/1964 juncto peraturan-peraturan pelaksanaannya.</li>
        </ol>
    </td>
    <td>
        <h4>CONDITIONS OF CONTRACT DOMESTIC REGULATION</h4>
        <ol>
            <li>Carriage hereunder is subject to the regulations of the Air Transport Ordonantie of The Republic of Indonesia Stbl. 1939/100, the law of the Republic of Indonesia No. 15, 1992 concerning Civil Aviation, the Government Regulation of the Republic of Indonesia No. 40, 1995 and to the applicable conditions of carriage, tariffs, time tables (except the times of departure and arrival stated therein) and other regulations of the carrier, which form an inseparable part hereof and which are available for inspection at the carriers booking offices.</li>
            <li>This passengers ticket is valid only for the person named hereon and is not transferable. The passenger agrees that the carrier reserves the right to check if necessary, whether this ticket is utilized by the rightful person. If anyone other than the person named on the ticket travels or endeavors to travel on this ticket, the carrier is entitled to refuse such transportation and the right of transportation on this ticket by the person entitled on the carriage will lapse.</li>
            <li>The carrier reserves the right to substitute other carriers for the execution of the contract and to alter agreed stopping places.</li>
            <li>Checked baggage carried hereunder will only be delivered to the passenger on production of the baggage claim tag.</li>
            <li>
                The carrier is liable for the damage and loss on the passengers baggage subject to the Government Regulation of the Republic of Indonesia No. 40, 1995 and the conditions of the carriage of the carrier.
                <ul style="list-style:lower-alpha">
                    <li>A baggage is regarded received in good order and condition by the passenger unless the passenger claims otherwise upon receipt of his/her baggage.</li>
                    <li>All claims are subject to proof of amount of actual loss. The liability for lost or damaged baggage is limited to IDR 100.000 (one hundred thousand rupiahs) per kilogram.</li>
                    <li>The carriage assumes no liability for fragile/perishable articles and live animals if carried as baggage.</li>
                    <li>The carrier assumes no liability for valuable goods such as money, jewellery, electronic device, medicines, documents and valuable papers etc, kept in the baggage.</li>
                </ul>
            </li>
            <li>No agent, employee or representative of the carrier has authority to alter or waive either wholly or partly any provision of the applicable conditions of carriage, tariffs, time tables and other regulations of the carrier.</li>
        </ol>
    </td>
</tr>
