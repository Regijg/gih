<tr>
    <td>Persyaratan Tiket Citilink:</td>
</tr>
<tr>
    <td>1. Tiket Citilink tidak dapat diuangkan kembali.</td>
</tr>
<tr>
    <td>2. Perubahan jadwal dan nama dikenakan biaya dengan ketentuan yang berlaku.</td>
</tr>
<tr>
    <td>
        <br>Bagasi:</td>
</tr>
<tr>
    <td>1. Satu buah tas tangan maksimal berat 5 kg dan atau ukuran 50x36x15cm.</td>
</tr>
<tr>
    <td>2. Biaya Bagasi Rp. 5.000 per kg.</td>
</tr>
<tr>
    <td>
        <br>Lapor Diri di Bandara:</td>
</tr>
<tr>
    <td>1. Check in dibuka 2(dua) jam dan ditutup 30 menit sebelum keberangkatan.</td>
</tr>
<tr>
    <td>2. Penumpang yang datang kurang dari 30 menit tidak akan diterima dan dengan demikian kehilangan hak atas kursinya-tanpa pengembalian uang.</td>
</tr>
<tr>
    <td>3. Wajib menunjukan kartu identitas untuk keperluan identifikasi di meja Check-in.</td>
</tr>
<tr>
    <td>
        <br>Catatan:</td>
</tr>
<tr>
    <td>Dokumen lengkap Persyaratan Pengangkutan Penumpang dan Bagasi tersedia di www.citilink.co.id</td>
</tr>
