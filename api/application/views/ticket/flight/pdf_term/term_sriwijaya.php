<tr valign="top">
    <td style="width:5px;">1.</td>
    <td style="width:690px;" align="left">Tiket anda tersimpan di system reservasi Sriwijaya Air dan ini adalah data tiket elektronik anda.
        <br>Your airline ticket is electronically stored in Sriwijaya Air computer reservation system and is your record of your electronic ticket.
    </td>
</tr>
<tr valign="top">
    <td>2.</td>
    <td align="left">Anda harus menunjukan tiket elektronik dan tanda pengenal anda di check-in counter dan untuk masuk ke Airport dan / atau untuk membuktikan perjalanan kembali
        <br>atau perjalanan lanjutan kepada petugas bea cukai dan imigrasi.
        <br>You should be needed to show this electronic ticket and identity card at check-in counter and to enter the airport and / or to prove return or onward travel to
        <br>customs and immigration officials.
    </td>
</tr>
<tr valign="top">
    <td>3.</td>
    <td align="left">Pengangkut dan layanan lain yang disediakan oleh perusahaan penerbangan tunduk pada syarat syarat pengangkutan / syarat syarat perjanjian peraturan dalam negeri
        <br>yang dalam hal ini digabung sesuai referensi kondisi ini dapat diperoleh dari perusahaan penerbangan penerbit tiket.
        <br>Carriage and other services provided by the carrier are subject to the conditions of carriage/condition of contract which are hereby incorporated by reference.
        <br>These conditions may be obtained from the issuing carrier.
    </td>
</tr>
<tr valign="top">
    <td>4.</td>
    <td align="left">Tarif berlaku dengan ketentuan dan kondisi tarif yang menyertainya.
        <br>Fares are issued subject to conditions apply.</td>
</tr>
<tr valign="top">
    <td>5.</td>
    <td align="left">Dimohon melapor 1½ jam sebelum keberangkatan.
        <br>Please check in 1½ prior to flight departure</td>
</tr>
<tr valign="top">
    <td>6.</td>
    <td align="left">
        Anda harus melapor di boarding gate paling lambat 30 menit sebelum pesawat anda berangkat atau kami akan meninggalkan anda untuk menghindari keterlambatan
        <br>jadwal penerbangan.
        <br> Please be at the boarding gate at least 30 minutes before your flight departs or we will leave without you to avoid unnecessary delays.
    </td>
</tr>
<tr valign="top">
    <td>7.</td>
    <td align="left">
        Barang barang berbahaya untuk alasan keselamatan penerbangan benda benda berbahaya seperti gas terkompresi ( benda mudah terbakar dan beracun ),
        <br>benda yang dapat menimbulkan karat ( asam, alkalis dan aki ) etiologic agents ( bakteri, virus dll ), bahan peledak ( munitions, kembang api, flares ) radio aktif
        <br>dan oxidizing material dan material berbahaya lainnya tidak diperbolehkan dibawa dalam kabin bagasi penumpang.
        <br> Dangerous Goods for safety reason, dangerous articles such as compressed gases ( Flammable, Non-Flammable and Poisonous ), Corrosives
        <br>( Acids, Alkalis and wet cell batteries ), Etiologic Agents ( Bacteria, Viruses Etc ), Explosives ( Munitions, Fireworks, Flares ) , Radioactive and oxidizing materials
        <br>or other dangerous goods article must not be carried in passengers baggage.
    </td>
</tr>
<tr valign="top">
    <td colspan="2" align="center">
        <br /> PT. Sriwijaya Air
        <br>Jl. Gunung Sahari Raya No. 13 Blok B8-10
        <br>Jakarta Utara, Indonesia
    </td>
</tr>
