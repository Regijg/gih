<tr valign="top">
    <td style="width: 340px;">
        <p><b>SYARAT-SYARAT PERJANJIAN PERATURAN DALAM NEGERI</b></p>
        <ol>
            <li>Perjanjian Pengangkutan ini tunduk kepada ketentuan-ketentuan Ordonansi Pengangkutan Udara Indonesia (Stbl. 1939/100), Undang-Undang Republik Indonesia No.15 Tahun 1992 tentang Penerbangan juncto Peraturan Pemerintah Republik Indonesia No. 40 tahun 1995 serta kepada persyaratan yang berlaku mengenai Pengangkutan, tarif, peraturan dinas (kecuali waktu keberangkatan dan waktu kedatangan yang tersebut didalamnya) dan peraturan lain dari pengangkut, yang merupakan bagian yang tidak dapat dipisahkan dari perjanjian ini dan yang dapat diperiksa di kantor pemesanan pengangkut.</li>
            <li>Tiket penumpang ini hanya dapat dipergunakan oleh orang yang namanya tertera didalamnya dan tidak dapat dipergunakan oleh orang lain. Penumpang menyetujui bahwa apabila diperlukan pengangkut dapat memeriksa apakah tiket dipergunakan oleh yang berhak. Jika tiket ini dipergunakan atau dicoba untuk dipergunakan oleh seseorang yang lain daripada yang namanya tercantum di dalam tiket ini, maka pengangkut berhak untuk menolak untuk memgangkut orang tersebut serta hak pengangkutan dengan tiket ini oleh yang berhak menjadi batal.</li>
            <li>Pengangkut berhak untuk menyerahkan penyelenggaraan perjanjian pengangkutan ini kepada perusahaan pengangkutan lain dan untuk mengubah tempat-tempat perhentian yang telah disetujui.</li>
            <li>Bagasi tercatat yang diangkut berdasarkan perjanjian ini, hanya akan diserahkan kepada penumpang yang jika tanda bukti klaim bagasinya dikembalikan kepada pengangkut.</li>
            <li>
                Pengangkut bertanggung jawab atas kerusakan dan kerugian-kerugian yang timbul pada bagasi penumpang berdasarkan pada syarat-syarat dan batas-batas yang ditentukan di dalam Peraturan Pemerintah Republik Indonesia No. 40 tahun 1995 dan syarat-syarat umum pengangkutan dari pengangkut.
                <ul style="list-style: lower-alpha">
                    <li>Bagasi dianggap telah diterima dalam kondisi dan keadaan yang lengkap dan baik pada saat penerimaan kecuali apabila penumpang mengajukan protes pada saat penerimaan barang tersebut.</li>
                    <li>Semua tuntutan ganti kerugian harus dapat dibuktikan besarnya kerugian yang secara nyata diderita. Tanggung jawab terbatas untuk kehilangan dan kerusakan bagasi ditetapkan sejumlah setinggi-tingginya IDR 100.000,- (seratus ribu rupiah) per kilogram.</li>
                    <li>Pengangkut udara tidak bertanggung jawab terhadap kerusakan barang-barang pecah belah/cepat busuk dan binatang hidup jika diangkut sebagai bagasi.</li>
                    <li>Pengangkut udara tidak bertanggung jawab terhadap barang-barang berharga seperti uang, perhiasan, barang elektronik, obat-obatan, dokumen serta surat berharga atau sejenisnya jika dimasukkan kedalam bagasi.</li>
                </ul>
            </li>
            <li>Tidak ada agen, pegawai atau wakil pengangkut yang memiliki hak untuk mengubah atau membatalkan syarat-syarat pengangkutan, tarif, jadwal dan peraturan lain dari pengangkut yang berlaku, baik sebagian maupun seluruhnya. Penumpang yang namanya tercantum di dalam tiket ini diasuransikan pada PT. Asuransi Kerugian Jasa Raharja berdasarkan Undang-Undang No. 33/1964 juncto peraturan-peraturan pelaksanaannya.</li>
        </ol>
    </td>
    <td style="width: 340px;">
        <p><b>CONDITIONS OF CONTRACT DOMESTIC REGULATION</b></p>
        <ol>
            <li>Carriage hereunder is subject to the regulations of the Air Transport Ordonantie of The Republic of Indonesia Stbl. 1939/100, the law of the Republic of Indonesia No. 15, 1992 concerning Civil Aviation, the Government Regulation of the Republic of Indonesia No. 40, 1995 and to the applicable conditions of carriage, tariffs, time tables (except the times of departure and arrival stated therein) and other regulations of the carrier, which form an inseparable part hereof and which are available for inspection at the carriers booking offices.</li>
            <li>This passengers ticket is valid only for the person named hereon and is not transferable. The passenger agrees that the carrier reserves the right to check if necessary, whether this ticket is utilized by the rightful person. If anyone other than the person named on the ticket travels or endeavors to travel on this ticket, the carrier is entitled to refuse such transportation and the right of transportation on this ticket by the person entitled on the carriage will lapse.</li>
            <li>The carrier reserves the right to substitute other carriers for the execution of the contract and to alter agreed stopping places.</li>
            <li>Checked baggage carried hereunder will only be delivered to the passenger on production of the baggage claim tag.</li>
            <li>
                The carrier is liable for the damage and loss on the passengers baggage subject to the Government Regulation of the Republic of Indonesia No. 40, 1995 and the conditions of the carriage of the carrier.
                <ul style="list-style: lower-alpha">
                    <li>A baggage is regarded received in good order and condition by the passenger unless the passenger claims otherwise upon receipt of his/her baggage.</li>
                    <li>All claims are subject to proof of amount of actual loss. The liability for lost or damaged baggage is limited to IDR 100.000 (one hundred thousand rupiahs) per kilogram.</li>
                    <li>The carriage assumes no liability for fragile/perishable articles and live animals if carried as baggage.</li>
                    <li>The carrier assumes no liability for valuable goods such as money, jewellery, electronic device, medicines, documents and valuable papers etc, kept in the baggage.</li>
                </ul>
            </li>
            <li>No agent, employee or representative of the carrier has authority to alter or waive either wholly or partly any provision of the applicable conditions of carriage, tariffs, time tables and other regulations of the carrier.</li>
        </ol>
    </td>
</tr>
