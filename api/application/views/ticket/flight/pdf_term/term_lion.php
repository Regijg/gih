<tr>
    <td height="20px" colspan="2">
        <p><b>Important Notes</b></p>
    </td>
</tr>
<tr>
    <td style="width: 8px;">*</td>
    <td style="width: 688px;">Please arrive at the airport 90 minutes before the flight for domestic travel and 2 hours for international travel.</td>
</tr>
<tr>
    <td>*</td>
    <td>Check-in closes 45 minutes before departure time.</td>
</tr>
<tr>
    <td>*</td>
    <td>Please be at the gate 30 minutes before departure time.</td>
</tr>
<tr>
    <td>*</td>
    <td>If paid by credit card please note that the credit card used must be presented by the card holder for verification at check-in or you may be denied boarding.</td>
</tr>
<tr>
    <td>*</td>
    <td>Baggage allowance Lion Air Group: </td>
</tr>
<tr>
    <td>&nbsp;</td>
    <td>
        <table>
            <tr>
                <td>-</td>
                <td>Lion Air = Domestic Flight ( Business Class 30Kg and Economy Class 20Kg), International Flight (Economy Class 20Kg)</td>
            </tr>
            <tr>
                <td>-</td>
                <td>Wings Air = Economy class 10Kg</td>
            </tr>
            <tr>
                <td>-</td>
                <td>Batik Air = Business Class 30Kg, Economy Class 20Kg</td>
            </tr>
            <tr>
                <td>-</td>
                <td>Thai Lion Air = Economy Class 15Kg</td>
            </tr>
            <tr>
                <td>-</td>
                <td>Malindo Air = Domestic Flight (Business Class 30Kg and Economy Class 15Kg), International Flight (Business Class 30Kg and Economy Class 20Kg)</td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td>*</td>
    <td>Economy Class Passengers are allowed to bring up to 7kg of hand luggage onboard Lion Air Flights. Please refer to our terms and condition for more information.</td>
</tr>
<tr>
    <td>*</td>
    <td>Passengers agree with Terms and Conditions of Carriage outlined by Company.</td>
</tr>
<tr>
    <td height="20px" colspan="2">
        <p><b>Catatan Penting</b></p>
    </td>
</tr>
<tr>
    <td>*</td>
    <td>Mohon tiba di Bandara selambat-lambatnya 90 menit sebelum keberangkatan untuk domestik atau 2 jam untuk internasional.</td>
</tr>
<tr>
    <td>*</td>
    <td>Cek-in ditutup 45 menit sebelum jam keberangkatan.</td>
</tr>
<tr>
    <td>*</td>
    <td>Mohon tiba di gerbang keberangkatan 30 menit sebelum keberangkatan.</td>
</tr>
<tr>
    <td>*</td>
    <td>Bila anda melakukan pembayaran menggunakan kartu kredit mohon menunjukan kartu tersebut berserta pemegang kartu untuk verifikasi pada konter cek in
        <br />atau proses boarding anda dapat dibatalkan.</td>
</tr>
<tr>
    <td>*</td>
    <td>Bagasi cuma-cuma Lion Air Group: </td>
</tr>
<tr>
    <td>&nbsp;</td>
    <td>
        <table>
            <tr>
                <td>-</td>
                <td>Lion Air = Penerbangan Domestik (Kelas Bisnis 30Kg dan Kelas Ekonomi 20Kg), Penerbangan Internasional (Kelas Ekonomi 20Kg)</td>
            </tr>
            <tr>
                <td>-</td>
                <td>Wings Air = Kelas Ekonomi 10Kg</td>
            </tr>
            <tr>
                <td>-</td>
                <td>Batik Air = Kelas Bisnis 30Kg, Kelas Ekonomi 20Kg</td>
            </tr>
            <tr>
                <td>-</td>
                <td>Thai Lion Air = Kelas Ekonomi 15Kg</td>
            </tr>
            <tr>
                <td>-</td>
                <td>Malindo Air = Penerbangan Domestik (Kelas Bisnis 30Kg dan Kelas Ekonomi 15Kg), Penerbangan Internasional (Kelas Bisnis 30Kg dan Kelas Ekonomi 20Kg)</td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td>*</td>
    <td>Penumpang kelas Ekonomi diperbolehkan membawa barang bawaan maksimum seberat 7Kg ke dalam kabin. Silahkan membaca persyaratan
        <br />dan ketentuan yang berlaku untuk informasi lebih lanjut.</td>
</tr>
<tr>
    <td>*</td>
    <td>Penumpang/Pemegang tiket ini tunduk kepada Syarat &amp; Ketentuan Penerbangan yang ditetapkan oleh Maskapai.</td>
</tr>
