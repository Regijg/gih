<tr>
    <td height="20px" colspan="2">
        <p><b>TERM & CONDITION :</b></p>
    </td>
</tr>
<tr>
    <td style="width: 8px;">-</td>
    <td style="width: 680px;">YOUR AIRLINE TICKET IS ELECTRONICALLY STORED IN OUR SYSTEM AND IS SUBJECT TO CONDITION OF CONTRACT.</td>
</tr>
<tr>
    <td>-</td>
    <td>PLEASE BRING THIS RECEIPT AND YOUR IDENTITY CARD ON YOUR TRAVEL IN CASE REQUIRED BY AIRPORT/CHECK-IN COUNTER/CUSTOMS AND IMMIGRATION
        <br> OFFICIALS AS PROOF OF PURCHASE.</td>
</tr>
<tr>
    <td>-</td>
    <td>THE FARE ABOVE IS SUBJECT TO THE APPLICABLE CONDITIONS.</td>
</tr>
<tr>
    <td>-</td>
    <td>CHECK-IN COUNTERS WILL BE CLOSED 45 MINUTES PRIOR TO DEPARTURE. YOU HAVE TO BE AT THE BOARDING GATE AT LEAST 30 MINUTES BEFORE FLIGHT
        <br> DEPARTS OR WE WILL LEAVE WITHOUT YOU TO AVOID UNNECESSARY DELAYS.</td>
</tr>
<tr>
    <td height="20px" colspan="2">
        <p><b>NOTICE :</b></p>
    </td>
</tr>
<tr>
    <td>-</td>
    <td>CARRIAGE AND OTHER SERVICES PROVIDED BY THE CARRIER ARE SUBJECT TO THE CONDITIONS OF CARRIAGE WHICH ARE HEREBY INCORPORATED
        <br> BY REFERENCE. THESE CONDITIONS MAY BE OBTAINED FROM THE ISSUING CARRIER</td>
</tr>
<tr>
    <td height="20px" colspan="2">
        <p><b>DANGEROUS GOODS</b></p>
    </td>
</tr>
<tr>
    <td>-</td>
    <td>FOR SAFETY REASON. DANGEROUS ARTICLES SUCH AS COMPRESSED GASES/ FLAMMABLE /NON FLAMMABLE/POISONOUS /CORROSIVES /ACIDS /ALKALIS
        <br> AND WET CELL BATTERIES/ETILOGIC AGENTS /BACTERIA/VIRUSES ETC/ EXPLOSIVES MUNITIONS/FIREWORKS /FLARES /RADIO ACTIVE /OXIDIZING MATERIALS
        <br> OR OTHER DANGEROUS GOODS ARTICLE MUST NOT BE CARRIED IN PASSENGERS BAGGAGE</td>
</tr>
<tr>
    <td height="20px" colspan="2">
        <p><b>ELECTRONIC TICKET RECEIPT CONDITIONS OF CONTRACTS DOMESTIC REGULATION</b></p>
    </td>
</tr>
<tr>
    <td>-</td>
    <td>Carriage hereunder is subject to the regulations of the Air Transport Ordonantie of The Republic of Indonesia Stbl. 1939/100, the Law of the Republic of Indonesia No. 15, 1992
        <br> concerning Civil Aviation, the Government Regulation of the Republic of Indonesia No. 40, 1995 and to the applicable conditions of carriage,tariffs, time tables (except the
        <br> times of departure and arrival stated therein) and other regulations of the carrier, which form an inseparable part hereof and which are available for inspection at the carrier's
        <br> booking offices.</td>
</tr>
<tr>
    <td>-</td>
    <td>This passenger's ticket is valid only for the person named hereon and is not transferable. The passenger agrees that the carrier reserves the right to check if necessary,
        <br> whether this ticket is utilized by the rightful person. If anyone other than the person named on the ticket travels or endeavors to travel on this ticket, the carrier is entitled to
        <br> refuse such transportation and the right of transportation on this ticket by the person entitled on the carriage will lapse.</td>
</tr>
<tr>
    <td>-</td>
    <td>The carrier reserves the right to substitute other carriers for the execution of the contract and to alter agreed stopping places.</td>
</tr>
<tr>
    <td>-</td>
    <td>Checked baggage carried hereunder will only be delivered to the passenger on production of the baggage claim tag.</td>
</tr>
<tr>
    <td>&nbsp;</td>
    <td>
        <table>
            <tr>
                <td>-</td>
                <td>The carrier is liable for the damage and loss on the passenger's baggage subject to the Government Regulation of the Republic of Indonesia No 40, 1995 and the conditions of
                    <br> the carriage of the carrier.</td>
            </tr>
            <tr>
                <td>-</td>
                <td>A baggage is regarded received in good order and condition by the passenger unless the passenger claims otherwise upon receipt of his/her baggage.</td>
            </tr>
            <tr>
                <td>-</td>
                <td>All claims are subject to proof of amount of actual loss. The liability for lost or damaged baggage is limited to IDR 100.000,- per kg.</td>
            </tr>
            <tr>
                <td>-</td>
                <td>The Carrier assumes no liability for fragile/ perishable articles and lives animals if carried as baggage.</td>
            </tr>
            <tr>
                <td>-</td>
                <td>The Carrier assumes no liability for valuable goods such as money, jewellery, electronic devices, medicines, documents and valuable papers etc.,kept in the baggage.</td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td>-</td>
    <td>No agent, employee or representative of the carrier has authority to alter or waive either wholly or partly any provision of the applicable conditions of carriage, tariffs, time tables
        <br> and other regulations of the carrier The passenger named hereon is insured with P.T Asuransi Kerugian Jasa Raharja in accordance with Act No. 33/1964 juncto its
        <br> implementation regulations.</td>
</tr>
