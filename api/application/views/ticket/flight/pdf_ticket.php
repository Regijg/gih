<?php
$dt_flight = $flight['dt_flight'];
$passenger = $flight['dt_flight_pax'];
$schedule  = $flight['dt_flight_schedule'];
$travel    = $flight['travel'];
#logo $travel['business_logo'];
/******** MARGIN ********/
$margin = $dt_flight[0]->dtf_margin;
if ($dt_flight[0]->dtf_roundtrip == 'return') {
    $depart_margin = $margin / 2;
    $return_margin = $margin / 2;

    for ($i=0; $i < count($schedule); $i++) {
        if ($schedule[$i]->sch_type == 'depart') {
            $depart_total_price = $depart_margin + $schedule[$i]->sch_ticket_price;
        }
        if ($schedule[$i]->sch_type == 'return') {
            $return_total_price = $return_margin + $schedule[$i]->sch_ticket_price;
        }
    }
} else {
    $depart_margin = $margin;
    for ($i=0; $i < count($schedule); $i++) {
        if ($schedule[$i]->sch_type == 'depart') {
            $depart_total_price = $depart_margin + $schedule[$i]->sch_ticket_price;
        }
    }
}
?>
<page style="font-family: Arial, Helvetica, sans-serif; font-size: 10px" pagegroup="new">

    <page_header>
        <div style="border: 1px solid #EEE; height: 1070px;">
        </div>
    </page_header>

    <div style="padding:1px 1px 1px 1px; width:99.7%;">

        <div style="padding: 5px 10px 5px 0; border-bottom: 0.7px solid #CCCCCC;">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr valign="middle">
                    <td style="width: 390px; height: 60px;">
                    	<?php if($travel['business_logo']!=""):?>
                        	<img src="<?=$travel['business_logo']?>" style="height: 60px; margin: 10px 0 10px 10px;">
						<?php else: ?>
                        	<img src="<?php echo $this->converter->header_logo_ticket($dt_flight[0]->depart_airlines_id); ?>" style="height: 60px; margin: 10px 0 10px 10px;">
                        <?php endif; ?>
                    </td>
                    <td style="width: 345px;" align="right">
                        <h4><?=$travel['business_name']?></h4>
                        <p style="margin-top:-10px; font-size:12px;"><?=$travel['business_address']?><br /><?=$travel['business_phone']?></p>
                    </td>
            </tr>
            </table>
        </div>

        <div style="border-bottom: 0.7px solid #CCCCCC;">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr valign="middle">
                    <td style="background-color: #FFFFFF; border-right: 0.7px dotted #CCCCCC; width: 370px;">
                        <table width="100%" style="margin: 0 0 10px 10px;">
                            <tr>
                                <td style="height: 15px;" width="100">Booking Date</td>
                                <td>:</td>
                                <td><?php echo $dt_flight[0]->dtf_bookdate; ?></td>
                            </tr>
                            <tr>
                                <td style="height:15px;">Name</td>
                                <td>:</td>
                                <td><?php echo strtoupper($dt_flight[0]->customer_name); ?></td>
                            </tr>
                            <tr>
                                <td style="height:15px;">Contact</td>
                                <td>:</td>
                                <td><?php echo $dt_flight[0]->customer_phone; ?></td>
                            </tr>
                            <tr>
                                <td style="height:15px;">Email</td>
                                <td>:</td>
                                <td><?php echo $dt_flight[0]->customer_email; ?></td>
                            </tr>
                        </table>
                    </td>
                    <td align="center" style="background-color: #FFFFFF; width: 370px;">
                    	<?php if($travel['business_logo']!=""):?>
                        	<img src="<?php echo $this->converter->header_logo_ticket($dt_flight[0]->depart_airlines_id); ?>" style="height: 60px; margin: 10px 20px 10px 10px;">
						<?php endif; ?>
                    	<barcode type="C128A" value="<?php echo $dt_flight[0]->depart_book_code; ?>" label="label" style="width:30mm; height:12mm; color: #000000; font-size: 4mm"></barcode>
                    </td>
                </tr>
            </table>
        </div>

    </div>

    <!-- <table style="border: 0.7px #CCCCCC solid; border-radius: 3px; margin: 20px 15px 20px 15px;" width="100%"> -->
    <table style="border: 0.7px #CCCCCC solid; border-radius: 3px; margin: 20px 10px 0 10px;">
        <tr style="padding: 5px 5px 5px 5px;" valign="middle">
            <td align="left" colspan="4" style="background-color: #C8C8C8; border-bottom: 0.7px #CCCCCC solid; padding: 5px 0 5px 10px;">
                <strong>Passenger</strong>
            </td>
        </tr>
        <?php
        $n = 0;
        foreach ($passenger as $pax) {
        ?>
        <tr valign="middle">
            <td style="height: 20px; padding-left: 5px; width: 5;"><?php echo ($n+1); ?></td>
            <td style="padding-left: 5px; width: 245px;"><?php echo strtoupper($pax->pax_title.'. '.$pax->pax_first_name.' '.$pax->pax_last_name); ?></td>
            <td style="padding-left: 5px; width: 245px;"><?php #echo $pax->pax_id_card; ?></td>
            <td style="padding-left: 5px; width: 180px;"><?php if ($pax->pax_depart_ticket_no != '') { echo $pax->pax_depart_ticket_no; } else { echo "-"; } ?></td>
        </tr>
        <?php } ?>
    </table>

    <!-- <table style="margin:20px 15px 20px 15px; border:0.7px #CCCCCC solid; border-radius:3px;" width="100%"> -->
    <table style="border: 0.7px #CCCCCC solid; border-radius: 5px; margin: 20px 10px 0 10px;">
        <tr style="padding: 5px 5px 5px 5px;" valign="middle">
            <td align="left" colspan="6" style="background-color: #C8C8C8; border-bottom: 0.7px #CCCCCC solid; padding: 5px 0 5px 10px;">
                <strong>Flight Detail</strong>
            </td>
        </tr>
        <tr valign="middle">
            <td style="padding-left: 5px; height: 20px; width: 123px;"><strong>Date</strong></td>
            <td style="padding-left: 5px; width: 90px;">Book Code</td>
            <td style="padding-left: 5px; width: 100px;"><strong>Flight No</strong></td>
            <td style="padding-left: 5px; width: 123px;"><strong>Departing</strong></td>
            <td style="padding-left: 5px; width: 123px;"><strong>Arriving</strong></td>
            <td style="padding-left: 5px; width: 85px;"><strong>Class</strong></td>
            <!--<td style="padding-left: 5px; width: 55px;"><strong>Baggage</strong></td>-->
        </tr>
        <?php
        foreach ($schedule as $sch) {
            if ($sch->sch_type == 'depart' OR $sch->sch_type == 'depart_connecting') {
        ?>
        <tr height="30px">
            <td style="padding-left: 5px; height: 20px; width: 123px;"><?php echo $this->converter->setTanggalNama($sch->sch_date,"2"); ?></td>
            <td style="padding-left: 5px; width: 90px;"><?php echo $dt_flight[0]->depart_book_code; ?></td>
            <td style="padding-left: 5px; width: 100px;"><?php echo $sch->sch_flight_no; ?></td>
            <td style="padding-left: 5px; width: 123px;"><?php echo airport_city($sch->sch_from).' ('.$sch->sch_etd.')'; ?></td>
            <td style="padding-left: 5px; width: 123px;"><?php echo airport_city($sch->sch_to).' ('.$sch->sch_eta.')'; ?></td>
            <td style="padding-left: 5px; width: 85px;"><?php echo $sch->sch_class_code; ?></td>
            <!--<td style="padding-left: 5px; width: 55px;"><?php echo $sch->sch_baggage; ?></td>-->
        </tr>
        <?php
            }
        }
        ?>
    </table>
    <table style="margin: 20px 10px 0 10px;">
        <tr valign="middle">
            <td align="right" colspan="2" style="padding-top: 10px;">
                <table style="border:0.7px dashed #CCCCCC; border-radius:3px;" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width:575px; height:20px;" align="right"><span><b>Harga Total</b></span></td>
                        <td align="right" style="padding-right:5px;"><span style="margin-left:95px; font-size:12px;"><?php echo number_format($depart_total_price, 0, ",", "."); ?></span></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <img src="<?=base_url()?>asset/image/paid_stamp.png" height="150px;" style="margin:-60px 0 40px 0px;" />
    <!-- <div style="margin: 20px 15px 0 15px;"> -->
    	<?php 
		$html = $this->converter->term_ticket($dt_flight[0]->depart_airlines_id, '', 'pdf');
		if($html!=""):
		?>
        <table style="border: 0.7px #CCCCCC solid; border-radius: 5px; font-size: 9px; margin: 20px 10px 0 10px;">
            <tr>
                <td colspan="2" style="background-color: #C8C8C8; font-size: 11px; padding: 5px 0 5px 10px; width: 710px;"><strong>Term & Condition</strong></td>
            </tr>
            <?php echo $this->load->view($html, '', TRUE); ?>
        </table>
    	<?php 
		endif;
		?>
    <!-- </div> -->

</page>

<?php if ($dt_flight[0]->dtf_roundtrip == 'return') { ?>
<page style="font-family: Arial, Helvetica, sans-serif; font-size: 10px" pagegroup="new">

    <page_header>
        <div style="border: 1px solid #EEE; height: 1070px;">
        </div>
    </page_header>

    <div style="padding:1px 1px 1px 1px; width:99.7%;">

        <div style="padding: 5px 10px 5px 0; border-bottom: 0.7px solid #CCCCCC;">
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr valign="middle">
                    <td style="width: 390px; height: 60px;"><img src="<?php echo $this->converter->header_logo_ticket($dt_flight[0]->return_airlines_id); ?>" style="height: 60px; margin: 10px 0 10px 10px;"></td>
                    <td style="width: 345px;" align="right">&nbsp;</td>
                </tr>
            </table>
        </div>

        <div style="border-bottom: 0.7px solid #CCCCCC;">
            <table border="0" cellpadding="0" cellspacing="0">
                <tr valign="middle">
                    <td style="background-color: #FFFFFF; border-right: 0.7px dotted #CCCCCC; width: 370px;">
                        <table width="100%" style="margin: 0 0 10px 10px;">
                            <tr>
                                <td style="height: 15px;" width="100">Booking Date</td>
                                <td>:</td>
                                <td><?php echo $dt_flight[0]->dtf_bookdate; ?></td>
                            </tr>
                            <tr>
                                <td style="height:15px;">Name</td>
                                <td>:</td>
                                <td><?php echo strtoupper($dt_flight[0]->customer_name); ?></td>
                            </tr>
                            <tr>
                                <td style="height:15px;">Contact</td>
                                <td>:</td>
                                <td><?php echo $dt_flight[0]->customer_phone; ?></td>
                            </tr>
                            <tr>
                                <td style="height:15px;">Email</td>
                                <td>:</td>
                                <td><?php echo $dt_flight[0]->customer_email; ?></td>
                            </tr>
                        </table>
                    </td>
                    <td align="center" style="background-color: #FFFFFF; width: 370px;">
                        <barcode type="C128A" value="<?php echo $dt_flight[0]->return_book_code; ?>" label="label" style="width:30mm; height:12mm; color: #000000; font-size: 4mm"></barcode>
                    </td>
                </tr>
            </table>
        </div>

    </div>

    <!-- <table style="border: 0.7px #CCCCCC solid; border-radius: 3px; margin: 20px 15px 20px 15px;" width="100%"> -->
    <table style="border: 0.7px #CCCCCC solid; border-radius: 3px; margin: 20px 10px 0 10px;">
        <tr style="padding: 5px 5px 5px 5px;" valign="middle">
            <td align="left" colspan="4" style="background-color: #C8C8C8; border-bottom: 0.7px #CCCCCC solid; padding: 5px 0 5px 10px;">
                <strong>Passenger</strong>
            </td>
        </tr>
        <?php
        $n = 0;
        foreach ($passenger as $pax) {
        ?>
        <tr valign="middle">
            <td style="height: 20px; padding-left: 5px; width: 5;"><?php echo ($n+1); ?></td>
            <td style="padding-left: 5px; width: 245px;"><?php echo strtoupper($pax->pax_title.'. '.$pax->pax_first_name.' '.$pax->pax_last_name); ?></td>
            <td style="padding-left: 5px; width: 245px;"><?php #echo $pax->pax_id_card; ?></td>
            <td style="padding-left: 5px; width: 180px;"><?php if ($pax->pax_return_ticket_no != '') { echo $pax->pax_return_ticket_no; } else { echo "-"; } ?></td>
        </tr>
        <?php } ?>
    </table>

    <!-- <table style="margin:20px 15px 20px 15px; border:0.7px #CCCCCC solid; border-radius:3px;" width="100%"> -->
    <table style="border: 0.7px #CCCCCC solid; border-radius: 5px; margin: 20px 10px 0 10px;">
        <tr style="padding: 5px 5px 5px 5px;" valign="middle">
            <td align="left" colspan="6" style="background-color: #C8C8C8; border-bottom: 0.7px #CCCCCC solid; padding: 5px 0 5px 10px;">
                <strong>Flight Detail</strong>
            </td>
        </tr>
        <tr valign="middle">
            <td style="padding-left: 5px; height: 20px; width: 123px;"><strong>Date</strong></td>
            <td style="padding-left: 5px; width: 90px;">Book Code</td>
            <td style="padding-left: 5px; width: 100px;"><strong>Flight No</strong></td>
            <td style="padding-left: 5px; width: 123px;"><strong>Departing</strong></td>
            <td style="padding-left: 5px; width: 123px;"><strong>Arriving</strong></td>
            <td style="padding-left: 5px; width: 85px;"><strong>Class</strong></td>
            <!--<td style="padding-left: 5px; width: 55px;"><strong>Baggage</strong></td>-->
        </tr>
        <?php
        foreach ($schedule as $sch) {
            if ($sch->sch_type == 'return' OR $sch->sch_type == 'return_connecting') {
        ?>
        <tr height="30px">
            <td style="padding-left: 5px; height: 20px; width: 123px;"><?php echo $this->converter->setTanggalNama($sch->sch_date,"2"); ?></td>
            <td style="padding-left: 5px; width: 90px;"><?php echo $dt_flight[0]->return_book_code; ?></td>
            <td style="padding-left: 5px; width: 100px;"><?php echo $sch->sch_flight_no; ?></td>
            <td style="padding-left: 5px; width: 123px;"><?php echo airport_city($sch->sch_from).' ('.$sch->sch_etd.')'; ?></td>
            <td style="padding-left: 5px; width: 123px;"><?php echo airport_city($sch->sch_to).' ('.$sch->sch_eta.')'; ?></td>
            <td style="padding-left: 5px; width: 85px;"><?php echo $sch->sch_class_code; ?></td>
            <!--<td style="padding-left: 5px; width: 55px;"><?php echo $sch->sch_baggage; ?></td>-->
        </tr>
        <?php
            }
        }
        ?>
    </table>
    <table style="margin: 20px 10px 0 10px;">
        <tr valign="middle">
            <td align="right" colspan="2" style="padding-top: 10px;">
                <table style="border:0.7px dashed #CCCCCC; border-radius: 3px;" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width:575px; height:20px;" align="right"><span><b>Harga Total</b></span></td>
                        <td align="right" style="padding-right:5px;"><span style="margin-left:95px; font-size:12px;"><?php echo number_format($return_total_price, 0, ",", "."); ?></span></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <!-- <div style="margin: 20px 15px 0 15px;"> -->
    	<?php 
		$html = $this->converter->term_ticket($dt_flight[0]->return_airlines_id, '', 'pdf');
		if($html!=""):
		?>
        <table style="border: 0.7px #CCCCCC solid; border-radius: 5px; font-size: 9px; margin: 20px 10px 0 10px;">
            <tr>
                <td colspan="2" style="background-color: #C8C8C8; font-size: 11px; padding: 5px 0 5px 10px; width: 710px;"><strong>Term & Condition</strong></td>
            </tr>
            <?php echo $this->load->view($html, '', TRUE); ?>
        </table>
        <?php 
		endif;
		?>
    <!-- </div> -->

</page>
<?php } ?>