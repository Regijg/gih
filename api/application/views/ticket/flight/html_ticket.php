<?php
$dt_flight = $flight['dt_flight'];
$passenger = $flight['dt_flight_pax'];
$schedule  = $flight['dt_flight_schedule'];

/******** MARGIN ********/
$margin = $dt_flight[0]->dtf_margin;
if ($dt_flight[0]->dtf_roundtrip == 'return') {
	$depart_margin = $margin / 2;
	$return_margin = $margin / 2;

	for ($i=0; $i < count($schedule); $i++) {
		if ($schedule[$i]->sch_type == 'depart') {
			$depart_total_price = $depart_margin + $schedule[$i]->sch_ticket_price;
		}
		if ($schedule[$i]->sch_type == 'return') {
			$return_total_price = $return_margin + $schedule[$i]->sch_ticket_price;
		}
	}
} else {
	$depart_margin = $margin;
	for ($i=0; $i < count($schedule); $i++) {
		if ($schedule[$i]->sch_type == 'depart') {
			$depart_total_price = $depart_margin + $schedule[$i]->sch_ticket_price;
		}
	}
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Flight Ticket Example</title>
</head>
<body>
	<table cellpadding="0" cellspacing="0" frame="box" width="100%" style="border-radius: 5px; overflow: hidden;">
		<!-- Header Comment -->
		<tr>
			<td align="left" style="border-bottom: 1px solid #CCCCCC;"><img src="<?php echo $this->converter->header_logo_ticket($dt_flight[0]->depart_airlines_id); ?>" style="margin: 10px 0 10px 25px;"></td>
			<td align="right" style="border-bottom: 1px solid #CCCCCC;">&nbsp;</td>
		</tr>
		<!-- /Header Comment -->
		<!-- Customer & Book Code -->
		<tr valign="middle">
			<!-- Customer Data -->
			<td style="padding: 10px 10px 10px 10px;">
				<table cellpadding="1" cellspacing="0" width="100%">
					<tr>
						<td style="padding-left: 5px;">Booking Date</td>
						<td>:</td>
						<td><?php echo $dt_flight[0]->dtf_bookdate; ?></td>
					</tr>
					<tr>
						<td style="padding-left: 5px;">Name</td>
						<td>:</td>
						<td><?php echo strtoupper($dt_flight[0]->customer_name); ?></td>
					</tr>
					<tr>
						<td style="padding-left: 5px;">Contact</td>
						<td>:</td>
						<td><?php echo $dt_flight[0]->customer_phone; ?></td>
					</tr>
					<tr>
						<td style="padding-left: 5px;">Email</td>
						<td>:</td>
						<td><?php echo $dt_flight[0]->customer_email; ?></td>
					</tr>
				</table>
			</td>
			<!-- /Customer Data -->
			<!-- Booking Code -->
			<td style="padding: 10px 10px 10px 10px;">
				<table cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td align="center" style="font-size:24px"><?php echo $dt_flight[0]->depart_book_code; ?></td>
					</tr>
				</table>
			</td>
			<!-- /Booking Code -->
		</tr>
		<!-- Customer & Book Code -->
		<!-- Passenger Data -->
		<tr>
			<td colspan="2" style="padding: 10px 10px 10px 10px;">
				<table cellpadding="0" cellspacing="0" frame="box" width="100%" style="border-radius: 5px; overflow: hidden;">
					<tr height="30px">
						<td colspan="4" style="background-color: #C8C8C8; padding: 5px 0 5px 10px;">Passenger</td>
					</tr>
					<?php
					$n = 0;
					foreach ($passenger as $pax) {
					?>
					<tr height="30px">
						<td style="padding-left: 5px;"><?php echo ($n+1); ?></td>
						<td><?php echo strtoupper($pax->pax_title.'. '.$pax->pax_first_name.' '.$pax->pax_last_name); ?></td>
						<td><?php echo $pax->pax_id_card; ?></td>
						<td><?php if ($pax->pax_depart_ticket_no != '') { echo $pax->pax_depart_ticket_no; } else { echo "-"; } ?></td>
					</tr>
					<?php } ?>
				</table>
			</td>
		</tr>
		<!-- /Passenger Data -->
		<!-- Schedule Data -->
		<tr>
			<td colspan="2" style="padding: 10px 10px 10px 10px;">
				<table cellpadding="0" cellspacing="0" frame="box" width="100%" style="border-radius: 5px; overflow: hidden;">
					<tr height="30px">
						<td colspan="6" style="background-color: #C8C8C8; padding: 5px 0 5px 10px;">Flight Detail</td>
					</tr>
					<tr height="30px">
						<td style="padding-left: 5px;">Date</td>
						<td>Flight No</td>
						<td>Departing</td>
						<td>Arriving</td>
						<td>Class</td>
						<!--<td>Baggage</td>-->
					</tr>
					<?php
					foreach ($schedule as $sch) {
						if ($sch->sch_type == 'depart' OR $sch->sch_type == 'depart_connecting') {
					?>
					<tr height="30px">
						<td style="padding-left: 5px;"><?php echo $sch->sch_date; ?></td>
						<td><?php echo $sch->sch_flight_no; ?></td>
						<td><?php echo $sch->sch_from.' ('.$sch->sch_etd.')'; ?></td>
						<td><?php echo $sch->sch_to.' ('.$sch->sch_eta.')'; ?></td>
						<td><?php echo $sch->sch_class_name; ?></td>
						<!--<td><?php echo $sch->sch_baggage; ?></td>-->
					</tr>
					<?php
						}
					}
					?>
				</table>
			</td>
		</tr>
		<!-- /Schedule Data -->
		<!-- Price -->
		<tr>
			<td align="right" colspan="2" style="padding: 0 10px 0 10px;">
				<table width="100%" style="border:1px dashed #CCCCCC; border-radius:5px;" cellpadding="0" cellspacing="0">
                    <tr height="25px">
                        <td width="70%" align="right"><span><b>Harga Total</b></td>
                        <td align="right" style="padding-right:5px;"><span style="margin-left: 95px; font-size: 16px; font-family: 'Courier New', Arial;"><b><?php echo number_format($depart_total_price, 0, ",", "."); ?></b></span></td>
                    </tr>
                </table>
			</td>
		</tr>
		<!-- /Price -->
		<!-- Term and Condition -->
        <?php 
		$html = $this->converter->term_ticket($dt_flight[0]->depart_airlines_id, '', 'html');
		if($html!=""):
		?>
		<tr>
			<td colspan="2" style="padding: 10px 10px 10px 10px;">
				<table cellpadding="0" cellspacing="0" frame="box" width="100%" style="border-radius: 5px; font-size: 9px; overflow: hidden;">
					<tr>
						<td colspan="2" style="background-color: #C8C8C8; font-size: 15px; padding: 5px 0 5px 10px;">Term & Condition</td>
					</tr>
                    <?php echo $this->load->view($html, '', TRUE); ?>
				</table>
			</td>
		</tr>
        <?php 
		endif;
		?>
		<!-- /Term and Condition -->
	</table>

	<?php if ($dt_flight[0]->dtf_roundtrip == 'return') { ?>
	<br>
	<table cellpadding="0" cellspacing="0" frame="box" width="100%" style="border-radius: 5px; overflow: hidden;">
		<!-- Header Comment -->
		<tr>
			<td align="left" style="border-bottom: 1px solid #CCCCCC;"><img src="<?php echo $this->converter->header_logo_ticket($dt_flight[0]->return_airlines_id); ?>" style="margin: 10px 0 10px 25px;"></td>
			<td align="right" style="border-bottom: 1px solid #CCCCCC;">&nbsp;</td>
		</tr>
		<!-- /Header Comment -->
		<!-- Customer & Book Code -->
		<tr valign="middle">
			<!-- Customer Data -->
			<td style="padding: 10px 10px 10px 10px;">
				<table cellpadding="1" cellspacing="0" width="100%">
					<tr>
						<td style="padding-left: 5px;">Booking Date</td>
						<td>:</td>
						<td><?php echo $dt_flight[0]->dtf_bookdate; ?></td>
					</tr>
					<tr>
						<td style="padding-left: 5px;">Name</td>
						<td>:</td>
						<td><?php echo strtoupper($dt_flight[0]->customer_name); ?></td>
					</tr>
					<tr>
						<td style="padding-left: 5px;">Contact</td>
						<td>:</td>
						<td><?php echo $dt_flight[0]->customer_phone; ?></td>
					</tr>
					<tr>
						<td style="padding-left: 5px;">Email</td>
						<td>:</td>
						<td><?php echo $dt_flight[0]->customer_email; ?></td>
					</tr>
				</table>
			</td>
			<!-- /Customer Data -->
			<!-- Booking Code -->
			<td style="padding: 10px 10px 10px 10px;">
				<table cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td align="center" style="font-size:24px"><?php echo $dt_flight[0]->return_book_code; ?></td>
					</tr>
				</table>
			</td>
			<!-- /Booking Code -->
		</tr>
		<!-- Customer & Book Code -->
		<!-- Passenger Data -->
		<tr>
			<td colspan="2" style="padding: 10px 10px 10px 10px;">
				<table cellpadding="0" cellspacing="0" frame="box" width="100%" style="border-radius: 5px; overflow: hidden;">
					<tr height="30px">
						<td colspan="4" style="background-color: #C8C8C8; padding: 5px 0 5px 10px;">Passenger</td>
					</tr>
					<?php
					$n = 0;
					foreach ($passenger as $pax) {
					?>
					<tr height="30px">
						<td style="padding-left: 5px;"><?php echo ($n+1); ?></td>
						<td><?php echo strtoupper($pax->pax_title.'. '.$pax->pax_first_name.' '.$pax->pax_first_name); ?></td>
						<td><?php echo $pax->pax_id_card; ?></td>
						<td><?php if ($pax->pax_return_ticket_no != '') { echo $pax->pax_return_ticket_no; } else { echo "-"; } ?></td>
					</tr>
					<?php } ?>
				</table>
			</td>
		</tr>
		<!-- /Passenger Data -->
		<!-- Schedule Data -->
		<tr>
			<td colspan="2" style="padding: 10px 10px 10px 10px;">
				<table cellpadding="0" cellspacing="0" frame="box" width="100%" style="border-radius: 5px; overflow: hidden;">
					<tr height="30px">
						<td colspan="6" style="background-color: #C8C8C8; padding: 5px 0 5px 10px;">Flight Detail</td>
					</tr>
					<tr height="30px">
						<td style="padding-left: 5px;">Date</td>
						<td>Flight No</td>
						<td>Departing</td>
						<td>Arriving</td>
						<td>Class</td>
						<td>Baggage</td>
					</tr>
					<?php
					foreach ($schedule as $sch) {
						if ($sch->sch_type == 'return' OR $sch->sch_type == 'return_connecting') {
					?>
					<tr height="30px">
						<td style="padding-left: 5px;"><?php echo $sch->sch_date; ?></td>
						<td><?php echo $sch->sch_flight_no; ?></td>
						<td><?php echo $sch->sch_from.' ('.$sch->sch_etd.')'; ?></td>
						<td><?php echo $sch->sch_to.' ('.$sch->sch_eta.')'; ?></td>
						<td><?php echo $sch->sch_class_name; ?></td>
						<td><?php echo $sch->sch_baggage; ?></td>
					</tr>
					<?php
						}
					}
					?>
				</table>
			</td>
		</tr>
		<!-- /Schedule Data -->
		<!-- Price -->
		<tr>
			<td align="right" colspan="2" style="padding: 0 10px 0 10px;">
				<table width="100%" style="border:1px dashed #CCCCCC; border-radius:5px;" cellpadding="0" cellspacing="0">
                    <tr height="25px">
                        <td width="70%" align="right"><span><b>Harga Total</b></td>
                        <td align="right" style="padding-right:5px;"><span style="margin-left: 95px; font-size: 16px; font-family: 'Courier New', Arial;"><b><?php echo number_format($return_total_price, 0, ",", "."); ?></b></span></td>
                    </tr>
                </table>
			</td>
		</tr>
		<!-- /Price -->
		<!-- Term and Condition -->
		<?php 
		$html = $this->converter->term_ticket($dt_flight[0]->return_airlines_id, '', 'html');
		if($html!=""):
		?>			
        <tr>
			<td colspan="2" style="padding: 10px 10px 10px 10px;">
				<table cellpadding="0" cellspacing="0" frame="box" width="100%" style="border-radius: 5px; font-size: 9px; overflow: hidden;">
					<tr>
						<td colspan="2" style="background-color: #C8C8C8; font-size: 15px; padding: 5px 0 5px 10px;">Term & Condition</td>
					</tr>
					<?php echo $this->load->view($html, '', TRUE); ?>		
				</table>
			</td>
		</tr>
        <?php 
		endif;
		?>
		<!-- /Term and Condition -->
	</table>
	<?php } ?>
</body>
</html>