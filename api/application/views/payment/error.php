<html>
<head>
	<meta http-equiv="refresh" content="5;url=<?=base_url()?>paymentdev/endtrx">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>	
	
    <div class="container">
    	
        <div class="alert alert-danger" role="alert" style="margin-top:10px;">
            <div class="row">
                <div class="col-xs-3">
                    <h1 align="center"><span class="glyphicon glyphicon-remove"></span></h1>
                </div>
                <div class="col-xs-9">
                	<h4>Ooopss !!!</h4>
                    <p align="center"><?php echo $reason?></p>
                </div>
            </div>
            <hr>
            <p align="center"><b>Mohon Maaf<br>Silahkan Hubungi Customer Service Kami.</b></p>
        </div>
        
        <a href="https://gih-indonesia.com/" class="btn btn-default btn-block">Home</a>
        
     </div>

</body>
</html>