<!DOCTYPE html>
<html>
<head>
	<title>FORM API</title>
</head>
<body>
	<h3>
		URL : <?php echo base_url('ws'); ?><br>
		WS (<?php echo $ws; ?>) --- Call (<?php echo $call; ?>)
	</h3>

	<form action="<?php echo base_url('ws'); ?>" method="post" enctype="multipart/form-data">
		<table>
			<?php for ($i=0; $i < count($field); $i++) { ?>
			<tr>
				<td><?php echo $field[$i]['name']; ?></td>
				<td>
					<input type="text" name="<?php echo $field[$i]['name']; ?>" value="<?php echo $field[$i]['val']; ?>"><br>
					<?php echo $field[$i]['msg']; ?>
				</td>
			</tr>
			<?php } ?>
			<tr>
				<td>img</td>
				<td><input type="file" name="img" id="img"/></td>
			</tr>
			<tr>
	        	<td colspan="2" align="right"><input type="submit" value="Kirim" /></td>
	        </tr>
		</table>
	</form>
</body>
</html>