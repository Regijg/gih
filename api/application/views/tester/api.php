<!DOCTYPE html>
<html>
<head>
	<title>List Testing API</title>
</head>
<body>

	<h2 align="center"> DAFTAR WS<br>METHOD : HTTP POST<br><b>HIT URL : <?=base_url('ws')?></b></h2>
    <table border="0" cellpadding="0" cellspacing="0" style="font-size: 12px; border:1px #E8E8FF dashed; padding:5px 5px 5px 5px;" width="100%">
        <tr style="font-weight: bold;"><td> - WS GENERAL</td></tr>
        <tr>
            <td>
                <table border="1" cellpadding="0" cellspacing="0" style="font-size: 12px; margin:10px 10px 10px 10px;" width="98%">
                    <tr>
                        <td>
                            <table border="0" cellpadding="0" cellspacing="0" style="font-size: 12px; margin:10px 10px 10px 10px;">
                                <tr style="font-weight: bold;"><td> - 1) WS LOGREG</td></tr>
                                <tr valign="top">
                                    <td>
                                        <div style="font-size: 14px;">
                                            <ol>
                                                <li><a href="<?php echo base_url('tester/class_logreg/login_process'); ?>" target="_blank">login_process</a></li>
                                                <li><a href="<?php echo base_url('tester/class_logreg/register_process'); ?>" target="_blank">register_proses</a></li>
                                                <li><a href="<?php echo base_url('tester/class_logreg/profile_update'); ?>" target="_blank">profile_update</a></li>
                                                <li><a href="<?php echo base_url('tester/class_logreg/password_change'); ?>" target="_blank">password_change</a></li>
                                                <li><a href="<?php echo base_url('tester/class_logreg/forgot_password'); ?>" target="_blank">forgot_password</a></li>
                                                <li><a href="<?php echo base_url('tester/class_logreg/business_scheme'); ?>" target="_blank">business_scheme</a></li>
                                                <li><a href="<?php echo base_url('tester/class_logreg/add_corporate_user'); ?>" target="_blank">add_corporate_user</a>
                                                </li>
                                                <li><a href="<?php echo base_url('tester/class_upload/upload_corporate_logo'); ?>" target="_blank">upload_corporate_logo</a></li>
                                                <li><a href="<?php echo base_url('tester/class_logreg/confirm_registration'); ?>" target="_blank">confirm_registration</a></li>
                                                <li><a href="<?php echo base_url('tester/class_logreg/get_branch'); ?>" target="_blank">get_branch</a></li>
                                                <li><a href="<?php echo base_url('tester/class_logreg/send_referal'); ?>" target="_blank">send_referal</a></li>
                                                <li><a href="<?php echo base_url('tester/class_logreg/update_status_corporate_user'); ?>" target="_blank">update_status_corporate_user</a></li>
                                            </ol>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                                        
                        </td>
                        <td valign="top">
                            <table border="0" cellpadding="0" cellspacing="0" style="font-size: 12px; margin:10px 10px 10px 10px;">
                                <tr style="font-weight: bold;"><td> - 4) WS Report</td></tr>
                                <tr valign="top">
                                    <td>
                                        <div style="font-size: 14px;">
                                            <ol>
                                                <li><a href="<?php echo base_url('tester/class_report/flight'); ?>" target="_blank">flight</a></li>
                                                <li><a href="<?php echo base_url('tester/class_report/flight_detail'); ?>" target="_blank">flight_detail</a></li>
                                                <li><a href="<?php echo base_url('tester/class_report/transaction'); ?>" target="_blank">transaction</a></li>
                                            </ol>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                                        
                        </td>
                    </tr>
                </table>
            </td>
        </tr><tr style="font-weight: bold;"><td> - WS PRODUK</td></tr>
        <tr>
            <td>
                <table border="1" cellpadding="0" cellspacing="0" style="font-size: 12px; margin:10px 10px 10px 10px;" width="98%">
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" style="font-size: 12px; margin:10px 10px 10px 10px;">
                            <tr style="font-weight: bold;"><td> - 1) WS FLIGHT</td></tr>
                            <tr valign="top">
                                <td>
                                    <div style="font-size: 14px;">
                                        <ol>
                                            <li><a href="<?php echo base_url('tester/class_flight/ws_getairlines'); ?>" target="_blank">ws_getairlines</a></li>
                                            <li><a href="<?php echo base_url('tester/class_flight/ws_getairport'); ?>" target="_blank">ws_getairport</a></li>
                                            <li><a href="<?php echo base_url('tester/class_flight/ws_search'); ?>" target="_blank">ws_search</a></li>
                                            <li><a href="<?php echo base_url('tester/class_flight/ws_getallclass'); ?>" target="_blank">ws_getallclass</a></li>
                                            <li><a href="<?php echo base_url('tester/class_flight/ws_faredetail'); ?>" target="_blank">ws_faredetail</a></li>
                                            <li><a href="<?php echo base_url('tester/class_flight/ws_book'); ?>" target="_blank">ws_book</a></li>
                                            <li><a href="<?php echo base_url('tester/class_flight/ws_payment'); ?>" target="_blank">ws_payment</a></li>
                                            <li><a href="<?php echo base_url('tester/class_flight/ws_cekpayment'); ?>" target="_blank">ws_cekpayment</a></li>
                                            <li><a href="<?php echo base_url('tester/class_flight/ws_getrealprice'); ?>" target="_blank">ws_getrealprice</a></li>
                                            <li><a href="<?php echo base_url('tester/class_flight/ws_getbalance'); ?>" target="_blank">ws_getbalance</a></li>
                                            <li><a href="<?php echo base_url('tester/class_flight/ws_getfavorite'); ?>" target="_blank">ws_getfavorite</a></li>
                                        </ol>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" style="font-size: 12px; margin:10px 10px 10px 10px;">
                            <tr style="font-weight: bold;"><td> - 2) WS HOTEL</td></tr>
                            <tr valign="top">
                                <td>
                                    <div style="font-size: 14px;">
                                        <ol>
                                            <li><a href="<?php echo base_url('tester/class_hotel/ws_srKeyword'); ?>" target="_blank">ws_srKeyword</a></li>
                                            <li><a href="<?php echo base_url('tester/class_hotel/ws_findHotel'); ?>" target="_blank">ws_findHotel</a></li>
                                            <!-- <li><a href="<?php echo base_url('tester/class_hotel/ws_nextHotel'); ?>" target="_blank">ws_nextHotel</a></li>
                                            <li><a href="<?php echo base_url('tester/class_hotel/ws_detailHotel'); ?>" target="_blank">ws_detailHotel</a></li>
                                            <li><a href="<?php echo base_url('tester/class_hotel/ws_selRoom'); ?>" target="_blank">ws_selRoom</a></li> -->
                                            <li><a href="<?php echo base_url('tester/class_hotel/ws_payment'); ?>" target="_blank">ws_payment</a></li>
                                            <li><a href="<?php echo base_url('tester/class_hotel/ws_bookInfo'); ?>" target="_blank">ws_bookInfo</a></li>
                                        </ol>
                                    </div>
                                </td>
                            </tr>
                        </table>        
                    </td>
                    <td valign="top">
                        <table border="0" cellpadding="0" cellspacing="0" style="font-size: 12px; margin:10px 10px 10px 10px;">
                            <tr style="font-weight: bold;"><td> - 3) WS DATA</td></tr>
                            <tr>
                                <td>
                                    <div style="font-size: 14px;">
                                        <ol>
                                            <li><a href="<?php echo base_url('tester/class_data/pdf_ticket'); ?>" target="_blank">pdf_ticket</a></li>
                                            <li><a href="<?php echo base_url('tester/class_data/html_ticket'); ?>" target="_blank">html_ticket</a></li>
                                            <li><a href="<?php echo base_url('tester/class_data/email_ticket'); ?>" target="_blank">email_ticket</a></li>
                                            <li><a href="<?php echo base_url('tester/class_data/slider'); ?>" target="_blank">slider</a></li>
                                            <li><a href="<?php echo base_url('tester/class_data/news'); ?>" target="_blank">news</a></li>
                                        </ol>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                </table>
            </td>
        </tr>
    </table>
    
	
</body>
</html>