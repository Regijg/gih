<?php

/**
* API Garuda class
*/
class Garuda
{
	private $airlines_id 	 	= '2';
	private $airlines_code	 	= 'GA';
	private $airlines_name	 	= 'Garuda';
	private $garuda_endpoint	= "https://dev-ags.garuda-indonesia.com/altea/";
	private $garuda_gateway		= "http://103.200.7.41:2019/api/garuda_gateway/submit_post";
	private $auth				= "Z2FydWRhaG9saWRheWRvdGNvbTpNdTU0RHZFYQ==";
	private $entity_code		= "SA3ARRX";
	private $shared_key_from_garuda = "SeAgMw2UKMFY";
	private $shared_key_from_PSP = "43R0tR4v3L";
	private $remote_address		= "103.200.7.41";
	private $user_agent			= "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.19 (KHTML, like Gecko) Chrome/1.0.154.53 Safari/525.19";

	function __construct()
	{
		$ci = & get_instance();
		//$ci->load->library('curl');
		$ci->load->library('parsing');
		$ci->load->library('converter');

		/*if (ENVIRONMENT !== 'production') {
			$this->baseEndpoint = "https://ws-uat.garuda-indonesia.com";
			$this->wordOsp = '43R0tR4v3L';
			$this->reqIdBookIssue = 'SA3AEET';
		}*/

		$this->datetime = date_create();
    	date_timezone_set($this->datetime,timezone_open("Asia/Jakarta"));
	}

	public function search($roundtrip='return', $from, $to, $depart_date, $return_date, $adult, $child, $infant, $search_type = "normal", $flight_int=null) {
		$ci =& get_instance();
		$result = [];


		if ($roundtrip == 'return') {
			$result = $this->searchReturn($roundtrip, $from, $to, $depart_date, $return_date, $adult, $child, $infant, $search_type, $flight_int);

		} else {
			$result = $this->searchOneway($roundtrip, $from, $to, $depart_date, $adult, $child, $infant, $search_type, $flight_int);
		}

		if (empty($result['error_no'])) {
			unset($result["error_no"]);

			$airlineInfo = array();
			$airlineInfo['airlines_id'] = $this->airlines_id;
			$airlineInfo['airlines_code'] = $this->airlines_code;
			$airlineInfo['airlines_name'] = $this->airlines_name;

			$searchInfo = array();
			$searchInfo['roundtrip'] = $roundtrip;
			$searchInfo['from'] = $from;
			$searchInfo['to'] = $to;
			$searchInfo['depart'] = $depart_date;
			$searchInfo['return']= $return_date;
			$searchInfo['adult'] = $adult;
			$searchInfo['child'] = $child;
			$searchInfo['infant'] = $infant;
			$searchInfo['page_ref'] = $ci->converter->encode($roundtrip."|".$from."|".$to."|".$depart_date."|".$return_date."|".$adult."|".$child."|".$infant);

			return array(
						'error_no' => "0",
						'search_info' => $searchInfo,
						'airlines_det' => $airlineInfo,
						'schedule' => $result
					);
		}

		return $result;
	}

	private function searchReturn($type, $from, $to, $depart_date, $return_date, $adult, $child, $infant, $search_type, $flight_int = NULL)
	{
		$departResult = $this->getAvailability('depart', $from, $to, $depart_date, $adult, $child, $infant, $search_type, $flight_int);
		$returnResult = $this->getAvailability('return', $to, $from, $return_date, $adult, $child, $infant, $search_type, $flight_int);

		if (empty($departResult['error_no']) && empty($returnResult['error_no'])) {
			return [
				'error_no' => '0',
				'depart' => $departResult['depart'],
				'return' => $returnResult['return']
			];
		} else {
			$error = $departResult;
			if (empty($departResult['error_no'])) {
				$error = $returnResult;
			}
			return [
				'error_no' => $error['error_no'],
				'error_msg' => $error['error_msg']
			];
		}
	}

	private function searchOneway($type, $from, $to, $date, $adult, $child, $infant, $search_type, $flight_int = NULL)
	{
		$searchResult = $this->getAvailability('depart', $from, $to, $date, $adult, $child, $infant, $search_type, $flight_int);
		if ($searchResult['error_no'] == '0') {
			return [
				"error_no" => "0",
				"depart" => $searchResult['depart']
			];
		} 

		return $searchResult;
	}

	public function getAvailability($type, $from, $to, $date, $adult, $child, $infant, $search_type, $flight_int = NULL)
	{
		$ci = &get_instance();
		$payload['method'] 			= 'Availability';
		$payload['Availability'] 	= [
			"entity"		=> "SA",
			"entityCode"	=> $this->entity_code,
			"username"		=> $this->entity_code,
			"origin"		=> $from,
			"dest"			=> $to,
			"tripType"		=> "o",
			"outDate"		=> $date,
			"serviceClass"	=> "all",
			"adults"		=> $adult,
			"childs"		=> $child,
			"infants"		=> $infant,
			"remoteAddr"	=> $this->remote_address,
			"httpUserAgent" => $this->user_agent
		];

		$params	= [
			"param" => $payload,
			"url"	=> $this->garuda_endpoint,
			"auth"	=> $this->auth
		];


		$response = $this->request($this->garuda_gateway, "Availability", json_encode($params));
		// var_dump($response);exit;
		if ($response['error_no'] == "0") {
			if($search_type == "normal"){
				$schedules = $this->parse_schedules2($type, $response['result']);	
			} else {
				$schedules = $this->parse_all_schedules($type, $response['result']);
			} 

			if (!empty($schedules)) {
				$result = array("error_no" => "0", $type => $schedules, "type" => $type);
			} else {
				$result = array("error_no" => "302", "error_msg" => "jadwal tidak di temukan");
			}
		} else {
			$result = array("error_no" => "302", "error_msg" => "jadwal tidak di temukan");
		}
		return $result;
	}

	private function parse_schedules($roundtrip, $response)
	{
		$ci =& get_instance();
		$schedules = array();
		$counterClass = 1;
		$fee = $ci->mgeneral->getValue('fee_values',array('airlines_id'=>$this->airlinesId,'passenger_types'=>'adult'),'service_fees');
		$price_list = [];

		foreach ($response['infoOnClasses'] as $schedule) {
			if(count($schedule) == 1){
				$ETD = explode(" ", $schedule[0]['departureDatetime']);
				$ETA = explode(" ", $schedule[0]['arrivalDatetime']);
				$departureDate 	= $ETD[0];
				$departureTime 	= date("H:i", strtotime($ETD[1]));
				$arrivalDate 	= $ETA[0];
				$arrivalTime 	= date("H:i", strtotime($ETA[1]));
				$departureLocation = $schedule[0]['origin'];
				$arrivalLocation = $schedule[0]['dest'];

				$flightType = $schedule[0]['companyIdentifier'];
				$flightIdentification = $schedule[0]['flightNumber'];				
				$flightNumber = $flightType." ".$flightIdentification;	
				
				$classes = [];
				$class_list = [];

				for($i=count($schedule[0]['infoOnClasses'])-1; $i>=0; $i--){
					if($schedule[0]['infoOnClasses'][$i]['productClassDetail']['availabilityStatus'] >= ($response['adults'] + $response['childs'] + $response['infants'])){
						$class = $schedule[0]['infoOnClasses'][$i]['productClassDetail']['serviceClass'];
						$seat = $schedule[0]['infoOnClasses'][$i]['productClassDetail']['availabilityStatus'];

						if(in_array($class, $response['cabinClassOfEconomy'])){
							$class_detail = "Economy";
						}
						else if(in_array($class, $response['cabinClassOfBusiness'])){
							$class_detail = "Executive";
						}
						else if(in_array($class, $response['cabinClassOfFirst'])){
							$class_detail = "FirstClass";
						}

						if(!isset($class_list[$class_detail])){

							$value = [
								"logId"			=> $response['logId'],
								"origin"		=> $response['origin'],
								"dest"			=> $response['dest'],
								"tripType"		=> $response['tripType'],
								"outDate"		=> $response['outDate'],
								"serviceClass"	=> $response['serviceClass'],
								"adults"		=> $response['adults'],
								"childs"		=> $response['childs'],
								"infants"		=> $response['infants'],
								"dep"			=> [
									"number"			=> $schedule[0]['number'],
						            "origin"			=> $schedule[0]['origin'],
						            "dest"				=> $schedule[0]['dest'],
						            "companyIdentifier" => $schedule[0]['companyIdentifier'],
						            "flightNumber"		=> $schedule[0]['flightNumber'],
						            "serviceClass"		=> $class,
						            "availabilityStatus"=> $seat,
						            "departureDatetime" => $schedule[0]['departureDatetime'],
						            "arrivalDatetime"	=> $schedule[0]['arrivalDatetime']
								]
							];
							$price_class = $value['origin']."-".$value['dest']."-".$class;
							if (!isset($price_list[$price_class])){
								$post = ['value' => ['value_1_a' => json_encode($value)]];
								$response_fare = $this->reqFareDetails($post, $flightInt);
								if($response_fare['error_no'] == "0"){
									$finalPrice = ceil($response_fare['result']['price']['adults']['netTotal']);
									$price_list[$price_class] = $finalPrice;
								} else {
									$finalPrice = NULL;
								}
							} else {
								$finalPrice = $price_list[$price_class];
							}

							if($finalPrice){
								$codeClass = ($roundtrip == 'depart') ? 'D' : 'R';

								$scheduleClass = array(
									'class_id' => $codeClass . 'GA' . ($counterClass),
									'class_name' => $class,
									'class' => $class_detail,
									'price' => $finalPrice,
									'seat' => $seat,
									'value' => $ci->converter->encode(json_encode($value))
								);
								$class_list[$class_detail] = $scheduleClass;
								array_push($classes, $scheduleClass);
								$counterClass++;
							}
						}
					}
				}

				//if(count($classes) > 0){
					$num_of_stop = $schedule[0]['additionalFlightInfo']['flightDetails']['numberOfStops'];
					$sch = array(
						'airline_id' => $this->airlines_id,
						'airline_name' => $this->airlines_name,
						'type' => $num_of_stop > 0 ? "transit" : "direct",
						'from' => $departureLocation,
						'to' => $arrivalLocation,
						'via' => $num_of_stop > 0 ? (string)$num_of_stop : "-",
						'fno' => $flightNumber,
						'date' => $departureDate,
						'etd' => $departureTime,
						'eta' => $arrivalTime,
						'connecting_flight' => '',
						'class' => $classes,
					);

					array_push($schedules, $sch);
				//}
			}
			else if (count($schedule) == 2) {
				$ETD = explode(" ", $schedule[0]['departureDatetime']);
				$ETA = explode(" ", $schedule[0]['arrivalDatetime']);
				$departureDate 	= $ETD[0];
				$departureTime 	= date("H:i", strtotime($ETD[1]));
				$arrivalDate 	= $ETA[0];
				$arrivalTime 	= date("H:i", strtotime($ETA[1]));
				$departureLocation = $schedule[0]['origin'];
				$arrivalLocation = $schedule[0]['dest'];

				$flightType = $schedule[0]['companyIdentifier'];
				$flightIdentification = $schedule[0]['flightNumber'];				
				$flightNumber = $flightType." ".$flightIdentification;	

				$ETD_conn = explode(" ", $schedule[1]['departureDatetime']);
				$ETA_conn = explode(" ", $schedule[1]['arrivalDatetime']);
				
				$num_of_stop_conn = $schedule[1]['additionalFlightInfo']['flightDetails']['numberOfStops'];
				$detail_connecting = array();
				$detail_connecting["airline_name"]	= $this->airlines_name;
				$detail_connecting["fno"]	= $schedule[1]['companyIdentifier']. " ". $schedule[1]['flightNumber'];
				$detail_connecting["from"]	= $schedule[1]['origin'];
				$detail_connecting["to"]	= $schedule[1]['dest'];
				$detail_connecting["via"] 	= $num_of_stop > 0 ? (string)$num_of_stop_conn : "-";
				$detail_connecting["etd"]	= date("H:i", strtotime($ETD_conn[1]));
				$detail_connecting["eta"]	= date("H:i", strtotime($ETA_conn[1]));
				$detail_connecting["date"]	= $ETD_conn[0];

				$value_conn = [
					'dep' => [
						"number"			=> $schedule[1]['number'],
			            "origin"			=> $schedule[1]['origin'],
			            "dest"				=> $schedule[1]['dest'],
			            "companyIdentifier" => $schedule[1]['companyIdentifier'],
			            "flightNumber"		=> $schedule[1]['flightNumber'],
			            "serviceClass"		=> "X",
			            "availabilityStatus"=> 0,
			            "departureDatetime" => $schedule[1]['departureDatetime'],
			            "arrivalDatetime"	=> $schedule[1]['arrivalDatetime']
					],
					"class" => NULL
				];

				for($i=count($schedule[1]['infoOnClasses'])-1; $i>=0; $i--){
					if($schedule[1]['infoOnClasses'][$i]['productClassDetail']['availabilityStatus'] > ($response['adults'] + $response['childs'] + $response['infants'])){
						$class = $schedule[1]['infoOnClasses'][$i]['productClassDetail']['serviceClass'];
						$seat = $schedule[1]['infoOnClasses'][$i]['productClassDetail']['availabilityStatus'];

						if(in_array($class, $response['cabinClassOfEconomy']) && !$value_conn['class']['Economy']){
							$value_conn['class']['Economy'] = [
								"class" => $class,
								"seat"	=> $seat
							];
						}
						else if(in_array($class, $response['cabinClassOfBusiness']) && !$value_conn['class']['Executive']){
							$value_conn['class']['Executive'] = [
								"class" => $class,
								"seat"	=> $seat
							];
						}
						else if(in_array($class, $response['cabinClassOfFirst']) && !$value_conn['class']['FirstClass']){
							$value_conn['class']['FirstClass'] = [
								"class" => $class,
								"seat"	=> $seat
							];
						}
					}
				}

				$classes = [];
				$class_list = [];
				for($i=count($schedule[0]['infoOnClasses'])-1; $i>=0; $i--){
					if($schedule[0]['infoOnClasses'][$i]['productClassDetail']['availabilityStatus'] > ($response['adults'] + $response['childs'] + $response['infants'])){
						$class = $schedule[0]['infoOnClasses'][$i]['productClassDetail']['serviceClass'];
						$seat = $schedule[0]['infoOnClasses'][$i]['productClassDetail']['availabilityStatus'];

						if(in_array($class, $response['cabinClassOfEconomy'])){
							$class_detail = "Economy";

						}
						else if(in_array($class, $response['cabinClassOfBusiness'])){
							$class_detail = "Executive";
						}
						else if(in_array($class, $response['cabinClassOfFirst'])){
							$class_detail = "FirstClass";
						}

						if (isset($value_conn['class'][$class_detail]) && !isset($class_list[$class_detail])){
							$value = [
								"logId"			=> $response['logId'],
								"origin"		=> $response['origin'],
								"dest"			=> $response['dest'],
								"tripType"		=> $response['tripType'],
								"outDate"		=> $response['outDate'],
								"serviceClass"	=> $response['serviceClass'],
								"adults"		=> $response['adults'],
								"childs"		=> $response['childs'],
								"infants"		=> $response['infants'],
								"dep"			=> [
									"number"			=> $schedule[0]['number'],
						            "origin"			=> $schedule[0]['origin'],
						            "dest"				=> $schedule[0]['dest'],
						            "companyIdentifier" => $schedule[0]['companyIdentifier'],
						            "flightNumber"		=> $schedule[0]['flightNumber'],
						            "serviceClass"		=> $class,
						            "availabilityStatus"=> $seat,
						            "departureDatetime" => $schedule[0]['departureDatetime'],
						            "arrivalDatetime"	=> $schedule[0]['arrivalDatetime']
								]
							];

							$price_class = $schedule[0]['origin']."-".$schedule[0]['dest']."-".$class."-".$value_conn['dep']['origin']."-".$value_conn['dep']['dest']."-".$value_conn['class'][$class_detail]['class'];
							if (!isset($price_list[$price_class])){
								
								$value_1_b = $value_conn['dep'];
								$value_1_b['serviceClass'] = $value_conn['class'][$class_detail]['class'];
								$value_1_b['availabilityStatus'] = $value_conn['class'][$class_detail]['seat'];
								$post = ['value' => ['value_1_a' => json_encode($value), 'value_1_b' => json_encode($value_1_b)]];
								
								$response_fare = $this->reqFareDetails($post, $flightInt);
								if($response_fare['error_no'] == "0"){
									$finalPrice = ceil($response_fare['result']['price']['adults']['netTotal']);
									$price_list[$price_class] = $finalPrice;
								} else {
									$finalPrice = NULL;
								}

							} else {
								
								$finalPrice = $price_list[$price_class];

							}

							if($finalPrice){
								$codeClass = ($roundtrip == 'depart') ? 'D' : 'R';

								$scheduleClass = array(
									'class_id' => $codeClass . 'GA' . ($counterClass),
									'class_name' => $class,
									'class' => $class_detail,
									'price' => $finalPrice,
									'seat' => $seat,
									'value' => $ci->converter->encode(json_encode($value))
								);
								$class_list[$class_detail] = $scheduleClass;
								array_push($classes, $scheduleClass);
								$counterClass++;
							}
						}
					}
				}
				if(count($classes) > 0){
					$detail_connecting['class'] = [[
						'class_name' => "X",
						'class' => "Economy",
						'price' => 0,
						'seat' => 9,
						'value' => $ci->converter->encode(json_encode($value_conn))
					]];

					$num_of_stop = $schedule[0]['additionalFlightInfo']['flightDetails']['numberOfStops'];
					$sch = array(
						'airline_id' => $this->airlines_id,
						'airline_name' => $this->airlines_name,
						'type' => "connecting",
						'from' => $departureLocation,
						'to' => $arrivalLocation,
						'via' => $num_of_stop > 0 ? (string)$num_of_stop : "-",
						'fno' => $flightNumber,
						'date' => $departureDate,
						'etd' => $departureTime,
						'eta' => $arrivalTime,
						'connecting_flight' => [$detail_connecting],
						'class' => $classes,
					);

					array_push($schedules, $sch);
				}
			}
		}
		
		return $schedules;
	}

	private function parse_schedules2($roundtrip, $response)
	{
		$ci =& get_instance();
		$schedules = [];
		$counterClass = 1;
		$fee = $ci->mgeneral->getValue('fee_values',array('airlines_id'=>$this->airlinesId,'passenger_types'=>'adult'),'service_fees');
		// print_r($response);
		$price_list = [];
		$all_class_list = [];

		foreach ($response['infoOnClasses'] as $schedule) {
			if(count($schedule) == 1){
				$ETD = explode(" ", $schedule[0]['departureDatetime']);
				$ETA = explode(" ", $schedule[0]['arrivalDatetime']);
				$departureDate 	= $ETD[0];
				$departureTime 	= date("H:i", strtotime($ETD[1]));
				$arrivalDate 	= $ETA[0];
				$arrivalTime 	= date("H:i", strtotime($ETA[1]));
				$departureLocation = $schedule[0]['origin'];
				$arrivalLocation = $schedule[0]['dest'];

				$flightType = $schedule[0]['companyIdentifier'];
				$flightIdentification = $schedule[0]['flightNumber'];				
				$flightNumber = $flightType." ".$flightIdentification;	
				
				$classes = [];
				$class_list = [];

				for($i=count($schedule[0]['infoOnClasses'])-1; $i>=0; $i--){
					if($schedule[0]['infoOnClasses'][$i]['productClassDetail']['availabilityStatus'] >= ($response['adults'] + $response['childs'] + $response['infants'])){
						$class = $schedule[0]['infoOnClasses'][$i]['productClassDetail']['serviceClass'];
						$seat = $schedule[0]['infoOnClasses'][$i]['productClassDetail']['availabilityStatus'];

						if(in_array($class, $response['cabinClassOfEconomy'])){
							$class_detail = "Economy";
						}
						else if(in_array($class, $response['cabinClassOfBusiness'])){
							$class_detail = "Executive";
						}
						else if(in_array($class, $response['cabinClassOfFirst'])){
							$class_detail = "FirstClass";
						}

						if(!isset($class_list[$class_detail])){

							$value = [
								"logId"			=> $response['logId'],
								"origin"		=> $response['origin'],
								"dest"			=> $response['dest'],
								"tripType"		=> $response['tripType'],
								"outDate"		=> $response['outDate'],
								"serviceClass"	=> $response['serviceClass'],
								"adults"		=> $response['adults'],
								"childs"		=> $response['childs'],
								"infants"		=> $response['infants'],
								"dep"			=> [
									"number"			=> $schedule[0]['number'],
						            "origin"			=> $schedule[0]['origin'],
						            "dest"				=> $schedule[0]['dest'],
						            "companyIdentifier" => $schedule[0]['companyIdentifier'],
						            "flightNumber"		=> $schedule[0]['flightNumber'],
						            "serviceClass"		=> $class,
						            "availabilityStatus"=> $seat,
						            "departureDatetime" => $schedule[0]['departureDatetime'],
						            "arrivalDatetime"	=> $schedule[0]['arrivalDatetime']
								]
							];
							$price_class = $value['origin']."-".$value['dest']."-".$class;
							if (!isset($price_list[$price_class])){
								$price_list[$price_class] = [
									'price' => 0,
									'value' => ['value_1_a' => $value]
								];
							}

							$finalPrice = 0;

							// if($finalPrice){
								$codeClass = ($roundtrip == 'depart') ? 'D' : 'R';

								$scheduleClass = array(
									'class_id' => $codeClass . 'GA' . ($counterClass),
									'class_name' => $class,
									'class' => $class_detail,
									'price' => $finalPrice,
									'seat' => $seat,
									'value' => $ci->converter->encode(json_encode($value))
								);
								$class_list[$class_detail] = $scheduleClass;
								array_push($classes, $scheduleClass);
								$counterClass++;
							// }
						}
					}
				}

				//if(count($classes) > 0){
					$num_of_stop = $schedule[0]['additionalFlightInfo']['flightDetails']['numberOfStops'];
					$sch = array(
						'airline_id' => $this->airlines_id,
						'airline_name' => $this->airlines_name,
						'type' => $num_of_stop > 0 ? "transit" : "direct",
						'from' => $departureLocation,
						'to' => $arrivalLocation,
						'via' => $num_of_stop > 0 ? (string)$num_of_stop : "-",
						'fno' => $flightNumber,
						'date' => $departureDate,
						'etd' => $departureTime,
						'eta' => $arrivalTime,
						'connecting_flight' => '',
						'class' => $classes,
					);

					array_push($schedules, $sch);
				//}
			}
			else if (count($schedule) == 2) {
				$ETD = explode(" ", $schedule[0]['departureDatetime']);
				$ETA = explode(" ", $schedule[0]['arrivalDatetime']);
				$departureDate 	= $ETD[0];
				$departureTime 	= date("H:i", strtotime($ETD[1]));
				$arrivalDate 	= $ETA[0];
				$arrivalTime 	= date("H:i", strtotime($ETA[1]));
				$departureLocation = $schedule[0]['origin'];
				$arrivalLocation = $schedule[0]['dest'];

				$flightType = $schedule[0]['companyIdentifier'];
				$flightIdentification = $schedule[0]['flightNumber'];				
				$flightNumber = $flightType." ".$flightIdentification;	

				$ETD_conn = explode(" ", $schedule[1]['departureDatetime']);
				$ETA_conn = explode(" ", $schedule[1]['arrivalDatetime']);
				
				$num_of_stop_conn = $schedule[1]['additionalFlightInfo']['flightDetails']['numberOfStops'];
				$detail_connecting = array();
				$detail_connecting["airline_name"]	= $this->airlines_name;
				$detail_connecting["fno"]	= $schedule[1]['companyIdentifier']. " ". $schedule[1]['flightNumber'];
				$detail_connecting["from"]	= $schedule[1]['origin'];
				$detail_connecting["to"]	= $schedule[1]['dest'];
				$detail_connecting["via"] 	= $num_of_stop > 0 ? (string)$num_of_stop_conn : "-";
				$detail_connecting["etd"]	= date("H:i", strtotime($ETD_conn[1]));
				$detail_connecting["eta"]	= date("H:i", strtotime($ETA_conn[1]));
				$detail_connecting["date"]	= $ETD_conn[0];

				$value_conn = [
					'dep' => [
						"number"			=> $schedule[1]['number'],
			            "origin"			=> $schedule[1]['origin'],
			            "dest"				=> $schedule[1]['dest'],
			            "companyIdentifier" => $schedule[1]['companyIdentifier'],
			            "flightNumber"		=> $schedule[1]['flightNumber'],
			            "serviceClass"		=> "X",
			            "availabilityStatus"=> 0,
			            "departureDatetime" => $schedule[1]['departureDatetime'],
			            "arrivalDatetime"	=> $schedule[1]['arrivalDatetime']
					],
					"class" => NULL
				];

				for($i=count($schedule[1]['infoOnClasses'])-1; $i>=0; $i--){
					if($schedule[1]['infoOnClasses'][$i]['productClassDetail']['availabilityStatus'] > ($response['adults'] + $response['childs'] + $response['infants'])){
						$class = $schedule[1]['infoOnClasses'][$i]['productClassDetail']['serviceClass'];
						$seat = $schedule[1]['infoOnClasses'][$i]['productClassDetail']['availabilityStatus'];

						if(in_array($class, $response['cabinClassOfEconomy']) && !$value_conn['class']['Economy']){
							$value_conn['class']['Economy'] = [
								"class" => $class,
								"seat"	=> $seat
							];
						}
						else if(in_array($class, $response['cabinClassOfBusiness']) && !$value_conn['class']['Executive']){
							$value_conn['class']['Executive'] = [
								"class" => $class,
								"seat"	=> $seat
							];
						}
						else if(in_array($class, $response['cabinClassOfFirst']) && !$value_conn['class']['FirstClass']){
							$value_conn['class']['FirstClass'] = [
								"class" => $class,
								"seat"	=> $seat
							];
						}
					}
				}

				$classes = [];
				$class_list = [];
				for($i=count($schedule[0]['infoOnClasses'])-1; $i>=0; $i--){
					if($schedule[0]['infoOnClasses'][$i]['productClassDetail']['availabilityStatus'] > ($response['adults'] + $response['childs'] + $response['infants'])){
						$class = $schedule[0]['infoOnClasses'][$i]['productClassDetail']['serviceClass'];
						$seat = $schedule[0]['infoOnClasses'][$i]['productClassDetail']['availabilityStatus'];

						if(in_array($class, $response['cabinClassOfEconomy'])){
							$class_detail = "Economy";

						}
						else if(in_array($class, $response['cabinClassOfBusiness'])){
							$class_detail = "Executive";
						}
						else if(in_array($class, $response['cabinClassOfFirst'])){
							$class_detail = "FirstClass";
						}

						if (isset($value_conn['class'][$class_detail]) && !isset($class_list[$class_detail])){
							$value = [
								"logId"			=> $response['logId'],
								"origin"		=> $response['origin'],
								"dest"			=> $response['dest'],
								"tripType"		=> $response['tripType'],
								"outDate"		=> $response['outDate'],
								"serviceClass"	=> $response['serviceClass'],
								"adults"		=> $response['adults'],
								"childs"		=> $response['childs'],
								"infants"		=> $response['infants'],
								"dep"			=> [
									"number"			=> $schedule[0]['number'],
						            "origin"			=> $schedule[0]['origin'],
						            "dest"				=> $schedule[0]['dest'],
						            "companyIdentifier" => $schedule[0]['companyIdentifier'],
						            "flightNumber"		=> $schedule[0]['flightNumber'],
						            "serviceClass"		=> $class,
						            "availabilityStatus"=> $seat,
						            "departureDatetime" => $schedule[0]['departureDatetime'],
						            "arrivalDatetime"	=> $schedule[0]['arrivalDatetime']
								]
							];

							$price_class = $schedule[0]['origin']."-".$schedule[0]['dest']."-".$class."-".$value_conn['dep']['origin']."-".$value_conn['dep']['dest']."-".$value_conn['class'][$class_detail]['class'];
							if (!isset($price_list[$price_class])){
								
								$value_1_b = $value_conn['dep'];
								$value_1_b['serviceClass'] = $value_conn['class'][$class_detail]['class'];
								$value_1_b['availabilityStatus'] = $value_conn['class'][$class_detail]['seat'];

								$price_list[$price_class] = [
									'price' => 0,
									'value' => ['value_1_a' => $value, 'value_1_b' => $value_1_b]
								];

							}

							$finalPrice = 0;

							// if($finalPrice){
								$codeClass = ($roundtrip == 'depart') ? 'D' : 'R';

								$scheduleClass = array(
									'class_id' => $codeClass . 'GA' . ($counterClass),
									'class_name' => $class,
									'class' => $class_detail,
									'price' => $finalPrice,
									'seat' => $seat,
									'value' => $ci->converter->encode(json_encode($value))
								);
								$class_list[$class_detail] = $scheduleClass;
								array_push($classes, $scheduleClass);
								$counterClass++;
							// }
						}
					}
				}
				if(count($classes) > 0){
					$detail_connecting['class'] = [[
						'class_name' => "X",
						'class' => "Economy",
						'price' => 0,
						'seat' => 9,
						'value' => $ci->converter->encode(json_encode($value_conn))
					]];

					$num_of_stop = $schedule[0]['additionalFlightInfo']['flightDetails']['numberOfStops'];
					$sch = array(
						'airline_id' => $this->airlines_id,
						'airline_name' => $this->airlines_name,
						'type' => "connecting",
						'from' => $departureLocation,
						'to' => $arrivalLocation,
						'via' => $num_of_stop > 0 ? (string)$num_of_stop : "-",
						'fno' => $flightNumber,
						'date' => $departureDate,
						'etd' => $departureTime,
						'eta' => $arrivalTime,
						'connecting_flight' => [$detail_connecting],
						'class' => $classes,
					);

					array_push($schedules, $sch);
				}
			}
		}
		// return $schedules;

		$new_price_list = $this->getPriceList($price_list);

		$new_schedules = [];
		$counterClass = 1;
		// var_dump($new_schedules);
		foreach ($response['infoOnClasses'] as $schedule) {
			if(count($schedule) == 1){
				$ETD = explode(" ", $schedule[0]['departureDatetime']);
				$ETA = explode(" ", $schedule[0]['arrivalDatetime']);
				$departureDate 	= $ETD[0];
				$departureTime 	= date("H:i", strtotime($ETD[1]));
				$arrivalDate 	= $ETA[0];
				$arrivalTime 	= date("H:i", strtotime($ETA[1]));
				$departureLocation = $schedule[0]['origin'];
				$arrivalLocation = $schedule[0]['dest'];

				$flightType = $schedule[0]['companyIdentifier'];
				$flightIdentification = $schedule[0]['flightNumber'];				
				$flightNumber = $flightType." ".$flightIdentification;	
				
				$classes = [];
				$class_list = [];

				for($i=count($schedule[0]['infoOnClasses'])-1; $i>=0; $i--){
					if($schedule[0]['infoOnClasses'][$i]['productClassDetail']['availabilityStatus'] >= ($response['adults'] + $response['childs'] + $response['infants'])){
						$class = $schedule[0]['infoOnClasses'][$i]['productClassDetail']['serviceClass'];
						$seat = $schedule[0]['infoOnClasses'][$i]['productClassDetail']['availabilityStatus'];

						if(in_array($class, $response['cabinClassOfEconomy'])){
							$class_detail = "Economy";
						}
						else if(in_array($class, $response['cabinClassOfBusiness'])){
							$class_detail = "Executive";
						}
						else if(in_array($class, $response['cabinClassOfFirst'])){
							$class_detail = "FirstClass";
						}

						if(!isset($class_list[$class_detail])){

							$value = [
								"logId"			=> $response['logId'],
								"origin"		=> $response['origin'],
								"dest"			=> $response['dest'],
								"tripType"		=> $response['tripType'],
								"outDate"		=> $response['outDate'],
								"serviceClass"	=> $response['serviceClass'],
								"adults"		=> $response['adults'],
								"childs"		=> $response['childs'],
								"infants"		=> $response['infants'],
								"dep"			=> [
									"number"			=> $schedule[0]['number'],
						            "origin"			=> $schedule[0]['origin'],
						            "dest"				=> $schedule[0]['dest'],
						            "companyIdentifier" => $schedule[0]['companyIdentifier'],
						            "flightNumber"		=> $schedule[0]['flightNumber'],
						            "serviceClass"		=> $class,
						            "availabilityStatus"=> $seat,
						            "departureDatetime" => $schedule[0]['departureDatetime'],
						            "arrivalDatetime"	=> $schedule[0]['arrivalDatetime']
								]
							];
							$price_class = $value['origin']."-".$value['dest']."-".$class;
							if (isset($new_price_list[$price_class])){
								$finalPrice = $new_price_list[$price_class];
							} else {
								$finalPrice = NULL;
							}

							if($finalPrice){
								$codeClass = ($roundtrip == 'depart') ? 'D' : 'R';

								$scheduleClass = array(
									'class_id' => $codeClass . 'GA' . ($counterClass),
									'class_name' => $class,
									'class' => $class_detail,
									'price' => $finalPrice,
									'seat' => $seat,
									'value' => $ci->converter->encode(json_encode($value))
								);
								$class_list[$class_detail] = $scheduleClass;
								array_push($classes, $scheduleClass);
								$counterClass++;
							}
						}
					}
				}

				if(count($classes) > 0){
					$num_of_stop = $schedule[0]['additionalFlightInfo']['flightDetails']['numberOfStops'];
					$sch = array(
						'airline_id' => $this->airlines_id,
						'airline_name' => $this->airlines_name,
						'type' => $num_of_stop > 0 ? "transit" : "direct",
						'from' => $departureLocation,
						'to' => $arrivalLocation,
						'via' => $num_of_stop > 0 ? (string)$num_of_stop : "-",
						'fno' => $flightNumber,
						'date' => $departureDate,
						'etd' => $departureTime,
						'eta' => $arrivalTime,
						'connecting_flight' => '',
						'class' => $classes,
					);

					array_push($new_schedules, $sch);
				}
			}
			else if (count($schedule) == 2) {
				$ETD = explode(" ", $schedule[0]['departureDatetime']);
				$ETA = explode(" ", $schedule[0]['arrivalDatetime']);
				$departureDate 	= $ETD[0];
				$departureTime 	= date("H:i", strtotime($ETD[1]));
				$arrivalDate 	= $ETA[0];
				$arrivalTime 	= date("H:i", strtotime($ETA[1]));
				$departureLocation = $schedule[0]['origin'];
				$arrivalLocation = $schedule[0]['dest'];

				$flightType = $schedule[0]['companyIdentifier'];
				$flightIdentification = $schedule[0]['flightNumber'];				
				$flightNumber = $flightType." ".$flightIdentification;	

				$ETD_conn = explode(" ", $schedule[1]['departureDatetime']);
				$ETA_conn = explode(" ", $schedule[1]['arrivalDatetime']);
				
				$num_of_stop_conn = $schedule[1]['additionalFlightInfo']['flightDetails']['numberOfStops'];
				$detail_connecting = array();
				$detail_connecting["airline_name"]	= $this->airlines_name;
				$detail_connecting["fno"]	= $schedule[1]['companyIdentifier']. " ". $schedule[1]['flightNumber'];
				$detail_connecting["from"]	= $schedule[1]['origin'];
				$detail_connecting["to"]	= $schedule[1]['dest'];
				$detail_connecting["via"] 	= $num_of_stop > 0 ? (string)$num_of_stop_conn : "-";
				$detail_connecting["etd"]	= date("H:i", strtotime($ETD_conn[1]));
				$detail_connecting["eta"]	= date("H:i", strtotime($ETA_conn[1]));
				$detail_connecting["date"]	= $ETD_conn[0];

				$value_conn = [
					'dep' => [
						"number"			=> $schedule[1]['number'],
			            "origin"			=> $schedule[1]['origin'],
			            "dest"				=> $schedule[1]['dest'],
			            "companyIdentifier" => $schedule[1]['companyIdentifier'],
			            "flightNumber"		=> $schedule[1]['flightNumber'],
			            "serviceClass"		=> "X",
			            "availabilityStatus"=> 0,
			            "departureDatetime" => $schedule[1]['departureDatetime'],
			            "arrivalDatetime"	=> $schedule[1]['arrivalDatetime']
					],
					"class" => NULL
				];

				for($i=count($schedule[1]['infoOnClasses'])-1; $i>=0; $i--){
					if($schedule[1]['infoOnClasses'][$i]['productClassDetail']['availabilityStatus'] > ($response['adults'] + $response['childs'] + $response['infants'])){
						$class = $schedule[1]['infoOnClasses'][$i]['productClassDetail']['serviceClass'];
						$seat = $schedule[1]['infoOnClasses'][$i]['productClassDetail']['availabilityStatus'];

						if(in_array($class, $response['cabinClassOfEconomy']) && !$value_conn['class']['Economy']){
							$value_conn['class']['Economy'] = [
								"class" => $class,
								"seat"	=> $seat
							];
						}
						else if(in_array($class, $response['cabinClassOfBusiness']) && !$value_conn['class']['Executive']){
							$value_conn['class']['Executive'] = [
								"class" => $class,
								"seat"	=> $seat
							];
						}
						else if(in_array($class, $response['cabinClassOfFirst']) && !$value_conn['class']['FirstClass']){
							$value_conn['class']['FirstClass'] = [
								"class" => $class,
								"seat"	=> $seat
							];
						}
					}
				}
				
				$classes = [];
				$class_list = [];
				for($i=count($schedule[0]['infoOnClasses'])-1; $i>=0; $i--){
					if($schedule[0]['infoOnClasses'][$i]['productClassDetail']['availabilityStatus'] > ($response['adults'] + $response['childs'] + $response['infants'])){
						$class = $schedule[0]['infoOnClasses'][$i]['productClassDetail']['serviceClass'];
						$seat = $schedule[0]['infoOnClasses'][$i]['productClassDetail']['availabilityStatus'];

						if(in_array($class, $response['cabinClassOfEconomy'])){
							$class_detail = "Economy";

						}
						else if(in_array($class, $response['cabinClassOfBusiness'])){
							$class_detail = "Executive";
						}
						else if(in_array($class, $response['cabinClassOfFirst'])){
							$class_detail = "FirstClass";
						}

						if (isset($value_conn['class'][$class_detail]) && !isset($class_list[$class_detail])){
							$value = [
								"logId"			=> $response['logId'],
								"origin"		=> $response['origin'],
								"dest"			=> $response['dest'],
								"tripType"		=> $response['tripType'],
								"outDate"		=> $response['outDate'],
								"serviceClass"	=> $response['serviceClass'],
								"adults"		=> $response['adults'],
								"childs"		=> $response['childs'],
								"infants"		=> $response['infants'],
								"dep"			=> [
									"number"			=> $schedule[0]['number'],
						            "origin"			=> $schedule[0]['origin'],
						            "dest"				=> $schedule[0]['dest'],
						            "companyIdentifier" => $schedule[0]['companyIdentifier'],
						            "flightNumber"		=> $schedule[0]['flightNumber'],
						            "serviceClass"		=> $class,
						            "availabilityStatus"=> $seat,
						            "departureDatetime" => $schedule[0]['departureDatetime'],
						            "arrivalDatetime"	=> $schedule[0]['arrivalDatetime']
								]
							];

							$price_class = $schedule[0]['origin']."-".$schedule[0]['dest']."-".$class."-".$value_conn['dep']['origin']."-".$value_conn['dep']['dest']."-".$value_conn['class'][$class_detail]['class'];
							if (isset($new_price_list[$price_class])){
								
								$finalPrice = $new_price_list[$price_class];

							} else {
								
								$finalPrice = NULL;

							}

							if($finalPrice){
								$codeClass = ($roundtrip == 'depart') ? 'D' : 'R';

								$scheduleClass = array(
									'class_id' => $codeClass . 'GA' . ($counterClass),
									'class_name' => $class,
									'class' => $class_detail,
									'price' => $finalPrice,
									'seat' => $seat,
									'value' => $ci->converter->encode(json_encode($value))
								);
								$class_list[$class_detail] = $scheduleClass;
								array_push($classes, $scheduleClass);
								$counterClass++;
							}
						}
					}
				}
				if(count($classes) > 0){
					$detail_connecting['class'] = [[
						'class_name' => "X",
						'class' => "Economy",
						'price' => 0,
						'seat' => 9,
						'value' => $ci->converter->encode(json_encode($value_conn))
					]];

					$num_of_stop = $schedule[0]['additionalFlightInfo']['flightDetails']['numberOfStops'];
					$sch = array(
						'airline_id' => $this->airlines_id,
						'airline_name' => $this->airlines_name,
						'type' => "connecting",
						'from' => $departureLocation,
						'to' => $arrivalLocation,
						'via' => $num_of_stop > 0 ? (string)$num_of_stop : "-",
						'fno' => $flightNumber,
						'date' => $departureDate,
						'etd' => $departureTime,
						'eta' => $arrivalTime,
						'connecting_flight' => [$detail_connecting],
						'class' => $classes,
					);

					array_push($new_schedules, $sch);
				}
			}
		}
		// echo json_encode($new_schedules);exit;
		return $new_schedules;
	}

	private function getPriceList($class_list){
		$ch = [];

		foreach($class_list as $key=>$pc) {
			$value = $pc['value']['value_1_a'];
			$value_conn = $pc['value']['value_1_b'];

			$dep = [$value['dep']];
			if( $value_conn ) $dep[] = $value_conn;

			$payload['method'] 	= 'Fare';
			$payload['Fare'] 	= [
				"entity"		=> "SA",
				"entityCode"	=> $this->entity_code,
				"username"		=> $this->entity_code,
				"logId"			=> $value['logId'],
				"origin"		=> $value['origin'],
				"dest"			=> $value['dest'],
				"tripType"		=> "o",
				"outDate"		=> $value['outDate'],
				"serviceClass"	=> "all",
				"adults"		=> $value['adults'],
				"childs"		=> $value['childs'],
				"infants"		=> $value['infants'],
				"remoteAddr"	=> $this->remote_address,
				"httpUserAgent" => $this->user_agent,
				"promotion"		=> ["entryCode" => ""],
				"select"		=> [
					"dep"	=> $dep
				]
			];

			$params	= [
				"param" => $payload,
				"url"	=> $this->garuda_endpoint,
				"auth"	=> $this->auth
			];
			$ch[$key] = curl_init();
			curl_setopt($ch[$key], CURLOPT_URL, $this->garuda_gateway);
	        curl_setopt($ch[$key], CURLOPT_POST, 1);
	        curl_setopt($ch[$key], CURLOPT_POSTFIELDS, "data=".json_encode($params));
	        curl_setopt($ch[$key], CURLOPT_RETURNTRANSFER, true);
		}
		
		$mh = curl_multi_init();
		foreach($ch as $c){
			curl_multi_add_handle($mh, $c);
		}

		// execute all queries simultaneously, and continue when all are complete
		$running = null;
		do {
			curl_multi_exec($mh, $running);
		} while ($running);

		$price_list = [];
		// all of our requests are done, we can now access the results
		foreach($ch as $k=>$c){
			$data[$k] = curl_multi_getcontent($c);
			$temp_data = json_decode($data[$k], true);
			if(!empty($temp_data) && $temp_data['error_no'] == "0"){
				if($temp_data['result']['login'] == true && $temp_data['result']['access'] == true && $temp_data['result']['success'] == true ){
					$price_list[$k] = ceil($temp_data['result']['Fare']['price']['adults']['netTotal']);
				}
			}
		}

		$this->saveLog($value['logId'], "getAllFare", json_encode($class_list), $data);

		return $price_list;
	}

	private function parse_all_schedules($roundtrip, $response)
	{
		$ci =& get_instance();

		$schedules = array();
		$counterClass = 1;
		$fee = $ci->mgeneral->getValue('fee_values',array('airlines_id'=>$this->airlinesId,'passenger_types'=>'adult'),'service_fees');
		// print_r($response);
		$price_eco = [];
		$price_exe = [];
		$price_fc  = [];
		foreach ($response['infoOnClasses'] as $schedule) {
			if(count($schedule) == 1){
				$ETD = explode(" ", $schedule[0]['departureDatetime']);
				$ETA = explode(" ", $schedule[0]['arrivalDatetime']);
				$departureDate 	= $ETD[0];
				$departureTime 	= date("H:i", strtotime($ETD[1]));
				$arrivalDate 	= $ETA[0];
				$arrivalTime 	= date("H:i", strtotime($ETA[1]));
				$departureLocation = $schedule[0]['origin'];
				$arrivalLocation = $schedule[0]['dest'];

				$flightType = $schedule[0]['companyIdentifier'];
				$flightIdentification = $schedule[0]['flightNumber'];				
				$flightNumber = $flightType." ".$flightIdentification;	
				
				$classes = [];

				for($i=count($schedule[0]['infoOnClasses'])-1; $i>=0; $i--){
					if($schedule[0]['infoOnClasses'][$i]['productClassDetail']['availabilityStatus'] >= ($response['adults'] + $response['childs'] + $response['infants'])){
						$class = $schedule[0]['infoOnClasses'][$i]['productClassDetail']['serviceClass'];
						$seat = $schedule[0]['infoOnClasses'][$i]['productClassDetail']['availabilityStatus'];

						// if (!empty($finalPrice)) {
							if(in_array($class, $response['cabinClassOfEconomy'])){
								$class_detail = "Economy";
							}
							else if(in_array($class, $response['cabinClassOfBusiness'])){
								$class_detail = "Executive";
							}
							else if(in_array($class, $response['cabinClassOfFirst'])){
								$class_detail = "FirstClass";
							}

							$value = [
								"logId"			=> $response['logId'],
								"origin"		=> $response['origin'],
								"dest"			=> $response['dest'],
								"tripType"		=> $response['tripType'],
								"outDate"		=> $response['outDate'],
								"serviceClass"	=> $response['serviceClass'],
								"adults"		=> $response['adults'],
								"childs"		=> $response['childs'],
								"infants"		=> $response['infants'],
								"dep"			=> [
									"number"			=> $schedule[0]['number'],
						            "origin"			=> $schedule[0]['origin'],
						            "dest"				=> $schedule[0]['dest'],
						            "companyIdentifier" => $schedule[0]['companyIdentifier'],
						            "flightNumber"		=> $schedule[0]['flightNumber'],
						            "serviceClass"		=> $class,
						            "availabilityStatus"=> $seat,
						            "departureDatetime" => $schedule[0]['departureDatetime'],
						            "arrivalDatetime"	=> $schedule[0]['arrivalDatetime']
								]
							];

							$codeClass = ($roundtrip == 'depart') ? 'D' : 'R';

							$scheduleClass = array(
								'class_id' => $codeClass . 'GA' . ($counterClass),
								'class_name' => $class,
								'class' => $class_detail,
								'price' => 0,
								'seat' => $seat,
								'value' => $ci->converter->encode(json_encode($value))
							);

							array_push($classes, $scheduleClass);
							$counterClass++;
						// }
					}
				}

				//if(count($classes) > 0){
					$num_of_stop = $schedule[0]['additionalFlightInfo']['flightDetails']['numberOfStops'];
					$sch = array(
						'airline_id' => $this->airlines_id,
						'airline_name' => $this->airlines_name,
						'type' => $num_of_stop > 0 ? "transit" : "direct",
						'from' => $departureLocation,
						'to' => $arrivalLocation,
						'via' => $num_of_stop > 0 ? (string)$num_of_stop : "-",
						'fno' => $flightNumber,
						'date' => $departureDate,
						'etd' => $departureTime,
						'eta' => $arrivalTime,
						'connecting_flight' => '',
						'class' => $classes,
					);

					array_push($schedules, $sch);
				//}
			}
			else if (count($schedule) == 2) {
				$ETD = explode(" ", $schedule[0]['departureDatetime']);
				$ETA = explode(" ", $schedule[0]['arrivalDatetime']);
				$departureDate 	= $ETD[0];
				$departureTime 	= date("H:i", strtotime($ETD[1]));
				$arrivalDate 	= $ETA[0];
				$arrivalTime 	= date("H:i", strtotime($ETA[1]));
				$departureLocation = $schedule[0]['origin'];
				$arrivalLocation = $schedule[0]['dest'];

				$flightType = $schedule[0]['companyIdentifier'];
				$flightIdentification = $schedule[0]['flightNumber'];				
				$flightNumber = $flightType." ".$flightIdentification;	

				$ETD_conn = explode(" ", $schedule[1]['departureDatetime']);
				$ETA_conn = explode(" ", $schedule[1]['arrivalDatetime']);
				
				$num_of_stop_conn = $schedule[1]['additionalFlightInfo']['flightDetails']['numberOfStops'];
				$detail_connecting = array();
				$detail_connecting["airline_name"]	= $this->airlines_name;
				$detail_connecting["fno"]	= $schedule[1]['companyIdentifier']. " ". $schedule[1]['flightNumber'];
				$detail_connecting["from"]	= $schedule[1]['origin'];
				$detail_connecting["to"]	= $schedule[1]['dest'];
				$detail_connecting["via"] 	= $num_of_stop > 0 ? (string)$num_of_stop_conn : "-";
				$detail_connecting["etd"]	= date("H:i", strtotime($ETD_conn[1]));
				$detail_connecting["eta"]	= date("H:i", strtotime($ETA_conn[1]));
				$detail_connecting["date"]	= $ETD_conn[0];

				$value_conn = [
					'dep' => [
						"number"			=> $schedule[1]['number'],
			            "origin"			=> $schedule[1]['origin'],
			            "dest"				=> $schedule[1]['dest'],
			            "companyIdentifier" => $schedule[1]['companyIdentifier'],
			            "flightNumber"		=> $schedule[1]['flightNumber'],
			            "serviceClass"		=> "X",
			            "availabilityStatus"=> 0,
			            "departureDatetime" => $schedule[1]['departureDatetime'],
			            "arrivalDatetime"	=> $schedule[1]['arrivalDatetime']
					],
					"class" => NULL
				];
				
				for($i=count($schedule[1]['infoOnClasses'])-1; $i>=0; $i--){
					if($schedule[1]['infoOnClasses'][$i]['productClassDetail']['availabilityStatus']>0){
						$class = $schedule[1]['infoOnClasses'][$i]['productClassDetail']['serviceClass'];
						$seat = $schedule[1]['infoOnClasses'][$i]['productClassDetail']['availabilityStatus'];

						// if (!empty($finalPrice)) {
							if(in_array($class, $response['cabinClassOfEconomy']) && !$value_conn['class']['Economy']){
								$value_conn['class']['Economy'] = [
									"class" => $class,
									"seat"	=> $seat
								];
							}
							else if(in_array($class, $response['cabinClassOfBusiness']) && !$value_conn['class']['Executive']){
								$value_conn['class']['Executive'] = [
									"class" => $class,
									"seat"	=> $seat
								];
							}
							else if(in_array($class, $response['cabinClassOfFirst']) && !$value_conn['class']['FirstClass']){
								$value_conn['class']['FirstClass'] = [
									"class" => $class,
									"seat"	=> $seat
								];
							}
						// }
					}
				}

				$classes = [];
				for($i=count($schedule[0]['infoOnClasses'])-1; $i>=0; $i--){
					if($schedule[0]['infoOnClasses'][$i]['productClassDetail']['availabilityStatus']>0){
						$class = $schedule[0]['infoOnClasses'][$i]['productClassDetail']['serviceClass'];
						$seat = $schedule[0]['infoOnClasses'][$i]['productClassDetail']['availabilityStatus'];

						// if (!empty($finalPrice)) {
							if(in_array($class, $response['cabinClassOfEconomy'])){
								$class_detail = "Economy";

							}
							else if(in_array($class, $response['cabinClassOfBusiness'])){
								$class_detail = "Executive";
							}
							else if(in_array($class, $response['cabinClassOfFirst'])){
								$class_detail = "FirstClass";
							}

							if (isset($value_conn['class'][$class_detail])){
								$value = [
									"logId"			=> $response['logId'],
									"origin"		=> $response['origin'],
									"dest"			=> $response['dest'],
									"tripType"		=> $response['tripType'],
									"outDate"		=> $response['outDate'],
									"serviceClass"	=> $response['serviceClass'],
									"adults"		=> $response['adults'],
									"childs"		=> $response['childs'],
									"infants"		=> $response['infants'],
									"dep"			=> [
										"number"			=> $schedule[0]['number'],
							            "origin"			=> $schedule[0]['origin'],
							            "dest"				=> $schedule[0]['dest'],
							            "companyIdentifier" => $schedule[0]['companyIdentifier'],
							            "flightNumber"		=> $schedule[0]['flightNumber'],
							            "serviceClass"		=> $class,
							            "availabilityStatus"=> $seat,
							            "departureDatetime" => $schedule[0]['departureDatetime'],
							            "arrivalDatetime"	=> $schedule[0]['arrivalDatetime']
									]
								];

								$codeClass = ($roundtrip == 'depart') ? 'D' : 'R';

								$scheduleClass = array(
									'class_id' => $codeClass . 'GA' . ($counterClass),
									'class_name' => $class,
									'class' => $class_detail,
									'price' => 0,
									'seat' => $seat,
									'value' => $ci->converter->encode(json_encode($value))
								);

								array_push($classes, $scheduleClass);
								$counterClass++;
							}
						// }
					}
				}
				if(count($classes) > 0){
					$detail_connecting['class'] = [[
						'class_name' => "X",
						'class' => "Economy",
						'price' => 0,
						'seat' => 9,
						'value' => $ci->converter->encode(json_encode($value_conn))
					]];

					$num_of_stop = $schedule[0]['additionalFlightInfo']['flightDetails']['numberOfStops'];
					$sch = array(
						'airline_id' => $this->airlines_id,
						'airline_name' => $this->airlines_name,
						'type' => "connecting",
						'from' => $departureLocation,
						'to' => $arrivalLocation,
						'via' => $num_of_stop > 0 ? (string)$num_of_stop : "-",
						'fno' => $flightNumber,
						'date' => $departureDate,
						'etd' => $departureTime,
						'eta' => $arrivalTime,
						'connecting_flight' => [$detail_connecting],
						'class' => $classes,
					);

					array_push($schedules, $sch);
				}
			}
		}

		return $schedules;
	}

	public function detail($post, $flightInt = NULL){
		$ci = & get_instance();

		$response = $this->reqFareDetails($post, $flightInt);

		if ($response['error_no'] == "0") {
			$schedule	= array("airline_is" => $this->airlines_id,
								"airline_name" => $this->airlines_name,
								"from" => $post['from'],
								"to" => $post["to"],
								"date" => $post["tglP"],
								"flight_no" =>"",
								"ETA" => ":",
								"ETD" => ":",
								"class" => "");

			$result = [
				"error_no" => "0",
				"sch_detail" 	=> $schedule,
				"priceAD" 		=> ceil($response['result']['price']['adults']['basicFare']),
				"priceCH" 		=> ceil($response['result']['price']['childs']['basicFare']),
				"priceIn" 		=> ceil($response['result']['price']['infants']['basicFare']),
				"service" 		=> ceil($response['result']['price']['adults']['taxsDetail']['D5CB']),
				"komisi" 		=> round($response['result']['fareData']['pax_pay'] - $response['result']['fareData']['agent_pay'])
			];
		} else {
			$result = array(
				'error_no' => '102',
				'error_message' => 'Kalkulasi harga error'
			);
		}

		return $result;
	}

	private function reqFareDetails($post_data, $flightInt = NULL) {
		$ci = & get_instance();

		$value = json_decode($post_data['value']['value_1_a'], true);
		$value_conn = json_decode($post_data['value']['value_1_b'], true);

		$dep = [$value['dep']];
		if( $value_conn ) $dep[] = $value_conn;

		$payload['method'] 	= 'Fare';
		$payload['Fare'] 	= [
			"entity"		=> "SA",
			"entityCode"	=> $this->entity_code,
			"username"		=> $this->entity_code,
			"logId"			=> $value['logId'],
			"origin"		=> $value['origin'],
			"dest"			=> $value['dest'],
			"tripType"		=> "o",
			"outDate"		=> $value['outDate'],
			"serviceClass"	=> "all",
			"adults"		=> $value['adults'],
			"childs"		=> $value['childs'],
			"infants"		=> $value['infants'],
			"remoteAddr"	=> $this->remote_address,
			"httpUserAgent" => $this->user_agent,
			"promotion"		=> ["entryCode" => ""],
			"select"		=> [
				"dep"	=> $dep
			]
		];

		$params	= [
			"param" => $payload,
			"url"	=> $this->garuda_endpoint,
			"auth"	=> $this->auth
		];

		$response = $this->request($this->garuda_gateway, "Fare", json_encode($params), $value['logId']);

		return $response;
	}

	public function book ($flight, $psgr, $customer){
		$ci = & get_instance();

		$response = $this->reqBook($flight, $psgr, $customer);

		if ($response['error_no'] == "0") {
			$time_limit = date('Y-m-d H:i:s',strtotime($response['result']['timelimit'] . ' - 15 minutes'));
			$result =  [
				"error_no" => "0",
				"book_code" => $response['result']['pnr'],
				"time_limit" => $time_limit,
				"harga_tiket" => ceil($response['result']['fareData']['pax_pay']),
				"komisi" => round($response['result']['fareData']['pax_pay'] - $response['result']['fareData']['agent_pay']),
				"price_adult" => ceil($response['result']['price']['adults']['basicFare']),
				"price_child" => ceil($response['result']['price']['childs']['basicFare']),
				"price_infant" => ceil($response['result']['price']['infants']['basicFare']),
				"tax_adult" => ceil($response['result']['price']['adults']['taxs']['ID']),
				"tax_child" => ceil($response['result']['price']['childs']['taxs']['ID']),
				"tax_infant" => ceil($response['result']['price']['infants']['taxs']['ID']),
				"totalTax" => ceil($response['result']['fareData']['all_tax']),
				"status" => "CONFIRMED"
			];

			$ci->mgeneral->save(["book_code" => $response['result']['pnr'], "invoice_number" => $response['result']['invoiceNumber'], "amount" => $response['result']['fareData']['agent_pay']], "book_garuda");
		} else {
			$result = $response;
		}
		
		return $result;
	}

	private function reqBook($flight, $psgr, $customer){
		$ci = & get_instance();

		$value = json_decode($flight['value']['value_1_a'], true);
		$value_conn = json_decode($flight['value']['value_1_b'], true);
		$dep = [$value['dep']];
		if( $value_conn ) $dep[] = $value_conn;

		$pax_detail = $this->setPaxDetails($psgr, $customer);

		$payload['method'] 	= 'Book';
		$payload['Book'] 	= [
			"entity"		=> "SA",
			"entityCode"	=> $this->entity_code,
			"username"		=> $this->entity_code,
			"logId"			=> $value['logId'],
			"origin"		=> $value['origin'],
			"dest"			=> $value['dest'],
			"tripType"		=> "o",
			"outDate"		=> $value['outDate'],
			"serviceClass"	=> "all",
			"adults"		=> $value['adults'],
			"childs"		=> $value['childs'],
			"infants"		=> $value['infants'],
			"remoteAddr"	=> $this->remote_address,
			"httpUserAgent" => $this->user_agent,
			"promotion"		=> ["entryCode" => ""],
			"select"		=> [
				"dep"	=> $dep
			],
			"paxDetail"		=> $pax_detail
		];

		$params	= [
			"param" => $payload,
			"url"	=> $this->garuda_endpoint,
			"auth"	=> $this->auth
		];
		// echo json_encode($pax_detail);exit;
		$response = $this->request($this->garuda_gateway, "Book", json_encode($params), $value['logId']);
		// $response = json_decode($this->sample_book(), true);
		return $response;
	}

	private function setPaxDetails($psgr, $customer){
		$ci =& get_instance();

		$cust_name = explode(" ", $customer['cust_name']);
		if(count($cust_name) == 1){
			$cust_first_name = $cust_name[0];
			$cust_last_name = $cust_name[0];
		}
		else if(count($cust_name) > 2){
			$cust_first_name = $cust_name[0];
			unset($cust_name[0]);
			$cust_last_name = implode("", trim($cust_name));
		}
		$pax_detail = [
			"contact"	=> [
				'title' 		=> "MR",
				"firstName"		=> $cust_first_name,
				'middleName'	=> "",
				"lastName"		=> $cust_last_name,
				"mobilePhone"	=> $customer['cust_phone'],
				"phone"			=> "",
				"email"			=> $customer['cust_email']
			],
			"adt"		=> [],
			"chd"		=> [],
			"inf"		=> []
		];

		foreach($psgr as $pax){
			if($pax['psgr_type'] == "adult"){
				$pax_detail['adt'][] = [
					"title"			=> strtoupper($pax['title']),
					"firstName"		=> $pax['first_name'],
					'middleName'	=> "",
					"lastName"		=> $pax['last_name'],
					"gff"			=> "",
					"email"			=> $customer['cust_email'],
					"birthDate"		=> $pax['birthdate']
				];
			} else if($pax['psgr_type'] == "child"){
				$datetime1 = date_create($pax['birthdate']); 
				$datetime2 = date_create(date("Y-m-d")); 
				$age = date_diff($datetime1, $datetime2);
				$age = $age->format("%y");
				$pax_detail['chd'][] = [
					"title"			=> ($pax['title'] == "chd") ? "MSTR" : strtoupper($pax['title']),
					"firstName"		=> $pax['first_name'],
					'middleName'	=> "",
					"lastName"		=> $pax['last_name'],
					"gff"			=> "",
					"birthDate"		=> $pax['birthdate'],
					"age"			=> $age
				];
			} else if($pax['psgr_type'] == "infant"){
				$datetime1 = date_create($pax['birthdate']); 
				$datetime2 = date_create(date("Y-m-d")); 
				$age = date_diff($datetime1, $datetime2);
				$age = $age->format("%y");
				$pax_detail['inf'][] = [
					"title"			=> ($pax['title'] == "inf") ? "MSTR" : strtoupper($pax['title']),
					"firstName"		=> $pax['first_name'],
					'middleName'	=> "",
					"lastName"		=> $pax['last_name'],
					"gff"			=> "",
					"birthDate"		=> $pax['birthdate'],
					"age"			=> $age
				];
			}
		}

		$counter = 0;
		for ($i = 0; $i < count($pax_detail["inf"]); $i++){
			$fullname_infant = trim($pax_detail["inf"][$i]['firstName'])." ".trim($pax_detail["inf"][$i]['lastName']);
			$fullname_adult = trim($pax_detail["adt"][$i]['firstName'])." ".trim($pax_detail["adt"][$i]['lastName']);

			$arr_fullname_inf = explode(" ", trim($fullname_infant));
			$arr_fullname_adt = explode(" ", trim($fullname_adult));

			if (count($arr_fullname_adt) == 1){
				$last_name_adt =  "";
				$first_name_adt = $arr_fullname_adt[0];	
			} else {
				$last_name_adt =  $arr_fullname_adt[count($arr_fullname_adt)-1];
				unset($arr_fullname_adt[count($arr_fullname_adt)-1]);
				$first_name_adt = implode(" ", $arr_fullname_adt);
			}

			if (count($arr_fullname_inf) == 1){
				$last_name_inf =  "";
				$first_name_inf = $arr_fullname_inf[0];	
			} else {
				$last_name_inf =  $arr_fullname_inf[count($arr_fullname_inf)-1];
				unset($arr_fullname_inf[count($arr_fullname_inf)-1]);
				$first_name_inf = implode(" ", $arr_fullname_inf);
			}
			
			$total_fullname = $last_name_adt."/".$first_name_adt." ".$pax_detail["adt"][$i]['title']."(ADT)(INF".$last_name_inf."/".$first_name_inf." ".$pax_detail["inf"][$i]['title']."/".date("dmy").")";
			if(strlen($total_fullname) > 58){
				$status = false;
				if($last_name_inf != ""){
					$j = 0;
					while($status != true && $j < count($arr_fullname_inf)){
						$arr_fullname_inf[$j] = $arr_fullname_inf[$j][0];
						$first_name_inf = implode(" ", $arr_fullname_inf);
						$total_fullname = $last_name_adt."/".$first_name_adt." ".$pax_detail["adt"][$i]['title']."(ADT)(INF".$last_name_inf."/".$first_name_inf." ".$pax_detail["inf"][$i]['title']."/".date("dmy").")";
						if(strlen($total_fullname) < 59){
							$status = true;
						}
						$j++;
					}
				}

				if($last_name_adt != ""){
					$j = 0;
					while($status != true && $j < count($arr_fullname_adt)){
						$arr_fullname_adt[$j] = $arr_fullname_adt[$j][0];
						$first_name_adt = implode(" ", $arr_fullname_adt);
						$total_fullname = $last_name_adt."/".$first_name_adt." ".$pax_detail["adt"][$i]['title']."(ADT)(INF".$last_name_inf."/".$first_name_inf." ".$pax_detail["inf"][$i]['title']."/".date("dmy").")";
						if(strlen($total_fullname) < 59){
							$status = true;
						}
						$j++;
					}
				}

				$pax_detail["adt"][$i]['firstName'] = $first_name_adt;
				$pax_detail["adt"][$i]['lastName'] = $last_name_adt;

				$pax_detail["inf"][$i]['firstName'] = $first_name_inf;
				$pax_detail["inf"][$i]['lastName'] = $last_name_inf;
			}
		}

		return $pax_detail;
	}

	public function issue($book_code){
		$ci = &get_instance();

		$book_detail = $ci->mgeneral->getWhere(["book_code" => $book_code], "book_garuda");

		if(count($book_detail) > 0){
			$payload['method'] 			= 'Transaction_Status';
			$payload['Transaction_Status'] 	= [
				"entity"		=> "SA",
				"entityCode"	=> $this->entity_code,
				"username"		=> $this->entity_code,
				"invoiceNumber"	=> $book_detail[0]->invoice_number,
				"remoteAddr"	=> $this->remote_address,
				"httpUserAgent" => $this->user_agent
			];

			$params	= [
				"param" => $payload,
				"url"	=> $this->garuda_endpoint,
				"auth"	=> $this->auth
			];

			$response = $this->request($this->garuda_gateway, "Transaction_Status", json_encode($params), $book_code);
			// $response = json_decode($this->sample_book(), true);
			
			if($response['error_no'] == "0" && isset($response['result']['tstData']) && $response['result']['tstData']['active'] == TRUE){
				$words = $this->shared_key_from_garuda.$book_detail[0]->invoice_number.$book_detail[0]->amount.".00".$response['result']['logId']."SUCCESS";
				$paymentWords = $book_detail[0]->invoice_number.$book_detail[0]->amount.".00".$this->entity_code.$this->shared_key_from_PSP;
				echo $words."<br>";
				echo $paymentWords."<br>";
				$payload2['method'] 			= 'Payment_Notify';
				$payload2['Payment_Notify'] 	= [
					"entity"			=> "SA",
					"entityCode"		=> $this->entity_code,
					"username"			=> $this->entity_code,
					"invoiceNumber"		=> $book_detail[0]->invoice_number,
					"amount"			=> $book_detail[0]->amount,
					"referenceNumber"	=> $response['result']['logId'],
					"responseCode"		=> '00',
					"status"			=> "SUCEESS",
					"paymentMethod"		=> "ODU",
					"paymentChannel"	=> 73,
					"words"				=> sha1($words),
					"paymentWords"		=> sha1($paymentWords),
					"remoteAddr"		=> $this->remote_address,
					"httpUserAgent" 	=> $this->user_agent
				];

				$params	= [
					"param" => $payload2,
					"url"	=> $this->garuda_endpoint,
					"auth"	=> $this->auth
				];


				$response = $this->request($this->garuda_gateway, "Payment_Notify", json_encode($params), $book_code);
				echo "<pre>";print_r($response);
			}
		}
		
		return true;
	}

	public function getTicketInfo($book_code, $psgr){
		$ci = &get_instance();

		$book_detail = $ci->mgeneral->getWhere(["book_code" => $book_code], "book_garuda");
		
		if(count($book_detail) > 0){
			$payload['method'] 			= 'Ticket';
			$payload['Ticket'] 	= [
				"entity"		=> "SA",
				"entityCode"	=> $this->entity_code,
				"username"		=> $this->entity_code,
				"invoiceNumber"	=> $book_detail[0]->invoice_number,
				"remoteAddr"	=> $this->remote_address,
				"httpUserAgent" => $this->user_agent
			];

			$params	= [
				"param" => $payload,
				"url"	=> $this->garuda_endpoint,
				"auth"	=> $this->auth
			];

			$params	= [
				"param" => $payload,
				"url"	=> $this->garuda_endpoint,
				"auth"	=> $this->auth
			];

			$response = $this->request($this->garuda_gateway, "Ticket", json_encode($params));

			if($response['error_no'] == "0"){
				$pax_detail = $this->setPaxDetailsForTicket($psgr);
				$data = [];
				foreach($pax_detail as $px){
					foreach($response['result']['tickets'] as $ticket){
						if(strtoupper($px['lastName']."/".$px['firstName']." ".$px['title']) == strtoupper($ticket['name'])){
							$data[] = [
								'flights_name' 		=> $px['full_name'],
								'flights_ticket'	=> $ticket['ticketNumber']
							];
							break;
						}
					}
				}
				$result = array("error_no"	=> "0",
								"status"	=> "TKT",
								"pax"		=> $data);
			}
			else {
				$result = $response;
			}
		} else {
			$result = [
				'error_no' 	=> "717",
				"error_msg"	=> "book code not found"
			];
		}

		return $result;
	}

	private function setPaxDetailsForTicket($psgr){
		$ci =& get_instance();
		$pax_detail = [
			"adt"		=> [],
			"chd"		=> [],
			"inf"		=> []
		];
		$psgr = json_decode(json_encode($psgr), true);
		foreach($psgr as $pax){
			if($pax['passengers_type'] == "adult"){
				$pax_detail['adt'][] = [
					"title"			=> strtoupper($pax['passengers_title']),
					"firstName"		=> $pax['passengers_first_name'],
					'middleName'	=> "",
					"lastName"		=> $pax['passengers_last_name'],
					"full_name"		=> $pax['passengers_first_name']." ".$pax['passengers_last_name'],
					"gff"			=> "",
					"email"			=> "",
					"birthDate"		=> ""
				];
			} else if($pax['passengers_type'] == "child"){
				$datetime1 = date_create($pax['passengers_dob']); 
				$datetime2 = date_create(date("Y-m-d")); 
				$age = date_diff($datetime1, $datetime2);
				$age = $age->format("%y");
				$pax_detail['chd'][] = [
					"title"			=> ($pax['passengers_title'] == "chd") ? "MSTR" : strtoupper($pax['passengers_title']),
					"firstName"		=> $pax['passengers_first_name'],
					'middleName'	=> "",
					"lastName"		=> $pax['passengers_last_name'],
					"full_name"		=> $pax['passengers_first_name']." ".$pax['passengers_last_name'],
					"gff"			=> "",
					"birthDate"		=> $pax['passengers_dob'],
					"age"			=> $age
				];
			} else if($pax['passengers_type'] == "infant"){
				$datetime1 = date_create($pax['passengers_dob']); 
				$datetime2 = date_create(date("Y-m-d")); 
				$age = date_diff($datetime1, $datetime2);
				$age = $age->format("%y");
				$pax_detail['inf'][] = [
					"title"			=> ($pax['passengers_title'] == "inf") ? "MSTR" : strtoupper($pax['passengers_title']),
					"firstName"		=> $pax['passengers_first_name'],
					'middleName'	=> "",
					"lastName"		=> $pax['passengers_last_name'],
					"full_name"		=> $pax['passengers_first_name']." ".$pax['passengers_last_name'],
					"gff"			=> "",
					"birthDate"		=> $pax['passengers_dob'],
					"age"			=> $age
				];
			}
		}

		$counter = 0;
		for ($i = 0; $i < count($pax_detail["inf"]); $i++){
			$fullname_infant = trim($pax_detail["inf"][$i]['firstName'])." ".trim($pax_detail["inf"][$i]['lastName']);
			$fullname_adult = trim($pax_detail["adt"][$i]['firstName'])." ".trim($pax_detail["adt"][$i]['lastName']);

			$arr_fullname_inf = explode(" ", trim($fullname_infant));
			$arr_fullname_adt = explode(" ", trim($fullname_adult));

			if (count($arr_fullname_adt) == 1){
				$last_name_adt =  "";
				$first_name_adt = $arr_fullname_adt[0];	
			} else {
				$last_name_adt =  $arr_fullname_adt[count($arr_fullname_adt)-1];
				unset($arr_fullname_adt[count($arr_fullname_adt)-1]);
				$first_name_adt = implode(" ", $arr_fullname_adt);
			}

			if (count($arr_fullname_inf) == 1){
				$last_name_inf =  "";
				$first_name_inf = $arr_fullname_inf[0];	
			} else {
				$last_name_inf =  $arr_fullname_inf[count($arr_fullname_inf)-1];
				unset($arr_fullname_inf[count($arr_fullname_inf)-1]);
				$first_name_inf = implode(" ", $arr_fullname_inf);
			}
			
			$total_fullname = $last_name_adt."/".$first_name_adt." ".$pax_detail["adt"][$i]['title']."(ADT)(INF".$last_name_inf."/".$first_name_inf." ".$pax_detail["inf"][$i]['title']."/".date("dmy").")";
			if(strlen($total_fullname) > 58){
				$status = false;
				if($last_name_inf != ""){
					$j = 0;
					while($status != true && $j < count($arr_fullname_inf)){
						$arr_fullname_inf[$j] = $arr_fullname_inf[$j][0];
						$first_name_inf = implode(" ", $arr_fullname_inf);
						$total_fullname = $last_name_adt."/".$first_name_adt." ".$pax_detail["adt"][$i]['title']."(ADT)(INF".$last_name_inf."/".$first_name_inf." ".$pax_detail["inf"][$i]['title']."/".date("dmy").")";
						if(strlen($total_fullname) < 59){
							$status = true;
						}
						$j++;
					}
				}

				if($last_name_adt != ""){
					$j = 0;
					while($status != true && $j < count($arr_fullname_adt)){
						$arr_fullname_adt[$j] = $arr_fullname_adt[$j][0];
						$first_name_adt = implode(" ", $arr_fullname_adt);
						$total_fullname = $last_name_adt."/".$first_name_adt." ".$pax_detail["adt"][$i]['title']."(ADT)(INF".$last_name_inf."/".$first_name_inf." ".$pax_detail["inf"][$i]['title']."/".date("dmy").")";
						if(strlen($total_fullname) < 59){
							$status = true;
						}
						$j++;
					}
				}

				$pax_detail["adt"][$i]['firstName'] = $first_name_adt;
				$pax_detail["adt"][$i]['lastName'] = $last_name_adt;

				$pax_detail["inf"][$i]['firstName'] = $first_name_inf;
				$pax_detail["inf"][$i]['lastName'] = $last_name_inf;
			}
		}

		// return $pax_detail;
		return array_merge($pax_detail['adt'], $pax_detail['chd'], $pax_detail['inf']);
	}

	private function saveLog($apilog_code, $action, $param, $result){
		$ci = & get_instance();
		$post	= array("apilog_code" => $apilog_code,
						"apilog_action" => $action,
						"apilog_post" => $param,
						"apilog_response" => json_encode($result),
						"apilog_date" => date("Y-m-d H:i:s"));

		#$ci->load->database('default', TRUE);
		#return $ci->mgeneral->save($post, 'apilog_garuda');
	}

	private function request($endpoint, $action, $parameter, $apilog_code=NULL)
	{
		$ci =& get_instance();

		$ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "data=".$parameter);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        
        $output = curl_exec ($ch);
        #print_r($output);
        $output = json_decode($output, true);
        $error = false;
        if (!empty(curl_error($ch))) {
        	$error = true;
        	$error_msg = curl_error($ch);
        } 
        else if ($output['error_no'] != "0"){
        	$error = true;
        	$error_msg = $output['error_msg'];
        }
        else if (!$output['result']){
        	$error = true;
        	$error_msg = "Terjadi kesalahan pada sistem";
        }
        else if (!$output['result']['success'] && $output['result']['errorMessages']){
        	$error = true;
        	$error_msg = $output['result']['errorMessages'][$output['result']['errorCode']];
        } 
        else if ($action != "TICKET" && !$output['result']['success']){
        	$error = true;
        	$error_msg = $output['result']['errorException'];
        } else if ($action == "TICKET" && $output['result']['responseCode'] != "0000" && $output['result']['responseMessage'] != "SUCCESS"){
        	$error = true;
        	$error_msg = $output['result']['responseMessage'];
        }
        else {
        	$response = [
        		"error_no" 	=> "0",
        		"result"	=> $output['result'][$action]
        	];
        }

        if ($error){
        	$apilog_code = $apilog_code ? $apilog_code : 'error '.$action;
        	$this->saveLog($apilog_code, $action, $parameter, $output);

        	$response = [
        		"error_no" => "717",
        		"error_msg" => $error_msg
        	];
        } else {
        	$apilog_code = $apilog_code ? $apilog_code : $response['result']['logId'];
        	$this->saveLog($apilog_code, $action, $parameter, $output);
        }


        curl_close ($ch);

        return $response;
	}

	private function sample_book(){
		return '{"error_no":"0","result":{"logId":"1549209901-5de933bda93f2057332d058aa8524f9a7b513193","SessionId":"F434E5JO3R0NQK9HUNHG8DBNN0:EFC3BC32F7556D33A783BAB30E676510668F25CA","entity":"SA","entityCode":"SA3AEET","username":"SA3AEET","invoiceNumber":"R57CUS190203","controlNumber":"R57CUS","status":"ord","pnrDetail":{"pnrHeader":{"reservationInfo":{"reservation":{"companyId":"1A","controlNumber":"R57CUS","date":"030219","time":"1604"}}},"securityInformation":{"responsibilityInformation":{"typeOfPnrElement":"RP","agentId":"WSSU","officeId":"JKTGI28BT","iataCode":15394654},"queueingInformation":{"queueingOfficeId":"JKTGI28BT"},"cityCode":"JKT","secondRpInformation":{"creationOfficeId":"JKTGI28BT","agentSignature":"9999WS","creationDate":30219,"creatorIataCode":15394654,"creationTime":"1603"}},"freetextData":{"freetextDetail":{"subjectQualifier":"3","type":"P12"},"longFreetext":"--- TST RLR ---"},"pnrHeaderTag":{"statusInformation":[{"indicator":"TST"},{"indicator":"RLR"}]},"sbrPOSDetails":{"sbrUserIdentificationOwn":{"originIdentification":{"originatorId":15394654,"inHouseIdentification1":"JKTGI28BT"},"originatorTypeCode":"A"},"sbrSystemDetails":{"deliveringSystem":{"companyId":"1A","locationId":"JKT"}},"sbrPreferences":{"userPreferences":{"codedCountry":"ID"}}},"sbrCreationPosDetails":{"sbrUserIdentificationOwn":{"originIdentification":{"originatorId":15394654,"inHouseIdentification1":"JKTGI28BT"},"originatorTypeCode":"A"},"sbrSystemDetails":{"deliveringSystem":{"companyId":"1A","locationId":"JKT"}},"sbrPreferences":{"userPreferences":{"codedCountry":"ID"}}},"sbrUpdatorPosDetails":{"sbrUserIdentificationOwn":{"originIdentification":{"originatorId":15394654,"inHouseIdentification1":"JKTGI28BT"},"originatorTypeCode":"A"},"sbrSystemDetails":{"deliveringSystem":{"companyId":"1A","locationId":"JKT"}},"sbrPreferences":{"userPreferences":{"codedCountry":"ID"}}},"technicalData":{"enveloppeNumberData":{"sequenceDetails":{"number":"3"}},"lastTransmittedEnvelopeNumber":{"currentRecord":2},"purgeDateData":{"dateTime":{"year":2019,"month":"5","day":"7"}}},"travellerInfo":{"elementManagementPassenger":{"reference":{"qualifier":"PT","number":2},"segmentName":"NM","lineNumber":1},"passengerData":{"travellerInformation":{"traveller":{"surname":"BUDI PERMADI","quantity":1},"passenger":{"firstName":"ROJALI MR","type":"ADT"}}}},"originDestinationDetails":{"originDestination":[],"itineraryInfo":{"elementManagementItinerary":{"reference":{"qualifier":"ST","number":1},"segmentName":"AIR","lineNumber":2},"travelProduct":{"product":{"depDate":"030519","depTime":"0535","arrDate":"030519","arrTime":"0840"},"boardpointDetail":{"cityCode":"CGK"},"offpointDetail":{"cityCode":"DPS"},"companyDetail":{"identification":"GA"},"productDetails":{"identification":"400","classOfService":"Y"},"typeDetail":{"detail":"ET"}},"itineraryMessageAction":{"business":{"function":"1"}},"itineraryReservationInfo":{"reservation":{"companyId":"GA","controlNumber":"R57CUS"}},"relatedProduct":{"quantity":1,"status":"HK"},"flightDetail":{"productDetails":{"equipment":"738","numOfStops":0,"weekDay":5},"departureInformation":{"departTerminal":"3"},"arrivalStationInfo":{"terminal":"D"},"timeDetail":{"checkinTime":"0450"},"facilities":{"entertainement":"M","entertainementDescription":"H"}},"selectionDetails":{"selection":{"option":"P2"}},"markerRailTour":[]}},"dataElementsMaster":{"marker2":[],"dataElementsIndiv":[{"elementManagementData":{"reference":{"qualifier":"OT","number":3},"segmentName":"AP","lineNumber":3},"otherDataFreetext":{"freetextDetail":{"subjectQualifier":"3","type":"P02"},"longFreetext":"ROJALI.BUDI@GMAIL.COM"}},{"elementManagementData":{"reference":{"qualifier":"OT","number":2},"segmentName":"AP","lineNumber":4},"otherDataFreetext":{"freetextDetail":{"subjectQualifier":"3","type":"7"},"longFreetext":"H-081321798398"}},{"elementManagementData":{"reference":{"qualifier":"OT","number":4},"segmentName":"AP","lineNumber":5},"otherDataFreetext":{"freetextDetail":{"subjectQualifier":"3","type":"5","status":"N"},"longFreetext":"E+ROJALI.BUDI@GMAIL.COM"},"referenceForDataElement":{"reference":{"qualifier":"PT","number":"2"}}},{"elementManagementData":{"reference":{"qualifier":"OT","number":8},"segmentName":"TK","lineNumber":6},"ticketElement":{"passengerType":"PAX","ticket":{"indicator":"OK","date":30219,"officeId":"JKTGI28BT"}}},{"elementManagementData":{"reference":{"qualifier":"OT","number":12},"segmentName":"OPC","lineNumber":7},"optionElement":{"optionElementInfo":{"mainOffice":"JKTGI28BT","date":"050219","queue":1,"category":8,"freetext":"GA CANCELLATION DUE TO NO TICKET JKT TIME ZONE","time":"2303"}},"referenceForDataElement":{"reference":[{"qualifier":"ST","number":"1"},{"qualifier":"PT","number":"2"}]}},{"elementManagementData":{"reference":{"qualifier":"OT","number":15},"segmentName":"FE","lineNumber":8},"otherDataFreetext":{"freetextDetail":{"subjectQualifier":"3","type":"10"},"longFreetext":"PAX *M***SA3AEET - AEROTICKETDOTCOM** - **0-2** - CONDITION APPLIES"},"referenceForDataElement":{"reference":[{"qualifier":"ST","number":"1"},{"qualifier":"PT","number":"2"}]}},{"elementManagementData":{"reference":{"qualifier":"OT","number":5},"segmentName":"FM","lineNumber":9},"otherDataFreetext":{"freetextDetail":{"subjectQualifier":"3","type":"11"},"longFreetext":"PAX *M*2"}},{"elementManagementData":{"reference":{"qualifier":"OT","number":16},"segmentName":"FV","lineNumber":10},"otherDataFreetext":{"freetextDetail":{"subjectQualifier":"3","type":"P18"},"longFreetext":"PAX GA"},"referenceForDataElement":{"reference":[{"qualifier":"ST","number":"1"},{"qualifier":"PT","number":"2"}]}},{"elementManagementData":{"reference":{"qualifier":"OT","number":6},"segmentName":"FZ","lineNumber":11},"otherDataFreetext":{"freetextDetail":{"subjectQualifier":"3","type":"P19"},"longFreetext":"PAX OTAGA-SA3AEET/JKT"}},{"elementManagementData":{"reference":{"qualifier":"OT","number":7},"segmentName":"FZ","lineNumber":12},"otherDataFreetext":{"freetextDetail":{"subjectQualifier":"3","type":"P19"},"longFreetext":"PAX OTAGA-PMT_DEPOSIT-GTN"}}]},"tstData":{"tstGeneralInformation":{"generalInformation":{"tstReferenceNumber":"1","tstCreationDate":"030219"}},"tstFreetext":[{"freetextDetail":{"subjectQualifier":"3","type":"41"},"longFreetext":"PAX"},{"freetextDetail":{"subjectQualifier":"3","type":"37"},"longFreetext":"JKT GA DPS1651000.00IDR1651000.00END"}],"fareBasisInfo":{"fareElement":{"primaryCode":"YOW","baggageAllowance":"20K"}},"fareData":{"issueIdentifier":"F","monetaryInfo":[{"qualifier":"F","amount":"1651000","currencyCode":"IDR"},{"qualifier":"T","amount":"1951100","currencyCode":"IDR"}],"taxFields":[{"taxIndicator":"X","taxCurrency":"IDR","taxAmount":"5000","taxCountryCode":"YR","taxNatureCode":"VB"},{"taxIndicator":"X","taxCurrency":"IDR","taxAmount":"130000","taxCountryCode":"D5","taxNatureCode":"CB"},{"taxIndicator":"X","taxCurrency":"IDR","taxAmount":"165100","taxCountryCode":"ID","taxNatureCode":"LO"}]},"segmentAssociation":{"selection":[{"option":"N"},{"option":"I"}]},"referenceForTstData":{"reference":[{"qualifier":"PT","number":"2"},{"qualifier":"ST","number":"1"}]}}},"tstData":{"active":true},"transdate":"2019-02-03","transtime":"2019-02-03 23:03:50","timelimit":"2019-02-04 00:49:23","statusMessage":"New Orders","responseCode":"9001","responseMessage":"New Orders"}}';
	}
}