<?php
class Citilink
{
    var $system     = "http://182.23.35.135:8008/";
    var $domain     = "EXT";
    var $agentName  = "Api_Gih";
    var $password   = "@P1_gih2017";
    var $roleCode   = "APIA";
    var $promo_stat = true;
    var $promo_code = "TDF";
    
    private $airlines_id    = '1';
    private $airlines_code  = 'QG';
    private $airlines_name  = 'Citilink';
    
    #URL DEV API CITILINK
    #private $url_session = 'http://182.23.35.135:3003/sessionmanager.svc?wsdl';
    #private $url_transaction = 'http://182.23.35.135:3003/BookingManager.svc?wsdl';
    
    
    /*
    #URL LIVE CITILINK 1
    private $url_session = 'http://182.23.35.135:8008/SessionManager.svc?wsdl';
    private $url_transaction = 'http://182.23.35.135:8008/BookingManager.svc?wsdl';
    
    
    
    #URL LIVE CITILINK 2
    // private $url_session = 'http://118.97.213.237:8008/SessionManager.svc?wsdl';
    // private $url_transaction = 'http://118.97.213.237:8008/BookingManager.svc?wsdl';
    // private $url_session = 'http://api-book.citilink.co.id/SessionManager.svc?wsdl';
    // private $url_transaction = 'http://api-book.citilink.co.id/BookingManager.svc?wsdl';*/
    private $url_session = 'https://api-book.ipqgpx.co.id/SessionManager.svc?wsdl';
    private $url_transaction = 'https://api-book.ipqgpx.co.id/BookingManager.svc?wsdl';
    
    
    function __construct()
    {
        $ci = & get_instance();
        $ci->load->library('curl');
        $ci->load->library('parsing');
    }    
     
    function search($roundtrip='return', $from, $to, $depart_date, $return_date, $adult, $child, $infant, $flight_int=null){
        $ci = & get_instance();
        if($roundtrip == "return"){
            $param1     = array('type'          => 'depart',
                                'from'          => $from,
                                'to'            => $to,
                                'depart_date'   => $depart_date,
                                'return_date'   => "",
                                "adult"         => $adult,
                                "child"         => $child,
                                "infant"        => $infant);
            $opts = array( CURLOPT_RETURNTRANSFER => true, CURLOPT_FOLLOWLOCATION => true, CURLOPT_SSL_VERIFYPEER => false, CURLOPT_SSL_VERIFYHOST => false, CURLOPT_TIMEOUT => '300', CURLOPT_POST=>true, CURLOPT_POSTFIELDS => $param1);
                
            $url    = base_url()."search/search_citilink/";
            $ci->curlmulti->addSession("$url", $opts );
            
            $param2     = array('type'          => 'return',
                                'from'          => $to,
                                'to'            => $from,
                                'depart_date'   => $return_date,
                                'return_date'   => "",
                                "adult"         => $adult,
                                "child"         => $child,
                                "infant"        => $infant);
            $opts = array( CURLOPT_RETURNTRANSFER => true, CURLOPT_FOLLOWLOCATION => true, CURLOPT_SSL_VERIFYPEER => false, CURLOPT_SSL_VERIFYHOST => false, CURLOPT_TIMEOUT => '300', CURLOPT_POST=>true, CURLOPT_POSTFIELDS => $param2);
            
            $url    = base_url()."search/search_citilink/";
            $ci->curlmulti->addSession("$url", $opts);
            
            $result_curl = $ci->curlmulti->exec();
            $ci->curlmulti->clear();
            
            $success_count = 0;
            foreach($result_curl as $rc){
                $rc2 = json_decode($rc, true);
                if($rc2['error_no'] == "0"){
                    $type = $rc2["type"];
                    $result[$type] = $rc2[$type];
                    $result[$type."_ref"] = $rc2[$type."_ref"];
                    $success_count++;
                }
                else{
                    $type = $rc2['type'];
                    $result[$type] = "";
                    $result[$type."_ref"] = "";
                }
            }

            if($success_count == 0){
                $rc3 = json_decode($result_curl[0], true);
                $result["error_no"] = $rc3['error_no'];
                $result["error_msg"] = $rc3['error_msg'];
            }
            else{
                $result["error_no"] = "0";
            }
        }
        else{
        
            $result_curl = $this->searchCitilink('depart', $from, $to, $depart_date, $return_date, $adult, $child, $infant);
            if($result_curl['error_no'] == "0"){
                $result["error_no"] = "0";
                $result["depart"]   = $result_curl["depart"];
                $result["depart_ref"] = $result_curl["depart_ref"];
            }
            else{
                $result["error_no"] = $result_curl['error_no'];
                $result["error_msg"] = $result_curl['error_msg'];
            }
            
        }
        
        if($result["error_no"] == "0"){
            unset($result["error_no"]);
            $airInfo['airlines_id']         = $this->airlines_id;
            $airInfo['airlines_code']       = $this->airlines_code;
            $airInfo['airlines_name']       = $this->airlines_name;
            
            $SchInfo['roundtrip']           = $roundtrip;
            $SchInfo['from']                = $from;
            $SchInfo['to']                  = $to;
            $SchInfo['depart']              = $depart_date;
            $SchInfo['return']              = $return_date;
            $SchInfo['adult']               = $adult;
            $SchInfo['child']               = $child;
            $SchInfo['infant']              = $infant;
            $SchInfo['page_ref']            = $ci->converter->encode($roundtrip."|".$from."|".$to."|".$depart_date."|".$return_date."|".$adult."|".$child."|".$infant);
            
            
            $data   = array('rest_no'      => "0",
                            'search_info'   => $SchInfo,
                            'airlines_det'  => $airInfo,
                            'schedule'      => $result);
        }
        else{
            $data = $result;
        }
        
        return $data;
    }
    
    function searchCitilink($type, $from, $to, $depart_date, $return_date, $adult, $child, $infant){
        $ci = & get_instance();
    
        $signature = $this->logon();
        
        if($signature['error_no'] == '0'){
            $total_pax = $adult + $child;
            $pax = '';
            for($i = 0; $i < $adult; $i++){
                $pax = $pax.'<PaxPriceType>
                                <PaxType>ADT</PaxType>
                                <PaxDiscountCode i:nil="true"></PaxDiscountCode>
                            </PaxPriceType>';
            }
            
            for($i = 0; $i < $child; $i++){
                $pax = $pax.'<PaxPriceType>
                                <PaxType>CHD</PaxType>
                                <PaxDiscountCode i:nil="true"></PaxDiscountCode>
                            </PaxPriceType>';
            }
            $header[] = array('http://schemas.navitaire.com/WebServices', 'ContractVersion', '0');
            $header[] = array('http://schemas.navitaire.com/WebServices', 'Signature', $signature['signature']);
            
            $apilog_code = $ci->converter->encode($signature['signature']);
            $param  =  '<GetAvailabilityRequest xmlns="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService">
                        <TripAvailabilityRequest xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Booking" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                            <AvailabilityRequests>
                                <AvailabilityRequest>
                                    <DepartureStation>'.$from.'</DepartureStation>
                                    <ArrivalStation>'.$to.'</ArrivalStation>
                                    <BeginDate>'.$depart_date.'T00:00:00</BeginDate>
                                    <EndDate>'.$depart_date.'T00:00:00</EndDate>
                                    <CarrierCode>QG</CarrierCode>
                                    <FlightNumber i:nil="true"></FlightNumber>
                                    <FlightType>All</FlightType>
                                    <PaxCount>'.$total_pax.'</PaxCount>
                                    <Dow>Daily</Dow>
                                    <CurrencyCode>IDR</CurrencyCode>
                                    <DisplayCurrencyCode i:nil="true"></DisplayCurrencyCode>
                                    <DiscountCode i:nil="true"></DiscountCode>
                                    <PromotionCode i:nil="true"></PromotionCode>
                                    <AvailabilityType>Default</AvailabilityType>
                                    <SourceOrganization i:nil="true"></SourceOrganization>
                                    <MaximumConnectingFlights>10</MaximumConnectingFlights>
                                    <AvailabilityFilter>Default</AvailabilityFilter>
                                    <FareClassControl>Default</FareClassControl>
                                    <MinimumFarePrice>0</MinimumFarePrice>
                                    <MaximumFarePrice>0</MaximumFarePrice>
                                    <ProductClassCode i:nil="true"></ProductClassCode>
                                    <SSRCollectionsMode>None</SSRCollectionsMode>
                                    <InboundOutbound>Both</InboundOutbound>
                                    <NightsStay>0</NightsStay>
                                    <IncludeAllotments>false</IncludeAllotments>
                                    <BeginTime i:nil="true"></BeginTime>
                                    <EndTime i:nil="true"></EndTime>
                                    <DepartureStations i:nil="true" xmlns:a="http://schemas.microsoft.com/2003/10/Serialization/Arrays"></DepartureStations>
                                    <ArrivalStations i:nil="true" xmlns:a="http://schemas.microsoft.com/2003/10/Serialization/Arrays"></ArrivalStations>
                                    <FareTypes i:nil="true" xmlns:a="http://schemas.microsoft.com/2003/10/Serialization/Arrays"></FareTypes>
                                    <ProductClasses i:nil="true" xmlns:a="http://schemas.microsoft.com/2003/10/Serialization/Arrays"></ProductClasses>
                                    <FareClasses i:nil="true" xmlns:a="http://schemas.microsoft.com/2003/10/Serialization/Arrays"></FareClasses>
                                    <PaxPriceTypes>
                                        '.$pax.'
                                    </PaxPriceTypes>
                                    <JourneySortKeys xmlns:a="http://schemas.navitaire.com/WebServices/DataContracts/Common/Enumerations">
                                        <a:JourneySortKey>EarliestDeparture</a:JourneySortKey>
                                        <a:JourneySortKey>LowestFare</a:JourneySortKey>
                                    </JourneySortKeys>
                                    <TravelClassCodes i:nil="true" xmlns:a="http://schemas.microsoft.com/2003/10/Serialization/Arrays"></TravelClassCodes>
                                    <IncludeTaxesAndFees>true</IncludeTaxesAndFees>
                                    <FareRuleFilter>Default</FareRuleFilter>
                                    <LoyaltyFilter>MonetaryOnly</LoyaltyFilter>
                                    <PaxResidentCountry>ID</PaxResidentCountry>
                                    <TravelClassCodeList i:nil="true" xmlns:a="http://schemas.microsoft.com/2003/10/Serialization/Arrays"></TravelClassCodeList>
                                </AvailabilityRequest>
                            </AvailabilityRequests>
                            <LoyaltyFilter>MonetaryOnly</LoyaltyFilter>
                        </TripAvailabilityRequest>
                    </GetAvailabilityRequest>';
            /*header("Content-type: text/xml");
            echo $param;
            exit;*/
            
            $result = $this->connect($this->url_transaction, $apilog_code, 'GetAvailability', $header, $param);
            
            if($result['error_no'] == '0'){
                if($type == "depart"){
                    $class_code = "DQG";
                    $class_conn_code = "DQC";
                    $depart_ref = $apilog_code;
                    $class_count = "1";
                    $class_conn_count = "1";
                }
                else{
                    $class_code = "RQG";
                    $class_conn_code = "RQC";
                    $return_ref = $apilog_code;
                    $class_count = "1";
                    $class_conn_count = "1";
                }
                $result = $result['result']->GetTripAvailabilityResponse->Schedules->ArrayOfJourneyDateMarket->JourneyDateMarket->Journeys->Journey;
                #print_r($result);echo"<br><br>";
                $result = $this->convertObjToArray($result);
                
                // $sql_sur = "select * from `surcharge_fees` where `depart_airport` = '$from' or `arrival_airport` = '$to'";
                /*$sql_sur = "select * from `surcharge_fees` where `depart_airport` = '$from' AND airlines_id = '1' ";
                $ci->load->database('default', TRUE);
                $arr_surcharge = $ci->mgeneral->post_query_sql($sql_sur);
                
                $sql_service = "select * from `service_fee` where `depart_airport` = '$from' or `arrival_airport` = '$to'";
                $ci->load->database('default', TRUE);
                $arr_service = $ci->mgeneral->post_query_sql($sql_service);*/
                $service_fee = 0;
                
                foreach($result as $r){
                    $journey_sell_key = $r->JourneySellKey;
                    $segment = $r->Segments->Segment;
                    $segment = $this->convertObjToArray($segment);
                    if(count($segment) == 1){
                        $class_utama = $segment[0];
                        unset($fare);
                        $fare = $this->convertObjToArray($class_utama->Fares->Fare);
                        if(count($fare) > 0){
                            $action_code = $class_utama->ActionStatusCode;
                            
                            $sch["airline_id"]      = $this->airlines_id;
                            $sch["airline_name"]    = $this->airlines_name;
                            $sch["from"]            = $class_utama->DepartureStation;
                            $sch["to"]              = $class_utama->ArrivalStation;
                            $sch["date"]            = $depart_date;
                            $sch["via"]             = '-';
                            $sch["type"]            = "direct";
                            $sch["fno"]             = $class_utama->FlightDesignator->CarrierCode.' '.str_replace(' ', '', $class_utama->FlightDesignator->FlightNumber);
                            $sch["etd"]             = $this->convertTimeCitilink($class_utama->STD);
                            $sch["eta"]             = $this->convertTimeCitilink($class_utama->STA);
                            
                            // START #trello261016
                            // comment service fee change to surcharge fee 
                            
                            /*foreach($arr_surcharge as $s){
                                if($sch["from"] == $s->depart_airport){
                                    $surcharge = $s->fee_values;
                                    break;
                                }
                            }*/
                            $surcharge = 0;
                            /*
                            foreach($arr_service as $s){
                                if($sch["from"] == $s->depart_airport && $sch['to'] == $s->arrival_airport){
                                    $service_fee = $s->fee_values;
                                    break;
                                }
                            }
                            */
                            // END #trello261016
                            
                            $conn_price = 0;

                            foreach($fare as $f){
                                if($f->Status == "Active" && $f->AvailableCount >= $total_pax){
                                    $class["class_id"]      = $class_code.$class_count;
                                    $class["class_name"]    = $f->ClassOfService;
                                    $class["seat"]          = $f->AvailableCount;
                                    
                                    $value = array('signature' => $signature['signature'], 'action_status_code'=> $action_code, 'journey_sell_key' => $journey_sell_key, 'fare_sell_key' => $f->FareSellKey, 'a'=>$adult, 'c'=>$child, 'i'=>$infant, 'fno'=>$sch["fno"], 'std'=>$class_utama->STD, 'from'=>$sch['from'],'to'=>$sch['to'], 'cl'=>$f->ClassOfService);
                                    $value = json_encode($value);
                                    
                                    $pax_fare = $this->convertObjToArray($f->PaxFares->PaxFare);
                                    
                                    foreach($pax_fare as $pf){
                                        if($pf->PaxType == "ADT"){
                                            $price = $pf->ServiceCharges->BookingServiceCharge;
                                            $price = $this->convertObjToArray($price);
                                            foreach($price as $pr){
                                                if($pr->ChargeType == 'FarePrice'){
                                                    $basic_price = preg_replace('/([0-9]*)\..*/', '$1', $pr->Amount);
                                                    break 2;
                                                }
                                            }
                                        }
                                    }
                                    $tax = $basic_price*0.1;
                                    
                                    $adult_price = $basic_price + $service_fee + $tax + $surcharge + 5000;
                                    $class['price'] = $adult_price;
                                    $class['value'] = $ci->converter->encode($value);
                                    $sch["class"][] = $class;
                                    $class_count++;
                                }
                            }
                            
                            if(count($sch['class'])!=0){
                                $schedule_data[] = $sch;
                            }
                            unset($sch);
                        }
                    }
                    else if(count($segment) == 2){
                        unset($fare);
                        unset($fare_conn);
                        $class_utama = $segment[0];
                        $fare = $this->convertObjToArray($class_utama->Fares->Fare);
                        $class_connecting = $segment[1];
                        $fare_conn = $this->convertObjToArray($class_connecting->Fares->Fare);
                        if(count($fare) > 0 && count($fare_conn) && count($fare) == count($fare_conn)){
                            $action_code = $class_utama->ActionStatusCode;
                            
                            $sch["airline_id"]      = $this->airlines_id;
                            $sch["airline_name"]    = $this->airlines_name;
                            $sch["from"]            = $class_utama->DepartureStation;
                            $sch["to"]              = $class_utama->ArrivalStation;
                            $sch["date"]            = $depart_date;
                            $sch["via"]             = '-';
                            $sch["fno"]             = $class_utama->FlightDesignator->CarrierCode.' '.str_replace(' ', '', $class_utama->FlightDesignator->FlightNumber);
                            $sch["etd"]             = $this->convertTimeCitilink($class_utama->STD);
                            $sch["eta"]             = $this->convertTimeCitilink($class_utama->STA);
                            $sch["type"]                = "connecting";
                            
                            /*connecting data*/
                            $detail_connecting = array();
                            $detail_connecting["fno"]   = $class_connecting->FlightDesignator->CarrierCode.' '.str_replace(' ', '', $class_connecting->FlightDesignator->FlightNumber);
                            $detail_connecting["from"]  = $class_connecting->DepartureStation;
                            $detail_connecting["to"]    = $class_connecting->ArrivalStation;
                            $detail_connecting["etd"]   = $this->convertTimeCitilink($class_connecting->STD);
                            $detail_connecting["eta"]   = $this->convertTimeCitilink($class_connecting->STA);
                            $detail_connecting["date"]  = $this->convertDateCitilink($class_connecting->STD);
                            
                            $class_conn["class_id"]     = "DQC01";
                            $class_conn["class_name"]   = "V";
                            $class_conn["seat"]         = '1';
                            $class_conn["price"]        = '0';  
                            $value_conn = array('fno'=>$detail_connecting["fno"], 'std'=>$class_connecting->STD, 'from'=>$detail_connecting["from"], 'to'=>$detail_connecting["to"], 'action_status_code'=>$class_connecting->ActionStatusCode);
                            $value_conn = json_encode($value_conn);
                            $class_conn['value'] = $ci->converter->encode($value_conn);
                            
                            $detail_connecting['class'] = array($class_conn);   
                            $sch['connecting_flight'] = array($detail_connecting);
                            /*connecting data*/
                            
                            // START #trello261016
                            // comment service fee change to surcharge fee 
                            
                            foreach($arr_surcharge as $s){
                                if($sch["from"] == $s->depart_airport){
                                    $surcharge = $s->fee_values;
                                    break;
                                }
                            }
                            /*
                            foreach($arr_service as $s){
                                if($sch["from"] == $s->depart_airport && $sch['to'] == $s->arrival_airport){
                                    $service_fee = $s->fee_values;
                                    break;
                                }
                            }
                            */
                            // END #trello261016
                            
                            $conn_price = 0;
                            
                            for($i = 0; $i < count($fare); $i++){
                                $f = $fare[$i];
                                $f2 = $fare_conn[$i];
                                if($f->Status == "Active" && $f2->Status == "Active" && $f->AvailableCount >= $total_pax){
                                    if($f->FareApplicationType == 'Governing'){
                                        $class_name = $f->ClassOfService;
                                    }
                                    else{
                                        $class_name = $f2->ClassOfService;
                                    }
                                
                                    $class["class_id"]      = $class_code.$class_count;
                                    $class["class_name"]    = $class_name;
                                    $class["seat"]          = (string) $f->AvailableCount;
                                    
                                    $value = array('signature' => $signature['signature'], 'action_status_code'=> $action_code, 'journey_sell_key' => $journey_sell_key, 'fare_sell_key' => $f->FareSellKey, 'fare_sell_key_conn' => $f2->FareSellKey, 'a'=>$adult, 'c'=>$child, 'i'=>$infant, 'fno'=>$sch["fno"], 'std'=>$class_utama->STD, 'from'=>$sch['from'],'to'=>$sch['to'], 'cl'=>$f->ClassOfService);
                                    $value = json_encode($value);
                                    
                                    $pax_fare = $this->convertObjToArray($f->PaxFares->PaxFare);
                                    
                                    foreach($pax_fare as $pf){
                                        if($pf->PaxType == "ADT"){
                                            $price = $pf->ServiceCharges->BookingServiceCharge;
                                            $price = $this->convertObjToArray($price);
                                            foreach($price as $pr){
                                                if($pr->ChargeType == 'FarePrice'){
                                                    $basic_price = preg_replace('/([0-9]*)\..*/', '$1', $pr->Amount);
                                                    break 2;
                                                }
                                            }
                                        }
                                    }
                                    
                                    $pax_fare_conn = $this->convertObjToArray($f2->PaxFares->PaxFare);
                                    foreach($pax_fare_conn as $pfc){
                                        if($pfc->PaxType == "ADT"){
                                            $price_conn = $pfc->ServiceCharges->BookingServiceCharge;
                                            $price_conn = $this->convertObjToArray($price_conn);
                                            foreach($price_conn as $prc){
                                                if($prc->ChargeType == 'FarePrice'){
                                                    $basic_price_conn = substr($prc->Amount, 0, strlen($prc->Amount)-5);
                                                    break 2;
                                                }
                                            }
                                        }
                                    }
                                    
                                    $tax_conn = $basic_price_conn*0.1;
                                    
                                    $conn_price = $basic_price_conn + $tax_conn + $surcharge_conn + 5000;

                                    $tax = $basic_price*0.1;
                                    $adult_price = $basic_price + $service_fee + $tax + $surcharge + 5000 + $conn_price;

                                    $class['price'] = $adult_price;
                                    $class['value'] = $ci->converter->encode($value);
                                    $sch["class"][] = $class;
                                    $class_count++;
                                }
                            }
                            
                            if(count($sch['class'])!=0){
                                $schedule_data[] = $sch;
                            }
                            unset($sch);
                        }
                    }
                }
                
                if($type == "depart"){
                    $res["depart_ref"] = $depart_ref;
                    $res["depart"]    = $schedule_data;
                }
                else{
                    $res["return_ref"] = $return_ref;
                    $res["return"]    = $schedule_data;
                }
                
                $res['error_no'] = "0";
                $res['type'] = $type;
                
                #belum di simpan ke dalam log.
            }
            else{
                $res = $result;
            }
        }
        else{
            $res = $signature;
        }

        return $res;
    }
    
    function detail($post, $flight_int=""){
        $ci = & get_instance();
        $keys = json_decode($post['value']["value_1_a"], true);
        $arr_key[] = $keys;
        
        $signature = $keys['signature'];
        $action_status_code = $keys['action_status_code'];
        $journey_sell_key = $keys['journey_sell_key'];
        $fare_sell_key = $keys['fare_sell_key'];
        $adult = $keys['a'];
        $child = $keys['c'];
        $infant = $keys['i'];
        $cl = $keys['cl'];
        
        $apilog_code = $ci->converter->encode($signature);
        $result_db = $ci->mgeneral->getWhere(array('apilog_code'=>$apilog_code, 'apilog_action' => 'result_detail', 'apilog_post' => json_encode($post)), 'apilog_citilink');

        if(count($result_db) > 0){
            $result = json_decode($result_db[0]->apilog_response, true);
        }
        else{
            if($post['value']["value_1_b"] != NULL && $post['value']["value_1_b"] != ""){
                $key_conn = json_decode($post['value']["value_1_b"], true);
                $key_conn['fare_sell_key'] = $keys['fare_sell_key_conn'];
                $arr_key[] = $key_conn;
                
                $fare_sell_key = $fare_sell_key.'^'.$key_conn['fare_sell_key'];
            }
            
            $total_pax = $adult + $child;
            
            $pax = '';
            for($i = 0; $i < $adult; $i++){
                $pax = $pax.'<a:PaxPriceType>
                                <a:PaxType>ADT</a:PaxType>
                                <a:PaxDiscountCode i:nil="true"></a:PaxDiscountCode>
                            </a:PaxPriceType>';
            }
            
            for($i = 0; $i < $child; $i++){
                $pax = $pax.'<a:PaxPriceType>
                                <a:PaxType>CHD</a:PaxType>
                                <a:PaxDiscountCode i:nil="true"></a:PaxDiscountCode>
                            </a:PaxPriceType>';
            }
                
            $header[] = array('http://schemas.navitaire.com/WebServices', 'ContractVersion', '0');
            $header[] = array('http://schemas.navitaire.com/WebServices', 'Signature', $signature);
            
            $param  = '<SellRequest xmlns="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService">
                            <SellRequestData xmlns:a="http://schemas.navitaire.com/WebServices/DataContracts/Booking" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                                <a:SellBy>JourneyBySellKey</a:SellBy>
                                <a:SellJourneyByKeyRequest>
                                    <a:SellJourneyByKeyRequestData>
                                        <a:ActionStatusCode>NN</a:ActionStatusCode>
                                        <a:JourneySellKeys>
                                            <a:SellKeyList>
                                                <a:JourneySellKey>'.$journey_sell_key.'</a:JourneySellKey>
                                                <a:FareSellKey>'.$fare_sell_key.'</a:FareSellKey>
                                                <a:StandbyPriorityCode i:nil="true"></a:StandbyPriorityCode>
                                            </a:SellKeyList>
                                        </a:JourneySellKeys>
                                        <a:PaxPriceType>
                                            '.$pax.'
                                        </a:PaxPriceType>
                                        <a:CurrencyCode>IDR</a:CurrencyCode>
                                        <a:SourcePOS xmlns:b="http://schemas.navitaire.com/WebServices/DataContracts/Common">
                                            <b:State>New</b:State>
                                            <b:AgentCode>Api_Gih</b:AgentCode>
                                            <b:OrganizationCode>0014002445</b:OrganizationCode>
                                            <b:DomainCode>EXT</b:DomainCode>
                                            <b:LocationCode i:nil="true"></b:LocationCode>
                                        </a:SourcePOS>
                                        <a:PaxCount>'.$total_pax.'</a:PaxCount>
                                        <a:TypeOfSale i:nil="true"></a:TypeOfSale>
                                        <a:LoyaltyFilter>MonetaryOnly</a:LoyaltyFilter>
                                        <a:IsAllotmentMarketFare>false</a:IsAllotmentMarketFare>
                                        </a:SellJourneyByKeyRequestData>
                                    </a:SellJourneyByKeyRequest>
                                <a:SellJourneyRequest i:nil="true"></a:SellJourneyRequest>
                                <a:SellSSR i:nil="true"></a:SellSSR>
                                <a:SellFee i:nil="true"></a:SellFee>
                            </SellRequestData>
                        </SellRequest>';
            
            $result_sell = $this->connect($this->url_transaction, $apilog_code, 'Sell', $header, $param);
            if($result_sell['error_no'] == '0'){
                $result['error_no'] = '0';
                $res = $result_sell['result'];
                $total_balance = $res->BookingUpdateResponseData->Success->PNRAmount->BalanceDue;
                $total_balance = substr($total_balance, 0, strlen($total_balance)-5);
                
                if($infant > 0){
                    foreach($arr_key as $ak){
                        $pax_ssr = '';
                        for($count=0; $count<$infant; $count++){
                            $pax_ssr = $pax_ssr.'<a:PaxSSR>'
                                            .'<State xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Common">New</State>'
                                            .'<a:ActionStatusCode>'.$ak['action_status_code'].'</a:ActionStatusCode>'
                                            .'<a:ArrivalStation>'.$ak['to'].'</a:ArrivalStation>'
                                            .'<a:DepartureStation>'.$ak['from'].'</a:DepartureStation>'
                                            .'<a:PassengerNumber>'.$count.'</a:PassengerNumber>'
                                            .'<a:SSRCode>INF</a:SSRCode>'
                                            .'<a:SSRNumber>'.$count.'</a:SSRNumber>'
                                            .'<a:SSRDetail i:nil="true"></a:SSRDetail>'
                                            .'<a:FeeCode i:nil="true"></a:FeeCode>'
                                            .'<a:Note i:nil="true"></a:Note>'
                                            .'<a:SSRValue>0</a:SSRValue>'
                                        .'</a:PaxSSR>';
                        }
                    
                        $flight_no = explode(' ', $ak['fno']);
                        $segment_req = '<a:SegmentSSRRequest>
                                            <a:FlightDesignator xmlns:b="http://schemas.navitaire.com/WebServices/DataContracts/Common">
                                                <b:CarrierCode>'.$flight_no[0].'</b:CarrierCode>
                                                <b:FlightNumber>'.$flight_no[1].'</b:FlightNumber>
                                                <b:OpSuffix i:nil="true"></b:OpSuffix>
                                            </a:FlightDesignator>
                                            <a:STD>'.$ak['std'].'</a:STD>
                                            <a:DepartureStation>'.$ak['from'].'</a:DepartureStation>
                                            <a:ArrivalStation>'.$ak['to'].'</a:ArrivalStation>
                                            <a:PaxSSRs>
                                                '.$pax_ssr.'
                                            </a:PaxSSRs>
                                        </a:SegmentSSRRequest>';
                    }
                    
                    $param_inf  =  '<SellRequest xmlns="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService">
                                        <SellRequestData xmlns:a="http://schemas.navitaire.com/WebServices/DataContracts/Booking" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                                            <a:SellBy>SSR</a:SellBy>
                                            <a:SellJourneyByKeyRequest i:nil="true"></a:SellJourneyByKeyRequest>
                                            <a:SellJourneyRequest i:nil="true"></a:SellJourneyRequest>
                                            <a:SellSSR>
                                                <a:SSRRequest>
                                                    <a:SegmentSSRRequests>
                                                        '.$segment_req.'
                                                    </a:SegmentSSRRequests>
                                                    <a:CurrencyCode i:nil="true">IDR</a:CurrencyCode>
                                                    <a:CancelFirstSSR>false</a:CancelFirstSSR>
                                                    <a:SSRFeeForceWaiveOnSell>false</a:SSRFeeForceWaiveOnSell>
                                                </a:SSRRequest>
                                            </a:SellSSR>
                                            <a:SellFee i:nil="true"></a:SellFee>
                                        </SellRequestData>
                                    </SellRequest>';
                                    
                    $result_sell_inf = $this->connect($this->url_transaction, $apilog_code, 'Sell', $header, $param_inf);
                    
                    if($result_sell_inf['error_no'] == '0'){
                        $res_inf = $result_sell_inf['result'];
                        
                        $total_balance_inf = $res_inf->BookingUpdateResponseData->Success->PNRAmount->BalanceDue;
                        $total_balance_inf = substr($total_balance_inf, 0, strlen($total_balance_inf)-5);
                        
                        $total_segment = $res_inf->BookingUpdateResponseData->Success->PNRAmount->SegmentCount;
                        
                        $total_price_inf = $total_balance_inf - $total_balance;
                        
                        $total_iwjr = 5000 * $total_segment * $infant;
                        
                        $basic_price_inf = (($total_price_inf-$total_iwjr) * (100/110)) / $infant;
                        $basic_price_inf = number_format($basic_price_inf, 0, ',', '');
                    }
                    else{
                        $result['error_no'] = '502';
                        $result['error_no'] = 'proses pengecekan detail infant gagal, silakan lakukan pencarian ulang';
                    }
                }
                
                if($this->promo_stat == true){
                    foreach($arr_key as $ak){
                        $pax_ssr = '';
                        for($count=0; $count<($adult+$child); $count++){
                            $pax_ssr = $pax_ssr.'<a:PaxSSR>'
                                            .'<State xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Common">New</State>'
                                            .'<a:ActionStatusCode>'.$ak['action_status_code'].'</a:ActionStatusCode>'
                                            .'<a:ArrivalStation>'.$ak['to'].'</a:ArrivalStation>'
                                            .'<a:DepartureStation>'.$ak['from'].'</a:DepartureStation>'
                                            .'<a:PassengerNumber>'.$count.'</a:PassengerNumber>'
                                            .'<a:SSRCode>'.$this->promo_code.'</a:SSRCode>'
                                            .'<a:SSRNumber>'.$count.'</a:SSRNumber>'
                                            .'<a:SSRDetail i:nil="true"></a:SSRDetail>'
                                            .'<a:FeeCode i:nil="true"></a:FeeCode>'
                                            .'<a:Note i:nil="true"></a:Note>'
                                            .'<a:SSRValue>0</a:SSRValue>'
                                        .'</a:PaxSSR>';
                        }
                    
                        $flight_no = explode(' ', $ak['fno']);
                        $segment_req = '<a:SegmentSSRRequest>
                                            <a:FlightDesignator xmlns:b="http://schemas.navitaire.com/WebServices/DataContracts/Common">
                                                <b:CarrierCode>'.$flight_no[0].'</b:CarrierCode>
                                                <b:FlightNumber>'.$flight_no[1].'</b:FlightNumber>
                                                <b:OpSuffix i:nil="true"></b:OpSuffix>
                                            </a:FlightDesignator>
                                            <a:STD>'.$ak['std'].'</a:STD>
                                            <a:DepartureStation>'.$ak['from'].'</a:DepartureStation>
                                            <a:ArrivalStation>'.$ak['to'].'</a:ArrivalStation>
                                            <a:PaxSSRs>
                                                '.$pax_ssr.'
                                            </a:PaxSSRs>
                                        </a:SegmentSSRRequest>';
                    }
                    
                    $param_promo    =  '<SellRequest xmlns="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService">
                                        <SellRequestData xmlns:a="http://schemas.navitaire.com/WebServices/DataContracts/Booking" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                                            <a:SellBy>SSR</a:SellBy>
                                            <a:SellJourneyByKeyRequest i:nil="true"></a:SellJourneyByKeyRequest>
                                            <a:SellJourneyRequest i:nil="true"></a:SellJourneyRequest>
                                            <a:SellSSR>
                                                <a:SSRRequest>
                                                    <a:SegmentSSRRequests>
                                                        '.$segment_req.'
                                                    </a:SegmentSSRRequests>
                                                    <a:CurrencyCode i:nil="true">IDR</a:CurrencyCode>
                                                    <a:CancelFirstSSR>false</a:CancelFirstSSR>
                                                    <a:SSRFeeForceWaiveOnSell>false</a:SSRFeeForceWaiveOnSell>
                                                </a:SSRRequest>
                                            </a:SellSSR>
                                            <a:SellFee i:nil="true"></a:SellFee>
                                        </SellRequestData>
                                    </SellRequest>';
                                    
                    $result_sell_promo = $this->connect($this->url_transaction, $apilog_code, 'Sell', $header, $param_promo);
                }
                
                if($result['error_no'] == '0'){
                    $param_get = '<GetBookingFromStateRequest xmlns="http://schemas.navitaire.com/WebServices" />';
                
                    $result_detail = $this->connect($this->url_transaction, $apilog_code, 'GetBookingFromState', $header, $param_get);
                    if($result_detail['error_no'] == '0'){                  
                        $segment = $result_detail['result']->BookingData->Journeys->Journey->Segments->Segment;
                        $segment = $this->convertObjToArray($segment);
                        
                        $basic_price_adult = 0;
                        $basic_price_child = 0;
                        $service_fee = 0;
                        $discount_adult = 0;
                        $discount_child = 0;
                        
                        foreach($segment as $seg){
                            $pax_fare = $this->convertObjToArray($seg->Fares->Fare->PaxFares->PaxFare);
                        
                            foreach($pax_fare as $pf){
                                if($pf->PaxType == "ADT"){
                                    $arr_price_adult = $pf->ServiceCharges->BookingServiceCharge;
                                    $arr_price_adult = $this->convertObjToArray($arr_price_adult);
                                    foreach($arr_price_adult as $pr){
                                        if($pr->ChargeType == 'FarePrice'){
                                            $basic_price_adult = $basic_price_adult + preg_replace('/([0-9]*)\..*/', '$1', $pr->Amount);
                                        }
                                        else if($pr->ChargeCode == 'PSC'){
                                            $service_fee = $service_fee + preg_replace('/([0-9]*)\..*/', '$1', $pr->Amount);
                                        }
                                        else if($pr->ChargeCode == 'SUR'){
                                            $service_fee = $service_fee + preg_replace('/([0-9]*)\..*/', '$1', $pr->Amount);
                                        }
                                        else if($pr->ChargeType == 'Discount'){
                                            $basic_price_adult = $basic_price_adult - preg_replace('/([0-9]*)\..*/', '$1', $pr->Amount);
                                        }
                                    }
                                }
                                else if($pf->PaxType == "CHD"){
                                    $arr_price_child = $pf->ServiceCharges->BookingServiceCharge;
                                    $arr_price_child = $this->convertObjToArray($arr_price_child);
                                    foreach($arr_price_child as $pr){
                                        if($pr->ChargeType == 'FarePrice'){
                                            $basic_price_child = $basic_price_child + preg_replace('/([0-9]*)\..*/', '$1', $pr->Amount);
                                        }
                                        else if($pr->ChargeType == 'Discount'){
                                            $basic_price_child = $basic_price_child - preg_replace('/([0-9]*)\..*/', '$1', $pr->Amount);
                                        }
                                    }
                                }
                            }
                        }
                        $discount = 0;
                        if($this->promo_stat == true){
                            $disc_stat  = 0;
                            $arr_dis    = $result_detail['result']->BookingData->Passengers->Passenger;
                            $arr_dis    = $this->convertObjToArray($arr_dis);
                            foreach($arr_dis as $arr_d){
                                if($arr_d->PassengerTypeInfos->PassengerTypeInfo->PaxType == "ADT" && $disc_stat == 0){
                                    $disc_stat  = 1;
                                    $pax_fees   = $arr_d->PassengerFees->PassengerFee;
                                    $pax_fees   = $this->convertObjToArray($pax_fees);
                                    foreach($pax_fees as $pax_fee){
                                        if($pax_fee->FeeCode != "INF"){
                                            $discount = $discount + preg_replace('/([0-9\-]*)\..*/', '$1', $pax_fee->ServiceCharges->BookingServiceCharge->Amount);
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                        
                        $schedule   = array("airline_is" => $this->airlines_id,
                                            "airline_name" => $this->airlines_name,
                                            "from" => $post['from'],
                                            "to" => $post["to"],
                                            "date" => $post["tglP"],
                                            "flight_no" =>"",
                                            "ETA" => ":",
                                            "ETD" => ":",
                                            "class" => "");
                                                        
                        $result = array("error_no" => "0",
                                        "sch_detail" => $schedule,
                                        "priceAD" => $basic_price_adult,
                                        "priceCH" => $basic_price_child,
                                        "priceIn" => $basic_price_inf,
                                        "komisi" => $komisi,
                                        "service" => ($service_fee + $discount));
                        
                        $this->saveLog($apilog_code, 'result_detail', json_encode($post), $result);
                    }
                    else{
                        $result['error_no'] = '503';
                        $result['error_no'] = 'kalkulasi harga error';
                    }
                }
            }
            else{
                $result['error_no'] = '501';
                $result['error_no'] = 'proses pengecekan detail penumpang gagal, silakan lakukan pencarian ulang';
            }
        }
        
        return $result;
    }
    
    function book($flight,$psgr,$cust){
        $ci = & get_instance();
        $keys = json_decode($flight['value']['value_1_a'], true);
        $signature = $keys['signature'];
        $apilog_code = $ci->converter->encode($signature);
        
        $param_contact  =  '<UpdateContactsRequest xmlns="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService">
                            <updateContactsRequestData xmlns:a="http://schemas.navitaire.com/WebServices/DataContracts/Booking" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                                <a:BookingContactList>
                                    <a:BookingContact>
                                        <State xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Common">New</State>
                                        <a:TypeCode>P</a:TypeCode>
                                        <a:Names>
                                            <a:BookingName>
                                                <State xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Common">New</State>
                                                <a:FirstName>INNOVATECH</a:FirstName>
                                                <a:MiddleName i:nil="true"></a:MiddleName>
                                                <a:LastName>MEDIASKY</a:LastName>
                                                <a:Suffix i:nil="true"></a:Suffix>
                                                <a:Title>MR</a:Title>
                                            </a:BookingName>
                                        </a:Names>
                                        <a:EmailAddress>support.team@aeroticket.co.id</a:EmailAddress>
                                        <a:HomePhone>'.$cust['cust_phone'].'</a:HomePhone>
                                        <a:WorkPhone>0217229992</a:WorkPhone>
                                        <a:OtherPhone>'.$cust['cust_phone'].'</a:OtherPhone>
                                        <a:Fax i:nil="true"></a:Fax>
                                        <a:CompanyName>innovatech1</a:CompanyName>
                                        <a:AddressLine1>JL.MELAWAI RAYA NO.C7</a:AddressLine1>
                                        <a:AddressLine2 i:nil="true"></a:AddressLine2>
                                        <a:AddressLine3 i:nil="true"></a:AddressLine3>
                                        <a:City>Jakarta</a:City>
                                        <a:ProvinceState>JKT</a:ProvinceState>
                                        <a:PostalCode>70114</a:PostalCode>
                                        <a:CountryCode>ID</a:CountryCode>
                                        <a:CultureCode i:nil="true"></a:CultureCode>
                                        <a:DistributionOption>None</a:DistributionOption>
                                        <a:CustomerNumber i:nil="true"></a:CustomerNumber>
                                        <a:NotificationPreference>None</a:NotificationPreference>
                                        <a:SourceOrganization>0014000357</a:SourceOrganization>
                                    </a:BookingContact>
                                </a:BookingContactList>
                            </updateContactsRequestData>
                        </UpdateContactsRequest>';
                    
        $header[] = array('http://schemas.navitaire.com/WebServices', 'ContractVersion', '0');
        $header[] = array('http://schemas.navitaire.com/WebServices', 'Signature', $signature);
        
        $res_update_contact = $this->connect($this->url_transaction, $apilog_code, 'UpdateContacts', $header, $param_contact);
        if($res_update_contact['error_no'] == "0"){
            $count_inf = 1;
            $param_pax  = "";
            $count_pax = 0;
            foreach($psgr as $ps){
                if($ps['psgr_type'] == "adult"){
                    $count_inf_baru = 1;
                    $status_inf = '0';
                    foreach($psgr as $ps_inf){
                        if($ps_inf['psgr_type'] == "infant"){
                            if($count_inf == $count_inf_baru){
                                $param_inf  =  '<a:Infant>
                                                    <State xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Common">New</State>
                                                    <a:DOB>'.$ps_inf['birthdate'].'T00:00:00</a:DOB>
                                                    <a:Gender>Male</a:Gender>
                                                    <a:Nationality>ID</a:Nationality>
                                                    <a:ResidentCountry i:nil="true"></a:ResidentCountry>
                                                    <a:Names>
                                                        <a:BookingName>
                                                            <State xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Common">New</State>
                                                            <a:FirstName>'.$ps_inf['first_name'].'</a:FirstName>
                                                            <a:MiddleName i:nil="true"></a:MiddleName>
                                                            <a:LastName>'.$ps_inf['last_name'].'</a:LastName>
                                                            <a:Suffix i:nil="true"></a:Suffix>
                                                            <a:Title>MR</a:Title>
                                                        </a:BookingName>
                                                    </a:Names>
                                                </a:Infant>';
                                
                                $status_inf = '1';
                                $count_inf++;
                                break;
                            }
                            $count_inf_baru++;
                        }
                    }
                    if($status_inf == '0'){
                        $param_inf = '<a:Infant i:nil="true"></a:Infant>';
                    }
                    $title = strtoupper($ps['title']);
                    if($ps['title'] == "mr"){
                        $gender = "Male";
                    }
                    else{
                        $gender = "Female";
                    }
                    
                    if($ps['birthdate'] == "" || $ps['birthdate'] == NULL || $ps['birthdate'] == '0000-00-00'){
                        $dob = date('Y-m-d', strtotime(date('Y-m-d H:i:s') . ' - 20 years')).'T00:00:00';
                    }
                    else{
                        $dob = $ps['birthdate'].'T00:00:00';
                    }
                    
                    $pax_type = "ADT";
                
                    $pax_data   = $pax_data.'<a:Passenger>
                                                <State xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Common">New</State>
                                                <a:PassengerPrograms i:nil="true"></a:PassengerPrograms>
                                                <a:CustomerNumber i:nil="true"></a:CustomerNumber>
                                                <a:PassengerNumber>'.$count_pax.'</a:PassengerNumber>
                                                <a:FamilyNumber>0</a:FamilyNumber>
                                                <a:PaxDiscountCode i:nil="true"></a:PaxDiscountCode>
                                                <a:Names>
                                                    <a:BookingName>
                                                        <State xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Common">New</State>
                                                        <a:FirstName>'.$ps['first_name'].'</a:FirstName>
                                                        <a:MiddleName i:nil="true"></a:MiddleName>
                                                        <a:LastName>'.$ps['last_name'].'</a:LastName>
                                                        <a:Suffix i:nil="true"></a:Suffix>
                                                        <a:Title>'.$title.'</a:Title>
                                                    </a:BookingName>
                                                </a:Names>'.
                                                $param_inf
                                                .'<a:PassengerInfo>
                                                    <State xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Common">New</State>
                                                    <a:BalanceDue>0</a:BalanceDue>
                                                    <a:Gender>'.$gender.'</a:Gender>
                                                    <a:Nationality>ID</a:Nationality>
                                                    <a:ResidentCountry i:nil="true"></a:ResidentCountry>
                                                    <a:TotalCost>0</a:TotalCost>
                                                    <a:WeightCategory>'.$gender.'</a:WeightCategory>
                                                </a:PassengerInfo>
                                                <a:PassengerProgram i:nil="true"></a:PassengerProgram>
                                                <a:PassengerFees i:nil="true"></a:PassengerFees>
                                                <a:PassengerAddresses i:nil="true"></a:PassengerAddresses>
                                                <a:PassengerTravelDocuments i:nil="true"></a:PassengerTravelDocuments>
                                                <a:PassengerBags i:nil="true"></a:PassengerBags>
                                                <a:PassengerID>0</a:PassengerID>
                                                <a:PassengerTypeInfos>
                                                    <a:PassengerTypeInfo>
                                                        <a:State>New</a:State>
                                                        <a:DOB>'.$dob.'</a:DOB>
                                                        <a:PaxType>'.$pax_type.'</a:PaxType>
                                                    </a:PassengerTypeInfo>
                                                </a:PassengerTypeInfos>
                                                <a:PassengerInfos>
                                                    <a:PassengerInfo>
                                                        <State xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Common">New</State>
                                                        <a:BalanceDue>0</a:BalanceDue>
                                                        <a:Gender>'.$gender.'</a:Gender>
                                                        <a:Nationality>ID</a:Nationality>
                                                        <a:ResidentCountry i:nil="true"></a:ResidentCountry>
                                                        <a:TotalCost>0</a:TotalCost>
                                                        <a:WeightCategory>'.$gender.'</a:WeightCategory>
                                                    </a:PassengerInfo>
                                                </a:PassengerInfos>
                                                <a:PassengerInfants i:nil="true"></a:PassengerInfants>
                                                <a:PseudoPassenger>false</a:PseudoPassenger>
                                            </a:Passenger>';
                    unset($param_inf);
                    $count_pax++;
                }

            }
            
            foreach($psgr as $ps){
                if($ps['psgr_type'] == "child"){
                    $title = "MR";
                    $gender = "Male";
                    $param_inf = '<a:Infant i:nil="true"></a:Infant>';
                    
                    if($ps['birthdate'] == "" || $ps['birthdate'] == NULL){
                        $dob = date('Y-m-d', strtotime(date('Y-m-d H:i:s') . ' - 11 years')).'T00:00:00';
                    }
                    else{
                        $dob = $ps['birthdate'].'T00:00:00';
                    }
                    $pax_type = "CHD";
                
                    $pax_data   = $pax_data.'<a:Passenger>
                                                <State xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Common">New</State>
                                                <a:PassengerPrograms i:nil="true"></a:PassengerPrograms>
                                                <a:CustomerNumber i:nil="true"></a:CustomerNumber>
                                                <a:PassengerNumber>'.$count_pax.'</a:PassengerNumber>
                                                <a:FamilyNumber>0</a:FamilyNumber>
                                                <a:PaxDiscountCode i:nil="true"></a:PaxDiscountCode>
                                                <a:Names>
                                                    <a:BookingName>
                                                        <State xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Common">New</State>
                                                        <a:FirstName>'.$ps['first_name'].'</a:FirstName>
                                                        <a:MiddleName i:nil="true"></a:MiddleName>
                                                        <a:LastName>'.$ps['last_name'].'</a:LastName>
                                                        <a:Suffix i:nil="true"></a:Suffix>
                                                        <a:Title>'.$title.'</a:Title>
                                                    </a:BookingName>
                                                </a:Names>'.
                                                $param_inf
                                                .'<a:PassengerInfo>
                                                    <State xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Common">New</State>
                                                    <a:BalanceDue>0</a:BalanceDue>
                                                    <a:Gender>'.$gender.'</a:Gender>
                                                    <a:Nationality>ID</a:Nationality>
                                                    <a:ResidentCountry i:nil="true"></a:ResidentCountry>
                                                    <a:TotalCost>0</a:TotalCost>
                                                    <a:WeightCategory>'.$gender.'</a:WeightCategory>
                                                </a:PassengerInfo>
                                                <a:PassengerProgram i:nil="true"></a:PassengerProgram>
                                                <a:PassengerFees i:nil="true"></a:PassengerFees>
                                                <a:PassengerAddresses i:nil="true"></a:PassengerAddresses>
                                                <a:PassengerTravelDocuments i:nil="true"></a:PassengerTravelDocuments>
                                                <a:PassengerBags i:nil="true"></a:PassengerBags>
                                                <a:PassengerID>0</a:PassengerID>
                                                <a:PassengerTypeInfos>
                                                    <a:PassengerTypeInfo>
                                                        <a:State>New</a:State>
                                                        <a:DOB>'.$dob.'</a:DOB>
                                                        <a:PaxType>'.$pax_type.'</a:PaxType>
                                                    </a:PassengerTypeInfo>
                                                </a:PassengerTypeInfos>
                                                <a:PassengerInfos>
                                                    <a:PassengerInfo>
                                                        <State xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Common">New</State>
                                                        <a:BalanceDue>0</a:BalanceDue>
                                                        <a:Gender>'.$gender.'</a:Gender>
                                                        <a:Nationality>ID</a:Nationality>
                                                        <a:ResidentCountry i:nil="true"></a:ResidentCountry>
                                                        <a:TotalCost>0</a:TotalCost>
                                                        <a:WeightCategory>'.$gender.'</a:WeightCategory>
                                                    </a:PassengerInfo>
                                                </a:PassengerInfos>
                                                <a:PassengerInfants i:nil="true"></a:PassengerInfants>
                                                <a:PseudoPassenger>false</a:PseudoPassenger>
                                            </a:Passenger>';
                    
                    $count_pax++;
                }

            }
            
        
            $param_pax  =  '<UpdatePassengersRequest xmlns="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService">
                            <updatePassengersRequestData xmlns:a="http://schemas.navitaire.com/WebServices/DataContracts/Booking" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                            <a:Passengers>
                            '.$pax_data.'
                            </a:Passengers>
                            <a:WaiveNameChangeFee>false</a:WaiveNameChangeFee>
                            </updatePassengersRequestData>
                            </UpdatePassengersRequest>';
                            
            $res_update_pax = $this->connect($this->url_transaction, $apilog_code, 'UpdatePassengers', $header, $param_pax);
            if($res_update_pax['error_no'] == '0'){
                $param_booking  = '<BookingCommitRequest xmlns="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService">
                                        <BookingCommitRequestData xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Booking" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                                            <State>New</State>
                                            <RecordLocator i:nil="true"></RecordLocator>
                                            <CurrencyCode>IDR</CurrencyCode>
                                            <PaxCount>2</PaxCount>
                                            <SystemCode i:nil="true"></SystemCode>
                                            <BookingID>0</BookingID>
                                            <BookingParentID>0</BookingParentID>
                                            <ParentRecordLocator i:nil="true"></ParentRecordLocator>
                                            <BookingChangeCode i:nil="true"></BookingChangeCode>
                                            <GroupName i:nil="true"></GroupName>
                                            <SourcePOS xmlns:a="http://schemas.navitaire.com/WebServices/DataContracts/Common">
                                                <a:State>New</a:State>
                                                <a:AgentCode>Api_Gih</a:AgentCode>
                                                <a:OrganizationCode>0014002445</a:OrganizationCode>
                                                <a:DomainCode>EXT</a:DomainCode>
                                                <a:LocationCode i:nil="true"></a:LocationCode>
                                            </SourcePOS>
                                            <BookingHold i:nil="true"></BookingHold>
                                            <ReceivedBy i:nil="true"></ReceivedBy>
                                            <RecordLocators i:nil="true" xmlns:a="http://schemas.navitaire.com/WebServices/DataContracts/Common"></RecordLocators>
                                            <Passengers i:nil="true"></Passengers>
                                            <BookingComments i:nil="true"></BookingComments>
                                            <BookingContacts i:nil="true"></BookingContacts>
                                            <NumericRecordLocator i:nil="true"></NumericRecordLocator>
                                            <RestrictionOverride>false</RestrictionOverride>
                                            <ChangeHoldDateTime>false</ChangeHoldDateTime>
                                            <WaiveNameChangeFee>false</WaiveNameChangeFee>
                                            <WaivePenaltyFee>false</WaivePenaltyFee>
                                            <WaiveSpoilageFee>false</WaiveSpoilageFee>
                                            <DistributeToContacts>false</DistributeToContacts>
                                        </BookingCommitRequestData>
                                    </BookingCommitRequest>';

                $res_book_commit = $this->connect($this->url_transaction, $apilog_code, 'BookingCommit', $header, $param_booking);
                if($res_book_commit['error_no'] == '0'){
                    $param_get = '<GetBookingFromStateRequest xmlns="http://schemas.navitaire.com/WebServices" />';
                
                    $result_detail = $this->connect($this->url_transaction, $apilog_code, 'GetBookingFromState', $header, $param_get);
                    
                    if($result_detail['error_no'] == '0'){
                        $res = $result_detail['result'];
                        if($res->BookingData->BookingInfo->BookingStatus == "Hold"){
                            $kode_booking = $res->BookingData->RecordLocator;
                            $time_limit = $res->BookingData->BookingHold->HoldDateTime;
                            $time_limit = substr($time_limit, 0, 10)." ".substr($time_limit, 11, 8);
                            $time_limit = date('Y-m-d H:i:s', strtotime($time_limit . ' + 7 hours'));
                            
                            $result = array("error_no" => "0",
                                            "book_code" => $kode_booking,
                                            "time_limit" => $time_limit,
                                            "status" => "CONFIRMED");
                        }
                        else{
                            $result['error_no'] = '603';
                            $result['error_msg'] = 'gagal booking';
                        }
                    }
                    else{
                        $result['error_no'] = '604';
                        $result['error_msg'] = 'gagal mendapatkan detail';
                    }
                }
                else{
                    $result['error_no'] = '603';
                    $result['error_msg'] = 'gagal booking';
                }
                
            }else{
                $result['error_no'] = '603';
                $result['error_msg'] = 'proses update data passenger gagal';
            }
            
        }
        else{
            $result['error_no'] = '601';
            $result['error_msg'] = 'proses update data customer gagal';
        }
        return $result;
    }
    
    function issue($book_code){
        $ci = & get_instance();
        
        $signature = $this->logon();
        
        if($signature['error_no'] == '0'){
            $header[] = array('http://schemas.navitaire.com/WebServices', 'ContractVersion', '0');
            $header[] = array('http://schemas.navitaire.com/WebServices', 'Signature', $signature['signature']);
            $apilog_code = $ci->converter->encode($signature['signature']);
            
            $param  =  '<GetBookingRequest xmlns="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService">
                            <GetBookingReqData xmlns:a="http://schemas.navitaire.com/WebServices/DataContracts/Booking" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                                <a:GetBookingBy>RecordLocator</a:GetBookingBy> 
                                <a:GetByRecordLocator>
                                    <a:RecordLocator>'.$book_code.'</a:RecordLocator> 
                                </a:GetByRecordLocator>
                                <a:GetByThirdPartyRecordLocator i:nil="true" /> 
                                <a:GetByID i:nil="true" /> 
                            </GetBookingReqData>
                        </GetBookingRequest>';
                        
            $res_get_book = $this->connect($this->url_transaction, $apilog_code, 'GetBooking', $header, $param);
            if($res_get_book['error_no'] == '0'){
                $res = $res_get_book['result'];
                $res = $res->Booking;
                if($res->BookingInfo->BookingStatus == 'Hold'){
                    $num_record_locator = $res->NumericRecordLocator;
                    $total_price = $res->BookingSum->BalanceDue;
                    $time_limit = $res->BookingHold->HoldDateTime;
                    $total_pax = $res->PaxCount;
                    
                    $param_add_payment  =  '<AddPaymentToBookingRequest xmlns="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService">
                                                <addPaymentToBookingReqData xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Booking" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                                                    <MessageState>New</MessageState> 
                                                    <WaiveFee>false</WaiveFee> 
                                                    <ReferenceType>Session</ReferenceType> 
                                                    <PaymentMethodType>AgencyAccount</PaymentMethodType> 
                                                    <PaymentMethodCode>AG</PaymentMethodCode> 
                                                    <QuotedCurrencyCode>IDR</QuotedCurrencyCode> 
                                                    <QuotedAmount>'.$total_price.'</QuotedAmount> 
                                                    <Status>New</Status> 
                                                    <AccountNumberID>0</AccountNumberID> 
                                                    <AccountNumber>0014000357</AccountNumber> 
                                                    <Expiration>'.$time_limit.'</Expiration> 
                                                    <ParentPaymentID>0</ParentPaymentID> 
                                                    <Installments>0</Installments> 
                                                    <PaymentText i:nil="true" /> 
                                                    <Deposit>false</Deposit> 
                                                    <PaymentFields i:nil="true" /> 
                                                    <PaymentAddresses i:nil="true" /> 
                                                    <AgencyAccount i:nil="true" /> 
                                                    <CreditShell i:nil="true" /> 
                                                    <CreditFile i:nil="true" /> 
                                                    <PaymentVoucher i:nil="true" /> 
                                                </addPaymentToBookingReqData>
                                            </AddPaymentToBookingRequest>'; 
                                            
                    $res_add_payment = $this->connect($this->url_transaction, $apilog_code, 'AddPaymentToBooking', $header, $param_add_payment);
                    
                    if($res_add_payment['error_no'] == '0'){
                        $param_commit   =  '<BookingCommitRequest xmlns="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService">
                                                <BookingCommitRequestData xmlns="http://schemas.navitaire.com/WebServices/DataContracts/Booking" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                                                    <State>New</State> 
                                                    <RecordLocator>'.$book_code.'</RecordLocator> 
                                                    <CurrencyCode>IDR</CurrencyCode> 
                                                    <PaxCount>'.$total_pax.'</PaxCount> 
                                                    <SystemCode i:nil="true" /> 
                                                    <BookingID>0</BookingID> 
                                                    <BookingParentID>0</BookingParentID> 
                                                    <ParentRecordLocator i:nil="true" /> 
                                                    <BookingChangeCode i:nil="true" /> 
                                                    <GroupName i:nil="true" /> 
                                                    <SourcePOS xmlns:a="http://schemas.navitaire.com/WebServices/DataContracts/Common">
                                                        <a:State>New</a:State> 
                                                        <a:AgentCode>Api_Gih</a:AgentCode> 
                                                        <a:OrganizationCode>0014002445</a:OrganizationCode> 
                                                        <a:DomainCode>EXT</a:DomainCode> 
                                                        <a:LocationCode i:nil="true" /> 
                                                    </SourcePOS>
                                                    <BookingHold i:nil="true" /> 
                                                    <ReceivedBy i:nil="true" /> 
                                                    <RecordLocators i:nil="true" xmlns:a="http://schemas.navitaire.com/WebServices/DataContracts/Common" /> 
                                                    <Passengers i:nil="true" /> 
                                                    <BookingComments i:nil="true" /> 
                                                    <BookingContacts i:nil="true" /> 
                                                    <NumericRecordLocator>'.$num_record_locator.'</NumericRecordLocator> 
                                                    <RestrictionOverride>false</RestrictionOverride> 
                                                    <ChangeHoldDateTime>false</ChangeHoldDateTime> 
                                                    <WaiveNameChangeFee>false</WaiveNameChangeFee> 
                                                    <WaivePenaltyFee>false</WaivePenaltyFee> 
                                                    <WaiveSpoilageFee>false</WaiveSpoilageFee> 
                                                    <DistributeToContacts>false</DistributeToContacts> 
                                                </BookingCommitRequestData>
                                            </BookingCommitRequest>';
                        
                        $res_book_commit = $this->connect($this->url_transaction, $apilog_code, 'BookingCommit', $header, $param_commit);
                        if($res_book_commit['error_no'] == '0'){
                            $result['error_no'] = '0';
                            $result['error_msg'] = 'sukses';                            
                        }
                        else{
                            $result['error_no'] = '706';
                            $result['error_msg'] = 'gagal commit';
                        }
                    }
                    else{
                        $result['error_no'] = '705';
                        $result['error_msg'] = 'gagal menambahkan payment';
                    }
                }
                else if($res->BookingInfo->BookingStatus == 'Confirmed'){
                    $result['error_no'] = '703';
                    $result['error_msg'] = 'bookingan anda telah terissue';
                }
                else{
                    $result['error_no'] = '704';
                    $result['error_msg'] = 'gagal mendapatkan detail bookingan anda';
                }
            }
            else{
                $result['error_no'] = '702';
                $result['error_msg'] = 'gagal mendapatkan data bookingan anda';
            }
        }
        else{
            $result['error_no'] = '701';
            $result['error_msg'] = 'gagal login ke maskapai citiilink';
        }
        return $result;
    }
    
    function getTicketInfo($book_code){
        $ci = & get_instance();
        
        $signature = $this->logon();
        
        if($signature['error_no'] == '0'){
            $header[] = array('http://schemas.navitaire.com/WebServices', 'ContractVersion', '0');
            $header[] = array('http://schemas.navitaire.com/WebServices', 'Signature', $signature['signature']);
            $apilog_code = $ci->converter->encode($signature['signature']);
            
            $param  =  '<GetBookingRequest xmlns="http://schemas.navitaire.com/WebServices/ServiceContracts/BookingService">
                            <GetBookingReqData xmlns:a="http://schemas.navitaire.com/WebServices/DataContracts/Booking" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                                <a:GetBookingBy>RecordLocator</a:GetBookingBy> 
                                <a:GetByRecordLocator>
                                    <a:RecordLocator>'.$book_code.'</a:RecordLocator> 
                                </a:GetByRecordLocator>
                                <a:GetByThirdPartyRecordLocator i:nil="true" /> 
                                <a:GetByID i:nil="true" /> 
                            </GetBookingReqData>
                        </GetBookingRequest>';
                        
            $res_get_book = $this->connect($this->url_transaction, $apilog_code, 'GetBooking', $header, $param);
            if($res_get_book['error_no'] == '0'){
                $res = $res_get_book['result'];
                $res = $res->Booking;
                $status = $res->BookingInfo->BookingStatus;
                if($status == 'Confirmed'){
                    $pax = $this->convertObjToArray($res->Passengers->Passenger);
                    #{"error_no":"0","status":"booking","pax":[{"flights_name":"Han Tan","flights_ticket":"10273613143007"}]}
                    foreach($pax as $p){
                        $first_name = $p->Names->BookingName->FirstName;
                        $last_name = $p->Names->BookingName->LastName;
                        
                        $arr_pax[]  = array("flights_name"      => $first_name." ".$last_name,
                                            "flights_ticket"    => $p->PassengerID);
                        $inf = $p->Infant;
                        if($inf != NULL){
                            $fname_inf = $inf->Names->BookingName->FirstName;
                            $lname_inf = $inf->Names->BookingName->LastName;
                            
                            $arr_pax[]  = array("flights_name"      => $fname_inf." ".$lname_inf,
                                            "flight_s_ticket"       => $p->PassengerID);
                        }
                    }
                    
                    $book_status = "konfirm";
                    
                    $result = array("error_no"  => '0',
                                    "status"    => $book_status,
                                    "pax"       => $arr_pax);
                }
                else{
                    $result['error_no'] == '802';
                    $result['error_msg'] == 'gagal issue. silakan hubungi customer service kami';
                }
            }
        }
        else{
            $result['error_no'] = '801';
            $result['error_msg'] = 'gagal login ke maskapai citiilink';
        }
        
        return $result;
    }
    
    function logon(){
        $ci = & get_instance();
        
        $ci->load->database('default', TRUE);
        
        $header = array('http://schemas.navitaire.com/WebServices', 'ContractVersion', '0');
        
        $param = '<LogonRequest xmlns="http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService">
                    <logonRequestData xmlns:a="http://schemas.navitaire.com/WebServices/DataContracts/Session" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                    <a:DomainCode>EXT</a:DomainCode> 
                    <a:AgentName>Api_Gih</a:AgentName> 
                    <a:Password>@P1_gih2017</a:Password> 
                    <a:LocationCode i:nil="true" /> 
                    <a:RoleCode>APIA</a:RoleCode> 
                    <a:TerminalInfo i:nil="true" /> 
                    </logonRequestData>
                    </LogonRequest>';
        
        $result = $this->connect($this->url_session, 'Logon', 'Logon', $header, $param);
        
        if($result['error_no'] == '0'){
            $res = array('error_no' => '0', 'signature' => $result['result']->Signature);
        }
        else{
            $res = array('error_no' => '717', 'error_msg' =>$result['error_msg']);
        }
        return $res;
    }
    
    // function connect($url, $apilog_code, $action, $header, $param){
    //  $ci = & get_instance();
    //  $resultCurl= $ci->curl->post('http://dev.aeroaffiliate.com/service/service_citilink/','http://dev.aeroaffiliate.com/service/service_citilink/',array("param"=>json_encode($param), 'action'=>$action, 'url'=>$url, 'header' => json_encode($header)));
    //  $result = (array) json_decode($resultCurl['result']);
        
    //  $this->saveLog($apilog_code, $action, $param, $result);
        
    //  return $result;
    // }
    
    function getBalance() {
        $ci = & get_instance();
        
        $signature = $this->logon();
        
        if($signature['error_no'] == '0'){
            $header[] = array('http://schemas.navitaire.com/WebServices', 'ContractVersion', '340');
            $header[] = array('http://schemas.navitaire.com/WebServices', 'Signature', $signature['signature']);
            $apilog_code = $ci->converter->encode($signature['signature']);
            
            $param  =  '<GetAccountRequest xmlns="http://schemas.navitaire.com/WebServices/ServiceContracts/AccountService">
            <GetAccountReqData xmlns:a="http://schemas.navitaire.com/WebServices/DataContracts/Account" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                <a:AccountID>246</a:AccountID>
            </GetAccountReqData>
        </GetAccountRequest>';
        
            $res_get_book = $this->connect($this->url_transaction, $apilog_code, 'AccountManager', $header, $param);
            print_r($res_get_book);
        }
        else{
            $result['error_no'] = '801';
            $result['error_msg'] = 'gagal login ke maskapai citiilink';
        }

    }
    
    function connect($url, $apilog_code, $action, $arr_header, $param){
        $ci = & get_instance();
        
        foreach($arr_header as $h){
            $header[] = new SoapHeader($h[0], $h[1], $h[2]);
        }
        
        try{
            // 6/11/2018 add stream context
            $soapClient = new SoapClient($url, array("trace"=>true, 'stream_context'=>stream_context_create(
                                                                        array('http'=>
                                                                            array(
                                                                                'protocol_version'=>'1.0',
                                                                                'header' => 'Connection: Close'
                                                                            )
                                                                        )
                                                                    )
                                                    ));
        
            $soapClient->__setSoapHeaders($header);
            
            $row = $soapClient->__soapCall($action, array(new soapVar($param, XSD_ANYXML)));
            
            $result = array('error_no' => '0', 'result'=>$row);
        } catch (SoapFault $exception) {
            
            $result = array('error_no' => '717', 'error_msg' => $exception->faultstring);
        }
        
        $this->saveLog($apilog_code, $action, $param, $result);
        
        return $result;
    }

    function logout(){
        $soapClient = new SoapClient($this->url_session, array("trace"=>true));
        
        $header[] = array('http://schemas.navitaire.com/WebServices', 'ContractVersion', '0');
        $header[] = array('http://schemas.navitaire.com/WebServices', 'Signature', 'pYD8edof4cY=|NTdihFjEaxDts3jvrBA8s4EQzHSu1v/TIMFf+xWeuxZqVHn6HVV6mKlNVmJp4/4BpZFTIVS9RiO+ew28Dmz+80/XfHRAv4Ga1S99ieJZapLBIXqjxSMHLonf2eAVlC7LUzOUSM8I2V4=');
        
        $soapClient->__setSoapHeaders($header);
        print_r($soapClient->__getFunctions());
        
        #$trip_var = new SoapVar($param, SOAP_ENC_OBJECT, 'logonRequestData', 'http://schemas.navitaire.com/WebServices/DataContracts/Session');    
        $trip_var = '<LogoutRequest xmlns="http://schemas.navitaire.com/WebServices/ServiceContracts/SessionService"/>';
        
        
        echo 'test7';
        $row = $soapClient->__soapCall('Logout', array(new soapVar($trip_var, XSD_ANYXML)));
        print_r($row);
    }
    
    function convertTimeCitilink($string){
        $time = substr($string, 11,5);
        return $time;
    }
    
    function convertDateCitilink($string){
        $time = substr($string, 0,10);
        return $time;
    }
    
    function convertObjToArray($obj){
        if(is_array($obj)){
            $result = $obj;
        }
        else{
            $result[0] = $obj;
        }
        return $result;
    }
    
    function saveLog($apilog_code, $action, $param, $result){
        $ci = & get_instance();
        $post   = array("apilog_code" => $apilog_code,
                        "apilog_action" => $action,
                        "apilog_post" => $param,
                        "apilog_response" => json_encode($result),
                        "apilog_date" => date("Y-m-d H:i:s"));
        
        $ci->load->database('default', TRUE);
        $ci->mgeneral->save($post, 'apilog_citilink');
    }
}
?>