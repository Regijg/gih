<?php

class Converter
{
    var $encrypt_method = "AES-256-CBC";
    var $secret_key = 'pakemtour';
    var $secret_iv = 'pakemrealtravel';

    function __construct() {
        $ci = & get_instance();
    }

    function encode($string) {

        $output = false;
        $key = hash('sha256', $this->secret_key);
        $iv = substr(hash('sha256', $this->secret_iv), 0, 16);
        $output = openssl_encrypt($string, $this->encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
        return $output;
    }

    function decode($string) {

        $output = false;
        $key = hash('sha256', $this->secret_key);
        $iv = substr(hash('sha256', $this->secret_iv), 0, 16);
        $output = openssl_decrypt(base64_decode($string), $this->encrypt_method, $key, 0, $iv);
        return $output;
    }

	function cek_is_logged(){
		$token = $this->db->query("SELECT agent_token from agent where agent_id = ".$this->session->userdata['id']);
		if($this->session->userdata['token'] != "" && $this->session->userdata['id'] != ""){

		}


	}
	function sqltotime($sqltime)
	{
		date_default_timezone_set('Asia/Jakarta');
		$pot1	= explode(" ",$sqltime);
		$pot2	= explode("-",$pot1[0]);
		$pot3	= explode(":",$pot1[1]);

    	return mktime($pot3[0],$pot3[1],$pot3[2],$pot2[1],$pot2[2],$pot2[0]);
		//return "$pot3[0],$pot3[1],$pot3[2],$pot2[1],$pot2[2],$pot2[0]";
	}

	function setTanggalNama($data,$tipe)
	{
		date_default_timezone_set('Asia/Jakarta');
		$day = date('l', strtotime($data));

		list($t,$b,$h) = split('[-]', $data);
		switch($b)
		{
			case"01"; $bln="Januari"; break;
			case"02"; $bln="Februari"; break;
			case"03"; $bln="Maret"; break;
			case"04"; $bln="April"; break;
			case"05"; $bln="Mei"; break;
			case"06"; $bln="Juni"; break;
			case"07"; $bln="Juli"; break;
			case"08"; $bln="Agustus"; break;
			case"09"; $bln="September"; break;
			case"10"; $bln="Oktober"; break;
			case"11"; $bln="November"; break;
			case"12"; $bln="Desember"; break;
		}
		switch($day)
		{
			case"Sunday";	$weekday="Minggu";	break;
			case"Monday";	$weekday="Senin";	break;
			case"Tuesday";	$weekday="Selasa";	break;
			case"Wednesday";$weekday="Rabu";	break;
			case"Thursday";	$weekday="Kamis";	break;
			case"Friday";	$weekday="Jumat";	break;
			case"Saturday";	$weekday="Sabtu";	break;
		}
		
		if($tipe=="2"):
			$tglIndo="$h $bln $t";
		else:
			$tglIndo=" $weekday, $h $bln $t";
		endif;
		return $tglIndo;
	}

	function setTanggalNamaTime($data)
	{
		date_default_timezone_set('Asia/Jakarta');
		$arr = explode(' ',$data);
		$day = date('l', strtotime($arr[0]));

		list($t,$b,$h) = split('[-]', $arr[0]);
		switch($b)
		{
			case"01"; $bln="Januari"; break;
			case"02"; $bln="Februari"; break;
			case"03"; $bln="Maret"; break;
			case"04"; $bln="April"; break;
			case"05"; $bln="Mei"; break;
			case"06"; $bln="Juni"; break;
			case"07"; $bln="Juli"; break;
			case"08"; $bln="Agustus"; break;
			case"09"; $bln="September"; break;
			case"10"; $bln="Oktober"; break;
			case"11"; $bln="November"; break;
			case"12"; $bln="Desember"; break;
		}
		switch($day)
		{
			case"Sunday";	$weekday="Minggu";	break;
			case"Monday";	$weekday="Senin";	break;
			case"Tuesday";	$weekday="Selasa";	break;
			case"Wednesday";$weekday="Rabu";	break;
			case"Thursday";	$weekday="Kamis";	break;
			case"Friday";	$weekday="Jumat";	break;
			case"Saturday";	$weekday="Sabtu";	break;
		}

		$tglIndo=" $weekday $h $bln $t $arr[1]";
		return $tglIndo;
	}

	function set_codeToCity($code) {
        $CI = & get_instance();
        $CI->db = $CI->load->database('default', TRUE);
        $query = $CI->db->query("select city as code_city from mstr_airport where airport_code='$code'");
        $q = $query->result();
        foreach ($q as $row) {
            return $row->code_city;
        }
    }

	function code_to_city($code)
	{
		$query = $this->db->query("select concat(airport_code,' ( ',city,' ) ') as code_city from mstr_airport where airport_code='$code'");
		$q = $query->result();
		foreach ($q as $row)
		{
			return $row->code_city;
		}
	}

	function code_to_city_button($code)
	{
		$query = $this->db->query("select concat(city,', ',airport_code) as code_city from airport where airport_code='$code'");
		$q = $query->result();
		foreach ($q as $row)
		{
			return $row->code_city;
		}
	}

	# Convert an stdClass to Array.
	function object_to_array(stdClass $Class){
		# Typecast to (array) automatically converts stdClass -> array.
		$Class = (array)$Class;

		# Iterate through the former properties looking for any stdClass properties.
		# Recursively apply (array).
		foreach($Class as $key => $value){
			if(is_object($value)&&get_class($value)==='stdClass'){
				$Class[$key] = self::object_to_array($value);
			}
		}
		return $Class;
    }

    function set_codeToStation($code) {
        $CI = & get_instance();
        $CI->db = $CI->load->database('default', TRUE);
        $query = $CI->db->query("select station_name as code_city from mstr_station where station_code='$code'");
        $q = $query->result();
        foreach ($q as $row) {
            return $row->code_city;
        }
    }
    
    # Convert an Array to stdClass.
    function array_to_object(array $array){
		# Iterate through our array looking for array values.
		# If found recurvisely call itself.
		foreach($array as $key => $value){
			if(is_array($value)){
				$array[$key] = self::array_to_object($value);
			}
		}

		# Typecast to (object) will automatically convert array -> stdClass
		return (object)$array;
    }

	#convert id airlines to name airlines
	function id_to_name($code)
	{
		$query = $this->db->query("select airlines_img from airlines where airlines_id='".$code."'");
		$q = $query->result();
		foreach ($q as $row)
		{
			return $row->airlines_img;
		}
	}
	// $array = array variabel $key= key of array $value= value of each key
	function array_push_assoc($array, $key, $value){
		$array[$key] = $value;
		return $array;
	}
		#convert id airlines to Name airlines
	function id_to_name_airlines($code)
	{
		$ci = & get_instance();
		$ci->load->database('sistem', TRUE);
		$query = $ci->db->query("select airlines_name from airlines where airlines_id='".$code."'");
		$q = $query->result();
		foreach ($q as $row)
		{
			return $row->airlines_name;
		}
	}

	# Convert to Indonesian Month Name. Ex : $code = '01'
    function month_indonesian($code){

		switch($code) {
			case "01":
				$bulan = "Januari";
			break;
			case "02":
				$bulan = "Februari";
			break;
			case "03":
				$bulan = "Maret";
			break;
			case "04":
				$bulan = "April";
			break;
			case "05":
				$bulan = "Mei";
			break;
			case "06":
				$bulan = "Juni";
			break;
			case "07":
				$bulan = "Juli";
			break;
			case "08":
				$bulan = "Agustus";
			break;
			case "09":
				$bulan = "September";
			break;
			case "10":
				$bulan = "Oktober";
			break;
			case "11":
				$bulan = "November";
			break;
			case "12":
				$bulan = "Desember";
			break;
		}
		# Typecast to (object) will automatically convert array -> stdClass
		return $bulan;
    }
	function ccy($from,$to,$value)
	{
		$ci = & get_instance();
		$ci->load->database('default', TRUE);
		$datSetting	= $ci->mgeneral->getWhere(array('general_setting_kelompok'=>"ccy",'general_setting_name'=>"bca"),'general_setting');
		foreach($datSetting as $d) {
			$upHarga		= $d->general_setting_nilai;
		}

		$cookie_file	= getcwd()."/cookie/bank/BCA-currency.tmp";
		$result	= $ci->curl->post("http://www.bca.co.id/id/biaya-limit/kurs_counter_bca/kurs_counter_bca_landing.jsp", "http://www.klikbca.com", "", $this->cookie_file);

		$html 	= $ci->domparser->str_get_html($result['result']);
		foreach ($html->find('table') as $table) {
			$data[] = $table->outertext;
		}

		$html2	= $ci->domparser->str_get_html($data['1']);
		foreach ($html2->find('tr') as $table2) {
			$data2[] = $table2->outertext;
		}

		for($a=2;$a<count($data2);$a++)
		{
			//echo htmlentities($data2[$a])."<br><Br>";
			$data3	= array();
			$html3	= $ci->domparser->str_get_html($data2[$a]);
			foreach ($html3->find('td') as $table3) {
				$data3[] = $table3->innertext;
			}

			$ccy[$data3[0]]=(number_format($data3[1], 0, '.', '')+$upHarga);
		}

		if($to=="IDR")
		{
			$convert	= $ccy[$from]*$value;
			return number_format($convert, 0, '.', '');
		}
		else
		{
			return 0;
		}
	}

	function ccy1 ($from, $value) {
		/*$ci	 =& get_instance();
		$kurs= 0;
		$ci->load->database('default', TRUE);
		$kursDB	= $ci->mgeneral->getWhere(array('matauang'=>$from),'kurs');
		$dt = (array) $kursDB[0];
		$dt['matauang']=0;
		$pkey = array_keys($dt, max($dt));
		$kurs = max($dt);
		foreach($kursDB as $d) {
			$bca	= $d->bca;
			$bi 	= $d->bi;
		}
		$bankA	= $pkey[0];

		$datSetting	= $ci->mgeneral->getWhere(array('general_setting_kelompok'=>'ccy','general_setting_name'=>$bankA),'general_setting');
		foreach($datSetting as $d) {
			$upHarga = $d->general_setting_nilai;
		}

		$data = array ('ccy'=>$kurs, 'upHarga'=>$upHarga, 'bank'=>$bankA);
		return $data;*/
	}

	function ambilAgentId($username)
	{
		$ci = & get_instance();
		$ci->db = $ci->load->database('sistem', TRUE);
		$query = $ci->db->query("select agent_id from agent where agent_username='".$username."'");
		$q = $query->result();
		foreach ($q as $row)
		{
			return $row->agent_id;
		}
	}

	function ambilAgentUsername($agent_id)
	{
		$ci = & get_instance();
		$ci->db = $ci->load->database('sistem', TRUE);
		$query = $ci->db->query("select agent_username from agent where agent_id='".$agent_id."'");
		$q = $query->result();
		foreach ($q as $row)
		{
			return $row->agent_username;
		}
	}
	function calcAge($birthDate){
		return floor( (strtotime(date('Y-m-d')) - strtotime($birthDate)) / 31556926);
		/*$interval = date_diff(date_create(), date_create('1983-12-25'));
		$dt = explode(":", $interval->format("%Y:%M:%d"));

		$diff = $birthday->diff(new DateTime());
		$months = $diff->format('%m') + 12 * $diff->format('%y');

		return $dt;*/
	}

	function calcAgeMonth($birthDate){
		/*$birthday = new DateTime($birthDate);
		$diff = $birthday->diff(new DateTime());
		return $months = $diff->format('%m') + 12 * $diff->format('%y'); */

		$dob = strtotime($birthDate);
		$current_time = time();

		$age_years = date('Y',$current_time) - date('Y',$dob);
		$age_months = date('m',$current_time) - date('m',$dob);
		$age_days = date('d',$current_time) - date('d',$dob);

		if ($age_days<0) {
			$days_in_month = date('t',$current_time);
			$age_months--;
			$age_days= $days_in_month+$age_days;
		}

		if ($age_months<0) {
			$age_years--;
			$age_months = 12+$age_months;
		}

		return (($age_years*12)+$age_months);
	}

	function urutkanPax($pax)
	{
		$ci = & get_instance();
		$ci->load->database('sistem', TRUE);
		return $pax;
/*		$query = $ci->db->query("select agent_id from agent where agent_username='".$username."'");
		$q = $query->result();
		foreach ($pax as $row)
		{
			return $row->agent_id;
		}*/
	}

	#fungsi untuk mengurutkan array multi dimensi
	function aasort (&$array, $key) {
		$sorter=array(); $ret=array();
		reset($array);
		foreach ($array as $ii => $va) {
			$sorter[$ii]=$va[$key];
		}
		asort($sorter);

		$i=0;
		foreach ($sorter as $ii => $va) {
			$ret[$i]=$array[$ii];
			$i++;
		}

		return $ret;
	}

	/* Pengecekan Array Kosong / NULL */
	public function is_multiArrayEmpty($multiarray) {
		if(is_array($multiarray) and !empty($multiarray)){
			$tmp = array_shift($multiarray);
				if(!is_multiArrayEmpty($multiarray) or !is_multiArrayEmpty($tmp)){
					return false;
				}
				return true;
		}
		if(empty($multiarray)){
			return true;
		}
		return false;
	}
	/* Pengecekan Array Kosong / NULL */

	/* Fungsi Converter error message */
	public function getErrorText($code='')
	{
		$ci =& get_instance();
		# Load DB Default
		$ci->load->database('default', TRUE);
		# Get Data Error
		$error					= $ci->mgeneral->getWhere(array('code'=>$code),"error_code");
		# Result
		if(!empty($error)):
			$result['error_no']		= $error[0]->code;
			$result['error_msg']	= $error[0]->error_msg;
		else:
			$result['error_msg']	= "masukkan error no dan error msg ke Databse";
		endif;

		return $result;
	}
	/* Fungsi Converter error message */

	function replace_name($name){
		$name		= preg_replace('/(\'|\.|\,|\")/', '', $name);
		$arr_new 	= explode(' ', $name);
		$new_name 	= $arr_new[0];

		for($i=1; $i<count($arr_new); $i++){
			if(strlen($arr_new[$i-1])==1){
				$new_name .= str_replace(' ', "", $arr_new[$i]);
			}
			else if(strlen($arr_new[$i])==1 && $i==count($arr_new)-1){
				$new_name .= str_replace(' ', "", $arr_new[$i]);
			}
			else{
				$new_name .= " ".str_replace(' ', "", $arr_new[$i]);
			}
		}

		return $new_name;
	}

	function header_logo_ticket($airline_id, $flight_no = '') {
		switch ($airline_id) {
			case '1': $path = base_url().'asset/image/icon-flight/logo-airasia.png'; break;
			case '2': $path = base_url().'asset/image/icon-flight/logo-citilink.png'; break;
			case '3': $path = base_url().'asset/image/icon-flight/logo-garuda.png'; break;
			case '4': $path = base_url().'asset/image/icon-flight/logo-sriwijaya.png'; break;
			case '5': $path = base_url().'asset/image/icon-flight/logo-lionair.png'; break;
			case '7': $path = base_url().'asset/image/icon-flight/logo-expressair.png'; break;
			case '8': $path = base_url().'asset/image/icon-flight/logo-transnusa.png'; break;
			case '9': $path = base_url().'asset/image/icon-flight/logo-trigana.png'; break;
		}

		return $path;
	}

	function term_ticket($airline_id = '', $flight_no = '', $type = '') {
		if ($type == 'html') {
			switch ($airline_id) {
				case '1': $path = 'ticket/flight/html_term/term_airasia'; break;
				case '2': $path = 'ticket/flight/html_term/term_citilink'; break;
				case '3': $path = 'ticket/flight/html_term/term_garuda'; break;
				case '4': $path = 'ticket/flight/html_term/term_sriwijaya'; break;
				case '5': $path = 'ticket/flight/html_term/term_lion'; break;
			}
		} else {
			switch ($airline_id) {
				case '1': $path = 'ticket/flight/pdf_term/term_airasia'; break;
				case '2': $path = 'ticket/flight/pdf_term/term_citilink'; break;
				case '3': $path = 'ticket/flight/pdf_term/term_garuda'; break;
				case '4': $path = 'ticket/flight/pdf_term/term_sriwijaya'; break;
				case '5': $path = 'ticket/flight/pdf_term/term_lion'; break;
			}
		}

		return $path;
	}

	function mobile_api_format($result){
		
		if($result['rest_no'] == 0):
			$formatResult = '{
								"header": {"app_name":"TABS"},"meta" : {"rest_no" : 0,"reason": "Success"},
								"data":['.json_encode($result['data']).']
					   		}';
		else:
			$formatResult = '{
								"header": {"app_name":"SPIRIT"},"meta" : {"rest_no" : 1,"reason": "'.$result['reason'].'"},
								"data":[]
					   		}';
		endif;
		
		echo $formatResult;
	}
	
	function xml_format($array, $rootElement = null, $xml = null)
	{
		$_xml = $xml;
 
		  if ($_xml === null) {
			$_xml = new SimpleXMLElement($rootElement !== null ? $rootElement : '<tabs/>');
		  }
		 
		  foreach ($array as $k => $v) {
			if (is_array($v)) { //nested array
			  if(is_numeric($k)){
			  	$this->xml_format($v, $k, $_xml->addChild("dt"));
			  }else{
			  	$this->xml_format($v, $k, $_xml->addChild($k));
			  }
			} else {
			  $_xml->addChild($k, $v);
			}
		  }
		 
		  return $_xml->asXML();
	}
}

?>
