<?php

class transaction
{	
    function __construct()
    {
		$ci =& get_instance();
    }
    
	function index($action,$parameter)
	{
	
		switch($action)
		{
			case "ppob";
				$result = $this->ppob($parameter);
				break;
			case "flight";
				$result = $this->flight($parameter);
				break;
			case "flight_detail";
				$result = $this->flight_detail($parameter);
				break;
			case "deposit";
				$result = $this->deposit($parameter);
				break;
			case "transaction";
				$result = $this->transaction($parameter);
				break;
			case "train";
				$result = $this->train($parameter);
				break;
			case "train_detail";
				$result = $this->train_detail($parameter);
				break;
			case "ppob_detail";
				$result = $this->ppob_detail($parameter);
				break;
			
		}
		return $result;
	}
	
	public function ppob($param) {
		$ci =& get_instance();
		
		$page 		= @$param['page'];
		$per_page 	= @$param['per_page'];
		$partner_id = $ci->converter->decode($param['partner_id']);
		$start_date = @$param['start_date'];
		$end_date 	= @$param['end_date'];
		$keyword  	= @$param['keyword'];
		$cek_partner = $ci->mgeneral->getWhere(array("partner_id" => $partner_id), "mstr_partner");
		if($cek_partner[0]->skema_bisnis_id == '3' || $cek_partner[0]->skema_bisnis_id == '4'){
			$partner_data = $ci->mgeneral->getWhere(array("corporate_id" => $cek_partner[0]->corporate_id), "mstr_partner");
			$arr_partner_id = array();
			foreach($partner_data as $pd){
				$arr_partner_id[] = $pd->partner_id;
			}
			$partner_id = implode(",", $arr_partner_id);
		}
		#handle untuk query ke database
		if(!$per_page): $per_page = 10; endif;
		if(!$page): $page = 1; endif;
		
		if($page!="1"): $first_row = (($page-1)*$per_page);elseif($page==1): $first_row = '0';endif;
		
		if ($page <= 0) {
			$result["rest_no"] = "38";
			$result["reason"] = getMobileErrorText("38");
			return $result;
			exit();
		} else {
			$data = $ci->mquery->ppob_transaction_data($partner_id, $keyword, $start_date, $end_date);
		}
		
		if (count($data) == 0) {
			$result["rest_no"] = "37";
			$result["reason"] = getMobileErrorText("37");
		} else {
			$result["rest_no"] = "0";
			
			$totaldata			= count($data);
			$page				= explode(".",$totaldata/$per_page);
			$sisaPage			= $totaldata%$per_page;
			if($sisaPage==0): $totalpage = $page[0]; else: $totalpage = $page[0]+1; endif;
			if($param['page']==""): $cpage = "1"; else: $cpage=$param['page']; endif;
			
			$result["current_page"] = $cpage;
			$result["total_page"] 	= $totalpage;
			
			$get_data = $ci->mquery->ppob_transaction_data($partner_id, $keyword, $start_date, $end_date, $first_row, $per_page);
			
			for($i = 0; $i < count($get_data); $i++) {
				$emailP = $ci->mgeneral->getValue("email",array('partner_id'=>$get_data[$i]->partner_id),"mstr_partner");
				$get_data[$i]->dtp_id = $ci->converter->encode($get_data[$i]->dtp_id);
				$get_data[$i]->partner_id = $ci->converter->encode($get_data[$i]->partner_id);
				$get_data[$i]->partner_email = $emailP;
				unset($get_data[$i]->partner_id);
			}
			
			
			$result["transaction_list"] = isset($get_data) ? $get_data : array();
		}
		
		return $result;
	}

	public function ppob_detail($param) {
		$ci =& get_instance();
		
		$book_id = $ci->converter->decode($param['book_id']);
		$data = $ci->mquery->ppob_transaction_detail($book_id);

		if (count($data) == 0) {
			$result["rest_no"] = "37";
			$result["reason"] = getMobileErrorText("37");
		} else {
			$result["rest_no"] = "0";
			
			$data[0]->dtp_id = $ci->converter->encode($data[0]->dtp_id);
			unset($data[0]->partner_id);
			
			$result["transaction_detail"] = $data[0];
		}

		return $result;
	}

	public function flight($param) {
		$ci =& get_instance();
		
		$page 		= @$param['page'];
		$per_page 	= @$param['per_page'];
		$partner_id = $ci->converter->decode($param['partner_id']);
		$start_date = @$param['start_date'];
		$end_date 	= @$param['end_date'];
		$keyword  	= @$param['keyword'];
		$cek_partner = $ci->mgeneral->getWhere(array("partner_id" => $partner_id), "mstr_partner");
		if($cek_partner[0]->skema_bisnis_id == '3' || $cek_partner[0]->skema_bisnis_id == '4'){
			$partner_data = $ci->mgeneral->getWhere(array("corporate_id" => $cek_partner[0]->corporate_id), "mstr_partner");
			$arr_partner_id = array();
			foreach($partner_data as $pd){
				$arr_partner_id[] = $pd->partner_id;
			}
			$partner_id = implode(",", $arr_partner_id);
		}

		#handle untuk query ke database
		if(!$per_page): $per_page = 10; endif;
		if(!$page): $page = 1; endif;
		
		if($page!="1"): $first_row = (($page-1)*$per_page);elseif($page==1): $first_row = '0';endif;
		if ($page <= 0) {
			$result["rest_no"] = "38";
			$result["reason"] = getMobileErrorText("38");
			return $result;
			exit();
		} else {

			$data = $ci->mquery->flight_transaction($partner_id, $keyword, $start_date, $end_date);

		}
		
		if (count($data) == 0) {
			$result["rest_no"] = "37";
			$result["reason"] = getMobileErrorText("37");
		} else {
			$result["rest_no"] = "0";
			
			$totaldata			= count($data);
			$page				= explode(".",$totaldata/$per_page);
			$sisaPage			= $totaldata%$per_page;
			if($sisaPage==0): $totalpage = $page[0]; else: $totalpage = $page[0]+1; endif;
			if($param['page']==""): $cpage = "1"; else: $cpage=$param['page']; endif;
			
			$result["current_page"] = $cpage;
			$result["total_page"] 	= $totalpage;
			
			$get_data = $ci->mquery->flight_transaction($partner_id, $keyword, $start_date, $end_date, ($first_row), $per_page);
			
			for($i = 0; $i < count($get_data); $i++) {
				$emailP = $ci->mgeneral->getValue("email",array('partner_id'=>$get_data[$i]->partner_id),"mstr_partner");
				$get_data[$i]->dtf_id = $ci->converter->encode($get_data[$i]->dtf_id);
				$get_data[$i]->partner_id = $ci->converter->encode($get_data[$i]->partner_id);
				$get_data[$i]->partner_email = $emailP;
				unset($get_data[$i]->app);
			}
			
			
			$result["transaction_list"] = isset($get_data) ? $get_data : array();
		}
		
		return $result;
	}

	public function flight_detail($param) {
		$ci =& get_instance();
		
		$bookid 	= $ci->converter->decode($param['book_id']);
		$page 		= @$param['page'];
		$per_page 	= @$param['per_page'];
		
		$data = $ci->mgeneral->getWhere(array("dtf_id" => $bookid), "dt_flight");
		
		if (count($data) == 0) {
			$result["rest_no"] = "37";
			$result["reason"] = getMobileErrorText("37");
		} else {
			$result = build_issue_result($bookid);
			$result['total_fares'] 		= (int)($data[0]->dtf_tiket_price);
			$result['commission'] 		= (int)($data[0]->dtf_comission);
			$result["commission_corp"]  = (int)($data[0]->dtf_comission_corp + $data[0]->dtf_branch_cashback);
			$result['NTA'] 				= (int)($data[0]->dtf_tiket_price - $data[0]->dtf_comission);
			$result['margin']			= (int)($data[0]->dtf_margin);
		}

		return $result;
	}

	private function deposit($param) {
		$ci =& get_instance();
			
		$partner_id = $ci->converter->decode($param['partner_id']);
		$start		= $param['start_date'];
		$end		= $param['end_date'];
		
		$cek_user	= $ci->mgeneral->getWhere(array('partner_id'=>$partner_id),'mstr_partner');
		
		
		if ($param['partner_id'] == "") {
			$result["rest_no"] = "30";
			$result["reason"] = getMobileErrorText($result["rest_no"]);
		} else if (count($cek_user)== 0) {
			$result["rest_no"] = "30";
			$result["reason"] = getMobileErrorText($result["rest_no"]);
		}else if ($start == "") {
			$result["rest_no"] = "50";
			$result["reason"] = getMobileErrorText($result["rest_no"]);
		}else if ($end == "") {
			$result["rest_no"] = "51";
			$result["reason"] = getMobileErrorText($result["rest_no"]);
		}else{
			
			$data	= $ci->mquery->get_deposit_report($partner_id,$start." 00:00:00",$end." 23:59:59");
			if(count($data)==0):
				$result["rest_no"] = "52";
				$result["reason"]= getMobileErrorText($result["rest_no"]);
			else:
				$result['rest_no']		= "0";
				$cDeposit = $ci->mgeneral->getValue("current_deposit",array('partner_id'=>$partner_id),"mstr_partner");
				$tdebet = 0 ; $tkredit= 0;
				foreach($data as $d):
					if($d->payment_type=="debet"):
						$tdebet = $tdebet+$d->payment_amount;
					else:
						$tkredit= $tkredit+$d->payment_amount;
					endif;
					
					$reportdata[] = array("report_date"	=> $d->payment_date,
										  "keterangan"	=> $d->payment_message,
										  "type"		=> $d->payment_type,
										  "amount"		=> $d->payment_amount,
										  "last_deposit"=> $d->payment_last_deposit);
				endforeach;
				$result['report_data']		= $reportdata;
				$result['total_debet']		= $tdebet;
				$result['total_kredit']		= $tkredit;
				$result['current_deposit']	= $cDeposit;
			endif;
		}
		return $result;
	}
	
	private function transaction($param) {
		$ci =& get_instance();
			
		$partner_id = $ci->converter->decode($param['partner_id']);
		$start		= $param['start_date'];
		$end		= $param['end_date'];
		
		$cek_user	= $ci->mgeneral->getWhere(array('partner_id'=>$partner_id),'mstr_partner');
		
		
		if ($param['partner_id'] == "") {
			$result["rest_no"] = "30";
			$result["reason"] = getMobileErrorText($result["rest_no"]);
		} else if (count($cek_user)== 0) {
			$result["rest_no"] = "30";
			$result["reason"] = getMobileErrorText($result["rest_no"]);
		}else if ($start == "") {
			$result["rest_no"] = "50";
			$result["reason"] = getMobileErrorText($result["rest_no"]);
		}else if ($end == "") {
			$result["rest_no"] = "51";
			$result["reason"] = getMobileErrorText($result["rest_no"]);
		}else{
			
			$data	= $ci->mquery->get_transaction_report($partner_id,$start." 00:00:00",$end." 23:59:59");
			if(count($data)==0):
				$result["rest_no"] = "52";
				$result["reason"]= getMobileErrorText($result["rest_no"]);
			else:
				$result['rest_no']		= "0";
				$tHarga = 0 ; $tKomisi= 0; $tMargin=0;
				foreach($data as $d):
					$tHarga = $tHarga+$d->harga;
					$tKomisi= $tKomisi+$d->komisi;
					$tMargin= $tMargin+$d->margin;
					$reportdata[] = array("trx_date"	=> $d->tgl_trx,
										  "keterangan"	=> $d->keterangan,
										  "harga"		=> $d->harga,
										  "komisi"		=> $d->komisi,
										  "margin"		=> $d->margin);
				endforeach;
				$result['report_data']		= $reportdata;
				$result['total_harga']		= $tHarga;
				$result['total_komisi']		= $tKomisi;
				$result['total_margin']		= $tMargin;
			endif;
		}
		return $result;
	}

	public function train($param) {
		$ci =& get_instance();
		
		$page 		= @$param['page'];
		$per_page 	= @$param['per_page'];
		$partner_id = $ci->converter->decode($param['partner_id']);
		$start_date = @$param['start_date'];
		$end_date 	= @$param['end_date'];
		$keyword  	= @$param['keyword'];
		$cek_partner = $ci->mgeneral->getWhere(array("partner_id" => $partner_id), "mstr_partner");
		if($cek_partner[0]->skema_bisnis_id == '3' || $cek_partner[0]->skema_bisnis_id == '4'){
			$partner_data = $ci->mgeneral->getWhere(array("corporate_id" => $cek_partner[0]->corporate_id), "mstr_partner");
			$arr_partner_id = array();
			foreach($partner_data as $pd){
				$arr_partner_id[] = $pd->partner_id;
			}
			$partner_id = implode(",", $arr_partner_id);
		}

		#handle untuk query ke database
		if(!$per_page): $per_page = 10; endif;
		if(!$page): $page = 1; endif;
		
		if($page!="1"): $first_row = (($page-1)*$per_page);elseif($page==1): $first_row = '0';endif;
		if ($page <= 0) {
			$result["rest_no"] = "38";
			$result["reason"] = getMobileErrorText("38");
			return $result;
			exit();
		} else {
			$data = $ci->mquery->train_transaction($partner_id, $keyword, $start_date, $end_date);
		}
		
		if (count($data) == 0) {
			$result["rest_no"] = "37";
			$result["reason"] = getMobileErrorText("37");
		} else {
			$result["rest_no"] = "0";
			
			$totaldata			= count($data);
			$page				= explode(".",$totaldata/$per_page);
			$sisaPage			= $totaldata%$per_page;
			if($sisaPage==0): $totalpage = $page[0]; else: $totalpage = $page[0]+1; endif;
			if($param['page']==""): $cpage = "1"; else: $cpage=$param['page']; endif;
			
			$result["current_page"] = $cpage;
			$result["total_page"] 	= $totalpage;
			
			$get_data = $ci->mquery->train_transaction($partner_id, $keyword, $start_date, $end_date, ($first_row), $per_page);
			
			for($i = 0; $i < count($get_data); $i++) {
				$emailP = $ci->mgeneral->getValue("email",array('partner_id'=>$get_data[$i]->partner_id),"mstr_partner");
				$get_data[$i]->dtt_id = $ci->converter->encode($get_data[$i]->dtt_id);
				$get_data[$i]->partner_id = $ci->converter->encode($get_data[$i]->partner_id);
				$get_data[$i]->partner_email = $emailP;
				unset($get_data[$i]->app);
			}
			
			
			$result["transaction_list"] = isset($get_data) ? $get_data : array();
		}
		
		return $result;
	}

	public function train_detail($param) {
		$ci =& get_instance();
		
		$bookid 	= $ci->converter->decode($param['book_id']);
		$page 		= @$param['page'];
		$per_page 	= @$param['per_page'];
		
		$data = $ci->mgeneral->getWhere(array("dtt_id" => $bookid), "dt_train");
		
		if (count($data) == 0) {
			$result["rest_no"] = "37";
			$result["reason"] = getMobileErrorText("37");
		} else {
			$result = build_train_pay_result($bookid);
			/*
			$result['total_fares'] 	= (int)($data[0]->dtf_tiket_price);
			$result['commission'] 	= (int)($data[0]->dtf_comission);
			$result['NTA'] 			= (int)($data[0]->dtf_tiket_price - $data[0]->dtf_comission);
			$result['margin']		= (int)($data[0]->dtf_margin);
			*/
		}

		return $result;
	}
}

?>


