<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ticket
{
	protected 	$ci;
	protected 	$url_pdf = '';
	protected 	$url_html = '';
	#protected 	$path_pdf = "C:\\xampp2\htdocs\apptik\asset\pdf";
	protected 	$path_pdf = "/var/www/html/api/asset/pdf";

	public function __construct() {
        $this->ci =& get_instance();
	}

	public function index($action, $parameter) {
		switch ($action) {
			case 'pdf_ticket': 	$result = $this->get_pdf($parameter); 	break;
			case 'html_ticket': 	$result = $this->get_html($parameter); 	break;
			case 'email_ticket': 	$result = $this->get_email($parameter); break;
		}

		return $result;
	}

	private function get_pdf($parameter) {
		switch ($parameter['product']) {
			case 'flight':
				$result = $this->flight_pdf($parameter);
				break;
			case 'hotel':
				$result = $this->hotel_pdf($parameter);
			break;
			case 'train':
				$result = $this->kereta_pdf($parameter);
				break;
			case 'ppob' : 
				$result = $this->ppob_pdf($parameter);
				break;
		}

		return $result;
	}

	private function get_html($parameter) {
		switch ($parameter['product']) {
			case 'flight':
				if ($parameter['type'] == "ticket") {
					$result = $this->flight($parameter);
				} else if ($parameter['type'] == "invoice") {
					$result = $this->flight_invoice($parameter);
				} else {
					$result = array(
						'rest_no'   => '0',
						'ticket' 	=> $this->flight($parameter),
						'invoice' 	=> $this->flight_invoice($parameter)
					);
				}
				break;
			case 'train':
				if ($parameter['type'] == "ticket") {
					$result = $this->kereta($parameter);
				} else if ($parameter['type'] == "invoice") {
					$result = $this->kereta_invoice($parameter);
				} else {
					$result = array(
						'ticket' 	=> $this->kereta($parameter),
						'invoice' 	=> $this->kereta_invoice($parameter)
					);
				}
				break;
			case 'ppob' : 
				$result = $this->ppob($parameter);
				break;
			case 'hotel' : 
				$result = $this->hotel($parameter);
				break;
		}

		return $result;
	}
	
	private function get_email($parameter) {
		switch ($parameter['product']) {
			case 'ppob' : 
				$result = $this->ppob_email($parameter);
				break;
			case 'flight':
				$result = $this->flight_email($parameter);
				break;
			case 'train':
				$result = $this->kereta_email($parameter);
				break;
			case 'hotel':
				$result = $this->hotel_email($parameter);
				break;
		}

		return $result;
	}

	/******** PLAN: ALL TICKET PLACE INSIDE LIBRARY ********/

	/******** FLIGHT ********/
	private function get_flight_data($book_id) {
		$ci =& get_instance();

		$data['dt_flight'] 			= $ci->mgeneral->getWhere(array('dtf_id' => $book_id), 'dt_flight');
		$data['dt_flight_pax'] 		= $ci->mgeneral->getWhere(array('dtf_id' => $book_id), 'dt_flight_pax');
		$data['dt_flight_schedule'] = $ci->mgeneral->getWhere(array('dtf_id' => $book_id), 'dt_flight_schedule');
		
		$partner 					= $ci->mgeneral->getWhere(array('partner_id' => $data['dt_flight'][0]->partner_id), 'mstr_partner');
		if($partner[0]->skema_bisnis_id == '3' || $partner[0]->skema_bisnis_id == '4'){
			$partner_admin 			= $ci->mgeneral->getWhere(array('skema_bisnis_id' => $partner[0]->skema_bisnis_id, 'corporate_id' => $partner[0]->corporate_id, 'partner_level' => '1'), 'mstr_partner');
			$corporate_data 		= $ci->mgeneral->getWhere(array('corporate_id' => $partner[0]->corporate_id), 'mstr_corporate');

			$data['travel']			= array("business_name" 	=> $partner_admin[0]->business_name,
											"business_address"	=> $partner_admin[0]->business_address,
											"business_phone"	=> ($corporate_data[0]->phone_number) ? $corporate_data[0]->phone_number : $corporate_data[0]->mobile_number,
											"business_logo"		=> ($corporate_data[0]->logo) ? base_url()."uploads/corp_logo/".$corporate_data[0]->logo : "");
		}

		if($data['dt_flight'][0]->dtf_status == "ticketed"){
			$data['rest_no'] = "0";
		}
		
		return $data;
	}

	public function flight($parameter) {
		$ci =& get_instance();
		$book_id = $ci->converter->decode($parameter['book_id']);

		$data['flight'] = $this->get_flight_data($book_id);
		
		if($data['flight']['rest_no'] == "0"){
			$html 	= $ci->load->view('ticket/flight/html_ticket', $data, TRUE);
			$result = array("rest_no" => "0", "ticket" => $html); 
		}
		else{
			$result	= array("rest_no"	=> "717", "reason" => "status book_id anda adalah ".$data['flight']['dt_flight'][0]->dtf_status.". sehingga tiket tidak dapat di cetak");
		}
		return $result;
	}

	public function flight_invoice($parameter) {
		$ci =& get_instance();

		$html = $ci->load->view('ticket/flight/html_invoice', $data, TRUE);
		return $html;
	}

	public function flight_pdf($parameter) {
		$ci =& get_instance();
		$book_id = $ci->converter->decode($parameter['book_id']);

		$data['flight'] = $this->get_flight_data($book_id);
		
		if($data['flight']['rest_no'] == "0"){
			$html = $ci->load->view('ticket/flight/pdf_ticket', $data, TRUE); // place for load view ticket html here

			$ci->load->library('./html2pdf/html2pdf');
			$html2pdf = new HTML2PDF('P','A4','en',true,'UTF-8',array(0, 0, 0, 0));
			$ci->html2pdf->WriteHTML($html);
			
			if ($parameter['call'] == "email_ticket") {
				$dtp_id = $ci->converter->decode($parameter['book_id']);
				$file_name = "/flight/e-ticket-".$dtp_id.".pdf";
				
				$full_path = $this->path_pdf . $file_name;
				ob_start();
				$ci->html2pdf->Output($full_path, "F");
				$result = array("rest_no" => "0", "ticket" => $full_path); 
			} else {
				$content_PDF = $ci->html2pdf->Output('', true);
				$result = array("rest_no" => "0", "ticket" => $content_PDF); 
			}
		}
		else{
			$result	= array("rest_no"	=> "717", "reason" => "status book_id anda adalah ".$data['flight']['dt_flight'][0]->dtf_status.". sehingga tiket tidak dapat di cetak");
		}
		
		return $result;
	}
	
	public function flight_email($parameter) {
		$ci =& get_instance();	
		$email 		= $parameter['email'];
		$pdf		= $this->flight_pdf($parameter);
		if($pdf['rest_no'] == '0'){
			$path['pdf']	= $pdf['ticket'];
			$book_id 		= $ci->converter->decode($parameter['book_id']);
			$dtp 			= $ci->mgeneral->getWhere(array("dtf_id" => $book_id), "dt_flight");
			
			/*edit subject dan content belum dilakukan*/
			$content		= "Terima kasih telah melakukan transaksi tiket pesawat di Pacific-Travel";
			$subject 		= "E-Ticket Pesawat Pacific-Travel";
			
			$ci->load->library("send");
			$result 		= $ci->send->emailAttach($email, $subject, $content, $path, $parameter['sender_name'], $parameter['return_address']);		
		}
		else{
			$result = $pdf;
		}
		
		/*hapus file pdf di server belum dilakukan*/
		
		return $result;
	}
	
	/******** HOTEL ********/
	public function hotel($parameter) {
		$ci =& get_instance();
		
		$book_id 		= $ci->converter->decode($parameter['book_id']);
		$data['booking']= $ci->mgeneral->getWhere(array('id_hotel_data'=>$book_id),"dt_hotel");
		
		$html = $ci->load->view('ticket/hotel/voucher', $data, TRUE); // place for load view ticket html here
		$result = array("rest_no" => "0", "ticket" => $html); 
		
		return $result; // return as binary
		// return $ci->html2pdf->Output('', "F"); // save as file on server
	}

	public function hotel_pdf($parameter) {
		$ci =& get_instance();
		
		$book_id 		= $ci->converter->decode($parameter['book_id']);
		$data['booking']= $ci->mgeneral->getWhere(array('id_hotel_data'=>$book_id),"dt_hotel");
		
		$html = $ci->load->view('ticket/hotel/voucher_pdf', $data, TRUE); // place for load view ticket html here
		#echo $html;exit;
		$ci->load->library('./html2pdf/html2pdf');
		$html2pdf = new HTML2PDF('P','A4','en',true,'UTF-8',array(0, 0, 0, 0));
		$ci->html2pdf->WriteHTML($html);

		if ($parameter['call'] == "email_ticket") {
			$file_name = "/hotel/invoice-hotel-".$book_id.".pdf";
			
			$full_path = $this->path_pdf . $file_name;
			ob_start();
			$ci->html2pdf->Output($full_path, "F");
			$result = array("rest_no" => "0", "ticket" => $full_path); 
		} else {
			$content_PDF = $ci->html2pdf->Output('', true);
			$result = array("rest_no" => "0", "ticket" => $content_PDF); 
		}
		
		return $result; // return as binary
		// return $ci->html2pdf->Output('', "F"); // save as file on server
	}

	public function hotel_email($parameter) {
		$ci =& get_instance();	
		$email 		= $parameter['email'];
		$pdf		= $this->hotel_pdf($parameter);
		if($pdf['rest_no'] == '0'){
			$path['pdf']	= $pdf['ticket'];
			
			/*edit subject dan content belum dilakukan*/
			$content		= "Terima kasih telah melakukan transaksi hotel di Pacific-Travel";
			$subject 		= "Invoice Hotel Pacific-Travel";
			
			$ci->load->library("send");
			$result 		= $ci->send->emailAttach($email, $subject, $content, $path, $parameter['sender_name'], $parameter['return_address']);		
		}
		else{
			$result = $pdf;
		}
		
		/*hapus file pdf di server belum dilakukan*/
		
		return $result;
	}
	
	/******** KERETA ********/
	private function get_kereta_data($book_id) {
		$ci =& get_instance();

		$data['dt_train'] 			= $ci->mgeneral->getWhere(array('dtt_id' => $book_id), 'dt_train');
		$data['dt_train_pax'] 		= $ci->mgeneral->getWhere(array('dtt_id' => $book_id), 'dt_train_pax');
		$data['dt_train_schedule'] 	= $ci->mgeneral->getWhere(array('dtt_id' => $book_id), 'dt_train_schedule');
		
		$partner 					= $ci->mgeneral->getWhere(array('partner_id' => $data['dt_train'][0]->partner_id), 'mstr_partner');
		if($partner[0]->skema_bisnis_id == '3' || $partner[0]->skema_bisnis_id == '4'){
			$partner_admin 			= $ci->mgeneral->getWhere(array('skema_bisnis_id' => $partner[0]->skema_bisnis_id, 'corporate_id' => $partner[0]->corporate_id, 'partner_level' => '1'), 'mstr_partner');
			$corporate_data 		= $ci->mgeneral->getWhere(array('corporate_id' => $partner[0]->corporate_id), 'mstr_corporate');

			$data['travel']			= array("business_name" 	=> $partner_admin[0]->business_name,
											"business_address"	=> $partner_admin[0]->business_address,
											"business_phone"	=> ($corporate_data[0]->phone_number) ? $corporate_data[0]->phone_number : $corporate_data[0]->mobile_number,
											"business_logo"		=> ($corporate_data[0]->logo) ? base_url()."uploads/corp_logo/".$corporate_data[0]->logo : "");
		}

		if($data['dt_train'][0]->dtt_status == "ticketed"){
			$data['rest_no'] = "0";
		}
		return $data;
	}
	
	public function kereta($parameter) {
		$ci =& get_instance();
		$book_id = $ci->converter->decode($parameter['book_id']);

		$data['kereta'] = $this->get_kereta_data($book_id);

		if($data['kereta']['rest_no'] == "0"){
			$html = $ci->load->view('ticket/kereta/html_ticket', $data, TRUE);
			$result = array("rest_no" => "0", "ticket" => $html); 
		}
		else{
			$result	= array("rest_no"	=> "717", "reason" => "status book_id anda adalah ".$data['kereta']['dt_flight'][0]->dtf_status.". sehingga tiket tidak dapat di cetak");
		}
		
		return $result;
	}

	public function kereta_invoice($parameter) {
		$ci =& get_instance();

		$html = $ci->load->view('ticket/kereta/html_invoice', $data, TRUE);
		return $html;
	}
	
	public function kereta_pdf($parameter) {
		$ci =& get_instance();
		$book_id = $ci->converter->decode($parameter['book_id']);

		$data['kereta'] = $this->get_kereta_data($book_id);
		
		if($data['kereta']['rest_no'] == "0"){
			$html = $ci->load->view('ticket/kereta/pdf_ticket', $data, TRUE); // place for load view ticket html here

			$ci->load->library('./html2pdf/html2pdf');
			$html2pdf = new HTML2PDF('P','A4','en',true,'UTF-8',array(0, 0, 0, 0));
			$ci->html2pdf->WriteHTML($html);
			
			if ($parameter['call'] == "email_ticket") {
				$dtp_id = $ci->converter->decode($parameter['book_id']);
				$file_name = "/kereta/e-ticket-".$dtp_id.".pdf";
				
				$full_path = $this->path_pdf . $file_name;
				ob_start();
				$ci->html2pdf->Output($full_path, "F");
				$result = array("rest_no" => "0", "ticket" => $full_path); 
			} else {
				$content_PDF = $ci->html2pdf->Output('', true);
				$result = array("rest_no" => "0", "ticket" => $content_PDF); 
			}
		}
		else{
			$result	= array("rest_no"	=> "717", "reason" => "status book_id anda adalah ".$data['flight']['dt_flight'][0]->dtf_status.". sehingga tiket tidak dapat di cetak");
		}
		
		return $result;
	}
	
	public function kereta_email($parameter) {
		$ci =& get_instance();	
		$email 		= $parameter['email'];
		$pdf		= $this->kereta_pdf($parameter);
		if($pdf['rest_no'] == '0'){
			$path['pdf']	= $pdf['ticket'];
			$book_id 		= $ci->converter->decode($parameter['book_id']);
			$dtp 			= $ci->mgeneral->getWhere(array("dtt_id" => $book_id), "dt_train");
			
			/*edit subject dan content belum dilakukan*/
			$content		= "Terima kasih telah melakukan transaksi tiket kereta di Pacific-Travel";
			$subject 		= "E-Ticket Kereta Pacific-Travel";
			
			$ci->load->library("send");
			$result 		= $ci->send->emailAttach($email, $subject, $content, $path, $parameter['sender_name'], $parameter['return_address']);		
		}
		else{
			$result = $pdf;
		}

		return $result;
	}
	
	/******** PPOB ********/
	private function get_ppob_data($dtp_id) {
		$ci =& get_instance();
		
		$ppob_data = $ci->mgeneral->getWhere(array("dtp_id" => $dtp_id), "dt_ppob");
		
		$param['pay_code'] = $ppob_data[0]->unique_id;
		$ci->load->library('ppob/kpo');
		$result = $ci->kpo->get_struk($param);
		$post_data				= $result['post_data'];
		unset($result['post_data']); #remove array yang tidak diperlukan
		$result_from_supplier 	= $result;
		#save to log PPOB
		$varLog	= array('action'	=> "get_struk",
						'paycode'	=> $ppob_data[0]->unique_id,
						'result'	=> json_encode($result));
		$ci->mgeneral->save($varLog,"log_ppob");
		
		return $result;
	}
	
	public function ppob($parameter) {
		$ci =& get_instance();
		$dtp_id = $ci->converter->decode($parameter['book_id']);
		$data = $this->get_ppob_data($dtp_id);

		if($data['error_no'] == '0'):
			$html = $data['result'];
			$html = str_replace('<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />', '', $html);
		else:
			$html = $data['error_msg'];
		endif;
		
		return $html;
	}

	public function ppob_pdf($parameter, $data = NULL) {
		$ci =& get_instance();
		
		if($data == NULL){
			$dtp_id = $ci->converter->decode($parameter['book_id']);
			$data = $this->get_ppob_data($dtp_id);
		}
		
		if($data['rest_no'] == '0'):
			$html = $data['result'];
		else:
			$html = $data['error_msg'];
		endif;
		
		$ci->load->library('./html2pdf/html2pdf');
		$html2pdf = new HTML2PDF('P','A4','en',true,'UTF-8',array(0, 0, 0, 0));
		$ci->html2pdf->WriteHTML($html);
		// return $ci->html2pdf->Output('', "F"); // save as file on server

		if ($parameter['function'] == "email") {
			$dtp_id = $ci->converter->decode($parameter['book_id']);
			$file_name = "/ppob/invoice-ppob-".$dtp_id.".pdf";
			
			$full_path = $this->path_pdf . $file_name;
			ob_start();
			$ci->html2pdf->Output($full_path, "F");
			return $full_path;
		} else {
			$content_PDF = $ci->html2pdf->Output('', true);
			return $result = array("rest_no" => "0", "ticket" => $content_PDF); 
		}
	}
	
	public function ppob_email($parameter) {
		$ci =& get_instance();	
		
		$dtp_id 	= $ci->converter->decode($parameter['book_id']);
		$data 		= $this->get_ppob_data($dtp_id);
		$email 		= $parameter['email'];

		if($data['error_no'] == '0'):
			$path['pdf']	= $this->ppob_pdf($parameter, $data);
			
			$dtp 			= $ci->mgeneral->getWhere(array("dtp_id" => $dtp_id), "dt_ppob");
			$ppob_produk 	= $ci->mgeneral->getWhere(array("ppob_produk_id" => $dtp[0]->ppob_produk_id), "mstr_ppob_produk");
			$content		= "Terima kasih telah melakukan transaksi " . ucfirst($ppob_produk[0]->ppob_produk_group) . " di Pacific-Travel";
			$subject 		= "Invoice produk " . ucfirst($ppob_produk[0]->ppob_produk_group) . " Pacific-Travel";
			
			$ci->load->library("send");
			$result 		= $ci->send->emailAttach($email, $subject, $content, $path, $parameter['sender_name'], $parameter['return_address']);
			
			/*hapus file pdf di server belum dilakukan*/
		else:
			$result = $data;
		endif;
		
		return $result;
	}
}

/* End of file ticket_library.php */
/* Location: ./application/libraries/ticket_library.php */
