<?php

class flight
{
	private $airline_one_code   = array("1", "2");
	function __construct()
    {
		$ci =& get_instance();
		$ci->load->library('airlines/citilink');
		$ci->load->library('airlines/garuda');
		$ci->load->model('mprice');
		$ci->load->library('validasi/validate_flight');
    }
	
	public function index($action,$parameter)
	{
		switch($action):
			default:
				$result = array('rest_no'=>"103",'reason'=>"Function tidak terdaftar");
			break;
			case 'ws_getairlines':
				$result = $this->get_airlines($parameter);
			break;
			case 'ws_getairport': 
				$result = $this->get_airport($parameter); 
			break;
			case "ws_search";
				$result	= $this->search($parameter);
			break;
			case "ws_getallclass";
                $result = $this->getallclass($parameter);
            break;
			case "ws_faredetail";
				$result = $this->detail($parameter);
			break;
			case "ws_book";
				$result = $this->book($parameter);
			break;
			case "ws_payment";
                $result = $this->payment($parameter);
			break;
			case "ws_cekpayment";
				$result = $this->cek_payment($parameter);
			break;
			case "ws_getrealprice";
				$result = $this->get_real_price($parameter);
			break;
			case "ws_getbalance";
				$result = $this->get_balance($parameter);
			break;
		endswitch;
		
		return $result;
	}
	
	private function get_airlines($param) {
		$this->ci =& get_instance();
		$data = $this->ci->mgeneral->getJoin('mstr_produk', 'mstr_subproduk', 'mstr_produk.produk_id = mstr_subproduk.produk_id', 'LEFT JOIN', array('mstr_subproduk.produk_id' => '1'));

		if ( ! empty($data)) {
			foreach ($data as $d) {
				$result_data[] = array(
					'airline_id' => $d->subproduk_id,
					'airline_code' => $d->subproduk_code,
					'airline_name' => $d->subproduk_name,
					'status' => $d->subproduk_status
				);
			}

			$result = array(
				'rest_no' => 0,
				'airlines' => $result_data
			);
		} else {
			$result = array(
				'rest_no' => 404,
				'reason' => 'no data available'
			);
		}

		return $result;
	}

	private function get_airport($param) {
		$this->ci =& get_instance();
		$data_airport = $this->ci->mgeneral->getLike(array('country' => $param['country']), 'mstr_airport','airport_code','ASC');

		if ( ! empty($data_airport)) {
			foreach ($data_airport as $da) {
				$result_data[] = array(
					'airport_code' => $da->airport_code,
					'airport_name' => $da->airport_name,
					'country' => strtoupper($da->country),
					'city' => $da->city
				);
			}

			$result = array(
				'rest_no' => 0,
				'airports' => $result_data
			);
		} else {
			$result = array(
				'rest_no' => 404,
				'reason' => 'no data available'
			);
		}

		return $result;
	}
	
	private function search($parameter)
	{
		$ci =& get_instance();
		#$valid	= $ci->validate_flight->search($parameter);
		
		#if($valid['error_no']=="0"):
			
			switch($parameter['airline_id']):

                case "1"; # CITILINK
                    $data=$ci->citilink->search("oneway",$parameter['dep'],$parameter['arr'],$parameter['date'],$parameter['date'],$parameter['adult'],$parameter['child'],$parameter['infant']);
                break;

                case "2"; # GARUDA
                    $data=$ci->garuda->search("oneway",$parameter['dep'],$parameter['arr'],$parameter['date'],$parameter['date'],$parameter['adult'],$parameter['child'],$parameter['infant']);
                break;

            endswitch;
            
			if(count($data['schedule']['depart'])=="0"):
				
				$error		= getErrorText("302");
				$result		= array('rest_no'		=> "302",
									'reason'		=> $error);
			
			else:
					
				//save session data
				$parameter2 = array_slice($parameter, 0, 2, true) + array("roundtrip" => "oneway") + array_slice($parameter, 2, count($parameter) - 1, true) ;
				unset($parameter2['client']);
				
				$varSession	= array('search_post'	=>json_encode($parameter2),
									'search_data'	=>json_encode($data));
				$session_id	= $ci->mgeneral->save($varSession,"log_session");
				
				$sesCode		= $ci->converter->encode($session_id);
				$varSession2	= array('session_code'	=> $sesCode);
				$ci->mgeneral->update(array('session_id'=>$session_id),$varSession2,"log_session");
					
				//hapus array format lama
				unset($data['search_info']['page_ref']);
				unset($data['search_info']['client']);
				unset($data['schedule']['depart_ref']);
				unset($data['schedule']['return_ref']);
							
				$result		= array('rest_no'			=> "0",
									'scReferenceId'		=> $ci->converter->encode($session_id),
									'schedule'			=> $data['schedule']['depart']
									);
				
			endif;
			
		/*else:
		
			$result = $valid;
			
		endif;*/
		return $result;
	}
	
	function getallclass($parameter){
        $ci =& get_instance();
        $session_id = $ci->converter->decode($parameter['scReferenceId']);
        $sesData    = $ci->mgeneral->getWhere(array('session_id' => $session_id), "log_session");

        if(count($sesData) != 0){
            foreach ($sesData as $sd) 
            {
                $search_post    = json_decode($sd->search_post,true);
                $search_result  = json_decode($sd->search_data,true);
            }

            switch($search_post['airline_id']):

                case "1"; # CITILINK
                    $data=$ci->citilink->search("oneway",$search_post['dep'],$search_post['arr'],$search_post['date'],$search_post['date'],$search_post['adult'],$search_post['child'],$search_post['infant']);
                break;

                case "2"; # GARUDA
                    $data=$ci->garuda->search("oneway",$search_post['dep'],$search_post['arr'],$search_post['date'],$search_post['date'],$search_post['adult'],$search_post['child'],$search_post['infant']);
                break;

            endswitch;
            
            if(count($data['schedule']['depart'])=="0"):

                $error  = getErrorText("302");
                $result = array('rest_no'        => "302",
                                    'reason'        => $error);

            else://save session data
                $parameter2 = array_slice($parameter, 0, 2, true) + array("roundtrip" => "oneway") + array_slice($parameter, 2, count($parameter) - 1, true) ;
                unset($parameter2['client']);

                $varSession = array('search_data'   =>json_encode($data));
                $ci->mgeneral->update(array('session_id'=>$session_id),$varSession,"log_session");

                //hapus array format lama
                unset($data['search_info']['page_ref']);
                unset($data['search_info']['client']);
                unset($data['schedule']['depart_ref']);
                unset($data['schedule']['return_ref']);

                //set tipe connecting flight depart
                $arr=0;

                    $cTime    = date("Y-m-d H:i:s");
                    $mTime    = date('Y-m-d H:i:s',strtotime($cTime . ' + 4 hours'));
                #print_r($data);
                foreach($data['schedule']['depart'] as $dscD)
                {
                    $dd    = $dscD['flights']['date']." ".$dscD['flights']['etd'].":00";
                    if(strtotime($dd) <= strtotime($mTime)): #membandingkan jadwal H-6 jam

                        unset($data['schedule']['depart'][$arr]); #hapus jadwal

                    else:

                        $tipe    = $dscD['flights_type'];
                        if($tipe=="connecting")
                        {
                            $clsUt    = $dscD['cabin_class'];
                            $clsCon    = $dscD['flights_connecting'][0]['cabin_class'];

                            $lisClassB=array();
                            $lisClassC=array();
                            for($cu=0;$cu<count($clsUt);$cu++)
                            {
                                $hrgTot    = $clsUt[$cu]['price']+$clsCon['0']['price']; //tambahkan harga connecting + class connnecting termurah
                                $lisClassB[]    = array('cabin_code'        => $clsUt[$cu]['cabin_code'],
                                                        'cabin_name'    => $clsUt[$cu]['cabin_name'],
                                                        'seat'            => $clsUt[$cu]['seat'],
                                                        'price'            => $hrgTot); //array class utama
                            #'value'            => $clsUt[$cu]['value']
                            }

                            $lisClassC[]    = array('cabin_name'    => $clsCon['0']['cabin_name'],
                                                    'seat'            => $clsCon['0']['seat'],
                                                    'price'            => $clsCon['0']['price']); //array class connecting
                            #'value'            => $clsCon['0']['value']
                            $data['schedule']['depart'][$arr]['cabin_class']    = $lisClassB;
                            $data['schedule']['depart'][$arr]['flights_connecting'][0]['cabin_class']    = $lisClassC;
                        }
                        else
                        {
                            $clsUt    = $dscD['cabin_class'];
                            for($cu=0;$cu<count($clsUt);$cu++)
                            {
                                $data['schedule']['depart'][$arr]['cabin_class'][$cu]['price'] = $data['schedule']['depart'][$arr]['cabin_class'][$cu]['price'];
                                unset($data['schedule']['depart'][$arr]['cabin_class'][$cu]['value']);
                            }
                        }
                    endif;
                    $arr++;
                }

                foreach($data['schedule']['depart'] as $nscD):
                    $newSCHd[]    = $nscD;
                endforeach;

                unset($data['schedule']['depart']);

                $data['schedule']['depart']    = $newSCHd;
                if(count($data['schedule']['depart']) == "0"){
                    $error      = getErrorText("302");
                    $result     = array('rest_no'        => "302",
                                        'reason'        => $error);

                    return $result;
                }


                $result        = array('rest_no'            => "0",
                                    'scReferenceId'        => $ci->converter->encode($session_id),
                                    'schedule'            => $data['schedule']['depart']
                                    );

            endif;
        }
        else{
            $error      = getErrorText("302");
            $result     = array('rest_no'        => "302",
                                'reason'        => $error);
        }

        return $result;
    }

	function detail($parameter)
	{
		$ci = & get_instance();
		if($parameter['scReferenceId_2']!=""):
			
			$result = $this->detail_join($parameter);
			
		else:
		
			#$valid = $ci->validate_flight->search_detail($parameter);
			
			#if ($valid['error_no'] == "0"):
				
				#echo "<pre>";
					
					$session_data = extract_session_search($parameter);
					$price1 = $this->price_search($session_data['detail_flight']['airline_id'], $session_data['post_to_detail'], $parameter['akses_kode'],$parameter['agent_id'],$parameter['client']['client_app'],$parameter);
					
					if ($price1['rest_no'] == "0") 
					{
						#set harga dewasa
						if ($session_data['search_post']['adult'] != 0 && $session_data['search_post']['adult'] != ""):
							$priceDetail['adult'] = $price1['adult_price'];
						endif;
						
						#set harga anak
						if ($session_data['search_post']['child'] != 0 && $session_data['search_post']['child'] != ""):
							$priceDetail['child'] = $price1['child_price'];
						endif;
						
						#set harga bayi
						if ($session_data['search_post']['infant'] != 0 && $session_data['search_post']['infant'] != ""): 
							$priceDetail['infant'] = $price1['infant_price'];
						endif;
						
						$result 	= array('rest_no' 			=> "0",
											'fareReferenceId' 	=> $parameter['scReferenceId'],
											'depart'            => array('schedule'         => $session_data['detail_flight'],
                                                                         'fares_detail'     => $priceDetail,
                                                                         'total_fares'      => $price1['total_fares'],
                                                                         'commission'       => $price1['commission'],
                                                                         'NTA'              => $price1['NTA']),
											'total_fares'		=> $price1['total_fares'],
											'commission'		=> $price1['commission'],
											'NTA'				=> $price1['NTA']);
						$result2 = $result;
						
						#add value class to session database		
						if ($session_data['post_to_detail']['price_con'] != "") 
						{
							$result2['depart']['connecting_flight'][0]['price'] = $session_data['post_to_detail']['price_con'];
						}
						$result2['depart']['commission']	= $price1['commission'];
						$result2['depart']['total_fares']	= $price1['total_fares'];
						$result2['depart']['value']	= array('value1' => $session_data['post_to_detail']['value']['value_1_a'],
															'value2' => $session_data['post_to_detail']['value']['value_1_b']);
						
					}
					else 
					{
						$error = getErrorText("503");
						$result['rest_no'] = "503";
						$result['reason'] = $error;
					}
				
				if ($result['rest_no'] == "0") 
				{
					#update session data
					$session_id		= $ci->converter->decode($parameter['scReferenceId']);
					$varSession 	= array('session_date' 	=> date('Y-m-d H:i:s'),
											'session_index'	=> "detail",
											'post_detail' 	=> json_encode($parameter),
											'data_detail' 	=> json_encode($result2));
					$ci->mgeneral->update(array('session_id' => $session_id), $varSession, "log_session");
				}
								
			#else:
				
				#$result = $valid;
				
			#endif;
		
		endif;
		
		return $result;
	}
	
	private function detail_join($parameter){
		$ci = & get_instance();
		
		$session_depart = array('scReferenceId'	=> $parameter['scReferenceId'],
								'fno'	=> $parameter['fno'],
								'class_id'	=> $parameter['class_id']);
		$session_data_depart = extract_session_search($session_depart);
		
		$session_return = array('scReferenceId'	=> $parameter['scReferenceId_2'],
								'fno'	=> $parameter['fno_2'],
								'class_id'	=> $parameter['class_id_2']);
		$session_data_return = extract_session_search($session_return);
		
		$post_search	= $session_data_depart['search_post'];
		$post_search['roundtrip']	= "return";
		$return_date	= $session_data_return['search_post']['date'];
		$return_airline	= $session_data_return['detail_flight']['airline_id'];
		$post_search	= array_slice($post_search, 0, 7, true) + array("return_airline" => $return_airline,"return_date" => $return_date) + array_slice($post_search, 7, count($post_search) - 1, true) ;

		$return_airline    = $session_data_return['detail_flight']['airline_id'];
        $depart_airline    = $session_data_depart['detail_flight']['airline_id'];
		
		/*if($depart_airline == $return_airline && in_array($depart_airline, $this->airline_one_code)){
            $post_to_detail     = array("depart"    => $session_data_depart['post_to_detail'],
                                        "return"    => $session_data_return['post_to_detail']);
            
            $price = $this->price_return($depart_airline, $post_to_detail);
            $session_data_depart['post_to_detail']['value'] = $price['value']['depart'];
            $session_data_return['post_to_detail']['value'] = $price['value']['return'];
            if($price['rest_no'] == "0"){
                $price1 = $price['depart'];
                $price1['rest_no'] = "0";
                $price2 = $price['return'];
                $price2['rest_no'] = "0";
            }
        } 
        else{*/
        	#exec curl multi for detail price return flight
			$postDp = $ci->converter->encode(json_encode($session_data_depart['post_to_detail']));
	        $url    = base_url()."search/search_detail/".$session_data_depart['detail_flight']['airline_id']."/".$parameter['api_key']."/".$parameter['client']['client_app']."/".$parameter['agent_id'];
	        $postRt = $ci->converter->encode(json_encode($session_data_return['post_to_detail']));
	        $url2    = base_url()."search/search_detail/".$session_data_return['detail_flight']['airline_id']."/".$parameter['api_key']."/".$parameter['client']['client_app']."/".$parameter['agent_id'];
	        
	          $ch_1 = curl_init($url);
	          $ch_2 = curl_init($url2);
	          curl_setopt($ch_1, CURLOPT_RETURNTRANSFER, true);
	          curl_setopt($ch_2, CURLOPT_RETURNTRANSFER, true);
	          curl_setopt($ch_1, CURLOPT_SSL_VERIFYPEER, false);
	          curl_setopt($ch_2, CURLOPT_SSL_VERIFYPEER, false);
	          curl_setopt($ch_1, CURLOPT_SSL_VERIFYHOST, false);
	          curl_setopt($ch_2, CURLOPT_SSL_VERIFYHOST, false);
	          curl_setopt($ch_1, CURLOPT_TIMEOUT, '300');
	          curl_setopt($ch_2, CURLOPT_TIMEOUT, '300');
	          curl_setopt($ch_1, CURLOPT_POST, true);
	          curl_setopt($ch_1, CURLOPT_POSTFIELDS,array("data"=>$postDp));
	          curl_setopt($ch_2, CURLOPT_POST, true);
	          curl_setopt($ch_2, CURLOPT_POSTFIELDS,array("data"=>$postRt));

	          // build the multi-curl handle, adding both $ch
	          $mh = curl_multi_init();
	          curl_multi_add_handle($mh, $ch_1);
	          curl_multi_add_handle($mh, $ch_2);

	          // execute all queries simultaneously, and continue when all are complete
	          $running = null;
	          do {
	            curl_multi_exec($mh, $running);
	          } while ($running);

	          // all of our requests are done, we can now access the results
	          $response_1 = curl_multi_getcontent($ch_1);
	          $response_2 = curl_multi_getcontent($ch_2);

	        $price1 = json_decode($response_1,true);
	        $price2 = json_decode($response_2,true);
			
			/*$price1 = $this->price_search($session_data_depart['detail_flight']['airline_id'], $session_data_depart['post_to_detail'], $parameter['akses_kode'],$parameter['agent_id']);
			$price2 = $this->price_search($session_data_return['detail_flight']['airline_id'], $session_data_return['post_to_detail'], $parameter['akses_kode'],$parameter['agent_id']);
			*/
        #}
		
		if ($price1['rest_no'] == "0" && $price2['rest_no'] == "0"):
		
			#set harga dewasa
			if ($session_data_depart['search_post']['adult'] != 0 && $session_data_depart['search_post']['adult'] != ""):
				$priceDepart['adult'] = $price1['adult_price'];
				$priceReturn['adult'] = $price2['adult_price'];
			endif;
			
			#set harga anak
			if ($session_data_depart['search_post']['child'] != 0 && $session_data_depart['search_post']['child'] != ""):
				$priceDepart['child'] = $price1['child_price'];
				$priceReturn['child'] = $price2['child_price'];
			endif;
			
			#set harga bayi
			if ($session_data_depart['search_post']['infant'] != 0 && $session_data_depart['search_post']['infant'] != ""): 
				$priceDepart['infant'] = $price1['infant_price'];
				$priceReturn['infant'] = $price2['infant_price'];
			endif;
			
			$result 	= array('rest_no' 		=> "0",
								'fareReferenceId' => $parameter['scReferenceId'],
								'depart'        => array('schedule'        => $session_data_depart['detail_flight'],
                                                         'fares_detail' => $priceDepart,
                                                         'total_fares'       => $price1['total_fares'],
                                                         'commission'        => $price1['commission'],
                                                         'NTA'               => $price1['NTA']),
                                'return'        => array('schedule'        => $session_data_return['detail_flight'],
                                                         'fares_detail' => $priceReturn,
                                                         'total_fares'       => $price2['total_fares'],
                                                         'commission'        => $price2['commission'],
                                                         'NTA'               => $price2['NTA']),
								'total_fares'	=> $price1['total_fares']+$price2['total_fares'],
								'commission'	=> $price1['commission']+$price2['commission'],
								'NTA'			=> $price1['NTA']+$price2['NTA']);
			
		else:
		
			$error = getErrorText("503");
			$result['rest_no'] = "503";
			$result['reason'] = $error;
		
		endif;
				
		#update to session data
		if ($result['rest_no'] == "0") 
		{	
			$result2 = $result;
			
			#add value class departure to session database
			if ($session_data_depart['post_to_detail']['price_con'] != "") 
			{
				$result2['depart']['connecting_flight'][0]['price'] = $session_data_depart['post_to_detail']['price_con'];
			}
			
			$result2['depart']['commission']	= $price1['commission'];
			$result2['depart']['total_fares']	= $price1['total_fares'];
			$result2['depart']['value']			= array('value1' => $session_data_depart['post_to_detail']['value']['value_1_a'],
														'value2' => $session_data_depart['post_to_detail']['value']['value_1_b']);
			
			#add value class retur  to session database
			if ($session_data_return['post_to_detail']['price_con'] != "") 
			{
				$result2['return']['connecting_flight'][0]['price'] = $session_data_return['post_to_detail']['price_con'];
			}
			$result2['return']['commission']	= $price2['commission'];
			$result2['return']['total_fares']	= $price2['total_fares'];
			$result2['return']['value']			= array('value1' => $session_data_return['post_to_detail']['value']['value_1_a'],
														'value2' => $session_data_return['post_to_detail']['value']['value_1_b']);
													
			#update session data
			$session_id		= $ci->converter->decode($parameter['scReferenceId']);
			$varSession 	= array('session_date' 	=> date('Y-m-d H:i:s'),
									'session_index'	=> "detail",
									'search_post'	=> json_encode($post_search),
									'post_detail' 	=> json_encode($parameter),
									'data_detail' 	=> json_encode($result2));
			$ci->mgeneral->update(array('session_id' => $session_id), $varSession, "log_session");
		}
		
		return $result;
	}
	
	public function price_search($airline, $post, $akses_code, $agent_id = NULL,$client_app) {
       
        $ci = & get_instance();
		$type = $post['type'];
		unset($post['type']);

        if ($post['value']['value_1_b'] != "") {
            $pengali = 2;
        } else {
            $pengali = 1;
        } //untuk iwjr + service fee jika tipe connecting

        #$valid = $ci->validate_flight->price_search($airline, $post);
		
        #if ($valid['error_no'] == "0"):
            switch ($airline):
                case "1";
                    $data = $ci->citilink->detail($post);
                    $data['komisi'] = ($post['adult'] * (($data['priceAD'] * 1.96) / 100)) + ($post['child'] * (($data['priceCH'] * 1.96) / 100));
                break;
                case "2";
                    $data = $ci->garuda->detail($post);
                break;

            endswitch;
            
            if ($data['error_no'] == "0"):
                if($source != "btrav"):

					if ($data['priceAD'] != "" && (int)$data['priceAD'] > 10000):
						$status = true;

						if($airline == "5"){
							$fno = json_decode($post['value']['value_1_a'], true);
							$fno = $fno['flight_no'];
							$fno = explode(',', $fno);
						}
	                    
						//set perhitungan detail harga ADULT
	                    $basicAD	= (int)$data['priceAD'];
	                    $taxAD		= number_format(($data['priceAD'] * 10 / 100), 0, ',', '');
	                    $iwjrAD		= 5000 * $pengali;
	                    $sFeeAD		= ($ci->mprice->getServiceFee($airline, "adult") * $pengali)+$data['service'];
	                    
						/*if($airline == "4"){
							$totServiceAD	= $taxAD+$iwjrAD+$sFeeAD;
							$totalAD		= ($basicAD + $taxAD + $iwjrAD + $sFeeAD);
						}else{
							$totServiceAD	= $taxAD+$iwjrAD+$sFeeAD;
							$totalAD		= ($basicAD + $taxAD + $iwjrAD + $sFeeAD)*$post['adult'];
						}*/
						$totServiceAD	= $taxAD+$iwjrAD+$sFeeAD;
						$totalAD		= ($basicAD + $taxAD + $iwjrAD + $sFeeAD)*$post['adult'];
						
	                    $prAD = array('sitter' => $post['adult'], 'basic_fare' => $basicAD,'taxes_fees' => $totServiceAD, 'adult_fare' => $totalAD);

	                    if($post['child'] != "0"):
	                    	if ($data['priceCH'] != ""):
		                        
								//set perhitungan detail harga CHILD
		                        $basicCH	= (int)$data['priceCH'];
		                        $taxCH		= number_format(($data['priceCH'] * 10 / 100), 0, ',', '');
		                        $iwjrCH		= 5000 * $pengali;
		                        $sFeeCH		= ($ci->mprice->getServiceFee($airline, "child") * $pengali)+$data['service'];
								
								/*if($airline == "4"){
									$totServiceCH	= $taxCH+$iwjrCH+$sFeeCH;
		                        	$totalCH		= ($basicCH + $taxCH + $iwjrCH + $sFeeCH);
								}else{
									$totServiceCH	= $taxCH+$iwjrCH+$sFeeCH;
		                        	$totalCH		= ($basicCH + $taxCH + $iwjrCH + $sFeeCH)*$post['child'];
								}*/
								$totServiceCH	= $taxCH+$iwjrCH+$sFeeCH;
	                        	$totalCH		= ($basicCH + $taxCH + $iwjrCH + $sFeeCH)*$post['child'];
								
		                        $prCH = array('sitter' => $post['child'], 'basic_fare' => $basicCH,'taxes_fees' => $totServiceCH,'child_fare' => $totalCH);
							else:
								$status = false;	
		                    endif;
	                    endif;

	                    if($post['infant'] != "0"):
	                    	if (($data['priceIn'] != "" || $airline == "14")):
						
		                        //set perhitungan detail harga INFANT
		                        $basicIN = (int)$data['priceIn'];
		                        $taxIN = number_format(($data['priceIn'] * 10 / 100), 0, ',', '');
		                        $iwjrIN = 5000 * $pengali;
		                        
								if ($airline == '11' || $airline == '1') {
		                            $taxIN = 0;
		                            $iwjrIN = 0;
		                        }
								
		                        $sFeeIN = $ci->mprice->getServiceFee($airline, "infant") * $pengali;
		                        if($type=='transit' && $airline == '12'){
									$sFeeIN = $sFeeIN+5000;
								}
								
								/*if($airline == "4"){
									$totServiceIN	= $taxIN+$iwjrIN+$sFeeIN;
		                        	$totalIN		= ($basicIN + $taxIN + $iwjrIN + $sFeeIN);
								}else{
									$totServiceIN	= $taxIN+$iwjrIN+$sFeeIN;
		                        	$totalIN		= ($basicIN + $taxIN + $iwjrIN + $sFeeIN) * $post['infant'];
								}*/
								$totServiceIN	= $taxIN+$iwjrIN+$sFeeIN;
	                        	$totalIN		= ($basicIN + $taxIN + $iwjrIN + $sFeeIN) * $post['infant'];
								
		                        $prIN = array('sitter' => $post['infant'], 'basic_fare' => $basicIN, 'taxes_fees' => $totServiceIN, 'infant_fare' => $totalIN);
		                    else:
								$status = false;	
							endif;
	                    endif;
						
						if($status == true):
							$ticketPrice = $totalAD + $totalCH + $totalIN;
							$data['komisi'] = (int)number_format($data['komisi'], 0, ',', '');
							
		                    $result = array('rest_no' => "0",
		                        			'schedule_det' => $data['sch_detail'],
											'adult_price' => $prAD);
											
							#price child
							if($data['priceCH'] != "" && $post['child'] != "0"):
								$result ['child_price'] = $prCH;  
							endif;
							
							#price infant
							if ($data['priceIn'] != "" && $post['infant'] != "0"):
								$result['infant_price'] = $prIN; 
							endif;
							
							
							#pricing skema
							$result['total_fares']	= $ticketPrice;				
							$result['commission']	= $data['komisi'];
							$result['NTA']			= $ticketPrice-$data['komisi'];
		                else:
		                	$error = getErrorText("302");
		                    $result['rest_no'] = "302";
		                    $result['reason'] = $error;
		                endif;
							
	                else:
					
	                    $error = getErrorText("302");
	                    $result['rest_no'] = "302";
	                    $result['reason'] = $error;
	                
					
					endif;
				
				else:

                    $result = array('rest_no' => "0",
                                    'schedule_det'  => $data['sch_detail'],
                                    'adult_price'   => $data['depart']['fares_detail']['adult']);

                    #price child
                    if($post['child'] != "0"):
                        $result ['child_price'] = $data['depart']['fares_detail']['child'];
                    endif;

                    #price infant
                    if ($post['infant'] != "0"):
                        $result['infant_price'] = $data['depart']['fares_detail']['infant'];
                    endif;


                    #pricing skema
                    $result['total_fares']    = $data['total_fares'];
                    $result['commission']    = $data['commission'];
                    $result['NTA']            = $data['NTA'];

                endif;
                
            else:
			
                $result = $data;
            
			endif;
			
        #else:
            #$result = $valid;
        #endif;
		
        return $result;
    }

    public function price_return($airline, $post){
        $ci = & get_instance();

        switch ($airline):
            case "1";
                $data = $ci->citilink->detail_return($post);
            break;
            case "2";
                $data = $ci->garuda->detail($post);
            break;
        endswitch;

        if ($data['rest_no'] == "0"):
                
            $depart['adult_price']  = $data['depart']['fares_detail']['adult'];
            $return['adult_price']  = $data['return']['fares_detail']['adult'];
            if($post['depart']['child'] != "0"):
                $depart['child_price'] = $data['depart']['fares_detail']['child'];
                $return['child_price'] = $data['return']['fares_detail']['child'];
            endif;
            if ($post['depart']['infant'] != "0"):
                $depart['infant_price'] = $data['depart']['fares_detail']['infant'];
                $return['infant_price'] = $data['return']['fares_detail']['infant'];
            endif;
            
            #pricing skema
            $depart['total_fares']    = (int)$data['depart']['total_fares'];
            $depart['commission']     = (int)$data['depart']['commission'];
            $depart['NTA']            = (int)($data['depart']['total_fares'] - $data['depart']['commission']);

            $return['total_fares']    = (int)$data['return']['total_fares'];
            $return['commission']     = (int)$data['return']['commission'];
            $return['NTA']            = (int)($data['return']['total_fares'] - $data['return']['commission']);

            $result = array("rest_no"   => '0',
                            "depart"    => $depart,
                            "return"    => $return,
                            "value"     => $data["value"]);
                
        else:

            $result = $data;

        endif;

        return $result;
    }
	
	private function book($parameter)
	{
		$ci = & get_instance();
		$session_data = extract_session_book($parameter);

		for($p=1;$p<=$session_data['tpax'];$p++)
		{
			$fname = $ci->converter->replace_name(preg_replace('!\s+!', ' ',$parameter['pax_first_name_'.$p]));
			$lname = $ci->converter->replace_name(preg_replace('!\s+!', ' ',$parameter['pax_last_name_'.$p]));
			
			$psgr[]		= array('psgr_type'		=>$parameter['pax_type_'.$p],
								'title'			=>strtolower($parameter['pax_title_'.$p]),
								'first_name'	=>$fname,
								'last_name'		=>$lname,
								'id_psgr'		=>$parameter['pax_id_no_'.$p],
								'birthdate'		=>$parameter['pax_birthdate_'.$p],
								'paspor'		=>$parameter['pax_pasport_'.$p],
								'expire_paspor'	=>$parameter['pax_expire_pasport_'.$p],
								'country_paspor'=>$parameter['pax_country_pasport_'.$p],
								'parent'		=> "1");
		}
		
		$parameter['psgr'] = $psgr;
		
		#$valid	= $ci->validate_flight->book($parameter);
		//	$valid['error_no'] = 0;
		#if ($valid['error_no'] == "0") 
		#{	
			$cust	= array('cust_name'		=> $ci->converter->replace_name($parameter['customer_name']),
							'cust_phone' 	=> $parameter['customer_phone'],
							'cust_email' 	=> $parameter['customer_email']);
			
			if($session_data['roundtrip'] == "oneway"):
				$bookData1 = $this->book_airlines($session_data['depart']['airline'], $session_data['depart'], $parameter['psgr'], $cust);
			/*elseif($session_data['roundtrip'] == "return" && $session_data['depart']['airline'] == $session_data['return']['airline'] && in_array($session_data['depart']['airline'], $this->airline_one_code)) :
                
                $bookData1   = $this->book_return($session_data['depart']['airline'], $session_data, $parameter['psgr'], $cust);
                $bookData2   = $bookData1;*/

            elseif($session_data['roundtrip'] == "return"):
				$arrD = array('airlines'    => $session_data['depart']['airline'],
                              'session'		=> json_encode($session_data['depart']),
                              'psgr'        => json_encode($parameter['psgr']),
                              'cust'        => json_encode($cust));	
        		$url1    = base_url()."search/book";
            	
        		$arrR = array('airlines'    => $session_data['return']['airline'],
                              'session'     => json_encode($session_data['return']),
                              'psgr'        => json_encode($parameter['psgr']),
                              'cust'        => json_encode($cust));

        		$url2    = base_url()."search/book";

                #exec curl multi for detail price return flight
                $url    = base_url()."search/book";

                  $ch_1 = curl_init($url1);
                  $ch_2 = curl_init($url2);
                  curl_setopt($ch_1, CURLOPT_RETURNTRANSFER, true);
                  curl_setopt($ch_2, CURLOPT_RETURNTRANSFER, true);
                  curl_setopt($ch_1, CURLOPT_SSL_VERIFYPEER, false);
                  curl_setopt($ch_2, CURLOPT_SSL_VERIFYPEER, false);
                  curl_setopt($ch_1, CURLOPT_SSL_VERIFYHOST, false);
                  curl_setopt($ch_2, CURLOPT_SSL_VERIFYHOST, false);
                  curl_setopt($ch_1, CURLOPT_TIMEOUT, '300');
                  curl_setopt($ch_2, CURLOPT_TIMEOUT, '300');
                  curl_setopt($ch_1, CURLOPT_POST, true);
                  curl_setopt($ch_2, CURLOPT_POST, true);
                  curl_setopt($ch_1, CURLOPT_POSTFIELDS,$arrD);
                  curl_setopt($ch_2, CURLOPT_POSTFIELDS,$arrR);

                  // build the multi-curl handle, adding both $ch
                  $mh = curl_multi_init();
                  curl_multi_add_handle($mh, $ch_1);
                  curl_multi_add_handle($mh, $ch_2);

                  // execute all queries simultaneously, and continue when all are complete
                  $running = null;
                  do {
                    curl_multi_exec($mh, $running);
                  } while ($running);

                  // all of our requests are done, we can now access the results
                  $response_1 = curl_multi_getcontent($ch_1);
                  $response_2 = curl_multi_getcontent($ch_2);

                $bookData1 = json_decode($response_1,true);
                $bookData2 = json_decode($response_2,true);
			
			endif;
			
			if($session_data['roundtrip'] == "oneway" && $bookData1['error_no']==0):
				
				#save booking data
				$book_id	= save_book_data($session_data['session_data'],$parameter,$bookData1);
				$result 	= build_book_result($book_id,$session_data,$bookData1,$psgr,"",$parameter['margin'],$cust);
				
			elseif($session_data['roundtrip'] == "return" && $bookData1['error_no']==0 && $bookData2['error_no']==0):
			
				#save booking data
				$book_id	= save_book_data($session_data['session_data'],$parameter,$bookData1,$bookData2);
				$result		= build_book_result($book_id,$session_data,$bookData1,$psgr,$bookData2,$parameter['margin'],$cust);
				
			else:
				
				$result = $bookData1;
				
			endif;
			
			#update session data
			$session_id		= $ci->converter->decode($parameter['session_id']);
			$varSession 	= array('session_date' 	=> date('Y-m-d H:i:s'),
									'session_index'	=> "book",
									'post_book' 	=> json_encode($parameter),
									'data_book' 	=> json_encode($result));
			$ci->mgeneral->update(array('session_id' => $session_id), $varSession, "log_session");
			
		 /*}
		 else 
		 {
			$result = $valid;
		 }*/
		
		return $result;
	}
	
	public function book_airlines($airline="", $flight=array(), $psgr=array(), $cust=array())
	{
		
		$ci = & get_instance();
		
		switch ($airline) {
			case "1";
				$data = $ci->citilink->book($flight, $psgr, $cust);
				#$data = array( 'error_no' => 0, 'book_code' => "JEDK9N", 'time_limit' => "2019-03-05 00:03:18", "status" => "CONFIRMED" );
				break;
			case "2";
				$data = $ci->garuda->book($flight, $psgr, $cust);
				break;
		}
		
		#$data = json_decode('{"repson_code":"0","book_code":"5OBMFM","time_limit":"'.date('Y-m-d').' '.(date('H')+1.).':'.date('i:s').'","basic_adult":710000,"basic_child":710000,"basic_infant":71000,"tax_adult":206000,"tax_child":206000,"tax_infant":12100,"komisi":104370,"invoice":"BTP-1016-000000941","status":"CONFIRMED"}', true);

        if (strlen($data['status']) <= 5) {
            $data = array('rest_no' 	=> "0",
						  'book_code'	=> "-",
						  'time_limit' 	=> date('Y-m-d H:i:s'),
						  'status' 		=> "failed");
        } else {
			
            $tlEdit = date('Y-m-d H:i:s', strtotime($data['time_limit'] . ' - 15 minutes'));
            $data = array('rest_no'         => "0",
                          'book_code'       => $data['book_code'],
                          'time_limit'      => $tlEdit,
                          'status'          => $data['status'],
                          'harga_tiket'     => $data['harga_tiket'],
                          'NTA'             => $data['NTA'],
                          'komisi'          => $data['komisi'],
                          'class_1'         => $data['class_1'],
                          'class_2'         => $data['class_2'],
                          'price_adult'     => $data['price_adult'],
                          'price_child'     => $data['price_child'],
                          'price_infant'    => $data['price_infant'],
                          'basic_adult'     => $data['basic_adult'],
                          'basic_child'     => $data['basic_child'],
                          'basic_infant'    => $data['basic_infant'],
                          'tax_adult'       => $data['tax_adult'],
                          'tax_child'       => $data['tax_child'],
                          'tax_infant'      => $data['tax_infant'],
                          'totalTax'        => $data['totalTax'],
                          'TaxIntperPax'    => $data['TaxIntperPax'],
                          'detail'          => $data['detail'],
                          "invoice"         => $data['invoice']);
			
        }
		return $data;
	}
	
	public function book_return($airline, $flight, $psgr, $cust)
    {
        $ci = & get_instance();
        
        switch ($airline) {
            case "1";
                $data = $ci->citilink->book_return($flight, $psgr, $cust);
                break;
            case "2";
                $data = $ci->btrav->book_return("ALT", $flight, $psgr, $cust);
                break;
        }
        #$data = json_decode('{"repson_code":"0","book_code":"5OBMFM","time_limit":"'.date('Y-m-d').' '.(date('H')+1.).':'.date('i:s').'","basic_adult":710000,"basic_child":710000,"basic_infant":71000,"tax_adult":206000,"tax_child":206000,"tax_infant":12100,"komisi":104370,"invoice":"BTP-1016-000000941","status":"CONFIRMED"}', true);

        if (strlen($data['status']) <= 5) {
            $data = array('rest_no'     => "0",
                          'book_code'    => "-",
                          'time_limit'     => date('Y-m-d H:i:s'),
                          'status'         => "failed");
        } else {

            $tlEdit = date('Y-m-d H:i:s', strtotime($data['time_limit'] . ' - 15 minutes'));
            $data = array('rest_no'         => "0",
                          'book_code'       => $data['book_code'],
                          'time_limit'      => $tlEdit,
                          'status'          => $data['status'],
                          'harga_tiket'     => $data['harga_tiket'],
                          'NTA'             => $data['NTA'],
                          'komisi'          => $data['komisi'],
                          'class_1'         => $data['class_1'],
                          'class_2'         => $data['class_2'],
                          'price_adult'     => $data['price_adult'],
                          'price_child'     => $data['price_child'],
                          'price_infant'    => $data['price_infant'],
                          'basic_adult'     => $data['basic_adult'],
                          'basic_child'     => $data['basic_child'],
                          'basic_infant'    => $data['basic_infant'],
                          'tax_adult'       => $data['tax_adult'],
                          'tax_child'       => $data['tax_child'],
                          'tax_infant'      => $data['tax_infant'],
                          'totalTax'        => $data['totalTax'],
                          'TaxIntperPax'    => $data['TaxIntperPax'],
                          'detail'          => $data['detail'],
                          "invoice"         => $data['invoice']);

        }

        return $data;
        
        /*
        ############# YANG MAU MERUBAH DATA INI SILAHKAN KONFIRMASI DULU ######################
        $str            = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $random_word    = str_shuffle($str);
        $bookCode        = substr($random_word,0,6);
        $TL             = date('Y-m-d')."  ".(date('H')+1.).":".date('i:s');

        $data    = array('rest_no'    => "0",
                        'book_code'    => $bookCode,
                        'time_limit'=> $TL,
                        'status'    => "CONFIRMED");   
        return $data; */
    }

	private function payment($parameter)
	{
		$ci = & get_instance();
		
		$bookid	 	= $ci->converter->decode($parameter['book_id']);
	
		$cekData 	= $ci->mgeneral->getWhere(array('dtf_id'=>$bookid),"dt_flight");
		$psgr 	 	= $ci->mgeneral->getWhere(array('dtf_id'=>$bookid),"dt_flight_pax");
		$schData    = $ci->mgeneral->getWhere(array('dtf_id'=>$bookid),"dt_flight_schedule");
		
		if(!empty($cekData) && $cekData[0]->dtf_status == "book"):
			
			$tipeFlight		= $cekData[0]->dtf_roundtrip;
			$roundtrip		= $tipeFlight;
			if($roundtrip == 'return' && strtotime($cekData[0]->depart_expired_date) > strtotime($cekData[0]->return_expired_date)):
                $cek_time_limit = $cekData[0]->return_expired_date;
            else:
                $cek_time_limit = $cekData[0]->depart_expired_date;
            endif;

            if(strtotime($cek_time_limit) > strtotime(date("Y-m-d H:i:s"))):
				$actionPayment['error_no'] = "0";
				if($actionPayment['error_no'] == "0"):
					
					#issue tiket disini
                    $fieldUpdate['dtf_issued_date'] = date('Y-m-d H:i:s');
                    $fieldUpdate['dtf_status']         = "issued";

                    $fieldUpdate['depart_book_status'] = "issued";
                    if ($roundtrip == "return"): $fieldUpdate['return_book_status'] = "issued"; endif;

                    #update data bookingan
                    $ci->mgeneral->update(array('dtf_id'=>$bookid),$fieldUpdate,"dt_flight");

					if($cekData[0]->dtf_roundtrip=="return" && $cekData[0]->depart_airlines_id == $cekData[0]->return_airlines_id && $cekData[0]->depart_book_code == $cekData[0]->return_book_code):
                        $this->issue_to_airline($cekData[0]->depart_airlines_id,$cekData[0]->depart_book_code, $cekData[0]->dtf_tiket_price, $cekData[0]->return_invoice);
                        $dataT1 = $this->tiketInfo($cekData[0]->depart_airlines_id, $cekData[0]->depart_book_code, $psgr);
                        $dataT2 = $dataT1;
                    else:
                        $dep_price = 0;
                        foreach($schData as $sd){
                            if($sd->sch_type == "depart" || $sd->sch_type == "depart_connecting"){
                                $dep_price = $dep_price + (($sd->sch_adult_price + $sd->sch_adult_taxes_fees) * $cekData[0]->dtf_adult) + (($sd->sch_child_price + $sd->sch_child_taxes_fees) * $cekData[0]->dtf_child) + (($sd->sch_infant_price + $sd->sch_infant_taxes_fees) * $cekData[0]->dtf_infant);
                            }
                        }
                        $this->issue_to_airline($cekData[0]->depart_airlines_id,$cekData[0]->depart_book_code, $dep_price, $cekData[0]->depart_invoice);
                        $dataT1 = $this->tiketInfo($cekData[0]->depart_airlines_id, $cekData[0]->depart_book_code, $psgr);


                        if($cekData[0]->dtf_roundtrip=="return"):
                            $ret_price = 0;
                            foreach($schData as $sd){
                                if($sd->sch_type == "return" || $sd->sch_type == "return_connecting"){
                                    $ret_price = $ret_price + (($sd->sch_adult_price + $sd->sch_adult_taxes_fees) * $cekData[0]->dtf_adult) + (($sd->sch_child_price + $sd->sch_child_taxes_fees) * $cekData[0]->dtf_child) + (($sd->sch_infant_price + $sd->sch_infant_taxes_fees) * $cekData[0]->dtf_infant);
                                }
                            }
                            $this->issue_to_airline($cekData[0]->return_airlines_id,$cekData[0]->return_book_code, $ret_price, $cekData[0]->return_invoice);
                            $dataT2 = $this->tiketInfo($cekData[0]->return_airlines_id, $cekData[0]->return_book_code, $psgr);
                        endif;
                    endif;
					
					#cek status apakah ticketed atau cuma issued
					if ($roundtrip == "oneway" && $dataT1['status'] == "TIKET OK"):
						$fieldUpdate['dtf_status'] = "ticketed";
					elseif ($roundtrip == "return"):
						if ($dataT1['status'] == "TIKET OK" && $dataT2['status'] == "TIKET OK"):
							$fieldUpdate['dtf_status'] = "ticketed";
						endif;
					endif;

                    #update data bookingan
					$ci->mgeneral->update(array('dtf_id'=>$bookid),$fieldUpdate,"dt_flight");
					
					#inser no tiket penumpang
					$countPsgr = 0;
					foreach ($psgr as $p) {
						$noP = "0";
						$fname = strtoupper($p->pax_first_name);
						$lname = strtoupper($p->pax_last_name);
						$fullname = $fname." ".$lname;
						foreach ($dataT1['pax'] as $ky => $px){
							if(str_replace(" ", "", $fullname) == str_replace(" ", "", strtoupper($px['flights_name']))){
								$noTicket = $px['flights_ticket'];
							}
						}
						
						if($p->passengers_type == 'adult' || $p->passengers_type == 'child'){
							$countPsgr++;
						}
						
						if ($roundtrip == "return") {
							foreach ($dataT2['pax'] as $ky2 => $px2){
								if(str_replace(" ", "", $fullname) == str_replace(" ", "", strtoupper($px2['flights_name']))){
									$noTicket2 = $px2['flights_ticket'];
								}
							}
							if($p->passengers_type == 'adult' || $p->passengers_type == 'child'){
								$countPsgr++;
							}
						}
				
						$psgrData = array('pax_depart_ticket_no' => $noTicket,'pax_return_ticket_no' => $noTicket2);
						$pId = $p->pax_id;
						$ci->mgeneral->update(array('pax_id' => $pId), $psgrData, "dt_flight_pax");
					}
					
					$result		= build_issue_result($bookid);
				
                else:

                    $result['rest_no'] = "702";
                    $result['reason'] = getErrorText("702");

                endif;

            else:

                $fieldUpdate['dtf_status'] = "ctl";
                $ci->mgeneral->update(array('dtf_id'=>$bookid),$fieldUpdate,"dt_flight");
                
                $result['rest_no'] = "702";
                $result['reason'] = getErrorText("702");

            endif;
		
		else:
		
            $result['rest_no'] = "701";
            $result['reason'] = getErrorText("701");
			
		endif;
		
		return $result;
	}
	
	private function cek_payment($parameter)
	{
		$ci =& get_instance();
		
		$bookid	 = $ci->converter->decode($parameter['book_id']);
	
		$cekData 	= $ci->mgeneral->getWhere(array('dtf_id'=>$bookid),"dt_flight");
		$schData    = $ci->mgeneral->getWhere(array('dtf_id'=>$bookid),"dt_flight_schedule");
		$psgr 	 	= $ci->mgeneral->getWhere(array('dtf_id'=>$bookid),"dt_flight_pax");
		$roundtrip	= $cekData[0]->dtf_roundtrip;
		
		if($cekData[0]->dtf_status == "issued" || $cekData[0]->dtf_status == "ticketed"):
			
			if($cekData[0]->dtf_roundtrip=="return" && $cekData[0]->depart_airlines_id == $cekData[0]->return_airlines_id && $cekData[0]->depart_book_code == $cekData[0]->return_book_code):
                $dataT1 = $this->tiketInfo2($cekData[0]->depart_airlines_id, $cekData[0]->depart_book_code, $psgr, $cekData[0]->depart_invoice);
                if($dataT1['status_airlines'] != 'book' && in_array($cekData[0]->depart_airlines_id, $this->btrav_airline)):
                    $dataT1['status'] = 'TIKET OK';
                endif;
                
                if ($dataT1['status'] != "TIKET OK"):
                    $this->issue_to_airline($cekData[0]->depart_airlines_id,$cekData[0]->depart_book_code, $cekData[0]->dtf_tiket_price, $cekData[0]->return_invoice);
                endif;

            else:

                $dataT1 = $this->tiketInfo2($cekData[0]->depart_airlines_id, $cekData[0]->depart_book_code, $psgr, $cekData[0]->depart_invoice);
                if($dataT1['status_airlines'] != 'book' && in_array($cekData[0]->depart_airlines_id, $this->btrav_airline)){
                    $dataT1['status'] = 'TIKET OK';
                }
                        
                if($cekData[0]->dtf_roundtrip=="return"){
                    $dataT2 = $this->tiketInfo2($cekData[0]->return_airlines_id, $cekData[0]->return_book_code, $psgr, $cekData[0]->return_invoice);    
                    if($dataT2['status_airlines'] != 'book' && in_array($cekData[0]->return_airlines_id, $this->btrav_airline)){
                        $dataT2['status'] = 'TIKET OK';
                    }
                }

                #cek status apakah ticketed atau cuma issued
                if ($roundtrip == "oneway" && $dataT1['status'] != "TIKET OK"):

                    $dep_price = 0;
                    foreach($schData as $sd){
                        if($sd->sch_type == "depart" || $sd->sch_type == "depart_connecting"){
                            $dep_price = $dep_price + (($sd->sch_adult_price + $sd->sch_adult_taxes_fees) * $cekData[0]->dtf_adult) + (($sd->sch_child_price + $sd->sch_child_taxes_fees) * $cekData[0]->dtf_child) + (($sd->sch_infant_price + $sd->sch_infant_taxes_fees) * $cekData[0]->dtf_infant);
                        }
                    }
                    $this->issue_to_airline($cekData[0]->depart_airlines_id,$cekData[0]->depart_book_code, $dep_price, $cekData[0]->depart_invoice);

                elseif($roundtrip == "return"):

                    if ($dataT1['status'] != "TIKET OK"):
                        $dep_price = 0;
                        foreach($schData as $sd){
                            if($sd->sch_type == "depart" || $sd->sch_type == "depart_connecting"){
                                $dep_price = $dep_price + (($sd->sch_adult_price + $sd->sch_adult_taxes_fees) * $cekData[0]->dtf_adult) + (($sd->sch_child_price + $sd->sch_child_taxes_fees) * $cekData[0]->dtf_child) + (($sd->sch_infant_price + $sd->sch_infant_taxes_fees) * $cekData[0]->dtf_infant);
                            }
                        }
                        $this->issue_to_airline($cekData[0]->depart_airlines_id,$cekData[0]->depart_book_code, $dep_price, $cekData[0]->depart_invoice);
                    endif;

                    if($dataT2['status'] != "TIKET OK"):
                        $ret_price = 0;
                        foreach($schData as $sd){
                            if($sd->sch_type == "return" || $sd->sch_type == "return_connecting"){
                                $ret_price = $ret_price + (($sd->sch_adult_price + $sd->sch_adult_taxes_fees) * $cekData[0]->dtf_adult) + (($sd->sch_child_price + $sd->sch_child_taxes_fees) * $cekData[0]->dtf_child) + (($sd->sch_infant_price + $sd->sch_infant_taxes_fees) * $cekData[0]->dtf_infant);
                            }
                        }
                        $this->issue_to_airline($cekData[0]->return_airlines_id,$cekData[0]->return_book_code, $ret_price, $cekData[0]->return_invoice);
                    endif;

                endif;
            endif;
			
			$this->update_tiket_info($cekData,$psgr,$bookid);
			$result	= build_issue_result($bookid);
			
		else:
            
            $result     = array("rest_no"   => "717",
                                "reason"    => "Maaf status transaksi anda adalah ". strtoupper($cekData[0]->dtf_status));
        
        endif;
		
		return $result;
	}
	
	private function update_tiket_info($cekData,$psgr,$bookid)
	{
		$ci =& get_instance();
		$roundtrip	= $cekData[0]->dtf_roundtrip;
		
		$dataT1 = $this->tiketInfo2($cekData[0]->depart_airlines_id, $cekData[0]->depart_book_code, $psgr, $cekData[0]->depart_invoice);

		if($cekData[0]->dtf_roundtrip=="return"){
			$dataT2 = $this->tiketInfo2($cekData[0]->return_airlines_id, $cekData[0]->return_book_code, $psgr, $cekData[0]->return_invoice);
		}
		
			#update status bookingan
			$fieldUpdate['dtf_issued_date'] = date('Y-m-d H:i:s');
			$fieldUpdate['dtf_status'] 		= "issued";
	
			$fieldUpdate['depart_book_status'] = "issued";
			if ($roundtrip == "return"): $fieldUpdate['return_book_status'] = "issued"; endif;
			
			#cek status apakah ticketed atau cuma issued
			if ($roundtrip == "oneway" && $dataT1['status'] == "TIKET OK"):
				$fieldUpdate['dtf_status'] = "ticketed";
			elseif ($roundtrip == "return"):
				if ($dataT1['status'] == "TIKET OK" && $dataT2['status'] == "TIKET OK"):
					$fieldUpdate['dtf_status'] = "ticketed";
				endif;
			endif;
	
			#update data bookingan
			$ci->mgeneral->update(array('dtf_id'=>$bookid),$fieldUpdate,"dt_flight");
			
			#inser no tiket penumpang
			$countPsgr = 0;
			foreach ($psgr as $p) {
				$noP = "0";
				$fname = strtoupper($p->pax_first_name);
				$lname = strtoupper($p->pax_last_name);
				$fullname = $fname." ".$lname;
				foreach ($dataT1['pax'] as $ky => $px){
					if(str_replace(" ", "", $fullname) == str_replace(" ", "", strtoupper($px['flights_name']))){
						$noTicket = $px['flights_ticket'];
					}
				}
				
				if($p->passengers_type == 'adult' || $p->passengers_type == 'child'){
					$countPsgr++;
				}
				
				if ($roundtrip == "return") {
					foreach ($dataT2['pax'] as $ky2 => $px2){
						if(str_replace(" ", "", $fullname) == str_replace(" ", "", strtoupper($px2['flights_name']))){
							$noTicket2 = $px2['flights_ticket'];
						}
					}
					if($p->passengers_type == 'adult' || $p->passengers_type == 'child'){
						$countPsgr++;
					}
				}
		
				$psgrData = array('pax_depart_ticket_no' => $noTicket,'pax_return_ticket_no' => $noTicket2);
				$pId = $p->pax_id;
				$ci->mgeneral->update(array('pax_id' => $pId), $psgrData, "dt_flight_pax");
			}
		
		return "OK";
	}
	
	private function issue_to_airline($airline, $bookcode, $ticket_price = 0, $invoice = NULL) {
   		  
		  $ci =& get_instance();
    	  
          switch($airline)
          {
			case "1";
				$dataT	= $ci->citilink->issue($bookcode);
			break;
			case "2";
				$dataT    = $ci->garuda->issue($bookcode);
			break;
          }
		  
		  if($dataT['rest_no']==0):
		  
		  	if($dataT['deposit']!="" && is_numeric($dataT['deposit'])):
		  		$varData	= array("deposit" => $dataT['deposit']);
				$ci->mgeneral->update(array('subproduk_id'=>$airline), $varData, 'mstr_subproduk');
			endif;
			
		  endif;
		  
    }
	
	private function tiketInfo($airline, $bookcode, $psgr) {
      
	    $ci = & get_instance();
        switch ($airline) {
            case "1";
                $dataT = $ci->citilink->getTicketInfo($bookcode);
                $statT = "Konfirm";
                break;
            case "2";
                $dataT = $ci->garuda->getTicketInfo($bookcode);
                $statT = "TKT";
                break;
        }

        //====== CEK STATUS TIKET ==========
        if (strtoupper($statT) == strtoupper($dataT['status'])) {
            $result['rest_no'] = '0';
            $statTiket = "TIKET OK";
        } elseif ($dataT['rest_no'] != '0') {
            $result['rest_no'] = $dataT['rest_no'];
            $statTiket = $dataT['error_msg'];
        } elseif (empty($dataT['pax'])) {
            $result['rest_no'] = '2';
            $statTiket = '#Data Penumpang tidak ditemukan';
        } else {
            $result['rest_no'] = '3';
            $statTiket = '#Ada Perubahan Status dari Maskapai';
        }

        //======== potong data penumpang berdasarkan airline ==============================
        $result['status'] = $statTiket;
        $result['status_airlines'] = $dataT['status'];
        $dtPsgr = $dataT['pax'];
        if(!in_array($airline, $this->btrav_airline)){
	        switch ($airline) {
	            default;
	                $result['pax'] = $dtPsgr;
	                break;
	            case "2";
	                /*
	                foreach ($dtPsgr as $dtP) {
	                    $psps[] = array('flights_name' => $dtP['flights_name'],
	                        'flights_ticket' => $dtP['flights_ticket']);
	                }
	                */

	                foreach ($psgr as $k => $p) {
						$psps[] = array('flights_name' => $p->pax_first_name . ' ' . $p->pax_last_name,
	                        'flights_ticket' => $dtPsgr[$k]['flights_ticket']);
	                }
	                $result['pax'] = $psps;

	                break;
	            case "3";
	            case "6";
	                //get format nama psgr
	                foreach ($psgr as $p) {
						if(trim($p->pax_last_name) != NULL && trim($p->pax_last_name != '')){
							$pnama = strtoupper(str_replace(" ", "", $p->pax_last_name)) . "/" . strtoupper(str_replace(" ", "", $p->pax_first_name)) . strtoupper($p->pax_title);
						}
						else{
							$pnama = strtoupper(str_replace(" ", "", $p->pax_first_name)) . "/" . strtoupper(str_replace(" ", "", $p->pax_first_name)) . strtoupper($p->pax_title);
						}

	                    foreach ($dtPsgr as $dtP) {
	                        if ($dtP['flights_name'] == $pnama) {
	                            $psgr['ticket_no'] = trim($dtP['flights_ticket']);
	                        }
	                    }
						
	                    $psps[] = array('flights_name' => $p->pax_first_name . ' ' . $p->pax_last_name,
	                        'flights_ticket' => $psgr['ticket_no']);
	                }
	                $result['pax'] = $psps;
	                break;
	            case "4";
	                foreach ($psgr as $p) {
	                    $arnama[] = strtolower(str_replace(" ", "_", $p->pax_first_name)) . "_" . strtolower(str_replace(" ", "_", $p->pax_last_name));
	                }
	                $ar = 0;
	                foreach ($dtPsgr as $dtP) {
	                    $psps[] = array('flights_name' => strtoupper(str_replace("_", " ", $arnama[$ar])),
	                        'flights_ticket' => $dtP['flights_ticket']);
	                    $ar++;
	                }
	                $result['pax'] = $psps;
	                break;
	        }
		}
        else{
            $countPsgr = 0;
            foreach ($psgr as $idx => $p) {
                $noP = "0";
                $fname = strtoupper(str_replace(' ', "", $p->pax_first_name));
                $lname = strtoupper(str_replace(' ', "", $p->pax_last_name));
                $fullname = $fname." ".$lname;
                
                foreach ($dtPsgr as $ky => $px){
                    if($fname == strtoupper(str_replace(' ', "", $px['first_name'])) && $lname == strtoupper(str_replace(' ', "", $px['last_name']))){
                        $psps[] = array('flights_name'      => $fullname,
                                        'flights_ticket'    => $px['depart_ticket_no']);
                    }
                }
                $result['pax'] = $psps;
            }
        }
        return $result;
    }

    private function tiketInfo2($airline, $bookcode, $psgr, $invoice = NULL) {
      
	    $ci = & get_instance();
        switch ($airline) {
         
            case "1";
                $dataT = $ci->citilink->getTicketInfo($bookcode);
                $statT = "Konfirm";
                break;
            case "2";
                $dataT = $ci->garuda->getTicketInfo($bookcode);
                $statT = "TKT";
                break;
        }

        //====== CEK STATUS TIKET ==========
        if (strtoupper($statT) == strtoupper($dataT['status'])) {
            $result['rest_no'] = '0';
            $statTiket = "TIKET OK";
        } elseif ($dataT['rest_no'] != '0') {
            $result['rest_no'] = $dataT['rest_no'];
            $statTiket = $dataT['error_msg'];
        } elseif (empty($dataT['pax'])) {
            $result['rest_no'] = '2';
            $statTiket = '#Data Penumpang tidak ditemukan';
        } else {
            $result['rest_no'] = '3';
            $statTiket = '#Ada Perubahan Status dari Maskapai';
        }

        //======== potong data penumpang berdasarkan airline ==============================
        $result['status'] = $statTiket;
        $result['status_airlines'] = $dataT['status'];
        $dtPsgr = $dataT['pax'];
        if(!in_array($airline, $this->btrav_airline)){
	        switch ($airline) {
	            default;
	                $result['pax'] = $dtPsgr;
	                break;
	            case "2";
	                /*
	                foreach ($dtPsgr as $dtP) {
	                    $psps[] = array('flights_name' => $dtP['flights_name'],
	                        'flights_ticket' => $dtP['flights_ticket']);
	                }
	                */

	                foreach ($psgr as $k => $p) {
						$psps[] = array('flights_name' => $p->pax_first_name . ' ' . $p->pax_last_name,
	                        'flights_ticket' => $dtPsgr[$k]['flights_ticket']);
	                }
	                $result['pax'] = $psps;
	                break;
	            case "3";
	            case "6";
	                //get format nama psgr
	                foreach ($psgr as $p) {
						if(trim($p->pax_last_name) != NULL && trim($p->pax_last_name != '')){
							$pnama = strtoupper(str_replace(" ", "", $p->pax_last_name)) . "/" . strtoupper(str_replace(" ", "", $p->pax_first_name)) . strtoupper($p->pax_title);
						}
						else{
							$pnama = strtoupper(str_replace(" ", "", $p->pax_first_name)) . "/" . strtoupper(str_replace(" ", "", $p->pax_first_name)) . strtoupper($p->pax_title);
						}

	                    foreach ($dtPsgr as $dtP) {
	                        if ($dtP['flights_name'] == $pnama) {
	                            $psgr['ticket_no'] = trim($dtP['flights_ticket']);
	                        }
	                    }
						
	                    $psps[] = array('flights_name' => $p->pax_first_name . ' ' . $p->pax_last_name,
	                        'flights_ticket' => $psgr['ticket_no']);
	                }
	                $result['pax'] = $psps;
	                break;
	            case "4";
	                foreach ($psgr as $p) {
	                    $arnama[] = strtolower(str_replace(" ", "_", $p->pax_first_name)) . "_" . strtolower(str_replace(" ", "_", $p->pax_last_name));
	                }
	                $ar = 0;
	                foreach ($dtPsgr as $dtP) {
	                    $psps[] = array('flights_name' => strtoupper(str_replace("_", " ", $arnama[$ar])),
	                        'flights_ticket' => $dtP['flights_ticket']);
	                    $ar++;
	                }
	                $result['pax'] = $psps;
	                break;
	        }
		}
        else{
            $countPsgr = 0;
            foreach ($psgr as $idx => $p) {
                $noP = "0";
                $fname = strtoupper(str_replace(' ', "", $p->pax_first_name));
                $lname = strtoupper(str_replace(' ', "", $p->pax_last_name));
                $fullname = $fname." ".$lname;
                
                foreach ($dtPsgr as $ky => $px){
                    if($fname == strtoupper(str_replace(' ', "", $px['first_name'])) && $lname == strtoupper(str_replace(' ', "", $px['last_name']))){
                        $psps[] = array('flights_name'      => $fullname,
                                        'flights_ticket'    => $px['depart_ticket_no']);
                    }
                }
                $result['pax'] = $psps;
            }
        }
        return $result;
    }
	
	function get_real_price($params){
		$ci = & get_instance();
		$airline = array("3", "5");
		$id = $ci->converter->decode($params['scReferenceId']);
		$session = $ci->mgeneral->getWhere(array('session_id' => $id), "log_session");

		if(count($session)>0){
			$search_post = json_decode($session[0]->search_post, true);
			$search_data = json_decode($session[0]->search_data, true);
			foreach($search_data['schedule']['depart'] as $dep){
				if($dep['schedule_id'] == $params['schedule_id']){
					$schedule = $dep;break;
				}
			}
			
			if(!empty($schedule)){
				switch($search_post['airline_id']):
					
					default:
						$result = array("rest_no" => "302", "reason" => "airline_id ".$search_post['airline_id']." tidak dapat melakukan proses ini");
					break;
					
					case "2"; # GARUDA
						$result = $ci->garuda->get_real_price($search_post, $schedule);
					break;
							
				endswitch;
			}
			else{
				$result = array("rest_no" => "302", "reason" => "schedule_id tidak ditemukan");
			}
		}
		else{
			$result = array("rest_no" => "302", "reason" => "session tidak ditemukan");
		}
		
		return $result;
	}
	
	function get_balance($params){
		$ci = & get_instance();
		
		switch($params['airline_id']):
			
			case "1": $result = $ci->citilink->getDeposit(); break;
			case "2": $result = $ci->garuda->get_balance(); break;
			
		endswitch;
		
		if($result['rest_no']==0):
			$varUpdate = array('deposit'=>$result['balance']);
			$ci->mgeneral->update(array('subproduk_id'=>$params['airline_id']),$varUpdate,"mstr_subproduk");
		endif;
		
		return $result;
	}
}

?>
