<?php

class wshotel
{
    
    function __construct()
    {
        $ci =& get_instance();
        $ci->load->library('curl');

        set_time_limit(700);
        ini_set('max_execution_time', 0); // 0 = NOLIMIT
    }
    
	public function index($action,$parameter)
    {
		switch($action):
			case 'ws_srkeyword':	$result = $this->keyword($parameter); 	break;
            case 'ws_findhotel':	$result = $this->search($parameter); 	break;
            case "ws_nexthotel";	$result = $this->page($parameter); 	break;
            case "ws_detailhotel";	$result = $this->detail($parameter);	break;
            case "ws_selroom";		$result = $this->selroom($parameter);	break;
            case "ws_payment";		$result = $this->call_hotel($action,$parameter);	break;
            case "ws_bookinfo";		$result = $this->call_hotel($action,$parameter);	break;
            default:
                $result = array('rest_no'=>"103",'reason'=>"Function tidak terdaftar");
            break;
        endswitch;

        return $result;
    }
	
	private function call_hotel($action,$param)
	{
		$ci =& get_instance();
        foreach ($param as $key => $value) {
            $post_items[] = $key . "=" . $value;
        }
        $post_string = implode("&", $post_items);

        $result = json_decode($ci->curl->app($post_string),true);
        return $result;
	}

    function keyword($param)
    {
        
        $post_data['akses_kode']    = "TESTPARTNERSHIP";
        $post_data['app']           = "hotel_v5";
        $post_data['action']        = "city_hotel";
        $post_data['city']          = $param['word'];

        foreach ($post_data as $key => $value) {
            $post_items[] = $key . "=" . $value;
        }
        $post_string = implode("&", $post_items);
        
        $result = json_decode($this->post($post_string),true);
        
        if($result['error_no'] == 0):
            $respon['rest_no'] = $result['error_no'];
            $respon['data'] = $result['data'];
        else:
            $respon['rest_no'] = $result['error_no'];
            $respon['reason'] = $result['error_msg'];
        endif;

        return $respon;
    }

    function search($param)
    {
        
        $post_data['akses_kode']    = "TESTPARTNERSHIP";
        $post_data['app']           = "hotel_v5";
        $post_data['action']        = "hotel_search";
        $post_data['city']          = $param['keyword'];
        $post_data['ci']            = $param['check_in'];
        $post_data['co']            = $param['check_out'];
        $post_data['room']          = $param['room'];
        $post_data['adult']         = $param['guest'];
        $post_data['child']         = "0";
        $post_data['hotel_name']    = "";

        foreach ($post_data as $key => $value) {
            $post_items[] = $key . "=" . $value;
        }
        $post_string = implode("&", $post_items);
        
        $result = json_decode($this->post($post_string),true);
        #$result = json_decode($this->result_hotel(), true);
        
        if($result['error_no'] == 0):
            $respon['rest_no'] = $result['error_no'];
            unset($result['error_no']);
            $respon['data'] = $result;
        else:
            $respon['rest_no'] = $result['error_no'];
            $respon['reason'] = $result['error_msg'];
        endif;

        return $respon;
    }

    function page($param)
    {
        
        $post_data['akses_kode']    = "TESTPARTNERSHIP";
        $post_data['app']           = "hotel_v5";
        $post_data['action']        = "hotel_search";
        $post_data['page']          = $param['page'];
        $post_data['session_id']    = $param['findReferenceId'];

        foreach ($post_data as $key => $value) {
            $post_items[] = $key . "=" . $value;
        }
        $post_string = implode("&", $post_items);
        
        $result = json_decode($this->post($post_string),true);
        
        if($result['error_no'] == 0):
            $respon['rest_no'] = $result['error_no'];
            unset($result['error_no']);
            $respon['data'] = $result;
        else:
            $respon['rest_no'] = $result['error_no'];
            $respon['reason'] = $result['error_msg'];
        endif;

        return $respon;
    }

    function detail($param)
    {
        
        $post_data['akses_kode']    = "TESTPARTNERSHIP";
        $post_data['app']           = "hotel_v5";
        $post_data['action']        = "hotel_detail";
        $post_data['session_id']    = $param['findReferenceId'];
        $post_data['hotelId']       = $param['hotelId'];

        foreach ($post_data as $key => $value) {
            $post_items[] = $key . "=" . $value;
        }
        $post_string = implode("&", $post_items);
        
        $result = json_decode($this->post($post_string),true);
        
        if($result['error_no'] == 0):
            $respon['rest_no'] = $result['error_no'];
            unset($result['error_no']);
            $respon['data'] = $result;
        else:
            $respon['rest_no'] = $result['error_no'];
            $respon['reason'] = $result['error_msg'];
        endif;

        return $respon;
    }

    function selroom($param)
    {
        
        $post_data['akses_kode']    = "TESTPARTNERSHIP";
        $post_data['app']           = "hotel_v5";
        $post_data['action']        = "select_room";
        $post_data['session_id']    = $param['detReferenceId'];
        $post_data['room_code']     = $param['room_code'];
        $post_data['number_of_room'] = "1";

        foreach ($post_data as $key => $value) {
            $post_items[] = $key . "=" . $value;
        }
        $post_string = implode("&", $post_items);
        
        $result = json_decode($this->post($post_string),true);
        
        if($result['error_no'] == 0):
            $respon['rest_no'] = $result['error_no'];
            unset($result['error_no']);
            $respon['data'] = $result;
        else:
            $respon['rest_no'] = $result['error_no'];
            $respon['reason'] = $result['error_msg'];
        endif;

        return $respon;
    }

    private function post($data="") {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://agent.aeroticket.com/service/v2");
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_USERAGENT, "AT2-JSN");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, '100');
        curl_setopt($ch, CURLOPT_TIMEOUT, 700);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);

        if ($result === false) {
            return curl_error($ch);
        } else {
            return $result;
        }

        curl_close($ch);
    }

    function result_hotel(){
        $data = '{
  "error_no": "0",
  "session_id": "tuCwUs5pk1g0sEzS2yIk9TLvsjq1_ePVISGBUd68q_8",
  "search_info": {
    "akses_kode": "TESTPARTNERSHIP",
    "app": "hotel_v5",
    "action": "hotel_search",
    "city": "Jakarta",
    "ci": "2019-03-21",
    "co": "2019-03-23",
    "room": "1",
    "adult": "1",
    "child": "0",
    "hotel_name": "",
    "page_ref": "8_6K7MuiBxBiYQofS9swWdKg-Hs9bPbBFSe88YleyfYHgNI8s5_PPeDmGaTS0SboIcMB_ESDMATo_wBAliJCIeR_rF8hye1tbXAinJTkmF_YFaQ7vDdFc9kgRXZQE4EX"
  },
  "result_data": [
    {
      "hotelId": 4489,
      "source": "KlikHotel",
      "structureType": "Guest House",
      "csm": null,
      "name": "Pesona Guest House Jakarta",
      "cityName": "Jakarta Selatan",
      "address": "Jl. MPR III Dalam, Nomor 3, Cilandak Barat, Jakarta Selatan",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 601765,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/4489/7-496.jpg",
      "det": null,
      "shortDescription": "",
      "room_info": "Non-Smoking, Balcony / Terrace, Wardrobe, AC, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Hairdryer, Ironing, Mini-Bar, Refrigerator, DVD Player, Working Desk, Bath-Tub, Shower Bathroom, Bath Amenities, Hot Water Bath, City View",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Laundry",
        "Deposit Box",
        "Non-smoking Room",
        "Cold Pool",
        "Public-area Wi-Fi",
        "Car Parking",
        "Balcony",
        "Cable TV",
        "Kitchen",
        "Airport Transfer"
      ],
      "price_info": {
        "basic_price_per_night": 601764,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 1294118,
        "basic_price": 601764,
        "fee_and_tax": 0,
        "total_price": 1294118,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.8058868,
        "latitude": -6.2827413,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 0,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": null,
        "reviewPercentage": null,
        "cleanlinessRating": null,
        "serviceAndStaffRating": null,
        "roomComfortRating": null,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": null,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 601764,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 601764,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 2296,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Swiss-Belhotel Airport Jakarta",
      "cityName": "Tangerang",
      "address": "Airport Hub. Jl. Husein Sastranegara Kav. 1, Benda, Tangerang",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 1023000,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/2296/1-347.jpg",
      "det": null,
      "shortDescription": "Swiss-Belhotel Airport adalah hotel internasional berbintang empat di dekat Bandara Internasional Soekarno-Hatta, Jakarta. Terletak hanya beberapa menit dari bandara, hotel ini juga menyediakan akses mudah ke pusat bisnis melalui jalan tol.\r\rBerdekatan dengan mal yang terhubung dengan bandara, hotel ini menawarkan berbagai fasilitas yang mencakup hiburan serta pilihan makanan dan minuman, sehingga akomodasi yang ditawarkan sangat nyaman untuk pengusaha atau wisatawan yang singgah.",
      "room_info": "Non-Smoking, AC, Telephone, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Hairdryer, Ironing, Kettle, Mini-Bar, Refrigerator, Working Desk, Shower Bathroom, Bath Amenities, Slippers / sandals, City View, Swimming Pool View",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Bar/Lounge",
        "Laundry",
        "Fitness Centre",
        "Lift/Elevator",
        "Concierge",
        "Deposit Box",
        "Non-smoking Room",
        "Cold Pool",
        "Restaurant",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Car Parking",
        "Cable TV",
        "Drug Store",
        "Airport Transfer"
      ],
      "price_info": {
        "basic_price_per_night": 1175000,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 2450000,
        "basic_price": 1175000,
        "fee_and_tax": 0,
        "total_price": 2450000,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.686988,
        "latitude": -6.113536,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 4,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": null,
        "reviewPercentage": null,
        "cleanlinessRating": null,
        "serviceAndStaffRating": null,
        "roomComfortRating": null,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": null,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 1175000,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 1125000,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 2750,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "AONE Hotel Jakarta",
      "cityName": "Glodok",
      "address": "Jl. Wahid Hasyim No. 80, Jakarta Pusat",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 732840,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/2750/2-916.jpg",
      "det": null,
      "shortDescription": "A One Hotel Jakarta adalah hotel bintang empat dengan konsep urban modern, yang mewah & dirancang dengan elegan, Hotel ini memiliki 150 kamar tamu yang didesain dengan indah untuk kenyamanan pribadi Anda selama menginap. A One Hotel Jakarta berlokasi di Jl. Wahid Hasyim yang merupakan distrik keuangan dan diplomatik di Jakarta.\r\rDengan kamar yang menakjubkan, fasilitas yang fantastis dan layanan bintang lima, A One Hotel Jakarta merupakan tempat yang nyaman untuk menginap. Tempat yang ideal untuk bisnis, kesenangan dan rekreasi, hotel baru yang modern ini ini terletak dekat dengan berbagai tempat wisata seperti Monumen Nasional (Monas), Mall Grand Indonesia, Plaza Indonesia, Sarinah Shopping Mall, dan Pasar tekstil Tanah Abang.",
      "room_info": "Non-Smoking, Wardrobe, AC, Telephone, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Kettle, Mini-Bar, Refrigerator, Working Desk, Shower Bathroom, Bath Amenities, Hot Water Bath, Slippers / sandals, City View",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Bar/Lounge",
        "Laundry",
        "Fitness Centre",
        "Business Centre",
        "Lift/Elevator",
        "Concierge",
        "Ballroom",
        "Deposit Box",
        "Non-smoking Room",
        "Cold Pool",
        "Restaurant",
        "Sauna / Spa",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Conference Rm",
        "Car Parking",
        "Cable TV",
        "Fax Machine",
        "Baby Friendly",
        "Airport Transfer"
      ],
      "price_info": {
        "basic_price_per_night": 825840,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 1776000,
        "basic_price": 825840,
        "fee_and_tax": 0,
        "total_price": 1776000,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.827942,
        "latitude": -6.186792,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 4,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": null,
        "reviewPercentage": null,
        "cleanlinessRating": null,
        "serviceAndStaffRating": null,
        "roomComfortRating": null,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": null,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 825840,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 825840,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 2451,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Sparks Life Jakarta",
      "cityName": "Mangga Besar",
      "address": "Jalan Mangga Besar No. 42, Jakarta Barat",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 465000,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/2451/1-225.jpg",
      "det": null,
      "shortDescription": "Sparks Life adalah hotel dengan segmen skala menengah dalam koleksi hotel Sparks yang dirancang dengan baik untuk menawarkan kenyamanan utama, kamar modern dan dilengkapi untuk menginap dengan layanan penuh perhatian dan efisien baik untuk pelancong bisnis maupun rekreasi. Ruang pertemuan menawarkan \"semua\" paket. Paket ini termasuk menyewa kamar Akses internet, rehat kopi dan makanan, semua peralatan teknis dan fasilitas lainnya. Hotel ini menyediakan sarapan sehat prasmanan, minibar, dan teh gratis & kopi di semua kamar.\r\rSparks Life Jakarta menawarkan kenyamanan dalam 114 kamar dan suite dengan 6 ruang pertemuan untuk memenuhi berbagai kebutuhan pertemuan yang Anda butuhkan.\rSetiap kamar dilengkapi dengan AC, TV LED, saluran internasional, panggilan IDD, fasilitas kopi & teh membuat, safe deposit, dan mini bar.",
      "room_info": "Non-Smoking, AC, Telephone, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Kettle, Mini-Bar, Working Desk, Shower Bathroom, Bath Amenities, Hot Water Bath, Slippers / sandals, City View",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Bar/Lounge",
        "Laundry",
        "Business Centre",
        "Lift/Elevator",
        "Concierge",
        "Deposit Box",
        "Non-smoking Room",
        "Cold Pool",
        "Restaurant",
        "Sauna / Spa",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Conference Rm",
        "Car Parking",
        "Cable TV",
        "Airport Transfer"
      ],
      "price_info": {
        "basic_price_per_night": 511500,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 1100000,
        "basic_price": 511500,
        "fee_and_tax": 0,
        "total_price": 1100000,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.82198476812,
        "latitude": -6.1497365632971,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 3,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": null,
        "reviewPercentage": null,
        "cleanlinessRating": null,
        "serviceAndStaffRating": null,
        "roomComfortRating": null,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": null,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 511500,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 511500,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 1777,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Sotis Hotel Jakarta",
      "cityName": "Blok M",
      "address": "Jl. Falatehan 1 No. 21-22, Jakarta Selatan",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 651000,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/1777/1-556.jpg",
      "det": null,
      "shortDescription": "Sotis merupakan kain tenunan tangan tradisional dari Nusa Tenggara Timur. Keindahan Kain sotis yang dibuat dengan  keterampilan, teknik dan semangat telah menginspirasi kami untuk memberikan kualitas terbaik, nilai dan layanan untuk memenuhi kebutuhan para tamu. \r\rSotis menjadi konsep hotel yang unik kami yakin bahwa setiap orang akan senang dan bangga untuk tinggal di sotis hotel.",
      "room_info": "In-room Internet Cable, Non-Smoking, Wardrobe, AC, Telephone, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Hairdryer, Kettle, Mini-Bar, Refrigerator, Working Desk, Shower Bathroom, Bath Amenities, Hot Water Bath, Slippers / sandals",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Bar/Lounge",
        "Laundry",
        "Business Centre",
        "Lift/Elevator",
        "Concierge",
        "Deposit Box",
        "Non-smoking Room",
        "Restaurant",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Conference Rm",
        "Car Parking",
        "Cable TV",
        "Fax Machine",
        "Airport Transfer"
      ],
      "price_info": {
        "basic_price_per_night": 790500,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 1700000,
        "basic_price": 790500,
        "fee_and_tax": 0,
        "total_price": 1700000,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.7439709,
        "latitude": -6.1385346,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 3,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": null,
        "reviewPercentage": null,
        "cleanlinessRating": null,
        "serviceAndStaffRating": null,
        "roomComfortRating": null,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": null,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 790500,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 790500,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 1031,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Big Hotel Jakarta",
      "cityName": "Mangga Besar",
      "address": "Jl. Kartini 64 No. 2 (sebelah Swiss-bel), Jakarta Pusat",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 341775,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/1031/1-76.jpg",
      "det": null,
      "shortDescription": "Big Hotel Jakarta memiliki beragam fasilitas yang berkualitas tinggi dan fitur untuk memastikan semua tamu menikmati pengalaman keramahan dan sambutan hangat dan menikmati kenyamanan surga Jakarta. Semua aspek Big Hotel Jakarta yang dibangun, dirancang, dan diteliti untuk memastikan bahwa kami memenuhi standar tinggi yang diterapkan oleh manajemen, dan untuk memastikan bahwa kunjungan Anda ke Jakarta dengan Hotel kami di sini adalah istimewa dan lebih berkesan.\r\rBig Hotel Jakarta memiliki 153 kamar. Hotel ini memiliki pusat bisnis bagi tamu yang mencari tempat yang tenang untuk melakukan pekerjaan, restoran, ruang pertemuan, dan tempat parkir mobil.\r\rDi setiap kamar Anda akan menemukan Kamar mandi pribadi, brankas, Minibar, pembuat teh / kopi, Sambungan Langsung Internasional (SLI), Layanan Kamar 24 jam, serta TV satelit.\r\rTemukan perpaduan yang menarik anatara pelayanan profesional dan sejumlah fasilitas di Big Hotel Jakarta",
      "room_info": "Non-Smoking, Wardrobe, AC, Telephone, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, DVD Player, Working Desk, Shower Bathroom, Bath Amenities, Hot Water Bath, Slippers / sandals",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Laundry",
        "Business Centre",
        "Lift/Elevator",
        "Concierge",
        "Deposit Box",
        "Non-smoking Room",
        "Restaurant",
        "Sauna / Spa",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Car Parking",
        "Cable TV"
      ],
      "price_info": {
        "basic_price_per_night": 340856,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 761250,
        "basic_price": 340856,
        "fee_and_tax": 0,
        "total_price": 761250,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.833173,
        "latitude": -6.1491236757465,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 3,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": 31,
        "reviewPercentage": null,
        "cleanlinessRating": 7.6,
        "serviceAndStaffRating": 7,
        "roomComfortRating": 7.5,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": 7.3,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 340856,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 367106,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 4391,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Hotel Bulevar Jakarta",
      "cityName": "Jakarta Barat",
      "address": "Jl. Tanjung Duren Raya Kav.1, Jakarta Barat",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 292950,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/4391/6-615.jpg",
      "det": null,
      "shortDescription": "Hotel Bulevar sangat dianjurkan untuk backpacker yang ingin menginap yang terjangkau namun nyaman pada saat bersamaan.\nHotel Bulevar adalah tempat penginapan ideal bagi para pelancong yang mencari daya tarik, kenyamanan dan kepraktisan di Jakarta. \nDari acara bisnis hingga gathering perusahaan, Hotel Bulevar menyediakan layanan dan fasilitas lengkap yang Anda dan kolega Anda butuhkan. Resepsionis 24-jam tersedia untuk melayani Anda, dari check-in hingga check-out, atau bantuan yang Anda butuhkan. Jika Anda menginginkan lebih, jangan ragu untuk bertanya ke resepsionis, kami selalu siap untuk mengakomodasi Anda.\nNikmati hidangan favorit Anda dengan aneka masakan spesial dari Hotel Bulevar khusus untuk Anda.\nWiFi tersedia di area umum properti untuk membantu Anda tetap terhubung dengan keluarga dan teman.\nHotel Bulevar adalah pilihan ideal bagi Anda yang mencari akomodasi yang nyaman namun terjangkau. \n\n\n\n\n\n\n",
      "room_info": "In-room Internet Cable, Wardrobe, AC, Telephone, Wireless/WIFI, Working Desk, Shower Bathroom",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Laundry",
        "Lift/Elevator",
        "Ballroom",
        "Deposit Box",
        "Non-smoking Room",
        "Restaurant",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Car Parking",
        "Cable TV"
      ],
      "price_info": {
        "basic_price_per_night": 292950,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 630000,
        "basic_price": 292950,
        "fee_and_tax": 0,
        "total_price": 630000,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.787556,
        "latitude": -6.174164,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 2,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": null,
        "reviewPercentage": null,
        "cleanlinessRating": null,
        "serviceAndStaffRating": null,
        "roomComfortRating": null,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": null,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 292950,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 292950,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 2675,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Kyriad Hotel Airport Jakarta",
      "cityName": "Tangerang",
      "address": "Jl. Marsekal Suryadarma No.01, Karang Sari, Tangerang",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 421600,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/2675/23-152.jpg",
      "det": null,
      "shortDescription": "Kyriad Hotel Airport Jakarta berada sekitar 17KM dari Bandara Internasional Soekarno Hatta, cocok bagi Anda yang akan transit atau dalam perjalanan bisnis.\r\rKyriad Hotel Airport Jakarta dilengkapi dengan berbagai fasilitas, diantaranya laundry, elevator (lift), sebuah kolam renang untuk bersantai sejenak setelah beraktivitas seharian, restoran dengan berbagai menu mancanegara maupun menu lokal, layanan kamar, koneksi internet nirkabel, salutan televisi kabeli dan antar jemput bandara.\r\rTempat menarik disekitar hotel :\r1.       800 meters to Airlanes Office : Garuda Indonesia, Air Asia, Sriwijaya and Airfast Charter Flight\r2.       7 km to Soekarno-Hatta International Airport\r3.       5 km to the world biggest dome, Al-Azhom mosque\r4.       5 km to Benteng Heritage Museum\r5.       5 km to Pasar Lama traditional shopping & culinary center\r6.       5 km to Tangcity Mall, Bale Kota Mall\r7.       10 km to Summarecon Mall Serpong and Lippo Karawaci Mall",
      "room_info": "Non-Smoking, Wardrobe, Fan, AC, Telephone, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Hairdryer, Ironing, Kettle, Working Desk, Shower Bathroom, Bath Amenities, Hot Water Bath, Slippers / sandals, City View, Swimming Pool View",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Laundry",
        "Lift/Elevator",
        "Concierge",
        "Deposit Box",
        "Non-smoking Room",
        "Cold Pool",
        "Restaurant",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Car Parking",
        "Cable TV",
        "Fax Machine",
        "Kitchen",
        "Airport Transfer"
      ],
      "price_info": {
        "basic_price_per_night": 421600,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 906667.2,
        "basic_price": 421600,
        "fee_and_tax": 0,
        "total_price": 906667,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.633024,
        "latitude": -6.15795,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 3,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": null,
        "reviewPercentage": null,
        "cleanlinessRating": null,
        "serviceAndStaffRating": null,
        "roomComfortRating": null,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": null,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 421600,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 421600,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 893,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "éL Royale Hotel Jakarta, Kelapa Gading (previously Grand Whiz Hotel Kelapa Gading)",
      "cityName": "Kelapa Gading",
      "address": "Jl. Bukit Gading Raya Kav.1, Jakarta Utara",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 0,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/893/1-83.jpg",
      "det": null,
      "shortDescription": "éL Royale Hotel Jakarta terletak di jantung kawasan Kelapa Gading, di Jakarta Utara. Hanya dibutuhkan 45 menit berkendara dari Bandara Internasional Soekarno-Hatta, 20 menit berkendara dari Bandara Halim Perdana Kusuma dan 30 menit dari Stasiun Kereta Gambir. Hotel bintang empat dengan 322 kamar ini dilengkapi dengan 13 ruang pertemuan dan ballroom yang bisa menampung hingga 1500 orang.\r\rHotel ini berada dalam jangkauan ke kawasan industri dan bisnis Jakarta Utara, dan berada di dekat pusat perbelanjaan dan kawasan komersial. Hal ini membuat hotel 27 lantai ini menjadi pilihan tepat bagi pelaku bisnis untuk mengadakan acara dan pertemuan.\r\rHotel ini juga merupakan tempat yang tepat bagi mereka yang ingin menghabiskan akhir pekan dengan keluarga dan teman-teman, karena éL Royale Hotel Jakarta menyediakan standart pelayanan berkualitas tinggi.",
      "room_info": null,
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Bar/Lounge",
        "Laundry",
        "Business Centre",
        "Lift/Elevator",
        "Concierge",
        "Ballroom",
        "Deposit Box",
        "Non-smoking Room",
        "Cold Pool",
        "Restaurant",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Conference Rm",
        "Car Parking",
        "Cable TV",
        "Fax Machine",
        "Kitchen"
      ],
      "price_info": {
        "basic_price_per_night": null,
        "fee_and_tax_per_night": null,
        "total_price_per_night": null,
        "basic_price": null,
        "fee_and_tax": null,
        "total_price": null,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": null,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.8964695,
        "latitude": -6.1528379,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 4,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": 5,
        "reviewPercentage": null,
        "cleanlinessRating": 8,
        "serviceAndStaffRating": 7.8,
        "roomComfortRating": 8,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": 7.6,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": null
    },
    {
      "hotelId": 2289,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Swiss-Belinn Simatupang Jakarta",
      "cityName": "Simatupang",
      "address": "Jl. RA. Kartini no.32 Lebak Bulus, Jakarta Selatan",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 548700,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/2289/1-462.jpg",
      "det": null,
      "shortDescription": "Swiss-Belinn Simatupang Jakarta berada di Jl. R.A. Kartini, Jakarta Selatan, dan berada dekat dengan salah satu mall terbesar di Jakarta, Cilandak Town Square. Mall ini dapat ditempuh dalam waktu 15 menit berkendara dan Bandara Internasional Soekarno Hatta dapat ditempuh dalam waktu 45 menit berkendara dari hotel.\r\rHotel ini dilengkapi dengan berbagai fasilitas untuk kenyamanan Anda, diantaranya kolam renang, fasilitas koneksi internet nirkabel, restorand dengan berbagai pilihan menu barat dan Indonesia, serta saluran televisi kabel.\r\rSwiss Belinn Simatupang cocok bagi Anda yang bepergian untuk tujuan berwisata atau keperluan bisnis.",
      "room_info": "Non-Smoking, Wardrobe, AC, Telephone, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Hairdryer, Ironing, Kettle, Mini-Bar, Refrigerator, Working Desk, Shower Bathroom, Bath Amenities, Hot Water Bath, Slippers / sandals, City View, Swimming Pool View",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Bar/Lounge",
        "Laundry",
        "Fitness Centre",
        "Lift/Elevator",
        "Concierge",
        "Deposit Box",
        "Non-smoking Room",
        "Cold Pool",
        "Restaurant",
        "Sauna / Spa",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Car Parking",
        "Cable TV",
        "Fax Machine"
      ],
      "price_info": {
        "basic_price_per_night": 596250,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 1250000,
        "basic_price": 596250,
        "fee_and_tax": 0,
        "total_price": 1250000,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.790402,
        "latitude": -6.292568,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 3,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": 1,
        "reviewPercentage": null,
        "cleanlinessRating": 8,
        "serviceAndStaffRating": 7,
        "roomComfortRating": 8,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": 7.5,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 596250,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 566250,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 174,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Bidakara Hotel Jakarta",
      "cityName": "Tebet",
      "address": "Jl. Gatot Subroto Kav 71-73, Jakarta Selatan",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 697500,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/174/2-387.jpg",
      "det": null,
      "shortDescription": "Menempati kawasan bisnis strategis di selatan Jakarta dan menyandang reputasi sebagai Hotel berbintang empat (****) dengan standar internasional Hotel Bidakara Jakarta tampil sebagai salah satu Hotel dengan produk dan layanan yang inovatif, yaitu bernilai tambah yang mendukung kegiatan bisnis dan konvensi berbagai institusi pemerintah dan swasta yang keberadaannya memiliki akses yang kuat terhadap lokasi Hotel Bidakara.\r\rFasilitas kamar berjumlah 174 kamar terdiri atas 145 kamar Deluxe , 15 kamar Executive, 11 kamar Junior Suite, 1 kamar Moderate Suite dan 2 kamar Family Suite. Fasilitas lainnya berupa 5 Outlet Restoran, Room Service 24 jam, laundry, Kolam Renang, Pusat Kebugaran dan 14 Ruang pertemuan yang keseluruhannya difasilitasi dengan layanan internet berkapasitas 1 Mbps.",
      "room_info": "Non-Smoking, Wardrobe, AC, Telephone, Wireless/WIFI, Local TV Channels, Satellite TV Channels, Kettle, Mini-Bar, Refrigerator, Working Desk, Shower Bathroom, Bath Amenities, Hot Water Bath, Slippers / sandals, City View",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Bar/Lounge",
        "Laundry",
        "Money Changer",
        "Fitness Centre",
        "Business Centre",
        "Lift/Elevator",
        "Concierge",
        "Ballroom",
        "Deposit Box",
        "Non-smoking Room",
        "Cold Pool",
        "Restaurant",
        "Sauna / Spa",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Conference Rm",
        "Car Parking",
        "Cable TV",
        "Fax Machine"
      ],
      "price_info": {
        "basic_price_per_night": 697500,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 1500000,
        "basic_price": 697500,
        "fee_and_tax": 0,
        "total_price": 1500000,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.8419017,
        "latitude": -6.2403324,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 4,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": 2,
        "reviewPercentage": null,
        "cleanlinessRating": 9,
        "serviceAndStaffRating": 9,
        "roomComfortRating": 8.5,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": 8.6,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 697500,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 697500,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 1379,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "favehotel Gatot Subroto Jakarta",
      "cityName": "Kuningan",
      "address": "Jl. Kartika Chandra Kav A9, Jakarta Selatan",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 515387,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/1379/1-128.jpg",
      "det": null,
      "shortDescription": "Favehotel Gatot Subroto di Jakarta dirancang untuk segmentasi wisatawan bisnis maupun keluarga dengan harga terjangkau. Sentuhan unik dengan suasana yang menyenangkan dan layanan yang ramah serta penuh kejutan membuat Favehotel menjadi pilihan favorit para wisatawan cerdas yang berkunjung ke Jakarta. Lokasi terletak di dekat atraksi populer kota Jakarta seperti Kedutaan Cina dan Mall Ambasador.\r\rHotel menyediakan layanan dan fasilitas seperti Layanan Kamar dan Laundry, Restoran, Ruang rapat, Ruang Konferensi, dan Area Parkir. Setiap kamar dirancang kedap suara untuk memberi kenyamanan maksimal, dan dilengkapi dengan AC, TV LED, Wi-Fi gratis, dan Kamar Mandi Shower.",
      "room_info": "Wardrobe, AC, Telephone, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Working Desk, Shower Bathroom, Bath Amenities, Hot Water Bath",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Bar/Lounge",
        "Laundry",
        "Lift/Elevator",
        "Concierge",
        "Deposit Box",
        "Restaurant",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Conference Rm",
        "Car Parking",
        "Cable TV"
      ],
      "price_info": {
        "basic_price_per_night": 515387,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 1108360,
        "basic_price": 515387,
        "fee_and_tax": 0,
        "total_price": 1108360,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.819552,
        "latitude": -6.227251,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 3,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": 2,
        "reviewPercentage": null,
        "cleanlinessRating": 8.5,
        "serviceAndStaffRating": 8,
        "roomComfortRating": 7.5,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": 7.5,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 515387,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 515387,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 1382,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "favehotel Melawai Jakarta",
      "cityName": "Blok M",
      "address": "Jl. Melawai 4 No. 3 - 11, Jakarta Selatan",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 425363,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/1382/1-768.jpg",
      "det": null,
      "shortDescription": "favehotel Melawai merupakan pilihan yang tepat dalam mencari akomodasi ketika anda mengunjungi Jakarta. Dengan akses yang mudah dari Bandar Udara, terminal bis, halte busway dan tranportasi antar kota. favehotel Melawai berlokasi di pusat bisnis, tempat belanja dan tempat hiburan di Jakarta Selatan. \r\rUntuk tamu yang sedang dalam urusan bisnis, kamu menyediakan semua fasilitas dan layanan untuk mendukung kebutuhan sehari hari anda, mulai dari area parkir yang cukup luas, layanan parkir valet, layanan binatu, ruang pertemuan, internet tanpa kabel baik di area umum maupun di dalam kamar, TV dengan siaran mancanegara, pancuran, toko serba ada, restoran dan kafe yang terkenal. \r\rfavehotel Melawai dikelilingi oleh berbagai macam pusat perbelanjaan dan tempat makan yang dapat memuaskan anda. Mulai dari penjaja makanan tradisional Indonesia, masakan asli dari Jepang sampai pada hidangan bintang 5. Anda juga dapat menikmati hiburan seperti klub, bar dan karaoke.",
      "room_info": "AC, Telephone, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Working Desk, Shower Bathroom, Bath Amenities, Hot Water Bath",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Laundry",
        "Lift/Elevator",
        "Concierge",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Cable TV",
        "Fax Machine",
        "Airport Transfer"
      ],
      "price_info": {
        "basic_price_per_night": 425363,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 914760,
        "basic_price": 425363,
        "fee_and_tax": 0,
        "total_price": 914760,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.801538,
        "latitude": -6.24486,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 3,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": 11,
        "reviewPercentage": null,
        "cleanlinessRating": 8,
        "serviceAndStaffRating": 7.4,
        "roomComfortRating": 7.7,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": 7.5,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 425363,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 425363,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 1635,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Swiss-Belinn Airport Jakarta",
      "cityName": "Tangerang",
      "address": "Jl. Husein Sastranegara No 9, Tangerang",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 535680,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/1635/2-662.jpg",
      "det": null,
      "shortDescription": "Swiss-Belinn Airport adalah hotel internasional bintang 3 yang berjarak hanya 10 menit dari Soekarno-Hatta International Airport. Dengan kombinasi desain modern dan stylish, hotel ini dikelilingi oleh taman-taman memberikan ketenangan pada tamu di tengah daerah Tangerang ramai.\r\rHotel ini memiliki 145 kamar kami yang nyamandengan desain kontemporer. Terdiri dari 136 kamar superior, 6 kamar deluxe dan 3 suite yang meliputi pilihan kamar non-merokok dan kamar tamau khusus untuk disabilitas. \r \rNikmati pengalaman bersantap Anda dalam kombinasi unik dari bar, restoran dan lounge. Dirancang untuk santai makan, restoran yang menyajikan masakan lokal dan internasional yang menyenangkan, lounge dan bar menawarkan berbagai makanan ringan dan minuman sepanjang hari. \r \rUntuk pertemuan dan bisnis, hotel ini menyediakan tujuh ruang pertemuan, ideal untuk mengadakan pertemuan kecil untuk menengah hingga 200 orang.",
      "room_info": "Non-Smoking, AC, Telephone, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Kettle, Mini-Bar, Working Desk, Shower Bathroom, Bath Amenities, Hot Water Bath, Slippers / sandals",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Bar/Lounge",
        "Laundry",
        "Fitness Centre",
        "Business Centre",
        "Lift/Elevator",
        "Concierge",
        "Deposit Box",
        "Restaurant",
        "Sauna / Spa",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Car Parking",
        "Balcony"
      ],
      "price_info": {
        "basic_price_per_night": 535680,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 1152000,
        "basic_price": 535680,
        "fee_and_tax": 0,
        "total_price": 1152000,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.685879,
        "latitude": -6.124923,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 3,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": 2,
        "reviewPercentage": null,
        "cleanlinessRating": 7.5,
        "serviceAndStaffRating": 8.5,
        "roomComfortRating": 8,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": 7.9,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 535680,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 535680,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 871,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "V Hotel Tebet Jakarta",
      "cityName": "Tebet",
      "address": "Jl. Prof Dr. Soepomo No 100 (100 meter dari Patung Pancoran), Jakarta Selatan",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 372000,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/871/1-479.jpg",
      "det": null,
      "shortDescription": "Terletak di Jakarta Selatan, Tebet, 100m dari Patung Pancoran, dari sini, para tamu dapat menikmati akses mudah ke berbagai area yang dapat ditemukan di dekat V Hotel Jakarta.  Hanya sekitar 15 menit dari Bandara Halim Perdanakusuma, dan 1 menit jalan kami  ke Bakmi GM, Superindo serta Hokben.\r\rSetiap kamar yang ditata dengan rapih dilengkapi oleh fasilitas AC, Televisi support oleh Indovision, Wi-Fi, brankas, dan Pemanas Air yang dapat memberi kenyamanan secara maksimal selama Anda menginap.\r\rTemukan layanan dengan standarisasi V Hotel Jakarta.",
      "room_info": "Non-Smoking, Wardrobe, AC, Telephone, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Ironing, Working Desk, Shower Bathroom, Bath Amenities, Hot Water Bath, Slippers / sandals, City View",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Lift/Elevator",
        "Deposit Box",
        "Non-smoking Room",
        "Restaurant",
        "Room Service",
        "Public-area Wi-Fi",
        "Car Parking",
        "Cable TV",
        "Fax Machine",
        "Airport Transfer"
      ],
      "price_info": {
        "basic_price_per_night": 432450,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 930000,
        "basic_price": 432450,
        "fee_and_tax": 0,
        "total_price": 930000,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.8447758,
        "latitude": -6.241943,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 2,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": 56,
        "reviewPercentage": null,
        "cleanlinessRating": 8.4,
        "serviceAndStaffRating": 7.2,
        "roomComfortRating": 8.1,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": 7.8,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 432450,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 432450,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 2821,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "POP! Hotel Pasar Baru Jakarta",
      "cityName": "Pasar Baru",
      "address": "Jl. K.H. Samanhudi No. 17-19, Pasar Baru - Sawah Besar, Jakarta Pusat",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 351540,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/2821/1-360.jpg",
      "det": null,
      "shortDescription": "POP! Hotel Pasar Baru Jakarta, hotel bergaya modern dengan harga murah untuk para pelancong yang mencari kenyamanan. Hotel baru yang terletak di Jakarta Pusat, tepatnya di daerah Pasar Baru Jakarta menawarkan pengalaman menginap yang nyaman dengan harga terjangkau.\r\rPara penginap dapat dengan mudahnya berkunjung ke Pasar Baru Jakarta yang berjarak sangat dekat dengan hotel. Selain itu, para penginap dapat mengakses tempat lain dengan mudah dan cepat.",
      "room_info": "Non-Smoking, AC, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Sofa-bed, Shower Bathroom, Hot Water Bath",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Laundry",
        "Lift/Elevator",
        "Deposit Box",
        "Non-smoking Room",
        "Restaurant",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Car Parking",
        "Cable TV"
      ],
      "price_info": {
        "basic_price_per_night": 351540,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 756000,
        "basic_price": 351540,
        "fee_and_tax": 0,
        "total_price": 756000,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.829977,
        "latitude": -6.162376,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 2,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": 3,
        "reviewPercentage": null,
        "cleanlinessRating": 8,
        "serviceAndStaffRating": 6.7,
        "roomComfortRating": 8,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": 7.3,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 351540,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 351540,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 1440,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "POP! Hotel Kemang Jakarta",
      "cityName": "Kemang",
      "address": "Jl. Kemang Raya No. 3, Jakarta Selatan",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 332940,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/1440/1-455.jpg",
      "det": null,
      "shortDescription": "Terletak sangat strategis di wilayah Kemang Jakarta, POP! Hotel Kemang Jakarta memberikan kemudahan akses kepada para tamu yang menginap. Hanya beberapa menit dengan berjalan kaki ke Kemang Village Mall, Kemchick Supermarket, dan juga ke banyak restoran dan kafe terkenal di Jakarta.\r\r110 Kamar yang disediakan dilengkapi dengan tempat tidur yang sangat nyaman, akses internet via WiFi gratis, televisi LCD yang dilengkapi dengan saluran internasional, kamar mandi dengan shower air panas, gratis morning bite di pagi hari dan juga air mineral.",
      "room_info": "Non-Smoking, AC, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Sofa-bed, Shower Bathroom",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Lift/Elevator",
        "Deposit Box",
        "Non-smoking Room",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Car Parking",
        "Cable TV"
      ],
      "price_info": {
        "basic_price_per_night": 332940,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 716000,
        "basic_price": 332940,
        "fee_and_tax": 0,
        "total_price": 716000,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.8088282,
        "latitude": -6.2549907,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 2,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": 6,
        "reviewPercentage": null,
        "cleanlinessRating": 8.7,
        "serviceAndStaffRating": 8.5,
        "roomComfortRating": 7.5,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": 7.8,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 332940,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 332940,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 291,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Jogjakarta Plaza Hotel",
      "cityName": "Yogyakarta",
      "address": "Jl. Affandi - Gejayan, Complex Colombo, Sleman",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 790500,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/291/1-564.jpg",
      "det": null,
      "shortDescription": "Jogjakarta Plaza Hotel adalah hotel berkelas internasional yang terletak di distrik kota pelajar. Properti ini memiliki arsitektur khas Jawa dengan fasilitas modern.\r\rTerletak hanya 5.0 Km dari pusat kota, 15 menit berkendara dari Bandara Adisucipto, dan dekat dengan area populer lainnya seperti Museum Affandi, Galeria Mall Yogyakarta.\r\rHotel menawarkan pusat kebugaran, kolam renang, lapangan tenis, dan kamar-kamar yang dilengkapi dengan Wi-Fi gratis. Tersedia toko, tempat parkir mobil, dan kotak penyimpanan aman. Layanan termasuk layanan kamar, pijat, penyewaan sepeda dan mobil. Layanan binatu dan laundry juga tersedia berdasarkan permintaan.\r\rJogjakarta Plaza Hotel telah menyediakan kids pool dan baby pool. Dengan kedalaman 80 cm untuk kids pool dan 30 cm untuk baby pool akan menjadi tempat favorit bagi buah hati anda untuk bermain. Selain itu, untuk  memberikan suasana baru diadakanlah pembaharuan lantai yang dipastikan aman bagi si kecil.",
      "room_info": "AC, Telephone, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Hairdryer, Kettle, Mini-Bar, Refrigerator, Working Desk, Shower Bathroom, Bath Amenities, Hot Water Bath, Slippers / sandals, Garden View",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Bar/Lounge",
        "Laundry",
        "Fitness Centre",
        "Business Centre",
        "Concierge",
        "Ballroom",
        "Deposit Box",
        "Cold Pool",
        "Restaurant",
        "Sauna / Spa",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Car Parking",
        "Cable TV",
        "Tennis Court"
      ],
      "price_info": {
        "basic_price_per_night": 790500,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 1700000,
        "basic_price": 790500,
        "fee_and_tax": 0,
        "total_price": 1700000,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 110.389369,
        "latitude": -7.776934,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 4,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": 1,
        "reviewPercentage": null,
        "cleanlinessRating": 8,
        "serviceAndStaffRating": 7,
        "roomComfortRating": 8,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": 7.5,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 790500,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 790500,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 1578,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "favehotel Puri Indah Jakarta",
      "cityName": "Slipi",
      "address": "Jl. Kembang Abadi Raya Blok A1 No.1, Kembangan Selatan, Jakarta Barat",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 418949,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/1578/1-991.jpg",
      "det": null,
      "shortDescription": "favehotel Puri Indah Jakarta memiliki 108 kamar bergaya modern, fungsional, super bersih menawarkan cutting edge, TV LED, internet cepat dan handal, televisi, tempat tidur kualitas terbaik dan linen. Terletak di lokasi yang sangat bagus dan ramai Puri Indah Jakarta Barat dengan akses mudah & cepat untuk jalan tol Jakarta - Tangerang, Kebon Jeruk, dan banyak bisnis dan sekitar Daan Mogot.",
      "room_info": "Non-Smoking, Wardrobe, AC, Telephone, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Hairdryer, Kettle, Mini-Bar, Refrigerator, Working Desk, Shower Bathroom, Bath Amenities, Hot Water Bath, Slippers / sandals",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Bar/Lounge",
        "Laundry",
        "Business Centre",
        "Concierge",
        "Deposit Box",
        "Restaurant",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Car Parking",
        "Cable TV",
        "Airport Transfer"
      ],
      "price_info": {
        "basic_price_per_night": 526640,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 1132560,
        "basic_price": 526640,
        "fee_and_tax": 0,
        "total_price": 1132560,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.7513560086,
        "latitude": -6.1828436660736,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 3,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": 6,
        "reviewPercentage": null,
        "cleanlinessRating": 8.2,
        "serviceAndStaffRating": 8,
        "roomComfortRating": 8,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": 7.7,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 526640,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 526640,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 1458,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Hotel Neo Tendean Jakarta",
      "cityName": "Mampang",
      "address": "Jl. Wolter Monginsidi No. 131, Jakarta Selatan",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 369098,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/1458/1-645.jpg",
      "det": null,
      "shortDescription": "Neo Tendean Jakarta terletak di Kebayoran baru Jakarta Selatan dan sangat dekat dengan pusat bisnis Jakarta. Hanya 5 menit dari Jl.Sudirman dan Jl.HR rasuna dan juga area Kemang. Untuk berbelanja ada Plaza blok M , Blok M Square, Senayan City dan juga Pacific Place.\r\rSelain memiliki lokasi yang sangat strategis, hotel Neo Tendean memiliki banyak kelebihan untuk anda :\r\r-  Baru dibuka Oktober 2013\r-  Desain unik Hotel ramah lingkungan dengan gaya modern\r-  2 Ruang rapat serba guna dengan fasilitas konferensi\r-  Front desk dan security 24 Jam\r-  Kualitas Internasional - Dikelola oleh Aston International\r-  Harga terbaik dengan servis kualitas tinggi\r-  Gratis Wi-Fi di kamar dan di ruang public",
      "room_info": "AC, Telephone, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Kettle, Mini-Bar, Working Desk, Sofa-bed, Shower Bathroom, Bath Amenities, Hot Water Bath, Slippers / sandals",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Bar/Lounge",
        "Laundry",
        "Business Centre",
        "Lift/Elevator",
        "Concierge",
        "Deposit Box",
        "Cold Pool",
        "Restaurant",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Conference Rm",
        "Car Parking",
        "Cable TV",
        "Fax Machine"
      ],
      "price_info": {
        "basic_price_per_night": 369098,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 793760,
        "basic_price": 369098,
        "fee_and_tax": 0,
        "total_price": 793760,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.817625,
        "latitude": -6.238702,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 3,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": 9,
        "reviewPercentage": null,
        "cleanlinessRating": 7.7,
        "serviceAndStaffRating": 7,
        "roomComfortRating": 6.9,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": 7.1,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 369098,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 369098,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 461,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Hotel Ciputra Jakarta",
      "cityName": "Slipi",
      "address": "Jl. Letnan Jenderal S. Parman, Slipi - Tomang, Jakarta Barat",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 632400,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/461/2-346.jpg",
      "det": null,
      "shortDescription": "Terletak di lokasi yang sangat strategis, merupakan salah satu hotel terdekat dari/ke Bandar Udara Internasional Jakarta Soekarno-Hatta, dan hanya beberapa menit dari pusat kota. Hotel ini menawarkan pilihan akomodasi yang terdiri dari 333 kamar dalam berbagai tipe dan ukuran. Setiap kamar didesain dengan gaya kontemporer yang elegan dan dilengkapi fasilitas modern untuk memastikan Anda memperoleh pelayanan terbaik selama tinggal di Jakarta.\r \rDi The Gallery Restaurant menyediakan berbagai pilihan makanan internasional yang menggugah selera atau sekedar menikmati waktu santai sambil menikmati aneka kreasi minuman di Lobby Lounge atau Pulau Bar and Restaurant. Fasilitas lain diantaranya adalah pusat kebugaran dengan peralatan lengkap, bersantai di sauna dan spa, atau menikmati pijat tradisional dan perawatan aromaterapi. \r \rHotel Ciputra Jakarta juga menyediakan ruangan-ruangan luas bagi Anda yang ingin mengadakan pertemuan bisnis.",
      "room_info": "In-room Internet Cable, Alarm Clock, Wardrobe, AC, Telephone, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Hairdryer, Kettle, Refrigerator, Working Desk, Bath-Tub, Shower Bathroom, Bath Amenities, Hot Water Bath, Slippers / sandals, City View",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Bar/Lounge",
        "Laundry",
        "Fitness Centre",
        "Business Centre",
        "Lift/Elevator",
        "Concierge",
        "Ballroom",
        "Deposit Box",
        "Non-smoking Room",
        "Cold Pool",
        "Restaurant",
        "Sauna / Spa",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Conference Rm",
        "Car Parking",
        "Cable TV",
        "Fax Machine",
        "Airport Transfer"
      ],
      "price_info": {
        "basic_price_per_night": 632400,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 1360000,
        "basic_price": 632400,
        "fee_and_tax": 0,
        "total_price": 1360000,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.78661750297,
        "latitude": -6.1679956642419,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 4,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": 1,
        "reviewPercentage": null,
        "cleanlinessRating": 8,
        "serviceAndStaffRating": 8,
        "roomComfortRating": 8,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": 8.3,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 632400,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 632400,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 4335,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "KYRIAD HOTEL FATMAWATI JAKARTA",
      "cityName": "Fatmawati",
      "address": "JL. RS FATMAWATI NO 1 - 21 CIPETE UTARA, KEBAYORAN BARU JAKARTA SELATAN, Jakarta Selatan",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 355710,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/4335/2-711.jpg",
      "det": null,
      "shortDescription": "",
      "room_info": "AC, Telephone, Wireless/WIFI, Safety Deposit Box, Satellite TV Channels, Kettle, Shower Bathroom, Bath Amenities, Hot Water Bath, Slippers / sandals, Connecting Room",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Laundry",
        "Lift/Elevator",
        "Deposit Box",
        "Non-smoking Room",
        "Public-area Wi-Fi",
        "Car Parking",
        "Cable TV"
      ],
      "price_info": {
        "basic_price_per_night": 355709,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 764967.28,
        "basic_price": 355709,
        "fee_and_tax": 0,
        "total_price": 764967,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.79694336477,
        "latitude": -6.260308261542,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 3,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": null,
        "reviewPercentage": null,
        "cleanlinessRating": null,
        "serviceAndStaffRating": null,
        "roomComfortRating": null,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": null,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 355709,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 355709,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 1318,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "POP! Hotel Airport Jakarta",
      "cityName": "Tangerang",
      "address": "Jl. Raya Bandara no.106 Rawa Bokor, Benda, Tangerang",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 370140,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/1318/1-858.jpg",
      "det": null,
      "shortDescription": "POP! Hotel Airport Jakarta adalah properti nyaman yang terletak di Tangerang di pinggir kota, mudah menjangkau Cengkareng Golf Club dan Soewarna Business Park. Hotel ini berada dalam wilayah Mall Ciputra Jakarta dan Central Park Mall.\r\rPOP! Hotel Airport Jakarta adalah akomodasi bebas asap rokok. Fasilitas di hotel ini termasuk Kafetaria, Elevator / Lift, dan Televisi di lobi. Hotel ini menawarkan layanan meja depan 24-jam dan transportasi gratis ke / dari bandara. Untuk kenyamanan para tamu, parkir tersedia tanpa biaya tambahan.",
      "room_info": "Non-Smoking, Wardrobe, AC, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Working Desk, Sofa, Shower Bathroom, Bath Amenities, Hot Water Bath",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Lift/Elevator",
        "Deposit Box",
        "Non-smoking Room",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Car Parking",
        "Cable TV",
        "Airport Transfer"
      ],
      "price_info": {
        "basic_price_per_night": 369440,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 816000,
        "basic_price": 369440,
        "fee_and_tax": 0,
        "total_price": 816000,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.681996,
        "latitude": -6.11633,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 2,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": 3,
        "reviewPercentage": null,
        "cleanlinessRating": 8.3,
        "serviceAndStaffRating": 8,
        "roomComfortRating": 7.3,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": 8.1,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 369440,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 389440,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 2305,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Cordex Hotel Ancol Jakarta",
      "cityName": "Ancol",
      "address": "Lodan Center Complex Blok A/9, Jl. Lodan Raya No. 2, Jakarta Utara",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 221340,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/2305/1-422.jpg",
      "det": null,
      "shortDescription": "Selamat datang di Cordex Hotel Ancol, Jakarta\r\rSebuah hunian terbaru persembahan Omega Hotel Management, yang terletak secara strategis di daerah Ancol, Jakarta. Hanya 15 menit dari keramaian pusat rekreasi Ancol\r\rDilengkapi dengan berbagai fasilitas hotel berbintang, sepert kamar baru dengan pendingin udara (AC), Saluran TV Cable dan koneksi Wi-Fi Internet. Anda akan dimanjakan dengan sebuah kenyamanan tinggal baru di daerah Ancol, Jakarta.",
      "room_info": "AC, Telephone, Wireless/WIFI, Safety Deposit Box, Satellite TV Channels, Bath Amenities",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Business Centre",
        "Lift/Elevator",
        "Concierge",
        "Non-smoking Room",
        "Public-area Wi-Fi",
        "Car Parking",
        "Cable TV"
      ],
      "price_info": {
        "basic_price_per_night": 221340,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 476000,
        "basic_price": 221340,
        "fee_and_tax": 0,
        "total_price": 476000,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.81649,
        "latitude": -6.12982,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 1,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": 2,
        "reviewPercentage": null,
        "cleanlinessRating": 9,
        "serviceAndStaffRating": 9,
        "roomComfortRating": 9,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": 8.5,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 221340,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 221340,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 3674,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Shakti Hotel Jakarta",
      "cityName": "Hayam Wuruk",
      "address": "Komplek Duta Merlin Blok D 3-6 , Jl.Gajah Mada No. 3, Jakarta Pusat",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 118713,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/3674/10-945.jpg",
      "det": null,
      "shortDescription": "Shakti Hotel adalah pilihan yang cerdas bagi para pelancong yang mencari akomodasi terjangkau dengan layanan luar biasa. Kamar-kamar bergaya dan dirancang cerdas menawarkan fasilitas lengkap, dan menyediakan kenyamanan dan kemudahan.\r\rTerletak sangat-strategis di Kompleks Duta Merlin Office, hotel menyediakan akses mudah ke kota Jakarta. Berjalan-jalan dan mengalami distrik bisnis yang ramai di dekatnya. pusat belanja dan hiburan yang hanya beberapa langkah lagi, siap untuk menawarkan pengalaman yang tak terlupakan di kota.\r\rLokasi hotel juga berfungsi sebagai titik pertemuan strategis untuk kegiatan bisnis atau kegiatan sosial. Hotel ini siap memberikan layanan yang lengkap dan fasilitas yang Anda dan rekan Anda butuhkan. Jadi apakah Anda pergi pada liburan atau bisnis, apakah Anda sedang mencari kegembiraan atau relaksasi, Shakti Hotel siap menyambut Anda pada kunjungan berikutnya ke ibukota negara.",
      "room_info": "Non-Smoking, AC, Wireless/WIFI, Safety Deposit Box, Kitchenette, Living Room / Area, Working Desk, Shower Bathroom, Bath Amenities, City View",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Laundry",
        "Lift/Elevator",
        "Concierge",
        "Deposit Box",
        "Non-smoking Room",
        "Restaurant",
        "Room Service",
        "Public-area Wi-Fi",
        "Car Parking",
        "Cable TV",
        "Kitchen"
      ],
      "price_info": {
        "basic_price_per_night": 118712,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 255296,
        "basic_price": 118712,
        "fee_and_tax": 0,
        "total_price": 255296,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.819712,
        "latitude": -6.165101,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 3,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": 4,
        "reviewPercentage": null,
        "cleanlinessRating": 8,
        "serviceAndStaffRating": 7.3,
        "roomComfortRating": 9,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": 8.4,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 118712,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 118712,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 1597,
      "source": "KlikHotel",
      "structureType": "Guest House",
      "csm": null,
      "name": "Venice Guest House Jakarta",
      "cityName": "Pluit",
      "address": "Taman Permata Indah 2 Blok Z1 No. 9, Penjaringan (Teluk Gong Raya), Jakarta Utara",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 186000,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/1597/16-627.jpg",
      "det": null,
      "shortDescription": "Venice Guesthouse adalah tempat yang unik dan nyaman bagi anda yang akan berlibur ke  Jakarta, akses yang mudah, fasilitas yang memadai serta harga yang terjangkau sangat cocok bagi anda yang akan berlibur ke Jakarta.\rVenice Guesthouse adalah tempat yang sempurna untuk liburan yang menyenangkan.",
      "room_info": "Non-Smoking, AC, Local TV Channels, Working Desk, Shower Bathroom, Bath Amenities",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Bar/Lounge",
        "Laundry",
        "Non-smoking Room",
        "Room Service",
        "Public-area Wi-Fi",
        "Car Parking",
        "Balcony",
        "Cable TV",
        "Fax Machine"
      ],
      "price_info": {
        "basic_price_per_night": 186000,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 400000,
        "basic_price": 186000,
        "fee_and_tax": 0,
        "total_price": 400000,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.77806661902,
        "latitude": -6.1390790106227,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 0,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": 2,
        "reviewPercentage": null,
        "cleanlinessRating": 7,
        "serviceAndStaffRating": 6.5,
        "roomComfortRating": 6.5,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": 6.7,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 186000,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 186000,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 2855,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Cordela Senen Jakarta",
      "cityName": "Senen",
      "address": "Jl. Kramat Raya, Jakarta Pusat",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 315456,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/2855/1-223.jpg",
      "det": null,
      "shortDescription": "Selamat datang di Cordela Hotel Senen\r\rSebuah hunian terbaru persembahan Omega Hotel Management, yang terletak secara strategis di daerah Senen, Jakarta. Hanya 10 menit dari keramaian pusat kota.\r\rDilengkapi dengan berbagai fasilitas hotel berbintang, sepert kamar baru dengan pendingin udara (AC), koneksi Wi-Fi Internet, restoran, dan ruang pertemuan. Anda akan dimanjakan dengan sebuah kenyamanan tinggal baru di Senen, Jakarta.",
      "room_info": "Non-Smoking, AC, Telephone, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Kettle, Working Desk, Sofa-bed, Shower Bathroom, Bath Amenities, Slippers / sandals",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Bar/Lounge",
        "Laundry",
        "Business Centre",
        "Lift/Elevator",
        "Deposit Box",
        "Non-smoking Room",
        "Restaurant",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Car Parking",
        "Cable TV",
        "Kitchen",
        "Airport Transfer"
      ],
      "price_info": {
        "basic_price_per_night": 315456,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 678400,
        "basic_price": 315456,
        "fee_and_tax": 0,
        "total_price": 678400,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.84296536381,
        "latitude": -6.1832051320665,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 2,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": 2,
        "reviewPercentage": null,
        "cleanlinessRating": 8,
        "serviceAndStaffRating": 7.5,
        "roomComfortRating": 8,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": 7.8,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 315456,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 315456,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 1940,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Nite & Day Jakarta Roxy",
      "cityName": "Hayam Wuruk",
      "address": "Jl. Biak No.54, Jakarta Pusat",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 276675,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/1940/1-511.jpg",
      "det": null,
      "shortDescription": "Dengan jarak hanya 1 KM dari pusat kota, membuat para tamu memiliki akses mudah untuk menikmati hiburan dikota Jakarta. Nikmati juga akses mudah menuju Jln Hayam Wuruk dan juga Istana Merdeka\n\nMenawarkan layanan superior dengan berbagai fasilitas kepada setiap tamu yang berkunjung, Nite and Day Jakarta Roxy berjanji untuk memberikan kenyamanan kepada tamu semaksimal mungkin. Frontdesk 24 jam, coffee shop, fasilitas pertemuan, Wi-Fi di area publik dan juga setiap kamar hanya merupakan beberapa fasilitas pendunkung yang diberikan NIte and Day Jakarta Roxy dibandingkan dengan hotel-hotel sekitarnya\n\nSuasana dari Nite and Day Jakarta Roxy tercermin dari setiap kamar tamu. Ruang duduk, satelit/TV kabel, TV layar datar/plasma, tersedia juga interconnecting room dan juga kamar mandi shower. Nite and Day Jakarta Roxy adalah pilihan yang tepat bagi anda yang ini berkunjung ke Jakarta, menawarkan penginapan yang nyaman dan tanpa ribet setiap waktunya\n\n*free Early C/i 9 a.m (if any room available",
      "room_info": "Non-Smoking, AC, Wireless/WIFI, Local TV Channels, Satellite TV Channels, Shower Bathroom, Bath Amenities, Hot Water Bath, Slippers / sandals, City View",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Bar/Lounge",
        "Lift/Elevator",
        "Restaurant",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Car Parking",
        "Cable TV"
      ],
      "price_info": {
        "basic_price_per_night": 276589,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 597450,
        "basic_price": 276589,
        "fee_and_tax": 0,
        "total_price": 597450,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.80838981349,
        "latitude": -6.1674846681856,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 2,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": 1,
        "reviewPercentage": null,
        "cleanlinessRating": 8,
        "serviceAndStaffRating": 6,
        "roomComfortRating": 8,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": 7.2,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 276589,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 279039,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 1350,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Whiz Hotel Cikini Jakarta",
      "cityName": "Menteng",
      "address": "Jl. Cikini Raya No 6, Jakarta Pusat",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 409200,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/1350/1-652.jpg",
      "det": null,
      "shortDescription": "Terletak strategis di Jakarta Pusat, Whiz Hotel Cikini memberikan kemudahan akses bagi para tamu yang menginap, mulai dari pusat bisnis di kota Jakarta, Mall-Mall besar, restoran dan cafe, dan juga tempat strategis lain-nya.  Whiz Hotel Cikini dirancang khusus dengan desain modern masa kini yang akan menjamin kenyamanan anda selama menginap.\r\rDilengkapi dengan keamanan 24 jam yang akan menjamin keamanan dan privasi anda.\r\rHotel kami adalah pilihan yang sempurna bagi perjalanan bisnis maupun liburan anda.",
      "room_info": "Non-Smoking, AC, Telephone, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Working Desk, Shower Bathroom, Bath Amenities, City View",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Laundry",
        "Money Changer",
        "Business Centre",
        "Lift/Elevator",
        "Deposit Box",
        "Non-smoking Room",
        "Public-area Wi-Fi",
        "Car Parking",
        "Cable TV"
      ],
      "price_info": {
        "basic_price_per_night": 409200,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 880000,
        "basic_price": 409200,
        "fee_and_tax": 0,
        "total_price": 880000,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.8368369,
        "latitude": -6.188299,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 2,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": 11,
        "reviewPercentage": null,
        "cleanlinessRating": 7.7,
        "serviceAndStaffRating": 6.8,
        "roomComfortRating": 7.3,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": 7.3,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 409200,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 409200,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 387,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Travellers Hotel Jakarta",
      "cityName": "Mangga Dua",
      "address": "Jl. Pangeran Jayakarta No. 70, Jakarta Pusat",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 398040,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/387/1-369.jpg",
      "det": null,
      "shortDescription": "Travellers Hotel Jakarta berlokasi di pusat kota, hanya 15 menit berkendara dari Chinatown yang populer. Hotel ini menawarkan akomodasi modern dengan sebuah spa, parkir gratis, dan akses internet gratis.\r\rKamar-kamar ber-AC-nya dilengkapi dengan minibar, meja kerja, dan TV. Kamar mandi en suite-nya dilengkapi dengan bathtub.\r\rHotel Travellers Jakarta memiliki sebuah pusat kebugaran yang lengkap. Anda dapat menikmati pijat relaksasi di spa, atau bersantai di ruang sauna. Layanan binatu dan dry cleaning juga ditawarkan.\r\rPepito Restaurant menyajikan berbagai macam hidangan lokal dan internasional. QJs Lounge menawarkan minuman dan fasilitas karaoke. Layanan kamar tersedia 24/7.\r\rHotel ini dapat dicapai dalam waktu 15 menit berkendara dari tol Ancol dan 45 menit berkendara dari Bandara Soekarno-Hatta. Jakarta International Expo & Convention Hall di Kemayoran hanya berselang 15 menit berkendara.",
      "room_info": "Non-Smoking, Wardrobe, AC, Telephone, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Hairdryer, Kettle, Mini-Bar, Refrigerator, Working Desk, Sofa, Bath-Tub, Shower Bathroom, Bath Amenities, Hot Water Bath, Slippers / sandals",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Bar/Lounge",
        "Laundry",
        "Fitness Centre",
        "Business Centre",
        "Lift/Elevator",
        "Concierge",
        "Ballroom",
        "Deposit Box",
        "Non-smoking Room",
        "Restaurant",
        "Sauna / Spa",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Conference Rm",
        "Car Parking",
        "Cable TV",
        "Fax Machine",
        "Drug Store",
        "Baby Friendly",
        "Airport Transfer"
      ],
      "price_info": {
        "basic_price_per_night": 398040,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 856000,
        "basic_price": 398040,
        "fee_and_tax": 0,
        "total_price": 856000,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.83156932143,
        "latitude": -6.146783986189,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 3,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": 2,
        "reviewPercentage": null,
        "cleanlinessRating": 8.5,
        "serviceAndStaffRating": 9.5,
        "roomComfortRating": 8,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": 8.2,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 398040,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 398040,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 2107,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Zest Hotel Airport Jakarta",
      "cityName": "Tangerang",
      "address": "Airport Hub, Jl Husein Sastranegara, Kavling 1, Benda Bandara Soekarno Hatta, Tangerang",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 367583,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/2107/1-814.jpg",
      "det": null,
      "shortDescription": "Zest Hotel Airport, Jakarta terletak hanya beberapa menit berkendara dari / ke bandara dan salah satu hotel terdekat ke Bandara Internasional Soekarno-Hatta, yang memungkinkan akses mudah untuk bisnis dan liburan ke pusat bisnis dan pemerintah kabupaten melalui jalan tol.\r\rBerdekatan dengan Mall - Airport Hub dan hotel internasional bintang empat - Swiss-Belhotel Airport, budget hotel bergaya ini menawarkan fasilitas nyaman termasuk minuman, makanan dan pilihan hiburan.",
      "room_info": "AC, Telephone, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Working Desk, Sofa-bed, Shower Bathroom, Bath Amenities, Hot Water Bath, City View",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Laundry",
        "Business Centre",
        "Concierge",
        "Ballroom",
        "Restaurant",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Car Parking",
        "Cable TV",
        "Airport Transfer"
      ],
      "price_info": {
        "basic_price_per_night": 399202,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 858500,
        "basic_price": 399202,
        "fee_and_tax": 0,
        "total_price": 858500,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.6869768,
        "latitude": -6.1137366,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 2,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": 4,
        "reviewPercentage": null,
        "cleanlinessRating": 8.8,
        "serviceAndStaffRating": 8.8,
        "roomComfortRating": 7.5,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": 8,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 399202,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 399202,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 2062,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Hotel GranDhika Iskandarsyah",
      "cityName": "Blok M",
      "address": "Jl. Iskandarsyah No. 65, Jakarta Selatan",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 1290840,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/2062/11-415.jpg",
      "det": null,
      "shortDescription": "Hotel GrandDhika Inskandarsyah terletak strategis di pusat Jakarta Selatan, dikelilingi oleh restoran dan butik. Berdekatan dengan terminal bus yang dengan mudah dapat menghubungkan Anda ke semua wilayah utama kota Jakarta.\r\rNikmati kamar kami dengan desain yang mewah dan warna-warna elegan. Lebih dari 200 kamar yang terdiri dari kamar deluxe nyaman dan kamar executive dengan lantai marmer yang memiliki pemandangan dari balkon ke langit kota Jakarta. \r\rUntuk menambah pengalaman bepergian Anda, staf profesional kami siap membantu setiap kebutuhan Anda 24 jam sehari. Anda akan menikmati pelayanan staff kami yang akan menambah kenyamanan perjalanan bisnis maupun liburan Anda.",
      "room_info": "Non-Smoking, Wardrobe, AC, Telephone, Wireless/WIFI, Safety Deposit Box, Satellite TV Channels, Hairdryer, Kettle, Mini-Bar, Refrigerator, Working Desk, Shower Bathroom, Bath Amenities, Hot Water Bath, Slippers / sandals",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Bar/Lounge",
        "Laundry",
        "Fitness Centre",
        "Business Centre",
        "Lift/Elevator",
        "Concierge",
        "Deposit Box",
        "Non-smoking Room",
        "Cold Pool",
        "Restaurant",
        "Sauna / Spa",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Conference Rm",
        "Car Parking",
        "Cable TV",
        "Fax Machine",
        "Kitchen",
        "Drug Store",
        "Baby Friendly",
        "Airport Transfer"
      ],
      "price_info": {
        "basic_price_per_night": 1313000,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 2776000,
        "basic_price": 1313000,
        "fee_and_tax": 0,
        "total_price": 2776000,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.804005,
        "latitude": -6.244217,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 4,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": null,
        "reviewPercentage": null,
        "cleanlinessRating": null,
        "serviceAndStaffRating": null,
        "roomComfortRating": null,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": null,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 1313000,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 1313000,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 1785,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "dCozie Hotel",
      "cityName": "Blok M",
      "address": "Jl. Sultan Hasanudin No.57, Jakarta Selatan",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 372000,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/1785/1-299.jpg",
      "det": null,
      "shortDescription": "Dcozie Hotel Blok M by Prasanthi terletak di daerah Melawai, Jakarta. Lokasi hotel yang strategis memudahkan anda untuk berkunjung ke 3 tempat perbelanjaan utama didaerah Melawai, diantaranya Mall Blok M Square, Plaza Blok M, dan Mal Pasaraya. Hotel ini juga dekat dengan terminal bus dan juga Bandara Internasional Soekarno Hatta.\r\rSetiap kamar di DCozie Hotel ini didesain dengan furnitur dan interior yang minimalis modern, namun masih memberikan nuansa hangat dan nyaman. Berbagai fasilitas lengkap yang disediakan hotel juga menjadi nilah tambah untuk menjadikan pengalaman menginap anda menjadi lebih menyenangkan.",
      "room_info": "Non-Smoking, AC, Telephone, Wireless/WIFI, Safety Deposit Box, Satellite TV Channels, Kettle, Working Desk, Shower Bathroom, Bath Amenities, Hot Water Bath, Slippers / sandals",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Bar/Lounge",
        "Laundry",
        "Lift/Elevator",
        "Deposit Box",
        "Non-smoking Room",
        "Restaurant",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Car Parking",
        "Cable TV",
        "Drug Store"
      ],
      "price_info": {
        "basic_price_per_night": 372000,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 800000,
        "basic_price": 372000,
        "fee_and_tax": 0,
        "total_price": 800000,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.802299,
        "latitude": -6.242681,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 3,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": 1,
        "reviewPercentage": null,
        "cleanlinessRating": 6,
        "serviceAndStaffRating": 6,
        "roomComfortRating": 6,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": 6.2,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 372000,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 372000,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 1888,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Hotel Maharadja",
      "cityName": "Jakarta Selatan",
      "address": "JL KAPT TENDEAN NO 01 MAMPANG, Jakarta Selatan",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 447059,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/1888/1-434.jpg",
      "det": null,
      "shortDescription": "Terletak di area Mampang yang menyenangkan, Maharadja Hotel memiliki posisi yang sangat bagus di pusat kehidupan malam, berbelanja, melakukan aktivitas bisnis di Jakarta. Dari sini, para tamu dapat menikmati akses mudah ke semua hal yang dapat ditemukan di sebuah kota yang aktif ini. Lingkungan yang terawat dengan baik serta lokasi yang berdekatan dengan Patung Pancoran, Kedutaan Besar Singapura, Kedutaan India memberikan nilai tambah untuk hotel ini.\r\rMaharadja Hotel menawarkan pelayanan sempurna dan segala fasilitas penting seperti area merokok, business center, lift, Wi-fi di tempat umum, Wi-Fi gratis di semua kamar untuk menjamin kenyamanan terbaik bagi para tamu kami.\r\rDisamping itu, hotel memiliki berbagai pilihan fasilitas rekreasi yang menjamin Anda melakukan bermacam hal selama menginap. Apapun tujuan kunjungan Anda, Maharadja Hotel merupakan pilihan yang istimewa untuk menginap di Jakarta.",
      "room_info": "Non-Smoking, Wardrobe, AC, Telephone, Wireless/WIFI, Safety Deposit Box, Satellite TV Channels, Working Desk, Sofa, Shower Bathroom, Hot Water Bath, Slippers / sandals",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Bar/Lounge",
        "Laundry",
        "Lift/Elevator",
        "Concierge",
        "Ballroom",
        "Deposit Box",
        "Non-smoking Room",
        "Restaurant",
        "Sauna / Spa",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Conference Rm",
        "Car Parking",
        "Cable TV",
        "Fax Machine",
        "Kitchen"
      ],
      "price_info": {
        "basic_price_per_night": 447059,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 894118,
        "basic_price": 447059,
        "fee_and_tax": 0,
        "total_price": 894118,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": null,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.824704,
        "latitude": -6.240664,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 3,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": null,
        "reviewPercentage": null,
        "cleanlinessRating": null,
        "serviceAndStaffRating": null,
        "roomComfortRating": null,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": null,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 447059,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 447059,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 483,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Hotel Mega Proklamasi",
      "cityName": "Menteng",
      "address": "Jl. Proklamasi No.40-42, Jakarta Pusat",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 322764,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/483/1-674.jpg",
      "det": null,
      "shortDescription": "Hotel Mega Proklamasi adalah sebuah hunian hotel  yang dirancang dengan suasana rumah yang akan menjamin kenyamanan tinggal Anda ketika Anda membutuhkan sebuah akomodasi ekonomis di Jakarta Pusat.\r\rDengan mengedepankan keramahan, suasana nyaman, dan layanan yang pribadi untuk memastikan Anda senantiasa memiliki alternatif tempat tinggal yang ekonomis dan strategis.",
      "room_info": "Non-Smoking, Alarm Clock, AC, Wireless/WIFI, Local TV Channels, Satellite TV Channels, Working Desk, Sofa, Shower Bathroom, Bath Amenities, Slippers / sandals",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Laundry",
        "Money Changer",
        "Lift/Elevator",
        "Deposit Box",
        "Restaurant",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Car Parking",
        "Cable TV",
        "Fax Machine"
      ],
      "price_info": {
        "basic_price_per_night": 322763,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 694116,
        "basic_price": 322763,
        "fee_and_tax": 0,
        "total_price": 694116,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.84426687235,
        "latitude": -6.2013973339456,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 2,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": 3,
        "reviewPercentage": null,
        "cleanlinessRating": 5.3,
        "serviceAndStaffRating": 7.3,
        "roomComfortRating": 5.3,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": 6.4,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 322763,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 322763,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 1829,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Manhattan Hotel",
      "cityName": "Jakarta Selatan",
      "address": "Prof.Dr.Satrio St. Casablanca, Kuningan, Jakarta Selatan",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 937440,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/1829/2-836.jpg",
      "det": null,
      "shortDescription": "Manhattan Hotel Jakarta\r\rNikmati kemewahan gaya suite di Manhattan Hotel Jakarta. Hotel bisnis mewah ini dirancang sebagai akomodasi modern minimalis dengan ikon Manhattan di New York City. Terletak di jantung Segitiga Emas Jakarta Selatan, di tengah-tengah restoran, pendirian, dan atraksi kota yang terkenal.\r\rTerinspirasi oleh desain perkotaan New York City - Manhattan, arsitektur hotel kami menggabungkan negara modern-minimalis dari seni NYC dan hiruk-pikuk kehidupan kota kota Jakarta. Kami membawa yang terbaik dari dua dunia langsung ke zona kenyamanan Anda.\r\rNikmati berbagai restoran, Freedom Tower Sky Lounge, Columbus Circle Club, spa, salon, kolam renang, sauna, dan bahkan reflexiology. Alami sendiri kenyamanan dan lingkungan modern-minimalis di Manhattan Hotel, hotel yang tidak pernah tidur!",
      "room_info": "In-room Internet Cable, Non-Smoking, Wardrobe, AC, Telephone, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Hairdryer, Kettle, Mini-Bar, Refrigerator, DVD Player, Working Desk, Bath-Tub, Shower Bathroom, Bath Amenities, Hot Water Bath, Slippers / sandals",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Bar/Lounge",
        "Laundry",
        "Fitness Centre",
        "Business Centre",
        "Lift/Elevator",
        "Concierge",
        "Ballroom",
        "Deposit Box",
        "Non-smoking Room",
        "Cold Pool",
        "Restaurant",
        "Sauna / Spa",
        "Hot-tub / Jacuzzi",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Conference Rm",
        "Car Parking",
        "Cable TV",
        "Fax Machine",
        "Kitchen",
        "Drug Store",
        "Airport Transfer"
      ],
      "price_info": {
        "basic_price_per_night": 1053000,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 2256000,
        "basic_price": 1053000,
        "fee_and_tax": 0,
        "total_price": 2256000,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.831137,
        "latitude": -6.225268,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 5,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": null,
        "reviewPercentage": null,
        "cleanlinessRating": null,
        "serviceAndStaffRating": null,
        "roomComfortRating": null,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": null,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 1053000,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 1053000,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 3698,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Yello Hotel Manggarai",
      "cityName": "Menteng",
      "address": "Jl. Minangkabau Timur No. 9, Jakarta Selatan",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 407340,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/3698/1-196.jpg",
      "det": null,
      "shortDescription": "YELLO Hotel Manggarai dilengkapi dengan 102 kamar non-smoking yang trendy dan fasilitas berupa WiFi gratis, gaming station, NetZone, restoran Wok n Tok serta meeting room untuk memberikan Anda pengalaman menginap yang nyaman dan menyenangkan.",
      "room_info": "Non-Smoking, AC, Telephone, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Kettle, Working Desk, Shower Bathroom, Bath Amenities, Hot Water Bath",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Laundry",
        "Lift/Elevator",
        "Non-smoking Room",
        "Restaurant",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Car Parking",
        "Cable TV"
      ],
      "price_info": {
        "basic_price_per_night": 407340,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 876000,
        "basic_price": 407340,
        "fee_and_tax": 0,
        "total_price": 876000,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.845235,
        "latitude": -6.210649,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 3,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": null,
        "reviewPercentage": null,
        "cleanlinessRating": null,
        "serviceAndStaffRating": null,
        "roomComfortRating": null,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": null,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 407340,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 407340,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 1476,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Aston Pluit Hotel & Residence",
      "cityName": "Pluit",
      "address": "Jl. Pluit Selatan No.1, Jakarta Utara",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 661676,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/1476/24-22.jpg",
      "det": null,
      "shortDescription": "Pluit Aston luar biasa ini terletak di jantung kota Pluit dan hanya 20 menit dari Bandara Internasional yang memungkinkan akses mudah ke semua atraksi utama di Jakarta untuk berbelanja dan hiburan. Hotel ini menawarkan kamar dan suite dengan pemandangan cakrawala kota. Rasakan pengalaman restoran terbaik, hiburan, dan perbelanjaan, serta tempat-tempat terbaik kota. Semua dapat diraih dengan berjalan kaki di Pluit.\r\rKedai kopi kami yang mengundang, menawarkan array yang menarik dari hidangan lokal dan import dimana Anda dapat bersantai dan menjelajahi masakan inovatif sambil menikmati angin laut dan atmosfer yang tenang. Tim koki kami yang berdedikasi selalu berusaha membuat setiap pengalaman kuliner menjadi tak terlupakan.\r\rNikmati pelayanan antar jemput bandara gratis dengan jadwal sebagai berikut:\rDari bandara ke hotel: 09.00, 15.00, 20.00.\rDari hotel ke bandara: 07.00, 12.30, 18.00.",
      "room_info": "Non-Smoking, Balcony / Terrace, Wardrobe, AC, Telephone, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Hairdryer, Mini-Bar, Refrigerator, Working Desk, Sofa, Shower Bathroom, Bath Amenities, Hot Water Bath, Slippers / sandals, City View, Swimming Pool View",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Bar/Lounge",
        "Laundry",
        "Fitness Centre",
        "Business Centre",
        "Lift/Elevator",
        "Concierge",
        "Ballroom",
        "Deposit Box",
        "Non-smoking Room",
        "Cold Pool",
        "Restaurant",
        "Sauna / Spa",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Car Parking",
        "Balcony",
        "Cable TV",
        "Fax Machine",
        "Kitchen",
        "Airport Transfer"
      ],
      "price_info": {
        "basic_price_per_night": 661676,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 1422960,
        "basic_price": 661676,
        "fee_and_tax": 0,
        "total_price": 1422960,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.79496861838,
        "latitude": -6.1242499733355,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 4,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": null,
        "reviewPercentage": null,
        "cleanlinessRating": null,
        "serviceAndStaffRating": null,
        "roomComfortRating": null,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": null,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 661676,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 661676,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 4104,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Hotel Neo+ Kebayoran",
      "cityName": "Jakarta Barat",
      "address": "Jl. Cileduk Raya, No. 35, Kebayoran Lama, RT005 / RW002, Jakarta Barat",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 369098,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/4104/1-559.jpg",
      "det": null,
      "shortDescription": "NEO + Kebayoran - Jakarta, menawarkan akomodasi modern dan bergaya dengan layanan terbaik untuk segmentasi pasar kelas pekerja kelas menengah di wilayah bisnis Jakarta Selatan. Lokasi itu sendiri berada diantara pusat perbelanjaan dan akses transportasi yang memudahkan keunggulan geografis. NEO + Kebayoran Jakarta akan disanjung sebagai hotel budget bergaya favorit di daerah ini. Dengan suasana yang menyenangkan, gartify and sociable service, NEO + Kebayoran menawarkan pengalaman menginap di hotel budget kota.",
      "room_info": "In-room Internet Cable, Non-Smoking, Wardrobe, AC, Telephone, Wireless/WIFI, Safety Deposit Box, Satellite TV Channels, Working Desk, Shower Bathroom, Bath Amenities, City View",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Bar/Lounge",
        "Laundry",
        "Business Centre",
        "Lift/Elevator",
        "Concierge",
        "Deposit Box",
        "Non-smoking Room",
        "Restaurant",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Cable TV"
      ],
      "price_info": {
        "basic_price_per_night": 369098,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 793760,
        "basic_price": 369098,
        "fee_and_tax": 0,
        "total_price": 793760,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.778114,
        "latitude": -6.23653,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 3,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": null,
        "reviewPercentage": null,
        "cleanlinessRating": null,
        "serviceAndStaffRating": null,
        "roomComfortRating": null,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": null,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 369098,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 369098,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 1472,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Aston Rasuna Hotel",
      "cityName": "Kuningan",
      "address": "Komplek Apartemen Taman Rasuna, Jl.HR Rasuna Said, Jakarta Selatan",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 962829,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/1472/1-768.jpg",
      "det": null,
      "shortDescription": "Satu dari beberapa Hotel Aston di Jakarta, Aston Rasuna terletak di Jl. HR Rasuna Said Kompleks Rasuna Epicentrum. Dari banyaknya tempat menarik disekitar, sampai fasilitas hotel, kami menjamin Anda mendapatkan masa menginap yang memuaskan di Jakarta. Kami menawarkan 221 kamar tamu mewah dengan suite 1, 2 dan 3 tempat tidur.\r\rSeluruhnya disiapkan secara lengkap dengan kamar tidur, ruang tamu, dapur dan ruang makan, kamar mandi dan balkon pribadi terpisah. Akses ke kamar tamu dengan kartu tamu dan CCTV tersedia di area lobi dan lift. Seluruh suite tersedia baik untuk menginap jangka panjang ataupun jangka pendek. Dengan mempercayakan Blue Bird Taksi tepat disamping hotel, Anda tidak akan kesulitan untuk berkeliling kota.",
      "room_info": "Non-Smoking, Balcony / Terrace, AC, Telephone, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Kettle, Mini-Bar, Kitchen, Kitchenette, Living Room / Area, Dining Room / Table, Working Desk, Sofa, Shower Bathroom, Bath Amenities, Hot Water Bath, Slippers / sandals, City View",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Bar/Lounge",
        "Laundry",
        "Business Centre",
        "Lift/Elevator",
        "Deposit Box",
        "Non-smoking Room",
        "Cold Pool",
        "Restaurant",
        "Sauna / Spa",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Conference Rm",
        "Cable TV",
        "Tennis Court",
        "Airport Transfer"
      ],
      "price_info": {
        "basic_price_per_night": 962829,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 2070600,
        "basic_price": 962829,
        "fee_and_tax": 0,
        "total_price": 2070600,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.836358,
        "latitude": -6.219513,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 4,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": null,
        "reviewPercentage": null,
        "cleanlinessRating": null,
        "serviceAndStaffRating": null,
        "roomComfortRating": null,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": null,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 962829,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 962829,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 1549,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "The Grove Suites Kuningan",
      "cityName": "Kuningan",
      "address": "Kawasan Rasuna Epicentrum, Jl. H.R. Rasuna Said, Kuningan, Jakarta Pusat",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 1235579,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/1549/19-275.jpg",
      "det": null,
      "shortDescription": "The Grove Suites adalah hotel bintang 5 pertama di bawah naungan grup Archipelago Internasional. Terletak sangat strategis di pusat bisnis dan di Jakarta Segitiga Emas, tamu yang menginap akan sangat dimudahkan oleh lokasi hotel kami.\r\r151 Kamar yang tersedia dilengkapi untuk memuaskan keinginan tamu kami yang menginap, baik untuk pebisnis maupun untuk liburan.\r\rThe Grove Suites menyediakan 2 tipe kamar : Suite 1 kamar tidur, dan Suite 2 kamar tidur yang menawarkan kenyamanan yang tidak terhingga. Dan untuk melengkapi kenyamanan-nya lebih lagi kami menawarkan kolam renang yang sangat eksotis, beranbda untuk semi-dining, restoran dengan variasi menu yang sangat beragam, Splash Bar yang menyegarkan, dan 3 ruang rapat.\r\rThe Grove Suites akan memastikan tamu kami memiliki pengalaman menginap yang tidak terlupakan",
      "room_info": "Non-Smoking, Balcony / Terrace, Wardrobe, AC, Telephone, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Hairdryer, Ironing, Mini-Bar, Kitchenette, Living Room / Area, Working Desk, Sofa, Bath-Tub, Shower Bathroom, Bath Amenities, Hot Water Bath, Slippers / sandals, Swimming Pool View",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Bar/Lounge",
        "Laundry",
        "Money Changer",
        "Fitness Centre",
        "Business Centre",
        "Lift/Elevator",
        "Concierge",
        "Ballroom",
        "Cold Pool",
        "Restaurant",
        "Sauna / Spa",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Conference Rm",
        "Car Parking",
        "Cable TV",
        "Fax Machine",
        "Baby Friendly",
        "Airport Transfer"
      ],
      "price_info": {
        "basic_price_per_night": 1253580,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 2657160,
        "basic_price": 1253580,
        "fee_and_tax": 0,
        "total_price": 2657160,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.83419527976,
        "latitude": -6.218271335719,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 5,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": null,
        "reviewPercentage": null,
        "cleanlinessRating": null,
        "serviceAndStaffRating": null,
        "roomComfortRating": null,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": null,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 1253580,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 1253580,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 1389,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "favehotel Pluit Junction",
      "cityName": "Pluit",
      "address": "Jl. Pluit No. 1, Pluit Penjaringan, Jakarta Utara",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 425363,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/1389/1-429.jpg",
      "det": null,
      "shortDescription": "Favehotel Pluit Junction menempati tempat istimewa di jantung Pluit,Mall Pluit Junction yang terkenal membuat semua fasilitas dan restoran di dalam mall mudah diakses oleh tamu Hotel. Hanya 5 menit berkendara menuju Mall Imperium Pluit, dan 15 menit berkendara menuju Mall Taman Anggrek. Sementara Bandara Soekarno-Hatta hanya berjarak 20 menit dari Hotel.\r\rFavehotel Pluit Junction menawarkan layanan dan fasilitas dengan standar tinggi untuk memenuhi kebutuhan individu semua wisatawan. Fasilitas seperti Restoran, Wi-Fi, dan Parkir Mobil tersedia untuk Anda nikmati. Kamar yang segar, bersahabat, dan menyenangkan dilengkapi dengan Televisi LCD besar dan kamar mandi en-suite dengan shower. \r\rNikmati pelayanan antar jemput bandara gratis dengan jadwal sebagai berikut:\rDari bandara ke hotel: 09.00, 15.00, 20.00.\rDari hotel ke bandara: 07.00, 12.30, 18.00.\r\rJika Anda mencari akomodasi yang nyaman di Jakarta, Favehotel Pluit Juction adalah pilihan yang tepat.",
      "room_info": "Non-Smoking, Wardrobe, AC, Telephone, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Working Desk, Shower Bathroom, Bath Amenities, Hot Water Bath",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Laundry",
        "Lift/Elevator",
        "Concierge",
        "Deposit Box",
        "Non-smoking Room",
        "Restaurant",
        "Public-area Wi-Fi",
        "Car Parking",
        "Cable TV",
        "Fax Machine",
        "Airport Transfer"
      ],
      "price_info": {
        "basic_price_per_night": 425364,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 914762,
        "basic_price": 425364,
        "fee_and_tax": 0,
        "total_price": 914762,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.79160454729,
        "latitude": -6.1257959486047,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 3,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": null,
        "reviewPercentage": null,
        "cleanlinessRating": null,
        "serviceAndStaffRating": null,
        "roomComfortRating": null,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": null,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 425364,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 425364,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 482,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Hotel Mega Cikini",
      "cityName": "Menteng",
      "address": "Jl. Cikini Raya Kav. 62-64, Jakarta Pusat",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 322764,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/482/1-823.jpg",
      "det": null,
      "shortDescription": "Hotel Mega Cikini adalah sebuah hotel bintang 2 yang terletak di area Jakarta Pusat. Dengan lokasi yang nyaman, staf yang berdedikasi dan fasilitas berkelas satu, hotel ini adalah akomodasi favorit bagi pelancong bisnis dan wisatawan.\r\rSemua kamar yang berjumlah 119 dilengkapi AC, akses internet nirkabel, dan juga TV. Selain menyediakan layanan kamar 24 jam, kedai kopi, dan restoran, hotel ini juga menyediakan fasilitas rekreasi dan bersantai yang superior, termasuk pijat. Jika Anda memilih Hotel Mega Cikini, hunian yang nyaman berikut pelayanan yang sempurna merupakan jaminannya.",
      "room_info": "Non-Smoking, Wardrobe, AC, Telephone, Wireless/WIFI, Local TV Channels, Satellite TV Channels, Working Desk, Sofa, Shower Bathroom, Bath Amenities, Hot Water Bath, Slippers / sandals",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Laundry",
        "Lift/Elevator",
        "Deposit Box",
        "Non-smoking Room",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Car Parking",
        "Cable TV",
        "Fax Machine"
      ],
      "price_info": {
        "basic_price_per_night": 322763,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 694116,
        "basic_price": 322763,
        "fee_and_tax": 0,
        "total_price": 694116,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.83874934722,
        "latitude": -6.1939259801426,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 2,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": 2,
        "reviewPercentage": null,
        "cleanlinessRating": 6.5,
        "serviceAndStaffRating": 9,
        "roomComfortRating": 7,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": 7.8,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 322763,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 322763,
          "rate": null,
          "promo": null
        }
      ]
    },
    {
      "hotelId": 1154,
      "source": "KlikHotel",
      "structureType": "Hotel",
      "csm": null,
      "name": "Sofyan Hotel Cut Meutia",
      "cityName": "Menteng",
      "address": "Jl. Cut Mutia No 9, Menteng, Jakarta Pusat",
      "postalCode": null,
      "airportCode": null,
      "currency": "IDR",
      "price": 418500,
      "nonRefundable": null,
      "freeCancellation": null,
      "freeCancelDate": null,
      "thumbNailUrl": null,
      "largeThumbnailURL": "https://static.klikhotel.com/klik/database/property/gallery/1154/19-934.jpg",
      "det": null,
      "shortDescription": "Terletak di daerah Menteng Jakarta. Hotel Sofyan Betawi adalah tempat ideal untuk memulai eksplorasi di Jakarta. Hanya 3 KM dari pusat kota, hotel bintang 3 ini memiliki lokasi yang sangat baik dan menyediakan akses ke berbagai tempat. Hanya 35 menit berkendara dari Bandara Internasional Soekarno-Hatta.\r\rSesuai dengan Visi dan Misi kami, Hotel yang dikelola dengan Prinsp Syariah Islam dengan tidak mengurangi rasa hormat, kami tidak dapat menerima pasangan yang bukan Suami Istri.  Untuk kunjungan tamu di anjurkan diterima di Lobby atau di restaurant. \r\rFasilitas dan pelayanan oleh Sofyan Hotel Betawi menjaminkan penginapan menyenangkan bagi para tamu. Hotel ini menyediakan akses ke sejumlah layanan termasuk layanan laundry, Pusat Kebugaran, Pusat Bisnis, Restoran, Layanan kamar, Wi-Fi, Ruang Rapat, tempat parkir mobil.\r\rHotel Sofyan Betawi adalah tempat yang sempurna untuk liburan yang menyenangkan.",
      "room_info": "In-room Internet Cable, Non-Smoking, Wardrobe, AC, Telephone, Wireless/WIFI, Safety Deposit Box, Local TV Channels, Satellite TV Channels, Kettle, Mini-Bar, Refrigerator, Working Desk, Sofa, Shower Bathroom, Bath Amenities",
      "currentAllotment": null,
      "supplierType": null,
      "rateType": null,
      "promoId": null,
      "promoDescription": null,
      "amenities": [
        "Frontdesk 24h",
        "Bar/Lounge",
        "Laundry",
        "Fitness Centre",
        "Business Centre",
        "Lift/Elevator",
        "Concierge",
        "Deposit Box",
        "Non-smoking Room",
        "Restaurant",
        "Room Service",
        "Public-area Wi-Fi",
        "Meeting Room",
        "Conference Rm",
        "Car Parking",
        "Cable TV",
        "Fax Machine"
      ],
      "price_info": {
        "basic_price_per_night": 418500,
        "fee_and_tax_per_night": 0,
        "total_price_per_night": 900000,
        "basic_price": 418500,
        "fee_and_tax": 0,
        "total_price": 900000,
        "nightlyRateTotal": null,
        "averageBaseRate": null,
        "averageRate": null
      },
      "drr_info": {
        "priceWithoutDrr": 0,
        "drrMessage": null,
        "surveyPriceLow": null,
        "surveyPriceHigh": null
      },
      "location_info": {
        "locationDescription": null,
        "longitude": 106.8339555,
        "latitude": -6.187507,
        "distanceInMiles": null,
        "distanceInKilometers": null
      },
      "review_rating": {
        "starRating": 3,
        "reviewImage": null,
        "reviewOverall": null,
        "reviewTotal": 1,
        "reviewPercentage": null,
        "cleanlinessRating": 4,
        "serviceAndStaffRating": 5,
        "roomComfortRating": 3,
        "hotelConditionRating": null,
        "confidenceRating": null,
        "superlativeMessage": null,
        "hotelRating": 5.5,
        "tripAdvisorRatingUrl": null,
        "tripAdvisorRating": null,
        "triAdvisorReviewCount": null
      },
      "nightly_rates": [
        {
          "baseRate": 418500,
          "rate": null,
          "promo": null
        },
        {
          "baseRate": 418500,
          "rate": null,
          "promo": null
        }
      ]
    }
  ],
  "current_page": 1,
  "total_page": 2
}';
       
        return $data;
    }
	
}