<?php

class wslogreg
{	
	public $app_url = "http://pacifictravel.co.id/agent/";
    function __construct()
    {
		$ci =& get_instance();
		#$ci->load->library('send');
    }
    
	function index($action,$parameter)
	{
		switch($action)
		{
			case "login_process";
				$result = $this->login_process($parameter);
				break;
			case "register_process";
				$result = $this->register_process($parameter);
				break;
			case "profile_update";
				$result = $this->profile_update($parameter);
				break;
			case "password_change";
				$result = $this->password_change($parameter);
				break;
			case "forgot_password";
				$result = $this->forgot_password($parameter);
				break;
			case "business_scheme";
				$result = $this->get_business_scheme($parameter);
				break;
			case "add_corporate_user";
				$result = $this->add_corporate_user($parameter);
				break;
			case "upload_corporate_logo";
				$result = $this->upload_corporate_logo($parameter);
				break;
			case "confirm_registration";
				$result = $this->confirm_registration($parameter);
				break;
			case "get_branch";
				$result = $this->get_branch($parameter);
				break;
			case "send_referal";
				$result = $this->send_referal($parameter);
				break;
			case "update_status_corporate_user";
				$result = $this->update_status_corporate_user($parameter);
				break;
			default:
                $result = array('rest_no'=>"103",'reason'=>"Function tidak terdaftar");
            break;
		}
		return $result;
	}
	
	public function login_process($param) {
		$this->ci =& get_instance();
		
		$user = @$param['email'];
		$pass = @$param['password'];
		$pass_conv = $this->ci->converter->encode($pass);
		
		$data 		= $this->ci->mgeneral->getWhere(array("email"=>$user), 'mstr_partner');
		$id 		= $data[0]->partner_id;
		$bank 		= $this->ci->mgeneral->getWhere(array("partner_id"=>$id), 'mstr_partner_bank');
		
		if (count($data) == 0) {
			$result["rest_no"] = "10";
			$result["reason"] = getMobileErrorText("10");
		} else if ($data[0]->password != $pass_conv) {
			$result["rest_no"] = "11";
			$result["reason"] = getMobileErrorText("11");
		}
		else if ($data[0]->partner_status == "tidak_aktif") {
			$result["rest_no"] = "21";
			$result["reason"] = getMobileErrorText("21");
		}
		else if ($data[0]->partner_status == "pending") {
			$result["rest_no"] 			= "717";
			$result["reason"] 			= "status akun anda pending";
			$result['status']			= "pending";
			$result['partner_id']		= $this->ci->converter->encode($id);
			$result['partner_level']	= $this->ci->mgeneral->getValue("level_name",array('skema_bisnis_id'=>$data[0]->skema_bisnis_id, "level_code" => $data[0]->partner_level),"mstr_partner_level");
		}
		else {
			if($data[0]->skema_bisnis_id == 1){
				$date_limit = " + 3 days";
			}
			else{
				$date_limit = " + 10 years";	
			}
			$time_limit = date("Y-m-d H:i:s", strtotime($data[0]->date_added .$date_limit));
			
			if(time() < strtotime($time_limit)){
				$result["rest_no"] = "0";
				
				foreach($data as $d):
					$skema_name	 	= $this->ci->mgeneral->getValue("skema_bisnis_name",array('skema_bisnis_id'=>$d->skema_bisnis_id),"skema_bisnis");
					$partner_level	= $this->ci->mgeneral->getValue("level_name",array('skema_bisnis_id'=>$d->skema_bisnis_id, "level_code" => $d->partner_level),"mstr_partner_level");
					$user_info = array('partner_id'		=> $this->ci->converter->encode($d->partner_id),
										 'register_date'	=> $d->date_added,
										 'first_name'		=> $d->first_name,
										 'last_name'		=> $d->last_name,
										 'email'			=> $d->email,
										 'mobile_number'	=> $d->mobile_number,
										 'phone_number'		=> $d->phone_number,
										 'address'			=> $d->address,
										 'postal_code'		=> $d->postal_code,
										 'business_name'	=> $d->business_name,
										 'business_address'	=> $d->business_address,
										 'current_deposit'	=> $d->current_deposit,
										 'skema_bisnis'		=> $skema_name,
										 'partner_level'	=> $partner_level);
					
				endforeach;

				$result['data']["user_info"] = $user_info;

				if($data[0]->skema_bisnis_id == "3" && $data[0]->partner_level == "1"){
					$corporate_data = $this->ci->mgeneral->getWhere(array("corporate_id" => $data[0]->corporate_id), "mstr_corporate");
					$result['data']["corporate_info"] 	= array("corporate_id"		=> $this->ci->converter->encode($data[0]->corporate_id),
																"corporate_logo"	=> ($corporate_data[0]->logo) ? base_url()."uploads/corp_logo/".$corporate_data[0]->logo : "");
					
				}
				else if($data[0]->skema_bisnis_id == "4" && $data[0]->partner_level == "1"){
					$corporate_data = $this->ci->mgeneral->getWhere(array("corporate_id" => $data[0]->corporate_id), "mstr_corporate");
					$branch_data = $this->ci->mgeneral->getWhere(array("branch_id" => $data[0]->branch_id), "mstr_branch");
					$result['data']["corporate_info"] 	= array("corporate_id"		=> $this->ci->converter->encode($data[0]->corporate_id),
																"corporate_logo"	=> ($corporate_data[0]->logo) ? base_url()."uploads/corp_logo/".$corporate_data[0]->logo : "");

					$result['data']["branch_info"]	= array("branch_id"		=> $this->ci->converter->encode($data[0]->branch_id),
															"branch_code"	=> $branch_data[0]->branch_code);
				}
				
				foreach($bank as $d):
					$banklist[] = array('bank_partner_id'	=> $this->ci->converter->encode($d->partner_bank_id),
										'bank_name'			=> $d->bank_name,
										'bank_akun_no'		=> $d->bank_account_no,
										'bank_akun_holder'	=> $d->bank_account_holder);
				endforeach;
				$result['data']["bank_account"] = $banklist;
			}
			else{
				$result["rest_no"] 	= "21";
				$result["reason"] 	= getMobileErrorText("21");
				$this->ci->mgeneral->update(array("partner_id" => $id), array("partner_status" => "tidak_aktif"), "mstr_partner");
			}
		}
		return  $result;
	}
	
	public function register_process($param) {
		$this->ci =& get_instance();
		
		$firstname 			= @$param['first_name'];
		$lastname 			= @$param['last_name'];
		$address 			= @$param['address'];
		$country 			= @$param['country'];
		$post_code 			= @$param['post_code'];
		$business_name 		= @$param['business_name'];
		$business_address 	= @$param['business_address'];
		$mobile_phone		= @$param['mobile_number'];
		$phone_number		= @$param['phone_number'];
		$pass 				= @$param['password'];
		$pass_confirm 		= @$param['password_confirm'];
		$business_scheme 	= @$param['business_scheme'];
		$email 				= @$param['email'];
		$branch_code 		= @$param['branch_id'];

		#ubah +62 dan 62 jadi 0
		$mobile_phone 	= preg_replace('/^(\+62|62|0)?/', "0", $mobile_phone);
		
		$cek_email 		= $this->ci->mgeneral->getWhere(array('email'=>$email),'mstr_partner');
		$cek_skema 		= $this->ci->mgeneral->getWhere(array('skema_bisnis_id'=>$this->ci->converter->decode($business_scheme)),'skema_bisnis');
		if($branch_code)
			#$cek_branch 		= $this->ci->mgeneral->getWhere(array('branch_code'=>$branch_code),'mstr_branch');
			$cek_branch 		= $this->ci->mgeneral->getWhere(array('branch_id'=>$this->ci->converter->decode($branch_code)),'mstr_branch');
		if ($email == "") {
			#dikarenakan email di isi dengan phone_number
			$result["rest_no"] = "17";
			$result["reason"] = getMobileErrorText($result["rest_no"]);
			#$result["rest_no"] = "17";
			#$result["reason"] = getMobileErrorText($result["rest_no"]);
		} 
		else if ($pass == "") {
			#dikarenakan email di isi dengan phone_number
			$result["rest_no"] = "18";
			$result["reason"] = getMobileErrorText($result["rest_no"]);
			#$result["rest_no"] = "17";
			#$result["reason"] = getMobileErrorText($result["rest_no"]);
		} 
		else if (count($cek_email) > 0) {
			$result["rest_no"] = "20";
			$result["reason"] = getMobileErrorText($result["rest_no"]);
		}
		else if (count($cek_skema) == 0 || $business_scheme == "") {
			$result["rest_no"] = "22";
			$result["reason"] = getMobileErrorText($result["rest_no"]);
		}
		else if ($branch_code && count($cek_branch) == 0) {
			$result["rest_no"] = "22";
			$result["reason"] = "branch_code tidak ditemukan";
		}
		else if ($pass_confirm != $pass) {
			$result["rest_no"] = "28";
			$result["reason"] = getMobileErrorText($result["rest_no"]);
		}
		else {
			
			$en_pass = $this->ci->converter->encode($pass);

			#save data partner
			$prof_data['email'] 			= $email;
			$prof_data['first_name'] 		= $firstname;
			$prof_data['last_name'] 		= $lastname;
			$prof_data['address'] 			= $address;
			$prof_data['country_id'] 		= $country;
			$prof_data['postal_code'] 		= $post_code;
			$prof_data['mobile_number'] 	= $mobile_phone;
			$prof_data['phone_number'] 		= $phone_number;
			$prof_data['business_name'] 	= $business_name;
			$prof_data['business_address'] 	= $business_address;
			$prof_data['password'] 			= $en_pass;
			$prof_data['partner_status'] 	= $cek_skema[0]->status_after_registration;
			$prof_data['skema_bisnis_id'] 	= $cek_skema[0]->skema_bisnis_id;
			$prof_data['partner_level'] 	= "1";
			$prof_data['komisi'] 			= $cek_skema[0]->skema_bisnis_komisi;
			$prof_data['branch_id'] 		= (count($cek_branch) > 0) ? $cek_branch[0]->branch_id : 0;

			$id = $this->ci->mgeneral->save($prof_data, 'mstr_partner');

			if($cek_skema[0]->skema_bisnis_id == 3){
				$corp_data 	= array("max_user"		=> 10);
				$corp_id 	= $this->ci->mgeneral->save($corp_data, 'mstr_corporate');
				$arr_update = array("corporate_id"	=> $corp_id);
				$this->ci->mgeneral->update(array("partner_id" => $id), $arr_update, 'mstr_partner');
			}
			else if($cek_skema[0]->skema_bisnis_id == 4){
				$corp_data 	= array("max_user"		=> 10);
				$corp_id 	= $this->ci->mgeneral->save($corp_data, 'mstr_corporate');

				$branch_data	= array("cashback"		=> 10);

				$branch_id 	= $this->ci->mgeneral->save($branch_data, 'mstr_branch');

				$arr_update = array("corporate_id"	=> $corp_id, "branch_id" => $branch_id);
				$this->ci->mgeneral->update(array("partner_id" => $id), $arr_update, 'mstr_partner');

				$this->ci->mgeneral->update(array("branch_id" => $branch_id), array("branch_code" => "PTCA".$branch_id), 'mstr_branch');
			}

			$data = $this->ci->mgeneral->getWhere(array('partner_id'=>$id),'mstr_partner');
			$result["rest_no"] = "0";
			
			foreach($data as $d):
				$skema_name	 	= $this->ci->mgeneral->getValue("skema_bisnis_name",array('skema_bisnis_id'=>$data[0]->skema_bisnis_id),"skema_bisnis");
				$partner_level	= $this->ci->mgeneral->getValue("level_name",array('skema_bisnis_id'=>$d->skema_bisnis_id, "level_code" => $d->partner_level),"mstr_partner_level");
				$user_info = array('partner_id'		=> $this->ci->converter->encode($d->partner_id),
									 'register_date'	=> $d->date_added,
									 'first_name'		=> $d->first_name,
									 'last_name'		=> $d->last_name,
									 'email'			=> $d->email,
									 'mobile_number'	=> $d->mobile_number,
									 'phone_number'		=> $d->phone_number,
									 'address'			=> $d->address,
									 'postal_code'		=> $d->postal_code,
									 'business_name'	=> $d->business_name,
									 'business_address'	=> $d->business_address,
									 'current_deposit'	=> $d->current_deposit,
									 'skema_bisnis'		=> $skema_name,
									 'partner_level'	=> $partner_level);
				
			endforeach;

			$result['data']["user_info"] = $user_info;

			if($data[0]->skema_bisnis_id == "3" && $data[0]->partner_level == "1"){
				$corporate_data = $this->ci->mgeneral->getWhere(array("corporate_id" => $data[0]->corporate_id), "mstr_corporate");
				$result['data']["corporate_info"] 	= array("corporate_id"		=> $this->ci->converter->encode($data[0]->corporate_id),
															"corporate_logo"	=> ($corporate_data[0]->logo) ? base_url()."uploads/corp_logo/".$corporate_data[0]->logo : "");
				
			}
			else if($data[0]->skema_bisnis_id == "4" && $data[0]->partner_level == "1"){
				$corporate_data = $this->ci->mgeneral->getWhere(array("corporate_id" => $data[0]->corporate_id), "mstr_corporate");
				$branch_data = $this->ci->mgeneral->getWhere(array("branch_id" => $data[0]->branch_id), "mstr_branch");
				$result['data']["corporate_info"] 	= array("corporate_id"		=> $this->ci->converter->encode($data[0]->corporate_id),
															"corporate_logo"	=> ($corporate_data[0]->logo) ? base_url()."uploads/corp_logo/".$corporate_data[0]->logo : "");

				$result['data']["branch_info"]	= array("branch_id"		=> $this->ci->converter->encode($data[0]->branch_id),
														"branch_code"	=> $branch_data[0]->branch_code);
			}
			
			/*edit subject dan content belum dilakukan*/
			#$content		= "Terima kasih telah melakukan registrasi di Pacific Travel. <br/>Silakan klik link berikut untuk melakukan aktivasi. ".$this->app_url."activation/";
			if($cek_skema[0]->skema_bisnis_id == '1'){
$content		= '<p>Terima kasih telah melakukan registrasi di Pacific Travel.</p>
<p>Harap perhatikan bahwa status akun Anda saat ini adalah <b>TIDAK AKTIF</b>. Anda dapat mengaktifkan status akun Anda menekan tombol '.$this->app_url.'activation/
    Masukan alamat email dan password yang anda isikan pada saat regitrasi. Kemudian klik "aktifasi" untuk melakukan aktifasi</p>
<p>Terima kasih atas ketertarikan Anda untuk bergabung dengan pacifictravel.  </p>
<p>Salam Hormat Kami, </p>
<p>Pacifictravel Sistem Admin</p>
';
			}
			else{
			$content		= '<p>Terima kasih telah melakukan registrasi di Pacific Travel.</p>
<p>Harap perhatikan bahwa status akun Anda saat ini adalah <b>TIDAK AKTIF</b>. Anda dapat mengaktifkan status akun Anda dengan membayar biaya pendaftaran (Joining Fee BISNIS) yang berlaku selamanya.  </p>
<p>Bergabung dengan BISNIS BISA dapat dilakukan dengan dua cara: </p>
<ol>
	<li><b>PEMBAYARAN LANGSUNG</b>
    <p>Dengan klik tombol '.$this->app_url.'activation/
    Masukan alamat email dan password yang anda isikan pada saat regitrasi. Kemudian pilih "Payment" untuk melakukan pembayaran secara langsung</p>
    </li>
    <li><b>TRANSFER BANK</b>
    	<p>Silahkan transfer ke salah satu akun di bawah ini : </p>
    	<ul>
        	<li>BANK MANDIRI
            	<p>Nama Akun : Gunawan Margaputra <br>Nomor Rekening: 164 000 2111 823</p>
            </li>
            <li>BANK BCA
            	<p>Nama Akun: Gunawan Margaputra <BR> Nomor Rekening: 880 077 6059  </p>
            </li>
            <li>BANK BCA
            	<p>Nama Akun: PT Surya Cipta Pacific <br> Nomor Rekening: 0123007414  </p>
            </li>
        </ul>
        <p>Setelah transfer mohon konfirmasikan melalui telepon ke Pacific Travel (021 25558841), Fahrul (085814871598).</p>
    </li>
</ol>
<p>Terima kasih atas ketertarikan Anda untuk bergabung dengan pacifictravel.  </p>
<p>Salam Hormat Kami, </p>
<p>Pacifictravel Sistem Admin</p>
<small><i>Catatan: Transfer di Bank yang sama biasanya akan mengambil 1 menit tercepat di hari kerja, pada akhir pekan dan penundaan hari changeover biasanya akan terjadi. Karena itu, jika ada penundaan dalam hal transfer, mohon tunggu beberapa menit agar Bank memproses transfer Anda. Perhatian: E-mail ini dibuat oleh sistem, jangan balas ke alamat pengirim</i></small>
';	
			}
			
			$subject 		= "Registrasi Pacific Travel";
			
			$this->ci->load->library("send");
			$this->ci->send->email($email, $subject, $content);		
		}

		return  $result;
	}
	
	public function profile_update($param) {
		$this->ci =& get_instance();
		
		$firstname 			= @$param['first_name'];
		$lastname 			= @$param['last_name'];
		$address 			= @$param['address'];
		$country 			= @$param['country'];
		$post_code 			= @$param['post_code'];
		$business_name 		= @$param['business_name'];
		$business_address 	= @$param['business_address'];
		$mobile_number		= @$param['mobile_number'];
		$phone_number		= @$param['phone_number'];
		$partner_id 		= $this->ci->converter->decode(@$param['partner_id']);
		
		if ($param['partner_id'] == "") {
			$result["rest_no"] = "30";
			$result["reason"] = getMobileErrorText($result["rest_no"]);
		} else {
			
			#update data partner
			$prof_data['first_name'] 		= $firstname;
			$prof_data['last_name'] 		= $lastname;
			$prof_data['address'] 			= $address;
			$prof_data['country_id'] 		= $country;
			$prof_data['postal_code'] 		= $post_code;
			$prof_data['mobile_number'] 	= $mobile_number;
			$prof_data['phone_number'] 		= $phone_number;
			$prof_data['business_name'] 	= $business_name;
			$prof_data['business_address'] 	= $business_address;
			$this->ci->mgeneral->update(array('partner_id'=>$partner_id), $prof_data, 'mstr_partner');
			
			$data 		= $this->ci->mgeneral->getWhere(array("partner_id"=>$partner_id), 'mstr_partner');
			$bank 		= $this->ci->mgeneral->getWhere(array("partner_id"=>$partner_id), 'mstr_partner_bank');
			$result["rest_no"] = "0";
				
			foreach($data as $d):
				$skema_name	 	= $this->ci->mgeneral->getValue("skema_bisnis_name",array('skema_bisnis_id'=>$d->skema_bisnis_id),"skema_bisnis");
				$partner_level	= $this->ci->mgeneral->getValue("level_name",array('skema_bisnis_id'=>$d->skema_bisnis_id, "level_code" => $d->partner_level),"mstr_partner_level");
				$user_info = array('partner_id'		=> $this->ci->converter->encode($d->partner_id),
									 'register_date'	=> $d->date_added,
									 'first_name'		=> $d->first_name,
									 'last_name'		=> $d->last_name,
									 'email'			=> $d->email,
									 'mobile_number'	=> $d->mobile_number,
									 'phone_number'		=> $d->phone_number,
									 'address'			=> $d->address,
									 'postal_code'		=> $d->postal_code,
									 'business_name'	=> $d->business_name,
									 'business_address'	=> $d->business_address,
									 'current_deposit'	=> $d->current_deposit,
									 'skema_bisnis'		=> $skema_name,
									 'partner_level'	=> $partner_level);
				
			endforeach;

			$result['data']["user_info"] = $user_info;

			if($data[0]->skema_bisnis_id == "3" && $data[0]->partner_level == "1"){
				$corporate_data = $this->ci->mgeneral->getWhere(array("corporate_id" => $data[0]->corporate_id), "mstr_corporate");
				$result['data']["corporate_info"] 	= array("corporate_id"		=> $this->ci->converter->encode($data[0]->corporate_id),
															"corporate_logo"	=> ($corporate_data[0]->logo) ? base_url()."uploads/corp_logo/".$corporate_data[0]->logo : "");
				
			}
			else if($data[0]->skema_bisnis_id == "4" && $data[0]->partner_level == "1"){
				$corporate_data = $this->ci->mgeneral->getWhere(array("corporate_id" => $data[0]->corporate_id), "mstr_corporate");
				$branch_data = $this->ci->mgeneral->getWhere(array("branch_id" => $data[0]->branch_id), "mstr_branch");
				$result['data']["corporate_info"] 	= array("corporate_id"		=> $this->ci->converter->encode($data[0]->corporate_id),
															"corporate_logo"	=> ($corporate_data[0]->logo) ? base_url()."uploads/corp_logo/".$corporate_data[0]->logo : "");

				$result['data']["branch_info"]	= array("branch_id"		=> $this->ci->converter->encode($data[0]->branch_id),
														"branch_code"	=> $branch_data[0]->branch_code);
			}
			
			foreach($bank as $d):
				$banklist[] = array('bank_partner_id'	=> $this->ci->converter->encode($d->partner_bank_id),
									'bank_name'			=> $d->bank_name,
									'bank_akun_no'		=> $d->bank_account_no,
									'bank_akun_holder'	=> $d->bank_account_holder);
			endforeach;
			$result['data']["bank_account"] = $banklist;
			
		}
		return  $result;
	}
	
	public function password_change($param) {
		$this->ci =& get_instance();
	
		$partner_id 		= $this->ci->converter->decode(@$param['partner_id']);
		$old_pass 			= @$param['old_password'];
		$new_pass 			= @$param['new_password'];
		$new_pass_confirm 	= @$param['new_password_confirm'];
		$en_old_pass 		= $this->ci->converter->encode($old_pass);
		$info_user 			= $this->ci->mgeneral->getWhere(array('partner_id'=> $partner_id),'mstr_partner');
		
		if ($param['partner_id'] == "") {
			$result["rest_no"] = "30";
			$result["reason"] = getMobileErrorText($result["rest_no"]);
		} else if ($old_pass == "") {
			$result["rest_no"] = "24";
			$result["reason"] = getMobileErrorText($result["rest_no"]);
		} else if ($info_user[0]->password != $en_old_pass) {
			$result["rest_no"] = "26";
			$result["reason"] = getMobileErrorText($result["rest_no"]);
		} else if ($new_pass == "") {
			$result["rest_no"] = "25";
			$result["reason"] = getMobileErrorText($result["rest_no"]);
		} else if (strlen($new_pass) < 5) {
			$result["rest_no"] = "27";
			$result["reason"] = getMobileErrorText($result["rest_no"]);
		} else if ($new_pass_confirm != $new_pass) {
			$result["rest_no"] = "28";
			$result["reason"] = getMobileErrorText($result["rest_no"]);
		} else if ($new_pass == $old_pass) {
			$result["rest_no"] = "29";
			$result["reason"] = getMobileErrorText($result["rest_no"]);
		} else {
			$en_pass = $this->ci->converter->encode($new_pass_confirm);
			$data = $this->ci->mgeneral->update(array('partner_id'=>$partner_id),array('password'=>$en_pass), 'mstr_partner');
			
			$result["rest_no"] = "0";
			$result["info"] 	= "Password Baru berhasil disimpan";
		}
	
		return  $result;
	}

	public function forgot_password($param) {
		$this->ci =& get_instance();
		$email = @$param['email'];
		$cek_user = $this->ci->mgeneral->getWhere(array('email'=> $email),'mstr_partner');
		
		if (count($cek_user) == 0) {
			$result["rest_no"] = "10";
			$result["reason"] = getMobileErrorText($result["rest_no"]);
		} else {

			$this->ci->load->library("send");
			$password 	= $this->ci->converter->decode($cek_user[0]->password);
			$msg 		= "berikut ini adalah password anda. password : ".$password." harap simpan informasi ini";
			$res 		= $this->ci->send->email($email, "Forgot Password", $msg);		
			
			$result["rest_no"] = "0";
			$result["data"]["message"] = "Kami telah mengirimkan informasi password ke Email Anda";
		}
	
		return  $result;
	}

	public function get_business_scheme($param){
		$this->ci =& get_instance();

		$data = $this->ci->mgeneral->getAll("skema_bisnis");

		$new_data = array();
		foreach($data as $d){
			$new_data[]	= array("id"	=> $this->ci->converter->encode($d->skema_bisnis_id),
								"name"	=> $d->skema_bisnis_name);
		}

		$result = array("rest_no" 	=> "0",
						"data"		=> array("business_scheme_list" => $new_data));

		return $result;
	}

	public function add_corporate_user($param) {
		$this->ci =& get_instance();
		
		$firstname 			= @$param['first_name'];
		$lastname 			= @$param['last_name'];
		$address 			= @$param['address'];
		$country 			= @$param['country'];
		$post_code 			= @$param['post_code'];
		$business_name 		= @$param['business_name'];
		$business_address 	= @$param['business_address'];
		$mobile_phone		= @$param['mobile_number'];
		$phone_number		= @$param['phone_number'];
		$pass 				= @$param['password'];
		$pass_confirm 		= @$param['password_confirm'];
		$email 				= @$param['email'];
		$partner_id 		= $this->ci->converter->decode(@$param['partner_id']);
		$corporate_id 		= $this->ci->converter->decode(@$param['corporate_id']);
		$partner_level2		= $param['partner_level'];
		#ubah +62 dan 62 jadi 0
		$mobile_phone 	= preg_replace('/^(\+62|62|0)?/', "0", $mobile_phone);
		
		$cek_email 		= $this->ci->mgeneral->getWhere(array('email'=>$email),'mstr_partner');
		$cek_user 		= $this->ci->mgeneral->getWhere(array('partner_id'=>$partner_id, "corporate_id" => $corporate_id),'mstr_partner');
		$cek_corporate 	= $this->ci->mgeneral->getWhere(array('corporate_id'=>$corporate_id),'mstr_corporate');
		$cek_member 	= $this->ci->mgeneral->getWhere(array('corporate_id'=>$corporate_id),'mstr_partner');
		if ($email == "") {
			#dikarenakan email di isi dengan phone_number
			$result["rest_no"] = "17";
			$result["reason"] = getMobileErrorText($result["rest_no"]);
			#$result["rest_no"] = "17";
			#$result["reason"] = getMobileErrorText($result["rest_no"]);
		} 
		else if ($pass == "") {
			#dikarenakan email di isi dengan phone_number
			$result["rest_no"] = "18";
			$result["reason"] = getMobileErrorText($result["rest_no"]);
			#$result["rest_no"] = "17";
			#$result["reason"] = getMobileErrorText($result["rest_no"]);
		}
		else if ($partner_id == "") {
			#dikarenakan email di isi dengan phone_number
			$result["rest_no"] 	= "17";
			$result["reason"] 	= "partner_id tidak boleh kosong";
		} 
		else if ($corporate_id == "") {
			#dikarenakan email di isi dengan phone_number
			$result["rest_no"] 	= "17";
			$result["reason"] 	= "corporate_id tidak boleh kosong";
		} 
		else if (count($cek_email) > 0) {
			$result["rest_no"] = "20";
			$result["reason"] = getMobileErrorText($result["rest_no"]);
		}
		else if (count($cek_user) == 0) {
			$result["rest_no"] = "30";
			$result["reason"] = getMobileErrorText("30");
		}
		else if (count($cek_corporate) == 0) {
			$result["rest_no"] 	= "30";
			$result["reason"] 	= "Corporate tidak terdeteksi";
		}
		else if ($cek_user[0]->partner_level != 1){
			$result["rest_no"] 	= "30";
			$result["reason"] 	= "hanya corporate admin yang bisa menambahkan user";
		}
		else if (count($cek_member) > $cek_corporate[0]->max_user){
			$result["rest_no"] 	= "30";
			$result["reason"] 	= "jumlah member sudah penuh";
		}
		else if ($pass_confirm != $pass) {
			$result["rest_no"] = "28";
			$result["reason"] = getMobileErrorText($result["rest_no"]);
		}
		else {
			$cek_partner_level = $this->ci->mgeneral->getWhere(array("skema_bisnis_id" => $cek_user[0]->skema_bisnis_id, "level_name" => $partner_level2), "mstr_partner_level");

			if(count($cek_partner_level) > 0){
				$en_pass = $this->ci->converter->encode($pass);

				#save data partner
				$prof_data['email'] 			= $email;
				$prof_data['first_name'] 		= $firstname;
				$prof_data['last_name'] 		= $lastname;
				$prof_data['address'] 			= $address;
				$prof_data['country_id'] 		= $country;
				$prof_data['postal_code'] 		= $post_code;
				$prof_data['mobile_number'] 	= $mobile_phone;
				$prof_data['phone_number'] 		= $phone_number;
				$prof_data['business_name'] 	= $business_name;
				$prof_data['business_address'] 	= $business_address;
				$prof_data['password'] 			= $en_pass;
				$prof_data['partner_status'] 	= "aktif";
				$prof_data['komisi'] 			= $cek_user[0]->komisi;
				$prof_data['branch_id'] 		= $cek_user[0]->branch_id;
				$prof_data['corporate_id'] 		= $cek_user[0]->corporate_id;
				$prof_data['skema_bisnis_id'] 	= $cek_user[0]->skema_bisnis_id;
				$prof_data['partner_level'] 	= $cek_partner_level[0]->level_code;
				
				$id = $this->ci->mgeneral->save($prof_data, 'mstr_partner');

				$data = $this->ci->mgeneral->getWhere(array('partner_id'=>$id),'mstr_partner');
				$result["rest_no"] = "0";
					
				foreach($data as $d):
					$skema_name	 	= $this->ci->mgeneral->getValue("skema_bisnis_name",array('skema_bisnis_id'=>$d->skema_bisnis_id),"skema_bisnis");
					$partner_level	= $this->ci->mgeneral->getValue("level_name",array('skema_bisnis_id'=>$d->skema_bisnis_id, "level_code" => $d->partner_level),"mstr_partner_level");
					$user_info = array('partner_id'		=> $this->ci->converter->encode($d->partner_id),
										 'register_date'	=> $d->date_added,
										 'first_name'		=> $d->first_name,
										 'last_name'		=> $d->last_name,
										 'email'			=> $d->email,
										 'mobile_number'	=> $d->mobile_number,
										 'phone_number'		=> $d->phone_number,
										 'address'			=> $d->address,
										 'postal_code'		=> $d->postal_code,
										 'business_name'	=> $d->business_name,
										 'business_address'	=> $d->business_address,
										 'current_deposit'	=> $d->current_deposit,
										 'skema_bisnis'		=> $skema_name,
										 'partner_level'	=> $partner_level);
					
				endforeach;

				$result['data']["user_info"] = $user_info;

				if($data[0]->skema_bisnis_id == "3" && $data[0]->partner_level == "1"){
					$corporate_data = $this->ci->mgeneral->getWhere(array("corporate_id" => $data[0]->corporate_id), "mstr_corporate");
					$result['data']["corporate_info"] 	= array("corporate_id"		=> $this->ci->converter->encode($data[0]->corporate_id),
																"corporate_logo"	=> ($corporate_data[0]->logo) ? base_url()."uploads/corp_logo/".$corporate_data[0]->logo : "");
					
				}
				else if($data[0]->skema_bisnis_id == "4" && $data[0]->partner_level == "1"){
					$corporate_data = $this->ci->mgeneral->getWhere(array("corporate_id" => $data[0]->corporate_id), "mstr_corporate");
					$branch_data = $this->ci->mgeneral->getWhere(array("branch_id" => $data[0]->branch_id), "mstr_branch");
					$result['data']["corporate_info"] 	= array("corporate_id"		=> $this->ci->converter->encode($data[0]->corporate_id),
																"corporate_logo"	=> ($corporate_data[0]->logo) ? base_url()."uploads/corp_logo/".$corporate_data[0]->logo : "");

					$result['data']["branch_info"]	= array("branch_id"		=> $this->ci->converter->encode($data[0]->branch_id),
															"branch_code"	=> $branch_data[0]->branch_code);
				}
			}
			else{
				$result["rest_no"] = "28";
				$result["reason"] = "partner level tidak ditemukan";
			}
		}

		return  $result;
	}

	private function upload_corporate_logo($param){
		$this->ci =& get_instance();
		$partner_id 		= $this->ci->converter->decode(@$param['partner_id']);
		$corporate_id 		= $this->ci->converter->decode(@$param['corporate_id']);
		$cek_user 		= $this->ci->mgeneral->getWhere(array('partner_id'=>$partner_id, "corporate_id" => $corporate_id),'mstr_partner');
		$cek_corporate 	= $this->ci->mgeneral->getWhere(array('corporate_id'=>$corporate_id),'mstr_corporate');

		if ($partner_id == "") {
			#dikarenakan email di isi dengan phone_number
			$result["rest_no"] 	= "17";
			$result["reason"] 	= "partner_id tidak boleh kosong";
		} 
		else if ($corporate_id == "") {
			#dikarenakan email di isi dengan phone_number
			$result["rest_no"] 	= "17";
			$result["reason"] 	= "corporate_id tidak boleh kosong";
		}
		else if (count($cek_user) == 0) {
			$result["rest_no"] = "30";
			$result["reason"] = getMobileErrorText("30");
		}
		else if (count($cek_corporate) == 0) {
			$result["rest_no"] 	= "30";
			$result["reason"] 	= "Corporate tidak terdeteksi";
		}
		else if ($cek_user[0]->partner_level != 1){
			$result["rest_no"] 	= "30";
			$result["reason"] 	= "hanya corporate admin yang dapat mengganti logo";
		}
		else {
			$this->ci->load->library("upload_file");

			$upload	= $this->ci->upload_file->upload_img($corporate_id);

			if($upload["err_no"] == 0){
				$this->ci->mgeneral->update(array("corporate_id" => $corporate_id), array("logo" => $upload['filename']), 'mstr_corporate');

				$result["rest_no"] = "0";
				$result["data"]["img_url"] = base_url().'/uploads/corp_logo/'.$upload['filename'];
			}
			else{
				$result = array("rest_no" 	=> $upload['err_no'],
								"reason"	=> $upload['err_msg']);
			}
		}

		return  $result;	
	} 

	private function confirm_registration($param){
		$this->ci =& get_instance();
		$partner_id 		= $this->ci->converter->decode(@$param['partner_id']);
		$cek_user 		= $this->ci->mgeneral->getWhere(array('partner_id'=>$partner_id),'mstr_partner');
		if ($partner_id == "") {
			#dikarenakan email di isi dengan phone_number
			$result["rest_no"] 	= "17";
			$result["reason"] 	= "partner_id tidak boleh kosong";
		} 
		else if (count($cek_user) == 0) {
			$result["rest_no"] = "30";
			$result["reason"] = getMobileErrorText("30");
		}
		else {
			$this->ci->mgeneral->update(array("partner_id" => $partner_id), array("partner_status" => "aktif"), "mstr_partner");
			$cek_user 		= $this->ci->mgeneral->getWhere(array('partner_id'=>$partner_id),'mstr_partner');
			if($cek_user[0]->partner_status == "aktif"){
				$result["rest_no"] = "0";
				$result["data"]["message"] = "konfirmasi berhasil";	
			}
			else{
				$result["rest_no"] 			= "717";
				$result["reason"] 			= "proses konfirmasi gagal, silahkan coba kembali.";
			}
		}

		return $result;
	}

	private function get_branch($param){
		$this->ci =& get_instance();
		$branch_id 		= $this->ci->converter->decode(@$param['branch_id']);
		$partner_data	= $this->ci->mgeneral->getWhere(array('branch_id'=>$branch_id),'mstr_partner');
		if ($branch_id == "") {
			$result["rest_no"] 	= "17";
			$result["reason"] 	= "branch_id tidak boleh kosong";
		} 
		else if (count($partner_data) == 0) {
			$result["rest_no"] 	= "30";
			$result["reason"] 	= "branch tidak ditemukan";
		}
		else {
			$branch_data 	= $this->ci->mgeneral->getWhere(array("branch_id" => $branch_id), "mstr_branch");
			$corporate_data = $this->ci->mgeneral->getWhere(array('corporate_id'=>$partner_data[0]->corporate_id),'mstr_corporate');
			if(count($branch_data) > 0 && count($corporate_data) > 0){
				$result["rest_no"] = "0";
				$result["data"]["branch_id"] 	= $this->ci->converter->encode($branch_id);
				$result['data']['branch_logo']	= ($corporate_data[0]->logo) ? base_url()."uploads/corp_logo/".$corporate_data[0]->logo : "";
			}
			else{
				$result["rest_no"] 			= "717";
				$result["reason"] 			= "branch tidak ditemukan";
			}
		}

		return $result;
	}

	private function send_referal($param){
		$this->ci =& get_instance();
		$branch_id 		= $this->ci->converter->decode(@$param['branch_id']);
		$email			= @$param['email'];
		if ($branch_id == "") {
			$result["rest_no"] 	= "17";
			$result["reason"] 	= "branch_id tidak boleh kosong";
		} 
		else if ($email == "") {
			#dikarenakan email di isi dengan phone_number
			$result["rest_no"] = "17";
			$result["reason"] = getMobileErrorText($result["rest_no"]);
			#$result["rest_no"] = "17";
			#$result["reason"] = getMobileErrorText($result["rest_no"]);
		}
		else {
			/*edit subject dan content belum dilakukan*/
			$content		= "Silakan klik link berikut untuk melakukan registrasi. ".$this->app_url."register?branch_id=".$param['branch_id'];
			$subject 		= "Registrasi Pacific Travel";
			
			$this->ci->load->library("send");
			$result 		= $this->ci->send->email($email, $subject, $content);
			if($result['rest_no'] == "0"){
				$result['data']['message'] = $result['message'];
				unset($result['message']);
			}
		}

		return $result;
	}

	private function update_status_corporate_user($param){
		$this->ci =& get_instance();
		$partner_id 		= $this->ci->converter->decode(@$param['partner_id']);
		$status 			= @$param['status'];

		$partner_data	= $this->ci->mgeneral->getWhere(array('partner_id'=>$partner_id),'mstr_partner');
		if (count($partner_data) == 0) {
			$result["rest_no"] 	= "30";
			$result["reason"] 	= "branch tidak ditemukan";
		}
		else {
			$this->ci->mgeneral->update(array("partner_id" => $partner_id), array("partner_status" => $status), "mstr_partner");
			$result['rest_no'] = "0";
			$result['data']['message'] = "update status berhasil";
		}

		return $result;
	}
}

?>

