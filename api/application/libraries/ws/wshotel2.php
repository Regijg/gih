<?php

class wshotel2
{
    
    function __construct()
    {
        $ci =& get_instance();
    }
    
	  public function index($action,$parameter)
    {
    		switch($action):
    			case 'ws_srkeyword':	$result = $this->keyword($parameter); 	break;
          case 'ws_findhotel':	$result = $this->search($parameter); 	break;
          // case "ws_detailhotel";$result = $this->detail($parameter);	break;
          // case "ws_selroom";		$result = $this->selroom($parameter);	break;
          case "ws_payment";		$result = $this->payment($action,$parameter);	break;
          case "ws_cancel";		$result = $this->call_hotel($action,$parameter);	break;
          default:
              $result = array('rest_no'=>"103",'reason'=>"Function tidak terdaftar");
          break;
        endswitch;

        return $result;
    }

    function keyword($param)
    {
        $url = "https://www.oyorooms.com/api/pwa/autocompletenew?query=".urlencode($param['word'])."&region=96&userId=109cc71a91298383e95cc48ecd4e9d27";
        $header = array(
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "cache-control: no-cache"
          );
        $data = $this->get($url,$header);

        if($data['rest_no'] == 0){
          $resultData['rest_no']    = "0";
          $resultData['result']   = $data['result']['responseObject'];
        }else{
          $resultData = $data;
        }

        return $resultData;
    }

    function search($param)
    {
      $ci =& get_instance();
        
        $url = "http://api.oyorooms.com/b2b_reseller/api/v2/hotels?offset=".$offset."&limit=100&countryCode=".$country;
        $searchHotelData = $ci->mquery->getHotelbyLatLong($param['lattitude'],$param['longitude']);

        if(count($searchHotelData)!=0){

          $hotelIDList  = array();
          $hotelList    = array();
          foreach($searchHotelData as $hd):
            $hotelIDList[] = $hd->id;
            $hotelList[$hd->id] = $hd;
            $galeri = $ci->mquery->getImgHotelByID($hd->id);
            $galeriList = array();
            foreach ($galeri as $g):
              $galeriList[] = $g->image;
            endforeach;
            $hotelList[$hd->id]->galeri = $galeriList;
          endforeach;

          //GET AVAILAIBILITY HOTELS
          $url = "http://api.oyorooms.com/b2b_reseller/api/v2/hotels/availability";
          $postData = array('hotelIds'  => $hotelIDList,
                            'checkin'   => strtotime($param['check_in']),
                            'checkout'  => strtotime($param['check_out']),
                            'adults'    => $param['adults'],
                            'children'  => $param['children'],
                            'rooms'     => $param['rooms']);
                            
          $header = array(
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Postman-Token: 4afb2d22-6e20-a348-4a00-3b81be938bbe,33c50e40-8199-4946-bfc0-30f3a5d539b8",
            "cache-control: no-cache",
            "x-access-token: dnNKQ0t5c2drSkd5cW0zTmpMd1E6WDVUeGNucTYxMTdnMUZRZGFyMk0="
          );

          $hotelAvail = $this->post($url,json_encode($postData), $header);

          if($hotelAvail['rest_no']==0):

            if(count($hotelAvail['result']['hotels']) != 0):
              
              //build hotel data and availability
              $hotelData = array();
              foreach($hotelAvail['result']['hotels'] as $ha):
                  
                  $dataHotel = json_decode(json_encode($hotelList[$ha['id']]),true);
                  $dataHotel['currencyCode']  = $ha['currencyCode'];
                  $dataHotel['mealPlan']      = $ha['mealPlan'];
                  $dataHotel['allocatedRoomOccupancy'] = $ha['allocatedRoomOccupancy'];
                  $dataHotel['rooms']         = $ha['rooms'];
                  $dataHotel['cancellationPolicy'] = $ha['cancellationPolicy'];
                  $dataHotel['distance']      = number_format((float)$dataHotel['distance'], 2, '.', '');
                  $hotelData[] = $dataHotel;

              endforeach;

              $resultData['rest_no']  = "0";
              $resultData['result']   = $hotelData;
            else:
              $resultData['rest_no']  = $hotelAvail['result']['errorCode'];
              $resultData['reason']   = $hotelAvail['result']['message'];
            endif;


          else:
            
            $resultData['rest_no']  = "1";
            $resultData['reason']   = "Hotel tidak tersedia";

          endif;

        }else{
          $resultData['rest_no']  = "1";
          $resultData['reason']   = "Hotel tidak ditemukan";
        }

        return $resultData;
    }

    private function payment($action,$param)
    {
      $ci =& get_instance();

      $url = 'http://api.oyorooms.com/b2b_reseller/api/v2/bookings?fields=cancellationPolicy,priceDetails,billingId';

      $dtHotelID = $ci->db->query('select max(id_hotel_data) as id from dt_hotel')->result();

      // if (count($dtHotelID) > -1) {
      //   $idRef = $ci->converter->encode('1');
      // }else {
      $idRef = $ci->converter->encode($dtHotelID[0]->id+1);
      // }

      // echo $ci->converter->decode($idRef);
      // exit();
      
      $postData['guest'] = array(
        'firstName'   => $param['firstName'],
        'lastName'    => $param['lastName'],
        'email'       => $param['email'],
        'countryCode' => $param['countryCode'],
        'phone'       => $param['phone']
      );

      $postData['booking'] = array(
        "single"              => $param['single'],
        "double"              => $param['double'],
        "extra"               => $param['extra'],
        "adults"              => $param['adults'],
        "children"            => $param['children'],
        "rooms"               => $param['rooms'],
        "checkin"             => strtotime($param['checkin']),
        "checkout"            => strtotime($param['checkout']),
        "hotelId"             => $param['hotelId'],
        "roomCategoryId"      => $param['roomCategoryId'],
        "externalReferenceId" => $idRef
      );

      $postData['payments'] = array(
        'billToAffiliate'     => $param['billToAffiliate']
      );

      $header = array(
        "Cache-Control: no-cache",
        "Content-Type: application/json",
        "Postman-Token: 4afb2d22-6e20-a348-4a00-3b81be938bbe,33c50e40-8199-4946-bfc0-30f3a5d539b8",
        "cache-control: no-cache",
        "x-access-token: dnNKQ0t5c2drSkd5cW0zTmpMd1E6WDVUeGNucTYxMTdnMUZRZGFyMk0="
      );

      $hotelAvail = $this->post($url,json_encode($postData), $header);

      $cekHotel = $ci->mgeneral->getWhere(array('id'=>$param['hotelId']),"hotel");
      // exit(json_encode($cekHotel));

      if($hotelAvail['rest_no']==0):
        
        if($hotelAvail['result']['invoice'] != ''):
          
          $checkin      = new DateTime(date('Y-m-d', $hotelAvail['result']['checkin']));
          $checkout     = new DateTime(date('Y-m-d', $hotelAvail['result']['checkout']));
          $jumlahMalam  = $checkout->diff($checkin);
          
          $postDtHotel = array(
            'nama_hotel'      => $cekHotel[0]->name,
            'alamat_hotel'    => $cekHotel[0]->street,
            'tanggal_booking' => date("Y-m-d H:i:s"),
            // 'corp_id'         => 123, //nyokot timana
            'partner_id'      => 1234567890, //nyokot timana
            'itinerary_id'    => $hotelAvail['result']['invoice'].$hotelAvail['result']['id'],
            'id_hotel'        => $cekHotel[0]->id,
            'img_hotel'       => $cekHotel[0]->bestImage,
            "star"            => '',
            "jumlah_kamar"    => $hotelAvail['result']['roomCount'],
            "tipe_kamar"      => 'Deluxe',
            "tanggal_cekin"   => date('Y-m-d', $hotelAvail['result']['checkin']),
            "tanggal_cekout"  => date('Y-m-d', $hotelAvail['result']['checkout']),
            "jumlah_malam"    => $jumlahMalam->d,
            "guest"           => $hotelAvail['result']['guestCount'],
            "harga_per_kamar" => 0, //nyokot timana
            "pajak_per_kamar" => 0, //nyokot timana
            "total_per_kamar" => 0, //nyokot timana
            "extra_guest_fee" => 0, //nyokot timana
            "harga_total"     => $hotelAvail['result']['finalAmount'],
            "komisi_hotel"    => $hotelAvail['result']['commission'],
            "status"          => 'BOOK',
            // "cancel_policy" => $param['roomCategoryId'],
            "nama_pemesan"    => $param['firstName'].' '.$param['lastName'],
            "phone_pemesan"   => $param['phone'],
            "email_pemesan"   => $param['email'],
            "link_detail"     => $cekHotel[0]->landingUrl,
            "app"             => 'agent',
          );

          $dth_id 	= $ci->mgeneral->save($postDtHotel,'dt_hotel');

          $postDtHotelRoom = array(
            'nama_tamu'       => $param['firstName'].' '.$param['lastName'],
            'hotel_data_id'   => $dth_id,
            'bed'             => '',
            'smoking'         => 'yes',
            'special_req'     => ''
          );

          $dthroom_id 	= $ci->mgeneral->save($postDtHotelRoom,'dt_hotel_room');
          
          $ticket_invoice = array(
            'booking_item'		=> 'hotel',
            'booking_id'		  => $dth_id,
            'booking_number'	=> $hotelAvail['result']['invoice'],
            'total_price'		  => $hotelAvail['result']['finalAmount'],
            'status_payment'	=> 'pending',
            'expired_time'		=> date('Y-m-d H:i:s'),
          );
    
          $booking_ticket_invoice 	= $ci->mgeneral->save($ticket_invoice,'booking_ticket_invoice');

          $hotelData = array();
          $resultData['rest_no']  = "0";
          $resultData['result']   = $hotelAvail['result'];
        else:
          $resultData['rest_no']  = $hotelAvail['result']['errorCode'];
          $resultData['reason']   = $hotelAvail['result']['message'];
        endif;
      else:
        
        $resultData['rest_no']  = "1";
        $resultData['reason']   = "Hotel tidak tersedia";

      endif;
      
      return $resultData;
    }

    function get($url,$header){

        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_POSTFIELDS => "",
          CURLOPT_HTTPHEADER => $header,
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        //PROCESS DATA OYO HOTEL
        if($err)
        {
          $resultData['rest_no']  = "1";
          $resultData['reason']   = "cURL Error #:" . $err;
        }
        else
        {
          $data = json_decode($response,true);
          $resultData['rest_no']    = "0";
          $resultData['result']   = $data;
        }
        
        return $resultData;
    }

    function post($url,$postData,$header){

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $postData,
          CURLOPT_HTTPHEADER => $header,
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        if($err)
        {
          $resultData['rest_no']  = "1";
          $resultData['reason']   = "cURL Error #:" . $err;
        }
        else
        {
          $data = json_decode($response,true);
          $resultData['rest_no']    = "0";
          $resultData['result']   = $data;
        }
        
        return $resultData;

    }
	
}