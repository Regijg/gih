<?php

class wsflight
{
    
    function __construct()
    {
        $ci =& get_instance();
        $ci->load->library('webservice/flight');
    }
    
	public function index($action,$parameter)
    {
		switch($action):
			case 'ws_getairlines':	$result = $this->call_flight($action,$parameter); 	break;
            case 'ws_getairport':	$result = $this->call_flight($action,$parameter); 	break;
            case "ws_search";		$result = $this->call_flight($action,$parameter); 	break;
            case "ws_getallclass";	$result = $this->call_flight($action,$parameter); 	break;
			case "ws_faredetail";	$result = $this->fare_detail($action,$parameter);	break;
            case "ws_book";			$result = $this->book($action,$parameter);	break;
            case "ws_payment";		$result = $this->payment($action,$parameter);	break;
            case "ws_cekpayment";	$result = $this->call_flight($action,$parameter);	break;
            case "ws_getrealprice";	$result = $this->call_flight($action,$parameter);	break;
            case "ws_getbalance";	$result = $this->call_flight($action,$parameter);	break;
            case "ws_getfavorite";	$result = $this->favorite($parameter);	break;
            default:
                $result = array('rest_no'=>"103",'reason'=>"Function tidak terdaftar");
            break;
        endswitch;

        return $result;
    }
	
	private function call_flight($action,$param)
	{
		$ci =& get_instance();
		$data = $ci->flight->index($action,$param);
		
		if($data['rest_no'] == 0):
			$data2= $data;
			unset($data2['rest_no']);
			$result = array('rest_no'	=> $data['rest_no'],
							'data'		=> $data2);
		else:
			$result = $data;
		endif;
		
		return $result;
	}

	private function fare_detail($action,$param)
	{
		$ci =& get_instance();
        $result = $this->call_flight($action, $param);
        
        if($param["client"]["access_type"] == "single"){
        	$param["partner_id"] = $ci->converter->encode($param["client"]["partner_id"]);
        }

        if($result['rest_no'] == 0){
        	$result = $this->calculate_NTA($param['partner_id'],$result);
        	$result['data']['commission_corp'] = $result['data']['commission_corp'] + $result['data']['cashback'];
        }
        #remove array
		unset($result['data']['depart']['commission']);
		unset($result['data']['depart']['NTA']);
		unset($result['data']['return']['commission']);
		unset($result['data']['return']['NTA']);
		unset($result['data']['cashback']);
		return $result;
	}
	
	private function book($action,$param)
	{
		$ci =& get_instance();
        $result = $this->call_flight($action, $param);
		
		if($param["client"]["access_type"] == "single"){
        	$param["partner_id"] = $ci->converter->encode($param["client"]["partner_id"]);
        }

        $komisiAwal = $result['data']['commission'];
		$result 	= $this->calculate_NTA($param['partner_id'],$result);
		$bookId		= $ci->converter->decode($result['data']['book_id']);
		
		$partnerId	= $ci->converter->decode($param['partner_id']);
		$komisiAkhir= $result['data']['commission'];
		$cashback 	= $result['data']['cashback'];
		$result['data']['commission_corp'] = $result['data']['commission_corp'] + $result['data']['cashback'];
		
		$dataArray	= array('corp_id'				=> "1",
							'partner_id' 			=> $partnerId,
							'dtf_tiket_price'		=> $result['data']['total_fares'],
							'dtf_comission'			=> $komisiAkhir,
							'dtf_branch_cashback'	=> $cashback,
							'dtf_comission_corp'	=> $komisiAwal-($komisiAkhir+$cashback));
		#$ci->mgeneral->update(array('dtf_id'=>$bookId),$dataArray,"dt_flight");
		
		#remove array
		unset($result['data']['depart']['commission']);
		unset($result['data']['depart']['NTA']);
		unset($result['data']['return']['commission']);
		unset($result['data']['return']['NTA']);
		unset($result['data']['cashback']);
		return $result;
	}
	
	private function calculate_NTA($partner,$result)
	{
		$ci =& get_instance();
		$partnerId	= $ci->converter->decode($partner);
		$partner_data = $ci->mgeneral->getWhere(array("partner_id" => $partnerId), "mstr_partner");
		if($partner_data[0]->branch_id && $partner_data[0]->branch_id != '0'){
			$branch_data 	= $ci->mgeneral->getWhere(array("branch_id" => $partner_data[0]->branch_id), "mstr_branch");
			$cashback		= $branch_data[0]->cashback;
		}
		else{
			$cashback		= 0;
		}

		if($partner_data[0]->corporate_id && $partner_data[0]->corporate_id != '0'){
			$komisi			 	= $ci->mgeneral->getValue("komisi",array("corporate_id"=>$partner_data[0]->corporate_id, "partner_level" => 1),"mstr_partner");			
		}
		else{
			$komisi		= $ci->mgeneral->getValue("komisi",array("partner_id"=>$partnerId),"mstr_partner");
		}

		$nilaiCashBack	= number_format(($cashback*$result['data']['commission'])/100, 0, ',', '');
		$nilaiKomisi	= number_format(($komisi*$result['data']['commission'])/100, 0, ',', '');

		$NTA			= $result['data']['total_fares'] - $nilaiKomisi;
		$komisi_corp 	= $result['data']['commission']  - $nilaiKomisi;

		$result['data']['cashback'] 		= (int)$nilaiCashBack;
		$result['data']['commission'] 		= (int)$nilaiKomisi;
		$result['data']['commission_corp'] 	= (int)$komisi_corp - $nilaiCashBack;
		$result['data']['NTA']				= $NTA;
		
		return $result;
	}
	
	private function payment($action,$param)
	{
		$ci =& get_instance();
        $bookid	 = $ci->converter->decode($param['book_id']);
	
		$cekData = $ci->mgeneral->getWhere(array('dtf_id'=>$bookid),"dt_flight");
		$psgr 	 = $ci->mgeneral->getWhere(array('dtf_id'=>$bookid),"dt_flight_pax");
		
		if(!empty($cekData) && $cekData[0]->dtf_status == "book"):
		
			$partner_id		= $cekData[0]->partner_id;
			$hargaBayar		= $cekData[0]->dtf_tiket_price-$cekData[0]->dtf_comission;
			$tipeFlight		= $cekData[0]->dtf_roundtrip;
			$roundtrip		= $tipeFlight;
			
			if($tipeFlight=="return"):
				$kdBook		= $cekData[0]->depart_book_code." dan ".$cekData[0]->return_book_code;
			else:
				$kdBook		= $cekData[0]->depart_book_code;
			endif;			
			$msg			= "Pembelian tiket pesawat kode booking ".$kdBook;
			
			//$actionPayment	= cek_deposit_dan_bayar($partner_id,$hargaBayar,$bookid,$msg);
			#echo $partner_id."<br>".$hargaBayar."<br>".$msg."<br>";
			#print_r($actionPayment);
			
			$actionPayment['error_no'] = "0";
			if($actionPayment['error_no'] == "0"):
				$result = $this->call_flight($action, $param);
			else:
				
				$result['rest_no']	= $actionPayment['error_no'];
           	 	$result['reason']	= $actionPayment['error_msg'];
						 
			endif;
			
		else:
		
            $result['rest_no']	= "701";
            $result['reason']	= "Data booking tidak ditemukan";
			
		endif;
		
		return $result;
	}
	
	function favorite($param)
	{
		$ci =& get_instance();
		
		$partner_id = $ci->converter->decode($param['partner_id']);
		$cekPax = $ci->mgeneral->post_query_sql("select cust_id,first_name,last_name,id_card,email,phone,birth_date from dt_customer where first_name LIKE '%".$param['name']."%' AND partner_id = '".$partner_id.")' ORDER BY cust_id limit 5");
		if (count($cekPax) == 0) {
			$result["rest_no"] = "37";
			$result["reason"] = "data customer tidak ditemukan";
		} else {
			$result["rest_no"] = "0";
			$result["data"] = $cekPax;
		}
		
		return $result;
		
	}
	
}