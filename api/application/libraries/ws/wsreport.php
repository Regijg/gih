<?php

class wsreport
{
    
    function __construct()
    {
        $ci =& get_instance();
        $ci->load->library('webservice/transaction');
    }
    
	public function index($action,$parameter)
    {
		switch($action):
			case "ppob";				$result = $this->call_transaction($action,$parameter); 	break;
			case "ppob_detail";			$result = $this->call_transaction($action,$parameter); 	break;
            case "flight";				$result = $this->call_transaction($action,$parameter); 	break;
            case "flight_detail";		$result = $this->call_transaction($action,$parameter); 	break;
            case "train";				$result = $this->call_transaction($action,$parameter); 	break;
            case "train_detail";		$result = $this->call_transaction($action,$parameter); 	break;
            case "deposit";				$result = $this->call_transaction($action,$parameter); 	break;
            case "transaction";			$result = $this->call_transaction($action,$parameter); 	break;
            case "get_corporate_user";	$result = $this->get_corporate_user($action,$parameter); 	break;
            case "get_branch_member";	$result = $this->get_branch_member($action,$parameter); 	break;
            default:
                $result = array('rest_no'=>"103",'reason'=>"Function tidak terdaftar");
            break;
        endswitch;

        return $result;
    }
	
	private function call_transaction($action,$param)
	{
		$ci =& get_instance();
		$data = $ci->transaction->index($action,$param);
		
		if($data['rest_no'] == 0):
			$data2= $data;
			unset($data2['rest_no']);
			$result = array('rest_no'	=> $data['rest_no'],
							'data'		=> $data2);
		else:
			$result = $data;
		endif;
		
		return $result;
	}

	private function get_corporate_user($action, $param){
		$ci =& get_instance();
		
		$page 				= @$param['page'];
		$per_page 			= @$param['per_page'];
		$partner_id 		= $ci->converter->decode(@$param['partner_id']);
		$corporate_id 		= $ci->converter->decode(@$param['corporate_id']);
		$cek_user 			= $ci->mgeneral->getWhere(array('partner_id'=>$partner_id, "corporate_id" => $corporate_id),'mstr_partner');
		$cek_corporate 		= $ci->mgeneral->getWhere(array('corporate_id'=>$corporate_id),'mstr_corporate');

		if ($partner_id == "") {
			#dikarenakan email di isi dengan phone_number
			$result["rest_no"] 	= "17";
			$result["reason"] 	= "partner_id tidak boleh kosong";
		} 
		else if ($corporate_id == "") {
			#dikarenakan email di isi dengan phone_number
			$result["rest_no"] 	= "17";
			$result["reason"] 	= "corporate_id tidak boleh kosong";
		} 
		else if (count($cek_user) == 0) {
			$result["rest_no"] = "30";
			$result["reason"] = getMobileErrorText("30");
		}
		else if (count($cek_corporate) == 0) {
			$result["rest_no"] 	= "30";
			$result["reason"] 	= "Corporate tidak terdeteksi";
		}
		else if ($cek_user[0]->partner_level != 1){
			$result["rest_no"] 	= "30";
			$result["reason"] 	= "hanya corporate admin yang bisa menambahkan user";
		}
		else{
			#handle untuk query ke database
			if(!$per_page): $per_page = 10; endif;
			if(!$page): $page = 1; endif;
			
			if($page!="1"): $first_row = (($page-1)*$per_page);elseif($page==1): $first_row = '0';endif;

			if ($page <= 0) {
				$result["rest_no"] = "38";
				$result["reason"] = getMobileErrorText("38");
				return $result;
				exit();
			} else {
				$data = $ci->mquery->get_member(" corporate_id = ".$corporate_id." AND partner_id != ".$partner_id." ");
			}
			
			if (count($data) == 0) {
				$result["rest_no"] = "37";
				$result["reason"] = getMobileErrorText("37");
			} else {
				$result["rest_no"] = "0";
				
				$totaldata			= count($data);
				$page				= explode(".",$totaldata/$per_page);
				$sisaPage			= $totaldata%$per_page;
				if($sisaPage==0): $totalpage = $page[0]; else: $totalpage = $page[0]+1; endif;
				if($param['page']==""): $cpage = "1"; else: $cpage=$param['page']; endif;
				
				$result['data']["current_page"] = $cpage;
				$result['data']["total_page"] 	= $totalpage;
				
				$get_data = $ci->mquery->get_member(" corporate_id = ".$corporate_id." AND partner_id != ".$partner_id." ", ($first_row), $per_page);
				
				$user_list = array();
				foreach($get_data as $d):
					$skema_name	 	= $ci->mgeneral->getValue("skema_bisnis_name",array('skema_bisnis_id'=>$data[0]->skema_bisnis_id),"skema_bisnis");
					$partner_level	= $ci->mgeneral->getValue("level_name",array('skema_bisnis_id'=>$d->skema_bisnis_id, "level_code" => $d->partner_level),"mstr_partner_level");
					$user_list[] = array('partner_id'		=> $ci->converter->encode($d->partner_id),
										 'register_date'	=> $d->date_added,
										 'first_name'		=> $d->first_name,
										 'last_name'		=> $d->last_name,
										 'email'			=> $d->email,
										 'mobile_number'	=> $d->mobile_number,
										 'phone_number'		=> $d->phone_number,
										 'address'			=> $d->address,
										 'postal_code'		=> $d->postal_code,
										 'business_name'	=> $d->business_name,
										 'business_address'	=> $d->business_address,
										 'current_deposit'	=> $d->current_deposit,
										 'skema_bisnis'		=> $skema_name,
										 'partner_level'	=> $partner_level,
										 'status'			=> $d->partner_status);
					
				endforeach;
				
				
				$result['data']["user_list"] = isset($user_list) ? $user_list : array();
			}
		}

		return $result;
	}

	private function get_branch_member($action, $param){
		$ci =& get_instance();
		
		$page 				= @$param['page'];
		$per_page 			= @$param['per_page'];
		$partner_id 		= $ci->converter->decode(@$param['partner_id']);
		$branch_id 			= $ci->converter->decode(@$param['branch_id']);
		$cek_user 			= $ci->mgeneral->getWhere(array('partner_id'=>$partner_id, "branch_id" => $branch_id),'mstr_partner');
		$cek_branch 		= $ci->mgeneral->getWhere(array('branch_id'=>$branch_id),'mstr_branch');

		if ($partner_id == "") {
			#dikarenakan email di isi dengan phone_number
			$result["rest_no"] 	= "17";
			$result["reason"] 	= "partner_id tidak boleh kosong";
		} 
		else if ($branch_id == "") {
			#dikarenakan email di isi dengan phone_number
			$result["rest_no"] 	= "17";
			$result["reason"] 	= "branch_id tidak boleh kosong";
		} 
		else if (count($cek_user) == 0) {
			$result["rest_no"] = "30";
			$result["reason"] = getMobileErrorText("30");
		}
		else if (count($cek_branch) == 0) {
			$result["rest_no"] 	= "30";
			$result["reason"] 	= "Branch tidak terdeteksi";
		}
		else{
			#handle untuk query ke database
			if(!$per_page): $per_page = 10; endif;
			if(!$page): $page = 1; endif;
			
			if($page!="1"): $first_row = (($page-1)*$per_page);elseif($page==1): $first_row = '0';endif;

			if ($page <= 0) {
				$result["rest_no"] = "38";
				$result["reason"] = getMobileErrorText("38");
				return $result;
				exit();
			} else {
				$data = $ci->mquery->get_member(" branch_id = ".$branch_id." AND partner_id != ".$partner_id." AND partner_level = 1 ");
			}
			
			if (count($data) == 0) {
				$result["rest_no"] = "37";
				$result["reason"] = getMobileErrorText("37");
			} else {
				$result["rest_no"] = "0";
				
				$totaldata			= count($data);
				$page				= explode(".",$totaldata/$per_page);
				$sisaPage			= $totaldata%$per_page;
				if($sisaPage==0): $totalpage = $page[0]; else: $totalpage = $page[0]+1; endif;
				if($param['page']==""): $cpage = "1"; else: $cpage=$param['page']; endif;
				
				$result['data']["current_page"] = $cpage;
				$result['data']["total_page"] 	= $totalpage;
				
				$get_data = $ci->mquery->get_member(" branch_id = ".$branch_id." AND partner_id != ".$partner_id." AND partner_level = 1 ", ($first_row), $per_page);
				
				$user_list = array();
				foreach($get_data as $d):
					$skema_name	 	= $ci->mgeneral->getValue("skema_bisnis_name",array('skema_bisnis_id'=>$d->skema_bisnis_id),"skema_bisnis");
					$partner_level	= $ci->mgeneral->getValue("level_name",array('skema_bisnis_id'=>$d->skema_bisnis_id, "level_code" => $d->partner_level),"mstr_partner_level");
					$user_list[] = array('partner_id'		=> $ci->converter->encode($d->partner_id),
										 'register_date'	=> $d->date_added,
										 'first_name'		=> $d->first_name,
										 'last_name'		=> $d->last_name,
										 'email'			=> $d->email,
										 'mobile_number'	=> $d->mobile_number,
										 'phone_number'		=> $d->phone_number,
										 'address'			=> $d->address,
										 'postal_code'		=> $d->postal_code,
										 'business_name'	=> $d->business_name,
										 'business_address'	=> $d->business_address,
										 'current_deposit'	=> $d->current_deposit,
										 'skema_bisnis'		=> $skema_name,
										 'partner_level'	=> $partner_level);
					
				endforeach;
				
				
				$result['data']["member_list"] = isset($user_list) ? $user_list : array();
			}
		}

		return $result;
	}
	
}