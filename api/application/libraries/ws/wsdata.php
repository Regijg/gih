<?php

class wsdata
{
    
    function __construct()
    {
        $ci =& get_instance();
        $ci->load->library('webservice/ticket');
    }
    
	public function index($action,$parameter)
    {
		switch($action):
			case 'pdf_ticket':			$result = $this->call_ticket($action,$parameter); 	break;
            case 'html_ticket':			$result = $this->call_ticket($action,$parameter); 	break;
            case "email_ticket";		$result = $this->call_ticket($action,$parameter); 	break;
            case "slider";				$result = $this->get_slider($action,$parameter); 	break;
            case "news";                $result = $this->get_news($action,$parameter);    break;
            default:
                $result = array('rest_no'=>"103",'reason'=>"Function tidak terdaftar");
            break;
        endswitch;

        return $result;
    }
	
	private function call_ticket($action,$param)
	{
		$ci =& get_instance();
		$data = $ci->ticket->index($action,$param);
		
		if($data['rest_no'] == 0):
			$data2= $data;
			unset($data2['rest_no']);
			$result = array('rest_no'	=> $data['rest_no'],
							'data'		=> $data2);
		else:
			$result = $data;
		endif;
		
		return $result;
	}

	private function get_slider($action, $param){
        $ci =& get_instance();

        $banner = $ci->mgeneral->getWhere(array("slider_status" => "on"), "mstr_slider");
        $static = array();
        $slider = array();
        foreach($banner as $b){
            $slider[]   = array(
                            "title"         => $b->slider_title,
                            "description"   => $b->slider_deskripsi, 
                            "img_name"      => $b->slider_img,
                            "img_url"       => "http://pacifictravel.co.id/agent/galeri/img/".$b->slider_img
                        );
        }


        $result = array("rest_no"   => "0",
                        "data"      => array(
                            "slider_list" 	=> $slider
                        )
                    );
        
        return $result;
    }

    private function get_news($action, $param){
        $ci =& get_instance();

        $page       = @$param['page'];
        $per_page   = @$param['per_page'];
        
        #handle untuk query ke database
        if(!$per_page): $per_page = 10; endif;
        if(!$page): $page = 1; endif;
        
        if($page!="1"): $first_row = (($page-1)*$per_page)+1;elseif($page==1): $first_row = 1;endif;
        
        if ($page <= 0) {
            $result["rest_no"] = "38";
            $result["reason"] = getMobileErrorText("38");
            return $result;
            exit();
        } else {
            $data = $ci->mgeneral->getWhere(array("tampil_partner" => "on"), "mstr_berita","mstr_berita_id","desc");
        }
        
        if (count($data) == 0) {
            $result["rest_no"] = "37";
            $result["reason"] = getMobileErrorText("37");
        } else {
            $result["rest_no"] = "0";
            
            $totaldata          = count($data);
            $page               = explode(".",$totaldata/$per_page);
            $sisaPage           = $totaldata%$per_page;
            if($sisaPage==0): $totalpage = $page[0]; else: $totalpage = $page[0]+1; endif;
            if($param['page']==""): $cpage = "1"; else: $cpage=$param['page']; endif;
            
            $newsData["current_page"] = $cpage;
            $newsData["total_page"]   = $totalpage;
            
            $get_data = $ci->mgeneral->getWhere(array("tampil_partner" => "on"), "mstr_berita", "mstr_berita_id", "desc", ($first_row), $per_page);
            
            for($i = 0; $i < count($get_data); $i++) {
                $get_data[$i]->mstr_berita_id = $ci->converter->encode($get_data[$i]->mstr_berita_id);
                unset($get_data[$i]->corp_id);
                unset($get_data[$i]->mstr_berita_status);
                unset($get_data[$i]->tampil_customer);
                unset($get_data[$i]->tampil_partner);
            }
            
            $newsData['news']   = isset($get_data) ? $get_data : array();
            $result["data"] = $newsData;
        }
        
        return $result;
    }
	
}