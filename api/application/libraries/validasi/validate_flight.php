<?php

class validate_flight 
{
	
	function __construct()
    {
		$ci =& get_instance();
		#$ci->load->library('validasi/validate_format');
		#$ci->load->model('mairport');
		#$ci->load->model('mbookdata');
    }
	function getErrorText($kode)
	{
		$ci =& get_instance();
		$ci->load->database('default', TRUE);
		$error	= $ci->mgeneral->getWhere(array('errorcode_code'=>$kode),"errorcode_list");
		return $error;
	}
	function search($data)
	{
		$ci =& get_instance();
		print_r($data);
		/*$cekKoneksi		 	= $ci->validate_format->cekStatusAirline($data['airline']);
		$cekKodeAirlines 	= $ci->validate_format->cekKodeAirline($data['airline']);
		$cekAirportFrom		= $ci->validate_format->cekKodeAirport($data['from']);
		$cekAirportTo		= $ci->validate_format->cekKodeAirport($data['to']);
		$cekRute			= $ci->mairport->cekRuteAirlines($data['airline'],$data['from'],$data['to']);
		
		if(trim($data['airline'])==""||trim($data['roundtrip'])==""||trim($data['from'])==""||trim($data['to'])==""||trim($data['depart'])==""||trim($data['adult'])=="")
		{
			$error	= $this->getErrorText("201");
			$result['error_no']	= "201";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		elseif($data['airline']=="4" && $data['infant']>= "1")
		{
			$error	= $this->getErrorText("402");
			$result['error_no']	= "402";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		elseif($data['airline']=="9" && $data['infant']>= "1")
		{
			$error	= $this->getErrorText("402");
			$result['error_no']	= "402";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		elseif($data['airline']=="5" && $data['infant']>= "1")
		{
			$error	= $this->getErrorText("402");
			$result['error_no']	= "402";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		elseif($data['adult']=="0")
		{
			$error	= $this->getErrorText("203");
			$result['error_no']	= "203";
			$result['error_msg']= $error[0]->errorcode_name." # jumlah adult ".$error[0]->errorcode_text;
		}
		elseif(trim($data['return'])=="" && $data['roundtrip']=="return")
		{
			$error	= $this->getErrorText("201");
			$result['error_no']	= "201";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		elseif($cekKodeAirlines['error_no']!="0") //cek kode airline
		{
			$result	= $cekKodeAirlines;
		}
		elseif($cekKoneksi['error_no']!="0") //cek kode airline
		{
			$result	= $cekKoneksi;
		}
		elseif($data['roundtrip']!="oneway" && $data['roundtrip']!="return")
		{
			$error	= $this->getErrorText("203");
			$result['error_no']	= "203";
			$result['error_msg']= $error[0]->errorcode_name." # format roundtrip ".$error[0]->errorcode_text;
		}
		elseif($cekAirportFrom['error_no']!="0") //cek kode airport from
		{
			$result	= $cekAirportFrom;
		}
		elseif($cekAirportTo['error_no']!="0") //cek kode airport to
		{
			$result	= $cekAirportTo;
		}
		elseif($ci->validate_format->cekDate($data['depart'])==false || $ci->validate_format->date_expired($data['depart'])==false)
		{
			$error	= $this->getErrorText("203");
			$result['error_no']	= "203";
			$result['error_msg']= $error[0]->errorcode_name." # format tgl berangkat ".$error[0]->errorcode_text;
		}
		elseif($ci->validate_format->cekDate($data['return'])==false && $data['roundtrip'] == "return")
		{
			$error	= $this->getErrorText("203");
			$result['error_no']	= "203";
			$result['error_msg']= $error[0]->errorcode_name." # format tgl kembali ".$error[0]->errorcode_text;
		}
		elseif(count($cekRute)=="0")
		{
			$error	= $this->getErrorText("401");
			$result['error_no']	= "401";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		else
		{
			$result['error_no']	= "0";
		}
		return $result;*/
	}
	function search_multi($data)
	{
		$ci =& get_instance();
		$ci->load->database('default', TRUE);
		$cekAirlineMulti	= $ci->validate_format->cekAirlinesMulti($data['airline']);
		$cekAirportFrom		= $ci->validate_format->cekKodeAirport($data['from']);
		$cekAirportTo		= $ci->validate_format->cekKodeAirport($data['to']);
		
		if(trim($data['airline'])==""||trim($data['roundtrip'])==""||trim($data['from'])==""||trim($data['to'])==""||trim($data['depart'])==""||trim($data['adult'])=="")
		{
			$error	= $this->getErrorText("201");
			$result['error_no']	= "201";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		elseif($data['adult']=="0")
		{
			$error	= $this->getErrorText("203");
			$result['error_no']	= "203";
			$result['error_msg']= $error[0]->errorcode_name." # jumlah adult ".$error[0]->errorcode_text;
		}
		elseif(trim($data['return'])=="" && $data['roundtrip']=="return")
		{
			$error	= $this->getErrorText("201");
			$result['error_no']	= "201";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		elseif($cekAirportFrom['error_no']!="0") //cek kode airport from
		{
			$result	= $cekAirportFrom;
		}
		elseif($cekAirportTo['error_no']!="0") //cek kode airport to
		{
			$result	= $cekAirportTo;
		}
		elseif($cekAirlineMulti['error_no']!="0")
		{
			$result = $cekAirlineMulti;
		}
		else
		{
			$result['error_no']	= "0";
		}
		return $result;
	}
	function price_search($airline,$data)
	{
		$ci =& get_instance();
		$cekKoneksi		 	= $ci->validate_format->cekStatusAirline($airline);
		if($cekKoneksi['error_no']!="0") //cek kode airline
		{
			$result	= $cekKoneksi;
		}
		elseif($data['fltype'] == "" || $data['from'] == "" || $data['to'] == "" || $data['tglP'] == "" || $data['adult'] == "" || $data['child'] == "" || $data['value']['value_1_a']=="")
		{
			$error	= $this->getErrorText("501");
			$result['error_no']	= "501";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		else
		{
			$result['error_no']	= "0";
		}
		return $result;
	}
	function price_detail($parameter)
	{
		$ci =& get_instance();
		$session_id		= $parameter['session_id'];
		$flight			= $parameter['flight'];
		$flightno		= $parameter['flightno'];
		$class			= $parameter['class_id'];
		
		$validSession	= $ci->validate_format->enkripsi($session_id);
		$dataSession	= $this->cekSessionId($ci->converter->decode($session_id));
		
		if($flight=="" || $flightno=="" || $class=="" || $session_id=="")
		{
			$error	= $this->getErrorText("201");
			$result['error_no']	= "201";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		elseif($validSession['error_no']!="0")
		{
			$result		= $validSession;
		}
		elseif($dataSession['error_no']!="0")
		{
			$result		= $dataSession;
		}
		elseif($flight!="depart" && $flight!="return")
		{
			$error	= $this->getErrorText("203");
			$result['error_no']	= "203";
			$result['error_msg']= $error[0]->errorcode_name." # format flight ".$error[0]->errorcode_text;
		}
		elseif($ci->validate_format->fno_format($flightno)=="false")
		{
			$error	= $this->getErrorText("203");
			$result['error_no']	= "203";
			$result['error_msg']= $error[0]->errorcode_name." # format flight number ".$error[0]->errorcode_text;
		}
		else
		{
			$result['error_no']	= "0";
		}
		
		return $result;
	}
	function search_detail($parameter)
	{
		$ci =& get_instance();
		$session_id		= $parameter['session_id'];
		$flightno1		= $parameter['flightno_1'];
		$class1			= $parameter['class_1'];
		$flightno2		= $parameter['flightno_2'];
		$class2			= $parameter['class_2'];
		
		$validSession	= $ci->validate_format->enkripsi($session_id);
		$dataSession	= $this->cekSessionId($ci->converter->decode($session_id));
		
		$sesData	= $ci->mgeneral->getWhere(array('session_id'=>$ci->converter->decode($session_id)),"session");
		foreach($sesData as $sd){ $search_info	= json_decode($sd->search_post); }
		
		if($validSession['error_no']!="0")
		{
			$result		= $validSession;
		}
		elseif($dataSession['error_no']!="0")
		{
			$result		= $dataSession;
		}
		elseif($roundtrip=="return" && $flightno2=="")
		{
			$error	= $this->getErrorText("201");
			$result['error_no']	= "201";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		elseif($roundtrip=="return" && $class2=="")
		{
			$error	= $this->getErrorText("201");
			$result['error_no']	= "201";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		elseif($ci->validate_format->fno_format($flightno1)=="false")
		{
			$error	= $this->getErrorText("203");
			$result['error_no']	= "203";
			$result['error_msg']= $error[0]->errorcode_name." # format flight number ".$error[0]->errorcode_text;
		}
		elseif($ci->validate_format->fno_format($flightno2)=="false" && $roundtrip=="return" )
		{
			$error	= $this->getErrorText("203");
			$result['error_no']	= "203";
			$result['error_msg']= $error[0]->errorcode_name." # format flight number ".$error[0]->errorcode_text;
		}
		else
		{
			$result['error_no']	= "0";
		}
		return $result;
	}
	function book($parameter)
	{
		$ci =& get_instance();
		$session_id		= $parameter['session_id'];
		$validSession	= $ci->validate_format->enkripsi($session_id);
		$dataSession	= $this->cekSessionId($ci->converter->decode($session_id));
		$sesData		= $ci->mgeneral->getWhere(array('session_id'=>$ci->converter->decode($session_id)),"session");
		$searchPost		= json_decode($sesData[0]->search_post);
		$validPsgr		= $this->cekPsgr($searchPost->adult,$searchPost->child,$searchPost->infant,$parameter['psgr']);
		
		if($validSession['error_no']!="0")
		{
			$result		= $validSession;
		}
		elseif($sesData[0]->book_data!="")
		{
			$error	= $this->getErrorText("603");
			$result['error_no']	= "603";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		elseif($parameter['cust_name']=="" && $parameter['cust_phone']=="")
		{
			$error	= $this->getErrorText("201");
			$result['error_no']	= "201";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		elseif($dataSession['error_no']!="0")
		{
			$result		= $dataSession;
		}
		elseif($sesData[0]->detail_post=="" || $sesData[0]->detail_data=="")
		{
			$error	= $this->getErrorText("902");
			$result['error_no']	= "902";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		elseif($validPsgr['error_no']!="0")
		{
			$result		= $validPsgr;
		}
		else
		{
			$result['error_no']	= "0";
		}
		return $result;
	}
	function issue($id)
	{
		$ci =& get_instance();
		$ci->load->database('sistem', TRUE);
		
		$bookdata	= $ci->mbookdata->get_detail($id);
		foreach($bookdata as $d1)
		{
			$bookStatus	= $d1->flight_data_status;
		}
			
		if(count($bookdata)=="0")
		{
			$error	= $this->getErrorText("701");
			$result['error_no']	= "701";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		elseif($bookStatus == "ctl" || $bookStatus == "ctl segment" || $bookStatus == "cancel")
		{
			$error	= $this->getErrorText("702");
			$result['error_no']	= "702";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		elseif($bookStatus == "issued" || $bookStatus == "ticketed" || $bookStatus == "refund")
		{
			$error	= $this->getErrorText("703");
			$result['error_no']	= "703";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		else
		{
			$result['error_no']	= "0";
		}
		return $result;
	}
	function cekPsgr($adult,$child,$infant,$psgr)
	{
		$ci =& get_instance();
		$tPsgr	= $adult+$child+$infant;
		
		foreach($psgr as $p){ $psgr_type[]	= $p['psgr_type'];	}
		$totalD	= array_count_values($psgr_type);
		$validP	= $this->cekPsgrData($psgr);
		
		if($tPsgr != count($psgr))
		{
			$error	= $this->getErrorText("601");
			$result['error_no']	= "601";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		elseif($adult!=$totalD['adult'])
		{
			$error	= $this->getErrorText("601");
			$result['error_no']	= "601";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		elseif($child!=$totalD['child'] && $child!="0")
		{
			$error	= $this->getErrorText("601");
			$result['error_no']	= "601";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		elseif($infant!=$totalD['infant'] && $infant!="0")
		{
			$error	= $this->getErrorText("601");
			$result['error_no']	= "601";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		elseif($validP==false)
		{
			$error	= $this->getErrorText("602");
			$result['error_no']	= "602";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		else
		{
			$result['error_no']	= "0";
		}
		
		return $result;
	}
	function cekPsgrData($psgr)
	{
		//echo "<pre>";
		foreach($psgr as $p)
		{
			//echo $p['psgr_type']." ".$p['id_psgr']."<br>";
			if($p['psgr_type']=="adult" && $p['id_psgr']=="")
			{
				//echo "1";
				return false; exit;
			}
			elseif($p['psgr_type']=="child" && $p['id_psgr']=="birthdate")
			{
				//echo "2";
				return false; exit;
			}
			elseif($p['first_name']=="" && $p['last_name']=="" && $p['title']=="")
			{
				//echo "3";
				return false; exit;
			}
			
			switch(strtolower($p['title']))
			{
				default:  return false; exit;
				case "mr";
				case "mrs";
				case "ms";
				case "chd";
				case "inf";
					$stat="ok";
				break; 
			}
			
		}
		return true;
	}
	function cekSessionId($id)
	{
		$ci =& get_instance();
		$ci->load->database('default', TRUE);
		
		if(is_numeric($id)){
			$sesData	= $ci->mgeneral->getWhere(array('session_id'=>$id),"session");
		}
		
		if(count($sesData)=="0")
		{
			$error	= $this->getErrorText("901");
			$result['error_no']	= "901";
			$result['error_msg']= $error[0]->errorcode_name." # session id ".$error[0]->errorcode_text;
		}
		else
		{
			$result['error_no']	= "0";
		}
		
		return $result;
	}
	
	function CekFlightSama($b2b_id,$id,$psgr)
	{
		$ci =& get_instance();
		#Ambil Data From Session
		$ci->load->database('default', TRUE);
		$sesData	= $ci->mgeneral->getWhere(array('session_id'=>$id),"session");
		$search_post = json_decode($sesData[0]->detail_data);
		
		#Ambil Data Schedule yang Sama
		$ci->load->database('sistem', TRUE);
		$schData		= $ci->mbookdata->b2b_hold_schedule($b2b_id,
															$search_post->search_info->from,
															$search_post->search_info->to,
															$search_post->depart_detail->airline_id,
															$search_post->return_detail->airline_id,
															$search_post->search_info->depart,
															$search_post->search_info->return,
															$search_post->search_info->roundtrip);
		
		if(count($schData)!=0){
			#Ambil Data Passengers Yang Samacdengan Flight Data Id sama
			foreach($psgr as $row){
				$psgrData	= $ci->mgeneral->getWhere(array('passengers_type'		=>$row['psgr_type'],
															'passengers_first_name'	=>$row['first_name'],
															'passengers_last_name'	=>$row['last_name'],
															'flight_data_id'		=>$schData[0]->flight_data_id),"passengers");
				}
			if(count($psgrData)!=0){
				$error	= $this->getErrorText("609");
				$result['error_no']	= "609";
				$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
			}else{
				$result['error_no']	= "0";
			}
		}
		else
		{
			$result['error_no']	= "0";
		}
		
		return $result;
	}
	
	function CekNama1Huruf($psgr)
	{
		$ci =& get_instance();
		$ci->load->database('sistem', TRUE);
		for($i=0;$i<count($psgr);$i++){
			$array = array($psgr[$i]['first_name'], $psgr[$i]['last_name']);
  			$name_psgr = implode(" ", $array);
			$strings = explode(" ",$name_psgr);
			foreach ($strings as $testcase) {
				if (strlen($testcase)=='1') {
					$error	= $this->getErrorText("604");
					$result['error_no']	= "604";
					$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;;
					return 	$result;
				} 
			}
		}
		
		$result	= array('error_no'	=> "0");
		return $result;
	}
	
	function CekNamaTidakAngka($psgr)
	{
/*		$ci =& get_instance();
		$ci->load->database('sistem', TRUE);
		for($i=0;$i<count($psgr);$i++){
			$array = array($psgr[$i]['first_name'], $psgr[$i]['last_name']);
  			$name_psgr = implode(" ", $array);
			$strings = explode(" ",$name_psgr);
			foreach ($strings as $testcase) {
				if (!ctype_alpha($testcase)) {
					$error	= $this->getErrorText("605");
					$result['error_no']	= "605";
					$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;;
					return 	$result;
				} 
			}
		}*/
		$result	= array('error_no'	=> "0");
		return $result;
	}
}
?>
