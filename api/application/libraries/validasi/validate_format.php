<?php

class validate_format
{
	
	function __construct()
    {
		$ci =& get_instance();
		$ci->load->model('mairport');
    }
	function getErrorText($kode)
	{
		$ci =& get_instance();
		$ci->load->database('default', TRUE);
		$error	= $ci->mgeneral->getWhere(array('errorcode_code'=>$kode),"errorcode_list");
		return $error;
	}
	function cekKodeAirline($kode)
	{
		$ci =& get_instance();
		$ci->load->database('default', TRUE);
		$dAir	= $ci->mgeneral->getWhere(array('airlines_id'=>$kode),"airlines");
		if(count($dAir)==0) //cek kode airline
		{
			$error	= $this->getErrorText("203");
			$result['error_no']	= "203";
			$result['error_msg']= $error[0]->errorcode_name." # kode airlines ".$error[0]->errorcode_text;
		}
		else
		{
			$result['error_no']	= "0";
		}
		return $result;
	}
	function cekKodeAirport($kode)
	{
		$ci =& get_instance();
		$ci->load->database('default', TRUE);
		$dAir	= $ci->mgeneral->getWhere(array('airport_code'=>$kode),"airport");
				
		if(strlen($kode)!="3" || count($dAir)==0 )
		{
			$error	= $this->getErrorText("203");
			$result['error_no']	= "203";
			$result['error_msg']= $error[0]->errorcode_name." # kode airport $kode ".$error[0]->errorcode_text;
		}
		else
		{
			$result['error_no']	= "0";
		}
		return $result;
	}
	function cekDate($date, $format = 'YYYY-MM-DD'){
		if(strlen($date) >= 8 && strlen($date) <= 10){
			$separator_only = str_replace(array('M','D','Y'),'', $format);
			$separator = $separator_only[0];
			if($separator){
				$regexp = str_replace($separator, "\\" . $separator, $format);
				$regexp = str_replace('MM', '(0[1-9]|1[0-2])', $regexp);
				$regexp = str_replace('M', '(0?[1-9]|1[0-2])', $regexp);
				$regexp = str_replace('DD', '(0[1-9]|[1-2][0-9]|3[0-1])', $regexp);
				$regexp = str_replace('D', '(0?[1-9]|[1-2][0-9]|3[0-1])', $regexp);
				$regexp = str_replace('YYYY', '\d{4}', $regexp);
				$regexp = str_replace('YY', '\d{2}', $regexp);
				if($regexp != $date && preg_match('/'.$regexp.'$/', $date)){
					foreach (array_combine(explode($separator,$format), explode($separator,$date)) as $key=>$value) {
						if ($key == 'YY') $year = '20'.$value;
						if ($key == 'YYYY') $year = $value;
						if ($key[0] == 'M') $month = $value;
						if ($key[0] == 'D') $day = $value;
					}
					if (checkdate($month,$day,$year)) return true;
				}
			}
		}
		return false;
	}
	function date_expired($date) 
	{
	  $dateNow	= date('Y-m-d');
	  $date1	= strtotime(date("Y-m-d", strtotime($date)));
	  $date2	= strtotime(date("Y-m-d", strtotime($dateNow)));
		  if($date1 >= $date2)
		  {
			  return true;
		  }
		  else
		  {
			  return false;
		  }
	}
	function cekCountryAirport($country)
	{
		$ci =& get_instance();
		$ci->load->database('default', TRUE);
		$dAir	= $ci->mgeneral->getWhere(array('country'=>$country),"airport");
			
		if(count($dAir)==0 )
		{
			$error	= $this->getErrorText("203");
			$result['error_no']	= "203";
			$result['error_msg']= $error[0]->errorcode_name." # country $country ".$error[0]->errorcode_text;
		}
		else
		{
			$result['error_no']	= "0";
		}
		return $result;
	}
	function departure_airport($airline)
	{
		$ci =& get_instance();
		$ci->load->database('default', TRUE);
		if($airline!="")
		{
			//cek kode airline
			$data	= $ci->mgeneral->getWhere(array('airlines_id'=>$airline),"airlines");
			
			if(count($data)==0)
			{
				$error				= $this->getErrorText('202');
				$result['error_no']	= "202";
				$result['error_msg']= $error[0]->errorcode_name." : kode airline ".$error[0]->errorcode_text;
			}
			else
			{
				$result['error_no']	= "0";
			}
		}
		else
		{
			$error				= $this->getErrorText('201');
			$result['error_no']	= "201";
			$result['error_msg']= $error[0]->errorcode_name." : ".$error[0]->errorcode_text;
		}
		
		return $result;
	}
	function arrival_airport($airline,$departure)
	{
		$ci =& get_instance();
		$ci->load->database('default', TRUE);
		if($airline!="" && $departure!="")
		{
			//cek kode airline
			$dataAirline	= $ci->mgeneral->getWhere(array('airlines_id'=>$airline),"airlines");
			$dataDeparture	= $ci->mgeneral->getWhere(array('airlines_id'=>$airline,'airport_code'=>$departure),"departure_airport");
			
			if(count($dataAirline)==0)
			{
				$error				= $this->getErrorText('202');
				$result['error_no']	= "202";
				$result['error_msg']= $error[0]->errorcode_name." : kode airline ".$error[0]->errorcode_text;
			}
			if(count($dataDeparture)==0)
			{
				$error				= $this->getErrorText('202');
				$result['error_no']	= "202";
				$result['error_msg']= $error[0]->errorcode_name." : departure airport ".$error[0]->errorcode_text;
			}
			else
			{
				$result['error_no']	= "0";
			}
		}
		else
		{
			$error				= $this->getErrorText('201');
			$result['error_no']	= "201";
			$result['error_msg']= $error[0]->errorcode_name." : ".$error[0]->errorcode_text;
		}
		
		return $result;
	}
	function cekStatusAirline($airline, $flightInt = NULL)
	{
		/*$ci =& get_instance();
		$ci->load->database('default', TRUE);
		$status	= $ci->mgeneral->getWhere(array('airlines_id'=>$airline),'airlines');
		if($status[0]->connection_status=="on")
		{
			if($flightInt == "internasional"){
				if($status[0]->airlines_type == "both" || $status[0]->airlines_type == 'internasional'){
					$result['error_no']	= "0";
				}
				else{
					$error	= $this->getErrorText("301");
					$result['error_no']	= "301";
					$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
				}
			}
			else{
				$result['error_no']	= "0";
			}
			
		}
		else
		{
			$error	= $this->getErrorText("301");
			$result['error_no']	= "301";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}*/
		$result['error_no']	= "0";
		return $result;
	}
	function cekAirlinesMulti($data)
	{
		$ci =& get_instance();
		if(strtolower($data)!="all")
		{
			$potAirlines	= explode(",",$data);
			if(count($potAirlines)=="0")
			{
				$error	= $this->getErrorText("203");
				$result['error_no']	= "203";
				$result['error_msg']= $error[0]->errorcode_name." # format airlines multi ".$error[0]->errorcode_text;
			}
			else
			{
				for($a=0;$a<count($potAirlines);$a++)
				{
					if(trim($potAirlines[$a])!="")
					{
						$cekKodeAirlines 	= $this->cekKodeAirline($potAirlines[$a]);
						if($cekKodeAirlines['error_no']!="0") //cek kode airline
						{
							$error	= $this->getErrorText("203");
							$result['error_no']	= "203";
							$result['error_msg']= $error[0]->errorcode_name." # format airlines multi ".$error[0]->errorcode_text;
							break;
						}
						else
						{
							$result['error_no']	= "0";
						}
					}
				}
			}
		}
		else
		{
			$result['error_no']	= "0";
		}
		return $result;
	}
	function enkripsi($data)
	{
		$ci =& get_instance();
		if($ci->converter->decode($data)=="" || strlen($data)<=10)
		{
			$result	= array('error_no'	=> "202",
							'error_msg'	=> "format input data salah");
		}
		else
		{
			$result['error_no']	= "0";
		}
		return $result;
	}
	function fno_format($data)
	{
		$pot	= substr($data,0,2);
		
		switch($pot)
		{
			case "SJ"; $st="true"; break; //sriwijaya
			case "QG"; $st="true"; break; //citilink
			case "MZ"; $st="true"; break; //merpati
			case "Y6"; $st="true"; break; //batavia
			case "GA"; $st="true"; break; //garuda
			case "KD"; $st="true"; break; //kalstar
			case "JT"; $st="true"; break; //lionair
			case "QZ"; $st="true"; break; //airasia
			case "AK"; $st="true"; break; //airasia
			case "FD"; $st="true"; break; //airasia thai
			case "D7"; $st="true"; break; //airasia x
			case "PQ"; $st="true"; break; //airasia phi
			case "JW"; $st="true"; break; //airasia jpn
			case "IW"; $st="true"; break; //Wingsiar
			case "ID"; $st="true"; break; //Batikair
			case "RI"; $st="true"; break; //mandala
			case "TR"; $st="true"; break; //Tiger Airways Singapore
			case "TT"; $st="true"; break; //Tiger Airways Australia
			case "DG"; $st="true"; break; //SEAIR
			case "TZ"; $st="true"; break; //Scoot
			case "SL"; $st="true"; break; //Thai Airlines
			case "XN"; $st="true"; break; //XpressAir
			case "IL"; $st="true"; break; //TriganaAir
			default : $st="false"; break;
		}
		
		return $st;
	}
	function cekPsgr($adult,$child,$infant,$psgr)
	{
		$tPsgr	= $adult+$child+$infant;
		for($a=0;$a<$tPsgr;$a++)
		{
			$no=$no+1;
			$pType	= $psgr[$a]['psgr_type'];
			
			switch($psgr[$a]['title'])
			{
				case "mr";
				case "mrs";
				case "ms";
				case "chd"; 
				case "inf";	
					$status	="OK";
				break;
			}
			
			switch($pType)
			{
				case "adult";
				case "child";
				case "infant";
					$status2="OK";
				break;
			}
			
			if($status!="OK")
			{
				$result['error_no']	= "401";
				$result['error_msg']= "format title salah";
				break;
			}
			if($status2!="OK")
			{
				$result['error_no']	= "402";
				$result['error_msg']= "format psgr salah";
				break;
			}
			elseif($psgr[$a]['first_name']==""||$psgr[$a]['last_name']=="")
			{
				$result['error_no']	= "403";
				$result['error_msg']= "data psgr tidak lengkap";
				break;
			}
			else
			{
				$result['error_no']	= "0";
			}
			
		}
		
		return $result;
	}
	
	function check_station_code($station){
		$ci =& get_instance();
		$ci->load->database('default', TRUE);
		$station 	= $ci->mgeneral->getWhere(array('station_code'=>$station),'station');
		if(count($station) > 0){
			$result['error_no'] = 0;
		}else{
			$result['error_no'] = 203;
			$error	= $this->getErrorText("203");
			$result['error_msg']= $error[0]->errorcode_name." # station code ".$error[0]->errorcode_text;
		}
		return $result;
	}
}
?>
