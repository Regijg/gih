<?php

class validate_post 
{
	
	function __construct()
    {
		$ci =& get_instance();
		$ci->load->model('mairport');
    }
	function getErrorText($kode)
	{
		$ci =& get_instance();
		$ci->load->database('default', TRUE);
		$error	= $ci->mgeneral->getWhere(array('errorcode_code'=>$kode),"errorcode_list");
		return $error;
	}
	function cekIp($akses_code,$ip)
	{
		$ci =& get_instance();
		$ci->load->database('default', TRUE);
		$b2b= $ci->mgeneral->getWhere(array('b2b_kode_akses'=>$akses_code),"b2b");
		
		if(count($b2b)=="0")
		{
			$error				= $this->getErrorText('101');
			$result['error_no']	= "101";
			$result['error_msg']= $error[0]->errorcode_name." : ".$error[0]->errorcode_text;
		}
		/*elseif($b2b[0]->b2b_IP_akses!=$ip)
		{
			$error				= $this->getErrorText('102');
			$result['error_no']	= "102";
			$result['error_msg']= $error[0]->errorcode_name." : ".$error[0]->errorcode_text;
		}*/
		else
		{
			$result['error_no']	= "0";
		}
		
		return $result;
	}
	function cekAplikasi($app,$action)
	{
		$ci =& get_instance();
		$ci->load->database('default', TRUE);
		$aplikasi	= $ci->mgeneral->getWhere(array('app'=>$app),"daftar_service");
		$actionD	= $ci->mgeneral->getWhere(array('action'=>$action),"daftar_service");
		
		if(count($aplikasi)=="0")
		{
			$error				= $this->getErrorText('103');
			$result['error_no']	= "103";
			$result['error_msg']= $error[0]->errorcode_name." : ".$error[0]->errorcode_text;
		}
		elseif(count($actionD)=="0")
		{
			$error				= $this->getErrorText('104');
			$result['error_no']	= "104";
			$result['error_msg']= $error[0]->errorcode_name." : ".$error[0]->errorcode_text;
		}
		else
		{
			$result['error_no']	= "0";
		}
		
		return $result;
	}
	function akses($ip,$ua,$akses_code)
	{
		$ci =& get_instance();
		$kodeAkses	= $this->cekIp($akses_code,$ip);
		
		if($ua!="AT2-JSN")
		{
			$error				= $this->getErrorText('101');
			$result['error_no']	= "101";
			$result['error_msg']= $error[0]->errorcode_name." : ".$error[0]->errorcode_text;
		}
		elseif($kodeAkses['error_no']!="0")
		{
			$result	= $kodeAkses;
		}
		else
		{
			$result['error_no']	= "0";
		}
		
		return $result;
	}
	function cekStatusAirline($airline)
	{
		$ci =& get_instance();
		$ci->load->database('default', TRUE);
		$status	= $ci->mgeneral->getWhere(array('airlines_id'=>$airline),'airlines');
		if($status[0]->connection_status=="off")
		{
			$error	= $this->getErrorText("301");
			$result['error_no']	= "301";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		else
		{
			$result['error_no']	= "0";
		}
		return $result;
	}
	function cekKodeAirline($kode)
	{
		$ci =& get_instance();
		$ci->load->database('default', TRUE);
		$dAir	= $ci->mgeneral->getWhere(array('airlines_id'=>$kode),"airlines");
		if(count($dAir)==0) //cek kode airline
		{
			$error	= $this->getErrorText("203");
			$result['error_no']	= "203";
			$result['error_msg']= $error[0]->errorcode_name." # kode airlines ".$error[0]->errorcode_text;
		}
		else
		{
			$result['error_no']	= "0";
		}
		return $result;
	}
	function cekKodeAirport($kode)
	{
		$ci =& get_instance();
		$ci->load->database('default', TRUE);
		$dAir	= $ci->mgeneral->getWhere(array('airport_code'=>$kode),"airport");
				
		if(strlen($kode)!="3" || count($dAir)==0 )
		{
			$error	= $this->getErrorText("203");
			$result['error_no']	= "203";
			$result['error_msg']= $error[0]->errorcode_name." # kode airport $kode ".$error[0]->errorcode_text;
		}
		else
		{
			$result['error_no']	= "0";
		}
		return $result;
	}
	function cekDate($date, $format = 'YYYY-MM-DD'){
		if(strlen($date) >= 8 && strlen($date) <= 10){
			$separator_only = str_replace(array('M','D','Y'),'', $format);
			$separator = $separator_only[0];
			if($separator){
				$regexp = str_replace($separator, "\\" . $separator, $format);
				$regexp = str_replace('MM', '(0[1-9]|1[0-2])', $regexp);
				$regexp = str_replace('M', '(0?[1-9]|1[0-2])', $regexp);
				$regexp = str_replace('DD', '(0[1-9]|[1-2][0-9]|3[0-1])', $regexp);
				$regexp = str_replace('D', '(0?[1-9]|[1-2][0-9]|3[0-1])', $regexp);
				$regexp = str_replace('YYYY', '\d{4}', $regexp);
				$regexp = str_replace('YY', '\d{2}', $regexp);
				if($regexp != $date && preg_match('/'.$regexp.'$/', $date)){
					foreach (array_combine(explode($separator,$format), explode($separator,$date)) as $key=>$value) {
						if ($key == 'YY') $year = '20'.$value;
						if ($key == 'YYYY') $year = $value;
						if ($key[0] == 'M') $month = $value;
						if ($key[0] == 'D') $day = $value;
					}
					if (checkdate($month,$day,$year)) return true;
				}
			}
		}
		return false;
	}
	function date_expired($date) 
	{
	  $dateNow	= date('Y-m-d');
	  $date1	= strtotime(date("Y-m-d", strtotime($date)));
	  $date2	= strtotime(date("Y-m-d", strtotime($dateNow)));
		  if($date1 >= $date2)
		  {
			  return true;
		  }
		  else
		  {
			  return false;
		  }
	}
	function cekCountryAirport($country)
	{
		$ci =& get_instance();
		$ci->load->database('default', TRUE);
		$dAir	= $ci->mgeneral->getWhere(array('country'=>$country),"airport");
			
		if(count($dAir)==0 )
		{
			$error	= $this->getErrorText("203");
			$result['error_no']	= "203";
			$result['error_msg']= $error[0]->errorcode_name." # country $country ".$error[0]->errorcode_text;
		}
		else
		{
			$result['error_no']	= "0";
		}
		return $result;
	}
	function departure_airport($airline)
	{
		$ci =& get_instance();
		$ci->load->database('default', TRUE);
		if($airline!="")
		{
			//cek kode airline
			$data	= $ci->mgeneral->getWhere(array('airlines_id'=>$airline),"airlines");
			
			if(count($data)==0)
			{
				$error				= $this->getErrorText('202');
				$result['error_no']	= "202";
				$result['error_msg']= $error[0]->errorcode_name." : kode airline ".$error[0]->errorcode_text;
			}
			else
			{
				$result['error_no']	= "0";
			}
		}
		else
		{
			$error				= $this->getErrorText('201');
			$result['error_no']	= "201";
			$result['error_msg']= $error[0]->errorcode_name." : ".$error[0]->errorcode_text;
		}
		
		return $result;
	}
	function arrival_airport($airline,$departure)
	{
		$ci =& get_instance();
		$ci->load->database('default', TRUE);
		if($airline!="" && $departure!="")
		{
			//cek kode airline
			$dataAirline	= $ci->mgeneral->getWhere(array('airlines_id'=>$airline),"airlines");
			$dataDeparture	= $ci->mgeneral->getWhere(array('airlines_id'=>$airline,'airport_code'=>$departure),"departure_airport");
			
			if(count($dataAirline)==0)
			{
				$error				= $this->getErrorText('202');
				$result['error_no']	= "202";
				$result['error_msg']= $error[0]->errorcode_name." : kode airline ".$error[0]->errorcode_text;
			}
			if(count($dataDeparture)==0)
			{
				$error				= $this->getErrorText('202');
				$result['error_no']	= "202";
				$result['error_msg']= $error[0]->errorcode_name." : departure airport ".$error[0]->errorcode_text;
			}
			else
			{
				$result['error_no']	= "0";
			}
		}
		else
		{
			$error				= $this->getErrorText('201');
			$result['error_no']	= "201";
			$result['error_msg']= $error[0]->errorcode_name." : ".$error[0]->errorcode_text;
		}
		
		return $result;
	}
	function airlines_akun($akses_code,$airline)
	{
		$ci =& get_instance();
		$ci->load->database('default', TRUE);
		//cek kode akses
		$dataB2B	= $ci->mgeneral->getWhere(array('b2b_kode_akses'=>$akses_code),"b2b");
		if($dataB2B[0]->b2b_airlines_akun!="self")
		{
			$error				= $this->getErrorText('105');
			$result['error_no']	= "105";
			$result['error_msg']= $error[0]->errorcode_name." : ".$error[0]->errorcode_text;
		}
		else
		{
			//cek kode airline
			$dataAirline	= $ci->mgeneral->getWhere(array('airlines_id'=>$airline),"airlines");
			if(count($dataAirline)==0 && $airline!="")
			{
				$error				= $this->getErrorText('202');
				$result['error_no']	= "202";
				$result['error_msg']= $error[0]->errorcode_name." : kode airline ".$error[0]->errorcode_text;
			}
			else
			{
				$result['error_no']	= "0";
			}
		}
		return $result;
	}
	function cekDeposit($airline)
	{
		$ci =& get_instance();
		$ci->load->database('default', TRUE);
		$cekKoneksi		 = $this->cekStatusAirline($airline);
		$cekKodeAirlines = $this->cekKodeAirline($airline);
		
		if(trim($airline)=="") //cek isian data airline
		{
			$error	= $this->getErrorText("201");
			$result['error_no']	= "201";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		elseif($cekKodeAirlines['error_no']!="0") //cek kode airline
		{
			$result	= $cekKodeAirlines;
		}
		elseif($cekKoneksi['error_no']!="0") //cek kode airline
		{
			$result	= $cekKoneksi;
		}
		else
		{
			$result['error_no']	= "0";
		}
		return $result;
	}
	function search($data)
	{
		$ci =& get_instance();
		$ci->load->database('default', TRUE);
		$cekKoneksi		 	= $this->cekStatusAirline($data['airline']);
		$cekKodeAirlines 	= $this->cekKodeAirline($data['airline']);
		$cekAirportFrom		= $this->cekKodeAirport($data['from']);
		$cekAirportTo		= $this->cekKodeAirport($data['to']);
		$cekRute			= $ci->mairport->cekRuteAirlines($data['airline'],$data['from'],$data['to']);
		
		if(trim($data['airline'])==""||trim($data['roundtrip'])==""||trim($data['from'])==""||trim($data['to'])==""||trim($data['depart'])==""||trim($data['adult'])=="")
		{
			$error	= $this->getErrorText("201");
			$result['error_no']	= "201";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		elseif($data['adult']=="0")
		{
			$error	= $this->getErrorText("203");
			$result['error_no']	= "203";
			$result['error_msg']= $error[0]->errorcode_name." # jumlah adult ".$error[0]->errorcode_text;
		}
		elseif(trim($data['return'])=="" && $data['roundtrip']=="return")
		{
			$error	= $this->getErrorText("201");
			$result['error_no']	= "201";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		elseif($cekKodeAirlines['error_no']!="0") //cek kode airline
		{
			$result	= $cekKodeAirlines;
		}
		elseif($cekKoneksi['error_no']!="0") //cek kode airline
		{
			$result	= $cekKoneksi;
		}
		elseif($data['roundtrip']!="oneway" && $data['roundtrip']!="return")
		{
			$error	= $this->getErrorText("203");
			$result['error_no']	= "203";
			$result['error_msg']= $error[0]->errorcode_name." # format roundtrip ".$error[0]->errorcode_text;
		}
		elseif($cekAirportFrom['error_no']!="0") //cek kode airport from
		{
			$result	= $cekAirportFrom;
		}
		elseif($cekAirportTo['error_no']!="0") //cek kode airport to
		{
			$result	= $cekAirportTo;
		}
		elseif($this->cekDate($data['depart'])==false || $this->date_expired($data['depart'])==false)
		{
			$error	= $this->getErrorText("203");
			$result['error_no']	= "203";
			$result['error_msg']= $error[0]->errorcode_name." # format tgl berangkat ".$error[0]->errorcode_text;
		}
		elseif($this->cekDate($data['return'])==false && $data['roundtrip'] == "return")
		{
			$error	= $this->getErrorText("203");
			$result['error_no']	= "203";
			$result['error_msg']= $error[0]->errorcode_name." # format tgl kembali ".$error[0]->errorcode_text;
		}
		elseif(count($cekRute)=="0")
		{
			$error	= $this->getErrorText("401");
			$result['error_no']	= "401";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		else
		{
			$result['error_no']	= "0";
		}
		return $result;
	}
	function search_multi($data)
	{
		$ci =& get_instance();
		$ci->load->database('default', TRUE);
		$cekAirlineMulti	= $this->cekAirlinesMulti($data['airline']);
		$cekAirportFrom		= $this->cekKodeAirport($data['from']);
		$cekAirportTo		= $this->cekKodeAirport($data['to']);
		
		if(trim($data['airline'])==""||trim($data['roundtrip'])==""||trim($data['from'])==""||trim($data['to'])==""||trim($data['depart'])==""||trim($data['adult'])=="")
		{
			$error	= $this->getErrorText("201");
			$result['error_no']	= "201";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		elseif($data['adult']=="0")
		{
			$error	= $this->getErrorText("203");
			$result['error_no']	= "203";
			$result['error_msg']= $error[0]->errorcode_name." # jumlah adult ".$error[0]->errorcode_text;
		}
		elseif(trim($data['return'])=="" && $data['roundtrip']=="return")
		{
			$error	= $this->getErrorText("201");
			$result['error_no']	= "201";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		elseif($cekAirportFrom['error_no']!="0") //cek kode airport from
		{
			$result	= $cekAirportFrom;
		}
		elseif($cekAirportTo['error_no']!="0") //cek kode airport to
		{
			$result	= $cekAirportTo;
		}
		elseif($cekAirlineMulti['error_no']!="0")
		{
			$result = $cekAirlineMulti;
		}
		else
		{
			$result['error_no']	= "0";
		}
		return $result;
	}
	function cekAirlinesMulti($data)
	{
		$ci =& get_instance();
		if($data!="all")
		{
			$potAirlines	= explode(",",$data);
			if(count($potAirlines)=="0")
			{
				$error	= $this->getErrorText("203");
				$result['error_no']	= "203";
				$result['error_msg']= $error[0]->errorcode_name." # format airlines multi ".$error[0]->errorcode_text;
			}
			else
			{
				for($a=0;$a<count($potAirlines);$a++)
				{
					if(trim($potAirlines[$a])!="")
					{
						$cekKodeAirlines 	= $this->cekKodeAirline($potAirlines[$a]);
						if($cekKodeAirlines['error_no']!="0") //cek kode airline
						{
							$error	= $this->getErrorText("203");
							$result['error_no']	= "203";
							$result['error_msg']= $error[0]->errorcode_name." # format airlines multi ".$error[0]->errorcode_text;
							break;
						}
						else
						{
							$result['error_no']	= "0";
						}
					}
				}
			}
		}
		else
		{
			$result['error_no']	= "0";
		}
		return $result;
	}
	function search_detail($airline,$data)
	{
		$ci =& get_instance();
		$cekKoneksi		 	= $this->cekStatusAirline($airline);
		if($cekKoneksi['error_no']!="0") //cek kode airline
		{
			$result	= $cekKoneksi;
		}
		else
		{
			$result['error_no']	= "0";
		}
		return $result;
	}
	function book_flight($parameter)
	{
		$ci =& get_instance();
		$pageDat	= explode("|",$ci->converter->decode($parameter['page_ref']));
		$roundtrip	= $pageDat[0];
		$airline1	= $parameter['airline1'];
		$value1		= $ci->converter->decode($parameter['value1']);
		$airline2	= $parameter['airline2'];
		$value2		= $ci->converter->decode($parameter['value2']);
		$cekPsgr	= $this->cekPsgr($pageDat[5],$pageDat[6],$pageDat[7],$parameter['psgr']);
		
		$cekKoneksi		 	= $this->cekStatusAirline($airline1);
		if($roundtrip=="return"){
			$cekKoneksi2	= $this->cekStatusAirline($airline2);
		}
		
		if($airline1==""||$value1==""||$parameter['cust_name']==""||$parameter['cust_phone']="")
		{
			$error	= $this->getErrorText("201");
			$result['error_no']	= "201";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		elseif($airline2=="" && $roundtrip=="return")
		{
			$error	= $this->getErrorText("201");
			$result['error_no']	= "201";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		elseif($value2=="" && $roundtrip=="return")
		{
			$error	= $this->getErrorText("201");
			$result['error_no']	= "201";
			$result['error_msg']= $error[0]->errorcode_name." # ".$error[0]->errorcode_text;
		}
		elseif($cekKoneksi['error_no']!="0") //cek kode airline
		{
			$result	= $cekKoneksi;
		}
		elseif($cekKoneksi2['error_no']!="0" && $roundtrip=="return" )
		{
			$result	= $cekKoneksi2;
		}
		elseif($cekPsgr['error_no']!="0") //cek kode airline
		{
			$result	= $cekPsgr;
		}
		else
		{
			$result['error_no']	= "0";
		}
		return $result;
	}
	function cekPsgr($adult,$child,$infant,$psgr)
	{
		$tPsgr	= $adult+$child+$infant;
		for($a=0;$a<$tPsgr;$a++)
		{
			$no=$no+1;
			$pType	= $psgr[$a]['psgr_type'];
			
			switch($psgr[$a]['title'])
			{
				case "mr";
				case "mrs";
				case "ms";
				case "chd"; 
				case "inf";	
					$status	="OK";
				break;
			}
			
			switch($pType)
			{
				case "adult";
				case "child";
				case "infant";
					$status2="OK";
				break;
			}
			
			if($status!="OK")
			{
				$result['error_no']	= "401";
				$result['error_msg']= "format title salah";
				break;
			}
			if($status2!="OK")
			{
				$result['error_no']	= "402";
				$result['error_msg']= "format psgr salah";
				break;
			}
			elseif($psgr[$a]['first_name']==""||$psgr[$a]['last_name']=="")
			{
				$result['error_no']	= "403";
				$result['error_msg']= "data psgr tidak lengkap";
				break;
			}
			else
			{
				$result['error_no']	= "0";
			}
			
		}
		
		return $result;
	}
}
?>
