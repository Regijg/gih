<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ticket
{
	protected 	$ci;
	protected 	$url_pdf = '';
	protected 	$url_html = '';

	public function __construct() {
        $this->ci =& get_instance();
	}

	public function index($action, $parameter) {
		switch ($action) {
			case 'pdf': 	$result = $this->get_pdf($parameter); 	break;
			case 'html': 	$result = $this->get_html($parameter); 	break;
			case 'email': 	$result = $this->get_email($parameter); break;
		}

		return $result;
	}

	private function get_pdf($parameter) {
		switch ($parameter['product']) {
			case 'flight':
				$result = $this->flight_pdf($parameter);
				break;
			case 'hotel':
				$result = $this->hotel_pdf($parameter);
			break;
		}

		return $result;
	}

	private function get_html($parameter) {
		switch ($parameter['product']) {
			case 'flight':
				if ($parameter['type'] == "ticket") {
					$result = $this->flight($parameter);
				} else if ($parameter['type'] == "invoice") {
					$result = $this->flight_invoice($parameter);
				} else {
					$result = array(
						'ticket' 	=> $this->flight($parameter),
						'invoice' 	=> $this->flight_invoice($parameter)
					);
				}
				break;
		}

		return $result;
	}

	/******** PLAN: ALL TICKET PLACE INSIDE LIBRARY ********/

	/******** FLIGHT ********/
	private function get_flight_data($book_id) {
		$ci =& get_instance();

		$data['dt_flight'] 			= $ci->mgeneral->getWhere(array('dtf_id' => $book_id), 'dt_flight');
		$data['dt_flight_pax'] 		= $ci->mgeneral->getWhere(array('dtf_id' => $book_id), 'dt_flight_pax');
		$data['dt_flight_schedule'] = $ci->mgeneral->getWhere(array('dtf_id' => $book_id), 'dt_flight_schedule');

		return $data;
	}

	public function flight($parameter) {
		$ci =& get_instance();
		$book_id = $ci->converter->decode($parameter['book_id']);

		$data['flight'] = $this->get_flight_data($book_id);

		$html = $ci->load->view('ticket/flight/html_ticket', $data, TRUE);
		return $html;
	}

	public function flight_invoice($parameter) {
		$ci =& get_instance();

		$html = $ci->load->view('ticket/flight/html_invoice', $data, TRUE);
		return $html;
	}

	public function flight_pdf($parameter) {
		$ci =& get_instance();
		$book_id = $ci->converter->decode($parameter['book_id']);

		$data['flight'] = $this->get_flight_data($book_id);

		$html = $ci->load->view('ticket/flight/pdf_ticket', $data, TRUE); // place for load view ticket html here

		$ci->load->library('html2pdf/html2pdf');
		$html2pdf = new HTML2PDF('P','A4','en',true,'UTF-8',array(0, 0, 0, 0));
		$ci->html2pdf->WriteHTML($html);
		return $ci->html2pdf->Output('', true); // return as binary
		// return $ci->html2pdf->Output('', "F"); // save as file on server
	}
	
	public function hotel_pdf($parameter) {
		$ci =& get_instance();
		
		$book_id 		= $ci->converter->decode($parameter['book_id']);
		$data['booking']= $ci->mgeneral->getWhere(array('id_hotel_data'=>$book_id),"dt_hotel");
		
		$html = $ci->load->view('ticket/hotel/voucher_pdf', $data, TRUE); // place for load view ticket html here
		echo $html;
		#$ci->load->library('html2pdf/html2pdf');
		#$html2pdf = new HTML2PDF('P','A4','en',true,'UTF-8',array(0, 0, 0, 0));
		#$ci->html2pdf->WriteHTML($html);
		#return $ci->html2pdf->Output('', true); // return as binary
		// return $ci->html2pdf->Output('', "F"); // save as file on server
	}
}

/* End of file ticket_library.php */
/* Location: ./application/libraries/ticket_library.php */
