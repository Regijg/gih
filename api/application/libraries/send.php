<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class send {
	
	var $url = "https://reguler.zenziva.net/apps/smsapi.php?userkey=bxf0y9&passkey=tabstech15";

	function sms($no_tujuan,$isi_pesan) 
	{	
		$ci =& get_instance();
		$ci->load->library("curl");
		
		$uri		= $this->url."&nohp=".$no_tujuan."&pesan=".urlencode($isi_pesan);
		
		$result 	= $ci->curl->get($uri, $uri);
		
		return $result;
	}
	
	function email($to, $subject, $message) {

		$CI =& get_instance();

		# Load library email
		$CI->load->library('email');

		# Initialize config
		$config['mailtype'] = 'html';
		$CI->email->initialize($config);

		$CI->email->from('support@pacifictravel.id', 'Pacific Travel'); ### email disesuaikan dengan travel agent
		$CI->email->to($to);
		$CI->email->subject($subject);
		$CI->email->message($message);

		if ($CI->email->send()) {
			$result['rest_no'] 	= "0";
			$result['message'] = "email berhasil dikirim";
			return $result;
		} else {			
			$result['rest_no'] 	= "1";
			$result['reason'] = "email gagal dikirim";
			return $result;
		}
	}

	function emailAttach($to, $subject, $message, $path, $sender_name, $return_address, $auto_send = '1') {

		$CI =& get_instance();

		# Load library email
		$CI->load->library('email');

		# Initialize config
		$config['mailtype'] = 'html';
		$CI->email->initialize($config);

		if ($sender_name == "" && $return_address == ""):
			$CI->email->from('support@pacifictravel.id', 'Pacific Travel'); ### email disesuaikan dengan travel agent
		else:
			$CI->email->from($return_address, $sender_name);
		endif;
		$CI->email->to($to);
		$CI->email->subject($subject);
		$CI->email->message($message);

		if ($auto_send == '0'):
			$CI->email->bcc('hyoichi@rocketmail.com');
		endif;


		if (!empty($path['invoice'])) {
			$CI->email->attach($path['pdf']);
			$CI->email->attach($path['invoice']);
		} else {
			$CI->email->attach($path['pdf']);
		}

		if ($CI->email->send()) {
			$result['rest_no'] = "0";
			if ($path['invoice'] != "") {
				unlink($path['invoice']);				
			}
			#unlink($path['pdf']);
			$result['message'] = "email berhasil dikirim";
			return $result;
		} else {
			$result['rest_no'] = "1";
			if ($path['invoice'] != "") {
				unlink($path['invoice']);				
			}
			unlink($path['pdf']);
			$result['reason'] = "email gagal dikirim";
			return $result;
		}
	}
	
	public function new_email($subject, $mailto, $message) {
        // multiple recipients
        $to = $mailto;
        
        $from_mail = "hyoichi@rocketmail.com";
        $from_name = "test";
        $replyto = "hyoichi@rocketmail.com";

        // message
        $msg = $message;

        // To send HTML mail, the Content-type header must be set
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

        // Additional headers
        $headers .= "From: " . $from_name . " <" . $from_mail . ">\r\n";
        $headers .= "Reply-To: " . $replyto . "\r\n";
        $headers .= 'X-Mailer: PHP/' . phpversion();

        // Mail it
        mail($to, $subject, $msg, $headers);
	}
}
		
?>
