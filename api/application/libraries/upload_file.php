<?php
class Upload_file
{	
	function upload_img($title) 
	{
		$ci = &get_instance();
		$config['upload_path']		= "./uploads/corp_logo/";
		$config['allowed_types']	= 'gif|jpg|png|jpeg|bmp|tiff';
		#$config['max_size']     	= '2048'; #1MB = 1024
		$config['file_name']		= $title."-".date('YmdHis');

		$ci->load->library('upload', $config);
			
		if($ci->upload->do_upload("img")):
			
			$data	 		= $ci->upload->data();
			
			#create thumbnail / resize image
			$ci->load->library('image_lib');
			$large_image	= $this->resize("./uploads/corp_logo/".$data['file_name'],"./uploads/corp_logo/","300","100");
			#$medium_image	= $this->resize("./static/upload/".$data['file_name'],"./static/upload/medium/","400","156");
			#$mini_image		= $this->resize("./uploads/corp_logo/".$data['file_name'],"./uploads/thumbnails/","50","50"); 
			
			$result = array('err_no'	=> "0",
							'filename'	=> $data['file_name']);
		else:
		
			$result	= array('err_no'	=> "206",
							'err_msg'	=> $ci->upload->display_errors('',''));
		
		endif;
		
		return $result;
	}
	
	function upload_img2($title) 
	{
		$ci = &get_instance();
		$config['upload_path']		= "./static/upload";
		$config['allowed_types']	= 'gif|jpg|png|jpeg|bmp|tiff';
		#$config['max_size']     	= '2048'; #1MB = 1024
		$config['file_name']		= preg_replace("/[^A-Za-z]/","_",$title)."_".date('YmdHis');
	
		$ci->load->library('upload', $config);
			
		if($ci->upload->do_upload("img")):
			
			$data	 		= $ci->upload->data();
			
			#create thumbnail / resize image
			$ci->load->library('image_lib');
			$large_image	= $this->resize("./static/upload/".$data['file_name'],"./static/upload/","413","381");
			#$medium_image	= $this->resize("./static/upload/".$data['file_name'],"./static/upload/medium/","400","156");
			#$mini_image		= $this->resize("./static/upload/".$data['file_name'],"./static/upload/mini/","50","50"); 
			
			$result = array('err_no'	=> "0",
							'filename'	=> $data['file_name']);
		else:
		
			$result	= array('err_no'	=> "206",
							'err_msg'	=> $ci->upload->display_errors('',''));
		
		endif;
		
		return $result;
	}
	
	function upload_slider($title) 
	{
		$ci = &get_instance();
		$config['upload_path']		= "./static/upload";
		$config['allowed_types']	= 'gif|jpg|png|jpeg|bmp|tiff';
		#$config['max_size']     	= '2048'; #1MB = 1024
		$config['file_name']		= preg_replace("/[^A-Za-z]/","_",$title)."_".date('YmdHis');
	
		$ci->load->library('upload', $config);
			
		if($ci->upload->do_upload("img")):
			
			$data	 		= $ci->upload->data();
			
			#create thumbnail / resize image
			$ci->load->library('image_lib');
			$large_image	= $this->resize("./static/upload/".$data['file_name'],"./static/upload/","1080","516");
			#$medium_image	= $this->resize("./static/upload/".$data['file_name'],"./static/upload/medium/","400","156");
			#$mini_image		= $this->resize("./static/upload/".$data['file_name'],"./static/upload/mini/","50","50"); 
			
			$result = array('err_no'	=> "0",
							'filename'	=> $data['file_name']);
		else:
		
			$result	= array('err_no'	=> "206",
							'err_msg'	=> $ci->upload->display_errors('',''));
		
		endif;
		
		return $result;
	}
	
	function resize($file_asli,$path,$width,$height)
	{
		$ci = &get_instance();

		$info = getimagesize($file_asli);

        if ($info[0] > 0 && $info[1] > 0 && $info['mime'])
        {
            if ($info[0] > $width OR $info[1] > $height)
            {
                $dim = (intval($info[0]) / intval($info[1])) - ($width / $height);

                // Resize
                $config_resize['image_library']     = 'gd2';
                $config_resize['source_image']      = "$file_asli";
                $config_resize['maintain_ratio']    = TRUE;
                $config_resize['new_image']    		= "$path";
                $config_resize['master_dim']    	= (($dim > 0) ? 'width' : 'height');
                $config_resize['width']         	= $width;
                $config_resize['height']        	= $height;

                // Resize process
                $ci->image_lib->initialize($config_resize);
                $ci->image_lib->resize();
                $ci->image_lib->clear();
            }
            
            $images[] = $file_name;
        }
		
	}
	
}
	
?>