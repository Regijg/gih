<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
class Curl {
	
	var $headers;
	var $user_agent;
	var $compression;
	var $cookie_file;
	var $proxy;
	var $proxy1="";
	var $cookies;

	function __construct()
	{
		$cookies=TRUE; $cookie='cookie/cookie.txt'; $compression='gzip'; $proxy='';
		$this->headers[] = 'Accept: text/html,application/xhtml+xml,application/xml';
		$this->headers[] = 'Connection: Keep-Alive';
		#$this->headers[] = 'Content-type: application/x-www-form-urlencoded;charset=UTF-8';
		$this->user_agent = 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0';
		#Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13
		$this->compression=$compression;
		$this->proxy=$proxy;
		$this->cookies=$cookies;
	}
	
	function get($url,$url2,$cookie) 
	{	
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
		curl_setopt($ch, CURLOPT_FAILONERROR, 1);
		curl_setopt($ch, CURLOPT_REFERER, $url2);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
		curl_setopt($ch,CURLOPT_ENCODING , $this->compression);
		curl_setopt($ch, CURLOPT_HTTPGET, TRUE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, '300');
		curl_setopt($ch, CURLOPT_TIMEOUT, 300);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		if ($this->proxy) curl_setopt($ch, CURLOPT_PROXY, $this->proxy);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		$result = curl_exec($ch);
		
		if($result === false)
		{
			$resultData['rest_no']		= curl_errno($ch);
			$resultData['reason']	= curl_error($ch);
		}
		else
		{
			$resultData['rest_no']		= "0";
			$resultData['result']		= $result;
		}
		
		return $resultData;
		curl_close($ch);
	}
		
	function post($url,$url2,$data,$cookie) 
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
		curl_setopt($ch, CURLOPT_REFERER, $url2);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_ENCODING , $this->compression);
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, '300');
		curl_setopt($ch, CURLOPT_TIMEOUT , '300' );
		curl_setopt($ch, CURLOPT_MAXREDIRS , '3' );
		//curl_setopt($ch, CURLOPT_FAILONERROR,true);
		
		//for windows
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
		curl_setopt($ch, CURLOPT_COOKIESESSION, false);
		 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		if ($this->proxy) curl_setopt($ch, CURLOPT_PROXY, $this->proxy);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
		$result = curl_exec($ch);
		
		if($result === false)
		{
			$resultData['rest_no']		= curl_errno($ch);
			$resultData['reason']	= curl_error($ch);
		}
		else
		{
			$resultData['rest_no']		= "0";
			$resultData['result']		= $result;
		}
		
		return $resultData;
		curl_close($ch);
	}

	function garuda_login($url,$url2,$data,$cookie)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
		curl_setopt($ch, CURLOPT_REFERER, $url2);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, '60');
		curl_setopt($ch, CURLOPT_TIMEOUT , '120' );
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
		curl_setopt($ch, CURLOPT_COOKIESESSION, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FAILONERROR,true);
		$result = curl_exec($ch);
		if($result === false)
		{
			$resultData['rest_no']		= curl_errno($ch);
			$resultData['reason']	= curl_error($ch);
		}
		else
		{
			$resultData['rest_no']		= "0";
			$resultData['result']		= $result;
		}
		return $resultData;
		curl_close($ch);
	}
	function garuda_post($url,$url2,$data,$cookie)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
		curl_setopt($ch, CURLOPT_REFERER, $url2);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_ENCODING , $this->compression);
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, '60');
		curl_setopt($ch, CURLOPT_TIMEOUT , '120' );
		curl_setopt($ch, CURLOPT_MAXREDIRS , '3' );
		
		//for windows
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
		curl_setopt($ch, CURLOPT_COOKIESESSION, false);
		curl_setopt($ch, CURLOPT_FAILONERROR,true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		if ($this->proxy) curl_setopt($ch, CURLOPT_PROXY, $this->proxy);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
		$result = curl_exec($ch);
		if($result === false)
		{
			$resultData['rest_no']		= curl_errno($ch);
			$resultData['reason']	= curl_error($ch);
		}
		else
		{
			$resultData['rest_no']		= "0";
			$resultData['result']		= $result;
		}
		
		return $resultData;
		curl_close($ch);
	}
	
	function post_custom ($url, $url2, $data, $cookie, $CONNECTTIMEOUT='30', $TIMEOUT='300', $pRequest='post') 
	{
		echo $url;
		exit();
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
		curl_setopt($ch, CURLOPT_REFERER, $url2);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_ENCODING , $this->compression);
		curl_setopt($ch, CURLOPT_VERBOSE, false);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $CONNECTTIMEOUT);
		curl_setopt($ch, CURLOPT_TIMEOUT , $TIMEOUT );
		curl_setopt($ch, CURLOPT_MAXREDIRS , '3' );
		#curl_setopt($ch, CURLOPT_FAILONERROR,true);
		
		//for windows
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
		#curl_setopt($ch, CURLOPT_COOKIESESSION, false);
		
		if ($pRequest == 'post'){
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		}
		
		//if ($this->proxy) curl_setopt($ch, CURLOPT_PROXY, $this->proxy);
		#if ($this->proxy1) curl_setopt($ch, CURLOPT_PROXY, $this->proxy1);
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
		$result = curl_exec($ch);
		
		if($result === false)
		{
			$resultData['rest_no']		= curl_errno($ch);
			$resultData['reason']	= curl_error($ch);
		}
		else
		{
			$resultData['rest_no']		= "0";
			$resultData['result']		= $result;
		}
		
		return $resultData;
		curl_close($ch);
	}
	
	function post_airasia ($url, $url2, $data, $cookie, $CONNECTTIMEOUT='30', $TIMEOUT='300', $pRequest='post') 
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
		curl_setopt($ch, CURLOPT_REFERER, $url2);
		curl_setopt($ch, CURLOPT_SSLVERSION, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_ENCODING , $this->compression);
		curl_setopt($ch, CURLOPT_VERBOSE, false);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $CONNECTTIMEOUT);
		curl_setopt($ch, CURLOPT_TIMEOUT , $TIMEOUT );
		curl_setopt($ch, CURLOPT_MAXREDIRS , '3' );
		#curl_setopt($ch, CURLOPT_FAILONERROR,true);
		
		//for windows
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
		#curl_setopt($ch, CURLOPT_COOKIESESSION, false);
		
		if ($pRequest == 'post'){
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		}
		
		//if ($this->proxy) curl_setopt($ch, CURLOPT_PROXY, $this->proxy);
		#if ($this->proxy1) curl_setopt($ch, CURLOPT_PROXY, $this->proxy1);
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
		$result = curl_exec($ch);
		
		if($result === false)
		{
			$resultData['rest_no']		= curl_errno($ch);
			$resultData['reason']	= curl_error($ch);
		}
		else
		{
			$resultData['rest_no']		= "0";
			$resultData['result']		= $result;
		}
		
		return $resultData;
		curl_close($ch);
	}
	
	function post_proxy ($url, $url2, $data, $cookie, $CONNECTTIMEOUT='300', $TIMEOUT='300', $pRequest='post') 
	{
		$ci =& get_instance();
		$ci->load->database('default', TRUE);
		
		#get proxy
		/*$query		= $ci->db->query("SELECT * FROM proxy_list WHERE status = '0' ORDER BY RAND() LIMIT 1");
		$res		= $query->result();
		$proxy_ip 	= $res[0]->proxy;
		$cErr		= $res[0]->err_counter;
		$used		= $res[0]->used;
		
		if($proxy_ip==""):
			$proxy_ip	= $this->proxy1;
		endif;*/
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
		curl_setopt($ch, CURLOPT_REFERER, $url2);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_ENCODING , $this->compression);
		curl_setopt($ch, CURLOPT_VERBOSE, false);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $CONNECTTIMEOUT);
		curl_setopt($ch, CURLOPT_TIMEOUT , $TIMEOUT );
		curl_setopt($ch, CURLOPT_MAXREDIRS , '3' );
		//curl_setopt($ch, CURLOPT_FAILONERROR,true);
		
		//for windows
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
		#curl_setopt($ch, CURLOPT_COOKIESESSION, false);
		
		if ($pRequest == 'post'){
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		}
		
		/*if ($this->proxy) curl_setopt($ch, CURLOPT_PROXY, $this->proxy);*/	//Awal sebelum pakai ganti proxy
		
		/*curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);					//Script awal ganti proxy
		curl_setopt($ch, CURLOPT_PROXY, "https://118.99.114.23:3128"); #http://116.251.220.18:80*/ //Script awal ganti proxy
		#if ($this->proxy1) curl_setopt($ch, CURLOPT_PROXY, $this->proxy1);
		
		#connect to proxy
		
		/*if($res[0]->username!=""):
			curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
			curl_setopt($ch, CURLOPT_PROXYUSERPWD, $res[0]->username.':'.$res[0]->password);
		endif;*/
		
		#curl_setopt($ch, CURLOPT_PROXY, $proxy_ip);
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
		$result = curl_exec($ch);
		
		/*if($result === false)
		{
			$resultData['rest_no']		= curl_errno($ch);
			$resultData['reason']	= curl_error($ch);
			
			$resultData = $this->post_proxy ($url, $url2, $data, $cookie, $CONNECTTIMEOUT, $TIMEOUT, $pRequest) ;
			
			#update status
			if(curl_errno($ch)!="28"):
				$tERR		= $cErr+1;
				if($tERR >= 50): $stat ="1"; else: $stat ="0"; endif;
				$dataProxy  = array('status'=>$stat,"err_counter"=>$tERR,'date_error'=>date('Y-m-d H:i:s'),'curl_errno'=>curl_errno($ch));
				$ci->mgeneral->update(array('proxy'=>$proxy_ip),$dataProxy,"proxy_list");
			endif;
		}
		else
		{*/
			$resultData['rest_no']		= "0";
			$resultData['result']		= $result;
		/*}
			#update counter penggunaan
			$tUsed		= $used+1;
			$dataUpdate = array('used'=>$tUsed);
			$ci->mgeneral->update(array('proxy'=>$proxy_ip),$dataUpdate,"proxy_list");
			*/
		return $resultData;
		curl_close($ch);
	
	}

	// curl post json
	function post_json($url='', $data) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_USERAGENT, 'AT2-JSN');
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, '120');
		curl_setopt($ch, CURLOPT_TIMEOUT, 120);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($data)));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		$result = curl_exec($ch);
		if ($result === false) {
			return curl_error($ch);
		} else {
			return $result;
		}
		curl_close($ch);
	}
	
	function post_hotel($ch, $url, $url2, $data, $cookie) {
		/*$ci =& get_instance();
		$ci->load->database('default', TRUE);
		#get proxy
		$query		= $ci->db->query("SELECT * FROM proxy_list WHERE status = '0' ORDER BY RAND() LIMIT 1");
		$res		= $query->result();
		$proxy_ip 	= $res[0]->proxy;*/
		
		$headers[] = 'Accept: text/html,application/xhtml+xml,application/xml';
		$headers[] = 'Connection: Keep-Alive';
		$headers[] = 'Content-type: application/x-www-form-urlencoded;charset=UTF-8';
		
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_REFERER, $url2);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_USERAGENT, $this->user_agent);
		curl_setopt($ch, CURLOPT_ENCODING , $this->compression);
		curl_setopt($ch, CURLOPT_COOKIEFILE, $cookie);
		curl_setopt($ch, CURLOPT_COOKIEJAR, $cookie);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
		curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, '60');
		curl_setopt($ch, CURLOPT_TIMEOUT , '360' );
		curl_setopt($ch, CURLOPT_MAXREDIRS , '3' );
		#curl_setopt($ch, CURLOPT_FAILONERROR, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_COOKIESESSION, true);
		
		
		if ($data != '') {
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		}  else {
			curl_setopt($curl_handle, CURLOPT_CUSTOMREQUEST, 'GET');
		}
		
		/*if($res[0]->username!=""):
			curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP);
			curl_setopt($ch, CURLOPT_PROXYUSERPWD, $res[0]->username.':'.$res[0]->password);
		endif;
		
		if($proxy_ip!=""):
			curl_setopt($ch, CURLOPT_PROXY, $proxy_ip);
		endif;*/
		
		$result = curl_exec($ch);

		if($result === false) {
			$resultData = array('rest_no' => curl_errno($ch), 'reason' => curl_error($ch) );
		}
		else {
			$resultData = array('rest_no' => '0' , 'result' => $result );
		}
		
		return $resultData;
	}
	
	function post_supplier($post_data,$url)
	{
		foreach ($post_data as $key => $value) {
            $post_items[] = $key . '=' . $value;
        }
        $data = implode('&', $post_items);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, '100');
		curl_setopt($ch, CURLOPT_TIMEOUT, 300);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		$result = curl_exec($ch);
		#print_r($result);
		if($result === false):
			$data	= array("rest_no"	=> "500",
							"reason"	=> "Server sedang mengalami ganguan, silahkan coba lagi.");
		else:
			$data	= json_decode($result,true);
		endif;
		
		return $data;
		curl_close($ch);
	}
	
}
		
?>
