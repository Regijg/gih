<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists('setting_kereta_harga'))
{
	function setting_kereta_harga($agent_id,$app,$komisi,$roundtrip)
	{
		$ci =& get_instance();
		
		#hitung nilai komisi berdsarkan skema bisnisnya
		if($agent_id==""):
			if($app == "customer"):
				$diskon		= $ci->mgeneral->getValue("amount",array('produk_id'=>"3","status"=>"aktif"),"diskon_customer");
			else:
				$diskon		= 0;
			endif;
			
			$result['diskon']		= $diskon;
			$result['komisiAgent']	= 0;
			$result['komisiClient']	= number_format($komisi-$diskon, 0, ',', '');	
		else:
			$diskon			= 0;
			
			$agent_id		= $ci->converter->decode($agent_id);
			$cekTipeKomisi	= $ci->mquery->get_skema_komisi($agent_id,"3","kereta");
			
			if($cekTipeKomisi['tipe'] == "fix"):
				$nilaiKomisi	= $cekTipeKomisi['nilai'];	
			elseif($cekTipeKomisi['tipe'] == "percen"):
				$nilaiKomisi	= ($result['commission']*$cekTipeKomisi['nilai'])/100;
			else:
				$nilaiKomisi	= $result['commission'];
			endif;
			
				if($roundtrip=="return"): $nilaiKomisi = $nilaiKomisi*2; endif;
				
			$result['diskon']		= $diskon;
			$result['komisiAgent']	= number_format(($komisi-$nilaiKomisi), 0, ',', '');
			$result['komisiClient']	= number_format(($komisi-($komisi-$nilaiKomisi)), 0, ',', '');
				
		endif;
		
		return $result;
	}
	
}

if ( ! function_exists('saveDataKai'))
{
	function saveDataKai($param,$result)
	{
		$ci =& get_instance();
		
		#save book data
		$varData	= array('client_id'				=> "1",
							'unique_id'				=> $param['session_id'],
							'book_id_supplier'		=> $result['book_id'],
							'partner_id'			=> $ci->converter->decode($param['agent_id']),
							'dtt_depart_date'		=> $result['search_info']['depart'],
							'dtt_return_date'		=> $result['search_info']['return'],
							'dtt_roundtrip'			=> $result['search_info']['roundtrip'],
							'dtt_from'				=> $result['search_info']['from'],
							'dtt_to'				=> $result['search_info']['to'],
							'dtt_adult'				=> $result['search_info']['adult'],
							'dtt_child'				=> $result['search_info']['child'],
							'dtt_infant'			=> $result['search_info']['infant'],
							'dtt_cust_name'			=> $param['customer_name'],
							'dtt_cust_email'		=> $param['customer_email'],
							'dtt_timelimit'			=> date('Y-m-d H:i:s'),
							'dtt_depart_bookcode'	=> $result['detail_info']['depart']['schedule']['book_code'],
							'dtt_return_bookcode'	=> $result['detail_info']['return']['schedule']['book_code'],
							'dtt_ticket_price'		=> $result['total_fare'],
							'dtt_partner_price'		=> $result['agent_fare'],
							'dtt_client_komisi'		=> $result['commission_client'],
							'dtt_partner_komisi'	=> $result['commission'],
							'dtt_discount'			=> $result['diskon'],
							'dtt_status'			=> "book",
							'app'					=> $param['client']['client_app']);
		$bookId		= $ci->mgeneral->save($varData,"dt_train");
		
		#pembagi untuk komisi masing2 schedule
		if($result['search_info']['roundtrip']=="return"): $pembagi = 2; else: $pembagi = 1; endif;
		
		#save schedule
		$unique_id		= $result['session_id'];
		$detailData		= $ci->mgeneral->getWhere(array('action'=>"detail","unique_id"=>$unique_id),"log_kai");
		$detailData2	= json_decode($detailData[0]->result_data_to_agent,true);
		
		$detailDepart	= $result['detail_info']['depart']['schedule'];
		$detailDepartDB	= $detailData2['detail_info']['depart'];
		$varSchDepart	= array('dtt_id'								=> $bookId,
								'dtt_schedule_type'						=> "departure",
								'dtt_schedule_departure_station_code'	=> $detailDepart['from'],
								'dtt_schedule_departure_station_name'	=> $detailDepartDB['from_st_name'],
								'dtt_schedule_arrival_station_code'		=> $detailDepart['to'],
								'dtt_schedule_arrival_station_name'		=> $detailDepartDB['to_st_name'],
								'dtt_schedule_etd'						=> $detailDepart['ETD'],
								'dtt_schedule_eta'						=> $detailDepart['ETA'],
								'dtt_schedule_departure_date'			=> $detailDepart['DD'],
								'dtt_schedule_arrival_date'				=> $detailDepart['AD'],
								'dtt_schedule_train_name'				=> $detailDepart['train_name'],
								'dtt_schedule_train_no'					=> $detailDepart['train_no'],
								'dtt_schedule_class'					=> $detailDepart['class'],
								'dtt_schedule_adult_price'				=> $detailDepartDB['price']['adult']['price'],
								'dtt_schedule_child_price'				=> $detailDepartDB['price']['child']['price'],
								'dtt_schedule_infant_price'				=> $detailDepartDB['price']['infant']['price'],
								'dtt_schedule_extra_fee'				=> $detailDepartDB['price']['extrafee'],
								'dtt_schedule_client_komisi'			=> ($result['commission_client']/$pembagi),
								'dtt_schedule_partner_komisi'			=> ($result['commission']/$pembagi),
								'dtt_schedule_discount'					=> 0,
								'dtt_schedule_status'					=> "book");
		$ci->mgeneral->save($varSchDepart,"dt_train_schedule");
		
		if($result['search_info']['roundtrip']=="return"):
			
			$detailReturn	= $result['detail_info']['return']['schedule'];
			$detailReturnDB	= $detailData2['detail_info']['return'];
			$varSchReturn	= array('dtt_id'								=> $bookId,
									'dtt_schedule_type'						=> "return",
									'dtt_schedule_departure_station_code'	=> $detailReturn['from'],
									'dtt_schedule_departure_station_name'	=> $detailReturnDB['from_st_name'],
									'dtt_schedule_arrival_station_code'		=> $detailReturn['to'],
									'dtt_schedule_arrival_station_name'		=> $detailReturnDB['to_st_name'],
									'dtt_schedule_etd'						=> $detailReturn['ETD'],
									'dtt_schedule_eta'						=> $detailReturn['ETA'],
									'dtt_schedule_departure_date'			=> $detailReturn['DD'],
									'dtt_schedule_arrival_date'				=> $detailReturn['AD'],
									'dtt_schedule_train_name'				=> $detailReturn['train_name'],
									'dtt_schedule_train_no'					=> $detailReturn['train_no'],
									'dtt_schedule_class'					=> $detailReturn['class'],
									'dtt_schedule_adult_price'				=> $detailReturnDB['price']['adult']['price'],
									'dtt_schedule_child_price'				=> $detailReturnDB['price']['child']['price'],
									'dtt_schedule_infant_price'				=> $detailReturnDB['price']['infant']['price'],
									'dtt_schedule_extra_fee'				=> $detailReturnDB['price']['extrafee'],
									'dtt_schedule_client_komisi'			=> ($result['commission_client']/$pembagi),
									'dtt_schedule_partner_komisi'			=> ($result['commission']/$pembagi),
									'dtt_schedule_discount'					=> 0,
									'dtt_schedule_status'					=> "book");
		$ci->mgeneral->save($varSchReturn,"dt_train_schedule");
			
		endif;
		
		#save pax
		$paxData = $result['pax'];
		foreach($paxData as $p):
		
			$varPax	= array('dtt_id'				=> $bookId,
							'dtt_pax_type'			=> $p['pax_type'],
							'dtt_pax_name'			=> $p['pax_name'],
							'dtt_pax_id_no'			=> $p['pax_id_no'],
							'dtt_pax_phone'			=> $p['pax_phone'],
							'dtt_pax_birthdate'		=> $p['pax_birthdate'],
							'dtt_pax_wagon_depart'	=> $p['depart_wagon'],
							'dtt_pax_seat_depart'	=> $p['depart_seat_no'],
							'dtt_pax_wagon_return'	=> $p['return_wagon'],
							'dtt_pax_seat_return'	=> $p['return_seat_no']);
			$ci->mgeneral->save($varPax,"dt_train_pax");
			
		endforeach;
		
		return $bookId;
	}
}
?>