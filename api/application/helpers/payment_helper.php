<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );

if (! function_exists ( 'cek_deposit_dan_bayar' )) {
	function cek_deposit_dan_bayar($partner_id, $harga, $book_id, $msg) {
		$ci = & get_instance ();
		
		// cek current deposit
		$partner_data = $ci->mgeneral->getWhere ( array (
				'partner_id' => $partner_id 
		), "mstr_partner" );
		
		if (! empty ( $partner_data )) :
			if($partner_data[0]->corporate_id && $partner_data[0]->corporate_id != '0'){
				#$corporate_data 	= $ci->mgeneral->getWhere(array("corporate_id" => $partner_data[0]->corporate_id), "mstr_corporate");
				$partner_data = $ci->mgeneral->getWhere ( array (
						'corporate_id' 	=> $partner_data[0]->corporate_id,
						'partner_level'	=> '1'
				), "mstr_partner" );
				$partner_id = $partner_data[0]->partner_id;
			}
		endif;

		if (! empty ( $partner_data )) :
			
			$current_deposit = $partner_data [0]->current_deposit;
			if ($current_deposit >= $harga) :
				
				// kurangi deposit partner
				$sql = "UPDATE mstr_partner SET current_deposit = current_deposit-" . $harga . " WHERE partner_id = '" . $partner_id . "' ";
				$ci->db->query ( $sql );
				
				// save to deposit history
				$last_deposit	= $ci->mgeneral->getValue("current_deposit",array('partner_id'=>$partner_id),"mstr_partner");
				
				$payment_history = array (
						'partner_id' => $partner_id,
						'payment_date' => date ( 'Y-m-d H:i:s' ),
						'payment_amount' => $harga,
						'payment_last_deposit' => $last_deposit,
						'payment_type' => "kredit",
						'payment_referensi' => $book_id,
						'payment_status' => "ok",
						'payment_message' => $msg 
				);
				$depositId = $ci->mgeneral->save($payment_history, "dt_deposit_history" );
				
				$result ['error_no'] = "0";
				$result ['pay_ref'] = $depositId;
			 

			else :
				
				$result ['error_no'] = "704";
				$result ['error_msg'] = getErrorText ( "704" );
			

			endif;
		 

		else :
			
			$result ['error_no'] = "705";
			$result ['error_msg'] = getErrorText ( "705" );
		

		endif;
		
		return $result;
	}
}

if (! function_exists ( 'cek_add_deposit' )) {
	function cek_add_deposit($transfer_to, $transfer_name, $transfer_date, $amount,$partner_id) {
		$ci = & get_instance ();
		
		$sql = 'select * from dt_mutasi_bank where bank_akun_id="' . $transfer_to . '" and bank_mutation_datebank="' . str_replace("-","",$transfer_date) . '" and bank_mutation_amount="' . $amount . '" and bank_mutation_status="0" and bank_mutation_text like "%' . $transfer_name . '%"';
		$MutasiCek = $ci->mgeneral->post_query_sql($sql);
		
		if (!empty( $MutasiCek )) :
		
			#update status mutasi rekening
			$ci->mgeneral->update(array('bank_mutation_id'=>$MutasiCek[0]->bank_mutation_id), array('bank_mutation_status'=>'1','payment_for' => "deposit"),'dt_mutasi_bank');
			
			#tambah deposit partner
			$ci->mquery->add_deposit($amount,$partner_id);
			
			#add to deposit history
			$lastDeposit= $ci->mgeneral->getValue("current_deposit",array('partner_id'=>$partner_id),"mstr_partner");
			$varDeposit = array ('partner_id' 			=> $partner_id,
								 'payment_date' 		=> date('Y-m-d H:i:s'),
								 'payment_amount' 		=> $amount,
								 'payment_last_deposit' => $lastDeposit,
								 'payment_type'			=> 'debet',
								 'payment_referensi' 	=> $MutasiCek[0]->bank_mutation_id,
								 'payment_status' 		=> 'ok',
								 'payment_message' 		=> 'Penambahan Deposit sebesar ' . $amount . '');
			$ci->mgeneral->save($varDeposit,'dt_deposit_history');
			
			$status = "success";
		
		else:
			$status = "pending";
		endif;
		
		return $status;
	}
}

if (! function_exists ( 'balik_deposit' )) {	
	function balik_deposit($partner_id, $harga, $book_id, $msg) {
		$ci = & get_instance ();
		
		// cek current deposit
		$partner_data = $ci->mgeneral->getWhere(array('partner_id'=>$partner_id), "mstr_partner" );
		
		if (! empty ( $partner_data )) :
			
			$current_deposit = $partner_data[0]->current_deposit;	
			// kurangi deposit partner
			$sql = "UPDATE mstr_partner SET current_deposit = current_deposit+" . $harga . " WHERE partner_id = '" . $partner_id . "' ";
			$ci->db->query ( $sql );
			
			// save to deposit history
			$last_deposit = $current_deposit + $harga;
			$payment_history = array (
					'partner_id' => $partner_id,
					'payment_date' => date ( 'Y-m-d H:i:s' ),
					'payment_amount' => $harga,
					'payment_last_deposit' => $last_deposit,
					'payment_type' => "debet",
					'payment_referensi' => $book_id,
					'payment_status' => "ok",
					'payment_message' => $msg 
			);
			$depositId = $ci->mgeneral->save ( $payment_history, "dt_deposit_history" );
			
			$result ['error_no'] = "0";
			$result ['pay_ref'] = $depositId;

		else :
			
			$result ['error_no'] = "705";
			$result ['error_msg'] = getErrorText ("705");

		endif;
		
		return $result;
	}
}

if (! function_exists ( 'branch_cashback' )) {	
	function branch_cashback($partner_id, $total_cashback, $book_id, $msg) {
		$ci = & get_instance ();
		
		// cek current deposit
		$partner_data = $ci->mgeneral->getWhere(array('partner_id'=>$partner_id), "mstr_partner" );
		
		if (! empty ( $partner_data )) :
			
			$current_deposit = $partner_data[0]->current_deposit;	
			// kurangi deposit partner
			$sql = "UPDATE mstr_partner SET current_deposit = current_deposit+" . $total_cashback . " WHERE partner_id = '" . $partner_id . "' ";
			$ci->db->query ( $sql );
			
			// save to deposit history
			$last_deposit = $current_deposit + $total_cashback;
			$payment_history = array (
					'partner_id' => $partner_id,
					'payment_date' => date ( 'Y-m-d H:i:s' ),
					'payment_amount' => $total_cashback,
					'payment_last_deposit' => $last_deposit,
					'payment_type' => "debet",
					'payment_referensi' => $book_id,
					'payment_status' => "ok",
					'payment_message' => $msg 
			);
			$depositId = $ci->mgeneral->save ( $payment_history, "dt_deposit_history" );
			
			$result ['error_no'] = "0";
			$result ['pay_ref'] = $depositId;

		else :
			
			$result ['error_no'] = "705";
			$result ['error_msg'] = getErrorText ("705");

		endif;
		
		return $result;
	}
}

?>