<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists('station_detail'))
{
    function station_detail($code)
    {
        $ci = & get_instance();
        $detail    = $ci->mgeneral->getWhere(array('station_code'=>$code),"mstr_station");

        $station = array("code" => $code, "city" => ucfirst($detail[0]->city), "station_name" => ucfirst($detail[0]->station_name));

        return $station;
    }
}

if ( ! function_exists('extract_session_train'))
{
    function extract_session_train($parameter)
    {
        $ci = & get_instance();
        $session_id    = $ci->converter->decode($parameter['scReferenceId']);
        $sesData    = $ci->mgeneral->getWhere(array('session_id' => $session_id), "log_session");

        foreach ($sesData as $sd)
        {
            $search_info     = json_decode($sd->search_post,true);
            $search_result    = json_decode($sd->search_data,true);
        }

        $departD = $search_result['schedule']['depart'];

        foreach ($departD as $departD)
        {
            if (strtolower($parameter['schedule_id']) == strtolower($departD['schedule_id']))
            {
                $classD        = $departD['cabin_class'];
                foreach ($classD as $classD)
                {
                    
                    if (strtoupper($parameter['sub_class']) == strtoupper($classD['sub_class']))
                    {
                        $dep_schedule   = $departD;
                        $dep_schedule['class_name'] = $classD['class_name'];
                        $dep_schedule['sub_class']  = $parameter['sub_class'];
                        unset($dep_schedule['schedule_id']);
                        unset($dep_schedule['cabin_class']);
                        
                        $dep_adult_price  = $search_info['adult'] * $classD['adult_price'];
                        $dep_price['adult'] = array("sitter"        => $search_info['adult'],
                                                    "price_per_pax" => $classD['adult_price'],
                                                    "total_price"   => $dep_adult_price);
                        if($search_info['infant'] > 0){
                            $dep_infant_price       = $search_info['infant'] * $classD['infant_price'];
                            $dep_price['infant']    = array("sitter"        => $search_info['infant'],
                                                            "price_per_pax" => $classD['infant_price'],
                                                            "total_price"   => $dep_infant_price);
                        }

                        $arr_value = array("depart" => $search_result['schedule']['depart_ref']);
                    }

                }
            }
        }
        
        if($dep_schedule){
            if($search_info['roundtrip'] == "return"){
                $returnR = $search_result['schedule']['return'];

                foreach ($returnR as $returnR)
                {

                    if (strtolower($parameter['schedule_id2']) == strtolower($returnR['schedule_id']))
                    {
                        $classR        = $returnR['cabin_class'];
                        foreach ($classR as $classR)
                        {
                            if (strtoupper($parameter['sub_class2']) == strtoupper($classR['sub_class']))
                            {
                                $ret_schedule = $returnR;
                                $ret_schedule['class_name'] = $classR['class_name'];
                                $ret_schedule['sub_class']  = $parameter['sub_class2'];
                                unset($ret_schedule['schedule_id']);
                                unset($ret_schedule['cabin_class']);
                                
                                $ret_adult_price  = $search_info['adult'] * $classR['adult_price'];
                                $ret_price['adult'] = array("sitter"        => $search_info['adult'],
                                                            "price_per_pax" => $classR['adult_price'],
                                                            "total_price"   => $ret_adult_price);
                                if($search_info['infant'] > 0){
                                    $ret_infant_price     = $search_info['infant'] * $classR['infant_price'];
                                    $ret_price['infant']    = array("sitter"        => $search_info['infant'],
                                                                    "price_per_pax" => $classR['infant_price'],
                                                                    "total_price"   => $ret_infant_price);
                                }

                                $arr_value['return'] = $search_result['schedule']['return_ref'];

                            }

                        }
                    }
                }

                if($ret_schedule){
                    $dep_commission = 5000;
                    $dep_extra_fee  = 7500;
                    $dep_normal_fare= $dep_adult_price + $dep_infant_price;
                    $dep_total_fare = $dep_normal_fare + $dep_extra_fee;

                    $ret_commission = 5000;
                    $ret_extra_fee  = 7500;
                    $ret_normal_fare= $ret_adult_price + $ret_infant_price;
                    $ret_total_fare = $ret_normal_fare + $ret_extra_fee;

                    $commission     = $dep_commission + $ret_commission;
                    $extra_fee      = $dep_extra_fee + $ret_extra_fee;
                    $normal_fare    = $dep_normal_fare + $ret_normal_fare;
                    $total_fare     = $dep_total_fare + $ret_total_fare;

                    $result     = array("rest_no"       => "0",
                                        "depart"        => array("schedule"         => $dep_schedule, 
                                                                 "fares_details"    => $dep_price, 
                                                                 "normal_fares"     => $dep_normal_fare,
                                                                 "extra_fee"        => $dep_extra_fee,
                                                                 "discount"         => 0,
                                                                 "total_fares"      => $dep_total_fare,
                                                                 "commission"       => $dep_commission),
                                        "return"        => array("schedule"         => $ret_schedule, 
                                                                 "fares_details"    => $ret_price, 
                                                                 "normal_fares"     => $ret_normal_fare,
                                                                 "extra_fee"        => $ret_extra_fee,
                                                                 "discount"         => 0,
                                                                 "total_fares"      => $ret_total_fare,
                                                                 "commission"       => $ret_commission),
                                        "normal_fares"  => $normal_fare,
                                        "extra_fee"     => $extra_fee,
                                        "discount"      => 0,
                                        "total_fares"   => $total_fare,
                                        "commission"    => $commission,
                                        "NTA"           => ($total_fare - $commission));
                }
                else{
                    $error = getErrorText("503");
                    $result['rest_no'] = "503";
                    $result['reason'] = $error;
                }
            }
            else{
                $dep_commission = 5000;
                $dep_extra_fee  = 7500;
                $dep_normal_fare= $dep_adult_price + $dep_infant_price;
                $dep_total_fare = $dep_normal_fare + $dep_extra_fee;

                $result     = array("rest_no"       => "0",
                                    "depart"        => array("schedule"         => $dep_schedule, 
                                                             "fares_details"    => $dep_price, 
                                                             "normal_fares"     => $dep_normal_fare,
                                                             "extra_fee"        => $dep_extra_fee,
                                                             "discount"         => 0,
                                                             "total_fares"      => $dep_total_fare,
                                                             "commission"       => $dep_commission),
                                    "normal_fares"  => $dep_normal_fare,
                                    "extra_fee"     => $dep_extra_fee,
                                    "discount"      => 0,
                                    "total_fares"   => $dep_total_fare,
                                    "commission"    => $dep_commission,
                                    "NTA"           => ($dep_total_fare - $dep_commission));
            }
        }
        else{
            $error = getErrorText("503");
            $result['rest_no'] = "503";
            $result['reason'] = $error;
        }
            
        $res    = array('search_post'   => $search_info,
                        'result'        => $result,
                        'value'         => $arr_value);

        return $res;
    }
}

if ( ! function_exists('update_detail_result'))
{
    function update_detail_result($roundtrip, $result, $price)
    {
        $ci = & get_instance();
        
        $result['depart'] = $price['depart'];
        if($roundtrip == "return"){
            $result['return'] = $price['return'];
        }

        $result["normal_fares"] = $price['depart']['normal_fares'] + $price['return']['normal_fares'];
        $result["extra_fee"]    = $price['depart']['extra_fee'] + $price['return']['extra_fee'];
        $result["discount"]     = $price['depart']['discount'] + $price['return']['discount'];
        $result["total_fares"]  = $price['depart']['total_fares'] + $price['return']['total_fares'];
        $result["commission"]   = $price['depart']['commission'] + $price['return']['commission'];
        $result["NTA"]          = $result["total_fares"] - $result["commission"];
        return $result;
    }
}

if ( ! function_exists('extract_session_train_book'))
{
    function extract_session_train_book($parameter)
    {
        $ci = & get_instance();
        $session_id = $ci->converter->decode($parameter['fareReferenceId']);

        #extract session from DB
        $sesData = $ci->mgeneral->getWhere(array('session_id' => $session_id), "log_session");
        foreach ($sesData as $sd)
        {
            $search_post    = json_decode($sd->search_post, true);
            $search_result    = json_decode($sd->search_data);
            $detail_post    = json_decode($sd->post_detail);
            $detail_result    = json_decode($sd->data_detail, true);
        }

        $result = array("search_post"   => $search_post,
                        "result"        => $detail_result,
                        "tpax"          => $search_post["adult"] + $search_post["infant"],
                        "roundtrip"     => $search_post["roundtrip"]);

        $param_depart['data']           = $detail_result['depart'];
        $param_depart['value']          = $ci->converter->decode($detail_result['value']['value_depart']);
        $param_depart['search_post']    = $search_post;
        $result['param_depart']         = $param_depart; 

        if($search_post["roundtrip"] == "return"){
            $param_return['data']           = $detail_result['return'];
            $param_return['value']          = $ci->converter->decode($detail_result['value']['value_return']);
            $param_return['search_post']    = $search_post;           
            $result['param_return']         = $param_return; 
        }

        return $result;
    }

}

if ( ! function_exists('build_train_book_result'))
{
    function build_train_book_result($roundtrip, $parameter, $dep_book, $ret_book = NULL)
    {
        $ci = & get_instance();
        
        $cust_details = array(
            "customer_name"     => $parameter['customer_name'],
            "customer_phone"    => $parameter['customer_phone'],
            "customer_email"    => $parameter['customer_email'],
        );

        if($roundtrip == "oneway"){
            if($dep_book['rest_no'] == '0'){
                $normal_fares   = $dep_book["normal_fares"];
                $extra_fee      = $dep_book["extra_fee"];
                $discount       = $dep_book['discount'];
                $total_fares    = $dep_book["total_fares"];
                $commission     = $dep_book["commission"];
                $NTA            = $dep_book["NTA"];
                $pax_details    = $dep_book["pax_details"];

                unset($dep_book["rest_no"]);
                unset($dep_book["pax_details"]);
                unset($dep_book["NTA"]);
                $result = array("rest_no"       => "0",
                                "depart"        => $dep_book,
                                "cust_details"  => $cust_details,
                                "pax_details"   => $pax_details,
                                "normal_fares"  => $normal_fares,
                                "extra_fee"     => $extra_fee,
                                "discount"      => $discount,
                                "total_fares"   => $total_fares,
                                "commission"    => $commission,
                                "NTA"           => $NTA);
            }
            else{
                $result = $dep_book;
            }
        }
        else{
            if($dep_book["rest_no"] == "0" && $ret_book["rest_no"] == "0"){
                $normal_fares       = $dep_book["normal_fares"] + $ret_book["normal_fares"];
                $extra_fee          = $dep_book["extra_fee"] + $ret_book["extra_fee"];
                $discount           = $dep_book['discount'] + $ret_book['discount'];
                $total_fares        = $dep_book["total_fares"] + $ret_book["total_fares"];
                $commission         = $dep_book["commission"] + $ret_book["commission"];
                $NTA                = $dep_book["NTA"] + $ret_book["NTA"];
                $dep_pax_details    = $dep_book["pax_details"];
                $ret_pax_details    = $ret_book["pax_details"];

                unset($dep_book["rest_no"]);
                unset($dep_book["pax_details"]);
                unset($dep_book["NTA"]);
                unset($ret_book["rest_no"]);
                unset($ret_book["pax_details"]);
                unset($ret_book["NTA"]);

                foreach ($ret_pax_details as $key => $value) {
                    $dep_pax_details[$key]['return_seat'] = $value["depart_seat"];
                }

                $result = array("rest_no"       => "0",
                                "depart"        => $dep_book,
                                "return"        => $ret_book,
                                "cust_details"  => $cust_details,
                                "pax_details"   => $dep_pax_details,
                                "normal_fares"  => $normal_fares,
                                "extra_fee"     => $extra_fee,
                                "discount"      => $discount,
                                "total_fares"   => $total_fares,
                                "commission"    => $commission,
                                "NTA"           => $NTA);
            }
            else{
                
                $result = array('rest_no'   => "717",
                                'reason'    => "gagal booking");
            }
        }

        return $result;
    }
}

if ( ! function_exists('build_train_pay_result'))
{
    function build_train_pay_result($book_id)
    {
        $ci = & get_instance();
        
        $train_data = $ci->mgeneral->getWhere(array("dtt_id" => $book_id), "dt_train");
        
        if($train_data[0]->dtt_status == "failed"){
            $error = getErrorText("906");
            $result['rest_no'] = "906";
            $result['reason'] = $error;
        }
        else{
            $train_pax  = $ci->mgeneral->getWhere(array("dtt_id" => $book_id), "dt_train_pax");
            $train_dep  = $ci->mgeneral->getWhere(array("dtt_id" => $book_id, "sch_type" => "depart"), "dt_train_schedule");

            $dep_station = station_detail($train_dep[0]->sch_from);
            $arr_station = station_detail($train_dep[0]->sch_to);
            $dep_fare_detail["adult_fare"]  = array(
                "sitter"        => (int)$train_data[0]->dtt_adult,
                "price_per_pax" => (int)$train_dep[0]->sch_adult_price,
                "total_price"   => (int)($train_data[0]->dtt_adult * $train_dep[0]->sch_adult_price)
            );

            if((int)($train_data[0]->dtt_infant) > 0){
                $dep_fare_detail["infant_fare"]  = array(
                    "sitter"        => (int)$train_data[0]->dtt_infant,
                    "price_per_pax" => (int)$train_dep[0]->sch_infant_price,
                    "total_price"   => (int)($train_data[0]->dtt_infant * $train_dep[0]->sch_infant_price)
                );                
            }

            $depart_data     = array(
                "schedule"      => array(
                    "train_number"      => $train_dep[0]->sch_train_no,
                    "train_name"        => $train_dep[0]->sch_train_name,
                    "dep_station_code"  => $dep_station['code'],
                    "dep_station_name"  => $dep_station['station_name'],
                    "dep_station_city"  => $dep_station['city'],
                    "arr_station_code"  => $arr_station['code'],
                    "arr_station_name"  => $arr_station['station_name'],
                    "arr_station_city"  => $arr_station['city'],
                    "departure_date"    => $train_dep[0]->sch_departure_date,
                    "departure_time"    => $train_dep[0]->sch_etd,
                    "arrival_date"      => $train_dep[0]->sch_arrival_date,
                    "arrival_time"      => $train_dep[0]->sch_eta,
                    "class_name"        => $train_dep[0]->sch_class_name,
                    "sub_class"         => $train_dep[0]->sch_sub_class,
                ),
                "book_details"  => array(
                    "book_code"         => $train_data[0]->depart_book_code,
                    "status"            => $train_data[0]->depart_book_status
                ),
                "fare_details"  => $dep_fare_detail,
                "normal_fares"  => $train_dep[0]->sch_normal_price,
                "extra_fee"     => $train_dep[0]->sch_extra_fee,
                "discount"      => $train_dep[0]->sch_discount,
                "total_fares"   => $train_dep[0]->sch_ticket_price
            );

            $result = array("rest_no"       => "0",
                            "book_id"       => $ci->converter->encode($book_id),
                            'book_date'     => $train_data[0]->dtt_bookdate,
                            'issued_date'   => $train_data[0]->dtt_issued_date,
                            'time_limit'    => $train_data[0]->depart_expired_date,
                            "status"        => $train_data[0]->dtt_status,
                            "depart"        => $depart_data);

            if($train_data[0]->dtt_roundtrip == "return"){
                if(strtotime($train_data[0]->depart_expired_date) > strtotime($train_data[0]->return_expired_date)){
                    $result['time_limit'] = $train_data[0]->return_expired_date;
                }
                $train_ret  = $ci->mgeneral->getWhere(array("dtt_id" => $book_id, "sch_type" => "return"), "dt_train_schedule");

                $dep_station = station_detail($train_dep[0]->sch_from);
                $arr_station = station_detail($train_dep[0]->sch_to);
                $dep_fare_detail["adult_fare"]  = array(
                    "sitter"        => (int)$train_data[0]->dtt_adult,
                    "price_per_pax" => (int)$train_dep[0]->sch_adult_price,
                    "total_price"   => (int)($train_data[0]->dtt_adult * $train_dep[0]->sch_adult_price)
                );

                if((int)($train_data[0]->dtt_infant) > 0){
                    $dep_fare_detail["infant_fare"]  = array(
                        "sitter"        => (int)$train_data[0]->dtt_infant,
                        "price_per_pax" => (int)$train_dep[0]->sch_infant_price,
                        "total_price"   => (int)($train_data[0]->dtt_infant * $train_dep[0]->sch_infant_price)
                    );                
                }

                $return_data     = array(
                    "schedule"      => array(
                        "train_number"      => $train_dep[0]->sch_train_no,
                        "train_name"        => $train_dep[0]->sch_train_name,
                        "dep_station_code"  => $dep_station['code'],
                        "dep_station_name"  => $dep_station['station_name'],
                        "dep_station_city"  => $dep_station['city'],
                        "arr_station_code"  => $arr_station['code'],
                        "arr_station_name"  => $arr_station['station_name'],
                        "arr_station_city"  => $arr_station['city'],
                        "departure_date"    => $train_dep[0]->sch_departure_date,
                        "departure_time"    => $train_dep[0]->sch_etd,
                        "arrival_date"      => $train_dep[0]->sch_arrival_date,
                        "arrival_time"      => $train_dep[0]->sch_eta,
                        "class_name"        => $train_dep[0]->sch_class_name,
                        "sub_class"         => $train_dep[0]->sch_sub_class,
                    ),
                    "book_details"  => array(
                        "book_code"         => $train_data[0]->return_book_code,
                        "status"            => $train_data[0]->return_book_status
                    ),
                    "fare_details"  => $dep_fare_detail,
                    "normal_fares"  => $train_dep[0]->sch_normal_price,
                    "extra_fee"     => $train_dep[0]->sch_extra_fee,
                    "discount"      => $train_dep[0]->sch_discount,
                    "total_fares"   => $train_dep[0]->sch_ticket_price
                );   

                $result["return"] = $return_data;
            }

            $result["cust_details"] = array(
                "customer_name"     => $train_data[0]->customer_name,
                "customer_phone"    => $train_data[0]->customer_phone,
                "customer_email"    => $train_data[0]->customer_email,
            );

            $pax_details = array();
            foreach($train_pax as $k => $pax){
                $pax_details[$k]  = array(
                    "pax_type"      => $train_pax[$k]->pax_type,
                    "pax_name"      => $train_pax[$k]->pax_name,
                    "pax_id_no"     => $train_pax[$k]->pax_id_card,
                    "pax_birthdate" => $train_pax[$k]->pax_dob,
                    "pax_phone"     => $train_pax[$k]->pax_phone,
                    "depart_seat"   => array(
                        "wagon_code"    => $train_pax[$k]->pax_depart_wagon_code,
                        "wagon_no"      => $train_pax[$k]->pax_depart_wagon_no,
                        "seat"          => $train_pax[$k]->pax_depart_seat
                    )
                );
                if($train_data[0]->dtt_roundtrip == "return"){
                    $pax_details[$k]["return_seat"] = array(
                        "wagon_code"    => $train_pax[$k]->pax_depart_wagon_code,
                        "wagon_no"      => $train_pax[$k]->pax_depart_wagon_no,
                        "seat"          => $train_pax[$k]->pax_depart_seat
                    );
                }
            }
            $result["pax_details"]  = $pax_details;
            $result["normal_fares"] = (int)$train_data[0]->dtt_normal_price;
            $result["extra_fee"]    = (int)$train_data[0]->dtt_extra_fee;
            $result["discount"]     = (int)$train_data[0]->dtt_discount;
            $result["total_fares"]  = (int)$train_data[0]->dtt_ticket_price;
            $result["commission"]   = (int)$train_data[0]->dtt_commission;
            $result["commission_corp"] = (int)$train_data[0]->dtt_commission_corp;
            $result["NTA"]          = (int)($train_data[0]->dtt_ticket_price - $train_data[0]->dtt_commission);
        }

        return $result;
    }
}

if ( ! function_exists('save_train_book'))
{
    function save_train_book($parameter, $session_data, $data)
    {
        $ci = & get_instance();

        $search_post    = $session_data['search_post'];
        $roundtrip      = $search_post['roundtrip'];

        $varBook = array('dtt_roundtrip'            => $roundtrip,
                        'dtt_status'                => "book",
                        'dtt_from'                  => $search_post['dep'],
                        'dtt_to'                    => $search_post['arr'],
                        'dtt_adult'                 => $search_post['adult'],
                        #'dtt_child'                 => $search_post['child'],
                        'dtt_infant'                => $search_post['infant'],
                        'customer_name'             => $parameter['customer_name'],
                        'customer_phone'            => $parameter['customer_phone'],
                        'customer_email'            => $parameter['customer_email'],
                        'depart_train_no'           => $data['depart']['schedule']['train_number'],
                        'depart_train_name'         => $data['depart']['schedule']['train_name'],
                        'depart_book_code'          => $data['depart']['book_details']['book_code'],
                        'depart_book_status'        => "hold",
                        'depart_expired_date'       => $data['depart']['book_details']['time_limit'],
                        'depart_invoice'            => $data['depart']['book_details']['invoice'],
                        'return_train_no'           => $data['return']['schedule']['train_number'],
                        'return_train_name'         => $data['return']['schedule']['train_name'],
                        'return_book_code'          => $data['return']['book_details']['book_code'],
                        'return_book_status'        => "hold",
                        'return_expired_date'       => $data['return']['book_details']['time_limit'],
                        'return_invoice'            => $data['return']['book_details']['invoice'],
                        'dtt_normal_price'          => $data['normal_fares'],
                        'dtt_extra_fee'             => $data['extra_fee'],
                        'dtt_discount'              => $data['discount'],
                        'dtt_ticket_price'          => $data['total_fares'],
                        'dtt_commission'            => $data['commission'],
                        'app'                       => $parameter['client']['client_app']);
        #$book_id = "2";
        $book_id = $ci->mgeneral->save($varBook, "dt_train");

            save_schedule_train("depart",$book_id,$data['depart']); #save schedule depart
        if($roundtrip=="return"):
            save_schedule_train("return",$book_id,$data['return']);
        endif; #save schedule return

        $pax = $data['pax_details'];
        for ($ps = 0; $ps < count($pax); $ps++) {
            $varPsgr    = array('dtt_id'                => $book_id,
                                'pax_type'              => $pax[$ps]['pax_type'],
                                'pax_name'              => $pax[$ps]['pax_name'],
                                'pax_dob'               => $pax[$ps]['pax_birthdate'],
                                'pax_id_card'           => $pax[$ps]['pax_id_no'],
                                'pax_phone'             => $pax[$ps]['pax_phone'],
                                'pax_depart_wagon_code' => $pax[$ps]['depart_seat']['wagon_code'],
                                'pax_depart_wagon_no'   => $pax[$ps]['depart_seat']['wagon_no'],
                                'pax_depart_seat'       => $pax[$ps]['depart_seat']['seat'],
                                'pax_return_wagon_code' => $pax[$ps]['return_seat']['wagon_code'],
                                'pax_return_wagon_no'   => $pax[$ps]['return_seat']['wagon_no'],
                                'pax_return_seat'       => $pax[$ps]['return_seat']['seat']);
            $ci->mgeneral->save($varPsgr, "dt_train_pax");
        }

        return $book_id;
    }
}

if ( ! function_exists('save_schedule_train'))
{
    function save_schedule_train($sch_type,$book_id,$schedule)
    {
        $ci = & get_instance();

        $varSchedule    = array('dtt_id'                 => $book_id,
                                'sch_type'               => $sch_type,
                                'sch_train_no'           => $schedule['schedule']["train_number"],
                                'sch_train_name'         => $schedule['schedule']["train_name"],
                                'sch_from'               => $schedule['schedule']["dep_station_code"],
                                'sch_to'                 => $schedule['schedule']["arr_station_code"],
                                'sch_departure_date'     => $schedule['schedule']["departure_date"],
                                'sch_etd'                => $schedule['schedule']["departure_time"],
                                'sch_arrival_date'       => $schedule['schedule']["arrival_date"],
                                'sch_eta'                => $schedule['schedule']["arrival_time"],
                                'sch_class_name'         => $schedule['schedule']["class_name"],
                                'sch_sub_class'          => $schedule['schedule']["sub_class"],
                                'sch_adult_price'        => $schedule['fare_details']['adult']['price_per_pax'],
                                'sch_child_price'        => $schedule['fare_details']['child']['price_per_pax'],
                                'sch_infant_price'       => $schedule['fare_details']['infant']['price_per_pax'],
                                'sch_normal_price'       => $schedule['normal_fares'],
                                'sch_extra_fee'          => $schedule['extra_fee'],
                                'sch_discount'           => $schedule['discount'],
                                'sch_ticket_price'       => $schedule['total_fares'],
                                'sch_ticket_commission'  => $schedule['commission']);
            $ci->mgeneral->save($varSchedule, "dt_train_schedule");
    }

}
?>