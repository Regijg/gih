<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('getErrorText'))
{
	function getErrorText($kode)
	{	
		$error['101']	= "Akses tidak dikenali, invalid request";
		$error['201']	= "Mohon isi semua field yang dibutuhkan";
		$error['202']	= "Format inputan salah atau tidak ditemukan";
		$error['102']	= "Client tidak dikenali";
		$error['801']	= "Sistem maskapai sedang mengalami ganguan";
		$error['301']	= "Maintenance sistem maskapai";
		$error['302']	= "Pencarian tidak ditemukan, atau jadwal tidak tersedia";
		$error['303']	= "Koneksi maskapai mengalami ganguan";
		$error['304']	= "Tidak ada user yang tersedia";
		$error['802']	= "Proses pencarian mengalami ganguan";
		$error['203']	= "Format salah, data tidak ditemukan";
		$error['103']	= "Class tidak ditemukan atau salah";
		$error['104']	= "Function tidak tersedia";
		$error['105']	= "Invalid access";
		$error['401']	= "Rute tidak ditemukan";
		$error['803']	= "Proses pemilihan jadwal mengalami ganguan";
		$error['804']	= "Proses booking mengalami ganguan";
		$error['501']	= "Class yang dipilih tidak tersedia atau habis";
		$error['901']	= "Invalid session ID";
		$error['902']	= "Kesalahan akses sistem";
		$error['601']	= "Jumlah penumpang tidak sesuai";
		$error['602']	= "Data penumpang tidak valid";
		$error['701']	= "Data booking tidak ditemukan";
		$error['502']	= "Parameter tidak valid";
		$error['402']	= "Pencarian Infant tidak tersedia";
		$error['603']	= "Double book";
		$error['702']	= "Status bookingan anda sudah expire";
		$error['703']	= "Bookingan anda sudah dilakukan pembayaran";
		$error['704']	= "Deposit anda tidak mencukupi untuk melakukan pembayaran ini";
		$error['705']	= "Data partner tidak ditemukan";
		$error['706']	= "Metode Pembayaran tidak ditemukan";
		$error['503']	= "Gagal mendapatkan harga, silahkan ulangi lagi";
		$error['403']	= "Minimum booking 2 hari dari tanggal keberangkatan";
		$error['604']	= "First Name atau Last Name tidak boleh 1 huruf atau disingkat";
		$error['605']	= "Nama mengandung angka atau special character";
		$error['606']	= "Booking mengalami gangguan";
		$error['607']	= "Booking penerbangan berangkat gagal";
		$error['608']	= "Booking penerbangan kembali gagal";
		$error['609']	= "Flight Sama";
		$error['610']	= "Rebook Penerbangan pergi gagal";
		$error['611']	= "Rebook penerbangan kembali gagal";
		
		#error ppob
		$error['801']	= "Data tidak ditemukan";
		
		
		
		return $error[$kode];
	}
	
	/**
	 * Only Mobile dimulai dengan 2 digit
	 * 
	 * @param unknown $kode
	 * @return string
	 */
	function getMobileErrorText($kode)
	{
		// Login_register 
		$error['10']	= "Username / Email tidak ditemukan";
		$error['11']	= "Password Salah";
		$error['12']	= "Nama depan tidak boleh kosong";
		$error['13']	= "Handphone tidak boleh kosong";
		$error['14']	= "Alamat tidak boleh kosong";
		$error['15']	= "Negara harus dipilih & tidak boleh kosong";
		$error['16']	= "Kodepos tidak boleh kosong";
		$error['17']	= "Email tidak boleh kosong";
		$error['18']	= "Password tidak boleh kosong";
		$error['19']	= "Konfirmasi Password tidak valid";
		$error['20']	= "Email Pengguna sudah pernah digunakan";
		$error['21']	= "Status partner tidak aktif";
		$error['22']	= "Skema Bisnis tidak boleh kosong/tidak ditemukan";
		$error['23']	= "Terjadi kesalahan ketika melakukan perubahan Password";
		$error['24']	= "Password Lama tidak boleh kosong";
		$error['25']	= "Password Baru tidak boleh kosong";
		$error['26']	= "Password Lama tidak valid";
		$error['27']	= "Password Baru harus lebih dari 5 Karakter";
		$error['28']	= "Konfirmasi Password Baru tidak valid";
		$error['29']	= "Password Baru tidak boleh sama dengan Password Lama";
		
		// Bank_account
		$error['30']	= "Partner tidak terdeteksi";
		$error['31']	= "Nama Bank tidak boleh kosong";
		$error['32']	= "Account rekening tidak boleh kosong";
		$error['33']	= "Account holder tidak boleh kosong";
		$error['34']	= "Data Bank tidak terdeteksi";
		$error['35']	= "Data Bank masih kosong";
		
		// Transaction
		$error['36']	= "Rountrip tidak terdeteksi";
		$error['37']	= "Data Transaksi tidak terdeteksi";
		$error['38']	= "Terjadi kesalahan ketika melakukan Limit";
		
		// Transaction Deposti
		$error['39']	= "Amount tidak boleh 0";
		$error['40']	= "Request Deposit sudah dilakukan, silahkan menunggu konfirmasi otomatis dari system";
		$error['41']	= "Anda harus login terlebih dahulu";
		$error['42']	= "Silahkan cek kesalahan dalam pengisian form.";
		
		// topup deposit
		$error['43']	= "bank topup tidak ditemukan";
		$error['44']	= "user belum melakukan topup deposit";
		$error['45']	= "Bank untuk melakukan topup belum dipilih";
		$error['46']	= "Bank partner belum dipilih";
		$error['47']	= "Jumlah transfer tidak boleh 0 atau kosong";
		$error['48']	= "topup deposit dengan nilai yang sama tidak di ijinkan, harap bedakan jumlahnya";
		$error['49']	= "transferan anda belum terdeteksi oleh sistem, harap lakukan transfer terlebih dahulu atau hubungi customer service kami";
		$error['53']	= "Jumlah minimal deposit Rp 25.000";
		
		// report
		$error['50']	= "start_date tidak boleh kosong";
		$error['51']	= "end_date tidak boleh kosong";
		$error['52']	= "laporan tidak tersedia untuk tanggal yang anda cari";
		
		return $error[$kode];
	}
}

