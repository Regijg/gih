<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


if ( ! function_exists('setting_diskon'))
{
	function setting_diskon($priceRange,$harga,$app)
	{
		$status = $priceRange[0]->status;
		if(strtolower($app)=="customer" && $status == "aktif"):
		
			foreach($priceRange as $p):
				$ranges[] = array($p->price_from,$p->price_to,$p->amount);
			endforeach;
			
			 $foundFlag=false;
			 foreach ($ranges as $range):
				if($harga>=$range[0] && $harga<=$range[1]):
					$foundFlag=true;
					$diskon = $range[2];
				endif;
			endforeach;
			if($foundFlag==false): $diskon = 0; endif;
		
		else:
			$diskon = 0;
		endif;
					
		return $diskon;
	}
	
}

if ( ! function_exists('extract_session_search'))
{
	function extract_session_search($parameter)
	{	
		$ci = & get_instance();
		$session_id	= $ci->converter->decode($parameter['scReferenceId']);
		$sesData	= $ci->mgeneral->getWhere(array('session_id' => $session_id), "log_session");
		
		foreach ($sesData as $sd) 
		{
			$search_info 	= json_decode($sd->search_post,true);
			$search_result	= json_decode($sd->search_data,true);
		}
			
			$departD = $search_result['schedule']['depart'];
			
			foreach ($departD as $departD) 
			{
				
				if ($parameter['fno'] == $departD['fno']) 
				{

					$airlineD	= $departD['airline_id'];
					$fltypeD	= $departD['type'];
					$classD		= $departD['class'];
					
					foreach ($classD as $classD) 
					{
						
						if ($parameter['class_id'] == $classD['class_id']) 
						{
							$class_id1 	= $classD['class_id'];
							$val1		= $ci->converter->decode($classD['value']);
							$val1_dec   = json_decode($val1, true);
							$class1		= $classD['class_name'];
							$classname1 = $ci->mprice->getClass($departD['airline_id'], $class1);

							if ($fltypeD == "connecting") 
							{
								$conFlight = $departD['connecting_flight'];

								foreach ($conFlight as $conFlight) 
								{
									if ($parameter['class_id'] == $class_id1) 
									{
										$conDepart[]	= array('fno' 	=> $conFlight['fno'],
																'date' 	=> $conFlight['date'],
																'dep' 	=> $conFlight['from'],
																'etd' 	=> $conFlight['etd'],
																'arr' 	=> $conFlight['to'],
																'eta' 	=> $conFlight['eta']);
									}
								}

								$utCls = 0;
								foreach ($conFlight['class_id'] as $conFlightC) 
								{
									if ($utCls == "0") 
									{
                                        $val2                       = $ci->converter->decode($conFlightC['value']);
                                        $classC                     = $conFlightC['class_name'];
                                        $conDepart[0]['class']      = $classC;
                                        $conDepart[0]['class_name'] = $ci->mprice->getClass($departD['airline_id'], $classC);
                                        $priceCon1                  = $conFlightC['price'];
									}
									$utCls++;
								}
							}

							$detailFlight 	= array('airline_id' 	=> $departD['airline_id'],
													'airline_name'	=> $departD['airline_name'],
													'flights_type' 	=> $departD['type'],
													'flights'		=> array('fno'  => $departD['fno'],
																			 'date' => $departD['date'],
																			 'dep' => $departD['from'],
																			 'etd' 	=> $departD['etd'],
																			 'arr' 	=> $departD['to'],
																			 'eta' 	=> $departD['eta'])
													);
						
						}
						
					}

					if ($fltypeD == "connecting"):
						$detailFlight['flights_connecting'] 	= $conDepart;
						$detailFlight['flights']['class'] 		= $class1;
						$detailFlight['flights']['class_name'] 	= $classname1;
					elseif($fltypeD == "transit"):
						$detailFlight['flights']['via'] 		= $departD['flights']['via'];
						$detailFlight['flights']['class'] 		= $class1;
						$detailFlight['flights']['class_name'] 	= $classname1;
					else:
						$detailFlight['flights']['class'] 		= $class1;
						$detailFlight['flights']['class_name'] 	= $classname1;
					endif;
				}
			}
			
			$postDetail 	= array('fltype'	=> "oneway",
									'type'		=> $detailFlight['type'],
									'from' 		=> $search_info['dep'],
									'to' 		=> $search_info['arr'],
									'tglP' 		=> $search_info['date'],
									'tglK' 		=> $search_info['date'],
									'adult' 	=> $search_info['adult'],
									'child' 	=> $search_info['child'],
									'infant' 	=> $search_info['infant'],
									'value' 	=> array('value_1_a' => $val1, 'value_1_b' => $val2),
									'price_con'	=> $priceCon1);
			
			$result = array('search_post'	=> $search_info,
							'detail_flight'	=> $detailFlight,
							'post_to_detail'=> $postDetail,
							'depart_ref'	=> $search_result['schedule']['depart_ref']);
		#print_r($result);
		return $result;
	}
}


if ( ! function_exists('extract_session_book'))
{
	function extract_session_book($parameter="")
	{	
		$ci = & get_instance();
		$session_id = $ci->converter->decode($parameter['fareReferenceId']);
		
		#extract session from DB		
		$sesData = $ci->mgeneral->getWhere(array('session_id' => $session_id), "log_session");
		foreach ($sesData as $sd) 
		{
			$search_post	= json_decode($sd->search_post);
			$search_result	= json_decode($sd->search_data);
			$detail_post	= json_decode($sd->post_detail);
			$detail_result	= json_decode($sd->data_detail);
		}
		
		#get rountrip & total pax		
		$tPax		= $search_post->adult+$search_post->child+$search_post->infant; if($tPax==""): $tPax = "1"; endif;#get total passenger
		$roundtrip	= $search_post->roundtrip;
		
		#prepare departure flight data
		$flight1	= array('fltype' 	=> "oneway",
							'from' 		=> $search_post->dep,
							'to' 		=> $search_post->arr,
							'tglP' 		=> $search_post->date,
							'adult' 	=> $search_post->adult,
							'child' 	=> $search_post->child,
							'infant' 	=> $search_post->infant,
							'airline'	=> $detail_result->depart->schedule->airline_id,
							'fno' 		=> $detail_result->depart->schedule->flights->fno,
							'etd' 		=> $detail_result->depart->schedule->flights->etd,
							'class'		=> $detail_result->depart->schedule->flights->class,
							'value' 	=> array('value_1_a' => $detail_result->depart->value->value1,
												 'value_1_b' => $detail_result->depart->value->value2));

		if ($detail_result->depart->schedule->flights_type == 'connecting') 
		{
			$dtConn = $detail_result->depart->schedule->flights_connecting;
			$arrConn 	= array('from1Db'	=> $detail_result->depart->schedule->flights->dep,
								'to1Db' 	=> $detail_result->depart->schedule->flights->arr,
								'etd1Db' 	=> $detail_result->depart->schedule->flights->etd,
								'eta1Db' 	=> $detail_result->depart->schedule->flights->eta,
								'fno1Db' 	=> $detail_result->depart->schedule->flights->fno,
								'tglP2Db'	=> $dtConn[0]->date,
								'from2Db' 	=> $dtConn[0]->dep,
								'to2Db' 	=> $dtConn[0]->arr,
								'fno2Db' 	=> $dtConn[0]->fno,
								'etd2Db' 	=> $dtConn[0]->etd,
								'eta2Db' 	=> $dtConn[0]->eta);
			$flight1 = array_merge_recursive($flight1, $arrConn);
		}
		
		$result = array('roundtrip'		=> $roundtrip,
						'tpax'			=> $tPax,
						'session_data'	=> $sesData,
						'depart'		=> $flight1);
						
		if($roundtrip=="return"):
		
			#prepare return flight data
			$flight2	= array('fltype' 	=> "oneway",
								'from' 		=> $search_post->arr,
								'to' 		=> $search_post->dep,
								'tglP' 		=> $search_post->return_date,
								'adult' 	=> $search_post->adult,
								'child' 	=> $search_post->child,
								'infant' 	=> $search_post->infant,
								'airline'	=> $detail_result->return->schedule->airline_id,
								'fno' 		=> $detail_result->return->schedule->flights->fno,
								'etd' 		=> $detail_result->return->schedule->flights->etd,
								'class'		=> $detail_result->return->schedule->flights->class,
								'value' 	=> array('value_1_a' => $detail_result->return->value->value1,
													 'value_1_b' => $detail_result->return->value->value2));
	
			if ($detail_result->return->schedule->flights_type == 'connecting') 
			{
				$dtConn = $detail_result->return->schedule->flights_connecting;
	
				$arrConn2 	= array('from1Db'	=> $detail_result->return->schedule->flights->dep,
									'to1Db' 	=> $detail_result->return->schedule->flights->to,
									'etd1Db' 	=> $detail_result->return->schedule->flights->etd,
									'eta1Db' 	=> $detail_result->return->schedule->flights->eta,
									'fno1Db' 	=> $detail_result->return->schedule->flights->fno,
									'tglP2Db'	=> $dtConn[0]->date,
									'from2Db' 	=> $dtConn[0]->dep,
									'to2Db' 	=> $dtConn[0]->arr,
									'fno2Db' 	=> $dtConn[0]->fno,
									'etd2Db' 	=> $dtConn[0]->etd,
									'eta2Db' 	=> $dtConn[0]->eta);
				$flight2 = array_merge_recursive($flight2, $arrConn2);
			}
			
			$result['return'] = $flight2;
			
		endif;
		
		return $result;
	}
	
}

if ( ! function_exists('save_book_data'))
{
	function save_book_data($session_data="",$parameter="",$bookdata="",$bookdata2="")
	{	
		$ci = & get_instance();
		$search_post	= json_decode($session_data[0]->search_post);
        $search_result	= json_decode($session_data[0]->search_data);
        $detail_post	= json_decode($session_data[0]->post_detail);
        $detail_result	= json_decode($session_data[0]->data_detail);

        $roundtrip = $search_post->roundtrip;
        $flightRute= "domestik";
		
        $varBook = array('dtf_roundtrip' 		=> $roundtrip,
						'dtf_status'			=> "book",
						'partner_id'			=> "1",
						'dtf_from' 				=> $search_post->dep,
						'dtf_to' 				=> $search_post->arr,
						'dtf_adult' 			=> $search_post->adult,
						'dtf_child' 			=> $search_post->child,
						'dtf_infant'			=> $search_post->infant,
						'customer_name' 		=> $parameter['customer_name'],
						'customer_phone'		=> $parameter['customer_phone'],
						'customer_email'		=> $parameter['customer_email'],
						'depart_airlines_id'	=> $detail_result->depart->schedule->airline_id,
						'depart_type' 			=> $detail_result->depart->schedule->flights_type,
						'depart_book_code' 		=> $bookdata['book_code'],
						'depart_book_status' 	=> "hold",
						'depart_expired_date'	=> $bookdata['time_limit'],
						'dtf_tiket_price'		=> $detail_result->total_fares,
						'dtf_comission'			=> $detail_result->commission,
						'dtf_margin'			=> $parameter['margin'],
						'dtf_rute' 				=> $flightRute,
						'app'					=> $detail_post->client->client_app);
        
        if($roundtrip == "return"):
        	$varBook['return_airlines_id']	= $detail_result->return->schedule->airline_id;
			$varBook['return_type']			= $detail_result->return->schedule->flights_type;
			$varBook['return_book_code']	= $bookdata2['book_code'];
			$varBook['return_book_status']	= "hold";
			$varBook['return_expired_date']	= $bookdata2['time_limit'];
        endif;

		#$book_id = "2";
        $book_id = $ci->mgeneral->save($varBook, "dt_flight");
		
		#save schedule depart
		if($book_id!=""):
			save_schedule_data("depart",$book_id,$detail_result->depart->schedule->airline_id,$detail_result);
		endif;

		#save schedule return
		if($roundtrip=="return" && $book_id!=""): 
			save_schedule_data("return",$book_id,$detail_result->return->schedule->airline_id,$detail_result); 
		endif;
		
		if($book_id!=""):
	        $pax = $parameter['psgr'];
	        for ($ps = 0; $ps < count($pax); $ps++) {
	        	
	            $varPsgr = array('dtf_id' 				=> $book_id,
								'pax_type' 				=> $pax[$ps]['psgr_type'],
								'pax_title' 			=> $pax[$ps]['title'],
								'pax_first_name'		=> $pax[$ps]['first_name'],
								'pax_last_name' 		=> $pax[$ps]['last_name'],
								'pax_dob' 				=> ($pax[$ps]['birthdate']!=""?$pax[$ps]['birthdate']:NULL),
								'pax_id_card' 			=> $pax[$ps]['id_psgr'],
								'pax_pasport' 			=> $pax[$ps]['paspor'],
								'pax_expired_pasport' 	=> $pax[$ps]['expire_paspor'],
								'pax_country' 			=> $pax[$ps]['country_paspor']);
	            $ci->mgeneral->save($varPsgr, "dt_flight_pax");
	        }
	    endif;
		
        return $book_id;
	}
}

if ( ! function_exists('save_schedule_data'))
{
	function save_schedule_data($sch_type="",$book_id="",$airline="",$detail_result="")
	{	
		$ci = & get_instance();
		
		if($sch_type=="depart"):
			$sch_data = $detail_result->depart;
		else:
			$sch_data = $detail_result->return;
		endif;
		
		switch ($airline):
			case "1";
			case "2";
				$baggageInfo	= "20";
			break;
			default:
				$baggageInfo	= "20";
        endswitch;

        $varSchedule = array('airlines_id' 			=> $airline,
							'dtf_id' 				=> $book_id,
							'sch_type' 				=> $sch_type,
							'sch_date'				=> $sch_data->schedule->flights->date,
							'sch_from'				=> $sch_data->schedule->flights->dep,
							'sch_to'				=> $sch_data->schedule->flights->arr,
							'sch_via'				=> $sch_data->schedule->flights->via,
							'sch_etd'				=> $sch_data->schedule->flights->etd,
							'sch_eta'				=> $sch_data->schedule->flights->eta,
							'sch_class_code'		=> $sch_data->schedule->flights->class,
							'sch_class_name'		=> $sch_data->schedule->flights->class_name,
							'sch_baggage'			=> $baggageInfo,
							'sch_flight_no'			=> $sch_data->schedule->flights->fno,
							'sch_adult_price'		=> $sch_data->fares_detail->adult->basic_fare,
							'sch_adult_taxes_fees' 	=> $sch_data->fares_detail->adult->taxes_fees,
							'sch_child_price' 		=> $sch_data->fares_detail->child->basic_fare,
							'sch_child_taxes_fees' 	=> $sch_data->fares_detail->child->taxes_fees,
							'sch_infant_price'		=> $sch_data->fares_detail->infant->basic_fare,
							'sch_infant_taxes_fees' => $sch_data->fares_detail->infant->taxes_fees,
							'sch_ticket_price' 		=> $sch_data->total_fares,
							'sch_ticket_comisi'		=> $sch_data->commission);
		
		$ci->mgeneral->save($varSchedule, "dt_flight_schedule");

        if ($sch_data->schedule->flights_type == "connecting")
		{
            foreach ($sch_data->schedule->flights_connecting as $conDepart) {
                
				if ($conDepart->class == "-" || $conDepart->class == "")
				{
                    $clsDepartC = $sch_data->schedule->class;
                    $clsDepartN = $sch_data->schedule->class_name;
                } else {
                    $clsDepartC = $conDepart->class;
                    $clsDepartN = $conDepart->class_name;
                }

                $varScheduleC = array('airlines_id' 			=> $airline,
									  'dtf_id'					=> $book_id,
									  'sch_type' 				=> $sch_type."_connecting",
									  'sch_date'				=> $conDepart->date,
									  'sch_from'				=> $conDepart->dep,
									  'sch_to'					=> $conDepart->arr,
									  'sch_via'					=> $conDepart->via,
									  'sch_etd'					=> $conDepart->etd,
									  'sch_eta'					=> $conDepart->eta,
									  'sch_class_code'			=> $clsDepartC,
									  'sch_class_name'			=> $clsDepartN,
									  'sch_baggage'				=> $baggageInfo,
									  'sch_flight_no'			=> $conDepart->fno,
									  'sch_adult_price'			=> "0",
									  'sch_adult_taxes_fees' 	=> "0",
									  'sch_child_price' 		=> "0",
									  'sch_child_taxes_fees' 	=> "0",
									  'sch_infant_price'		=> "0",
									  'sch_infant_taxes_fees' 	=> "0",
									  'sch_ticket_price' 		=> "0",
									  'sch_ticket_comisi'		=> "0");
                $ci->mgeneral->save($varScheduleC, "dt_flight_schedule");
            }
        }
	}
	
}

if ( ! function_exists('build_book_result'))
{
	function build_book_result($book_id="",$session_data="",$bookData1="",$psgr="",$bookData2="",$margin="",$cust="")
	{	
		$ci = & get_instance();
        $search_post	= json_decode($session_data['session_data'][0]->search_post,true);
		$detail_result	= json_decode($session_data['session_data'][0]->data_detail,true);
		$depart			= $detail_result['depart'];
		
		$kdbook1 = array('booking_code'		=> $bookData1['book_code'],
						 'booking_status'	=> strtoupper($bookData1['status']),
						 'booking_limit'	=> $bookData1['time_limit']);
		$depart	= array_slice($depart, 0, 2, true) + $kdbook1 + array_slice($depart, 2, count($depart) - 1, true) ;
		
		if(trim($margin) == ""): $margin = 0; endif; 
			
		if($search_post['roundtrip']=="oneway"):
			
			unset($depart['value']);
			unset($depart['connecting_flight']);
			
			$result = array('rest_no'		=> "0",
							'book_id'		=> $ci->converter->encode($book_id),
							'depart'		=> $depart,
							'customer'		=> $cust,
							'psgr'			=> $psgr,
							'margin'		=> $margin,
							'total_fares'	=> $detail_result['total_fares'],
							'commission'	=> $detail_result['commission'],
							'NTA'			=> $detail_result['NTA']);			
		
		elseif($search_post['roundtrip']=="return"):
		
			$return			= $detail_result['return'];
			
			$kdbook2 = array('booking_code'		=> $bookData2['book_code'],
						 	 'booking_status'	=> strtoupper($bookData2['status']),
						 	 'booking_limit'	=> $bookData2['time_limit']);
							 
			unset($depart['value']);
			unset($depart['connecting_flight']);
			unset($return['value']);
			unset($return['connecting_flight']);
			
			$return	= array_slice($return, 0, 2, true) + $kdbook2 + array_slice($return, 2, count($return) - 1, true) ;
						
			$result = array('rest_no'		=> "0",
							'book_id'		=> $ci->converter->encode($book_id),
							'depart'		=> $depart,
							'return'		=> $return,
							'psgr'			=> $psgr,
							'customer'		=> $cust,
							'margin'		=> $margin,
							'total_fares'	=> $detail_result['total_fares'],
							'commission'	=> $detail_result['commission'],
							'NTA'			=> $detail_result['NTA']);
		
		endif;
		
		return $result;
	}
}

if ( ! function_exists('lion_update'))
{
    function lion_update($agent_id="",$session_data="",$bookdata="",$bookdata2="")
    {   
        $ci = & get_instance();
        $search_post    = json_decode($session_data[0]->search_post);
        $search_result  = json_decode($session_data[0]->search_data);
        $detail_post    = json_decode($session_data[0]->post_detail);
        $detail_result  = json_decode($session_data[0]->data_detail);

        $roundtrip      = $search_post->roundtrip;
        $dAirline       = $detail_result->depart->schedule->airline_id;
        $flightType     = $detail_result->depart->schedule->flights_type;
        if($flightType=="connecting"): $iwjr = "10000"; else: $iwjr = "5000"; endif;
            
            if($dAirline=="5"):
                
                if($detail_result->depart->fares_detail->adult->sitter >= 1):
                    $detail_result->depart->fares_detail->adult->basic_fare = $bookdata['price_adult'];
                    $detail_result->depart->fares_detail->adult->taxes_fees = $bookdata['tax_adult']+$iwjr;
                    $detail_result->depart->fares_detail->adult->adult_fare = ($bookdata['price_adult']+$bookdata['tax_adult']+$iwjr) * $detail_result->depart->fares_detail->adult->sitter;
                endif;

                if($detail_result->depart->fares_detail->child->sitter >= 1):
                    $detail_result->depart->fares_detail->child->basic_fare = $bookdata['price_child'];
                    $detail_result->depart->fares_detail->child->taxes_fees = $bookdata['tax_child']+$iwjr;
                    $detail_result->depart->fares_detail->child->child_fare = ($bookdata['price_child']+$bookdata['tax_child']+$iwjr) * $detail_result->depart->fares_detail->child->sitter;
                endif;

                if($detail_result->depart->fares_detail->infant->sitter >= 1):
                    $detail_result->depart->fares_detail->infant->basic_fare = $bookdata['price_infant'];
                    $detail_result->depart->fares_detail->infant->taxes_fees = $bookdata['tax_infant']+$iwjr;
                    $detail_result->depart->fares_detail->infant->infant_fare= ($bookdata['price_infant']+$bookdata['tax_infant']+$iwjr) * $detail_result->depart->fares_detail->infant->sitter;
                endif;
                
                #hitung nilai komisi berdsarkan skema bisnisnya
                /*if($agent_id==""):
                    $komisiAgent    = 0;
                    $komisiClient   = number_format($bookdata['komisi'], 0, ',', '');   
                else:
                    $agent_id       = $ci->converter->decode($agent_id);
                    $cekTipeKomisi  = $ci->mquery->get_skema_komisi($agent_id,"5");
                    
                    if($cekTipeKomisi['tipe'] == "fix"):
                        echo "test";
                        $nilaiKomisi    = $cekTipeKomisi['nilai'];  
                    elseif($cekTipeKomisi['tipe'] == "percen"):
                        echo "test2";
                        $nilaiKomisi    = ($bookdata['komisi']*$cekTipeKomisi['nilai'])/100;
                    else:
                        echo "test3";
                        $nilaiKomisi    = $bookdata['komisi'];
                    endif;
                    #$komisiAgent   = number_format(($bookdata['komisi']-$nilaiKomisi), 0, ',', '');
                    #$komisiClient  = number_format(($bookdata['komisi']-($bookdata['komisi']-$nilaiKomisi)), 0, ',', '');
                    $komisiClient   = number_format(($bookdata['komisi']-$nilaiKomisi), 0, ',', '');
                    $komisiAgent    = number_format(($bookdata['komisi']-($bookdata['komisi']-$nilaiKomisi)), 0, ',', '');  
                endif;*/
                $komisiClient   = 0;
                $komisiAgent    = number_format($bookdata['komisi'], 0, ',', '');   
                
                $agentPrice     = number_format(($bookdata['harga_tiket'] - $komisiAgent), 0, ',', '');
                
                /*$detail_result->depart->comisi = $bookdata['komisi'];
                $detail_result->depart->comisi_agent = $komisiAgent;
                $detail_result->depart->comisi_client = $komisiClient;
                $detail_result->depart->total_fare = $bookdata['harga_tiket'];
            
                $detail_result->total_fare = $bookdata['harga_tiket'];
                $detail_result->commission = $komisiAgent;
                $detail_result->agent_fare = $agentPrice;
                $detail_result->commission_client = $komisiClient;*/
                
                $detail_result->depart->commission  = (int)$bookdata['komisi'];
                $detail_result->depart->total_fares = (int)$bookdata['harga_tiket'];
                $detail_result->depart->NTA         = (int)$agentPrice;
            endif;
            
        if($roundtrip=="return"):
        
            $rAirline       = $detail_result->return->schedule->airline_id;
            $flightType     = $detail_result->return->schedule->flights_type;
            if($flightType=="connecting"): $iwjr = "10000"; else: $iwjr = "5000"; endif;
            
            if($rAirline=="5"):
                
                if($detail_result->return->fares_detail->adult->sitter >= 1):
                    $detail_result->return->fares_detail->adult->basic_fare = $bookdata2['price_adult'];
                    $detail_result->return->fares_detail->adult->taxes_fees = $bookdata2['tax_adult']+$iwjr;
                    $detail_result->return->fares_detail->adult->adult_fare = ($bookdata2['price_adult']+$bookdata2['tax_adult']+$iwjr) * $detail_result->return->fares_detail->adult->sitter;
                endif;

                if($detail_result->return->fares_detail->child->sitter >= 1):
                    $detail_result->return->fares_detail->child->basic_fare = $bookdata2['price_child'];
                    $detail_result->return->fares_detail->child->taxes_fees = $bookdata2['tax_child']+$iwjr;
                    $detail_result->return->fares_detail->child->child_fare = ($bookdata2['price_child']+$bookdata2['tax_child']+$iwjr) * $detail_result->return->fares_detail->child->sitter;
                endif;

                if($detail_result->return->fares_detail->infant->sitter >= 1):
                    $detail_result->return->fares_detail->infant->basic_fare = $bookdata2['price_infant'];
                    $detail_result->return->fares_detail->infant->taxes_fees = $bookdata2['tax_infant']+$iwjr;
                    $detail_result->return->fares_detail->infant->infant_fare= ($bookdata2['price_infant']+$bookdata2['tax_infant']+$iwjr) * $detail_result->return->fares_detail->infant->sitter;
                endif;
                
                #hitung nilai komisi berdsarkan skema bisnisnya
                /*if($agent_id==""):
                    $komisiAgent    = 0;
                    $komisiClient   = number_format($bookdata2['komisi'], 0, ',', '');  
                else:
                    $agent_id       = $ci->converter->decode($agent_id);
                    $cekTipeKomisi  = $ci->mquery->get_skema_komisi($agent_id,"5");
                    
                    if($cekTipeKomisi['tipe'] == "fix"):
                        $nilaiKomisi    = $cekTipeKomisi['nilai'];  
                    elseif($cekTipeKomisi['tipe'] == "percen"):
                        $nilaiKomisi    = ($bookdata2['komisi']*$cekTipeKomisi['nilai'])/100;
                    else:
                        $nilaiKomisi    = $bookdata2['komisi'];
                    endif;
                    #$komisiAgent   = number_format(($bookdata2['komisi']-$nilaiKomisi), 0, ',', '');
                    #$komisiClient  = number_format(($bookdata2['komisi']-($bookdata2['komisi']-$nilaiKomisi)), 0, ',', '');
                    $komisiClient   = number_format(($bookdata2['komisi']-$nilaiKomisi), 0, ',', '');
                    $komisiAgent    = number_format(($bookdata2['komisi']-($bookdata2['komisi']-$nilaiKomisi)), 0, ',', '');
                    
                endif;*/
                
                $komisiClient   = 0;
                $komisiAgent    = number_format($bookdata2['komisi'], 0, ',', '');  
                
                $agentPrice     = number_format(($bookdata2['harga_tiket'] - $komisiAgent), 0, ',', '');
                
                /*$detail_result->return->comisi = $bookdata2['komisi'];
                $detail_result->return->comisi_agent = $komisiAgent;
                $detail_result->return->comisi_client = $komisiClient;
                $detail_result->return->total_fare = $bookdata2['harga_tiket'];*/
                $detail_result->return->commission  = (int)$bookdata2['komisi'];
                $detail_result->return->total_fares = (int)$bookdata2['harga_tiket'];
                $detail_result->return->NTA         = (int)$agentPrice;
                    
            endif;
            
        endif;
        
        
        if($roundtrip=="oneway" && $dAirline=="5"):
            
            /*$agentPrice       = number_format(($detail_result->depart->total_fare - $detail_result->depart->comisi_agent), 0, ',', '');
            
            $detail_result->total_fare = $detail_result->depart->total_fare;
            $detail_result->commission = $detail_result->depart->comisi_agent;
            $detail_result->agent_fare = $agentPrice;
            $detail_result->commission_client = $detail_result->depart->comisi_client;*/
            
            $agentPrice     = number_format(($detail_result->depart->total_fares - $detail_result->depart->commission), 0, ',', '');
            $detail_result->total_fares = (int)$detail_result->depart->total_fares;
            $detail_result->commission  = (int)$detail_result->depart->commission;
            $detail_result->NTA         = (int)$agentPrice;
            
            $varSession = array('data_detail' => json_encode($detail_result));
            $ci->mgeneral->update(array('session_id' => $session_data[0]->session_id), $varSession, "log_session");
            
        elseif($dAirline=="5" || $rAirline=="5"):
            
            /*$agentPriceD  = number_format(($detail_result->depart->total_fare - $detail_result->depart->comisi_agent), 0, ',', '');
            $agentPriceR    = number_format(($detail_result->return->total_fare - $detail_result->return->comisi_agent), 0, ',', '');
            
            $detail_result->total_fare = $detail_result->depart->total_fare+$detail_result->return->total_fare;
            $detail_result->commission = $detail_result->depart->comisi_agent+$detail_result->return->comisi_agent;
            $detail_result->agent_fare = $agentPriceD+$agentPriceR;
            $detail_result->commission_client = $detail_result->depart->comisi_client+$detail_result->return->comisi_client;*/
            
            $agentPriceD    = number_format(($detail_result->depart->total_fares - $detail_result->depart->commission), 0, ',', '');
            $agentPriceR    = number_format(($detail_result->return->total_fares - $detail_result->return->commission), 0, ',', '');
            
            $detail_result->total_fares = (int)($detail_result->depart->total_fares+$detail_result->return->total_fares);
            $detail_result->commission  = (int)($detail_result->depart->commission+$detail_result->return->commission);
            $detail_result->NTA         = (int)($agentPriceD+$agentPriceR);
            
            $varSession = array('data_detail' => json_encode($detail_result));
            $ci->mgeneral->update(array('session_id' => $session_data[0]->session_id), $varSession, "log_session");
            
        endif;
        
        #echo "<pre>";
        #print_r($detail_result);
        return json_encode($detail_result);
    }
}

if ( ! function_exists('return_update'))
{
    function return_update($session_data="", $book_data="", $book_data2="")
    {   
        $ci = & get_instance();
        $search_post    = json_decode($session_data[0]->search_post);
        $search_result    = json_decode($session_data[0]->search_data);
        $detail_post    = json_decode($session_data[0]->post_detail);
        $detail_result    = json_decode($session_data[0]->data_detail);

        $roundtrip        = $search_post->roundtrip;
        $dAirline        = $detail_result->depart->schedule->airline_id;
        $flightType        = $detail_result->depart->schedule->flights_type;

        $arr_airline    = array("3");
        $total_depart   = 0;
        $total_return   = 0;
        
        $rAirline        = $detail_result->return->schedule->airline_id;

        if($dAirline == $rAirline && in_array($dAirline, $arr_airline) && $book_data['status'] == "CONFIRMED"):

            if($detail_result->return->fares_detail->adult->sitter >= 1):
                $return_basic_adult = $book_data['basic_adult'] - $detail_result->depart->fares_detail->adult->basic_fare;

                $return_tax_adult   = $book_data['tax_adult'] - $detail_result->depart->fares_detail->adult->taxes_fees;
                $detail_result->return->fares_detail->adult->basic_fare = $return_basic_adult;
                $detail_result->return->fares_detail->adult->taxes_fees = $return_tax_adult;
                $detail_result->return->fares_detail->adult->adult_fare = ($return_basic_adult+$return_tax_adult) * $detail_result->return->fares_detail->adult->sitter;
                $total_return = $total_return + ($detail_result->return->fares_detail->adult->adult_fare);
            endif;

            if($detail_result->return->fares_detail->child->sitter >= 1):
                $return_basic_child = $book_data['basic_child'] - $detail_result->depart->fares_detail->child->basic_fare;
                $return_tax_child   = $book_data['tax_child'] - $detail_result->depart->fares_detail->child->taxes_fees;
                $detail_result->return->fares_detail->child->basic_fare = $return_basic_child;
                $detail_result->return->fares_detail->child->taxes_fees = $return_tax_child;
                $detail_result->return->fares_detail->child->child_fare = ($return_basic_child+$return_tax_child) * $detail_result->return->fares_detail->child->sitter;
                $total_return = $total_return + ($detail_result->return->fares_detail->child->child_fare);
            endif;

            if($detail_result->return->fares_detail->infant->sitter >= 1):
                $return_basic_infant = $book_data['basic_infant'] - $detail_result->depart->fares_detail->infant->basic_fare;
                $return_tax_infant   = $book_data['tax_infant'] - $detail_result->depart->fares_detail->infant->taxes_fees;
                $detail_result->return->fares_detail->infant->basic_fare = $return_basic_infant;
                $detail_result->return->fares_detail->infant->taxes_fees = $return_tax_infant;
                $detail_result->return->fares_detail->infant->infant_fare = ($return_basic_infant+$return_tax_infant) * $detail_result->return->fares_detail->infant->sitter;
                $total_return = $total_return + ($detail_result->return->fares_detail->infant->infant_fare);
            endif;

            $detail_result->return->commission     = $book_data['komisi'] - $detail_result->depart->commission;
            $detail_result->return->total_fares    = $total_return;
            $detail_result->return->NTA            = ($total_return - $detail_result->return->commission);

            $agentPriceD    = number_format(($detail_result->depart->total_fares - $detail_result->depart->commission), 0, ',', '');
            $agentPriceR    = number_format(($detail_result->return->total_fares - $detail_result->return->commission), 0, ',', '');

            $detail_result->total_fares    = $detail_result->depart->total_fares+$detail_result->return->total_fares;
            $detail_result->commission     = $detail_result->depart->commission+$detail_result->return->commission;
            $detail_result->NTA            = $agentPriceD+$agentPriceR;

            $varSession    = array('data_detail' => json_encode($detail_result));
            $ci->mgeneral->update(array('session_id' => $session_data[0]->session_id), $varSession, "log_session");

        endif;

        #echo "<pre>";
        #print_r($detail_result);
        return json_encode($detail_result);
    }
}

if ( ! function_exists('build_issue_result'))
{
	function build_issue_result($bookid="")
	{	
		$ci = & get_instance();
		
		$fData	 = $ci->mgeneral->getWhere(array('dtf_id'=>$bookid),"dt_flight");
		$sch 	 = $ci->mgeneral->getWhere(array('dtf_id'=>$bookid),"dt_flight_schedule");
		$psgr 	 = $ci->mgeneral->getWhere(array('dtf_id'=>$bookid),"dt_flight_pax");
		
		foreach ($sch as $j) {
			switch ($j->sch_type) {
				case "depart";
					$schDepart = array('airline_id' 	=> $j->airlines_id,
										'airline_name' 	=> $ci->mgeneral->getValue('subproduk_name',array('subproduk_id'=>$j->airlines_id),"mstr_subproduk"),
										'flights_type' 	=> $fData[0]->depart_type,
										'fno' 			=> $j->sch_flight_no,
										'date' 			=> $j->sch_date,
										'dep' 			=> $j->sch_from,
										'dep_city'		=> $ci->converter->set_codeToCity($j->sch_from),
										'etd' 			=> $j->sch_etd,
										'via' 			=> $j->sch_via,
										'arr' 			=> $j->sch_to,
										'arr_city'		=> $ci->converter->set_codeToCity($j->sch_to),
										'eta' 			=> $j->sch_eta,
										'cabin_code' 	=> $j->sch_class_code);
					
					$priceDepart = array();
					if($fData[0]->dtf_adult != 0):
						$adultPrice = array('sitter'=>$fData[0]->dtf_adult,
											'basic_fare'=>$j->sch_adult_price,
											'taxes_fees'=>$j->sch_adult_taxes_fees,
											'adult_fare'=>($j->sch_adult_price+$j->sch_adult_taxes_fees));
						$priceDepart['adult'] = $adultPrice;
					endif;
					if($fData[0]->dtf_child != 0):
						$childPrice = array('sitter'=>$fData[0]->dtf_child,
											'basic_fare'=>$j->sch_child_price,
											'taxes_fees'=>$j->sch_child_taxes_fees,
											'adult_fare'=>($j->sch_child_price+$j->sch_child_taxes_fees));
						$priceDepart['child'] = $childPrice;
					endif;
					if($fData[0]->dtf_infant != 0):
						$infantPrice = array('sitter'=>$fData[0]->dtf_infant,
											'basic_fare'=>$j->sch_infant_price,
											'taxes_fees'=>$j->sch_infant_taxes_fees,
											'adult_fare'=>($j->sch_infant_price+$j->sch_infant_taxes_fees));
						$priceDepart['infant'] = $infantPrice;
					endif;
					
					break;
				case "depart_connecting";
					$schDepartC[] = array('fno' 			=> $j->sch_flight_no,
											'date' 			=> $j->sch_date,
											'dep' 			=> $j->sch_from,
											'dep_city'		=> $ci->converter->set_codeToCity($j->sch_from),
											'etd' 			=> $j->sch_etd,
											'via' 			=> $j->sch_via,
											'arr' 			=> $j->sch_to,
											'arr_city'		=> $ci->converter->set_codeToCity($j->sch_to),
											'eta' 			=> $j->sch_eta,
											'cabin_code' 	=> $j->sch_class_code);
					break;
				case "return";
					$schReturn = array('airline_id' 	=> $j->airlines_id,
										'airline_name' 	=> $ci->mgeneral->getValue('subproduk_name',array('subproduk_id'=>$j->airlines_id),"mstr_subproduk"),
										'flights_type' 	=> $fData[0]->return_type,
										'fno' 			=> $j->sch_flight_no,
										'date' 			=> $j->sch_date,
										'dep' 			=> $j->sch_from,
										'dep_city'		=> $ci->converter->set_codeToCity($j->sch_from),
										'etd' 			=> $j->sch_etd,
										'via' 			=> $j->sch_via,
										'arr' 			=> $j->sch_to,
										'arr_city'		=> $ci->converter->set_codeToCity($j->sch_to),
										'eta' 			=> $j->sch_eta,
										'cabin_code' 	=> $j->sch_class_code);
					
					$priceReturn = array();
					if($fData[0]->dtf_adult != 0):
						$adultPrice = array('sitter'=>$fData[0]->dtf_adult,
											'basic_fare'=>$j->sch_adult_price,
											'taxes_fees'=>$j->sch_adult_taxes_fees,
											'adult_fare'=>($j->sch_adult_price+$j->sch_adult_taxes_fees));
						$priceReturn['adult'] = $adultPrice;
					endif;
					if($fData[0]->dtf_child != 0):
						$childPrice = array('sitter'=>$fData[0]->dtf_child,
											'basic_fare'=>$j->sch_child_price,
											'taxes_fees'=>$j->sch_child_taxes_fees,
											'adult_fare'=>($j->sch_child_price+$j->sch_child_taxes_fees));
						$priceReturn['child'] = $childPrice;
					endif;
					if($fData[0]->dtf_infant != 0):
						$infantPrice = array('sitter'=>$fData[0]->dtf_infant,
											'basic_fare'=>$j->sch_infant_price,
											'taxes_fees'=>$j->sch_infant_taxes_fees,
											'adult_fare'=>($j->sch_infant_price+$j->sch_infant_taxes_fees));
						$priceReturn['infant'] = $infantPrice;
					endif;
					break;
				case "return_connecting";
					$schReturnC[] = array('fno' 		=> $j->sch_flight_no,
											'date' 		=> $j->sch_date,
											'dep' 		=> $j->sch_from,
											'dep_city'	=> $ci->converter->set_codeToCity($j->sch_from),
											'etd' 		=> $j->sch_etd,
											'via' 		=> $j->sch_via,
											'arr' 		=> $j->sch_to,
											'arr_city'	=> $ci->converter->set_codeToCity($j->sch_to),
											'eta' 		=> $j->sch_eta,
											'cabin_code'=> $j->sch_class_code);
					break;
			}
		}
		
		$customer	= array('customer_name'	=> $fData[0]->customer_name,
							'customer_phone'=> $fData[0]->customer_phone,
							'customer_email'=> $fData[0]->customer_email);
		
		$schDepart['booking_code']	= $fData[0]->depart_book_code;
		if($fData[0]->dtf_roundtrip == "return"): $schReturn['booking_code']	= $fData[0]->return_book_code; endif;
		
		if($fData[0]->depart_type=="connecting"): $schDepart['connecting'] = $schDepartC; endif;
		if($fData[0]->return_type=="connecting"): $schReturn['connecting'] = $schReturnC; endif;
		
		if($fData[0]->dtf_roundtrip == "oneway"):
			$flightInfo	= array('depart'=>$schDepart,'depart_price'=>$priceDepart);
		else:
			$flightInfo	= array('depart'=>$schDepart,'depart_price'=>$priceDepart,'return'	=> $schReturn,'return_price'=>$priceReturn);
		endif;
		
		foreach($psgr as $p):
			$pax[]	= array('pax_type' 			=> $p->pax_type,
                    		'title' 			=> $p->pax_title,
                    		'first_name'	 	=> $p->pax_first_name,
                    		'last_name' 		=> $p->pax_last_name,
                    		'dob' 				=> $p->pax_dob,
                    		'id_card' 			=> $p->pax_id_card,
                    		'depart_ticket_no' 	=> $p->pax_depart_ticket_no,
                    		'return_ticket_no' 	=> $p->pax_return_ticket_no);
		endforeach;
		
		$result	= array('rest_no'		=> "0",
						'book_id'		=> $ci->converter->encode($bookid),
						'book_date'		=> $fData[0]->dtf_bookdate,
						'time_limit'		=> $fData[0]->depart_expired_date,
						'issued_date'	=> $fData[0]->dtf_issued_date,
						'status'		=> $fData[0]->dtf_status,
						'customer'		=> $customer,
						'schedule'		=> $flightInfo,
						'pax'			=> $pax);
		
		return $result;	
	}
}

if ( ! function_exists('airport_city'))
{
	function airport_city($code="")
	{
		$ci = & get_instance();
		$city	= $ci->mgeneral->getValue("city",array('airport_code'=>$code),"mstr_airport");
		
		return ucfirst($city);
	}
}

if ( ! function_exists('airport_detail'))
{
	function airport_detail($code="")
	{
		$ci = & get_instance();
		$detail	= $ci->mgeneral->getWhere(array('airport_code'=>$code),"mstr_airport");
		
		$airport = array("country" => ucfirst($detail[0]->country), "city" => ucfirst($detail[0]->city), "airport_name" => ucfirst($detail[0]->airport_name));
		
		return $airport;
	}
}
?>