<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	private $partner_id = null;
    private $partner_data = null;
	
    function __construct() {
        parent::__construct();
		
		if ($this->session->userdata('login_data')) {
			$login_data = $this->session->userdata('login_data');
			
			if ($login_data['type'] == 'partner') {
				$this->partner_id 		= $login_data['id'];
				$this->partner_data 	= $login_data['detail'];
			}
		}
    }
	
	function check_logged($login = false) {
		if ($this->partner_id) {
			if ($login) {
				redirect('dashboard', 'refresh');
			}
		} else {
			if (!$login) {
				$this->session->sess_destroy();
				
				redirect('login', 'refresh');
			}
		}
	}
	
	function get_partner_id() {
		return $this->partner_id;
	}
	
	function get_partner_data() {
		return $this->partner_data;
	}
}

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */