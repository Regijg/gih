<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

#akses ke semua tabel di database

class mquery extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function getHotelbyLatLong($lat,$long){

    	$sql = "SELECT * FROM (
				    SELECT *, 
				        (
				            (
				                (
				                    acos(
				                        sin(( $lat * pi() / 180))
				                        *
				                        sin(( `latitude` * pi() / 180)) + cos(( $lat * pi() /180 ))
				                        *
				                        cos(( `latitude` * pi() / 180)) * cos((( $long - `longitude`) * pi()/180)))
				                ) * 180/pi()
				            ) * 60 * 1.1515 * 1.609344
				        )
				    as distance FROM hotel
				) hotel
				WHERE distance <= 20
				order by distance asc";
		$query = $this->db->query($sql)->result();
		
		return $query;
    }

    function getImgHotelByID($hotelId){
    	$sql = "select image from hotel_image where hotel_id = $hotelId ";
    	$query = $this->db->query($sql)->result();
		return $query;
    }
    
    function get_route($param) {
        $sql = "SELECT concat(airport_code,' - ',city) as code_city FROM mstr_airport where country = 'indonesia' AND (airport_code LIKE '%$param%' OR city LIKE '%$param%')";
        $q = $this->db->query($sql);

        return $q->result_array();
    }
	
	function get_skema_komisi($partner,$produk,$tipe) {
		
		if($tipe=="hotel"):
			$sqlT = "produk_id ='".$produk."'";
		elseif($tipe=="kereta"):
			$sqlT = "produk_id ='".$produk."'";
		else:
			$sqlT = "subproduk_id ='".$produk."'";
		endif;
		
        $sql = "SELECT skema_bisnis_detail_type,skema_bisnis_detail_amount FROM `mstr_partner` p LEFT JOIN skema_bisnis s ON p.skema_bisnis_id =  s.skema_bisnis_id LEFT JOIN  skema_bisnis_detail sd ON p.skema_bisnis_id = sd.skema_bisnis_id WHERE partner_id = '".$partner."' and ".$sqlT;
        $q = $this->db->query($sql)->result();

        foreach($q as $r)
		{
			$data['tipe']	= $r->skema_bisnis_detail_type;
			$data['nilai']	= $r->skema_bisnis_detail_amount;
		}
		return $data;
    }
	
	function add_deposit($amount,$partner)
	{
		$sql2 = "UPDATE mstr_partner SET current_deposit = current_deposit+".$amount." WHERE partner_id = '".$partner."'";
		$this->db->query($sql2);
	}
	
	function get_deposit_report($partner_id, $start, $end) {
		$sql = "SELECT * FROM dt_deposit_history WHERE partner_id = '".$this->db->escape_str($partner_id)."'";
		
		if (isset($start)) {
			$sql .= " AND payment_date >= '".$this->db->escape_str($start)."' AND payment_date <= '".$this->db->escape_str($end)."'";
		}
		$sql .= "AND payment_status = 'Ok' ORDER BY payment_date ASC";
		
		$query = $this->db->query($sql)->result();
		
		return $query;
	}
	
	function get_transaction_report($id,$start, $end) {        
		$query	= "SELECT dtf_issued_date as tgl_trx, CONCAT('Tiket Pesawat ',dtf_from,' - ',dtf_to,' Kode Booking ',depart_book_code, ' ',IFNULL(return_book_code, '')) as keterangan, dtf_tiket_price as harga, dtf_comission as komisi, dtf_margin as margin  FROM `dt_flight` WHERE `dtf_status` = 'ticketed' AND partner_id = '".$id."' AND dtf_issued_date >= '".$start." 00:00:00' AND dtf_issued_date <= '".$end." 00:00:00' 
UNION ALL
SELECT dtp_transaction_date as tgl_trx, CONCAT('Pulsa / PPOB Pelanggan: ',id_pelanggan,' ',nama_pelanggan,' Trx Id: ',refid) as keterangan, jumlah_bayar as harga, komisi_partner as komisi, margin as margin from dt_ppob WHERE STATUS = 'sukses' AND partner_id = '".$id."' AND dtp_transaction_date >= '".$start." 00:00:00' AND dtp_transaction_date <= '".$end." 00:00:00'
UNION ALL
SELECT dtt_issued_date as tgl_trx, CONCAT('Tiket Kereta ',dtt_from,' - ',dtt_to,' Kode Booking ',depart_book_code, ' ',IFNULL(return_book_code,'')) as keterangan, dtt_ticket_price as harga, dtt_commission as komisi, 0 as margin FROM dt_train WHERE dtt_status = 'ticketed' AND partner_id = '".$id."' AND dtt_issued_date >= '".$start." 00:00:00' AND dtt_issued_date <= '".$end." 00:00:00' 
ORDER BY tgl_trx asc";

        $q = $this->db->query($query);
        return $q->result();
    }
	
	function ppob_transaction_data($partner_id, $keyword, $start_date, $end_date, $start, $end)
	{
		if($start != NULL && $end != NULL):
			$limit = "limit ".$start.",".$end; 
		endif;

		$condition = " partner_id IN (".$partner_id.") and d.ppob_produk_id = m.ppob_produk_id AND d.status != 'inquiry' ";
		
		if($keyword != NULL){
			$condition .= " AND (`id_pelanggan` LIKE '%".$keyword."%' OR `nama_pelanggan` LIKE '%".$keyword."%')";
		}

		if($start_date != NULL){
			$condition .= " AND d.dtp_transaction_date >= '".$start_date." 00:00:00' ";
		}

		if($end_date != NULL){
			$condition .= " AND d.dtp_transaction_date <= '".$end_date." 23:59:59' ";
		}
		
		$query=$this->db->query("SELECT d.dtp_id, d.partner_id, d.dtp_transaction_date, m.ppob_produk_group, m.ppob_produk_code, m.ppob_produk_name, d.refid, d.id_pelanggan, d.nama_pelanggan, d.jumlah_tagihan,d.biaya_admin,d.jumlah_bayar, d.margin, d.komisi_partner, d.harga_partner, d.refnumber, d.status FROM dt_ppob d, mstr_ppob_produk m
		where ".$condition." order by d.dtp_transaction_date DESC ".$limit);

		return $query->result();
	}

	function ppob_transaction_detail($ppob_id)
	{
		$query=$this->db->query("SELECT d.dtp_id, d.partner_id, d.dtp_transaction_date, m.ppob_produk_group, m.ppob_produk_code, m.ppob_produk_name, d.refid, d.id_pelanggan, d.nama_pelanggan, d.jumlah_tagihan,d.biaya_admin,d.jumlah_bayar, d.margin, d.komisi_partner, d.harga_partner, d.refnumber, d.status FROM dt_ppob d, mstr_ppob_produk m
		where d.dtp_id = '".$ppob_id."' and d.ppob_produk_id = m.ppob_produk_id AND d.status != 'inquiry' order by d.dtp_transaction_date DESC ");

		return $query->result();
	}

	function get_member($condition, $start, $end)
	{
		if($start != NULL && $end != NULL):
			$limit = "limit ".$start.",".$end; 
		endif;
		
		$query=$this->db->query("SELECT *
								FROM mstr_partner
		where ".$condition.$limit);
		return $query->result();
	}

	function flight_transaction($partner_id, $keyword, $start_date, $end_date, $start, $end)
	{
		if($start != NULL && $end != NULL):
			$limit = "limit ".$start.",".$end; 
		endif;

		$condition = " partner_id IN (".$partner_id.") ";
		if($keyword != NULL){
			$condition .= " AND (`customer_name` LIKE '%".$keyword."%' OR `depart_book_code` LIKE '%".$keyword."%' OR `return_book_code` LIKE '%".$keyword."%')";
		}

		if($start_date != NULL){
			$condition .= " AND dtf_bookdate >= '".$start_date." 00:00:00' ";
		}

		if($end_date != NULL){
			$condition .= " AND dtf_bookdate <= '".$end_date." 23:59:59' ";
		}


		$query=$this->db->query("SELECT *
								FROM dt_flight
		where ".$condition." order by dtf_bookdate DESC ".$limit);
		
		return $query->result();
	}

	function train_transaction($partner_id, $keyword, $start_date, $end_date, $start, $end)
	{
		if($start != NULL && $end != NULL):
			$limit = "limit ".$start.",".$end; 
		endif;

		$condition = " partner_id IN (".$partner_id.") ";
		if($keyword != NULL){
			$condition .= " AND (`customer_name` LIKE '%".$keyword."%' OR `depart_book_code` LIKE '%".$keyword."%' OR `return_book_code` LIKE '%".$keyword."%')";
		}

		if($start_date != NULL){
			$condition .= " AND dtt_bookdate >= '".$start_date." 00:00:00' ";
		}

		if($end_date != NULL){
			$condition .= " AND dtt_bookdate <= '".$end_date." 23:59:59' ";
		}


		$query=$this->db->query("SELECT *
								FROM dt_train
		where ".$condition." order by dtt_bookdate DESC ".$limit);

		return $query->result();
	}
}
