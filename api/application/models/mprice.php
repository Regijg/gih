<?php

class mprice extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function getServiceFee($airline,$psgr)
    {
        $this->db->where('airlines_id',$airline);
		$this->db->where('passenger_types',$psgr);
        $q = $this->db->get('service_fees')->result();
		
		foreach($q as $r)
		{
			$value	= $r->fee_values;
		}
		
			if(count($q)=="0")
			{
				return 0;
			}
			else
			{
				return $value;
			}
    }
	
	function getSetting($kelompok,$name)
	{
		$this->db->where('general_setting_kelompok',$kelompok);
		$this->db->where('general_setting_name',$name);
        $q = $this->db->get('general_setting')->result();
		
		foreach($q as $r)
		{
			$value	= $r->general_setting_nilai;
		}
		
		return $value;
	}
	
	 function getClass($airline,$class)
    {
        $this->db->where('airlines_id',$airline);
		$this->db->where('airlines_class_code',$class);
        $q = $this->db->get('mstr_class')->result();
		
		foreach($q as $r)
		{
			$value	= $r->airlines_class_name;
		}
		
			if(count($q)=="0")
			{
				return "-";
			}
			else
			{
				return $value;
			}
    }
	//$this->db2	= $this->load->database('corporate', TRUE);
}

?>
