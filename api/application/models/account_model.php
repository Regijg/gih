<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Account Model
 *
 * @author: Fajar | http://newbierise.com
 * 
 */
class account_model extends CI_Model{
	function __construct() {
        parent::__construct();
    }
	
	function get_partner_data($partner_id) {
		$query = $this->db->query("SELECT * FROM mstr_partner WHERE partner_id = '".$this->db->escape_str($partner_id)."'")->result();
		
		return $query;
	}
}

/* End of file account_model.php */
/* Location: ./application/model/account_model.php */