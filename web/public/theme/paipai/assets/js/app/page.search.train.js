var filter = {
        "depart": {"class":[]},
        "return": {"class":[]}
    };
    
var order = {"depart":[],"return":[]}

$(function() {
	initializeSearch();

    $('.toggle-train-list').click(function(e) {
        e.preventDefault();
                    
        switchList(this);
    });

    $('.train-filter h5 a').click(function(e){
        e.preventDefault();
        var schedule = $(this).attr('data-schedule');

        $('#train-'+schedule+' .train-filter h5 a').removeClass('active');
        $(this).addClass('active');
        $('#train-'+schedule+' .train-filter-item li').hide();
        $('#train-'+schedule+' .train-filter-item li#' + $(this).attr('rel')).slideDown();

        schedule = null; delete schedule;
    });

    $('.filter-class').click(function(e){
        e.preventDefault();

        if ($(this).hasClass('active'))
        {
            $(this).removeClass('active');
        }
        else
        {
            $(this).addClass('active');
        }

        filterResult($(this).attr('data-schedule'));
    });

    $('.train-sort li a').click(function(e){
        e.preventDefault();

        var schedule = $(this).attr('data-schedule'),
            direction = $(this).attr('data-direction');


        $('#train-'+schedule+' .train-sort li a').removeClass('active');
        $('#train-'+schedule+' .train-sort li a').attr('data-direction', 'asc');
        $('#train-'+schedule+' .train-sort li a i').attr('class', 'fa fa-sort');

        $(this).addClass('active');

        if (direction == 'asc')
        {
            $(this).attr('data-direction', 'desc');
            $(this).find('i').attr('class', 'fa fa-sort-up');
        }
        else
        {
            $(this).attr('data-direction', 'asc');
            $(this).find('i').attr('class', 'fa fa-sort-down');
        }

        sortResult(schedule);

        schedule = null; delete schedule;
        direction = null; delete direction;
    });
});

function initializeSearch()
{
    var ajax = $.ajax({
        url: $('#search-data').attr('search-url'),
        beforeSend: function() {
            openLoader({
                wrapper: '.result-head',
                type: 'inline',
            });

            $('.result-loader > div').css('width', '10%');
        },
        success: function(res)
        {
            if (res.code == 200)
            {
                $('#train-depart .train-list').append(res.data.depart.content);

                filterResult('depart');
                sortResult('depart');

                if ($('#train-depart .train-list li').length > 0)
                {
                    $('#train-depart .search-loader').fadeOut(function(){
                        $(this).remove();
                    });

                    if ($('#train-depart').hasClass('active') && ( ! $('#train-depart').hasClass('taken')))
                    {
                        $('#train-depart .result-filter').show();
                    }
                }

                if (isset(res.data.return))
                {
                    $('#train-return .train-list').append(res.data.return.content);

                    filterResult('return');
                    sortResult('return');

                    if ($('#train-return .train-list li').length > 0)
                    {
                        $('#train-return .search-loader').fadeOut(function(){
                            $(this).remove();
                        });

                        if ($('#train-return').hasClass('active') && ( ! $('#train-return').hasClass('taken')))
                        {
                            $('#train-return .result-filter').show();
                        }
                    }
                }

                $('.show-train-detail').unbind('click');
                $('.show-train-detail').click(function(e) {
                    e.preventDefault();
                    
                    showDetail($(this).attr('data-id'));
                });

                $('.set-train').unbind('click');
                $('.set-train').click(function(e) {
                    e.preventDefault();
                    
                    setTrain(this);
                });
            }
            else
            {
                openNotification({wrapper:'.pop',style:'error',title:'Error',popup:true,message:res.message});
            }

            $('.result-loader > div').css('width', '100%');
            setTimeout(function(){
                $('.result-loader > div').css('height', '0');
                closeLoader({
                    wrapper: '.result-head'
                });
            }, 300);
                
            $('#train-depart .search-loader').fadeOut(function(){
                $(this).remove();
            });

            if ($('#train-depart .train-item').length == 0)
            {
                $('#train-depart .result-items').prepend('<div class="search-empty">Tidak ada hasil dari pencarian.</div>');
            }

            if ($('#train-return').length)
            {
                $('#train-return .search-loader').fadeOut(function(){
                    $(this).remove();
                });

                if ($('#train-return .train-item').length == 0)
                {
                    $('#train-return .result-items').prepend('<div class="search-empty">Tidak ada hasil dari pencarian.</div>');
                }
            }
        },
        error: function(x, t, m)
        {
            openNotification({wrapper:'.pop',style:'error',title:'Error',popup:true,message:m});

            $('.result-loader > div').css('width', '100%');
            setTimeout(function(){
                $('.result-loader > div').css('height', '0');
                closeLoader({
                    wrapper: '.result-head'
                });
            }, 300);

            $('#train-depart .search-loader').fadeOut(function(){
                $(this).remove();
            });

            if ($('#train-depart .train-item').length == 0)
            {
                $('#train-depart .result-items').prepend('<div class="search-empty">Tidak ada hasil dari pencarian.</div>');
            }

            if ($('#train-return').length)
            {
                $('#train-return .search-loader').fadeOut(function(){
                    $(this).remove();
                });

                if ($('#train-return .train-item').length == 0)
                {
                    $('#train-return .result-items').prepend('<div class="search-empty">Tidak ada hasil dari pencarian.</div>');
                }
            }
        }
    });

    ajax = null; delete ajax;
}

function setTrain(elem)
{
    var schedule = $(elem).attr('data-schedule');
    $('#train-'+schedule+' .train-list > li').hide();
    $('#' + $(elem).attr('data-id')).show();
    order[schedule] = {
        reference: $(elem).attr('data-ref'),
        schedule: $(elem).attr('data-schedule-id'),
        code: $(elem).attr('data-code'),
        passenger: {
            adult: parseInt($('#search-data').attr('search-adult')),
            infant: parseInt($('#search-data').attr('search-infant')),
        }
    }

    $('#train-'+schedule).addClass('taken');
    $('#train-'+schedule+' .result-filter').hide();
    $('#train-'+schedule+' .result-items .train-item > div.detail').hide();
    $('#train-'+schedule+' .result-items').prepend('<div class="taken-schedule"><div class="overlay"></div><div class="retake"><button type="button" class="link-button" onclick="unsetTrain(\''+schedule+'\');"><i class="fa fa-refresh"></i> GANTI PILIHAN</button></div></div>');

    if (($('.result-wrapper.taken').length) >= parseInt($('#search-data').attr('search-item')))
    {
        $('.result-body').after('<div class="result-checkout text-center"><hr class="mtop-20 mbottom-20"><button type="button" class="link-button large block" onclick="checkoutTrain();"><i class="fa fa-check-square-o"></i> CHECKOUT</button></div>');
    }
}

function unsetTrain(schedule)
{
    order[schedule] = [];
    filterResult(schedule);
    sortResult(schedule);
    $('.result-body').parent().find('.result-checkout').remove();
    $('#train-'+schedule).removeClass('taken');
    $('#train-'+schedule+' .result-filter').show();
    $('#train-'+schedule+' .result-items .taken-schedule').remove();
}

function switchList(elem)
{
    $('.result-items > .overlay').remove();
    $('.toggle-train-list').removeClass('active');
    $('.result-wrapper').removeClass('active');
    $('.result-wrapper').addClass('inactive');
    $('.result-wrapper .result-filter').hide();

    $(elem).addClass('active');
    $(elem).parent().addClass('active');
    $(elem).parent().removeClass('inactive');

    if ( ! $(elem).parent().hasClass('taken'))
    {
        if ($(elem).parent().find('.train-list > li').length)
        {
            $(elem).parent().find('.result-filter').show();
        }
    }

    $('.result-wrapper.inactive .result-items').prepend('<div class="overlay"></div>');
}

function checkoutTrain()
{
    var ajax = $.ajax({
        url: $('#common-data').attr('app-url') + 'product/train/checkout',
        type: 'post',
        dataType: 'json',
        data: {data:order},
        beforeSend: function() {
            openLoader();
        },
        success: function(res)
        {
            if (res.code == 200)
            {
                window.location = res.redirect;
            }
            else
            {
                closeLoader();
                openNotification({wrapper:'.pop',style:'error',title:'Error',popup:true,message:res.message});
            }
        },
        error: function(x, t, m)
        {
            closeLoader();
            openNotification({wrapper:'.pop',style:'error',title:'Error',popup:true,message:m});
        }
    });

    ajax = null; delete ajax;
}

function showDetail(id)
{
    if ($('#' + id + ' > .detail').is(':visible'))
    {
        $('#' + id + ' > .detail').slideUp();
    }
    else
    {
        $('#' + id + ' > .detail').slideDown();
    }
}

function filterResult(schedule)
{
    filter[schedule].class = [];
    if ($('#filter-class-' + schedule + ' a.active').length > 0)
    {
        $('#filter-class-' + schedule + ' a').each(function(){
            if ($(this).hasClass('active'))
            {
                filter[schedule].class.push($(this).attr('rel'));
            }
        });
    }

    if (filter[schedule].class.length > 0)
    {
        $('#train-'+schedule+' .train-list > li').hide();

        $('#train-'+schedule+' .train-list > li').each(function(){
            if (($.inArray($(this).attr('data-class'), filter[schedule].class) != -1))
            {
                $(this).show();
            }
        });
    }
    else
    {
        $('#train-'+schedule+' .train-list > li').show();
    }
}

function sortResult(schedule) {
    $('#train-'+schedule+' .train-list li.train-item').sort(sortir).appendTo('#train-'+schedule+' .train-list');

    function sortir(a, b){
        if ($('#train-'+schedule+' .train-sort li a.active').attr('data-direction') == 'desc')
        {
            return ($(b).data($('#train-'+schedule+' .train-sort li a.active').attr('rel'))) < ($(a).data($('#train-'+schedule+' .train-sort li a.active').attr('rel'))) ? 1 : -1;
        }
        else
        {
            return ($(b).data($('#train-'+schedule+' .train-sort li a.active').attr('rel'))) < ($(a).data($('#train-'+schedule+' .train-sort li a.active').attr('rel'))) ? -1 : 1;
        }
    }
}