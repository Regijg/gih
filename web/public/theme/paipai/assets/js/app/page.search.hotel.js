var order = [];

$(function() {
	initializeSearch();

    $('input[name="filter_name"]').bind('keyup', function() {
        filterResult();
    });

    $('.hotel-sort li a').click(function(e){
        e.preventDefault();

        var direction = $(this).attr('data-direction');


        $('#hotel-result .hotel-sort li a').removeClass('active');
        $('#hotel-result .hotel-sort li a').attr('data-direction', 'asc');
        $('#hotel-result .hotel-sort li a i').attr('class', 'fa fa-sort');

        $(this).addClass('active');

        if (direction == 'asc')
        {
            $(this).attr('data-direction', 'desc');
            $(this).find('i').attr('class', 'fa fa-sort-up');
        }
        else
        {
            $(this).attr('data-direction', 'asc');
            $(this).find('i').attr('class', 'fa fa-sort-down');
        }

        sortResult();

        direction = null; delete direction;
    });
});

function initializeSearch()
{
    var ajax = $.ajax({
        url: $('#search-data').attr('search-url'),
        beforeSend: function() {
            openLoader({
                wrapper: '.result-head',
                type: 'inline',
            });

            $('.result-loader > div').css('width', '10%');
        },
        success: function(res)
        {
            $('#hotel-result .search-loader').fadeOut(function(){
                $(this).remove();
            });

            if (res.code == 200)
            {
                closeLoader({wrapper:'.result-head'});

                $('.result-loader > div').css('width', '100%');

                $('#hotel-result .hotel-list').append(res.data.content);
                
                $('#search-data').attr('search-reference', res.data.ref);
                $('#search-data').attr('search-page', res.data.page);
                $('#search-data').attr('search-loaded', 1);
                $('#hotel-result .result-filter').show();

                filterResult();
                sortResult();

                if (res.data.page > 1)
                {
                    $('.result-loader').html('<ul class="clearfix"></ul>');
                    for (i = 1; i <= res.data.page; i++)
                    {
                        $('.result-loader ul').append('<li id="search-page-'+i+'" style="width:'+((1/res.data.page) * 100)+'%"><div></div><i class="fa fa-circle"></i></li>');
                    }

                    $('.result-loader').addClass('mbottom-10');
                    $('.result-loader #search-page-1 i').addClass('completed');
                    $('.result-loader #search-page-1 div').css('width', '100%');

                    if ($('#search-data').attr('search-reference') != '')
                    {
                        $('#hotel-result .hotel-list').after('<div class="load-more text-center mtop-20"><button type="button" class="link-button outline" onclick="getHotel(2);">Muat Lebih Banyak</button></div>');
                    }
                }
                else
                {
                    setTimeout(function(){
                        $('.result-loader > div').css('height', '0');
                        closeLoader({
                            wrapper: '.result-head'
                        });
                    }, 300);
                }
            }
            else
            {
                openNotification({wrapper:'.pop',style:'error',title:'Error',popup:true,message:res.message});
                $('#hotel-result .result-items').prepend('<div class="search-empty">Tidak ada hasil dari pencarian.</div>');

                setTimeout(function(){
                    $('.result-loader > div').css('height', '0');
                    closeLoader({
                        wrapper: '.result-head'
                    });
                }, 300);
            }
        },
        error: function(x, t, m)
        {
            openNotification({wrapper:'.pop',style:'error',title:'Error',popup:true,message:m});
            $('#hotel-result .result-items').prepend('<div class="search-empty">Tidak ada hasil dari pencarian.</div>');

            setTimeout(function(){
                $('.result-loader > div').css('height', '0');
                closeLoader({
                    wrapper: '.result-head'
                });
            }, 300);
        }
    });

    ajax = null; delete ajax;
}

function getHotel(page)
{
    page = typeof page !== 'undefined' ? parseInt(page) : 1;
    var ref = $('#search-data').attr('search-reference');

    if (ref != '')
    {
        var ajax = $.ajax({
            url: $('#common-data').attr('app-url') + 'product/hotel/load?reference_id=' + ref + '&page=' + page,
            beforeSend: function() {
                $('#hotel-result .load-more button').fadeOut(function() {
                    openLoader({
                        wrapper: '#hotel-result .load-more',
                        type: 'inline',
                    });
                });

                $('.result-loader #search-page-'+page+' div').css('width', '10%');
            },
            success: function(res)
            {
                closeLoader({wrapper:'#hotel-result .load-more'});

                if (res.code == 200)
                {
                    $('.result-loader #search-page-'+page+' i').addClass('completed');
                    $('.result-loader #search-page-'+page+' div').css('width', '100%');

                    $('#hotel-result .hotel-list').append(res.data.content);
                    $('#search-data').attr('search-loaded', page);

                    filterResult();
                    sortResult();

                    if (res.data.page > parseInt($('#search-data').attr('search-loaded')))
                    {
                        $('#hotel-result .load-more button').attr('onclick', 'getHotel('+(page + 1)+')');
                        $('#hotel-result .load-more button').fadeIn();
                    }
                }
                else
                {
                    $('#hotel-result .load-more').remove();
                }
            },
            error: function(x, t, m)
            {
                closeLoader({wrapper:'#hotel-result .load-more'});
            }
        });

        ajax = null; delete ajax;
    }
}

function showDetailHotel(elem)
{
    $('body').addClass('hide-overflow');

    if ($('.pop-detail').length == 0)
    {
        $('#search-data').after('<div class="pop-detail">' +
                '<div class="pop-detail-wrapper">' +
                    '<div class="pop-close">' +
                        '<button type="button" class="link-button block" onclick="closeDetailHotel();"><i class="fa fa-close"></i></button>' +
                    '</div>' +
                    '<div class="pop-content"></div>' +
                '</div>' +
            '</div>');
    }
    else
    {
        $('.pop-detail').fadeIn();
    }

    getHotelDetail($(elem).attr('data-ref'), $(elem).attr('data-hotel-id'));
}

function closeDetailHotel()
{
    $('.pop-detail').fadeOut('fast', function(){
        $('.pop-content .detail-item').hide();
        $('body').removeClass('hide-overflow');
    });
}

function getHotelDetail(ref, id)
{
    ref = typeof ref !== 'undefined' ? ref : '';
    id = typeof id !== 'undefined' ? id : '';

    if ((ref != '') & (id != ''))
    {
        closeNotification({wrapper:'.pop-detail-wrapper .pop-content'});

        if ($('#hotel-detail-' + id).length)
        {
            $('#hotel-detail-' + id).fadeIn();
        }
        else
        {
            var ajax = $.ajax({
                url: $('#common-data').attr('app-url') + 'product/hotel/load/detail?reference_id=' + ref + '&hotel_id=' + id,
                beforeSend: function() {
                    openLoader({wrapper:'.pop-detail-wrapper'});
                },
                success: function(res)
                {
                    closeLoader({wrapper:'.pop-detail-wrapper'});

                    if (res.code == 200)
                    {
                        $('.pop-detail .pop-content').append(res.data.content);
                    }
                    else
                    {
                        openNotification({wrapper:'.pop-detail-wrapper .pop-content',style:'error',title:'Error',message:res.message});
                    }
                },
                error: function(x, t, m)
                {
                    closeLoader({wrapper:'.pop-detail-wrapper'});
                    openNotification({wrapper:'.pop-detail-wrapper .pop-content',style:'error',title:'Error',message:m});
                }
            });

            ajax = null; delete ajax;
        }

        $('.pop-detail-wrapper').animate({scrollTop: 0}, 500);
    }
    else
    {
        openNotification({wrapper:'.pop-detail-wrapper .pop-content',style:'error',title:'Error',message:lang.error_parameter});
    }
}

function setRoom(elem)
{
    var hotel_id =  $(elem).attr('data-hotel-id'),
        room_id =  $(elem).attr('data-room-id');

    $('#hotel-detail-'+hotel_id+' .room-list > li').hide();
    $('#room-' + room_id).show();

    order = {
        reference: $(elem).attr('data-ref'),
        code: $(elem).attr('data-code'),
        room: parseInt($('#search-data').attr('search-room'))
    }

    $('#hotel-detail-'+hotel_id+' .room-list').prepend('<li class="taken-item"><div class="overlay"></div><div class="retake"><button type="button" class="link-button gray" onclick="unsetRoom();"><i class="fa fa-refresh"></i> GANTI</button>&nbsp;&nbsp;<button type="button" class="link-button" onclick="checkoutHotel();"><i class="fa fa-check-square-o"></i> CHECKOUT</button></div></li>');
}

function unsetRoom()
{
    order = [];

    $('.hotel-detail-wrapper .taken-item').remove();
    $('.hotel-detail-wrapper .room-list > li').show();
}

function checkoutHotel()
{
    var ajax = $.ajax({
        url: $('#common-data').attr('app-url') + 'product/hotel/checkout',
        type: 'post',
        dataType: 'json',
        data: {data:order},
        beforeSend: function() {
            openLoader();
        },
        success: function(res)
        {
            if (res.code == 200)
            {
                window.location = res.redirect;
            }
            else
            {
                closeLoader();
                openNotification({wrapper:'.pop',style:'error',title:'Error',popup:true,message:res.message});
            }
        },
        error: function(x, t, m)
        {
            closeLoader();
            openNotification({wrapper:'.pop',style:'error',title:'Error',popup:true,message:m});
        }
    });

    ajax = null; delete ajax;
}

function filterResult()
{
    filter = $('input[name="filter_name"]').val();

    if (filter != '')
    {
        $('#hotel-result .hotel-list > li').hide();

        $('#hotel-result .hotel-list > li').each(function(){
            var str = $(this).find('.hotel-name').text();

            if (str.toLowerCase().indexOf(filter.toLowerCase()) >= 0)
            {
                $(this).show();
            }
        });
    }
    else
    {
        $('#hotel-result .hotel-list > li').show();
    }
}

function sortResult() {
    $('#hotel-result .hotel-list li.hotel-item').sort(sortir).appendTo('#hotel-result .hotel-list');

    function sortir(a, b){
        if ($('#hotel-result .hotel-sort li a.active').attr('data-direction') == 'desc')
        {
            return ($(b).data($('#hotel-result .hotel-sort li a.active').attr('rel'))) < ($(a).data($('#hotel-result .hotel-sort li a.active').attr('rel'))) ? 1 : -1;
        }
        else
        {
            return ($(b).data($('#hotel-result .hotel-sort li a.active').attr('rel'))) < ($(a).data($('#hotel-result .hotel-sort li a.active').attr('rel'))) ? -1 : 1;
        }
    }
}

function resizeMap(id)
{
    if ($('#hotel-map-'+id).length)
    {
        $('#hotel-map-'+id).remove();
    }

    if ($('#hotel-detail-'+id+' .site-metadata').length)
    {
        $('#hotel-detail-'+id+' .site-metadata').after('<div id="hotel-map-'+id+'" class="maps-wrapper mtop-10"></div>');
        initMap(parseFloat($('#hotel-detail-'+id+' .site-metadata').attr('data-latitude')), parseFloat($('#hotel-detail-'+id+' .site-metadata').attr('data-longitude')), 'hotel-map-'+id);
    }
}

function openMap(elem)
{
    $('body').addClass('hide-overflow');
    $('#search-data').after('<div class="pop-maps">' +
            '<div class="pop-maps-wrapper">' +
                '<div class="pop-close text-center">' +
                    '<button type="button" class="link-button block" onclick="closeMap();"><i class="fa fa-close"></i> Tutup</button>' +
                '</div>' +
                '<div id="map-canvas"></div>' +
            '</div>' +
        '</div>');

    initMap(parseFloat($(elem).attr('data-latitude')), parseFloat($(elem).attr('data-longitude')));
}

function closeMap()
{
    $('.pop-maps').fadeOut('fast', function(){
        $(this).remove();
        $('body').removeClass('hide-overflow');
    });
}

function initMap(lat, lng, wrapper)
{
    wrapper = typeof wrapper !== 'undefined' ? wrapper : 'map-canvas';
    var position = new google.maps.LatLng(lat, lng);

    var mapOptions = {
        zoom: 14,
        center: position,
        disableDefaultUI: true,
        styles: [{"featureType":"all","elementType":"geometry","stylers":[{"visibility":"simplified"}]}]
    };

    var mapElement = document.getElementById(wrapper);
    var map = new google.maps.Map(mapElement, mapOptions);
    var marker = new google.maps.Marker({
        position: position,
        map: map
    });

    marker.setMap(map);
    map.setCenter(marker.position);

    mapOptions = null; delete mapOptions;
    mapElement = null; delete mapElement;
    marker = null; delete marker;
    map = null; delete map;
    position = null; delete position;
}