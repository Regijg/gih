$(function() {
    $('#countdown').countdown($('#countdown').attr('rel'))
		.on('update.countdown', function(event) {
			var format = '%H:%M:%S';
			if(event.offset.totalDays > 0) {
				format = '%-d Hari ' + format;
			}
			if(event.offset.weeks > 0) {
				format = '%-w Minggu ' + format;
			}
			$(this).html(event.strftime(format));
		})
		.on('finish.countdown', function(event) {
			$(this).html('Batas Waktu Pembayaran Habis');
			var ajax = $.ajax({
		        url: $('#common-data').attr('app-url') + 'product/train/remove/book'
		    });

		    ajax = null; delete ajax;
	});

	$('.payment-method ul li a').click(function() {
    	$('.payment-method .payment-tabs').hide();
    	$('.payment-method #' + $(this).attr('rel')).toggle();
    	$('.payment-method ul li a').addClass('gray');
    	$('.payment-method ul li a').addClass('outline');
    	$(this).removeClass('gray');
    	$(this).removeClass('outline');
    });
});
