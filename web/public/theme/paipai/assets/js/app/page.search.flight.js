var airlines = 0,
    loaded_airlines = 0;

var filter = {
        "depart": {"type":[], "airline":[]},
        "return": {"type":[], "airline":[]}
    };
    
var order = {"depart":[],"return":[]}

$(function() {
	initializeSearch();

    $('.toggle-flight-list').click(function(e) {
        e.preventDefault();
                    
        switchList(this);
    });

    $('.flight-filter h5 a').click(function(e){
        e.preventDefault();
        var schedule = $(this).attr('data-schedule');

        $('#flight-'+schedule+' .flight-filter h5 a').removeClass('active');
        $(this).addClass('active');
        $('#flight-'+schedule+' .flight-filter-item li').hide();
        $('#flight-'+schedule+' .flight-filter-item li#' + $(this).attr('rel')).slideDown();

        schedule = null; delete schedule;
    });

    $('.filter-type').click(function(e){
        e.preventDefault();

        if ($(this).hasClass('active'))
        {
            $(this).removeClass('active');
        }
        else
        {
            $(this).addClass('active');
        }

        filterResult($(this).attr('data-schedule'));
    });

    $('.filter-airline').click(function(e){
        e.preventDefault();

        if ($(this).hasClass('active'))
        {
            $(this).removeClass('active');
        }
        else
        {
            $(this).addClass('active');
        }

        filterResult($(this).attr('data-schedule'));
    });

    $('.flight-sort li a').click(function(e){
        e.preventDefault();

        var schedule = $(this).attr('data-schedule'),
            direction = $(this).attr('data-direction');


        $('#flight-'+schedule+' .flight-sort li a').removeClass('active');
        $('#flight-'+schedule+' .flight-sort li a').attr('data-direction', 'asc');
        $('#flight-'+schedule+' .flight-sort li a i').attr('class', 'fa fa-sort');

        $(this).addClass('active');

        if (direction == 'asc')
        {
            $(this).attr('data-direction', 'desc');
            $(this).find('i').attr('class', 'fa fa-sort-up');
        }
        else
        {
            $(this).attr('data-direction', 'asc');
            $(this).find('i').attr('class', 'fa fa-sort-down');
        }

        sortResult(schedule);

        schedule = null; delete schedule;
        direction = null; delete direction;
    });
});

function initializeSearch()
{
    var ajax = $.ajax({
        url: $('#search-data').attr('search-url'),
        beforeSend: function() {
            openLoader({
                wrapper: '.result-head',
                type: 'inline',
            });
        },
        success: function(res)
        {
            if (res.code == 200)
            {
                $.each(res.data.res, function(key1, schedule) {
                    airlines = airlines + schedule.length;

                    $.each(schedule, function(key2, id) {
                        getFlight(id, key1);
                    });
                });
            }
            else
            {
                closeLoader({wrapper:'.result-head'});
                openNotification({wrapper:'.pop',style:'error',title:'Error',popup:true,message:res.message});
            }
        },
        error: function(x, t, m)
        {
            closeLoader({wrapper:'.result-head'});
            openNotification({wrapper:'.pop',style:'error',title:'Error',popup:true,message:m});
        }
    });

    ajax = null; delete ajax;
}

function getFlight(id, schedule)
{
	var ajax = $.ajax({
        url: $('#common-data').attr('app-url') + 'product/flight/load?identification_id=' + id + '&schedule=' + schedule,
        success : function(res)
        {
            if (res.code == 100)
            {
            	getFlight(id, schedule);
            }
            else if (res.code == 200)
            {
                $('#flight-'+schedule+' .flight-list').append(res.data.content);
                if (order[schedule].length == 0)
                {
                    filterResult(schedule);
                    sortResult(schedule);
                }

                if ($('#flight-'+schedule+' .flight-list li').length > 0)
                {
                    $('#flight-'+schedule+' .search-loader').fadeOut(function(){
                        $(this).remove();
                    });

                    if ($('#flight-'+schedule).hasClass('active') && ( ! $('#flight-'+schedule).hasClass('taken')))
                    {
                        $('#flight-'+schedule+' .result-filter').show();
                    }
                }

            	loaded_airlines = loaded_airlines + 1;

                $('.show-flight-detail').unbind('click');
                $('.show-flight-detail').click(function(e) {
                    e.preventDefault();
                    
                    showDetail($(this).attr('data-id'));
                });

                $('.set-flight').unbind('click');
                $('.set-flight').click(function(e) {
                    e.preventDefault();
                    
                    setFlight(this);
                });
            }
            else
            {
            	loaded_airlines = loaded_airlines + 1;
            }
            
            $('.result-loader > div').css('width', ((loaded_airlines/airlines) * 100) + '%');
            	
        	if (loaded_airlines == airlines)
        	{
            	setTimeout(function(){
					$('.result-loader > div').css('height', '0');
                    closeLoader({
                        wrapper: '.result-head'
                    });
				}, 300);

                $('#flight-'+schedule+' .search-loader').fadeOut(function(){
                    $(this).remove();
                });

                if ($('#flight-'+schedule+' .flight-item').length == 0)
                {
                    $('#flight-'+schedule+' .result-items').prepend('<div class="search-empty">Tidak ada hasil dari pencarian.</div>');
                }
            }
        },
        error: function(x, t, m)
        {
            if (order[schedule].length == 0)
            {
                filterResult(schedule);
                sortResult(schedule);
            }

            loaded_airlines = loaded_airlines + 1;
            $('.result-loader > div').css('width', ((loaded_airlines/airlines) * 100) + '%');
            if (loaded_airlines == airlines)
            {
                setTimeout(function(){
                    $('.result-loader > div').css('height', '0');
                    closeLoader({
                        wrapper: '.result-head'
                    });
                }, 300);

                $('#flight-'+schedule+' .search-loader').fadeOut(function(){
                    $(this).remove();
                });

                if ($('#flight-'+schedule+' .flight-item').length == 0)
                {
                    $('#flight-'+schedule+' .result-items').prepend('<div class="search-empty">Tidak ada hasil dari pencarian.</div>');
                }
            }
        }
    });

    ajax = null; delete ajax;
}

function setFlight(elem)
{
    console.info(elem);
    var schedule = $(elem).attr('data-schedule');
    $('#flight-'+schedule+' .flight-list > li').hide();
    $('#' + $(elem).attr('data-id')).show();
    order[schedule] = {
        // reference: $(elem).data('sc-ref-id'),
        fno: $(elem).data('fno'),
        from: $(elem).data('from'),
        to: $(elem).data('to'),
        date: $(elem).data('date'),
        // class: $(elem).data('class-id'),
        passenger: {
            adult: parseInt($('#search-data').attr('search-adult')),
            child: parseInt($('#search-data').attr('search-child')),
            infant: parseInt($('#search-data').attr('search-infant')),
        }
    }

    $('#flight-'+schedule).addClass('taken');
    $('#flight-'+schedule+' .result-filter').hide();
    $('#flight-'+schedule+' .result-items .flight-item > div.detail').hide();
    $('#flight-'+schedule+' .result-items').prepend('<div class="taken-schedule"><div class="overlay"></div><div class="retake"><button type="button" class="link-button" onclick="unsetFlight(\''+schedule+'\');"><i class="fa fa-refresh"></i> GANTI PILIHAN</button></div></div>');

    if (($('.result-wrapper.taken').length) >= parseInt($('#search-data').attr('search-item')))
    {
        $('.result-body').after('<div class="result-checkout text-center"><hr class="mtop-20 mbottom-20"><button type="button" class="link-button large block" onclick="checkoutFlight();"><i class="fa fa-check-square-o"></i> CHECKOUT</button></div>');
    }
}

function unsetFlight(schedule)
{
    order[schedule] = [];
    filterResult(schedule);
    sortResult(schedule);
    $('.result-body').parent().find('.result-checkout').remove();
    $('#flight-'+schedule).removeClass('taken');
    $('#flight-'+schedule+' .result-filter').show();
    $('#flight-'+schedule+' .result-items .taken-schedule').remove();
}

function switchList(elem)
{
    $('.result-items > .overlay').remove();
    $('.toggle-flight-list').removeClass('active');
    $('.result-wrapper').removeClass('active');
    $('.result-wrapper').addClass('inactive');
    $('.result-wrapper .result-filter').hide();

    $(elem).addClass('active');
    $(elem).parent().addClass('active');
    $(elem).parent().removeClass('inactive');

    if ( ! $(elem).parent().hasClass('taken'))
    {
        if ($(elem).parent().find('.flight-list > li').length)
        {
            $(elem).parent().find('.result-filter').show();
        }
    }

    $('.result-wrapper.inactive .result-items').prepend('<div class="overlay"></div>');
}

function checkoutFlight()
{
    var ajax = $.ajax({
        url: $('#common-data').attr('app-url') + 'product/flight/checkout',
        type: 'post',
        dataType: 'json',
        data: {data:order},
        beforeSend: function() {
            openLoader();
        },
        success: function(res)
        {
            console.log(res)
            if (res.code == 200)
            {
                window.location = res.redirect;
            }
            else
            {
                closeLoader();
                openNotification({wrapper:'.pop',style:'error',title:'Error',popup:true,message:res.message});
            }
        },
        error: function(x, t, m)
        {
            closeLoader();
            openNotification({wrapper:'.pop',style:'error',title:'Error',popup:true,message:m});
        }
    });

    ajax = null; delete ajax;
}

function showDetail(id)
{
    if ($('#' + id + ' > .detail').is(':visible'))
    {
        $('#' + id + ' > .detail').slideUp();
    }
    else
    {
        $('#' + id + ' > .detail').slideDown();
    }
}

function filterResult(schedule)
{
    filter[schedule].type = [];
    if ($('#filter-type-' + schedule + ' a.active').length > 0)
    {
        $('#filter-type-' + schedule + ' a').each(function(){
            if ($(this).hasClass('active'))
            {
                filter[schedule].type.push($(this).attr('rel'));
            }
        });
    }

    filter[schedule].airline = [];
    if ($('#filter-airline-' + schedule + ' a.active').length > 0)
    {
        $('#filter-airline-' + schedule + ' a').each(function(){
            if ($(this).hasClass('active'))
            {
                filter[schedule].airline.push($(this).attr('rel'));
            }
        });
    }

    if (filter[schedule].type.length > 0 || filter[schedule].airline.length > 0)
    {
        $('#flight-'+schedule+' .flight-list > li').hide();

        $('#flight-'+schedule+' .flight-list > li').each(function(){
            if (filter[schedule].type.length > 0 && filter[schedule].airline.length > 0)
            {
                if (($.inArray($(this).attr('data-type'), filter[schedule].type) != -1) && ($.inArray($(this).attr('data-airline'), filter[schedule].airline) != -1))
                {
                    $(this).show();
                }
            }
            else
            {
                if (filter[schedule].type.length > 0)
                {
                    if (($.inArray($(this).attr('data-type'), filter[schedule].type) != -1))
                    {
                        $(this).show();
                    }
                }
                else if (filter[schedule].airline.length > 0)
                {
                    if (($.inArray($(this).attr('data-airline'), filter[schedule].airline) != -1))
                    {
                        $(this).show();
                    }
                }
            }
        });
    }
    else
    {
        $('#flight-'+schedule+' .flight-list > li').show();
    }
}

function sortResult(schedule) {
    $('#flight-'+schedule+' .flight-list li.flight-item').sort(sortir).appendTo('#flight-'+schedule+' .flight-list');

    function sortir(a, b){
        if ($('#flight-'+schedule+' .flight-sort li a.active').attr('data-direction') == 'desc')
        {
            return ($(b).data($('#flight-'+schedule+' .flight-sort li a.active').attr('rel'))) < ($(a).data($('#flight-'+schedule+' .flight-sort li a.active').attr('rel'))) ? 1 : -1;
        }
        else
        {
            return ($(b).data($('#flight-'+schedule+' .flight-sort li a.active').attr('rel'))) < ($(a).data($('#flight-'+schedule+' .flight-sort li a.active').attr('rel'))) ? -1 : 1;
        }
    }
}