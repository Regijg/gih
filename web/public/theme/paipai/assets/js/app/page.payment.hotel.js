$(function() {
    $('.payment-method ul li a').click(function() {
    	$('.payment-method .payment-tabs').hide();
    	$('.payment-method #' + $(this).attr('rel')).toggle();
    	$('.payment-method ul li a').addClass('gray');
    	$('.payment-method ul li a').addClass('outline');
    	$(this).removeClass('gray');
    	$(this).removeClass('outline');
    });
});
