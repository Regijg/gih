$(function() {
	$('.campaign-wrapper .pax a.toggle, .campaign-wrapper .qty a.toggle').click(function(e) {
        e.preventDefault();

        if ($(this).hasClass('active'))
        {
            $(this).removeClass('active');
            $(this).find('i').attr('class', 'fa fa-angle-down');
            $(this).parent().find('ul').slideUp();
        }
        else
        {
            $(this).addClass('active');
            $(this).find('i').attr('class', 'fa fa-angle-up');
            $(this).parent().find('ul').slideDown();
        }
    });

    $('.campaign-wrapper .pax ul li a, .campaign-wrapper .qty ul li a').click(function(e) {
        e.preventDefault();

        $(this).parent().parent().slideUp();
        $(this).parent().parent().parent().find('a.toggle').removeClass('active');
        $(this).parent().parent().parent().find('a.toggle span').html($(this).attr('data-value'));
        $(this).parent().parent().parent().find('a.toggle i').attr('class', 'fa fa-angle-down');
        $(this).parent().parent().parent().parent().parent().find('input[name="'+$(this).attr('data-type')+'"]').val($(this).attr('data-value'));
    });

    $('.datepicker.date-depart-campaign').datepicker({
        dateFormat: "dd-mm-yy",
        minDate: 0,
        autoclose: true,
        onSelect: function(date) { 
            var minDate = new Date($(this).datepicker('getDate'));
            minDate.setDate(minDate.getDate());
            $('.datepicker.date-return-campaign').datepicker("destroy");
            $('.datepicker.date-return-campaign').datepicker({
                dateFormat: "dd-mm-yy",
                minDate: minDate,
                autoclose: true
            });
        }
    });

    $('.datepicker.date-return-campaign').datepicker({
        dateFormat: "dd-mm-yy",
        minDate: (($('.datepicker.date-depart-campaign').val() != '') ? $('.datepicker.date-depart-campaign').datepicker('getDate') : 0),
        autoclose: true
    });

    $('.campaign-search form').submit(function(){
        openLoader();
    });

    $('.campaign-search .date-switch a').click(function(e) {
        e.preventDefault();
        var type = $(this).attr('rel');

        if ( ! $(this).hasClass('active'))
        {
            $('.campaign-search .date-switch a').removeClass('active');
            $(this).addClass('active');

            if ($(this).hasClass('return'))
            {
                $('.campaign-search .date .item.return').fadeIn();
            }
            else
            {
                $(this).parent().parent().find('input[name="return"]').val('');
                $('.campaign-search .date .item.return').hide();
            }
        }
    });

    $('.quick-action').click(function(){
        openLoader();

        var type = $(this).attr('rel');
        switch(type)
        {
            case 'flight':
                var url = $('#common-data').attr('app-url') + 'product/flight/search';
                    url += '?origin=' + (($('#common-data').attr('data-origin') != '') ? $('#common-data').attr('data-origin') : 'CGK');
                    url += '&origin_label=' + (($('#common-data').attr('data-origin-label') != '') ? $('#common-data').attr('data-origin-label') : 'JAKARTA, INDONESIA (CGK)');
                    url += '&destination=' + $(this).attr('data-code');
                    url += '&destination_label=' + $(this).attr('data-label');
                    url += '&depart=' + $('#common-data').attr('data-date-today');
                    url += '&return=' + $('#common-data').attr('data-date-tomorrow');
                    url += '&adult=1&child=0&infant=0';

                window.location = url;
            break;
            case 'train':
                var url = $('#common-data').attr('app-url') + 'product/train/search';
                    url += '?from=GMR';
                    url += '&from_label=GAMBIR (GMR), JAKARTA';
                    url += '&to=' + $(this).attr('data-code');
                    url += '&to_label=' + $(this).attr('data-label');
                    url += '&depart=' + $('#common-data').attr('data-date-today');
                    url += '&return=' + $('#common-data').attr('data-date-tomorrow');
                    url += '&adult=1&infant=0';

                window.location = url;
            break;
            case 'hotel':
                var url = $('#common-data').attr('app-url') + 'product/hotel/search';
                    url += '?location=' + $(this).attr('data-keyword');
                    url += '&checkin=' + $('#common-data').attr('data-date-today');
                    url += '&checkout=' + $('#common-data').attr('data-date-tomorrow');
                    url += '&room=1&guest=1';

                window.location = url;
            break;
            default:
                closeLoader();
            break;
        }
    });
});