var lang = {};

    getLang();

function getLang()
{
    var ajax = $.ajax({
        url: $('#common-data').attr('app-url') + 'common/get_language',
        success : function(response) {
            if (response.code == 200) {
                lang = response.data.res;
            }
        }
    });

    ajax = null; delete ajax;
}

$(window).load(function() {
    openLoader();
});

$(function() {
	$('a').click(function(event) {
		if (($(this).attr('href') == '#') || $(this).hasClass('ajax'))
		{
			event.preventDefault();
		}
		else
		{
			if ( ! $(this).hasClass('no-loader'))
			{
				openLoader();
			}
		}
	});

    if ($('#common-message').attr('message-default') != '') {
        openNotification({wrapper:'.pop',title:'Notification',popup:true,message:$('#common-message').attr('message-default')});
    }

    if ($('#common-message').attr('message-info') != '') {
        openNotification({wrapper:'.pop',style:'info',popup:true,title:'Notification',message:$('#common-message').attr('message-info')});
    }

    if ($('#common-message').attr('message-warning') != '') {
        openNotification({wrapper:'.pop',style:'warning',popup:true,title:'Notification',message:$('#common-message').attr('message-warning')});
    }

    if ($('#common-message').attr('message-error') != '') {
        openNotification({wrapper:'.pop',style:'error',popup:true,title:'Notification',message:$('#common-message').attr('message-error')});
    }

    if ($('#common-message').attr('message-success') != '') {
        openNotification({wrapper:'.pop',style:'success',popup:true,title:'Notification',message:$('#common-message').attr('message-success')});
    }

	$(window).bind('pageshow', function(event) {
	    closeLoader();
	});

	$(window).scroll(function() {
		if ($(this).scrollTop() > $('main').offset().top) {
			$('body, header').addClass('scrolled');
		}
		else
		{
			$('body, header').removeClass('scrolled');
		}
    });

    // Nav
    $('.nav-menu, .nav-close').click(function(e){
        if ( ! $(this).hasClass('no-toggle'))
        {
            e.preventDefault();

            toggleMenu();
        }
    });

    $('button.result-change').click(function(e) {
        e.preventDefault();

        $('.search-category li a').removeClass('active');
        $('.search-category li a[rel="'+$(this).attr('rel')+'"]').addClass('active');

        $('.search-form form.form-search').hide();
        $('.search-form form#form-' + $(this).attr('rel')).fadeIn();
        $('.search-wrapper').addClass('active');
    });
});

function swipeLeft() {
    if ($('body').hasClass('menu-active'))
    {
        $('body').removeClass('menu-active');
        $('body').removeClass('hide-overflow');
        $('.nav-close').hide();
        $('nav .nav-wrapper').toggle('slide', {direction: 'left'}, 'fast', function(){
            $('nav').hide();
        });
    }
}

function swipeRight() {
    if ( ! $('body').hasClass('menu-active'))
    {
        $('body').addClass('menu-active');
        $('body').addClass('hide-overflow');
        $('nav').fadeIn('fast', function(){
            $('nav .nav-wrapper').toggle('slide', {direction: 'left'}, 'fast', function(){
                $('.nav-close').show();
            });
        });
    }
}

function toggleMenu() {
    if ($('body').hasClass('menu-active'))
    {
        swipeLeft();
    }
    else
    {
        swipeRight();
    }
}

function openLoader(params, callback) {
    params = (typeof params !== 'undefined' && params != null) ? params : false;
    callback = typeof callback !== 'undefined' ? callback : false;

    wrapper = (isset(params.wrapper) ? params.wrapper : 'body');
    type = (isset(params.type) ? ' ' + params.type : '');
    style = (isset(params.style) ? ' ' + params.style : '');

    if ($(wrapper + ' > .loader').length == 0)
    {
        $(wrapper).addClass('pos-relative');
        $(wrapper).prepend('<div class="loader'+type+style+'"><div class="loader-overlay"></div><div class="loader-horizontal"><div></div><div></div><div></div><div></div><div></div></div></div>');
    }

    if (callback != false) callback();
}

function closeLoader(params, callback) {
    params = (typeof params !== 'undefined' && params != null) ? params : false;
    callback = typeof callback !== 'undefined' ? callback : false;

    wrapper = (isset(params.wrapper) ? params.wrapper : 'body');
    $(wrapper + ' > .loader').fadeOut(function(){
        $(wrapper).removeClass('pos-relative');
        $(this).remove();

        if (callback != false) callback();
    });
}

function openNotification(params, callback) {
    params = typeof params !== 'undefined' ? params : false;
    callback = typeof callback !== 'undefined' ? callback : false;

    if (params) {
        if ($((isset(params.wrapper) ? params.wrapper : 'body') + ' > .notification').length != 0) {
            closeNotification(params);
        }

        $((isset(params.wrapper) ? params.wrapper : 'body')).prepend(''+
                '<div class="notification ' + (isset(params.style) ? params.style : '') + ' ' + (isset(params.popup) ? 'popup' : '') + '" style="display: none;">'+
                    '<a class="close" onclick="closeNotification({wrapper:\''+(isset(params.wrapper) ? params.wrapper : 'body')+'\'});"><i class="fa fa-remove"></i></a>'+
                    (isset(params.title) ? '<h4>' + params.title + '</h4>' : '') +
                    '<p>'+(isset(params.message) ? params.message : '-') +'</p>'+
                '</div>');

        if (isset(params.popup))
        {
            $('.pop').show();
        }
        $((isset(params.wrapper) ? params.wrapper : 'body') + ' > .notification').slideDown('fast');

        if (callback != false) callback();
    }
}

function closeNotification(params, callback) {
    params = typeof params !== 'undefined' ? params : false;
    callback = typeof callback !== 'undefined' ? callback : false;

    if (params) {
        $((isset(params.wrapper) ? params.wrapper : 'body') + ' > .notification').slideUp('fast', function(){
            $(this).remove();

            if (isset(params.wrapper) && (params.wrapper == '.pop'))
            {
                $('.pop').hide();
            }
        });

        if (callback != false) callback();
    }
}

function isset(data) {
    if (typeof data != 'undefined') {
        return true;
    } else {
        return false;
    }
}

function set_number_format(number, decimal, decimal_symbol, thousands_symbol, tag, shorten) {
    number = parseFloat(number);
    decimal = typeof decimal !== 'undefined' ? parseInt(decimal) : 0;
    tag = typeof tag !== 'undefined' ? tag : '';
    shorten = typeof shorten !== 'undefined' ? shorten.toUpperCase() : '';

    if (isset(decimal_symbol) && (decimal_symbol != ''))
    {
        decimal_symbol = decimal_symbol;
    }
    else
    {
        decimal_symbol = lang.decimal_symbol;
    }

    if (isset(thousands_symbol) && (thousands_symbol != ''))
    {
        thousands_symbol = thousands_symbol;
    }
    else
    {
        thousands_symbol = lang.thousands_symbol;
    }

    if (number > 1000) {
        if (shorten == 'k') {
            number = Math.round(number/1000);
        }
    }

    value_number = number.toFixed(decimal);
    split_number = value_number.toString().split('.');

    formatted_number = split_number[0].replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1' + thousands_symbol) + (isset(split_number[1]) ? decimal_symbol+split_number[1] :'');

    if (tag != '') {
        number_temp = formatted_number.split(thousands_symbol);

        tag = tag.toLowerCase();
        formatted_number = '';

        if (number_temp.length > 1) {
            for (i = 0; i < number_temp.length; i++) {
                if (i != (number_temp.length - 1)) {
                    formatted_number += '<'+tag+'>'+number_temp[i]+'</'+tag+'>'+thousands_symbol;
                } else {
                    formatted_number += number_temp[i];
                }
            }
        } else {
            formatted_number += '<'+tag+'>'+number_temp[0]+'</'+tag+'>';
        }
    }

    if (shorten == 'K') {
        formatted_number = formatted_number + shorten;
    }

    delete decimal_symbol;
    delete thousands_symbol;

    return formatted_number;
}