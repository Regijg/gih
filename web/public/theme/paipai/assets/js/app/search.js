var search_poi = null,
    airport = {},
    station = {};

getAirport();
getStation();
initLocation();

$(function() {
    // Search Toggle
    $('.search-toggle').click(function(e) {
        e.preventDefault();

        if ($('.search-wrapper').hasClass('active'))
        {
            $('.search-wrapper').removeClass('active');
        }
        else
        {
            $('.search-wrapper').addClass('active');
        }
    });

    // Search Category
    $('.search-category li a.no-loader').click(function(e) {
        e.preventDefault();

        $('.search-category li a').removeClass('active');
        $(this).addClass('active');

        $('.search-form form.form-search').hide();
        $('.search-form form#form-' + $(this).attr('rel')).fadeIn();
    });

    //
    $('.switch-route').click(function(e) {
        var elem = $(this).parent().parent().parent().parent(),
            type = $(this).attr('data-type'),
            tmp_code = '';
            tmp_label = '';

        switch(type)
        {
            case 'flight':
                tmp_code = $(elem).find('input[name="origin"]').val();
                tmp_label = $(elem).find('input[name="origin_label"]').val();

                $(elem).find('input[name="origin"]').val($(elem).find('input[name="destination"]').val());
                $(elem).find('input[name="origin_label"]').val($(elem).find('input[name="destination_label"]').val());
                $(elem).find('.select-get-origin').val($(elem).find('input[name="destination_label"]').val());
                $(elem).find('input[name="destination"]').val(tmp_code);
                $(elem).find('input[name="destination_label"]').val(tmp_label);
                $(elem).find('.select-get-destination').val(tmp_label);
            break;
            case 'train':
                tmp_code = $(elem).find('input[name="from"]').val();
                tmp_label = $(elem).find('input[name="from_label"]').val();

                $(elem).find('input[name="from"]').val($(elem).find('input[name="to"]').val());
                $(elem).find('input[name="from_label"]').val($(elem).find('input[name="to_label"]').val());
                $(elem).find('.select-get-from').val($(elem).find('input[name="to_label"]').val());
                $(elem).find('input[name="to"]').val(tmp_code);
                $(elem).find('input[name="to_label"]').val(tmp_label);
                $(elem).find('.select-get-to').val(tmp_label);
            break;
        }
    });

    // Search Date -> oneway | return
    $('.search-wrapper .date-switch a').click(function(e) {
        e.preventDefault();
        var type = $(this).attr('rel');

        if ( ! $(this).hasClass('active'))
        {
            $('#form-search-'+type+' .date-switch a').removeClass('active');
            $(this).addClass('active');

            if ($(this).hasClass('return'))
            {
                $('#form-search-'+type+' .date .item.return').fadeIn();
            }
            else
            {
                $(this).parent().parent().find('input[name="return"]').val('');
                $('#form-search-'+type+' .date .item.return').hide();
            }
        }
    });

    // Search Pax & Qty
    $('.search-wrapper .pax a.toggle, .search-wrapper .qty a.toggle').click(function(e) {
        e.preventDefault();

        if ($(this).hasClass('active'))
        {
            $(this).removeClass('active');
            $(this).find('i').attr('class', 'fa fa-angle-down');
            $(this).parent().find('ul').slideUp();
        }
        else
        {
            $(this).addClass('active');
            $(this).find('i').attr('class', 'fa fa-angle-up');
            $(this).parent().find('ul').slideDown();
        }
    });

    $('.search-wrapper .pax ul li a, .search-wrapper .qty ul li a').click(function(e) {
        e.preventDefault();

        $(this).parent().parent().slideUp();
        $(this).parent().parent().parent().find('a.toggle').removeClass('active');
        $(this).parent().parent().parent().find('a.toggle span').html($(this).attr('data-value'));
        $(this).parent().parent().parent().find('a.toggle i').attr('class', 'fa fa-angle-down');
        $(this).parent().parent().parent().parent().parent().find('input[name="'+$(this).attr('data-type')+'"]').val($(this).attr('data-value'));
    });

    // Search Date
    $('.datepicker.date-depart').datepicker({
        dateFormat: "dd-mm-yy",
        minDate: 0,
        autoclose: true,
        onSelect: function(date) { 
            var minDate = new Date($(this).datepicker('getDate'));
            minDate.setDate(minDate.getDate());
            $('.datepicker.date-return').datepicker("destroy");
            $('.datepicker.date-return').datepicker({
                dateFormat: "dd-mm-yy",
                minDate: minDate,
                autoclose: true
            });
        }
    });

    $('.datepicker.date-return').datepicker({
        dateFormat: "dd-mm-yy",
        minDate: (($('.datepicker.date-depart').val() != '') ? $('.datepicker.date-depart').datepicker('getDate') : 0),
        autoclose: true
    });

    //----------------------
    $('.datepicker.date-checkin').datepicker({
        dateFormat: "dd-mm-yy",
        minDate: 0,
        autoclose: true,
        onSelect: function(date) {
            var minDate = new Date($(this).datepicker('getDate'));
            var maxDate = new Date($(this).datepicker('getDate'));
            minDate.setDate(minDate.getDate() + 1);
            maxDate.setDate(maxDate.getDate() + 8);
            $('.datepicker.date-checkout').datepicker("destroy");
            $('.datepicker.date-checkout').datepicker({
                dateFormat: "dd-mm-yy",
                minDate: minDate,
                maxDate: maxDate,
                autoclose: true
            });
        }
    });

    $('.datepicker.date-checkout').datepicker({
        dateFormat: "dd-mm-yy",
        minDate: "+1d",
        maxDate: "+8d",
        autoclose: true,
    });

    // Search Date
    $('.datepicker.date-show').datepicker({
        dateFormat: "dd-mm-yy",
        minDate: 0,
        autoclose: true
    });

    $(document).click(function(event) {
        if ( ! $(event.target).closest('.select-get-origin, .selection-container').length) {
            $('.select-get-origin').parent().find('.selection-container').hide();
        }

        if ( ! $(event.target).closest('.select-get-destination, .selection-container').length) {
            $('.select-get-destination').parent().find('.selection-container').hide();
        }

        if ( ! $(event.target).closest('.select-get-from, .selection-container').length) {
            $('.select-get-from').parent().find('.selection-container').hide();
        }

        if ( ! $(event.target).closest('.select-get-to, .selection-container').length) {
            $('.select-get-to').parent().find('.selection-container').hide();
        }

        if ( ! $(event.target).closest('.select-get-hotelpoi, .selection-container').length) {
            $('.select-get-hotelpoi').parent().find('.selection-container').hide();
        }
    });

    $('.select-get-origin').on('focus', function(){
        $(this).parent().find('.selection-container').show();
    });

    $('.select-get-destination').on('focus', function(){
        $(this).parent().find('.selection-container').show();
    });

    $('.select-get-from').on('focus', function(){
        $(this).parent().find('.selection-container').show();
    });

    $('.select-get-to').on('focus', function(){
        $(this).parent().find('.selection-container').show();
    });

    $('.select-get-hotelpoi').on('focus', function(){
        $(this).parent().find('.selection-container').show();
    });

    $('.select-get-origin').bind('keyup', function() {
        var elem = this;
        var q = $(this).val();

        $(elem).parent().find('.selection-container li.item').remove();
            
        if (airport.length > 0)
        {
            $(elem).parent().find('.selection-container li.message').html('Memuat data...').show();

            var found = 0;
            $.each(airport, function(key, res) {
                if ((res.airport_code.toLowerCase().indexOf(q.toLowerCase()) >= 0)
                    || (res.airport_name.toLowerCase().indexOf(q.toLowerCase()) >= 0)
                    || (res.city.toLowerCase().indexOf(q.toLowerCase()) >= 0)
                    )
                {
                    $(elem).parent().find('.selection-container li.message').html('').hide();
                    $(elem).parent().find('.selection-container li.message').after('<li class="item"><a id="set-airport-'+res.airport_code+'">'+res.city.toUpperCase()+', '+res.country.toUpperCase()+' ('+res.airport_code.toUpperCase()+')</a></li>');

                    $('#set-airport-'+res.airport_code).unbind('click');
                    $('#set-airport-'+res.airport_code).click(function(){
                       setAirport(this, 'origin', res);
                    });

                    found++;
                }
            });

            if ( ! found)
            {
                $(elem).parent().find('.selection-container li.message').html('Data tidak ditemukan').show();
            }

            found = null; delete found;
        }
        else
        {
            $(elem).parent().find('.selection-container li.message').html('Mencari data...').show();

            getAirport();
        }

        q = null; delete q;
    });

    $('.select-get-destination').bind('keyup', function(){
        var elem = this;
        var q = $(this).val();

        $(elem).parent().find('.selection-container li.item').remove();
            
        if (airport.length > 0)
        {
            $(elem).parent().find('.selection-container li.message').html('Memuat data...').show();

            var found = 0;
            $.each(airport, function(key, res) {
                if ((res.airport_code.toLowerCase().indexOf(q.toLowerCase()) >= 0)
                    || (res.airport_name.toLowerCase().indexOf(q.toLowerCase()) >= 0)
                    || (res.city.toLowerCase().indexOf(q.toLowerCase()) >= 0)
                    )
                {
                    $(elem).parent().find('.selection-container li.message').html('').hide();
                    $(elem).parent().find('.selection-container li.message').after('<li class="item"><a id="set-airport-'+res.airport_code+'">'+res.city.toUpperCase()+', '+res.country.toUpperCase()+' ('+res.airport_code.toUpperCase()+')</a></li>');

                    $('#set-airport-'+res.airport_code).unbind('click');
                    $('#set-airport-'+res.airport_code).click(function(){
                       setAirport(this, 'destination', res);
                    });

                    found++;
                }
            });

            if ( ! found)
            {
                $(elem).parent().find('.selection-container li.message').html('Data tidak ditemukan').show();
            }

            found = null; delete found;
        }
        else
        {
            $(elem).parent().find('.selection-container li.message').html('Mencari data...').show();

            getAirport();
        }

        q = null; delete q;
    });

    $('.select-get-from').bind('keyup', function() {
        var elem = this;
        var q = $(this).val();

        $(elem).parent().find('.selection-container li.item').remove();
            
        if (station.length > 0)
        {
            $(elem).parent().find('.selection-container li.message').html('Memuat data...').show();

            var found = 0;
            $.each(station, function(key, res) {
                if ((res.station_code.toLowerCase().indexOf(q.toLowerCase()) >= 0)
                    || (res.station_name.toLowerCase().indexOf(q.toLowerCase()) >= 0)
                    || (res.city.toLowerCase().indexOf(q.toLowerCase()) >= 0)
                    )
                {
                    $(elem).parent().find('.selection-container li.message').html('').hide();
                    $(elem).parent().find('.selection-container li.message').after('<li class="item"><a id="set-station-'+res.station_code+'">'+res.station_name.toUpperCase()+' ('+res.station_code.toUpperCase()+'), '+res.city.toUpperCase()+'</a></li>');

                    $('#set-station-'+res.station_code).unbind('click');
                    $('#set-station-'+res.station_code).click(function(){
                       setStation(this, 'from', res);
                    });

                    found++;
                }
            });

            if ( ! found)
            {
                $(elem).parent().find('.selection-container li.message').html('Data tidak ditemukan').show();
            }

            found = null; delete found;
        }
        else
        {
            $(elem).parent().find('.selection-container li.message').html('Mencari data...').show();

            getStation();
        }

        q = null; delete q;
    });

    $('.select-get-to').bind('keyup', function(){
        var elem = this;
        var q = $(this).val();

        $(elem).parent().find('.selection-container li.item').remove();
            
        if (station.length > 0)
        {
            $(elem).parent().find('.selection-container li.message').html('Memuat data...').show();

            var found = 0;
            $.each(station, function(key, res) {
                if ((res.station_code.toLowerCase().indexOf(q.toLowerCase()) >= 0)
                    || (res.station_name.toLowerCase().indexOf(q.toLowerCase()) >= 0)
                    || (res.city.toLowerCase().indexOf(q.toLowerCase()) >= 0)
                    )
                {
                    $(elem).parent().find('.selection-container li.message').html('').hide();
                    $(elem).parent().find('.selection-container li.message').after('<li class="item"><a id="set-station-'+res.station_code+'">'+res.station_name.toUpperCase()+' ('+res.station_code.toUpperCase()+'), '+res.city.toUpperCase()+'</a></li>');

                    $('#set-station-'+res.station_code).unbind('click');
                    $('#set-station-'+res.station_code).click(function(){
                       setStation(this, 'to', res);
                    });

                    found++;
                }
            });

            if ( ! found)
            {
                $(elem).parent().find('.selection-container li.message').html('Data tidak ditemukan').show();
            }

            found = null; delete found;
        }
        else
        {
            $(elem).parent().find('.selection-container li.message').html('Mencari data...').show();

            getStation();
        }

        q = null; delete q;
    });

    $('.select-get-hotelpoi').bind('keyup', function() {
        var elem = this;

        if (search_poi != null) {
            search_poi.abort();
        }

        search_poi = $.ajax({
            url: $('#common-data').attr('app-url') + 'common/get_poi',
            type: 'POST',
            data: {keyword: $(this).val()},
            dataType: 'json',
            beforeSend: function() {
                $(elem).parent().find('.selection-container li.item').remove();
                $(elem).parent().find('.selection-container li.message').html('Memuat data...').show();
            },
            success : function(response) {
                if (response.code == 200)
                {
                    $(elem).parent().find('.selection-container li.message').html('').hide();
                    $.each(response.data.res, function(key, res) {
                        if(res.category === "subregion" || res.category === "region"){
                            $(elem).parent().find('.selection-container li.message').after('<li class="item"><a onclick="setPoi(this, \'hotelpoi\',\''+res.value+'\')">'+res.value+'</a></li>');
                        }
                    });
                }
                else
                {
                    $(elem).parent().find('.selection-container li.message').html(response.message).show();
                }
            },
            error: function(x, t, m) {
                if (x.statusText != 'abort')
                {
                    $(elem).parent().find('.selection-container li.message').html(m).show();
                }
            }
        });
    });

    $('.form-search').submit(function(e){
        e.preventDefault();

        submitSearch(this);
    });
});

function getAirport()
{
    var ajax = $.ajax({
        url: $('#common-data').attr('app-url') + 'common/get_airport',
        success : function(response) {
            if (response.code == 200) {
                airport = response.data.res;
            }
        }
    });

    ajax = null; delete ajax;
}

function setAirport(elem, type, data)
{
    var container = $(elem).parent().parent().parent().parent();
    container.find('.selection-container').hide();
    container.find('.selection-container li.item').remove();
    container.find('input[name="'+type+'"]').val(data.airport_code);
    container.find('input[name="'+type+'_label"]').val(data.city.toUpperCase()+', '+data.country.toUpperCase()+' ('+data.airport_code.toUpperCase()+')');
    container.find('input[type="text"]').val(data.city.toUpperCase()+', '+data.country.toUpperCase()+' ('+data.airport_code.toUpperCase()+')');
}

function getStation()
{
    var ajax = $.ajax({
        url: $('#common-data').attr('app-url') + 'common/get_station',
        success : function(response) {
            if (response.code == 200) {
                station = response.data.res;
            }
        }
    });

    ajax = null; delete ajax;
}

function setStation(elem, type, data)
{
    var container = $(elem).parent().parent().parent().parent();
    container.find('.selection-container').hide();
    container.find('.selection-container li.item').remove();
    container.find('input[name="'+type+'"]').val(data.station_code);
    container.find('input[name="'+type+'_label"]').val(data.station_name.toUpperCase()+' ('+data.station_code.toUpperCase()+'), '+data.city.toUpperCase());
    container.find('input[type="text"]').val(data.station_name.toUpperCase()+' ('+data.station_code.toUpperCase()+'), '+data.city.toUpperCase());
}

function setPoi(elem, type, data)
{
    var container = $(elem).parent().parent().parent().parent();
    container.find('.selection-container').hide();
    container.find('.selection-container li.item').remove();
    container.find('input[type="text"]').val(data);
}


function initLocation() {
    if (($('#common-data').attr('data-latitude') == '') && ($('#common-data').attr('data-longitude') == ''))
    {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                $('#common-data').attr('data-latitude', position.coords.latitude);
                $('#common-data').attr('data-longitude', position.coords.longitude);

                codeLatLng(position.coords.latitude, position.coords.longitude);
            }, function(err) {
                handleLocationError(true, err);
            });
        } else {
            handleLocationError(false);
        }
    }
    else
    {
        codeLatLng(position.coords.latitude, position.coords.longitude);
    }
}

function handleLocationError(browserHasGeolocation, error) {
    error = typeof error !== 'undefined' ? error : false;

    var msg = (browserHasGeolocation ?
          'Error: The Geolocation service failed.' + (isset(error.code) ? ' Code: ' + error.code : '') + (isset(error.message) ? ' # ' + error.message : ''):
          'Error: Your browser doesn\'t support geolocation.');

    // openNotification({wrapper:'.pop',style:'error',popup:true,title:'Notification',message:msg});
}

function codeLatLng(lat, lng) {
    var ajax = $.ajax({
        url: $('#common-data').attr('app-url') + 'common/get_iata/'+lat+'/'+lng,
        success : function(response) {
            if (response.code == 200) {
                if (airport.length > 0)
                {
                    var code = response.data.res.code;
                    $.each(airport, function(key, res) {
                        if ((res.airport_code.toLowerCase().indexOf(code.toLowerCase()) >= 0))
                        {
                            $('#common-data').attr('data-origin', res.airport_code);
                            $('#common-data').attr('data-origin-label', res.city.toUpperCase()+', '+res.country.toUpperCase()+' ('+res.airport_code.toUpperCase()+')');
                            $('#common-data').attr('data-origin-city', res.city.toUpperCase());

                            return false;
                        }
                    });
                }
            }
        }
    });

    ajax = null; delete ajax;
}

function submitSearch(form)
{
    var ajax = $.ajax({
        url: $(form).attr('action'),
        type: 'post',
        dataType: 'json',
        data: $(form).serialize(),
        beforeSend: function() {
            openLoader();
        },
        success : function(response) {
            if (response.code == '200')
            {
                window.location = response.redirect;
            }
            else
            {
                closeLoader(null, function(){
                    openNotification({wrapper:'.pop',style:'error',popup:true,title:'Notification',message:response.message});
                });
            }
        },
        error: function(x, t, m) {
            closeLoader(null, function(){
                openNotification({wrapper:'.pop',style:'error',popup:true,title:'Notification',message:response.message});
            });
        }
    });

    ajax = null; delete ajax;
}