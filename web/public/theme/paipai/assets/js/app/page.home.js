$(function() {
	// Slideshow
	$('.main-slider').bxSlider({
        auto: true,
        pause: 7000,
        adaptiveHeight: true,
        nextText: '<i class="fa fa-angle-right"></i>',
        prevText: '<i class="fa fa-angle-left"></i>',
        onSliderLoad: function(){
            $('.main-slider').css('visibility', 'visible');
        }
    });

    $('.quick-action').click(function(){
        openLoader();

        var type = $(this).attr('rel');
        switch(type)
        {
            case 'flight':
                var url = $('#common-data').attr('app-url') + 'product/flight/search';
                    url += '?origin=' + (($('#common-data').attr('data-origin') != '') ? $('#common-data').attr('data-origin') : 'CGK');
                    url += '&origin_label=' + (($('#common-data').attr('data-origin-label') != '') ? $('#common-data').attr('data-origin-label') : 'JAKARTA, INDONESIA (CGK)');
                    url += '&destination=' + $(this).attr('data-code');
                    url += '&destination_label=' + $(this).attr('data-label');
                    url += '&depart=' + $('#common-data').attr('data-date-today');
                    url += '&return=' + $('#common-data').attr('data-date-tomorrow');
                    url += '&adult=1&child=0&infant=0';

                window.location = url;
            break;
            case 'train':

            break;
            case 'hotel':

            break;
            default:
                closeLoader();
            break;
        }
    });
});