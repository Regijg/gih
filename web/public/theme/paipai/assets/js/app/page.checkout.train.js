$(function() {
    $('.datedob').datepicker({
        dateFormat: "dd-mm-yy",
        maxDate: 0,
        autoclose: true,
        changeYear: true,
        yearRange: "-80:+0",
    });

    $('#form-checkout').submit(function(e) {
    	e.preventDefault();

    	submitCheckout(this);
    });
});

function submitCheckout(form)
{
    var ajax = $.ajax({
        url: $(form).attr('action'),
        type: 'post',
        dataType: 'json',
        data: $(form).serialize(),
        beforeSend: function() {
            openLoader();
        },
        success: function(res)
        {
            if (res.code == 200)
            {
                window.location = res.redirect;
            }
            else
            {
                closeLoader();
                openNotification({wrapper:'.pop',style:'error',title:'Error',popup:true,message:res.message});
            }
        },
        error: function(x, t, m)
        {
            closeLoader();
            openNotification({wrapper:'.pop',style:'error',title:'Error',popup:true,message:m});
        }
    });

    ajax = null; delete ajax;
}
