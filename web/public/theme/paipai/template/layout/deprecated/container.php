<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
    <!--<title><?php //echo $document['app_title']; ?><?php // echo ( ! empty($document['page_title']) ? ' - ' . $document['page_title'] : ( ! empty($config['app']['tagline']['value']) ? ' - ' . $config['app']['tagline']['value'] : '')); ?></title>-->
    <title>Real Travel Ticket</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="HandheldFriendly" content="true">
    <link rel="shortcut icon" href="https://realtravel.co.id/static/images/favicon.png" type="image/x-icon">
    <!-- STYLES -->
    <link href="<?php echo $assets_url; ?>css/reset.css" rel="stylesheet">
    <link href="<?php echo $assets_url; ?>css/style.css" rel="stylesheet">
    <link href="<?php echo $assets_url; ?>css/search.css" rel="stylesheet">
    <link href="<?php echo $assets_url; ?>js/jquery-ui/jquery-ui.css" rel="stylesheet">
    <?php
    if ( ! empty($document['css']))
    {
        foreach ($document['css'] as $document_css)
        {
            echo "\n" . '<link href="'.$document_css.'" rel="stylesheet">';
        }
    }
    ?>
    <link href="<?php echo $assets_url; ?>css/color-default.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="<?php echo $assets_url; ?>js/html5shiv.min.js" type="text/javascript"></script>
      <script src="<?php echo $assets_url; ?>js/respond.min.js" type="text/javascript"></script>
    <![endif]-->
    <!-- SCRIPTS -->
    <script src="<?php echo $assets_url; ?>js/jquery-2.0.0.min.js" type="text/javascript"></script>
    <script src="<?php echo $assets_url; ?>js/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
</head>
<body>
    <?php echo ( ! empty($app['type']) && ($app['type'] == 'full') ? $header : ''); ?>

    <main>
        <?php echo ( ! empty($app['type']) && ($app['type'] == 'full') ? $search : ''); ?>

        <?php echo $content; ?>
    </main>
    
    <?php echo ( ! empty($app['type']) && ($app['type'] == 'full') ? $footer : ''); ?>

    <div class="pop"></div>
	<div id="common-data" class="site-metadata" base-url="<?php echo base_url(); ?>" app-url="<?php echo $app_url; ?>" assets-url="<?php echo $assets_url; ?>" storage-url="<?php echo $storage_url; ?>" data-latitude="" data-longitude="" data-origin="CGK" data-origin-label="JAKARTA, INDONESIA (CGK)" data-origin-city="" data-date-today="<?php echo date('Y-m-d'); ?>" data-date-tomorrow="<?php echo date('Y-m-d', strtotime("+1 day")); ?>"></div>
    <div id="common-message" class="site-metadata" message-default="<?php echo $this->session->flashdata('default'); ?>" message-info="<?php echo $this->session->flashdata('info'); ?>" message-warning="<?php echo $this->session->flashdata('warning'); ?>" message-error="<?php echo $this->session->flashdata('error'); ?>" message-success="<?php echo $this->session->flashdata('success'); ?>"></div>
    <script src="<?php echo $assets_url; ?>js/app/main.js" type="text/javascript"></script>
    <script src="<?php echo $assets_url; ?>js/app/search.js" type="text/javascript"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?php echo $this->config->item('gmaps_api_key'); ?>"></script>
    <?php
    if ( ! empty($document['js']))
    {
        foreach ($document['js'] as $document_js)
        {
            echo "\n" . '<script src="'.$document_js.'" type="text/javascript"></script>';
        }
    }
    ?>
</body>
</html>
