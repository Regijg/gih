<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<footer>
	<div class="container">
		<div class="footer-nav clearfix">
			<div class="about">
				<img src="<?php echo $assets_url; ?>img/gih-logo.png" alt="tabs">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
	quis nostrud exercitation.</p>
				<div class="clear"></div>

				<ul class="social clearfix">
					<li><a href="#" class="facebook"><i class="fa fa-facebook"></i> Facebook</a></li>
					<li><a href="#" class="google-plus"><i class="fa fa-google-plus"></i> Google+</a></li>
					<li><a href="#" class="twitter"><i class="fa fa-twitter"></i> Twitter</a></li>
					<li><a href="#" class="instagram"><i class="fa fa-instagram"></i> Instagram</a></li>
				</ul>
			</div>
			<div class="footer-menu clearfix">
				<ul class="link">
					<li><a href="<?php echo $app_url; ?>about">Tentang Kami</a></li>
					<li><a href="<?php echo $app_url; ?>contact">Kontak</a></li>
					<li><a href="<?php echo $app_url; ?>career">Karir</a></li>
				</ul>
				<ul class="link">
					<li><a href="<?php echo $app_url; ?>terms">Syarat &amp; Ketentuan</a></li>
					<li><a href="<?php echo $app_url; ?>privacy">Kebijakan Privasi</a></li>
					<li><a href="<?php echo $app_url; ?>help">Bantuan</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="container clearfix ptop-20">
		<div class="download-app">
			<div class="desc" style="padding-left: 0px;">
				<!--<h4>Download Gratis Aplikasi <?php //echo $config['app']['name']['value']; ?>!</h4>-->
				<h4>Download Gratis Aplikasi Holidays Garuda Indonesia!</h4>
				<small>Dapatkan kemudahan aplikasi langsung dari ponsel Anda</small>
				<p class="mtop-10 clearfix">
					<a href="#"><img src="<?php echo $assets_url; ?>img/download-gplay.png"></a>
					<a href="#"><img src="<?php echo $assets_url; ?>img/download-appstore.png"></a>
				</p>
			</div>
		</div>
		<div class="subscribe">
			<h4>Berlangganan Newsletter!</h4>
			<small>Daftarkan email Anda untuk info Promo/Penawaran spesial!</small>
			<form onsubmit="event.preventDefault();">
				<input type="text" placeholder="Masukkan email Anda">
				<button type="submit">SUBSCRIBE</button>
			</form>
		</div>
	</div>
	<!--<div class="copyright">Copyrights &copy; 2016 <?php echo $config['app']['name']['value']; ?>. All Rights Reserved.</div>-->
	<div class="copyright">Copyrights &copy; 2019 Holidays Garuda Indonesia. All Rights Reserved.</div>
</footer>
