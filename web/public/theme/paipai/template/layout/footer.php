<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<footer>
	<div class="copyright">
		<div class="container">
		<div class="footer-left">© Copyright 2019 by RealTravel, PT All rights reserved.</div>
		<div class="footer-social">
	    	<a href="https://twitter.com/GI_Holidays?lang=id" target="new"><i class="fa fa-twitter"></i></a>
			<a href="https://www.facebook.com/garudaindonesiaholidays/" target="new"><i class="fa fa-facebook"></i></a>
	        <a href="https://www.instagram.com/garudaindonesiaholidays/?hl=id" target="new"><i class="fa fa-instagram"></i></a>
		
		</div>
	</div>
	</div>
</footer>
