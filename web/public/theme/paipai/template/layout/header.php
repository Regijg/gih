<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<header>
    <div class="header-top">
        <div class="container">
            <!-- <a class="nav nav-menu no-loader"><i class="fa fa-navicon"></i></a> -->
            <div class="main-logo">
                <a href="https://landing.realtravel.co.id/" style="margin-top: 0px;">
                    <img src="https://realtravel.co.id/static/images/logo.png" alt="GIH">
                </a>
            </div>
            <div class="top-nav clearfix" style="margin-left: 20px;">
                <ul class="progress-indicator" style="margin-top:20px;">
                    <li class="<?=$document['progress']['step1']?>">
                        <span class="bubble"></span>
                        <?php 
                            $uriSegments = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
                        ?> 
                        <b>1. Pilih <?php echo $uriSegments[2]?></b>
                    </li>
                    <li class="<?=$document['progress']['step2']?>">
                        <span class="bubble"></span>
                        <?php if ($uriSegments[2] == 'flight') { ?>                        
                            <b>2. Informasi Penumpang</b>
                        <?php } else { ?>                        
                            <b>2. Informasi Data</b>
                        <?php } ?>
                    </li>
                    <li class="<?=$document['progress']['step3']?>">
                        <span class="bubble"></span>
                        <b>3. Pembayaran</b>
                    </li>
                    <li class="<?=$document['progress']['step4']?>">
                        <span class="bubble"></span>
                        <b>4. Voucher</b>
                    </li>
                </ul>

            </div>
        </div>
    </div>
    <div class="header-main">
        <div class="container clearfix">
            <!--<ul class="main-menu clearfix">
                <li><a href="<?php echo $app_url; ?>product/flight">PESAWAT</a></li>
                <li><a href="<?php echo $app_url; ?>product/train">KERETA</a></li>
                <li><a href="<?php echo $app_url; ?>product/hotel">HOTEL</a></li>
            </ul>
            <div class="main-action">
                <a href="#"><i class="fa fa-sign-in"></i> LOGIN / DAFTAR</a>
            </div>-->
        </div>
    </div>
    <nav>
        <a class="nav-close no-loader"><i class="fa fa-chevron-left"></i></a>
        <ul class="nav-wrapper">
            <li class="logo"><img src="https://realtravel.co.id/static/images/logo.png" alt="TABS"></li>
            <li class="login"><a href="#">LOGIN / DAFTAR</a></li>
            <li class="separator"></li>
            <li><a href="<?php echo $app_url; ?>product/flight" class="no-border"><i class="fa fa-plane"></i> Pesawat</a></li>
            <!-- <li><a href="<?php echo $app_url; ?>product/train"><i class="fa fa-train"></i> Kereta</a></li> -->
            <!-- <li><a href="<?php echo $app_url; ?>product/hotel"><i class="fa fa-hotel"></i> Hotel</a></li> -->
            <li class="separator"></li>
            <li><a href="<?php echo $app_url; ?>help" class="no-border"><i class="fa fa-question-circle"></i> Bantuan</a></li>
            <!-- <li><a href="#"><i class="fa fa-check-square-o"></i> Cek Pemesanan</a></li> -->
            <li class="separator"></li>
            <li><a href="#" class="no-border"><i class="fa fa-sign-out"></i> Logout</a></li>
        </ul>
    </nav>
</header>
