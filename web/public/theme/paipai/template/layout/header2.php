<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<header>
    <div class="header-top">
        <div class="container">
            <a class="nav nav-menu no-loader"><i class="fa fa-navicon"></i></a>
            <div class="main-logo">
                <a href="<?php echo base_url(); ?>" style="margin-top: 0px;">
                    <img src="<?php echo $assets_url; ?>img/gih-logo.png" alt="TABS" style="width: 150px;">
                </a>
            </div>
            <div class="top-nav clearfix" style="margin-left: 20px;">
                <ul class="nav-left clearfix">
                    <li><a href="<?php echo $app_url; ?>help">Bantuan</a></li>
                    <li><a href="#">Cek Pemesanan</a></li>
                </ul>
                <ul class="nav-right clearfix">
                    <li class="service-hour">Jam Layanan : 07.00 - 24.00<br><small>Sabtu, Minggu &amp; Hari Libur 24 Jam</small></li>
                    <li><i class="lnr lnr-phone-handset"></i> (021) 1234 5678</li>
                    <li><i class="lnr lnr-envelope"></i>info@garudaindonesiaholidays.com</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="header-main">
        <div class="container clearfix">
            <ul class="main-menu clearfix">
                <li><a href="<?php echo $app_url; ?>product/flight">PESAWAT</a></li>
                <!--<li><a href="<?php echo $app_url; ?>product/train">KERETA</a></li>-->
                <li><a href="<?php echo $app_url; ?>product/hotel">HOTEL</a></li>
            </ul>
            <div class="main-action">
                <a href="#"><i class="fa fa-sign-in"></i> LOGIN / DAFTAR</a>
            </div>
        </div>
    </div>
    <nav>
        <a class="nav-close no-loader"><i class="fa fa-chevron-left"></i></a>
        <ul class="nav-wrapper">
            <li class="logo"><img src="<?php echo $assets_url; ?>img/logo.png" alt="TABS"></li>
            <li class="login"><a href="#">LOGIN / DAFTAR</a></li>
            <li class="separator"></li>
            <li><a href="<?php echo $app_url; ?>product/flight" class="no-border"><i class="fa fa-plane"></i> Pesawat</a></li>
            <li><a href="<?php echo $app_url; ?>product/train"><i class="fa fa-train"></i> Kereta</a></li>
            <!-- <li><a href="<?php echo $app_url; ?>product/hotel"><i class="fa fa-hotel"></i> Hotel</a></li> -->
            <li class="separator"></li>
            <li><a href="<?php echo $app_url; ?>help" class="no-border"><i class="fa fa-question-circle"></i> Bantuan</a></li>
            <li><a href="#"><i class="fa fa-check-square-o"></i> Cek Pemesanan</a></li>
            <li class="separator"></li>
            <li><a href="#" class="no-border"><i class="fa fa-sign-out"></i> Logout</a></li>
        </ul>
    </nav>
</header>
