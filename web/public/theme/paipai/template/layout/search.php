<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$search_active = 'flight';

if (isset($document->setting['search']) && ($document->setting['search'] === 'train'))
{
	$search_active = 'train';
}
if (isset($document->setting['search']) && ($document->setting['search'] === 'hotel'))
{
	$search_active = 'hotel';
}
?>
<div class="container">
	<div class="search-wrapper<?php echo (( ! empty($document->setting['search']) && ($document->setting['search'] === TRUE)) ? ' active' : ''); ?>">
		<h4><i class="fa fa-search"></i> PENCARIAN CEPAT</h4>
		<div class="search-form">
			<ul class="search-category clearfix">
				<li><a class="no-loader<?php echo (($search_active == 'flight') ? ' active' : ''); ?>" rel="search-flight" title="Pesawat"><i class="fa fa-plane"></i> Pesawat</a></li>
				<!--<li><a class="no-loader<?php echo (($search_active == 'train') ? ' active' : ''); ?>" rel="search-train" title="Kereta"><i class="fa fa-train"></i> Kereta</a></li>-->
				<li><a class="no-loader<?php echo (($search_active == 'hotel') ? ' active' : ''); ?>" rel="search-hotel" title="Hotel"><i class="fa fa-hotel"></i> Hotel</a></li>
			</ul>
			<form id="form-search-flight" action="<?php echo $app_url; ?>product/flight/submit" method="GET" class="pbottom-20 form-search" <?php echo (($search_active == 'flight') ? '' : ' style="display: none;"'); ?>>
				<input type="hidden" name="redirect" value="<?php echo current_url(); ?>">
				<input type="hidden" name="search" value="flight">
				<div class="box box-white">
					<ul class="route">
						<li>
							<div class="icon"><i class="fa fa-map-marker"></i></div>
							<div class="item bordered">
								<label>Kota Asal</label>
								<input type="hidden" name="origin" value="<?php echo $this->input->get('origin'); ?>">
								<input type="hidden" name="origin_label" value="<?php echo $this->input->get('origin_label'); ?>">
								<input type="text" placeholder="Pilih Kota Asal" class="select-get-origin" value="<?php echo $this->input->get('origin_label'); ?>">
								<div class="selection-container">
									<ul>
										<li class="message"><span class="text-muted">Ketik Kata Kunci</span></li>
									</ul>
								</div>
							</div>
							<a class="switch switch-route no-loader" data-type="flight"><i class="fa fa-refresh"></i></a>
						</li>
						<li>
							<div class="icon"><i class="fa fa-map-marker"></i></div>
							<div class="item">
								<label>Kota Tujuan</label>
								<input type="hidden" name="destination" value="<?php echo $this->input->get('destination'); ?>">
								<input type="hidden" name="destination_label" value="<?php echo $this->input->get('destination_label'); ?>">
								<input type="text" placeholder="Pilih Kota Tujuan" class="select-get-destination" value="<?php echo $this->input->get('destination_label'); ?>">
								<div class="selection-container">
									<ul>
										<li class="message"><span class="text-muted">Ketik Kata Kunci</span></li>
									</ul>
								</div>
							</div>
						</li>
					</ul>
				</div>
				<div class="box box-white mtop-10">
					<div class="date">
						<div class="date-switch">
							<a class="no-loader oneway <?php echo ( ! $this->input->get('return') ? ' active' : ''); ?>" rel="flight">Sekali Jalan</a>
							<a class="no-loader return <?php echo ($this->input->get('return') ? ' active' : ''); ?>" rel="flight">Pulang Pergi</a>
						</div>
						<div class="item mtop-20">
							<label>Tanggal Pergi</label>
							<div class="icon"><i class="fa fa-calendar"></i></div>
							<input type="text" name="depart" class="datepicker date-depart" readonly="readonly" placeholder="Tanggal Pergi" value="<?php echo ($this->input->get('depart') ? date('d-m-Y', strtotime($this->input->get('depart'))) : ''); ?>">
						</div>
						<div class="item mtop-10 return"<?php echo ($this->input->get('return') ? '' : ' style="display: none;"'); ?>>
							<label>Tanggal Pulang</label>
							<div class="icon"><i class="fa fa-calendar"></i></div>
							<input type="text" name="return" class="datepicker date-return" readonly="readonly" placeholder="Tanggal Pulang" value="<?php echo ($this->input->get('return') ? date('d-m-Y', strtotime($this->input->get('return'))) : ''); ?>">
						</div>
					</div>
				</div>
				<div class="box box-white mtop-10">
					<input type="hidden" name="adult" value="<?php echo ($this->input->get('adult') ? (int) $this->input->get('adult') : 1); ?>">
					<input type="hidden" name="child" value="<?php echo (int) $this->input->get('child'); ?>">
					<input type="hidden" name="infant" value="<?php echo (int) $this->input->get('infant'); ?>">
					<ul class="pax clearfix">
						<li>
							<label>Dewasa</label>
							<a class="toggle no-loader"><span><?php echo ($this->input->get('adult') ? (int) $this->input->get('adult') : 1); ?></span> <i class="fa fa-angle-down"></i></a>
							<ul>
								<li><a class="no-loader" data-type="adult" data-value="1">1</a></li>
								<li><a class="no-loader" data-type="adult" data-value="2">2</a></li>
								<li><a class="no-loader" data-type="adult" data-value="3">3</a></li>
								<li><a class="no-loader" data-type="adult" data-value="4">4</a></li>
								<li><a class="no-loader" data-type="adult" data-value="5">5</a></li>
								<li><a class="no-loader" data-type="adult" data-value="6">6</a></li>
								<li><a class="no-loader" data-type="adult" data-value="7">7</a></li>
							</ul>
						</li>
						<li>
							<label>Anak</label>
							<a class="toggle no-loader"><span><?php echo (int) $this->input->get('child'); ?></span> <i class="fa fa-angle-down"></i></a>
							<ul>
								<li><a class="no-loader" data-type="child" data-value="0">0</a></li>
								<li><a class="no-loader" data-type="child" data-value="1">1</a></li>
								<li><a class="no-loader" data-type="child" data-value="2">2</a></li>
								<li><a class="no-loader" data-type="child" data-value="3">3</a></li>
								<li><a class="no-loader" data-type="child" data-value="4">4</a></li>
								<li><a class="no-loader" data-type="child" data-value="5">5</a></li>
								<li><a class="no-loader" data-type="child" data-value="6">6</a></li>
							</ul>
						</li>
						<li>
							<label>Bayi</label>
							<a class="toggle no-loader"><span><?php echo (int) $this->input->get('infant'); ?></span> <i class="fa fa-angle-down"></i></a>
							<ul>
								<li><a class="no-loader" data-type="infant" data-value="0">0</a></li>
								<li><a class="no-loader" data-type="infant" data-value="1">1</a></li>
								<li><a class="no-loader" data-type="infant" data-value="2">2</a></li>
								<li><a class="no-loader" data-type="infant" data-value="3">3</a></li>
								<li><a class="no-loader" data-type="infant" data-value="4">4</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="mtop-10">
					<button type="submit" class="submit"><i class="fa fa-search"></i> CARI</button>
				</div>
			</form>

			<!--<form id="form-search-train" action="<?php echo $app_url; ?>product/train/submit" method="GET" class="pbottom-20 form-search" <?php echo (($search_active == 'train') ? '' : ' style="display: none;"'); ?>>
				<input type="hidden" name="search" value="train">
				<div class="box box-white">
					<ul class="route">
						<li>
							<div class="icon"><i class="fa fa-map-marker"></i></div>
							<div class="item bordered">
								<label>Stasiun Asal</label>
								<input type="hidden" name="from" value="<?php echo $this->input->get('from'); ?>">
								<input type="hidden" name="from_label" value="<?php echo $this->input->get('from_label'); ?>">
								<input type="text" placeholder="Pilih Stasiun Asal" class="select-get-from" value="<?php echo $this->input->get('from_label'); ?>">
								<div class="selection-container">
									<ul>
										<li class="message"><span class="text-muted">Ketik Kata Kunci</span></li>
									</ul>
								</div>
							</div>
							<a class="switch switch-route no-loader" data-type="train"><i class="fa fa-refresh"></i></a>
						</li>
						<li>
							<div class="icon"><i class="fa fa-map-marker"></i></div>
							<div class="item">
								<label>Stasiun Tujuan</label>
								<input type="hidden" name="to" value="<?php echo $this->input->get('to'); ?>">
								<input type="hidden" name="to_label" value="<?php echo $this->input->get('to_label'); ?>">
								<input type="text" placeholder="Pilih Stasiun Tujuan" class="select-get-to" value="<?php echo $this->input->get('to_label'); ?>">
								<div class="selection-container">
									<ul>
										<li class="message"><span class="text-muted">Ketik Kata Kunci</span></li>
									</ul>
								</div>
							</div>
						</li>
					</ul>
				</div>
				<div class="box box-white mtop-10">
					<div class="date">
						<div class="date-switch">
							<a class="no-loader oneway <?php echo ( ! $this->input->get('return') ? ' active' : ''); ?>" rel="train">Sekali Jalan</a>
							<a class="no-loader return <?php echo ($this->input->get('return') ? ' active' : ''); ?>" rel="train">Pulang Pergi</a>
						</div>
						<div class="item mtop-20">
							<label>Tanggal Pergi</label>
							<div class="icon"><i class="fa fa-calendar"></i></div>
							<input type="text" name="depart" class="datepicker date-depart" readonly="readonly" placeholder="Tanggal Pergi" value="<?php echo ($this->input->get('depart') ? date('d-m-Y', strtotime($this->input->get('depart'))) : ''); ?>">
						</div>
						<div class="item mtop-10 return"<?php echo ($this->input->get('return') ? '' : ' style="display: none;"'); ?>>
							<label>Tanggal Pulang</label>
							<div class="icon"><i class="fa fa-calendar"></i></div>
							<input type="text" name="return" class="datepicker date-return" readonly="readonly" placeholder="Tanggal Pulang" value="<?php echo ($this->input->get('return') ? date('d-m-Y', strtotime($this->input->get('return'))) : ''); ?>">
						</div>
					</div>
				</div>
				<div class="box box-white mtop-10">
					<input type="hidden" name="adult" value="<?php echo ($this->input->get('adult') ? (int) $this->input->get('adult') : 1); ?>">
					<input type="hidden" name="infant" value="<?php echo (int) $this->input->get('infant'); ?>">
					<ul class="qty clearfix">
						<li>
							<label>Dewasa</label>
							<a class="toggle no-loader"><span><?php echo ($this->input->get('adult') ? (int) $this->input->get('adult') : 1); ?></span> <i class="fa fa-angle-down"></i></a>
							<ul>
								<li><a class="no-loader" data-type="adult" data-value="1">1</a></li>
								<li><a class="no-loader" data-type="adult" data-value="2">2</a></li>
								<li><a class="no-loader" data-type="adult" data-value="3">3</a></li>
								<li><a class="no-loader" data-type="adult" data-value="4">4</a></li>
								<li><a class="no-loader" data-type="adult" data-value="5">5</a></li>
								<li><a class="no-loader" data-type="adult" data-value="6">6</a></li>
								<li><a class="no-loader" data-type="adult" data-value="7">7</a></li>
							</ul>
						</li>
						<li>
							<label>Bayi</label>
							<a class="toggle no-loader"><span><?php echo (int) $this->input->get('infant'); ?></span> <i class="fa fa-angle-down"></i></a>
							<ul>
								<li><a class="no-loader" data-type="infant" data-value="0">0</a></li>
								<li><a class="no-loader" data-type="infant" data-value="1">1</a></li>
								<li><a class="no-loader" data-type="infant" data-value="2">2</a></li>
								<li><a class="no-loader" data-type="infant" data-value="3">3</a></li>
								<li><a class="no-loader" data-type="infant" data-value="4">4</a></li>
								<li><a class="no-loader" data-type="infant" data-value="5">5</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="mtop-10">
					<button type="submit" class="submit"><i class="fa fa-search"></i> CARI</button>
				</div>
			</form>-->

			 <form id="form-search-hotel" action="<?php echo $app_url; ?>product/hotel/submit" method="GET" class="pbottom-20 form-search" <?php echo (($search_active == 'hotel') ? '' : ' style="display: none;"'); ?>>
				<input type="hidden" name="search" value="hotel">
				<div class="box box-white">
					<div class="default">
						<div class="item">
							<label>Lokasi Menginap</label>
							<div class="icon"><i class="fa fa-map-o"></i></div>
							<input type="text" name="location" class="select-get-hotelpoi" placeholder="Nama Kota, Tempat Wisata, atau Nama Hotel" value="<?php echo ($this->input->get('location') ? $this->input->get('location') : ''); ?>">
							<div class="selection-container">
								<ul>
									<li class="message"><span class="text-muted">Ketik Kata Kunci</span></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="box box-white mtop-10">
					<div class="date">
						<div class="item">
							<label>Tanggal Check-In</label>
							<div class="icon"><i class="fa fa-calendar"></i></div>
							<input type="text" name="checkin" class="datepicker date-checkin" readonly="readonly" placeholder="Tanggal Check-In" value="<?php echo ($this->input->get('checkin') ? date('d-m-Y', strtotime($this->input->get('checkin'))) : ''); ?>">
						</div>
						<div class="item mtop-10">
							<label>Tanggal Check-Out</label>
							<div class="icon"><i class="fa fa-calendar"></i></div>
							<input type="text" name="checkout" class="datepicker date-checkout" readonly="readonly" placeholder="Tanggal Check-Out" value="<?php echo ($this->input->get('checkout') ? date('d-m-Y', strtotime($this->input->get('checkout'))) : ''); ?>">
						</div>
					</div>
				</div>
				<div class="box box-white mtop-10">
					<input type="hidden" name="room" value="<?php echo ($this->input->get('room') ? (int) $this->input->get('room') : 1); ?>">
					<input type="hidden" name="guest" value="<?php echo ($this->input->get('guest') ? (int) $this->input->get('guest') : 1); ?>">
					<ul class="qty clearfix">
						<li>
							<label>Kamar</label>
							<a class="toggle no-loader"><span><?php echo ($this->input->get('room') ? (int) $this->input->get('room') : 1); ?></span> <i class="fa fa-angle-down"></i></a>
							<ul>
								<li><a class="no-loader" data-type="room" data-value="1">1</a></li>
								<li><a class="no-loader" data-type="room" data-value="2">2</a></li>
								<li><a class="no-loader" data-type="room" data-value="3">3</a></li>
								<li><a class="no-loader" data-type="room" data-value="4">4</a></li>
								<li><a class="no-loader" data-type="room" data-value="5">5</a></li>
								<li><a class="no-loader" data-type="room" data-value="6">6</a></li>
								<li><a class="no-loader" data-type="room" data-value="7">7</a></li>
								<li><a class="no-loader" data-type="room" data-value="8">8</a></li>
							</ul>
						</li>
						<li>
							<label>Tamu per Kamar</label>
							<a class="toggle no-loader"><span><?php echo ($this->input->get('guest') ? (int) $this->input->get('guest') : 1); ?></span> <i class="fa fa-angle-down"></i></a>
							<ul>
								<li><a class="no-loader" data-type="guest" data-value="1">1</a></li>
								<li><a class="no-loader" data-type="guest" data-value="2">2</a></li>
								<li><a class="no-loader" data-type="guest" data-value="3">3</a></li>
								<li><a class="no-loader" data-type="guest" data-value="4">4</a></li>
								<li><a class="no-loader" data-type="guest" data-value="5">5</a></li>
								<li><a class="no-loader" data-type="guest" data-value="6">6</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="mtop-10">
					<button type="submit" class="submit"><i class="fa fa-search"></i> CARI</button>
				</div>
			</form>
		</div>
		<a class="search-toggle no-loader"><i class="fa fa-chevron-down"></i></a>
	</div>
</div>
