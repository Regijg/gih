<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container">
	<div class="slider-wrapper">
        <ul class="main-slider">
            <li><img src="<?php echo $storage_url; ?>default/slider/slider-1.jpg"></li>
            <li><img src="<?php echo $storage_url; ?>default/slider/slider-2.jpg"></li>
            <li><img src="<?php echo $storage_url; ?>default/slider/slider-3.jpg"></li>
        </ul>
    </div>
</div>

<div class="container ptop-30 pbottom-30">
	<div class="ads-wrapper">
		<img src="<?php echo $storage_url; ?>default/banner-leaderboard.jpg" alt="">
	</div>
	<div class="box-wrapper mtop-30">
		<div class="box-head">
			<h4><i class="fa fa-circle-o"></i> Destinasi <span>Populer</span></h4>
		</div>
		<div class="box-body">
			<ul class="popular-list clearfix">
				<li>
					<a class="quick-action" rel="flight" data-code="CGK" data-label="JAKARTA, INDONESIA (CGK)">
						<img src="<?php echo $storage_url; ?>default/city/jakarta.jpg" alt="">
						<div class="title">Jakarta</div>
						<div class="desc">
							<i class="fa fa-plane"></i> Mulai dari Rp. 200.000
						</div>
					</a>
				</li>
				<li>
					<a class="quick-action" rel="flight" data-code="BDO" data-label="BANDUNG, INDONESIA (BDO)">
						<img src="<?php echo $storage_url; ?>default/city/bandung.jpg" alt="">
						<div class="title">Bandung</div>
						<div class="desc">
							<i class="fa fa-plane"></i> Mulai dari Rp. 200.000
						</div>
					</a>
				</li>
				<li>
					<a class="quick-action" rel="flight" data-code="JOG" data-label="YOGYAKARTA, INDONESIA (JOG)">
						<img src="<?php echo $storage_url; ?>default/city/yogyakarta.jpg" alt="">
						<div class="title">Yogyakarta</div>
						<div class="desc">
							<i class="fa fa-plane"></i> Mulai dari Rp. 200.000
						</div>
					</a>
				</li>
				<li>
					<a class="quick-action" rel="flight" data-code="DPS" data-label="DENPASAR, INDONESIA (DPS)">
						<img src="<?php echo $storage_url; ?>default/city/bali.jpg" alt="">
						<div class="title">Bali</div>
						<div class="desc">
							<i class="fa fa-plane"></i> Mulai dari Rp. 200.000
						</div>
					</a>
				</li>
				<li>
					<a class="quick-action" rel="flight" data-code="CGK" data-label="JAKARTA, INDONESIA (CGK)">
						<img src="<?php echo $storage_url; ?>default/city/bogor.jpg" alt="">
						<div class="title">Bogor</div>
						<div class="desc">
							<i class="fa fa-plane"></i> Mulai dari Rp. 200.000
						</div>
					</a>
				</li>
				<li>
					<a class="quick-action" rel="flight" data-code="SUB" data-label="SURABAYA, INDONESIA (SUB)">
						<img src="<?php echo $storage_url; ?>default/city/surabaya.jpg" alt="">
						<div class="title">Surabaya</div>
						<div class="desc">
							<i class="fa fa-plane"></i> Mulai dari Rp. 200.000
						</div>
					</a>
				</li>
				<li>
					<a class="quick-action" rel="flight" data-code="MLG" data-label="MALANG, INDONESIA (MLG)">
						<img src="<?php echo $storage_url; ?>default/city/malang.jpg" alt="">
						<div class="title">Malang</div>
						<div class="desc">
							<i class="fa fa-plane"></i> Mulai dari Rp. 200.000
						</div>
					</a>
				</li>
				<li>
					<a class="quick-action" rel="flight" data-code="KNO" data-label="MEDAN, INDONESIA (KNO)">
						<img src="<?php echo $storage_url; ?>default/city/medan.jpg" alt="">
						<div class="title">Medan</div>
						<div class="desc">
							<i class="fa fa-plane"></i> Mulai dari Rp. 200.000
						</div>
					</a>
				</li>
				<li>
					<a class="quick-action" rel="flight" data-code="SRG" data-label="SEMARANG, INDONESIA (SRG)">
						<img src="<?php echo $storage_url; ?>default/city/semarang.jpg" alt="">
						<div class="title">Semarang</div>
						<div class="desc">
							<i class="fa fa-plane"></i> Mulai dari Rp. 200.000
						</div>
					</a>
				</li>
				<li>
					<a class="quick-action" rel="flight" data-code="SOC" data-label="SOLO, INDONESIA (SOC)">
						<img src="<?php echo $storage_url; ?>default/city/solo.jpg" alt="">
						<div class="title">Solo</div>
						<div class="desc">
							<i class="fa fa-plane"></i> Mulai dari Rp. 200.000
						</div>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="box-wrapper mtop-30">
		<div class="box-head">
			<h4><i class="fa fa-circle-o"></i> Mengapa <span>Memilih Kami?</span></h4>
		</div>
		<div class="box-body">
			<ul class="campaign clearfix">
				<li>
					<div class="icon"><i class="lnr lnr-star"></i></div>
					<div class="desc">
						<h4>Produk &amp; Fitur Yang Lengkap</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
					</div>
				</li>
				<li>
					<div class="icon"><i class="lnr lnr-tag"></i></div>
					<div class="desc">
						<h4>Penawaran Harga Terbaik</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
					</div>
				</li>
				<li>
					<div class="icon"><i class="lnr lnr-laptop-phone"></i></div>
					<div class="desc">
						<h4>Tersedia di Berbagai Device</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
					</div>
				</li>
				<li>
					<div class="icon"><i class="lnr lnr-lock"></i></div>
					<div class="desc">
						<h4>Jaminan Transaksi Aman</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
					</div>
				</li>
				<li>
					<div class="icon"><i class="lnr lnr-smile"></i></div>
					<div class="desc">
						<h4>Komitmen Pelayanan Prima</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
					</div>
				</li>
			</ul>
		</div>
	</div>
	<!--
	<div class="box-wrapper mtop-30">
		<div class="box-head">
			<h4><i class="fa fa-circle-o"></i> Partner <span>Maskapai</span></h4>
		</div>
		<div class="box-body">
			<ul class="airlines-list">
				<li><img src="<?php // echo $storage_url; ?>default/airlines/airasia.png" alt=""></li>
				<li><img src="<?php // echo $storage_url; ?>default/airlines/batikair.png" alt=""></li>
				<li><img src="<?php // echo $storage_url; ?>default/airlines/citilink.png" alt=""></li>
				<li><img src="<?php // echo $storage_url; ?>default/airlines/express.png" alt=""></li>
				<li><img src="<?php // echo $storage_url; ?>default/airlines/garuda.png" alt=""></li>
				<li><img src="<?php // echo $storage_url; ?>default/airlines/kalstar.png" alt=""></li>
				<li><img src="<?php // echo $storage_url; ?>default/airlines/lionair.png" alt=""></li>
				<li><img src="<?php // echo $storage_url; ?>default/airlines/namair.png" alt=""></li>
				<li><img src="<?php // echo $storage_url; ?>default/airlines/sriwijaya.png" alt=""></li>
				<li><img src="<?php // echo $storage_url; ?>default/airlines/transnusa.png" alt=""></li>
				<li><img src="<?php // echo $storage_url; ?>default/airlines/trigana.png" alt=""></li>
				<li><img src="<?php // echo $storage_url; ?>default/airlines/wingsair.png" alt=""></li>
			</ul>
		</div>
	</div> -->
</div>

<!-- SLIDER -->
<link href="<?php echo $assets_url; ?>js/bxslider/jquery.bxslider.css" rel="stylesheet">
<script src="<?php echo $assets_url; ?>js/bxslider/jquery.bxslider.min.js" type="text/javascript"></script>
