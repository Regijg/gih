<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="container pbottom-30">
	<div class="info-wrapper">
		<ul class="info-navigation">
			<li><a href="<?php echo $app_url; ?>about" class="<?php echo (($info_type == 'about') ? 'active' : ''); ?>">Tentang Kami</a></li>
			<li><a href="<?php echo $app_url; ?>contact" class="<?php echo (($info_type == 'contact') ? 'active' : ''); ?>">Kontak</a></li>
			<li><a href="<?php echo $app_url; ?>career" class="<?php echo (($info_type == 'career') ? 'active' : ''); ?>">Karir</a></li>
			<li><a href="<?php echo $app_url; ?>terms" class="<?php echo (($info_type == 'terms') ? 'active' : ''); ?>">Syarat &amp; Ketentuan</a></li>
			<li><a href="<?php echo $app_url; ?>privacy" class="<?php echo (($info_type == 'privacy') ? 'active' : ''); ?>">Kebijakan Privasi</a></li>
			<li><a href="<?php echo $app_url; ?>help" class="<?php echo (($info_type == 'help') ? 'active' : ''); ?>">Bantuan</a></li>
		</ul>
		<div id="about" class="info-content" style="<?php echo (($info_type == 'about') ? 'display: block;' : ''); ?>">
			<h1><i class="fa fa-hashtag"></i> Tentang Kami</h1>
			<div class="content-wrapper">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla malesuada dignissim purus, finibus mattis lectus auctor feugiat. Duis nisl eros, dictum vel velit eget, volutpat congue lectus. Integer maximus tempor orci a laoreet. Quisque vel ex justo. Etiam tincidunt arcu et diam fringilla, ac molestie lectus ullamcorper. Maecenas venenatis sollicitudin pellentesque. Aenean et orci eu eros scelerisque molestie. Cras eget enim varius turpis tincidunt congue. Donec pulvinar elit quis dui consequat, nec rutrum magna facilisis. Etiam luctus sapien urna. Nam et ullamcorper orci. Nunc ac vehicula magna. Curabitur molestie id ex maximus luctus. Nullam sodales augue enim. Nulla eget nunc mollis, tristique turpis in, convallis dui.</p>
				<ul>
					<li>Suspendisse quis ullamcorper urna</li>
					<li>Sed ut eros libero. Vivamus volutpat dignissim felis</li>
					<li>Donec cursus lacinia nulla</li>
					<li>Phasellus ullamcorper ipsum lacinia posuere sagittis</li>
					<li>Donec maximus nibh ex, vel interdum tortor consequat id</li>
				</ul>
				<p>Sed in metus malesuada, dapibus ligula eu, suscipit ante. Maecenas tincidunt dui vitae nisi elementum, ac accumsan elit posuere. Vivamus consequat felis quis orci egestas gravida. Nulla facilisi. Ut ultrices elit quis enim volutpat, ut tristique tellus volutpat. Sed sit amet quam in nibh tempus posuere. Mauris rhoncus interdum gravida. Vestibulum quis nulla vitae elit efficitur congue ac at sapien. Cras in risus ac nisi congue consequat quis eu nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed rutrum arcu volutpat, ultrices ipsum ut, auctor ligula. Ut laoreet vulputate quam, in volutpat nibh lacinia sed. Cras commodo augue sed malesuada convallis. Phasellus pulvinar, tortor vel volutpat egestas, mi diam aliquam velit, vel vestibulum erat justo imperdiet nibh.</p>
				<ol>
					<li>Suspendisse quis ullamcorper urna</li>
					<li>Sed ut eros libero. Vivamus volutpat dignissim felis</li>
					<li>Donec cursus lacinia nulla</li>
					<li>Phasellus ullamcorper ipsum lacinia posuere sagittis</li>
					<li>Donec maximus nibh ex, vel interdum tortor consequat id</li>
				</ol>
				<p>Morbi finibus vehicula tempus. Fusce id molestie augue, eget imperdiet sapien. Vivamus varius augue non leo condimentum egestas. Donec sed molestie dolor. Maecenas a magna elementum, vehicula ipsum ac, placerat neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean dui tellus, facilisis sed tincidunt vel, efficitur sed velit. Morbi ac tortor felis. Cras quis hendrerit sapien. Etiam dignissim nibh et odio congue, quis accumsan nisl mollis. Maecenas eu viverra tellus. Vestibulum non purus nec augue pharetra malesuada eget quis nisl. Etiam euismod, lacus nec congue elementum, elit massa consequat felis, vitae pretium mauris orci sit amet odio. Nunc aliquet quam in augue porta luctus. Nulla ornare id erat nec pulvinar. Nunc sit amet eros a neque egestas aliquam.</p>
			</div>
		</div>
		<div id="contact" class="info-content" style="<?php echo (($info_type == 'contact') ? 'display: block;' : ''); ?>">
			<h1><i class="fa fa-book"></i> Kontak</h1>
			<div class="content-wrapper">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla malesuada dignissim purus, finibus mattis lectus auctor feugiat. Duis nisl eros, dictum vel velit eget, volutpat congue lectus. Integer maximus tempor orci a laoreet. Quisque vel ex justo. Etiam tincidunt arcu et diam fringilla, ac molestie lectus ullamcorper. Maecenas venenatis sollicitudin pellentesque. Aenean et orci eu eros scelerisque molestie. Cras eget enim varius turpis tincidunt congue. Donec pulvinar elit quis dui consequat, nec rutrum magna facilisis. Etiam luctus sapien urna. Nam et ullamcorper orci. Nunc ac vehicula magna. Curabitur molestie id ex maximus luctus. Nullam sodales augue enim. Nulla eget nunc mollis, tristique turpis in, convallis dui.</p>
				<ul>
					<li>Suspendisse quis ullamcorper urna</li>
					<li>Sed ut eros libero. Vivamus volutpat dignissim felis</li>
					<li>Donec cursus lacinia nulla</li>
					<li>Phasellus ullamcorper ipsum lacinia posuere sagittis</li>
					<li>Donec maximus nibh ex, vel interdum tortor consequat id</li>
				</ul>
				<p>Sed in metus malesuada, dapibus ligula eu, suscipit ante. Maecenas tincidunt dui vitae nisi elementum, ac accumsan elit posuere. Vivamus consequat felis quis orci egestas gravida. Nulla facilisi. Ut ultrices elit quis enim volutpat, ut tristique tellus volutpat. Sed sit amet quam in nibh tempus posuere. Mauris rhoncus interdum gravida. Vestibulum quis nulla vitae elit efficitur congue ac at sapien. Cras in risus ac nisi congue consequat quis eu nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed rutrum arcu volutpat, ultrices ipsum ut, auctor ligula. Ut laoreet vulputate quam, in volutpat nibh lacinia sed. Cras commodo augue sed malesuada convallis. Phasellus pulvinar, tortor vel volutpat egestas, mi diam aliquam velit, vel vestibulum erat justo imperdiet nibh.</p>
				<ol>
					<li>Suspendisse quis ullamcorper urna</li>
					<li>Sed ut eros libero. Vivamus volutpat dignissim felis</li>
					<li>Donec cursus lacinia nulla</li>
					<li>Phasellus ullamcorper ipsum lacinia posuere sagittis</li>
					<li>Donec maximus nibh ex, vel interdum tortor consequat id</li>
				</ol>
				<p>Morbi finibus vehicula tempus. Fusce id molestie augue, eget imperdiet sapien. Vivamus varius augue non leo condimentum egestas. Donec sed molestie dolor. Maecenas a magna elementum, vehicula ipsum ac, placerat neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean dui tellus, facilisis sed tincidunt vel, efficitur sed velit. Morbi ac tortor felis. Cras quis hendrerit sapien. Etiam dignissim nibh et odio congue, quis accumsan nisl mollis. Maecenas eu viverra tellus. Vestibulum non purus nec augue pharetra malesuada eget quis nisl. Etiam euismod, lacus nec congue elementum, elit massa consequat felis, vitae pretium mauris orci sit amet odio. Nunc aliquet quam in augue porta luctus. Nulla ornare id erat nec pulvinar. Nunc sit amet eros a neque egestas aliquam.</p>
			</div>
		</div>
		<div id="career" class="info-content" style="<?php echo (($info_type == 'career') ? 'display: block;' : ''); ?>">
			<h1><i class="fa fa-users"></i> Karir</h1>
			<div class="content-wrapper">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla malesuada dignissim purus, finibus mattis lectus auctor feugiat. Duis nisl eros, dictum vel velit eget, volutpat congue lectus. Integer maximus tempor orci a laoreet. Quisque vel ex justo. Etiam tincidunt arcu et diam fringilla, ac molestie lectus ullamcorper. Maecenas venenatis sollicitudin pellentesque. Aenean et orci eu eros scelerisque molestie. Cras eget enim varius turpis tincidunt congue. Donec pulvinar elit quis dui consequat, nec rutrum magna facilisis. Etiam luctus sapien urna. Nam et ullamcorper orci. Nunc ac vehicula magna. Curabitur molestie id ex maximus luctus. Nullam sodales augue enim. Nulla eget nunc mollis, tristique turpis in, convallis dui.</p>
				<ul>
					<li>Suspendisse quis ullamcorper urna</li>
					<li>Sed ut eros libero. Vivamus volutpat dignissim felis</li>
					<li>Donec cursus lacinia nulla</li>
					<li>Phasellus ullamcorper ipsum lacinia posuere sagittis</li>
					<li>Donec maximus nibh ex, vel interdum tortor consequat id</li>
				</ul>
				<p>Sed in metus malesuada, dapibus ligula eu, suscipit ante. Maecenas tincidunt dui vitae nisi elementum, ac accumsan elit posuere. Vivamus consequat felis quis orci egestas gravida. Nulla facilisi. Ut ultrices elit quis enim volutpat, ut tristique tellus volutpat. Sed sit amet quam in nibh tempus posuere. Mauris rhoncus interdum gravida. Vestibulum quis nulla vitae elit efficitur congue ac at sapien. Cras in risus ac nisi congue consequat quis eu nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed rutrum arcu volutpat, ultrices ipsum ut, auctor ligula. Ut laoreet vulputate quam, in volutpat nibh lacinia sed. Cras commodo augue sed malesuada convallis. Phasellus pulvinar, tortor vel volutpat egestas, mi diam aliquam velit, vel vestibulum erat justo imperdiet nibh.</p>
				<ol>
					<li>Suspendisse quis ullamcorper urna</li>
					<li>Sed ut eros libero. Vivamus volutpat dignissim felis</li>
					<li>Donec cursus lacinia nulla</li>
					<li>Phasellus ullamcorper ipsum lacinia posuere sagittis</li>
					<li>Donec maximus nibh ex, vel interdum tortor consequat id</li>
				</ol>
				<p>Morbi finibus vehicula tempus. Fusce id molestie augue, eget imperdiet sapien. Vivamus varius augue non leo condimentum egestas. Donec sed molestie dolor. Maecenas a magna elementum, vehicula ipsum ac, placerat neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean dui tellus, facilisis sed tincidunt vel, efficitur sed velit. Morbi ac tortor felis. Cras quis hendrerit sapien. Etiam dignissim nibh et odio congue, quis accumsan nisl mollis. Maecenas eu viverra tellus. Vestibulum non purus nec augue pharetra malesuada eget quis nisl. Etiam euismod, lacus nec congue elementum, elit massa consequat felis, vitae pretium mauris orci sit amet odio. Nunc aliquet quam in augue porta luctus. Nulla ornare id erat nec pulvinar. Nunc sit amet eros a neque egestas aliquam.</p>
			</div>
		</div>
		<div id="terms" class="info-content" style="<?php echo (($info_type == 'terms') ? 'display: block;' : ''); ?>">
			<h1><i class="fa fa-warning"></i> Syarat &amp; Ketentuan</h1>
			<div class="content-wrapper">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla malesuada dignissim purus, finibus mattis lectus auctor feugiat. Duis nisl eros, dictum vel velit eget, volutpat congue lectus. Integer maximus tempor orci a laoreet. Quisque vel ex justo. Etiam tincidunt arcu et diam fringilla, ac molestie lectus ullamcorper. Maecenas venenatis sollicitudin pellentesque. Aenean et orci eu eros scelerisque molestie. Cras eget enim varius turpis tincidunt congue. Donec pulvinar elit quis dui consequat, nec rutrum magna facilisis. Etiam luctus sapien urna. Nam et ullamcorper orci. Nunc ac vehicula magna. Curabitur molestie id ex maximus luctus. Nullam sodales augue enim. Nulla eget nunc mollis, tristique turpis in, convallis dui.</p>
				<ul>
					<li>Suspendisse quis ullamcorper urna</li>
					<li>Sed ut eros libero. Vivamus volutpat dignissim felis</li>
					<li>Donec cursus lacinia nulla</li>
					<li>Phasellus ullamcorper ipsum lacinia posuere sagittis</li>
					<li>Donec maximus nibh ex, vel interdum tortor consequat id</li>
				</ul>
				<p>Sed in metus malesuada, dapibus ligula eu, suscipit ante. Maecenas tincidunt dui vitae nisi elementum, ac accumsan elit posuere. Vivamus consequat felis quis orci egestas gravida. Nulla facilisi. Ut ultrices elit quis enim volutpat, ut tristique tellus volutpat. Sed sit amet quam in nibh tempus posuere. Mauris rhoncus interdum gravida. Vestibulum quis nulla vitae elit efficitur congue ac at sapien. Cras in risus ac nisi congue consequat quis eu nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed rutrum arcu volutpat, ultrices ipsum ut, auctor ligula. Ut laoreet vulputate quam, in volutpat nibh lacinia sed. Cras commodo augue sed malesuada convallis. Phasellus pulvinar, tortor vel volutpat egestas, mi diam aliquam velit, vel vestibulum erat justo imperdiet nibh.</p>
				<ol>
					<li>Suspendisse quis ullamcorper urna</li>
					<li>Sed ut eros libero. Vivamus volutpat dignissim felis</li>
					<li>Donec cursus lacinia nulla</li>
					<li>Phasellus ullamcorper ipsum lacinia posuere sagittis</li>
					<li>Donec maximus nibh ex, vel interdum tortor consequat id</li>
				</ol>
				<p>Morbi finibus vehicula tempus. Fusce id molestie augue, eget imperdiet sapien. Vivamus varius augue non leo condimentum egestas. Donec sed molestie dolor. Maecenas a magna elementum, vehicula ipsum ac, placerat neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean dui tellus, facilisis sed tincidunt vel, efficitur sed velit. Morbi ac tortor felis. Cras quis hendrerit sapien. Etiam dignissim nibh et odio congue, quis accumsan nisl mollis. Maecenas eu viverra tellus. Vestibulum non purus nec augue pharetra malesuada eget quis nisl. Etiam euismod, lacus nec congue elementum, elit massa consequat felis, vitae pretium mauris orci sit amet odio. Nunc aliquet quam in augue porta luctus. Nulla ornare id erat nec pulvinar. Nunc sit amet eros a neque egestas aliquam.</p>
			</div>
		</div>
		<div id="privacy" class="info-content" style="<?php echo (($info_type == 'privacy') ? 'display: block;' : ''); ?>">
			<h1><i class="fa fa-warning"></i> Kebijakan Privasi</h1>
			<div class="content-wrapper">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla malesuada dignissim purus, finibus mattis lectus auctor feugiat. Duis nisl eros, dictum vel velit eget, volutpat congue lectus. Integer maximus tempor orci a laoreet. Quisque vel ex justo. Etiam tincidunt arcu et diam fringilla, ac molestie lectus ullamcorper. Maecenas venenatis sollicitudin pellentesque. Aenean et orci eu eros scelerisque molestie. Cras eget enim varius turpis tincidunt congue. Donec pulvinar elit quis dui consequat, nec rutrum magna facilisis. Etiam luctus sapien urna. Nam et ullamcorper orci. Nunc ac vehicula magna. Curabitur molestie id ex maximus luctus. Nullam sodales augue enim. Nulla eget nunc mollis, tristique turpis in, convallis dui.</p>
				<ul>
					<li>Suspendisse quis ullamcorper urna</li>
					<li>Sed ut eros libero. Vivamus volutpat dignissim felis</li>
					<li>Donec cursus lacinia nulla</li>
					<li>Phasellus ullamcorper ipsum lacinia posuere sagittis</li>
					<li>Donec maximus nibh ex, vel interdum tortor consequat id</li>
				</ul>
				<p>Sed in metus malesuada, dapibus ligula eu, suscipit ante. Maecenas tincidunt dui vitae nisi elementum, ac accumsan elit posuere. Vivamus consequat felis quis orci egestas gravida. Nulla facilisi. Ut ultrices elit quis enim volutpat, ut tristique tellus volutpat. Sed sit amet quam in nibh tempus posuere. Mauris rhoncus interdum gravida. Vestibulum quis nulla vitae elit efficitur congue ac at sapien. Cras in risus ac nisi congue consequat quis eu nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed rutrum arcu volutpat, ultrices ipsum ut, auctor ligula. Ut laoreet vulputate quam, in volutpat nibh lacinia sed. Cras commodo augue sed malesuada convallis. Phasellus pulvinar, tortor vel volutpat egestas, mi diam aliquam velit, vel vestibulum erat justo imperdiet nibh.</p>
				<ol>
					<li>Suspendisse quis ullamcorper urna</li>
					<li>Sed ut eros libero. Vivamus volutpat dignissim felis</li>
					<li>Donec cursus lacinia nulla</li>
					<li>Phasellus ullamcorper ipsum lacinia posuere sagittis</li>
					<li>Donec maximus nibh ex, vel interdum tortor consequat id</li>
				</ol>
				<p>Morbi finibus vehicula tempus. Fusce id molestie augue, eget imperdiet sapien. Vivamus varius augue non leo condimentum egestas. Donec sed molestie dolor. Maecenas a magna elementum, vehicula ipsum ac, placerat neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean dui tellus, facilisis sed tincidunt vel, efficitur sed velit. Morbi ac tortor felis. Cras quis hendrerit sapien. Etiam dignissim nibh et odio congue, quis accumsan nisl mollis. Maecenas eu viverra tellus. Vestibulum non purus nec augue pharetra malesuada eget quis nisl. Etiam euismod, lacus nec congue elementum, elit massa consequat felis, vitae pretium mauris orci sit amet odio. Nunc aliquet quam in augue porta luctus. Nulla ornare id erat nec pulvinar. Nunc sit amet eros a neque egestas aliquam.</p>
			</div>
		</div>
		<div id="help" class="info-content" style="<?php echo (($info_type == 'help') ? 'display: block;' : ''); ?>">
			<h1><i class="fa fa-question-circle-o"></i> Bantuan</h1>
			<div class="content-wrapper">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla malesuada dignissim purus, finibus mattis lectus auctor feugiat. Duis nisl eros, dictum vel velit eget, volutpat congue lectus. Integer maximus tempor orci a laoreet. Quisque vel ex justo. Etiam tincidunt arcu et diam fringilla, ac molestie lectus ullamcorper. Maecenas venenatis sollicitudin pellentesque. Aenean et orci eu eros scelerisque molestie. Cras eget enim varius turpis tincidunt congue. Donec pulvinar elit quis dui consequat, nec rutrum magna facilisis. Etiam luctus sapien urna. Nam et ullamcorper orci. Nunc ac vehicula magna. Curabitur molestie id ex maximus luctus. Nullam sodales augue enim. Nulla eget nunc mollis, tristique turpis in, convallis dui.</p>
				<ul>
					<li>Suspendisse quis ullamcorper urna</li>
					<li>Sed ut eros libero. Vivamus volutpat dignissim felis</li>
					<li>Donec cursus lacinia nulla</li>
					<li>Phasellus ullamcorper ipsum lacinia posuere sagittis</li>
					<li>Donec maximus nibh ex, vel interdum tortor consequat id</li>
				</ul>
				<p>Sed in metus malesuada, dapibus ligula eu, suscipit ante. Maecenas tincidunt dui vitae nisi elementum, ac accumsan elit posuere. Vivamus consequat felis quis orci egestas gravida. Nulla facilisi. Ut ultrices elit quis enim volutpat, ut tristique tellus volutpat. Sed sit amet quam in nibh tempus posuere. Mauris rhoncus interdum gravida. Vestibulum quis nulla vitae elit efficitur congue ac at sapien. Cras in risus ac nisi congue consequat quis eu nunc. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed rutrum arcu volutpat, ultrices ipsum ut, auctor ligula. Ut laoreet vulputate quam, in volutpat nibh lacinia sed. Cras commodo augue sed malesuada convallis. Phasellus pulvinar, tortor vel volutpat egestas, mi diam aliquam velit, vel vestibulum erat justo imperdiet nibh.</p>
				<ol>
					<li>Suspendisse quis ullamcorper urna</li>
					<li>Sed ut eros libero. Vivamus volutpat dignissim felis</li>
					<li>Donec cursus lacinia nulla</li>
					<li>Phasellus ullamcorper ipsum lacinia posuere sagittis</li>
					<li>Donec maximus nibh ex, vel interdum tortor consequat id</li>
				</ol>
				<p>Morbi finibus vehicula tempus. Fusce id molestie augue, eget imperdiet sapien. Vivamus varius augue non leo condimentum egestas. Donec sed molestie dolor. Maecenas a magna elementum, vehicula ipsum ac, placerat neque. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean dui tellus, facilisis sed tincidunt vel, efficitur sed velit. Morbi ac tortor felis. Cras quis hendrerit sapien. Etiam dignissim nibh et odio congue, quis accumsan nisl mollis. Maecenas eu viverra tellus. Vestibulum non purus nec augue pharetra malesuada eget quis nisl. Etiam euismod, lacus nec congue elementum, elit massa consequat felis, vitae pretium mauris orci sit amet odio. Nunc aliquet quam in augue porta luctus. Nulla ornare id erat nec pulvinar. Nunc sit amet eros a neque egestas aliquam.</p>
			</div>
		</div>
	</div>
</div>
