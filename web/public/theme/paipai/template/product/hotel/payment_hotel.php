<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container pbottom-30">
	<div class="checkout-wrapper clearfix hotel">
		<div class="cart-wrapper mbottom-20">
			<h1>Total Pembayaran</h1>
			<?php
				$total_amount = 0;
				if ( ! empty($book['hotel']['room']['detail_price']['total_price']))
				{
					$total_amount = $total_amount + (float) $book['hotel']['room']['detail_price']['total_price'];
				}
			?>
			<div class="mtop-20 notification text-center text-small">
				<h4><small>IDR</small> <?php echo set_number_format($total_amount, 0); ?></h4>
				<p><span class="text-error"><i class="fa fa-exclamation-triangle"></i></span> Pastikan Anda segera melakukan pembayaran sesuai nilai yang tertera di atas.</p>
			</div>
			
			<h1>Rincian Pesanan</h1>
			<?php if ( ! empty($book['hotel']['room'])) { ?>
				<?php $room = $book['hotel']['room']; ?>
				<div class="cart-item has-image">
					<div class="clearfix">
						<div class="cart-image">
						<?php if ( ! empty($room['thumbNailUrl'])) { ?>
							<img src="<?php echo $room['thumbNailUrl']; ?>" alt="<?php echo $room['hotelName']; ?>">
						<?php } else { ?>
							<i class="fa fa-building"></i>
						<?php } ?>
						</div>
						<div class="cart-description">
							<ul class="detail-list">
								<li class="active clearfix">
									<div class="desc">
										<h5><?php echo $room['room_name']; ?></h5>
										<p><?php echo $room['hotelName']; ?></p>
									</div>
								</li>
								<li class="clearfix">
									<div class="icon"><i class="fa fa-calendar-o"></i></div>
									<div class="schedule">
										<?php echo date('d M Y', strtotime($room['check_in'])); ?> (Check-In)
									</div>
								</li>
								<li class="clearfix">
									<div class="icon"><i class="fa fa-calendar-o"></i></div>
									<div class="schedule">
										<?php echo date('d M Y', strtotime($room['check_out'])); ?> (Check-Out)
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="cart-notes"><?php echo $room['cancelTitle']; ?></div>
					<?php if ( ! empty($room['detail_price'])) { ?>
					<ul class="cart-price mtop-10">
						<?php if ( ! empty($room['detail_price']['basic_price'])) { ?>
						<li class="clearfix">
							<div class="price-title">Basic Price</div>
							<div class="price-amount"><small><?php echo ( ! empty($room['detail_price']['currency_code']) ? $room['detail_price']['currency_code'] : 'IDR'); ?></small> <?php echo set_number_format($room['detail_price']['basic_price'], 0); ?></div>
						</li>
						<?php } ?>
						<?php if ( ! empty($room['detail_price']['fee_and_tax'])) { ?>
						<li class="clearfix">
							<div class="price-title">Tax and Service</div>
							<div class="price-amount"><small><?php echo ( ! empty($room['detail_price']['currency_code']) ? $room['detail_price']['currency_code'] : 'IDR'); ?></small> <?php echo set_number_format($room['detail_price']['fee_and_tax'], 0); ?></div>
						</li>
						<?php } ?>
						<?php if ( ! empty($room['detail_price']['extra_guest_fee'])) { ?>
						<li class="clearfix">
							<div class="price-title">Extra Guest Free</div>
							<div class="price-amount"><small><?php echo ( ! empty($room['detail_price']['currency_code']) ? $room['detail_price']['currency_code'] : 'IDR'); ?></small> <?php echo set_number_format($room['detail_price']['extra_guest_fee'], 0); ?></div>
						</li>
						<?php } ?>
					</ul>
					<?php } ?>
				</div>
			<?php } ?>
		</div>
    	<div class="input-wrapper">
			<h1>Metode Pembayaran</h1>
			<div class="mtop-20 notification text-small">
				<p><span class="text-error"><i class="fa fa-exclamation-triangle"></i></span> Silahkan pilih metode pembayaran dibawah ini.</p>
			</div>
			<div class="payment-method">
				<ul class="clearfix">
				    <li>
				    	<a rel="payment-tabs-1" class="link-button small block no-loader">Instant Payment</a>
						<div id="payment-tabs-1" class="payment-tabs">
							<ul class="payment-method-item">
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>BCA KlikPay - Klik BCA Individu / Kartu Kredit BCA Card</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/bank-bca.jpg" alt="Bank BCA" />
										<img src="<?php echo $assets_url; ?>img/payment/channel-bcaklikpay.jpg" alt="BCA Klikpay" />
									</div>
								</li>
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>Mandiri Clickpay - Kartu Debit Mandiri</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/bank-mandiri.jpg" alt="Bank Mandiri" />
										<img src="<?php echo $assets_url; ?>img/payment/channel-mandiriclickpay.jpg" alt="Mandiri clickpay" />
									</div>
								</li>
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>e-Pay BRI - Kartu Debit BRI</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/bank-bri.jpg" alt="Bank BRI" />
										<img src="<?php echo $assets_url; ?>img/payment/channel-epaybri.jpg" alt="e-Pay BRI" />
									</div>
								</li>
							</ul>
						</div>
				    </li>
				    <li>
				    	<a rel="payment-tabs-2" class="link-button small block outline gray no-loader">Credit Card</a>
						<div id="payment-tabs-2" class="payment-tabs" style="display: none;">
							<ul class="payment-method-item">
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>Kartu Kredit Visa / Mastercard / JCB semua Bank di Indonesia</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/card-visa.jpg" alt="Visa" />
										<img src="<?php echo $assets_url; ?>img/payment/card-master.jpg" alt="MasterCard" />
										<img src="<?php echo $assets_url; ?>img/payment/card-jcb.jpg" alt="JCB" />
									</div>
								</li>
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>Kartu Kredit Mandiri Visa / Mastercard</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/bank-mandiri.jpg" alt="Bank Mandiri" />
										<img src="<?php echo $assets_url; ?>img/payment/card-visamaster.jpg" alt="Visa MasterCard" />
									</div>
								</li>
							</ul>
						</div>
				    </li>
				    <li>
				    	<a rel="payment-tabs-3" class="link-button small block outline gray no-loader">e-Wallet</a>
						<div id="payment-tabs-3" class="payment-tabs" style="display: none;">
							<ul class="payment-method-item">
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>Saldo Wallet / Kartu Kredit untuk anggota DOKU Wallet</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/channel-dokuwallet.jpg" alt="Doku Wallet" />
										<img src="<?php echo $assets_url; ?>img/payment/card-visamaster.jpg" alt="Visa MasterCard" />
									</div>
								</li>
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>XL Tunai</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/channel-xltunai.jpg" alt="XL Tunai" />
									</div>
								</li>
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>Telkomsel T-Cash</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/channel-tcash.jpg" alt="Telkomsel T-Cash" />
									</div>
								</li>
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>Indosat Dompetku</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/channel-dompetku.jpg" alt="Indosat Dompetku" />
									</div>
								</li>
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>Mandiri e-cash</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/channel-mandiriecash.jpg" alt="Mandiri e-cash" />
									</div>
								</li>
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>BCA Sakuku</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/channel-bcasakuku.jpg" alt="BCA Sakuku" />
									</div>
								</li>
							</ul>
						</div>
				    </li>
				    <li>
				    	<a rel="payment-tabs-4" class="link-button small block outline gray no-loader">Transfer</a>
						<div id="payment-tabs-4" class="payment-tabs" style="display: none;">
							<ul class="payment-method-item">
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>Transfer ke Rekening BCA, Mandiri, BRI, BNI, Permata, atau CIMB Niaga</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/transfer-bca.jpg" alt="Bank BCA" />
										<img src="<?php echo $assets_url; ?>img/payment/transfer-mandiri.jpg" alt="Bank Mandiri" />
										<img src="<?php echo $assets_url; ?>img/payment/transfer-bri.jpg" alt="Bank BRI" />
										<img src="<?php echo $assets_url; ?>img/payment/transfer-bni.jpg" alt="Bank BNI" />
									</div>
									<div class="description">
										<ul class="transfer-description clearfix">
											<li>
												<h4>Bank BCA</h4>
												<p>
												No. Rekening : <strong>123.456.7890</strong><br />
												a/n : <strong>PT TABS Indonesia</strong><br />
												Cabang : Bandung
												</p>
											</li>
											<li>
												<h4>Bank Mandiri</h4>
												<p>
												No. Rekening : <strong>123-00-4567890-1</strong><br />
												a/n : <strong>PT TABS Indonesia</strong><br />
												Cabang : Bandung
												</p>
											</li>
											<li>
												<h4>Bank BRI</h4>
												<p>
												No. Rekening : <strong>1234-01-123456-78-9</strong><br />
												a/n : <strong>PT TABS Indonesia</strong><br />
												Cabang : Bandung
												</p>
											</li>
											<li>
												<h4>Bank BNI</h4>
												<p>
												No. Rekening : <strong>123456789</strong><br />
												a/n : <strong>PT TABS Indonesia</strong><br />
												Cabang : Bandung
												</p>
											</li>
										</ul>
									</div>
								</li>
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>Permata Virtual Account menggunakan ATM jaringan Bersama, PRIMA, dan ALTO</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/transfer-permata.jpg" alt="Permata Bank" />
										<img src="<?php echo $assets_url; ?>img/payment/network-all.jpg" alt="Jaringan" />
									</div>
								</li>
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>BCA Virtual Account</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/bank-bca.jpg" alt="Bank BCA" />
									</div>
								</li>
							</ul>
						</div>
				    </li>
				    <li>
				    	<a rel="payment-tabs-5" class="link-button small block outline gray no-loader">Mini Market</a>
				    	<div id="payment-tabs-5" class="payment-tabs" style="display: none;">
							<ul class="payment-method-item">
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>Mini Market / Modern Retail</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/channel-alfamart.jpg" alt="Alfamart" />
										<img src="<?php echo $assets_url; ?>img/payment/channel-indomaret.jpg" alt="Indomaret" />
										<img src="<?php echo $assets_url; ?>img/payment/channel-posindonesia.jpg" alt="POS Indonesia" />
										<img src="<?php echo $assets_url; ?>img/payment/channel-pegadaian.jpg" alt="Pegadaian" />
									</div>
								</li>
							</ul>
						</div>
				    </li>
				</ul>
			</div>
		</div>
	</div>
</div>