<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php foreach ($search_result as $key => $value) { ?>
	<?php if ( ! empty($value['price'])) { ?>
	<li id="<?php echo strtolower($value['hotelId']) . '-' . $key; ?>" class="hotel-item" data-name="<?php echo url_title(str_replace('/', '-', $value['name']), 'dash', TRUE); ?>" data-price="<?php echo (float) $value['price']; ?>" data-availability="<?php echo ( ! empty($value['room_info']) && ! empty($value['room_info']['numberOfRoomsLeftForUrgencyMsg']) ? (int) $value['room_info']['numberOfRoomsLeftForUrgencyMsg'] : 0); ?>" data-distance="<?php echo ( ! empty($value['location_info']) && ! empty($value['location_info']['distanceInKilometers']) ? (float) $value['location_info']['distanceInKilometers'] : 0); ?>" style="display: none;">
		<div class="preview clearfix">
			<div class="hotel-image">
				<?php if ( ! empty($value['largeThumbnailURL'])) { ?>
					<img src="<?php echo $value['largeThumbnailURL']; ?>" alt="<?php echo $value['name']; ?>">
				<?php } else { ?>
					<i class="fa fa-hotel"></i>
				<?php } ?>
			</div>
			<div class="hotel-desc">
				<h4 class="hotel-name"><?php echo $value['name']; ?></h4>
				<?php if ( ! empty($value['room_info']) && ! empty($value['room_info']['textOfRoomsLeftForUrgencyMsg'])) { ?>
					<p class="availability text-success"><?php echo $value['room_info']['textOfRoomsLeftForUrgencyMsg']; ?></p>
				<?php } ?>
				<?php if ( ! empty($value['location_info']) && ! empty($value['location_info']['locationDescription'])) { ?>
					<p><?php echo $value['location_info']['locationDescription']; ?></p>
				<?php } ?>
				<div class="hotel-location clearfix">
					<i class="fa fa-map-marker"></i> <?php echo $value['cityName']; ?>
					<?php if ( ! empty($value['location_info']) && ! empty($value['location_info']['distanceInKilometers'])) { ?>
					&mdash; (<?php echo $value['location_info']['distanceInKilometers']; ?> KM)
					<?php } ?>
					<?php if ( ! empty($value['location_info']) && ! empty($value['location_info']['longitude']) && ! empty($value['location_info']['latitude'])) { ?>
						<button type="button" class="mtop-10 link-button gray extra-small outline" data-longitude="<?php echo $value['location_info']['longitude']; ?>" data-latitude="<?php echo $value['location_info']['latitude']; ?>" onclick="openMap(this);">Lihat Peta Lokasi</button>
					<?php } ?>
				</p>
			</div>
		</div>
		<div class="hotel-price clearfix">
			<ul class="hotel-detail-price">
				<li class="clearfix">
					<label>Basic</label>
					<p><small>IDR</small> <?php echo set_number_format($value['price_info']['basic_price'], 0); ?></p>
				</li>
				<li class="clearfix">
					<label>Tax and Service</label>
					<p><small>IDR</small> <?php echo set_number_format($value['price_info']['fee_and_tax'], 0); ?></p>
				</li>
			</ul>
			<div class="hotel-total-price">
				<div class="clearfix">
					<label>Total Price</label>
					<p><small>IDR</small> <?php echo set_number_format($value['price'], 0, '', '', 'i'); ?></p>
				</div>
				<div class="price-notes">
					<span>*Rata-Rata Permalam</span> &amp; 
					<?php if (isset($value['freeCancellation']) && ($value['freeCancellation'] == 'yes')) { ?>
						<span class="text-success">Refundable</span>
                    <?php } else { ?>
						<span class="text-error">*No Refund</span>
                    <?php } ?>
				</div>
			</div>
		</div>
		<div class="action clearfix">
			<button type="button" class="link-button block action set-hotel" data-ref="<?php echo $search_reference; ?>" data-hotel-id="<?php echo $value['hotelId']; ?>" data-id="<?php echo strtolower($value['hotelId']) . '-' . $key; ?>" onclick="showDetailHotel(this);">PILIH</button>
		<?php if ( ! empty($value['room_info']) && ! empty($value['room_info']['numberOfRoomsLeftForUrgencyMsg'])) { ?>
			<button type="button" class="link-button block action set-hotel" data-ref="<?php echo $search_reference; ?>" data-hotel-id="<?php echo $value['hotelId']; ?>" data-id="<?php echo strtolower($value['hotelId']) . '-' . $key; ?>" onclick="showDetailHotel(this);">PILIH</button>
		<?php } else { ?>
			<!--<div class="soldout">SOLDOUT</div>-->
		<?php } ?>
		</div>
	</li>
	<?php } ?>
<?php } ?>