<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container pbottom-30">
	<div class="search-result flight">
		<div class="result-head mbottom-10">
			<i class="fa fa-hotel"></i>
			<h1><?php echo $this->input->get('location'); ?></h1>
			<p>
			<?php if ($this->input->get('checkin')) { ?>
				<?php echo set_date_format($this->input->get('checkin')); ?>
			<?php } ?>
			<?php if ($this->input->get('checkout')) { ?>
				- <?php echo set_date_format($this->input->get('checkout')); ?>
			<?php } ?>
			<?php if ($this->input->get('room')) { ?>
				&bull; <?php echo (int) $this->input->get('room'); ?> Kamar
			<?php } ?>
			<?php if ($this->input->get('guest')) { ?>
				&bull; <?php echo (int) $this->input->get('guest'); ?> Tamu per Kamar
			<?php } ?>
			</p>
			<hr>
			<span class="text-small"><i class="fa fa-question-circle"></i> Untuk hasil lebih akurat silahkan masukkan kata kunci yang sesuai &amp; benar.</span>
			
			<?php if ( ! empty($app['type']) && ($app['type'] == 'full')) {  ?>
				<button type="button" class="link-button gray result-change" rel="search-hotel"><i class="fa fa-refresh"></i> &nbsp;Ganti Pencarian</button>
			<?php } else { ?>
				<a href="<?php echo $app_url; ?>product/hotel" class="link-button gray result-change"><i class="fa fa-refresh"></i> &nbsp;Ganti Pencarian</a>
			<?php } ?>
		</div>
		<div class="mtop-20 result-loader"><div></div></div>
		<div class="result-body clearfix">
			<div id="hotel-result" class="result-wrapper active">
				<div class="result-items ptop-10">
					<div class="search-loader mbottom-20">Sedang mencari data</div>
					<div class="result-filter">
						<div class="hotel-filter">
							<h5><i class="fa fa-filter"></i> Filter:</h5>
							<input type="text" name="filter_name" placeholder="Berdasarkan Nama Hotel" value="">
						</div>
						<ul class="hotel-sort clearfix">
							<li><a class="no-loader" data-direction="asc" rel="price">Harga <i class="fa fa-sort"></i></a></li>
							<li><a class="no-loader" data-direction="asc" rel="availability">Kamar <i class="fa fa-sort"></i></a></li>
							<li><a class="no-loader" data-direction="asc" rel="name">Nama <i class="fa fa-sort"></i></a></li>
							<li><a class="no-loader active" data-direction="desc" rel="distance">Jarak <i class="fa fa-sort-up"></i></a></li>
						</ul>
					</div>
					<ul class="hotel-list">
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="search-data" class="site-metadata" search-url="<?php echo $search_url; ?>" search-reference="" search-room="<?php echo (int) $this->input->get('room'); ?>" search-page="0" search-loaded="0"></div>