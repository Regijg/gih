<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container pbottom-30">
	<div class="checkout-wrapper clearfix hotel">
    	<form id="form-checkout" class="default-form" method="POST" action="<?php echo $app_url; ?>checkout/booking/hotel" enctype="multipart/form-data">
			<div class="input-wrapper">
				<h1>Informasi Pelanggan</h1>
				<div class="form-group">
					<div class="form-item">
						<input type="text" name="cust_name" class="input-item" placeholder="Nama">
					</div>
					<div class="form-item">
						<input type="email" name="cust_email" class="input-item" placeholder="Email">
					</div>
					<div class="form-item">
						<input type="number" name="cust_phone" class="input-item" placeholder="No Telp">
					</div>
					<div class="mtop-20 mbottom-10 notification text-small">
						<p><span class="text-error"><i class="fa fa-exclamation-triangle"></i></span> Pastikan data pelanggan diisi dengan benar. <strong>e-Voucher &amp; Informasi Pemesanan</strong> akan dikirim ke email di atas.</p>
					</div>
				</div>
				<h1 class="mtop-20">Informasi Tamu/Pengunjung</h1>
				<?php $guest = 1; ?>
				<?php if ( ! empty($cart['hotel']['params']['room_booking'])) { ?>
					<?php for ($i = 0; $i < $cart['hotel']['params']['room_booking']; $i++) { ?>
					<div class="form-group">
						<div class="group-title">Tamu/Pengunjung Kamar #<?php echo $guest; ?></div>
						<div class="form-item">
							<select name="guest_title_<?php echo $guest; ?>" class="input-item">
								<option selected disabled>Titel</option>
								<option value="mr">Tuan</option>
								<option value="ms">Nona</option>
								<option value="mrs">Nyonya</option>
							</select>
						</div>
						<div class="form-item">
							<input type="text" name="guest_fullname_<?php echo $guest; ?>" class="input-item" placeholder="Nama Lengkap">
						</div>
						<?php $guest++; ?>
					</div>
					<?php } ?>
				<?php } ?>

				<div class="mtop-20 notification text-small">
					<p><span class="text-error"><i class="fa fa-exclamation-triangle"></i></span> Pastikan data penumpang diisi dengan benar. <strong>Klaim Kamar</strong> berdasarkan data yang dimasukkan di atas.</p>
				</div>
			</div>
			<div class="cart-wrapper">
				<h1>Rincian Pesanan</h1>
				<?php if ( ! empty($cart['hotel']['room'])) { ?>
					<?php $room = $cart['hotel']['room']; ?>
					<div class="cart-item has-image">
						<div class="clearfix">
							<div class="cart-image">
							<?php if ( ! empty($room['thumbNailUrl'])) { ?>
								<img src="<?php echo $room['thumbNailUrl']; ?>" alt="<?php echo $room['hotelName']; ?>">
							<?php } else { ?>
								<i class="fa fa-building"></i>
							<?php } ?>
							</div>
							<div class="cart-description">
								<ul class="detail-list">
									<li class="active clearfix">
										<div class="desc">
											<h5><?php echo $room['name_room']; ?></h5>
											<p><?php echo $room['name_hotel']; ?></p>
										</div>
									</li>
									<li class="clearfix">
										<div class="icon"><i class="fa fa-calendar-o"></i></div>
										<div class="schedule">
											<?php echo date('d M Y', strtotime($room['ci'])); ?> (Check-In)
										</div>
									</li>
									<li class="clearfix">
										<div class="icon"><i class="fa fa-calendar-o"></i></div>
										<div class="schedule">
											<?php echo date('d M Y', strtotime($room['co'])); ?> (Check-Out)
										</div>
									</li>
								</ul>
							</div>
						</div>
						<div class="cart-notes"><?php echo $room['cancelTitle']; ?></div>
						<?php if ( ! empty($room['room_price'])) { ?>
						<ul class="cart-price mtop-10">
							<?php if ( ! empty($room['room_price']['basic_price'])) { ?>
							<li class="clearfix">
								<div class="price-title">Basic Price</div>
								<div class="price-amount"><small><?php echo ( ! empty($room['room_price']['currency_code']) ? $room['room_price']['currency_code'] : 'IDR'); ?></small> <?php echo set_number_format($room['room_price']['basic_price'], 0); ?></div>
							</li>
							<?php } ?>
							<?php if ( ! empty($room['room_price']['fee_and_tax'])) { ?>
							<li class="clearfix">
								<div class="price-title">Tax and Service</div>
								<div class="price-amount"><small><?php echo ( ! empty($room['room_price']['currency_code']) ? $room['room_price']['currency_code'] : 'IDR'); ?></small> <?php echo set_number_format($room['room_price']['fee_and_tax'], 0); ?></div>
							</li>
							<?php } ?>
							<?php if ( ! empty($room['room_price']['extra_guest_fee'])) { ?>
							<li class="clearfix">
								<div class="price-title">Extra Guest Free</div>
								<div class="price-amount"><small><?php echo ( ! empty($room['room_price']['currency_code']) ? $room['room_price']['currency_code'] : 'IDR'); ?></small> <?php echo set_number_format($room['room_price']['extra_guest_fee'], 0); ?></div>
							</li>
							<?php } ?>
						</ul>
						<?php } ?>
					</div>
				<?php } ?>

				<hr class="mtop-20">
				<div>
				<?php
					$total_amount = 0;
					if ( ! empty($cart['hotel']['room']['room_price']['total_price']))
					{
						$total_amount = $total_amount + (float) $cart['hotel']['room']['room_price']['total_price'];
					}
				?>
					<div class="total-price clearfix">
						<div class="price-title">Total Harga</div>
						<div class="price-amount"><small>IDR</small> <?php echo set_number_format($total_amount, 0); ?></div>
					</div>
				</div>
				<hr class="mtop-10">
	            <div class="mtop-20">
					<button type="submit" class="link-button large block">PESAN SEKARANG</button>
	            </div>
			</div>
		</form>
	</div>
</div>