<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div id="hotel-detail-<?php echo $detail_hotel['hotelId']; ?>" class="hotel-detail-wrapper detail-item">
	<div class="detail-head">
		<h1><?php echo $detail_hotel['name']; ?></h1>
		<div class="text-small">
			<i class="fa fa-building"></i> <?php //echo $detail_hotel['hotelType']; ?>
			<i class="fa fa-map-marker"></i> <?php echo $detail_hotel['address']; ?>
		</div>
	</div>
	<ul class="detail-info">
		<li>
			<a class="no-loader" onclick="$(this).parent().find('.info-content').toggle();"><i class="fa fa-list-ul"></i> Informasi</a>
			<div class="info-content amenities">
				<?php echo html_entity_decode($detail_hotel['hotel_info']['propertyDescription'], ENT_QUOTES, 'UTF-8'); ?>
			</div>
		</li>
		<li>
			<a class="no-loader" onclick="$(this).parent().find('.info-content').toggle();"><i class="fa fa-picture-o"></i> Galeri</a>
			<div class="info-content gallery">
			<?php if ( ! empty($detail_hotel['image_thumb']) && ! empty($detail_hotel['image_thumb']['big_img'])) { ?>
				<ul class="clearfix">
				<?php foreach ($detail_hotel['image_thumb']['big_img'] as $key => $img) { ?>
					<li><img src="<?php echo $img; ?>" alt=""></li>
				<?php } ?>
				</ul>
			</div>
			<?php } else { ?>
				<h4 class="ptop-20 pbottom-20 text-center text-gray">Tidak ada data.</h4>
			<?php } ?>
		</li>
		<li>
			<a class="no-loader" onclick="$(this).parent().find('.info-content').toggle();resizeMap('<?php echo $detail_hotel['hotelId']; ?>');"><i class="fa fa-map-o"></i> Lokasi</a>
			<div class="info-content location">
			<?php if ( ! empty($detail_hotel['location_info']) && ! empty($detail_hotel['location_info']['locationDescription'])) { ?>
				<p><?php echo $detail_hotel['location_info']['locationDescription']; ?></p>
			<?php } ?>

			<?php if ( ! empty($detail_hotel['location_info']) && ! empty($detail_hotel['location_info']['longitude']) && ! empty($detail_hotel['location_info']['latitude'])) { ?>
				<div class="site-metadata" data-longitude="<?php echo $detail_hotel['location_info']['longitude']; ?>" data-latitude="<?php echo $detail_hotel['location_info']['latitude']; ?>">
			<?php } ?>
			</div>
		</li>
		<li>
			<a class="no-loader" onclick="$(this).parent().find('.info-content').toggle();"><i class="fa fa-comments-o"></i> Ulasan</a>
			<div class="info-content review">
			<?php if ( ! empty($detail_hotel['review_rating']) && ! empty($detail_hotel['review_rating']['urlReview'])) { ?>
				<label class="mbottom-10">Supported By</label>
				<img src="<?php echo $assets_url; ?>img/tripadvisor.png" alt="TripAdvisor">
				<div class="mtop-10">
					<a href="<?php echo $detail_hotel['review_rating']['urlReview']; ?>" target="_blank" class="mtop-20 link-button extra-small outline">Lihat Ulasan TripAdvisor <i class="fa fa-external-link"></i></a>
				</div>
			<?php } else { ?>
				<h4 class="ptop-20 pbottom-20 text-center text-gray">Tidak ada data.</h4>
			<?php } ?>
			</div>
		</li>
	</ul>

	<?php if ( ! empty($detail_hotel['available_room'])) { ?>
		<ul class="room-list mtop-20">
		<?php foreach ($detail_hotel['available_room'] as $key => $room) { ?>
			<li id="room-<?php echo $detail_hotel['hotelId']; ?>-<?php echo $room['room_code']; ?>">
				<div class="room-head" class="clearfix">
					<div class="room-image">
						<?php if ( ! empty($room['small_images'])) { ?>
							<img src="<?php echo $room['small_images']; ?>" alt="<?php echo $room['room_name']; ?>">
						<?php } else { ?>
							<i class="fa fa-hotel"></i>
						<?php } ?>
					</div>
					<div class="room-title">
						<h4><?php echo $room['room_name']; ?></h4>
						<p>
						<?php if ( ! empty($room['cancelTitle'])) { ?>
							<?php echo $room['cancelTitle']; ?>
						<?php } ?>
						<?php if ( ! empty($room['cancelDetail'])) { ?>
							(<?php echo $room['cancelDetail']; ?>)
						<?php } ?>
						</p>
					</div>
				</div>
				<?php if ( ! empty($room['room_description']) OR ! empty($room['room_amenity'])) { ?>
				<ul class="room-desc clearfix">
					<?php if ( ! empty($room['room_description'])) { ?>
					<li>
						<strong>Deskripsi</strong>
						<div class="content-html">
							<?php echo html_entity_decode($room['room_description'], ENT_QUOTES, 'UTF-8'); ?>
						</div>
					</li>
					<?php if ( ! empty($room['valueAdds'])) { ?>
					<?php } ?>
					<li>
						<strong>Fasilitas</strong>
						<div class="content-html">
							<?php echo html_entity_decode($room['valueAdds'], ENT_QUOTES, 'UTF-8'); ?>
						</div>
					</li>
					<?php } ?>
				</ul>
				<?php } ?>
				<div class="room-price">
					<label>Harga</label>
					<p><small><?php echo $room['currency_code']; ?></small> <?php echo set_number_format($room['total_price'], 0); ?></p>
				</div>
				<div class="room-action">
					<button type="button" class="link-button block" 
					data-ref="<?php echo $detail_reference; ?>" 
					data-code="<?php echo $room['room_code']; ?>" 
				 	data-hotel-id="<?php echo $detail_hotel['hotelId']; ?>"
				 	data-room-id="<?php echo $detail_hotel['hotelId']; ?>-<?php echo $room['room_code']; ?>"
					onclick="setRoom(this);">PILIH</button>
				</div>
			</li>
		<?php } ?>
		</ul>
	<?php } else { ?>
		<hr class="mtop-20 ptop-20">
		<h3 class="pbottom-20 text-center text-gray">Tidak ada data kamar yang bisa dipilih.</h3>
	<?php } ?>
</div>