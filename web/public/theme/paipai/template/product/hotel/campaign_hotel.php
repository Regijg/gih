<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container pbottom-30">
	<div class="campaign-wrapper hotel">
	<?php if ( ! empty($app['type']) && ($app['type'] == 'full')) {  ?>
		<div class="campaign-head mbottom-10">
			<h1><i class="fa fa-hotel"></i> Pencarian Hotel</h1>
		</div>
	<?php } ?>

		<div class="campaign-search">
			<form action="<?php echo $app_url; ?>search/hotel" method="GET" class="form-search">
				<input type="hidden" name="search" value="hotel">
				<div class="box box-1">
					<div class="default">
						<div class="item">
							<label>Lokasi Menginap</label>
							<div class="icon"><i class="fa fa-map-o"></i></div>
							<input type="text" name="location" class="select-get-hotelpoi" placeholder="Nama Kota, Tempat Wisata, atau Nama Hotel" value="">
							<div class="selection-container">
								<ul>
									<li class="message"><span class="text-muted">Ketik Kata Kunci</span></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="box box-2">
					<div class="date">
						<div class="item">
							<label>Tanggal Check-In</label>
							<div class="icon"><i class="fa fa-calendar"></i></div>
							<input type="text" name="checkin" class="datepicker date-checkin" readonly="readonly" placeholder="Tanggal Check-In" value="">
						</div>
						<div class="item mtop-10">
							<label>Tanggal Check-Out</label>
							<div class="icon"><i class="fa fa-calendar"></i></div>
							<input type="text" name="checkout" class="datepicker date-checkout" readonly="readonly" placeholder="Tanggal Check-Out" value="">
						</div>
					</div>
				</div>
				<div class="box box-3 no-border">
					<input type="hidden" name="room" value="1">
					<input type="hidden" name="guest" value="1">
					<ul class="qty clearfix">
						<li>
							<label>Kamar</label>
							<a class="toggle no-loader"><span>1</span> <i class="fa fa-angle-down"></i></a>
							<ul>
								<li><a class="no-loader" data-type="room" data-value="1">1</a></li>
								<li><a class="no-loader" data-type="room" data-value="2">2</a></li>
								<li><a class="no-loader" data-type="room" data-value="3">3</a></li>
								<li><a class="no-loader" data-type="room" data-value="4">4</a></li>
								<li><a class="no-loader" data-type="room" data-value="5">5</a></li>
								<li><a class="no-loader" data-type="room" data-value="6">6</a></li>
								<li><a class="no-loader" data-type="room" data-value="7">7</a></li>
								<li><a class="no-loader" data-type="room" data-value="8">8</a></li>
							</ul>
						</li>
						<li>
							<label>Tamu/Kamar</label>
							<a class="toggle no-loader"><span>1</span> <i class="fa fa-angle-down"></i></a>
							<ul>
								<li><a class="no-loader" data-type="guest" data-value="1">1</a></li>
								<li><a class="no-loader" data-type="guest" data-value="2">2</a></li>
								<li><a class="no-loader" data-type="guest" data-value="3">3</a></li>
								<li><a class="no-loader" data-type="guest" data-value="4">4</a></li>
								<li><a class="no-loader" data-type="guest" data-value="5">5</a></li>
								<li><a class="no-loader" data-type="guest" data-value="6">6</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="clear"></div>
				<div class="action">
					<button type="submit" class="submit"><i class="fa fa-search"></i> CARI</button>
				</div>
			</form>
		</div>
		
	<?php if ( ! empty($app['type']) && ($app['type'] == 'full')) {  ?>
		<div class="box-wrapper mtop-30">
			<div class="box-head">
				<h4><i class="fa fa-circle-o"></i> Kota <span>Populer</span></h4>
			</div>
			<div class="box-body">
				<ul class="campaign-list clearfix">
					<li>
						<a class="quick-action" rel="hotel" data-keyword="Jakarta">
							<img src="<?php echo $storage_url; ?>default/city/jakarta.jpg" alt="">
							<div class="title">Jakarta</div>
							<div class="desc">
								<i class="fa fa-hotel"></i> Mulai dari Rp. 200.000
							</div>
						</a>
					</li>
					<li>
						<a class="quick-action" rel="hotel" data-keyword="Bandung">
							<img src="<?php echo $storage_url; ?>default/city/bandung.jpg" alt="">
							<div class="title">Bandung</div>
							<div class="desc">
								<i class="fa fa-hotel"></i> Mulai dari Rp. 200.000
							</div>
						</a>
					</li>
					<li>
						<a class="quick-action" rel="hotel" data-keyword="Yogyakarta">
							<img src="<?php echo $storage_url; ?>default/city/yogyakarta.jpg" alt="">
							<div class="title">Yogyakarta</div>
							<div class="desc">
								<i class="fa fa-hotel"></i> Mulai dari Rp. 200.000
							</div>
						</a>
					</li>
					<li>
						<a class="quick-action" rel="hotel" data-keyword="Bali">
							<img src="<?php echo $storage_url; ?>default/city/bali.jpg" alt="">
							<div class="title">Bali</div>
							<div class="desc">
								<i class="fa fa-hotel"></i> Mulai dari Rp. 200.000
							</div>
						</a>
					</li>
					<li>
						<a class="quick-action" rel="hotel" data-keyword="Bogor">
							<img src="<?php echo $storage_url; ?>default/city/bogor.jpg" alt="">
							<div class="title">Bogor</div>
							<div class="desc">
								<i class="fa fa-hotel"></i> Mulai dari Rp. 200.000
							</div>
						</a>
					</li>
					<li>
						<a class="quick-action" rel="hotel" data-keyword="Surabaya">
							<img src="<?php echo $storage_url; ?>default/city/surabaya.jpg" alt="">
							<div class="title">Surabaya</div>
							<div class="desc">
								<i class="fa fa-hotel"></i> Mulai dari Rp. 200.000
							</div>
						</a>
					</li>
					<li>
						<a class="quick-action" rel="hotel" data-keyword="Malang">
							<img src="<?php echo $storage_url; ?>default/city/malang.jpg" alt="">
							<div class="title">Malang</div>
							<div class="desc">
								<i class="fa fa-hotel"></i> Mulai dari Rp. 200.000
							</div>
						</a>
					</li>
					<li>
						<a class="quick-action" rel="hotel" data-keyword="Medan">
							<img src="<?php echo $storage_url; ?>default/city/medan.jpg" alt="">
							<div class="title">Medan</div>
							<div class="desc">
								<i class="fa fa-hotel"></i> Mulai dari Rp. 200.000
							</div>
						</a>
					</li>
					<li>
						<a class="quick-action" rel="hotel" data-keyword="Semarang">
							<img src="<?php echo $storage_url; ?>default/city/semarang.jpg" alt="">
							<div class="title">Semarang</div>
							<div class="desc">
								<i class="fa fa-hotel"></i> Mulai dari Rp. 200.000
							</div>
						</a>
					</li>
					<li>
						<a class="quick-action" rel="hotel" data-keyword="Solo">
							<img src="<?php echo $storage_url; ?>default/city/solo.jpg" alt="">
							<div class="title">Solo</div>
							<div class="desc">
								<i class="fa fa-hotel"></i> Mulai dari Rp. 200.000
							</div>
						</a>
					</li>
				</ul>
			</div>
		</div>
	<?php } ?>
	</div>
</div>