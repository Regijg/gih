<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php
	foreach ($search_result as $value)
	{
		$train_etd = ( ! empty($value['departure_time']) && ($value['departure_time'] != ':') ? $value['departure_time'] : '00:00');
		$train_eta = ( ! empty($value['arrival_time']) && ($value['arrival_time'] != ':') ? $value['arrival_time'] : '00:00');

		$train_dep 		= ucwords(strtolower($value['dep_station_name'])) . ' (' . strtoupper($value['dep_station_code']) . ')';
		$train_dep_city 	= $value['dep_station_city'];
		$train_arr 		= ucwords(strtolower($value['arr_station_name'])) . ' (' . strtoupper($value['arr_station_code']) . ')';
		$train_arr_city 	= $value['arr_station_city'];

		$etd = new DateTime($value['departure_date'] . ' ' . $train_etd);
		$eta = new DateTime($value['arrival_date'] . ' ' . $train_eta);
		$interval = $etd->diff($eta);

		foreach ($value['cabin_class'] as $key => $class) {
?>
	<li id="<?php echo preg_replace('/\s+/', '', strtolower($value['train_name'])) . '-' . $search_schedule . '-' . $key; ?>" class="train-item" data-class="<?php echo strtolower($class['class_name']); ?>" data-name="<?php echo strtolower($value['train_name']); ?>" data-etd="<?php echo $etd->format('U'); ?>" data-eta="<?php echo $eta->format('U'); ?>" data-duration="<?php echo (((int) $interval->h) * 60) + (int) $interval->i; ?>" data-price="<?php echo (float) $class['adult_price']; ?>" style="display: none;">
		<div class="preview clearfix">
			<div class="list-data train-title">
				<img src="<?php echo $assets_url; ?>img/kai.png" alt="<?php echo $value['train_name']; ?>">
				<p><?php echo $value['train_name']; ?></p>
			</div>
			<div class="list-data train-schedule clearfix">
				<div>
					<p class="time"><?php echo $train_etd; ?></p>
					<p class="desc"><?php echo $train_dep_city; ?></p>
				</div>
				<div>
					<p class="time"><?php echo $train_eta; ?></p>
					<p class="desc"><?php echo $train_arr_city; ?></p>
				</div>
				<div>
					<p class="time"><?php echo $interval->h; ?>j <?php echo $interval->i; ?>m</p>
					<p class="desc"><?php echo ucwords(strtolower($class['class_name'])); ?></p>
				</div>
			</div>
			<div class="list-data train-price">
				<small>IDR</small>
				<p><?php echo set_number_format($class['adult_price'], 0, '', '', 'i'); ?></p>
			</div>
			<div class="list-data train-action">
				<button type="button" class="link-button block action set-train" data-ref="<?php echo $search_reference; ?>" data-schedule-id="<?php echo $value['schedule_id']; ?>" data-code="<?php echo ( ! empty($class['sub_class']) ? $class['sub_class'] : ''); ?>" data-id="<?php echo preg_replace('/\s+/', '', strtolower($value['train_name'])) . '-' . $search_schedule . '-' . $key; ?>" data-schedule="<?php echo $search_schedule; ?>">PILIH</button>
				<button type="button" class="link-button extra-small outline gray block detail show-train-detail" data-id="<?php echo preg_replace('/\s+/', '', strtolower($value['train_name'])) . '-' . $search_schedule . '-' . $key; ?>">Lihat Rincian</button>
			</div>
		</div>
		<div class="detail clearfix">
			<ul class="route">
				<li class="active clearfix">
					<div class="desc">
						<h5><?php echo $value['train_name']; ?></h5>
						<p>
							<?php echo $class['class_name']; ?>
							<?php if ( ! empty($class['sub_class'])) { ?>
								/ <?php echo $class['sub_class']; ?>
							<?php } ?>
						</p>
					</div>
				</li>
				<li class="clearfix">
					<div class="icon"><i class="fa fa-circle-o"></i></div>
					<div class="time">
						<h5><?php echo $value['departure_time']; ?></h5>
						<span><?php echo date('d M Y', strtotime($value['departure_date'])); ?></span>
					</div>
					<div class="airport">
						<h5><?php echo $train_dep; ?></h5>
						<span><?php echo $train_dep_city; ?></span>
					</div>
				</li>
				<li class="clearfix">
					<div class="icon"><i class="fa fa-circle-o"></i></div>
					<div class="time">
						<h5><?php echo $value['arrival_time']; ?></h5>
						<span><?php echo date('d M Y', strtotime($value['arrival_date'])); ?></span>
					</div>
					<div class="airport">
						<h5><?php echo $train_arr; ?></h5>
						<span><?php echo $train_arr_city; ?></span>
					</div>
				</li>
			</ul>
		</div>
	</li>
	<?php } ?>
<?php } ?>