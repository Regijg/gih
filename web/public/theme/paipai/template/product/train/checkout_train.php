<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container pbottom-30">
	<div class="checkout-wrapper clearfix train">
    	<form id="form-checkout" class="default-form" method="POST" action="<?php echo $app_url; ?>product/train/payment" enctype="multipart/form-data">
			<div class="input-wrapper">
				<h1>Informasi Pelanggan</h1>
				<div class="form-group">
					<div class="form-item">
						<input type="text" name="cust_name" class="input-item" placeholder="Nama">
					</div>
					<div class="form-item">
						<input type="email" name="cust_email" class="input-item" placeholder="Email">
					</div>
					<div class="form-item">
						<input type="number" name="cust_phone" class="input-item" placeholder="No Telp">
					</div>
					<div class="mtop-20 mbottom-10 notification text-small">
						<p><span class="text-error"><i class="fa fa-exclamation-triangle"></i></span> Pastikan data pelanggan diisi dengan benar. <strong>e-Ticket &amp; Informasi Pemesanan</strong> akan dikirim ke email di atas.</p>
					</div>
				</div>
				<h1 class="mtop-20">Informasi Penumpang</h1>
				<?php 
				$pass = 1;
				$adult = ( ! empty($cart['train']['depart']['fares_details']['adult']['sitter']) ? (int) $cart['train']['depart']['fares_details']['adult']['sitter'] : ( ! empty($cart['train']['return']['fares_details']['adult']['sitter']) ? (int) $cart['train']['return']['fares_details']['adult']['sitter'] : 0));
					$infant = ( ! empty($cart['train']['depart']['fares_details']['infant']['sitter']) ? (int) $cart['train']['depart']['fares_details']['infant']['sitter'] : ( ! empty($cart['train']['return']['fares_details']['infant']['sitter']) ? (int) $cart['train']['return']['fares_details']['infant']['sitter'] : 0));
				?>
				
				<?php for ($i = 0; $i < $adult; $i++) { ?>
					<input type="hidden" name="passenger[<?php echo $pass; ?>][type]" value="adult">
					<div class="form-group">
						<div class="group-title">Penumpang #<?php echo $pass; ?> (Dewasa)</div>
						<div class="form-item">
							<input type="text" name="passenger[<?php echo $pass; ?>][name]" class="input-item" placeholder="Nama Penumpang">
						</div>
						<div class="form-item">
							<input type="text" name="passenger[<?php echo $pass; ?>][id]" class="input-item" placeholder="No Identitas">
						</div>
						<div class="form-item">
							<input type="text" name="passenger[<?php echo $pass; ?>][dob]" class="input-item datedob" placeholder="Tgl Lahir" readonly>
						</div>
						<div class="form-item">
							<input type="number" name="passenger[<?php echo $pass; ?>][phone]" class="input-item" placeholder="No Telepon">
						</div>
					</div>
					<?php $pass++; ?>
				<?php } ?>

				<?php for ($i = 0; $i < $infant; $i++) { ?>
					<input type="hidden" name="passenger[<?php echo $pass; ?>][type]" value="infant">
					<div class="form-group">
						<div class="group-title">Penumpang #<?php echo $pass; ?> (Bayi)</div>
						<div class="form-item">
							<input type="text" name="passenger[<?php echo $pass; ?>][name]" class="input-item" placeholder="Nama Penumpang">
						</div>
						<div class="form-item">
							<input type="text" name="passenger[<?php echo $pass; ?>][id]" class="input-item" placeholder="No Identitas">
						</div>
						<div class="form-item">
							<input type="text" name="passenger[<?php echo $pass; ?>][dob]" class="input-item datedob" placeholder="Tgl Lahir" readonly>
						</div>
						<div class="form-item">
							<input type="number" name="passenger[<?php echo $pass; ?>][phone]" class="input-item" placeholder="No Telepon">
						</div>
					</div>
					<?php $pass++; ?>
				<?php } ?>

				<div class="mtop-20 notification text-small">
					<p><span class="text-error"><i class="fa fa-exclamation-triangle"></i></span> Pastikan data penumpang diisi dengan benar. <strong>Validitas tiket</strong> berdasarkan data yang dimasukkan di atas.</p>
				</div>
			</div>
			<div class="cart-wrapper">
				<h1>Rincian Pesanan</h1>
				<?php if ( ! empty($cart['train']['depart']['schedule'])) { ?>
					<?php $train = $cart['train']['depart']['schedule']; ?>
					<div class="cart-item has-image">
						<div class="clearfix">
							<div class="cart-image">
								<img src="<?php echo $assets_url; ?>img/kai.png" alt="<?php echo $train['train_name']; ?>">
								<p><?php echo ucwords($train['class_name']); ?></p>
							</div>
							<div class="cart-description">
								<ul class="route">
									<li class="active clearfix">
										<div class="desc">
											<h5><?php echo $train['train_name']; ?> - <?php echo $train['class_name']; ?> / <?php echo $train['sub_class']; ?></h5>
										</div>
									</li>
									<li class="clearfix">
										<div class="icon"><i class="fa fa-circle-o"></i></div>
										<div class="schedule">
											<?php echo $train['dep_station_name']; ?> - <?php echo $train['dep_station_code']; ?><br>
											(<?php echo date('d M Y H:i', strtotime($train['departure_date'] . ' ' .$train['departure_time'])); ?>)
										</div>
									</li>
									<li class="clearfix">
										<div class="icon"><i class="fa fa-circle-o"></i></div>
										<div class="schedule">
											<?php echo $train['arr_station_name']; ?> - <?php echo $train['arr_station_code']; ?><br>
											(<?php echo date('d M Y H:i', strtotime($train['arrival_date'] . ' ' .$train['arrival_time'])); ?>)
										</div>
									</li>
								</ul>
							</div>
						</div>
						<?php if ( ! empty($cart['train']['depart']['fares_details'])) { ?>
						<ul class="cart-price mtop-10">
							<?php foreach($cart['train']['depart']['fares_details'] as $key => $fares) { ?>
							<li class="clearfix">
								<div class="price-title"><?php echo ucwords($key); ?> (x<?php echo $fares['sitter']; ?>)</div>
								<div class="price-amount"><small>IDR</small> <?php echo set_number_format($fares['total_price'], 0); ?></div>
							</li>
							<?php } ?>
							<?php if ( ! empty($cart['train']['depart']['extra_fee'])) { ?>
							<li class="clearfix">
								<div class="price-title">Extra Fee</div>
								<div class="price-amount"><small>IDR</small> <?php echo set_number_format($cart['train']['depart']['extra_fee'], 0); ?></div>
							</li>
							<?php } ?>
							<?php if ( ! empty($cart['train']['depart']['discount'])) { ?>
							<li class="clearfix">
								<div class="price-title">Discount</div>
								<div class="price-amount text-success"><small>IDR</small> <?php echo set_number_format($cart['train']['depart']['discount'], 0); ?></div>
							</li>
							<?php } ?>
						</ul>
						<?php } ?>
					</div>
				<?php } ?>

				<?php if ( ! empty($cart['train']['return']['schedule'])) { ?>
					<?php $train = $cart['train']['return']['schedule']; ?>
					<div class="cart-item has-image">
						<div class="clearfix">
							<div class="cart-image">
								<img src="<?php echo $assets_url; ?>img/kai.png" alt="<?php echo $train['train_name']; ?>">
								<p><?php echo ucwords($train['class_name']); ?></p>
							</div>
							<div class="cart-description">
								<ul class="route">
									<li class="active clearfix">
										<div class="desc">
											<h5><?php echo $train['train_name']; ?> - <?php echo $train['class_name']; ?> / <?php echo $train['sub_class']; ?></h5>
										</div>
									</li>
									<li class="clearfix">
										<div class="icon"><i class="fa fa-circle-o"></i></div>
										<div class="schedule">
											<?php echo $train['dep_station_name']; ?> - <?php echo $train['dep_station_code']; ?><br>
											(<?php echo date('d M Y H:i', strtotime($train['departure_date'] . ' ' .$train['departure_time'])); ?>)
										</div>
									</li>
									<li class="clearfix">
										<div class="icon"><i class="fa fa-circle-o"></i></div>
										<div class="schedule">
											<?php echo $train['arr_station_name']; ?> - <?php echo $train['arr_station_code']; ?><br>
											(<?php echo date('d M Y H:i', strtotime($train['arrival_date'] . ' ' .$train['arrival_time'])); ?>)
										</div>
									</li>
								</ul>
							</div>
						</div>
						<?php if ( ! empty($cart['train']['return']['fares_details'])) { ?>
						<ul class="cart-price mtop-10">
							<?php foreach($cart['train']['return']['fares_details'] as $key => $fares) { ?>
							<li class="clearfix">
								<div class="price-title"><?php echo ucwords($key); ?> (x<?php echo $fares['sitter']; ?>)</div>
								<div class="price-amount"><small>IDR</small> <?php echo set_number_format($fares['total_price'], 0); ?></div>
							</li>
							<?php } ?>
							<?php if ( ! empty($cart['train']['return']['extra_fee'])) { ?>
							<li class="clearfix">
								<div class="price-title">Extra Fee</div>
								<div class="price-amount"><small>IDR</small> <?php echo set_number_format($cart['train']['return']['extra_fee'], 0); ?></div>
							</li>
							<?php } ?>
							<?php if ( ! empty($cart['train']['return']['discount'])) { ?>
							<li class="clearfix">
								<div class="price-title">Discount</div>
								<div class="price-amount text-success"><small>IDR</small> <?php echo set_number_format($cart['train']['return']['discount'], 0); ?></div>
							</li>
							<?php } ?>
						</ul>
						<?php } ?>
					</div>
				<?php } ?>
				<hr class="mtop-20">
				<div class="total-price clearfix">
					<div class="price-title">Total Harga</div>
					<div class="price-amount"><small>IDR</small> <?php echo set_number_format($cart['train']['total_fares'], 0); ?></div>
				</div>
				<hr class="mtop-10">
	            <div class="mtop-20">
					<button type="submit" class="link-button large block">PESAN SEKARANG</button>
	            </div>
			</div>
		</form>
	</div>
</div>