<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container pbottom-30">
	<div class="campaign-wrapper train">
	<?php if ( ! empty($app['type']) && ($app['type'] == 'full')) {  ?>
		<div class="campaign-head">
			<h1><i class="fa fa-train"></i> Pencarian Kereta</h1>
		</div>
	<?php } ?>

		<div class="campaign-search">
			<form action="<?php echo $app_url; ?>product/train/submit" method="POST" class="form-search">
				<input type="hidden" name="search" value="train">
				<div class="box box-1">
					<ul class="route">
						<li>
							<div class="icon"><i class="fa fa-map-marker"></i></div>
							<div class="item bordered">
								<label>Stasiun Asal</label>
								<input type="hidden" name="from" value="">
								<input type="hidden" name="from_label" value="">
								<input type="text" placeholder="Pilih Stasiun Asal" class="select-get-from" value="">
								<div class="selection-container">
									<ul>
										<li class="message"><span class="text-muted">Ketik Kata Kunci</span></li>
									</ul>
								</div>
							</div>
							<a class="switch switch-route no-loader" data-type="train"><i class="fa fa-refresh"></i></a>
						</li>
						<li>
							<div class="icon"><i class="fa fa-map-marker"></i></div>
							<div class="item">
								<label>Stasiun Tujuan</label>
								<input type="hidden" name="to" value="">
								<input type="hidden" name="to_label" value="">
								<input type="text" placeholder="Pilih Stasiun Tujuan" class="select-get-to" value="">
								<div class="selection-container">
									<ul>
										<li class="message"><span class="text-muted">Ketik Kata Kunci</span></li>
									</ul>
								</div>
							</div>
						</li>
					</ul>
				</div>
				<div class="box box-2">
					<div class="date">
                        <div class="date-switch">
                            <a class="no-loader oneway active" rel="train">Sekali Jalan</a>
                            <a class="no-loader return" rel="train">Pulang Pergi</a>
                        </div>
						<div class="item">
							<label>Tanggal Pergi</label>
							<div class="icon"><i class="fa fa-calendar"></i></div>
							<input type="text" name="depart" class="datepicker date-depart-campaign" readonly="readonly" placeholder="Tanggal Pergi" value="">
						</div>
						<div class="item mtop-10 return" style="display: none;">
							<label>Tanggal Pulang</label>
							<div class="icon"><i class="fa fa-calendar"></i></div>
							<input type="text" name="return" class="datepicker date-return-campaign" readonly="readonly" placeholder="Tanggal Pulang" value="">
						</div>
					</div>
				</div>
				<div class="box box-3 no-border">
					<input type="hidden" name="adult" value="1">
					<input type="hidden" name="infant" value="0">
					<ul class="qty clearfix">
						<li>
							<label>Dewasa</label>
							<a class="toggle no-loader"><span>1</span> <i class="fa fa-angle-down"></i></a>
							<ul>
								<li><a class="no-loader" data-type="adult" data-value="1">1</a></li>
								<li><a class="no-loader" data-type="adult" data-value="2">2</a></li>
								<li><a class="no-loader" data-type="adult" data-value="3">3</a></li>
								<li><a class="no-loader" data-type="adult" data-value="4">4</a></li>
								<li><a class="no-loader" data-type="adult" data-value="5">5</a></li>
								<li><a class="no-loader" data-type="adult" data-value="6">6</a></li>
								<li><a class="no-loader" data-type="adult" data-value="7">7</a></li>
							</ul>
						</li>
						<li>
							<label>Bayi</label>
							<a class="toggle no-loader"><span>0</span> <i class="fa fa-angle-down"></i></a>
							<ul>
								<li><a class="no-loader" data-type="infant" data-value="0">0</a></li>
								<li><a class="no-loader" data-type="infant" data-value="1">1</a></li>
								<li><a class="no-loader" data-type="infant" data-value="2">2</a></li>
								<li><a class="no-loader" data-type="infant" data-value="3">3</a></li>
								<li><a class="no-loader" data-type="infant" data-value="4">4</a></li>
								<li><a class="no-loader" data-type="infant" data-value="5">5</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="clear"></div>
				<div class="action">
					<button type="submit" class="submit"><i class="fa fa-search"></i> CARI</button>
				</div>
			</form>
		</div>

	<?php if ( ! empty($app['type']) && ($app['type'] == 'full')) {  ?>
		<div class="box-wrapper mtop-30">
			<div class="box-head">
				<h4><i class="fa fa-circle-o"></i> Rute <span>Populer</span></h4>
			</div>
			<div class="box-body">
				<ul class="campaign-list clearfix">
					<li>
						<a class="quick-action" rel="train" data-code="YK" data-label="YOGYAKARTA (YK), YOGYAKARTA">
							<img src="<?php echo $storage_url; ?>default/city/yogyakarta.jpg" alt="">
							<div class="title">Yogyakarta</div>
							<div class="desc">
								<i class="fa fa-plane"></i> Mulai dari Rp. 200.000
							</div>
						</a>
					</li>
					<li>
						<a class="quick-action" rel="train" data-code="SGU" data-label="SURABAYA GUBENG (SGU), SURABAYA">
							<img src="<?php echo $storage_url; ?>default/city/surabaya.jpg" alt="">
							<div class="title">Surabaya</div>
							<div class="desc">
								<i class="fa fa-plane"></i> Mulai dari Rp. 200.000
							</div>
						</a>
					</li>
					<li>
						<a class="quick-action" rel="train" data-code="MLK" data-label="MALANG KOTA (MLK), MALANG">
							<img src="<?php echo $storage_url; ?>default/city/malang.jpg" alt="">
							<div class="title">Malang</div>
							<div class="desc">
								<i class="fa fa-plane"></i> Mulai dari Rp. 200.000
							</div>
						</a>
					</li>
					<li>
						<a class="quick-action" rel="train" data-code="SMT" data-label="SEMARANG TAWANG (SMT), SEMARANG">
							<img src="<?php echo $storage_url; ?>default/city/semarang.jpg" alt="">
							<div class="title">Semarang</div>
							<div class="desc">
								<i class="fa fa-plane"></i> Mulai dari Rp. 200.000
							</div>
						</a>
					</li>
					<li>
						<a class="quick-action" rel="train" data-code="SLO" data-label="SOLOBALAPAN (SLO), SOLO">
							<img src="<?php echo $storage_url; ?>default/city/solo.jpg" alt="">
							<div class="title">Solo</div>
							<div class="desc">
								<i class="fa fa-plane"></i> Mulai dari Rp. 200.000
							</div>
						</a>
					</li>
				</ul>
			</div>
		</div>
	<?php } ?>
	</div>
</div>