<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container pbottom-30">
	<div class="search-result train <?php echo $roundtrip; ?>">
		<div class="result-head mbottom-10">
			<i class="fa fa-train"></i>
			<h1><?php echo $this->input->get('from_label'); ?> - <?php echo $this->input->get('to_label'); ?></h1>
			<p>
			<?php if ($this->input->get('depart')) { ?>
				<?php echo set_date_format($this->input->get('depart')); ?>
			<?php } ?>
			<?php if ($this->input->get('return')) { ?>
				- <?php echo set_date_format($this->input->get('return')); ?>
			<?php } ?>
			<?php if ($this->input->get('adult')) { ?>
				&bull; <?php echo (int) $this->input->get('adult'); ?> Dewasa
			<?php } ?>
			<?php if ($this->input->get('infant')) { ?>
				&bull; <?php echo (int) $this->input->get('infant'); ?> Bayi
			<?php } ?>
			</p>

			<?php if ( ! empty($app['type']) && ($app['type'] == 'full')) {  ?>
				<button type="button" class="link-button gray result-change" rel="search-train"><i class="fa fa-refresh"></i> &nbsp;Ganti Pencarian</button>
			<?php } else { ?>
				<a href="<?php echo $app_url; ?>product/train" class="link-button gray result-change"><i class="fa fa-refresh"></i> &nbsp;Ganti Pencarian</a>
			<?php } ?>
		</div>
		<div class="result-loader"><div></div></div>
		<div class="result-body clearfix<?php echo ($this->input->get('return') ? ' col-2' : ''); ?>">
			<?php if ($this->input->get('depart')) { ?>
			<div id="train-depart" class="result-wrapper active">
				<a class="no-loader mtop-10 mbottom-10 toggle-train-list active">
					<h4>Pilih Jadwal Pergi</h4>
				</a>
				<div class="result-items ptop-10">
					<div class="search-loader mbottom-20">Sedang mencari jadwal</div>
					<div class="result-filter">
						<div class="train-filter">
							<h5>
								Filter:
								<a class="no-loader" data-schedule="depart" rel="filter-class-depart">Tipe Kelas</a>
							</h5>
							<ul class="train-filter-item">
								<li id="filter-class-depart">
									<h6>Tipe Kelas</h6>
									<a class="filter-class no-loader" data-schedule="depart" rel="executive">Executive</a>
									<a class="filter-class no-loader" data-schedule="depart" rel="business">Business</a>
									<a class="filter-class no-loader" data-schedule="depart" rel="economy">Economy</a>
								</li>
							</ul>
						</div>
						<ul class="train-sort clearfix">
							<li><a class="no-loader" data-schedule="depart" data-direction="asc" rel="etd">Berangkat <i class="fa fa-sort"></i></a></li>
							<li><a class="no-loader" data-schedule="depart" data-direction="asc" rel="eta">Tiba <i class="fa fa-sort"></i></a></li>
							<li><a class="no-loader" data-schedule="depart" data-direction="asc" rel="duration">Durasi <i class="fa fa-sort"></i></a></li>
							<li><a class="no-loader active" data-schedule="depart" data-direction="desc" rel="price">Harga <i class="fa fa-sort-up"></i></a></li>
						</ul>
					</div>
					<ul class="train-list">
					</ul>
				</div>
			</div>
			<?php } ?>

			<?php if ($this->input->get('return')) { ?>
			<div id="train-return" class="result-wrapper inactive">
				<a class="no-loader mtop-10 mbottom-10 toggle-train-list">
					<h4>Pilih Jadwal Pulang</h4>
				</a>
				<div class="result-items ptop-10">
					<div class="overlay"></div>
					<div class="search-loader mbottom-20">Sedang mencari jadwal</div>
					<div class="result-filter">
						<div class="train-filter">
							<h5>
								Filter:
								<a class="no-loader" data-schedule="return" rel="filter-class-return">Tipe Kelas</a>
							</h5>
							<ul class="train-filter-item">
								<li id="filter-class-return">
									<h6>Tipe Kelas</h6>
									<a class="filter-class no-loader" data-schedule="depart" rel="executive">Executive</a>
									<a class="filter-class no-loader" data-schedule="depart" rel="business">Business</a>
									<a class="filter-class no-loader" data-schedule="depart" rel="economy">Economy</a>
								</li>
							</ul>
						</div>
						<ul class="train-sort clearfix">
							<li><a class="no-loader" data-schedule="return" data-direction="asc" rel="etd">Berangkat <i class="fa fa-sort"></i></a></li>
							<li><a class="no-loader" data-schedule="return" data-direction="asc" rel="eta">Tiba <i class="fa fa-sort"></i></a></li>
							<li><a class="no-loader" data-schedule="return" data-direction="asc" rel="duration">Durasi <i class="fa fa-sort"></i></a></li>
							<li><a class="no-loader active" data-schedule="return" data-direction="desc" rel="price">Harga <i class="fa fa-sort-up"></i></a></li>
						</ul>
					</div>
					<ul class="train-list">
					</ul>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</div>
<?php
	$item = 0;
	if ($this->input->get('depart')) $item = $item + 1;
	if ($this->input->get('return')) $item = $item + 1;
?>
<div id="search-data" class="site-metadata" search-url="<?php echo $search_url; ?>" search-adult="<?php echo $this->input->get('adult'); ?>" search-infant="<?php echo $this->input->get('infant'); ?>" search-item="<?php echo $item; ?>"></div>