<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container pbottom-30">
	<div class="search-result flight <?php echo $roundtrip; ?>">
		<div class="result-head mbottom-10">
			<i class="fa fa-plane"></i>
			<h1><?php echo $this->input->get('origin_label'); ?> - <?php echo $this->input->get('destination_label'); ?></h1>
			<p>
			<?php if ($this->input->get('depart')) { ?>
				<?php echo set_date_format($this->input->get('depart')); ?>
			<?php } ?>
			<?php if ($this->input->get('return')) { ?>
				- <?php echo set_date_format($this->input->get('return')); ?>
			<?php } ?>
			<?php if ($this->input->get('adult')) { ?>
				&bull; <?php echo (int) $this->input->get('adult'); ?> Dewasa
			<?php } ?>
			<?php if ($this->input->get('child')) { ?>
				&bull; <?php echo (int) $this->input->get('child'); ?> Anak
			<?php } ?>
			<?php if ($this->input->get('infant')) { ?>
				&bull; <?php echo (int) $this->input->get('infant'); ?> Bayi
			<?php } ?>
			</p>

			<?php if ( ! empty($app['type']) && ($app['type'] == 'full')) {  ?>
				<button type="button" class="link-button result-change" rel="search-flight"><i class="fa fa-refresh"></i> &nbsp;Ganti Pencarian</button>
			<?php } else { ?>
				<a href="<?php echo $app_url; ?>product/flight" class="link-button result-change"><i class="fa fa-refresh"></i> &nbsp;Ganti Pencarian</a>
			<?php } ?>
		</div>
		<div class="result-loader"><div></div></div>
		<div class="result-body clearfix<?php echo ($this->input->get('return') ? ' col-2' : ''); ?>">
			<?php if ($this->input->get('depart')) { ?>
			<div id="flight-depart" class="result-wrapper active">
				<a class="no-loader mtop-10 mbottom-10 toggle-flight-list active">
					<h4>Pilih Penerbangan Pergi</h4>
				</a>
				<div class="result-items ptop-10">
					<div class="search-loader mbottom-20">Sedang mencari jadwal</div>
					<div class="result-filter">
						<div class="flight-filter">
							<h5>
								Filter:
								<a class="no-loader" data-schedule="depart" rel="filter-type-depart">Tipe Penerbangan</a>
								<a class="no-loader" data-schedule="depart" rel="filter-airline-depart">Maskapai</a>
							</h5>
							<ul class="flight-filter-item">
								<li id="filter-type-depart">
									<h6>Tipe Penerbangan</h6>
									<a class="filter-type no-loader" data-schedule="depart" rel="direct">Direct</a>
									<a class="filter-type no-loader" data-schedule="depart" rel="transit">Transit</a>
									<a class="filter-type no-loader" data-schedule="depart" rel="connecting">Connecting</a>
								</li>
								<li id="filter-airline-depart">
									<h6>Maskapai</h6>
									<?php foreach ($airlines as $key => $airline) { ?>
										<a class="filter-airline no-loader" data-schedule="depart" rel="<?php echo strtolower($airline['airline_name']); ?>">
											<img src="<?php echo $storage_url; ?>default/airlines/<?php echo strtolower($airline['airline_name']); ?>.png" alt="">
											<?php echo $airline['airline_name']; ?>
										</a>
									<?php } ?>
								</li>
							</ul>
						</div>
						<ul class="flight-sort clearfix">
							<li><a class="no-loader" data-schedule="depart" data-direction="asc" rel="etd">Berangkat <i class="fa fa-sort"></i></a></li>
							<li><a class="no-loader" data-schedule="depart" data-direction="asc" rel="eta">Tiba <i class="fa fa-sort"></i></a></li>
							<li><a class="no-loader" data-schedule="depart" data-direction="asc" rel="duration">Durasi <i class="fa fa-sort"></i></a></li>
							<li><a class="no-loader active" data-schedule="depart" data-direction="desc" rel="price">Harga <i class="fa fa-sort-up"></i></a></li>
						</ul>
					</div>
					<ul class="flight-list">
					</ul>
				</div>
			</div>
			<?php } ?>

			<?php if ($this->input->get('return')) { ?>
			<div id="flight-return" class="result-wrapper inactive">
				<a class="no-loader mtop-10 mbottom-10 toggle-flight-list">
					<h4>Pilih Penerbangan Pulang</h4>
				</a>
				<div class="result-items ptop-10">
					<div class="overlay"></div>
					<div class="search-loader mbottom-20">Sedang mencari jadwal</div>
					<div class="result-filter">
						<div class="flight-filter">
							<h5>
								Filter:
								<a class="no-loader" data-schedule="return" rel="filter-type-return">Tipe Penerbangan</a>
								<a class="no-loader" data-schedule="return" rel="filter-airline-return">Maskapai</a>
							</h5>
							<ul class="flight-filter-item">
								<li id="filter-type-return">
									<h6>Tipe Penerbangan</h6>
									<a class="filter-type no-loader" data-schedule="return" rel="direct">Direct</a>
									<a class="filter-type no-loader" data-schedule="return" rel="transit">Transit</a>
									<a class="filter-type no-loader" data-schedule="return" rel="connecting">Connecting</a>
								</li>
								<li id="filter-airline-return">
									<h6>Maskapai</h6>
									<?php foreach ($airlines as $key => $airline) { ?>
										<a class="filter-airline no-loader" data-schedule="depart" rel="<?php echo strtolower($airline['airline_name']); ?>">
											<img src="<?php echo $storage_url; ?>default/airlines/<?php echo strtolower($airline['airline_name']); ?>.png" alt="">
											<?php echo $airline['airline_name']; ?>
										</a>
									<?php } ?>
								</li>
							</ul>
						</div>
						<ul class="flight-sort clearfix">
							<li><a class="no-loader" data-schedule="return" data-direction="asc" rel="etd">Berangkat <i class="fa fa-sort"></i></a></li>
							<li><a class="no-loader" data-schedule="return" data-direction="asc" rel="eta">Tiba <i class="fa fa-sort"></i></a></li>
							<li><a class="no-loader" data-schedule="return" data-direction="asc" rel="duration">Durasi <i class="fa fa-sort"></i></a></li>
							<li><a class="no-loader active" data-schedule="return" data-direction="desc" rel="price">Harga <i class="fa fa-sort-up"></i></a></li>
						</ul>
					</div>
					<ul class="flight-list">
					</ul>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</div>
<?php
	$item = 0;
	if ($this->input->get('depart')) $item = $item + 1;
	if ($this->input->get('return')) $item = $item + 1;
?>
<div id="search-data" class="site-metadata" search-url="<?php echo $search_url; ?>" search-adult="<?php echo $this->input->get('adult'); ?>" search-child="<?php echo $this->input->get('child'); ?>" search-infant="<?php echo $this->input->get('infant'); ?>" search-item="<?php echo $item; ?>"></div>