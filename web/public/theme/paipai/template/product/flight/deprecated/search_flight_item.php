<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php
	foreach ($search_result as $key => $value)
	{
		$flight_etd = $value['etd'];
		$flight_eta = $value['eta'];
		/** 
		$flight_dep 		= $value['flights']['dep'];
		$flight_dep_city 	= $value['flights']['dep_city'];
		$flight_arr 		= $value['flights']['arr'];
		$flight_arr_city 	= $value['flights']['arr_city'];
		**/
		$flight_dep 		= 'flight_dep';
		$flight_dep_city 	= 'flight_dep_city';;
		$flight_arr 		= 'flight_arr';;
		$flight_arr_city 	= 'flight_arry_city';;


		if (array_key_exists('connecting_flight', $value))
		{
			$connecting = end($value['connecting_flight']);
			$conn_etd = ( !empty($connecting['etd']) && ($connecting['etd'] != ':') ? $connecting['etd'] : $flight_etd);
			$flight_eta = ( !empty($connecting['eta']) && ($connecting['eta'] != ':') ? $connecting['eta'] : '00:00');
			$flight_arr = $connecting['from'];
			$flight_arr_city = $connecting['to'];

			if ((int) substr($flight_eta, 0, 2) < (int) substr($conn_etd, 0, 2))
			{
				$eta = new DateTime(date('Y-m-d', strtotime("+1 day", strtotime($connecting['date']))) . ' ' . $flight_eta);
			}
			else
			{
				$eta = new DateTime($connecting['date'] . ' ' . $flight_eta);
			}
		}
		else
		{
			if ((int) substr($flight_eta, 0, 2) < (int) substr($flight_etd, 0, 2))
			{
				$eta = new DateTime(date('Y-m-d', strtotime("+1 day", strtotime($value['date']))) . ' ' . $flight_eta);
			}
			else
			{
				$eta = new DateTime($value['date'] . ' ' . $flight_eta);
			}
		}

		$etd = new DateTime($value['date'] . ' ' . $flight_etd);
		$interval = $etd->diff($eta);
?>
	<li id="<?php echo preg_replace('/\s+/', '', strtolower($value['airline_name'])) . '-' . $search_schedule . '-' . $key; ?>" class="flight-item" data-type="<?php echo strtolower($value['type']); ?>" data-airline="<?php echo strtolower($value['airline_name']); ?>" data-etd="<?php echo $etd->format('U'); ?>" data-eta="<?php echo $eta->format('U'); ?>" data-duration="<?php echo (((int) $interval->h) * 60) + (int) $interval->i; ?>" data-price="<?php echo (float) $value['class'][0]['price']; ?>" style="display: none;">
		<div class="preview clearfix">
			<div class="list-data flight-title">
				<img src="<?php echo $assets_url; ?>img/airlines/<?php echo preg_replace('/\s+/', '', strtolower($value['airline_name'])); ?>.png" alt="<?php echo $value['airline_name']; ?>">
				<p><?php echo $value['fno']; ?></p>
			</div>
			<div class="list-data flight-schedule clearfix">
				<div>
					<p class="time"><?php echo $flight_etd; ?></p>
					<p class="desc"><?php echo $value['from'] ?></p>
				</div>
				<div>
					<p class="time"><?php echo $flight_eta; ?></p>
					<p class="desc"><?php echo $value['to']; ?></p>
				</div>
				<div>
					<p class="time"><?php echo $interval->h; ?>j <?php echo $interval->i; ?>m</p>
					<p class="desc"><?php echo ucwords(strtolower($value['type'])); ?></p>
				</div>
			</div>
			<div class="list-data flight-price">
				<small>IDR</small>
				<p><?php echo set_number_format($value['class'][0]['price'], 0, '', '', 'i'); ?></p>
			</div>
			<div class="list-data flight-action">
				<button type="button" class="link-button block action set-flight" 
				data-ref="<?php echo $search_reference; ?>" data-code="<?php echo ( ! empty($value['cabin'][0]['code']) ? $value['class'][0]['code'] : ''); ?>" data-id="<?php echo preg_replace('/\s+/', '', strtolower($value['airline_name'])) . '-' . $search_schedule . '-' . $key; ?>" data-schedule="<?php echo $search_schedule; ?>">PILIH</button>
			</div>
		</div>
		
	</li>
<?php } ?>