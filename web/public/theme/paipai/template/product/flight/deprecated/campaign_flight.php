<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container pbottom-30">
	<div class="campaign-wrapper flight">
	<?php if ( ! empty($app['type']) && ($app['type'] == 'full')) {  ?>
		<div class="campaign-head">
			<h1><i class="fa fa-plane"></i> Pencarian Pesawat</h1>
		</div>
	<?php } ?>
		
		<div class="campaign-search">
			<form action="<?php echo $app_url; ?>product/flight/submit" method="POST" class="form-search">
				<input type="hidden" name="redirect" value="<?php echo current_url(); ?>">
				<input type="hidden" name="search" value="flight">
				<div class="box box-1">
					<ul class="route">
						<li>
							<div class="icon"><i class="fa fa-map-marker"></i></div>
							<div class="item bordered">
								<label>Kota Asal</label>
								<input type="hidden" name="origin" value="">
								<input type="hidden" name="origin_label" value="">
								<input type="text" placeholder="Pilih Kota Asal" class="select-get-origin" value="">
								<div class="selection-container">
									<ul>
										<li class="message"><span class="text-muted">Ketik Kata Kunci</span></li>
									</ul>
								</div>
							</div>
							<a class="switch switch-route no-loader" data-type="flight"><i class="fa fa-refresh"></i></a>
						</li>
						<li>
							<div class="icon"><i class="fa fa-map-marker"></i></div>
							<div class="item">
								<label>Kota Tujuan</label>
								<input type="hidden" name="destination" value="">
								<input type="hidden" name="destination_label" value="">
								<input type="text" placeholder="Pilih Kota Tujuan" class="select-get-destination" value="">
								<div class="selection-container">
									<ul>
										<li class="message"><span class="text-muted">Ketik Kata Kunci</span></li>
									</ul>
								</div>
							</div>
						</li>
					</ul>
				</div>
				<div class="box box-2">
					<div class="date">
                        <div class="date-switch">
                            <a class="no-loader oneway active" rel="flight">Sekali Jalan</a>
                            <a class="no-loader return" rel="flight">Pulang Pergi</a>
                        </div>
						<div class="item bordered">
							<label>Tanggal Pergi</label>
							<div class="icon"><i class="fa fa-calendar"></i></div>
							<input type="text" name="depart" class="datepicker date-depart-campaign" readonly="readonly" placeholder="Tanggal Pergi" value="">
						</div>
						<div class="item mtop-10 return" style="display: none;">
							<label>Tanggal Pulang</label>
							<div class="icon"><i class="fa fa-calendar"></i></div>
							<input type="text" name="return" class="datepicker date-return-campaign" readonly="readonly" placeholder="Tanggal Pulang" value="">
						</div>
					</div>
				</div>
				<div class="box box-3 no-border">
					<input type="hidden" name="adult" value="1">
					<input type="hidden" name="child" value="0">
					<input type="hidden" name="infant" value="0">
					<ul class="pax clearfix">
						<li>
							<label>Dewasa</label>
							<a class="toggle no-loader"><span>1</span> <i class="fa fa-angle-down"></i></a>
							<ul>
								<li><a class="no-loader" data-type="adult" data-value="1">1</a></li>
								<li><a class="no-loader" data-type="adult" data-value="2">2</a></li>
								<li><a class="no-loader" data-type="adult" data-value="3">3</a></li>
								<li><a class="no-loader" data-type="adult" data-value="4">4</a></li>
								<li><a class="no-loader" data-type="adult" data-value="5">5</a></li>
								<li><a class="no-loader" data-type="adult" data-value="6">6</a></li>
								<li><a class="no-loader" data-type="adult" data-value="7">7</a></li>
							</ul>
						</li>
						<li>
							<label>Anak</label>
							<a class="toggle no-loader"><span>0</span> <i class="fa fa-angle-down"></i></a>
							<ul>
								<li><a class="no-loader" data-type="child" data-value="0">0</a></li>
								<li><a class="no-loader" data-type="child" data-value="1">1</a></li>
								<li><a class="no-loader" data-type="child" data-value="2">2</a></li>
								<li><a class="no-loader" data-type="child" data-value="3">3</a></li>
								<li><a class="no-loader" data-type="child" data-value="4">4</a></li>
								<li><a class="no-loader" data-type="child" data-value="5">5</a></li>
								<li><a class="no-loader" data-type="child" data-value="6">6</a></li>
							</ul>
						</li>
						<li>
							<label>Bayi</label>
							<a class="toggle no-loader"><span>0</span> <i class="fa fa-angle-down"></i></a>
							<ul>
								<li><a class="no-loader" data-type="infant" data-value="0">0</a></li>
								<li><a class="no-loader" data-type="infant" data-value="1">1</a></li>
								<li><a class="no-loader" data-type="infant" data-value="2">2</a></li>
								<li><a class="no-loader" data-type="infant" data-value="3">3</a></li>
								<li><a class="no-loader" data-type="infant" data-value="4">4</a></li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="clear"></div>
				<div class="action">
					<button type="submit" class="submit"><i class="fa fa-search"></i> CARI</button>
				</div>
			</form>
		</div>

	<?php if ( ! empty($app['type']) && ($app['type'] == 'full')) {  ?>
		<div class="box-wrapper mtop-30">
			<div class="box-head">
				<h4><i class="fa fa-circle-o"></i> Rute <span>Populer</span></h4>
			</div>
			<div class="box-body">
				<ul class="campaign-list clearfix">
					<li>
						<a class="quick-action" rel="flight" data-code="CGK" data-label="JAKARTA, INDONESIA (CGK)">
							<img src="<?php echo $storage_url; ?>default/city/jakarta.jpg" alt="">
							<div class="title">Jakarta</div>
							<div class="desc">
								<i class="fa fa-plane"></i> Mulai dari Rp. 200.000
							</div>
						</a>
					</li>
					<li>
						<a class="quick-action" rel="flight" data-code="BDO" data-label="BANDUNG, INDONESIA (BDO)">
							<img src="<?php echo $storage_url; ?>default/city/bandung.jpg" alt="">
							<div class="title">Bandung</div>
							<div class="desc">
								<i class="fa fa-plane"></i> Mulai dari Rp. 200.000
							</div>
						</a>
					</li>
					<li>
						<a class="quick-action" rel="flight" data-code="JOG" data-label="YOGYAKARTA, INDONESIA (JOG)">
							<img src="<?php echo $storage_url; ?>default/city/yogyakarta.jpg" alt="">
							<div class="title">Yogyakarta</div>
							<div class="desc">
								<i class="fa fa-plane"></i> Mulai dari Rp. 200.000
							</div>
						</a>
					</li>
					<li>
						<a class="quick-action" rel="flight" data-code="DPS" data-label="DENPASAR, INDONESIA (DPS)">
							<img src="<?php echo $storage_url; ?>default/city/bali.jpg" alt="">
							<div class="title">Bali</div>
							<div class="desc">
								<i class="fa fa-plane"></i> Mulai dari Rp. 200.000
							</div>
						</a>
					</li>
					<li>
						<a class="quick-action" rel="flight" data-code="CGK" data-label="JAKARTA, INDONESIA (CGK)">
							<img src="<?php echo $storage_url; ?>default/city/bogor.jpg" alt="">
							<div class="title">Bogor</div>
							<div class="desc">
								<i class="fa fa-plane"></i> Mulai dari Rp. 200.000
							</div>
						</a>
					</li>
					<li>
						<a class="quick-action" rel="flight" data-code="SUB" data-label="SURABAYA, INDONESIA (SUB)">
							<img src="<?php echo $storage_url; ?>default/city/surabaya.jpg" alt="">
							<div class="title">Surabaya</div>
							<div class="desc">
								<i class="fa fa-plane"></i> Mulai dari Rp. 200.000
							</div>
						</a>
					</li>
					<li>
						<a class="quick-action" rel="flight" data-code="MLG" data-label="MALANG, INDONESIA (MLG)">
							<img src="<?php echo $storage_url; ?>default/city/malang.jpg" alt="">
							<div class="title">Malang</div>
							<div class="desc">
								<i class="fa fa-plane"></i> Mulai dari Rp. 200.000
							</div>
						</a>
					</li>
					<li>
						<a class="quick-action" rel="flight" data-code="KNO" data-label="MEDAN, INDONESIA (KNO)">
							<img src="<?php echo $storage_url; ?>default/city/medan.jpg" alt="">
							<div class="title">Medan</div>
							<div class="desc">
								<i class="fa fa-plane"></i> Mulai dari Rp. 200.000
							</div>
						</a>
					</li>
					<li>
						<a class="quick-action" rel="flight" data-code="SRG" data-label="SEMARANG, INDONESIA (SRG)">
							<img src="<?php echo $storage_url; ?>default/city/semarang.jpg" alt="">
							<div class="title">Semarang</div>
							<div class="desc">
								<i class="fa fa-plane"></i> Mulai dari Rp. 200.000
							</div>
						</a>
					</li>
					<li>
						<a class="quick-action" rel="flight" data-code="SOC" data-label="SOLO, INDONESIA (SOC)">
							<img src="<?php echo $storage_url; ?>default/city/solo.jpg" alt="">
							<div class="title">Solo</div>
							<div class="desc">
								<i class="fa fa-plane"></i> Mulai dari Rp. 200.000
							</div>
						</a>
					</li>
				</ul>
			</div>
		</div>
	<?php } ?>
	</div>
</div>