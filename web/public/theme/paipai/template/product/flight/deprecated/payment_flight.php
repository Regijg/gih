<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container pbottom-30">
	<div class="checkout-wrapper clearfix flight">
		<div class="cart-wrapper mbottom-20">
			<h1>Batas Waktu &amp; Total Pembayaran</h1>
			
			<div class="mtop-20 notification error text-center text-small">
				<h4 id="countdown" rel="<?php echo date('Y-m-d H:i:s', strtotime($book['flight']['booking_limit'])); ?>"><?php echo date('Y-m-d H:i:s', strtotime($book['flight']['booking_limit'])); ?></h4>
				<p>Pastikan Anda segera melakukan pembayaran sebelum batas waktu habis.</p>
			</div>
			
			<div class="mtop-20 notification text-center text-small">
				<h4><small>IDR</small> <?php echo set_number_format($book['flight']['total_fares'], 0); ?></h4>
				<p><span class="text-error"><i class="fa fa-exclamation-triangle"></i></span> Pastikan <strong>Jumlah Pembayaran</strong> sesuai nilai yang tertera di atas.</p>
			</div>

			<h1>Rincian Pesanan</h1>
			<?php if ( ! empty($book['flight']['depart']['schedule'])) { ?>
				<?php $flight = $book['flight']['depart']['schedule']; ?>
				<div class="cart-item has-image">
					<div class="clearfix">
						<div class="cart-image">
							<img src="<?php echo $storage_url; ?>default/airlines/<?php echo preg_replace('/\s+/', '', strtolower($flight['airline_name'])); ?>.png" alt="<?php echo $flight['airline_name']; ?>">
							<p><?php echo ucwords($flight['flights_type']); ?></p>
						</div>
						<div class="cart-description">
							<ul class="route">
								<li class="active clearfix">
									<div class="desc">
										<h5><?php echo $flight['airline_name']; ?> <?php echo $flight['flights']['fno']; ?></h5>
									</div>
								</li>
								<li class="clearfix">
									<div class="icon"><i class="fa fa-circle-o"></i></div>
									<div class="schedule">
										<?php echo $flight['flights']['dep']; ?> - <?php echo $flight['flights']['etd']; ?> 
										(<?php echo date('d M Y', strtotime($flight['flights']['date'])); ?>)
									</div>
								</li>
								<li class="clearfix">
									<div class="icon"><i class="fa fa-circle-o"></i></div>
									<div class="schedule">
										<?php echo $flight['flights']['arr']; ?> - <?php echo $flight['flights']['eta']; ?>
										<?php if ((int) substr($flight['flights']['eta'], 0, 2) < (int) substr($flight['flights']['etd'], 0, 2)) { ?>
											(<?php echo date('d M Y', strtotime("+1 day", strtotime($flight['flights']['date']))); ?>)
										<?php } else { ?>
											(<?php echo date('d M Y', strtotime($flight['flights']['date'])); ?>)
										<?php } ?>
									</div>
								</li>
								<?php if ( ! empty($flight['flights_connecting'])) { ?>
									<?php foreach($flight['flights_connecting'] as $key2 => $conn) { ?>
									<li class="active clearfix">
										<div class="desc">
											<h5><?php echo ( ! empty($conn['airlines_name']) ? $conn['airlines_name'] : $flight['airline_name']) ; ?> <?php echo $conn['fno']; ?></h5>
										</div>
									</li>
									<li class="clearfix">
										<div class="icon"><i class="fa fa-circle-o"></i></div>
										<div class="schedule">
											<?php echo $conn['dep']; ?> - <?php echo $conn['etd']; ?> (<?php echo date('d M Y', strtotime($conn['date'])); ?>)
										</div>
									</li>
									<li class="clearfix">
										<div class="icon"><i class="fa fa-circle-o"></i></div>
										<div class="schedule">
											<?php echo $conn['arr']; ?> - <?php echo $conn['eta']; ?>
											<?php if ((int) substr($conn['eta'], 0, 2) < (int) substr($conn['etd'], 0, 2)) { ?>
												(<?php echo date('d M Y', strtotime("+1 day", strtotime($conn['date']))); ?>)
											<?php } else { ?>
												(<?php echo date('d M Y', strtotime($conn['date'])); ?>)
											<?php } ?>
										</div>
									</li>
									<?php } ?>
								<?php } ?>
							</ul>
						</div>
					</div>
					<?php if ( ! empty($book['flight']['depart']['fares_detail'])) { ?>
					<ul class="cart-price mtop-10">
						<?php foreach($book['flight']['depart']['fares_detail'] as $key => $fares) { ?>
						<li class="clearfix">
							<div class="price-title"><?php echo ucwords($key); ?> (x<?php echo $fares['sitter']; ?>)</div>
							<div class="price-amount"><small>RP</small> <?php echo set_number_format($fares[$key . '_fare'], 0); ?></div>
						</li>
						<?php } ?>
					</ul>
					<?php } ?>
				</div>
			<?php } ?>
			<?php if ( ! empty($book['flight']['return']['schedule'])) { ?>
				<?php $flight = $book['flight']['return']['schedule']; ?>
				<div class="cart-item has-image">
					<div class="clearfix">
						<div class="cart-image">
							<img src="<?php echo $storage_url; ?>default/airlines/<?php echo preg_replace('/\s+/', '', strtolower($flight['airline_name'])); ?>.png" alt="<?php echo $flight['airline_name']; ?>">
							<p><?php echo ucwords($flight['flights_type']); ?></p>
						</div>
						<div class="cart-description">
							<ul class="route">
								<li class="active clearfix">
									<div class="desc">
										<h5><?php echo $flight['airline_name']; ?> <?php echo $flight['flights']['fno']; ?></h5>
									</div>
								</li>
								<li class="clearfix">
									<div class="icon"><i class="fa fa-circle-o"></i></div>
									<div class="schedule">
										<?php echo $flight['flights']['dep']; ?> - <?php echo $flight['flights']['etd']; ?> 
										(<?php echo date('d M Y', strtotime($flight['flights']['date'])); ?>)
									</div>
								</li>
								<li class="clearfix">
									<div class="icon"><i class="fa fa-circle-o"></i></div>
									<div class="schedule">
										<?php echo $flight['flights']['arr']; ?> - <?php echo $flight['flights']['eta']; ?>
										<?php if ((int) substr($flight['flights']['eta'], 0, 2) < (int) substr($flight['flights']['etd'], 0, 2)) { ?>
											(<?php echo date('d M Y', strtotime("+1 day", strtotime($flight['flights']['date']))); ?>)
										<?php } else { ?>
											(<?php echo date('d M Y', strtotime($flight['flights']['date'])); ?>)
										<?php } ?>
									</div>
								</li>
								<?php if ( ! empty($flight['flights_connecting'])) { ?>
									<?php foreach($flight['flights_connecting'] as $key2 => $conn) { ?>
									<li class="active clearfix">
										<div class="desc">
											<h5><?php echo ( ! empty($conn['airlines_name']) ? $conn['airlines_name'] : $flight['airline_name']) ; ?> <?php echo $conn['fno']; ?></h5>
										</div>
									</li>
									<li class="clearfix">
										<div class="icon"><i class="fa fa-circle-o"></i></div>
										<div class="schedule">
											<?php echo $conn['dep']; ?> - <?php echo $conn['etd']; ?> (<?php echo date('d M Y', strtotime($conn['date'])); ?>)
										</div>
									</li>
									<li class="clearfix">
										<div class="icon"><i class="fa fa-circle-o"></i></div>
										<div class="schedule">
											<?php echo $conn['arr']; ?> - <?php echo $conn['eta']; ?>
											<?php if ((int) substr($conn['eta'], 0, 2) < (int) substr($conn['etd'], 0, 2)) { ?>
												(<?php echo date('d M Y', strtotime("+1 day", strtotime($conn['date']))); ?>)
											<?php } else { ?>
												(<?php echo date('d M Y', strtotime($conn['date'])); ?>)
											<?php } ?>
										</div>
									</li>
									<?php } ?>
								<?php } ?>
							</ul>
						</div>
					</div>
					<?php if ( ! empty($book['flight']['return']['fares_detail'])) { ?>
					<ul class="cart-price mtop-10">
						<?php foreach($book['flight']['return']['fares_detail'] as $key => $fares) { ?>
						<li class="clearfix">
							<div class="price-title"><?php echo ucwords($key); ?> (x<?php echo $fares['sitter']; ?>)</div>
							<div class="price-amount"><small>RP</small> <?php echo set_number_format($fares[$key . '_fare'], 0); ?></div>
						</li>
						<?php } ?>
					</ul>
					<?php } ?>
				</div>
			<?php } ?>
		</div>
		<div class="input-wrapper">
			<h1>Metode Pembayaran</h1>
			<div class="mtop-20 notification text-small">
				<p><span class="text-error"><i class="fa fa-exclamation-triangle"></i></span> Silahkan pilih metode pembayaran dibawah ini.</p>
			</div>
			<div class="payment-method">
				<ul class="clearfix">
				    <li>
				    	<a rel="payment-tabs-1" class="link-button small block no-loader">Instant Payment</a>
						<div id="payment-tabs-1" class="payment-tabs">
							<ul class="payment-method-item">
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>BCA KlikPay - Klik BCA Individu / Kartu Kredit BCA Card</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/bank-bca.jpg" alt="Bank BCA" />
										<img src="<?php echo $assets_url; ?>img/payment/channel-bcaklikpay.jpg" alt="BCA Klikpay" />
									</div>
								</li>
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>Mandiri Clickpay - Kartu Debit Mandiri</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/bank-mandiri.jpg" alt="Bank Mandiri" />
										<img src="<?php echo $assets_url; ?>img/payment/channel-mandiriclickpay.jpg" alt="Mandiri clickpay" />
									</div>
								</li>
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>e-Pay BRI - Kartu Debit BRI</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/bank-bri.jpg" alt="Bank BRI" />
										<img src="<?php echo $assets_url; ?>img/payment/channel-epaybri.jpg" alt="e-Pay BRI" />
									</div>
								</li>
							</ul>
						</div>
				    </li>
				    <li>
				    	<a rel="payment-tabs-2" class="link-button small block outline gray no-loader">Credit Card</a>
						<div id="payment-tabs-2" class="payment-tabs" style="display: none;">
							<ul class="payment-method-item">
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>Kartu Kredit Visa / Mastercard / JCB semua Bank di Indonesia</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/card-visa.jpg" alt="Visa" />
										<img src="<?php echo $assets_url; ?>img/payment/card-master.jpg" alt="MasterCard" />
										<img src="<?php echo $assets_url; ?>img/payment/card-jcb.jpg" alt="JCB" />
									</div>
								</li>
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>Kartu Kredit Mandiri Visa / Mastercard</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/bank-mandiri.jpg" alt="Bank Mandiri" />
										<img src="<?php echo $assets_url; ?>img/payment/card-visamaster.jpg" alt="Visa MasterCard" />
									</div>
								</li>
							</ul>
						</div>
				    </li>
				    <li>
				    	<a rel="payment-tabs-3" class="link-button small block outline gray no-loader">e-Wallet</a>
						<div id="payment-tabs-3" class="payment-tabs" style="display: none;">
							<ul class="payment-method-item">
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>Saldo Wallet / Kartu Kredit untuk anggota DOKU Wallet</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/channel-dokuwallet.jpg" alt="Doku Wallet" />
										<img src="<?php echo $assets_url; ?>img/payment/card-visamaster.jpg" alt="Visa MasterCard" />
									</div>
								</li>
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>XL Tunai</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/channel-xltunai.jpg" alt="XL Tunai" />
									</div>
								</li>
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>Telkomsel T-Cash</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/channel-tcash.jpg" alt="Telkomsel T-Cash" />
									</div>
								</li>
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>Indosat Dompetku</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/channel-dompetku.jpg" alt="Indosat Dompetku" />
									</div>
								</li>
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>Mandiri e-cash</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/channel-mandiriecash.jpg" alt="Mandiri e-cash" />
									</div>
								</li>
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>BCA Sakuku</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/channel-bcasakuku.jpg" alt="BCA Sakuku" />
									</div>
								</li>
							</ul>
						</div>
				    </li>
				    <li>
				    	<a rel="payment-tabs-4" class="link-button small block outline gray no-loader">Transfer</a>
						<div id="payment-tabs-4" class="payment-tabs" style="display: none;">
							<ul class="payment-method-item">
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>Transfer ke Rekening BCA, Mandiri, BRI, BNI, Permata, atau CIMB Niaga</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/transfer-bca.jpg" alt="Bank BCA" />
										<img src="<?php echo $assets_url; ?>img/payment/transfer-mandiri.jpg" alt="Bank Mandiri" />
										<img src="<?php echo $assets_url; ?>img/payment/transfer-bri.jpg" alt="Bank BRI" />
										<img src="<?php echo $assets_url; ?>img/payment/transfer-bni.jpg" alt="Bank BNI" />
									</div>
									<div class="description">
										<ul class="transfer-description clearfix">
											<li>
												<h4>Bank BCA</h4>
												<p>
												No. Rekening : <strong>123.456.7890</strong><br />
												a/n : <strong>PT TABS Indonesia</strong><br />
												Cabang : Bandung
												</p>
											</li>
											<li>
												<h4>Bank Mandiri</h4>
												<p>
												No. Rekening : <strong>123-00-4567890-1</strong><br />
												a/n : <strong>PT TABS Indonesia</strong><br />
												Cabang : Bandung
												</p>
											</li>
											<li>
												<h4>Bank BRI</h4>
												<p>
												No. Rekening : <strong>1234-01-123456-78-9</strong><br />
												a/n : <strong>PT TABS Indonesia</strong><br />
												Cabang : Bandung
												</p>
											</li>
											<li>
												<h4>Bank BNI</h4>
												<p>
												No. Rekening : <strong>123456789</strong><br />
												a/n : <strong>PT TABS Indonesia</strong><br />
												Cabang : Bandung
												</p>
											</li>
										</ul>
									</div>
								</li>
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>Permata Virtual Account menggunakan ATM jaringan Bersama, PRIMA, dan ALTO</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/transfer-permata.jpg" alt="Permata Bank" />
										<img src="<?php echo $assets_url; ?>img/payment/network-all.jpg" alt="Jaringan" />
									</div>
								</li>
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>BCA Virtual Account</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/bank-bca.jpg" alt="Bank BCA" />
									</div>
								</li>
							</ul>
						</div>
				    </li>
				    <li>
				    	<a rel="payment-tabs-5" class="link-button small block outline gray no-loader">Mini Market</a>
				    	<div id="payment-tabs-5" class="payment-tabs" style="display: none;">
							<ul class="payment-method-item">
								<li>
									<div class="selector">
										<input type="radio" name="pay">
										<label>Mini Market / Modern Retail</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/channel-alfamart.jpg" alt="Alfamart" />
										<img src="<?php echo $assets_url; ?>img/payment/channel-indomaret.jpg" alt="Indomaret" />
										<img src="<?php echo $assets_url; ?>img/payment/channel-posindonesia.jpg" alt="POS Indonesia" />
										<img src="<?php echo $assets_url; ?>img/payment/channel-pegadaian.jpg" alt="Pegadaian" />
									</div>
								</li>
							</ul>
						</div>
				    </li>
				</ul>
			</div>
		</div>
	</div>
</div>
<script src="<?php echo $assets_url; ?>js/jquery.countdown.min.js" type="text/javascript"></script>
