<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container pbottom-30">
	<div class="checkout-wrapper clearfix flight">
		<div class="cart-wrapper mbottom-20">
			<h1>Batas Waktu &amp; Total Pembayaran</h1>
			
			<div class="mtop-20 notification error text-center text-small">
				<h4 id="countdown" rel="<?php echo date('Y-m-d H:i:s', strtotime($book['flight']['flight_timelimit'])); ?>">
				<?php echo date('Y-m-d H:i:s', strtotime($book['flight']['flight_timelimit'])); ?></h4>
				<p>Pastikan Anda segera melakukan pembayaran sebelum batas waktu habis.</p>
			</div>
			
			<div class="mtop-20 notification text-center text-small">
				<h4><small>IDR</small> <?php echo set_number_format($book['flight']['flight_totalfare'], 0); ?></h4>
				<p><span class="text-error"><i class="fa fa-exclamation-triangle"></i></span> Pastikan <strong>Jumlah Pembayaran</strong> sesuai nilai yang tertera di atas.</p>
			</div>

			<h1>Rincian Pesanan</h1>
			<!-- DEPART -->
			<?php if ( ! empty($book['flight']['flight_departure'])) { ?>
				<?php $flight = $book['flight']; 
					$transit = explode("-", $flight['flight_infotransit']);
					$info_etd = $transit[0];
					$info_eta = $transit[1];

					$flightTime = explode("-", $flight['flight_time']);
					$time_etd = $flightTime[0];
					$time_eta = $flightTime[1];

					$flightRute = explode("-", $flight['flight_route']);
					$etd = $flightRute[0];
					$eta = $flightRute[1];
					
				?>
				<div class="cart-item has-image">
					<div class="clearfix">
						<div class="cart-image">
							<img src="<?php echo $storage_url; ?>default/airlines/<?php echo preg_replace('/\s+/', '', strtolower($flight['flight'])); ?>.png" alt="<?php echo $flight['flight']; ?>">
							<p>Direct</p>
						</div>
						<div class="cart-description">
							<ul class="route">
								<li class="active clearfix">
									<div class="desc">
										<h5><?php echo $flight['flight']; ?> <?php echo $flight['flight_code']; ?></h5>
									</div>
								</li>
								<li class="clearfix">
									<div class="icon"><i class="fa fa-circle-o"></i></div>
									<div class="schedule">
										<?php echo $etd.' - '.$time_etd; ?>
										(<?php echo date('d M Y', strtotime($flight['tanggal'])); ?>)
									</div>
								</li>
								<li class="clearfix">
									<div class="icon"><i class="fa fa-circle-o"></i></div>
									<div class="schedule">
										<?php echo $eta.' - '.$time_eta; ?>
										<?php if ((int) $time_eta < (int) $time_etd) { ?>
											(<?php echo date('d M Y', strtotime("+1 day", strtotime($flight['tanggal']))); ?>)
										<?php } else { ?>
											(<?php echo date('d M Y', strtotime($flight['tanggal'])); ?>)
										<?php } ?>
									</div>
								</li>
								<!-- <?php if ( ! empty($flight['flights_connecting'])) { ?>
									<?php foreach($flight['flights_connecting'] as $key2 => $conn) { ?>
									<li class="active clearfix">
										<div class="desc">
											<h5><?php echo ( ! empty($conn['airlines_name']) ? $conn['airlines_name'] : $flight['airline_name']) ; ?> <?php echo $conn['fno']; ?></h5>
										</div>
									</li>
									<li class="clearfix">
										<div class="icon"><i class="fa fa-circle-o"></i></div>
										<div class="schedule">
											<?php echo $conn['dep']; ?> - <?php echo $conn['etd']; ?> (<?php echo date('d M Y', strtotime($conn['date'])); ?>)
										</div>
									</li>
									<li class="clearfix">
										<div class="icon"><i class="fa fa-circle-o"></i></div>
										<div class="schedule">
											<?php echo $conn['arr']; ?> - <?php echo $conn['eta']; ?>
											<?php if ((int) substr($conn['eta'], 0, 2) < (int) substr($conn['etd'], 0, 2)) { ?>
												(<?php echo date('d M Y', strtotime("+1 day", strtotime($conn['date']))); ?>)
											<?php } else { ?>
												(<?php echo date('d M Y', strtotime($conn['date'])); ?>)
											<?php } ?>
										</div>
									</li>
									<?php } ?>
								<?php } ?> -->
							</ul>
						</div>
					</div>
					<?php if ( ! empty($book['flight']['flight_totalfare'])) { ?>
					<ul class="cart-price mtop-10">
						<?php foreach($book['flight']['flight_datapassengers_json'] as $key => $fares) { ?>
						<li class="clearfix">
							<div class="price-title"><?php echo ucwords($key); ?> (x<?php echo $fares['sitter']; ?>)</div>
							<div class="price-amount"><small>RP</small> <?php echo set_number_format($fares[$key . '_fare'], 0); ?></div>
						</li>
						<?php } ?>
					</ul>
					<?php } ?>
				</div>
			<?php } ?>
			<?php if ( ! empty($book['flight']['return']['schedule'])) { ?>
				<?php $flight = $book['flight']['return']['schedule']; ?>
				<div class="cart-item has-image">
					<div class="clearfix">
						<div class="cart-image">
							<img src="<?php echo $storage_url; ?>default/airlines/<?php echo preg_replace('/\s+/', '', strtolower($flight['airline_name'])); ?>.png" alt="<?php echo $flight['airline_name']; ?>">
							<p><?php echo ucwords($flight['flights_type']); ?></p>
						</div>
						<div class="cart-description">
							<ul class="route">
								<li class="active clearfix">
									<div class="desc">
										<h5><?php echo $flight['airline_name']; ?> <?php echo $flight['flights']['fno']; ?></h5>
									</div>
								</li>
								<li class="clearfix">
									<div class="icon"><i class="fa fa-circle-o"></i></div>
									<div class="schedule">
										<?php echo $flight['flights']['dep']; ?> - <?php echo $flight['flights']['etd']; ?> 
										(<?php echo date('d M Y', strtotime($flight['flights']['date'])); ?>)
									</div>
								</li>
								<li class="clearfix">
									<div class="icon"><i class="fa fa-circle-o"></i></div>
									<div class="schedule">
										<?php echo $flight['flights']['arr']; ?> - <?php echo $flight['flights']['eta']; ?>
										<?php if ((int) substr($flight['flights']['eta'], 0, 2) < (int) substr($flight['flights']['etd'], 0, 2)) { ?>
											(<?php echo date('d M Y', strtotime("+1 day", strtotime($flight['flights']['date']))); ?>)
										<?php } else { ?>
											(<?php echo date('d M Y', strtotime($flight['flights']['date'])); ?>)
										<?php } ?>
									</div>
								</li>
								<?php if ( ! empty($flight['flights_connecting'])) { ?>
									<?php foreach($flight['flights_connecting'] as $key2 => $conn) { ?>
									<li class="active clearfix">
										<div class="desc">
											<h5><?php echo ( ! empty($conn['airlines_name']) ? $conn['airlines_name'] : $flight['airline_name']) ; ?> <?php echo $conn['fno']; ?></h5>
										</div>
									</li>
									<li class="clearfix">
										<div class="icon"><i class="fa fa-circle-o"></i></div>
										<div class="schedule">
											<?php echo $conn['dep']; ?> - <?php echo $conn['etd']; ?> (<?php echo date('d M Y', strtotime($conn['date'])); ?>)
										</div>
									</li>
									<li class="clearfix">
										<div class="icon"><i class="fa fa-circle-o"></i></div>
										<div class="schedule">
											<?php echo $conn['arr']; ?> - <?php echo $conn['eta']; ?>
											<?php if ((int) substr($conn['eta'], 0, 2) < (int) substr($conn['etd'], 0, 2)) { ?>
												(<?php echo date('d M Y', strtotime("+1 day", strtotime($conn['date']))); ?>)
											<?php } else { ?>
												(<?php echo date('d M Y', strtotime($conn['date'])); ?>)
											<?php } ?>
										</div>
									</li>
									<?php } ?>
								<?php } ?>
							</ul>
						</div>
					</div>
					<?php if ( ! empty($book['flight']['return']['fares_detail'])) { ?>
					<ul class="cart-price mtop-10">
						<?php foreach($book['flight']['return']['fares_detail'] as $key => $fares) { ?>
						<li class="clearfix">
							<div class="price-title"><?php echo ucwords($key); ?> (x<?php echo $fares['sitter']; ?>)</div>
							<div class="price-amount"><small>RP</small> <?php echo set_number_format($fares[$key . '_fare'], 0); ?></div>
						</li>
						<?php } ?>
					</ul>
					<?php } ?>
				</div>
			<?php } ?>
		</div>
		
		<div class="input-wrapper">
		<form action="https://gih.appsku.tech/api/paymentdev" method="get">
			<h1>Metode Pembayaran</h1>
			<div class="mtop-20 notification text-small">
				<p><span class="text-error"><i class="fa fa-exclamation-triangle"></i></span> Silahkan pilih metode pembayaran dibawah ini.</p>
			</div>
			<div class="payment-method">
			
				<input type="hidden" name="transaction_id" value="<?=$book['flight']['book_id']?>">
				<input type="hidden" name="amount" value="<?=$book['flight']['total_fares']?>">
				<ul class="clearfix">
				    <li>
				    	<a rel="payment-tabs-1" class="link-button small block no-loader">Instant Payment</a>
						<div id="payment-tabs-1" class="payment-tabs">
							<ul class="payment-method-item">
								<li>
									<a href="https://apitiket.realtravel.co.id/transaction/payment/duitku_inquiry/ticket?invoice_number=<?= 'FL'.$book['flight']['kodebooking'] ?>&amp;channel_id=12" onclick="showLoader();">
										<div class="selector">
											<label>BNI Virtual Account</label>
										</div>
										<div class="thumbnail">
											<img src="https://storage.realtravel.co.id/payment/I1.PNG" alt="Bank BNI" />
										</div>
									</a>
								</li>
								<li>
									<a href="https://apitiket.realtravel.co.id/transaction/payment/duitku_inquiry/ticket?invoice_number=<?= 'FL'.$book['flight']['kodebooking'] ?>&amp;channel_id=7" onclick="showLoader();">
										<div class="selector">
											<label>BCA KlikPay</label>
										</div>
										<div class="thumbnail">
											<img src="<?php echo $assets_url; ?>img/payment/channel-bcaklikpay.jpg" alt="BCA Klikpay" />
										</div>
									</a>
								</li>
								<li>
									<a href="https://apitiket.realtravel.co.id/transaction/payment/duitku_inquiry/ticket?invoice_number=<?= 'FL'.$book['flight']['kodebooking'] ?>&channel_id=8" onclick="showLoader();">
										<div class="selector">
											<label>Permata Bank Virtual Account</label>
										</div>
										<div class="thumbnail">
											<img src="https://storage.realtravel.co.id/payment/BT.PNG" alt="Bank Permata" />
										</div>
									</a>
								</li>
								<li>
									<a href="https://apitiket.realtravel.co.id/transaction/payment/duitku_inquiry/ticket?invoice_number=<?= 'FL'.$book['flight']['kodebooking'] ?>&channel_id=9" onclick="showLoader();">
										<div class="selector">
											<label>My Bank Virtual Account</label>
										</div>
										<div class="thumbnail">
											<img src="https://storage.realtravel.co.id/payment/VA.PNG" alt="My Bank" />
										</div>
									</a>
								</li>
								<li>
									<a href="https://apitiket.realtravel.co.id/transaction/payment/duitku_inquiry/ticket?invoice_number=<?= 'FL'.$book['flight']['kodebooking'] ?>&channel_id=6" onclick="showLoader();">
										<div class="selector">
											<label>CIMB Niaga Virtual Account</label>
										</div>
										<div class="thumbnail">
											<img src="https://storage.realtravel.co.id/payment/B1.PNG" alt="CIMB Niaga" />
										</div>
									</a>
								</li>
							</ul>
						</div>
				    </li>
				    <!-- <li>
				    	<a rel="payment-tabs-2" class="link-button small block outline gray no-loader">Credit Card</a>
						<div id="payment-tabs-2" class="payment-tabs" style="display: none;">
							<ul class="payment-method-item">
								<li>
									<div class="selector">
										<input type="radio" name="pay" value="VC">
										<label>Kartu Kredit Visa / Mastercard semua Bank di Indonesia</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/card-visa.jpg" alt="Visa" />
										<img src="<?php echo $assets_url; ?>img/payment/card-master.jpg" alt="MasterCard" />
									</div>
								</li>
							</ul>
						</div>
				    </li> -->
				    <li>
				    	<a rel="payment-tabs-3" class="link-button small block outline gray no-loader">Lainnya</a>
						<div id="payment-tabs-3" class="payment-tabs" style="display: none;">
							<ul class="payment-method-item">
								<li>
									<a href="https://apitiket.realtravel.co.id/transaction/payment/transfer?type=package&invoice_number=<?= 'FL'.$book['flight']['kodebooking'] ?>" onclick="showLoader();">
										<div class="selector">
											<label>Bank Transfer ATM Bersama, Prima atau Alto</label>
										</div>
										<div class="thumbnail">
											<img src="https://storage.realtravel.co.id/payment/transfer.svg" alt="Bank Transfer" />
										</div>
									</a>
								</li>
								<li>
									<a href="https://apitiket.realtravel.co.id/transaction/payment/ipaymu_inquiry/package?invoice_number=<?= 'FL'.$book['flight']['kodebooking'] ?>&channel_id=13" onclick="showLoader();">
										<div class="selector">
											<label>Indomaret</label>
										</div>
										<div class="thumbnail">
											<img src="https://storage.realtravel.co.id/payment/indomaret.svg" alt="Indomaret" />
										</div>
									</a>
								</li>
								<li>
									<a href="https://apitiket.realtravel.co.id/transaction/payment/ipaymu_inquiry/package?invoice_number=<?= 'FL'.$book['flight']['kodebooking'] ?>&channel_id=14" onclick="showLoader();">
										<div class="selector">
											<label>Alfamart</label>
										</div>
										<div class="thumbnail">
											<img src="https://my.ipaymu.com/asset/images/alfamart.png" alt="Alfamart" />
										</div>
									</a>
								</li>
								<!-- <li>
									<div class="selector">
										<input type="radio" name="pay" value="BT">
										<label>Bank Permata</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/bank-permata.jpg" alt="XL Tunai" />
									</div>
								</li>
								<li>
									<div class="selector">
										<input type="radio" name="pay" value="B1">
										<label>CIMB Niaga</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/bank-cimb.jpg" alt="Telkomsel T-Cash" />
									</div>
								</li>
								<li>
									<div class="selector">
										<input type="radio" name="pay" value="I1">
										<label>Bank BNI</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/bank-bni.jpg" alt="Indosat Dompetku" />
									</div>
								</li>
								<li>
									<div class="selector">
										<input type="radio" name="pay" value="A1">
										<label>ATM Bersama</label>
									</div>
									<div class="thumbnail">
										<img src="<?php echo $assets_url; ?>img/payment/atm_bersama.png" alt="Mandiri e-cash" />
									</div>
								</li> -->
							</ul>
						</div>
				    </li>
				</ul>
			</div>
		</div>
		<hr class="mtop-10">
            <!-- <div class="mtop-20">
				<button type="submit" class="link-button large block">BAYAR SEKARANG</button>
            </div> -->
		</form>
	</div>
</div>
<script src="<?php echo $assets_url; ?>js/jquery.countdown.min.js" type="text/javascript"></script>

