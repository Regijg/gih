<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php
	foreach ($search_result as $key => $value)
	{
		$dateFlight = explode("-", $value['flight_datetime']);
		$flight_etd = $dateFlight[0];
		$flight_eta = $dateFlight[1];
		/** 
		$flight_dep 		= $value['flights']['dep'];
		$flight_dep_city 	= $value['flights']['dep_city'];
		$flight_arr 		= $value['flights']['arr'];
		$flight_arr_city 	= $value['flights']['arr_city'];
		**/
		$flight_dep 		= 'flight_dep';
		$flight_dep_city 	= 'flight_dep_city';;
		$flight_arr 		= 'flight_arr';;
		$flight_arr_city 	= 'flight_arry_city';;


		// if (array_key_exists('connecting_flight', $value) && is_array($value['connecting_flight']))
		// {
		// 	$connecting = end($value['connecting_flight']);
		// 	$conn_etd = ( !empty($connecting['etd']) && ($connecting['etd'] != ':') ? $connecting['etd'] : $flight_etd);
		// 	$flight_eta = ( !empty($connecting['eta']) && ($connecting['eta'] != ':') ? $connecting['eta'] : '00:00');
		// 	$flight_arr = $connecting['from'];
		// 	$flight_arr_city = $connecting['to'];

		// 	if ((int) substr($flight_eta, 0, 2) < (int) substr($conn_etd, 0, 2))
		// 	{
		// 		$eta = new DateTime(date('Y-m-d', strtotime("+1 day", strtotime($connecting['date']))) . ' ' . $flight_eta);
		// 	}
		// 	else
		// 	{
		// 		$eta = new DateTime($connecting['date'] . ' ' . $flight_eta);
		// 	}
		// }
		// else
		// {
			if ((int) substr($flight_eta, 0, 2) < (int) substr($flight_etd, 0, 2))
			{
				$eta = new DateTime(date('Y-m-d', strtotime("+1 day", strtotime($value['flight_date']))) . ' ' . $flight_eta);
			}
			else
			{
				$eta = new DateTime($value['flight_date'] . ' ' . $flight_eta);
			}
		// }

		$etd = new DateTime($value['flight_date'] . ' ' . $flight_etd);
		$interval = $etd->diff($eta);

		// $price_perclass = 0;
		// if (array_key_exists(0, $value['flight_price'])) {
			$price_perclass = (float) $value['flight_price'];
		// }
?>
	<li id="<?php echo preg_replace('/\s+/', '', strtolower($value['flight'])) . '-' . $search_schedule . '-' . $key; ?>" class="flight-item" data-type="<?php echo strtolower($value['type']); ?>" data-airline="<?php echo strtolower($value['flight']); ?>" data-etd="<?php echo $etd->format('U'); ?>" data-eta="<?php echo $eta->format('U'); ?>" data-duration="<?php echo (((int) $interval->h) * 60) + (int) $interval->i; ?>" 
		data-price="<?php echo  $price_perclass ?>" style="display: none;">
		<div class="preview clearfix">
			<div class="list-data flight-title">
				<img src="<?php echo $assets_url; ?>img/airlines/<?php echo preg_replace('/\s+/', '', strtolower($value['flight'])); ?>.png" alt="<?php echo $value['flight']; ?>">
				<p><?php echo $value['flight_code']; ?></p>
			</div>
			<div class="list-data flight-schedule clearfix">
				<div>
					<p class="time"><?php echo $flight_etd; ?></p>
					<p class="desc"><?php echo $value['flight_from'] ?></p>
				</div>
				<div>
					<p class="time"><?php echo $flight_eta; ?></p>
					<p class="desc"><?php echo $value['flight_to']; ?></p>
				</div>
				<div>
					<p class="time"><?php echo $interval->h; ?>j <?php echo $interval->i; ?>m</p>
					<p class="desc"><?php echo ucwords(strtolower($value['type'])); ?></p>
				</div>
			</div>
			<div class="list-data flight-price">
				<small>IDR</small>
				<p><?php echo set_number_format($price_perclass, 0, '', '', 'i'); ?></p>
			</div>
			<div class="list-data flight-action">
				<button type="button" class="link-button block action set-flight" 
				data-sc-ref-id="<?php echo $search_reference; ?>" 
				data-fno="<?php echo $value['flight_code'] ?>" 
				data-from="<?php echo $value['flight_from'] ?>" 
				data-to="<?php echo $value['flight_to'] ?>" 
				data-date="<?php echo $value['flight_date'] ?>" 
				data-airline-id="<?php echo $value['airline_id'] ?>"
				data-class-id="<?php echo $value['class'][0]['class_id'] ?>"
				data-id="<?php echo preg_replace('/\s+/', '', strtolower($value['flight'])) . '-' . $search_schedule . '-' . $key; ?>" data-schedule="<?php echo $search_schedule; ?>">PILIH</button>
			</div>
		</div>
		
	</li>
<?php } ?>