<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container pbottom-30">
	<div class="checkout-wrapper clearfix flight">
    	<form id="form-checkout" class="default-form" method="POST" action="<?php echo $app_url; ?>product/flight/payment" enctype="multipart/form-data">
			<div class="input-wrapper">
				<h1>Informasi Pelanggan</h1>
				<div class="form-group">
					<div class="form-item">
						<input type="text" name="cust_name" class="input-item" placeholder="Nama">
					</div>
					<div class="form-item">
						<input type="email" name="cust_email" class="input-item" placeholder="Email">
					</div>
					<div class="form-item">
						<input type="number" name="cust_phone" class="input-item" placeholder="No Telp">
					</div>
					<div class="mtop-20 mbottom-10 notification text-small">
						<p><span class="text-error"><i class="fa fa-exclamation-triangle"></i></span> Pastikan data pelanggan diisi dengan benar. <strong>email akan dijadikan username anda &amp; e-Ticket</strong> akan dikirim ke email di atas.</p>
					</div>
				</div>
				<h1 class="mtop-20">Informasi Penumpang</h1>
				<?php 
				$pass = 1;
				$adult = ( ! empty($cart['flight']['adult']) ? (int) $cart['flight']['adult'] : ( ! empty($cart['flight']['return']['fares_detail']['adult']['sitter']) ? (int) $cart['flight']['return']['fares_detail']['adult']['sitter'] : 0));
				$child = ( ! empty($cart['flight']['child']) ? (int) $cart['flight']['child'] : ( ! empty($cart['flight']['return']['fares_detail']['child']['sitter']) ? (int) $cart['flight']['return']['fares_detail']['child']['sitter'] : 0));
				$infant = ( ! empty($cart['flight']['infant']) ? (int) $cart['flight']['infant'] : ( ! empty($cart['flight']['return']['fares_detail']['infant']['sitter']) ? (int) $cart['flight']['return']['fares_detail']['infant']['sitter'] : 0));
				?>

				<?php for ($i = 0; $i < $adult; $i++) { ?>
					<input type="hidden" name="passenger[<?php echo $pass; ?>][type]" value="adult">
					<div class="form-group">
						<div class="group-title">Penumpang #<?php echo $pass; ?> (Dewasa)</div>
						<div class="form-item">
							<select name="passenger[<?php echo $pass; ?>][title]" class="input-item">
								<option selected disabled>Titel</option>
								<option value="mr">Tuan</option>
								<option value="ms">Nona</option>
								<option value="mrs">Nyonya</option>
							</select>
						</div>
						<div class="form-item">
							<input type="text" name="passenger[<?php echo $pass; ?>][firstname]" class="input-item" placeholder="Nama Depan">
						</div>
						<div class="form-item">
							<input type="text" name="passenger[<?php echo $pass; ?>][lastname]" class="input-item" placeholder="Nama Belakang">
						</div>
						<div class="form-item">
							<input type="text" name="passenger[<?php echo $pass; ?>][id]" class="input-item" placeholder="No Identitas">
						</div>
						<div class="form-item">
							<input type="text" name="passenger[<?php echo $pass; ?>][dob]" class="input-item datedob" placeholder="Tgl Lahir" readonly>
						</div>
					</div>
					<?php $pass++; ?>
				<?php } ?>

				<?php for ($i = 0; $i < $child; $i++) { ?>
					<input type="hidden" name="passenger[<?php echo $pass; ?>][type]" value="child">
					<div class="form-group">
						<div class="group-title">Penumpang #<?php echo $pass; ?> (Anak)</div>
						<div class="form-item">
							<select name="passenger[<?php echo $pass; ?>][title]" class="input-item">
								<option selected disabled>Titel</option>
								<option value="mr">Tuan</option>
								<option value="ms">Nona</option>
								<option value="mrs">Nyonya</option>
							</select>
						</div>
						<div class="form-item">
							<input type="text" name="passenger[<?php echo $pass; ?>][firstname]" class="input-item" placeholder="Nama Depan">
						</div>
						<div class="form-item">
							<input type="text" name="passenger[<?php echo $pass; ?>][lastname]" class="input-item" placeholder="Nama Belakang">
						</div>
						<div class="form-item">
							<input type="text" name="passenger[<?php echo $pass; ?>][id]" class="input-item" placeholder="No Identitas">
						</div>
						<div class="form-item">
							<input type="text" name="passenger[<?php echo $pass; ?>][dob]" class="input-item datedob" placeholder="Tgl Lahir" readonly>
						</div>
					</div>
					<?php $pass++; ?>
				<?php } ?>

				<?php for ($i = 0; $i < $infant; $i++) { ?>
					<input type="hidden" name="passenger[<?php echo $pass; ?>][type]" value="infant">
					<div class="form-group">
						<div class="group-title">Penumpang #<?php echo $pass; ?> (Bayi)</div>
						<div class="form-item">
							<select name="passenger[<?php echo $pass; ?>][title]" class="input-item">
								<option selected disabled>Titel</option>
								<option value="mr">Tuan</option>
								<option value="ms">Nona</option>
								<option value="mrs">Nyonya</option>
							</select>
						</div>
						<div class="form-item">
							<input type="text" name="passenger[<?php echo $pass; ?>][firstname]" class="input-item" placeholder="Nama Depan">
						</div>
						<div class="form-item">
							<input type="text" name="passenger[<?php echo $pass; ?>][lastname]" class="input-item" placeholder="Nama Belakang">
						</div>
						<div class="form-item">
							<input type="text" name="passenger[<?php echo $pass; ?>][id]" class="input-item" placeholder="No Identitas">
						</div>
						<div class="form-item">
							<input type="text" name="passenger[<?php echo $pass; ?>][dob]" class="input-item datedob" placeholder="Tgl Lahir" readonly>
						</div>
					</div>
					<?php $pass++; ?>
				<?php } ?>

				<div class="mtop-20 notification text-small">
					<p><span class="text-error"><i class="fa fa-exclamation-triangle"></i></span> Pastikan data penumpang diisi dengan benar. <strong>Validitas tiket</strong> berdasarkan data yang dimasukkan di atas.</p>
				</div>
			</div>
			<div class="cart-wrapper">
				<h1>Rincian Pesanan</h1>
				<?php if ( ! empty($cart)) {?>
					<?php $flight = $cart['flight']; 
						$dateFlight = explode("-", $flight['flight_time']);
						$flight_etd = $dateFlight[0];
						$flight_eta = $dateFlight[1];
					?>
					<div class="cart-item has-image">
						<div class="clearfix">
							<div class="cart-image">
								<img src="<?php echo $storage_url; ?>default/airlines/<?php echo preg_replace('/\s+/', '', strtolower($flight['flight'])); ?>.png" alt="<?php echo $flight['flight']; ?>">
								<p><?php echo ucwords($flight['flights_type']); ?></p>
							</div>
							<div class="cart-description">
								<ul class="route">
									<li class="active clearfix">
										<div class="desc">
											<h5><?php echo $flight['flight']; ?> <?php echo $flight['flight_code']; ?></h5>
										</div>
									</li>
									<li class="clearfix">
										<div class="icon"><i class="fa fa-circle-o"></i></div>
										<div class="schedule">
											<?php echo $flight['flight_from']; ?> - <?php echo $flight_etd; ?> 
											(<?php echo date('d M Y', strtotime($flight['flight_date'])); ?>)
										</div>
									</li>
									<li class="clearfix">
										<div class="icon"><i class="fa fa-circle-o"></i></div>
										<div class="schedule">
											<?php echo $flight['flight_to']; ?> - <?php echo $flight_eta; ?>
											<?php if ((int) $flight_eta < (int) $flight_etd) { ?>
												(<?php echo date('d M Y', strtotime("+1 day", strtotime($flight['flight_date']))); ?>)
											<?php } else { ?>
												(<?php echo date('d M Y', strtotime($flight['flight_date'])); ?>)
											<?php } ?>
										</div>
									</li>
									<?php if ( ! empty($flight['flights_connecting'])) { ?>
										<?php foreach($flight['flights_connecting'] as $key2 => $conn) { ?>
										<li class="active clearfix">
											<div class="desc">
												<h5><?php echo ( ! empty($conn['airlines_name']) ? $conn['airlines_name'] : $flight['airline_name']) ; ?> <?php echo $conn['fno']; ?></h5>
											</div>
										</li>
										<li class="clearfix">
											<div class="icon"><i class="fa fa-circle-o"></i></div>
											<div class="schedule">
												<?php echo $conn['dep']; ?> - <?php echo $conn['etd']; ?> (<?php echo date('d M Y', strtotime($conn['date'])); ?>)
											</div>
										</li>
										<li class="clearfix">
											<div class="icon"><i class="fa fa-circle-o"></i></div>
											<div class="schedule">
												<?php echo $conn['arr']; ?> - <?php echo $conn['eta']; ?>
												<?php if ((int) substr($conn['eta'], 0, 2) < (int) substr($conn['etd'], 0, 2)) { ?>
													(<?php echo date('d M Y', strtotime("+1 day", strtotime($conn['date']))); ?>)
												<?php } else { ?>
													(<?php echo date('d M Y', strtotime($conn['date'])); ?>)
												<?php } ?>
											</div>
										</li>
										<?php } ?>
									<?php } ?>
								</ul>
							</div>
						</div>
						<!-- <?php if ( ! empty($cart['flight'])) { ?>
						<ul class="cart-price mtop-10">
							<?php foreach($cart['flight']['depart']['fares_detail'] as $key => $fares) { ?>
							<li class="clearfix">
								<div class="price-title"><?php echo ucwords($key); ?> (x<?php echo $fares['sitter']; ?>)</div>
								<div class="price-amount"><small>RP</small> <?php echo set_number_format($fares[$key . '_fare'], 0); ?></div>
							</li>
							<?php } ?>
						</ul>
						<?php } ?> -->
					</div>
				<?php } ?>
				<?php if ( ! empty($cart['flight']['return']['schedule'])) { ?>
					<?php $flight = $cart['flight']['return']['schedule']; ?>
					<div class="cart-item has-image">
						<div class="clearfix">
							<div class="cart-image">
								<img src="<?php echo $storage_url; ?>default/airlines/<?php echo preg_replace('/\s+/', '', strtolower($flight['airline_name'])); ?>.png" alt="<?php echo $flight['airline_name']; ?>">
								<p><?php echo ucwords($flight['flights_type']); ?></p>
							</div>
							<div class="cart-description">
								<ul class="route">
									<li class="active clearfix">
										<div class="desc">
											<h5><?php echo $flight['airline_name']; ?> <?php echo $flight['flights']['fno']; ?></h5>
										</div>
									</li>
									<li class="clearfix">
										<div class="icon"><i class="fa fa-circle-o"></i></div>
										<div class="schedule">
											<?php echo $flight['flights']['dep']; ?> - <?php echo $flight['flights']['etd']; ?> 
											(<?php echo date('d M Y', strtotime($flight['flights']['date'])); ?>)
										</div>
									</li>
									<li class="clearfix">
										<div class="icon"><i class="fa fa-circle-o"></i></div>
										<div class="schedule">
											<?php echo $flight['flights']['arr']; ?> - <?php echo $flight['flights']['eta']; ?>
											<?php if ((int) substr($flight['flights']['eta'], 0, 2) < (int) substr($flight['flights']['etd'], 0, 2)) { ?>
												(<?php echo date('d M Y', strtotime("+1 day", strtotime($flight['flights']['date']))); ?>)
											<?php } else { ?>
												(<?php echo date('d M Y', strtotime($flight['flights']['date'])); ?>)
											<?php } ?>
										</div>
									</li>
									<?php if ( ! empty($flight['flights_connecting'])) { ?>
										<?php foreach($flight['flights_connecting'] as $key2 => $conn) { ?>
										<li class="active clearfix">
											<div class="desc">
												<h5><?php echo ( ! empty($conn['airlines_name']) ? $conn['airlines_name'] : $flight['airline_name']) ; ?> <?php echo $conn['fno']; ?></h5>
											</div>
										</li>
										<li class="clearfix">
											<div class="icon"><i class="fa fa-circle-o"></i></div>
											<div class="schedule">
												<?php echo $conn['dep']; ?> - <?php echo $conn['etd']; ?> (<?php echo date('d M Y', strtotime($conn['date'])); ?>)
											</div>
										</li>
										<li class="clearfix">
											<div class="icon"><i class="fa fa-circle-o"></i></div>
											<div class="schedule">
												<?php echo $conn['arr']; ?> - <?php echo $conn['eta']; ?>
												<?php if ((int) substr($conn['eta'], 0, 2) < (int) substr($conn['etd'], 0, 2)) { ?>
													(<?php echo date('d M Y', strtotime("+1 day", strtotime($conn['date']))); ?>)
												<?php } else { ?>
													(<?php echo date('d M Y', strtotime($conn['date'])); ?>)
												<?php } ?>
											</div>
										</li>
										<?php } ?>
									<?php } ?>
								</ul>
							</div>
						</div>
						<?php if ( ! empty($cart['flight']['return']['fares_detail'])) { ?>
						<ul class="cart-price mtop-10">
							<?php foreach($cart['flight']['return']['fares_detail'] as $key => $fares) { ?>
							<li class="clearfix">
								<div class="price-title"><?php echo ucwords($key); ?> (x<?php echo $fares['sitter']; ?>)</div>
								<div class="price-amount"><small>RP</small> <?php echo set_number_format($fares[$key . '_fare'], 0); ?></div>
							</li>
							<?php } ?>
						</ul>
						<?php } ?>
					</div>
				<?php } ?>
				<hr class="mtop-20">
				<div class="total-price clearfix">
					<div class="price-title">Total Harga</div>
					<div class="price-amount"><small>IDR</small> <?php echo number_format($cart['flight']['totalfare'],0,",",".") ?></div>
				</div>
				<hr class="mtop-10">
	            <div class="mtop-20">
					<button type="submit" class="link-button large block">PESAN SEKARANG</button>
	            </div>
			</div>
		</form>
	</div>
</div>