<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container ptop-50 pbottom-50">
	<div class="text-center">
		<div style="font-size: 50px;"><i class="fa fa-chain-broken"></i></div>
		<h2 class="mtop-10 text-primary">Error <?php echo $code; ?></h2>
		<p class="mtop-10"><?php echo $message; ?></p>
	</div>
</div>