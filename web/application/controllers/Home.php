<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends App_Controller {

	public function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
		// redirect('https://gih-indonesia.com/');
		// exit();
		// Load View
		$this->set_document('setting', array('search' => TRUE));

		$this->set_document('css', array(
			$this->data['assets_url'] . 'css/page.home.css'
		));

		$this->set_document('js', array(
			$this->data['assets_url'] . 'js/app/page.home.js'
		));

        $this->print_layout('home');
	}
}
