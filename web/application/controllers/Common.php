<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Common extends Base_Controller {

	public function __construct()
    {
        parent::__construct();
    }

	public function get_language()
    {
		$this->res['code']        = 200;
		$this->res['message']     = lang('code_' . $this->res['code']);
		$this->res['data']['res'] = $this->lang->language;

		$this->print_output();
    }

    public function get_airport()
    {
    	$get_airport = file_get_contents($this->data_dir . 'airport.json');
        $get_airport = json_decode($get_airport, 1);

       	$this->res['code']        = 200;
		$this->res['message']     = lang('code_' . $this->res['code']);
		$this->res['data']['res'] = $get_airport['data']['airports'];

		$this->print_output();
    }

    public function get_station()
    {
        $get_station = file_get_contents($this->data_dir . 'station.json');
        $get_station = json_decode($get_station, 1);

        $this->res['code']        = 200;
        $this->res['message']     = lang('code_' . $this->res['code']);
        $this->res['data']['res'] = $get_station['data']['station'];

        $this->print_output();
    }

    public function get_poi()
    {
        $this->load->library('curl');

        $this->res['code']       = 404;
        $this->res['message']    = lang('message_' . $this->res['code']);

        if ( ! empty($this->req['keyword']))
        {
            $req_url = $this->data['config']['api']['hotel_keyword']['url'];
            $req_params['api_key'] = $this->data['config']['api']['key'];
            $req_params['ws']   = $this->data['config']['api']['hotel_keyword']['service'];
            $req_params['call'] = $this->data['config']['api']['hotel_keyword']['call'];
            $req_params['word'] = $this->req['keyword'];
        
            $this->load->library('curl');
            $api_request = $this->curl->post($req_url, $req_params, '', FALSE);
            $api_request = json_decode($api_request, 1);

            if (isset($api_request['rest_no']) && ($api_request['rest_no'] == 0) 
                && ( ! empty($api_request['data'])))
            {
                $this->res['code']        = 200;
                $this->res['message']     = lang('code_' . $this->res['code']);
                $this->res['data']['res'] = $api_request['data'];
            }
        }
        
        $this->print_output();
    }

    public function get_iata($lat = '', $lang = '')
    {
        $this->load->library('curl');
        $api_request = $this->curl->get('http://iatageo.com/getCode/' . $lat . '/' . $lang, array(), '', FALSE);
        $api_request = json_decode($api_request, 1);

        if ( ! empty($api_request['code']))
        {
            $this->res['code']        = 200;
            $this->res['message']     = lang('code_' . $this->res['code']);
            $this->res['data']['res'] = $api_request;
        }
        else
        {
            $this->res['code']       = 404;
            $this->res['message']    = lang('message_' . $this->res['code']);
        }

        $this->set_output();
    }
}