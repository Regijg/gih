<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hotel extends App_Controller {

	public function __construct()
    {
        parent::__construct();

        $this->res['redirect'] = ( ! empty($this->req['redirect']) ? $this->req['redirect'] : $this->data['app_url'] . 'product/hotel');
        $this->data['cart'] = $this->session->userdata('cart-' . $this->config->item('encryption_key'));
        $this->data['book'] = $this->session->userdata('book-' . $this->config->item('encryption_key'));
    }

	public function index()
	{
		// Load View
		$this->set_document('setting', array('search' => 'hotel'));

		$this->set_document('css', array(
			$this->data['assets_url'] . 'css/page.campaign.css'
		));

		$this->set_document('js', array(
			$this->data['assets_url'] . 'js/app/page.campaign.js'
		));

        $this->print_layout('product/hotel/campaign_hotel');
	}

	public function search()
	{
		$params = $this->input->get();

		if (( ! empty($params['location'])) 
			&& ( ! empty($params['checkin'])) && ( ! empty($params['checkout'])) 
			&& ( ! empty($params['room'])) && ( ! empty($params['guest']))
			)
		{
			$this->data['search_url'] = $this->data['app_url'] . 'product/hotel/load?' . http_build_query($params, '', "&");

			// Load View
			$this->set_document('title', 'Pencarian Hotel');
			$this->set_document('setting', array('search' => FALSE));

			$this->set_document('css', array(
				$this->data['assets_url'] . 'css/page.search.css',
				$this->data['assets_url'] . 'css/page.search.hotel.css'
			));

			$this->set_document('js', array(
				$this->data['assets_url'] . 'js/app/page.search.hotel.js'
			));

	        $this->print_layout('product/hotel/search_hotel');
		}
		else
		{
			redirect($this->data['app_url'] . 'product/hotel');
		}
	}

	public function checkout()
	{
		if ( ! empty($this->req['data']))
		{
	        $data = $this->req['data'];

	        if ( ! empty($data['reference']) && ! empty($data['code']) && ! empty($data['room']))
        	{
	        	$req_url    = $this->data['config']['api']['hotel_fare']['url'];
	        	$req_params['api_key'] = $this->data['config']['api']['key'];
	        	$req_params['ws'] 	= $this->data['config']['api']['hotel_fare']['service'];
				$req_params['call'] = $this->data['config']['api']['hotel_fare']['call'];

				$req_params['detReferenceId'] 	= $data['reference'];
				$req_params['room_code'] 		= $data['code'];

	            $this->load->library('curl');
				$api_request = $this->curl->post($req_url, $req_params, '', FALSE);
				$api_request = json_decode($api_request, 1);
				
				if (isset($api_request['rest_no']) && ($api_request['rest_no'] == 0))
				{
					$this->data['cart']['hotel']['params']    = array('detail_reference' => $data['reference'], 'room_code' => $data['code'], 'room_booking' => $data['room']);
                	$this->data['cart']['hotel']['room']      = $api_request['data'];
					$this->session->set_userdata('cart-' . $this->config->item('encryption_key'), $this->data['cart']);

					// Updating RESPONSE data
		            $this->res['code'] 		= 200;
		            $this->res['message'] 	= lang('message_' . $this->res['code']);
		            $this->res['redirect'] 	= $this->data['app_url'] . 'product/hotel/checkout';
				}
				else
				{
					// Updating RESPONSE data
					$code 		= 400;
					$message 	= sprintf(lang('error_validation'), 'API Error', 'Terdapat Kesalahan dari Request API', (isset($api_request['reason']) ? $api_request['reason'] : lang('message_400')));

		            $this->res['code'] 		= $code;
		            $this->res['message'] 	= $message;
				}
	        }
	        else
	        {
	        	$code 		= 400;
				$message 	= sprintf(lang('error_message'), lang('code_' . $code), lang('message_' . $code));

	            $this->res['code'] 		= $code;
	            $this->res['message'] 	= $message;
	        }

        	$this->print_output();
		}
		else
		{
			if ( ! empty($this->data['cart']['hotel']))
			{
				// Load View
	            $this->set_document('title', 'Checkout');
	            $this->set_document('setting', array('search' => FALSE));

	            $this->set_document('css', array(
	                $this->data['assets_url'] . 'css/page.checkout.css',
	                $this->data['assets_url'] . 'css/page.checkout.hotel.css'
	            ));

	            $this->set_document('js', array(
	                $this->data['assets_url'] . 'js/app/page.checkout.hotel.js'
	            ));

		        $this->print_layout('product/hotel/checkout_hotel');
			}
			else
			{
				redirect($this->data['app_url'] . 'product/hotel');
			}
		}
	}

	public function payment()
	{
		if ( ! empty($this->req))
		{
	        if ( ! empty($this->data['cart']['hotel']['fareReferenceId']))
	        {
	        	$this->load->library(array('form_validation'));
	        	$rules[] = array('cust_name', 'trim|required|max_length[100]', 'Nama Lengkap');
	            $rules[] = array('cust_email', 'trim|required|valid_email|max_length[100]', 'Email');
	            $rules[] = array('cust_phone', 'trim|required|numeric|max_length[15]', 'No. Telepon');

	             // Validation guest
	            $guest = 1;

	            if ( ! empty($cart['hotel']['params']['room_booking']))
	            {
	                for ($i = 0; $i < $cart['hotel']['params']['room_booking']; $i++)
	                {
	                    $rules[] = array('guest_title_' . $guest, 'trim|required|regex_match[(mr|ms|mrs)]', 'Titel Tamu #' . $guest);
	                    $rules[] = array('guest_fullname_' . $guest, 'trim|required|max_length[100]', 'Nama Lengkap Tamu #' . $guest);

	                    $guest++;
	                }
	            }

	            set_rules($rules);

	            if (($this->form_validation->run() == TRUE))
	            {
	                $this->data['cart']['hotel']['guest'] = $this->req;
                
	                // Skip process
	                $this->session->unset_userdata('cart-' . $this->config->item('encryption_key'));
	                $this->session->set_userdata('book-' . $this->config->item('encryption_key'), $this->data['cart']);

            	}
				else
				{
					// Updating RESPONSE data
					$code 		= 400;
					$message 	= sprintf(lang('error_validation'), lang('code_' . $code), lang('message_' . $code), validation_errors());

		            $this->res['code'] 		= $code;
		            $this->res['message'] 	= $message;
		            $this->res['data'] 		= get_rules_error($rules);
				}
		    }
	        else
	        {
	        	$code 		= 400;
				$message 	= sprintf(lang('error_message'), lang('code_' . $code), lang('message_' . $code));

	            $this->res['code'] 		= $code;
	            $this->res['message'] 	= $message;
	        }

        	$this->print_output();
		}
		else
		{
			if ( ! empty($this->data['book']['hotel']))
			{
				// Load View
	            $this->set_document('title', 'Payment');
	            $this->set_document('setting', array('search' => FALSE));

	            $this->set_document('css', array(
	                $this->data['assets_url'] . 'css/page.checkout.css',
	                $this->data['assets_url'] . 'css/page.checkout.hotel.css'
	            ));

	            $this->set_document('js', array(
	                $this->data['assets_url'] . 'js/app/page.payment.hotel.js'
	            ));

		        $this->print_layout('product/hotel/payment_hotel');
			}
			else
			{
				redirect($this->data['app_url'] . 'product/hotel/checkout');
			}
		}
	}

	public function submit()
	{
		$this->load->library(array('form_validation'));

        $rules[] = array('location', 'trim|required', 'Lokasi Menginap');
        $rules[] = array('checkin', 'trim|required', 'Tanggal Check-In');
        $rules[] = array('checkout', 'trim|required', 'Tanggal Check-Out');
        $rules[] = array('room', 'trim|required|integer', 'Kamar');
        $rules[] = array('guest', 'trim|required|integer', 'Tamu per Kamar');

        set_rules($rules);

        if (($this->form_validation->run() == TRUE))
		{
			// Updating RESPONSE data
            $this->res['code'] 		= 200;
            $this->res['message'] 	= lang('message_' . $this->res['code']);
            $this->res['redirect'] 	= $this->data['app_url'] . 'product/hotel/search?' . http_build_query($this->req, '', "&");
		}
		else
		{
			// Updating RESPONSE data
			$code 		= 400;
			$message 	= sprintf(lang('error_validation'), lang('code_' . $code), lang('message_' . $code), validation_errors());

            $this->res['code'] 		= $code;
            $this->res['message'] 	= $message;
            $this->res['data'] 		= get_rules_error($rules);
		}

        $this->print_output();
	}

	public function remove($params = array())
	{
		// Default RESPONSE data
		$code 		= 400;
		$message 	= sprintf(lang('error_message'), lang('code_' . $code), lang('message_' . $code));

        $this->res['code'] 		= $code;
        $this->res['message'] 	= $message;

		if ( ! empty($params[0]))
		{
			switch ($params[0]) {
				case 'cart':
					if ( ! empty($this->data['cart']['hotel']))
					{
						unset($this->data['cart']['hotel']);
						$this->session->set_userdata('cart-' . $this->config->item('encryption_key'), $this->data['cart']);

						$this->res['code'] 		= 200;
            			$this->res['message'] 	= lang('message_' . $this->res['code']);
					}
				break;
				case 'book':
					if ( ! empty($this->data['book']['hotel']))
					{
						unset($this->data['book']['hotel']);
						$this->session->set_userdata('book-' . $this->config->item('encryption_key'), $this->data['book']);

						$this->res['code'] 		= 200;
            			$this->res['message'] 	= lang('message_' . $this->res['code']);
					}
				break;
			}
		}

        $this->print_output();
	}

	public function load($params = array())
	{
		// Detail hotel
		if ( ! empty($params[0]) && ($params[0] == 'detail'))
		{
			$this->res['code']        = 400;
			$this->res['message']     = lang('message_' . $this->res['code']);

			$params = $this->input->get();

			if ( ! empty($params['reference_id']) && ! empty($params['hotel_id']))
			{
				$this->res = $this->api_search(array('reference_id' => $params['reference_id'], 'hotel_id' => $params['hotel_id']), 'detail');
			}
		}
		else // Pencarian hotel
		{
			$params = $this->input->get();

			// Jika sudah ada reference_id
			if ( ! empty($params['reference_id']))
			{
				$page = ( ! empty($params['page']) ? (int) $params['page'] : 1);

				$this->res = $this->api_search(array('reference_id' => $params['reference_id'], 'page' => $page));
			}
			else // Jika belum ada
			{
				if (( ! empty($params['location'])) 
					&& ( ! empty($params['checkin'])) && ( ! empty($params['checkout'])) 
					&& ( ! empty($params['room'])) && ( ! empty($params['guest'])))
				{

					// Reconcile Parameters
					$params['keyword'] 		= $params['location'];
					$params['check_in'] 	= ( ! empty($params['checkin']) ? date('Y-m-d', strtotime($params['checkin'])) : date('Y-m-d', strtotime("+1 day")));
					$params['check_out'] 	= ( ! empty($params['checkout']) ? date('Y-m-d', strtotime($params['checkout'])) : $params['check_in']);
					$params['room'] 	= (((int) $params['room'] < 7) ? (int) $params['room'] : 7);
					$params['guest'] 	= (((int) $params['guest'] < 5) ? (int) $params['guest'] : 5);

					$this->res = $this->api_search($params);
				}
				else
				{
					$this->res['code']        = 400;
					$this->res['message']     = lang('message_' . $this->res['code']);
				}
			}
		}

		$this->print_output();
	}

	private function api_search($params = array(), $action = 'list')
	{
		$this->load->library('curl');

        $response['code'] 		= 404;
    	$response['message'] 	= lang('message_' . $response['code']);

		switch ($action)
		{
    		case 'list':
    			if ( ! empty($params['reference_id']))
				{
					$req_url    = $this->data['config']['api']['hotel_search_next']['url'];
			    	$req_params['api_key'] = $this->data['config']['api']['key'];
			    	$req_params['ws'] 	= $this->data['config']['api']['hotel_search_next']['service'];
					$req_params['call'] = $this->data['config']['api']['hotel_search_next']['call'];

					$req_params['findReferenceId'] = $params['reference_id'];
					$req_params['page'] = ( ! empty($params['page']) ? (int) $params['page'] : 1);
				}
				else
				{
					$req_url    = $this->data['config']['api']['hotel_search']['url'];
			    	$req_params['api_key'] = $this->data['config']['api']['key'];
			    	$req_params['ws'] 	= $this->data['config']['api']['hotel_search']['service'];
					$req_params['call'] = $this->data['config']['api']['hotel_search']['call'];
					
					$keySearch = explode(",", $params['keyword']);
					if(count($keySearch)==1):
						$keyword = $keySearch;
					else:
						$keyword = $keySearch[1];
					endif;
					$req_params['keyword'] 		= trim($keyword);
					$req_params['check_in'] 	= $params['check_in'];
					$req_params['check_out'] 	= $params['check_out'];
					$req_params['room'] 		= $params['room'];
					$req_params['guest'] 		= $params['guest'];
				}

				$api_request = $this->curl->post($req_url, $req_params, '', FALSE);
		        $api_request = json_decode($api_request, 1);

		        if (isset($api_request['rest_no']) && ($api_request['rest_no'] == 0))
		        {
		        	$this->data['search_reference'] = $api_request['data']['session_id'];
					$this->data['search_page'] 		= $api_request['data']['total_page'];
					$this->data['search_result'] 	= $api_request['data']['result_data'];

		    		$response['code'] 			= 200;
			    	$response['message'] 		= lang('message_' . $response['code']);
			    	$response['data']['res'] 	= $this->data['search_result'];
			    	$response['data']['ref'] 	= $this->data['search_reference'];
			    	$response['data']['page'] 	= $this->data['search_page'];
		    		$response['data']['content'] = $this->load->extview($this->theme . '/template/product/hotel/search_hotel_item', $this->data, TRUE);
		    	}
    		break;

    		case 'detail':
    			$req_url    = $this->data['config']['api']['hotel_search_detail']['url'];
		    	$req_params['api_key'] = $this->data['config']['api']['key'];
		    	$req_params['ws'] 	= $this->data['config']['api']['hotel_search_detail']['service'];
				$req_params['call'] = $this->data['config']['api']['hotel_search_detail']['call'];

				$req_params['findReferenceId'] = $params['reference_id'];
				$req_params['hotelId'] = ( ! empty($params['hotel_id']) ? $params['hotel_id'] : '');

				$api_request = $this->curl->post($req_url, $req_params, '', FALSE);
		        $api_request = json_decode($api_request, 1);

		    	if (isset($api_request['rest_no']) && ($api_request['rest_no'] == 0))
		        {
		        	$this->data['detail_reference'] = $params['reference_id'];
					$this->data['detail_hotel'] 	= $api_request['data']['result_data'];

		    		$response['code'] 			= 200;
			    	$response['message'] 		= lang('message_' . $response['code']);
			    	$response['data']['res'] 	= $this->data['detail_hotel'];
			    	$response['data']['ref'] 	= $this->data['detail_reference'];
		    		$response['data']['content'] = $this->load->extview($this->theme . '/template/product/hotel/search_hotel_detail', $this->data, TRUE);
		    	}
			break;
		}

        return $response;
	}

    public function validate_date($date)
    {
        if ( ! empty($date))
        {
            if ($date == date('d-m-Y', strtotime($date)))
            {
                return TRUE;
            }
            else
            {
                $this->form_validation->set_message('validate_date', 'Format tanggal salah. Gunakan d-m-Y.');
                
                return FALSE;
            }
        }
        else
        {
            return TRUE;
        }
    }
}
