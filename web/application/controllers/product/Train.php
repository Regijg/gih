<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Train extends App_Controller {

	public function __construct()
    {
        parent::__construct();

        $this->res['redirect'] = ( ! empty($this->req['redirect']) ? $this->req['redirect'] : $this->data['app_url'] . 'product/train');
        $this->data['cart'] = $this->session->userdata('cart-' . $this->config->item('encryption_key'));
        $this->data['book'] = $this->session->userdata('book-' . $this->config->item('encryption_key'));
    }

	public function index()
	{
		// DEFAULT STATION
		$get_stations = file_get_contents($this->data_dir . 'station.json');
        $get_stations = json_decode($get_stations, 1);

        $this->data['stations'] = $get_stations['data']['station'];

		// Load View
		$this->set_document('setting', array('search' => 'train'));

		$this->set_document('css', array(
			$this->data['assets_url'] . 'css/page.campaign.css'
		));

		$this->set_document('js', array(
			$this->data['assets_url'] . 'js/app/page.campaign.js'
		));

        $this->print_layout('product/train/campaign_train');
	}

	public function search()
	{
		$params = $this->input->get();

		if (( ! empty($params['from'])) && ( ! empty($params['from_label']))
			&& ( ! empty($params['to'])) && ( ! empty($params['to_label']))
			&& ( ! empty($params['depart'])) && ( ! empty($params['adult']))
			)
		{
			// DEFAULT STATION
			$get_stations = file_get_contents($this->data_dir . 'station.json');
	        $get_stations = json_decode($get_stations, 1);

	        $this->data['stations'] = $get_stations['data']['station'];

			$this->data['roundtrip'] = (( ! empty($params['return']) && ! empty($params['return'])) ? 'return' : 'oneway');
			$this->data['search_url'] = $this->data['app_url'] . 'product/train/load?' . http_build_query($params, '', "&");

			// Load View
			$this->set_document('title', 'Pencarian Kereta');
			$this->set_document('setting', array('search' => FALSE));

			$this->set_document('css', array(
				$this->data['assets_url'] . 'css/page.search.css',
				$this->data['assets_url'] . 'css/page.search.train.css'
			));

			$this->set_document('js', array(
				$this->data['assets_url'] . 'js/app/page.search.train.js'
			));

	        $this->print_layout('product/train/search_train');
		}
		else
		{
			redirect($this->data['app_url'] . 'product/train');
		}
	}

	public function checkout()
	{
		if ( ! empty($this->req['data']))
		{
	        $data = $this->req['data'];

	        if ( ! empty($data['depart']['reference']) && ! empty($data['depart']['schedule']) && ! empty($data['depart']['code']))
	        {
	        	$req_url    = $this->data['config']['api']['train_fare']['url'];
	        	$req_params['api_key'] = $this->data['config']['api']['key'];
	        	$req_params['ws'] 	= $this->data['config']['api']['train_fare']['service'];
				$req_params['call'] = $this->data['config']['api']['train_fare']['call'];

				$req_params['scReferenceId'] 	= $data['depart']['reference'];
				$req_params['schedule_id'] 		= $data['depart']['schedule'];
				$req_params['sub_class'] 		= $data['depart']['code'];

				if ( ! empty($data['return']['reference']) && ! empty($data['return']['schedule']) && ! empty($data['return']['code']))
	        	{
					$req_params['scReferenceId2'] 	= $data['return']['reference'];
					$req_params['schedule_id2'] 	= $data['return']['schedule'];
					$req_params['sub_class2'] 		= $data['return']['code'];
	        	}

	            $this->load->library('curl');

				$api_request = $this->curl->post($req_url, $req_params, '', FALSE);
				$api_request = json_decode($api_request, 1);

				if (isset($api_request['rest_no']) && ($api_request['rest_no'] == 0) && ! empty($api_request['data']['fareReferenceId']))
				{
					$this->data['cart']['train'] = $api_request['data'];
					$this->session->set_userdata('cart-' . $this->config->item('encryption_key'), $this->data['cart']);

					// Updating RESPONSE data
		            $this->res['code'] 		= 200;
		            $this->res['message'] 	= lang('message_' . $this->res['code']);
		            $this->res['redirect'] 	= $this->data['app_url'] . 'product/train/checkout';
				}
				else
				{
					// Updating RESPONSE data
					$code 		= 400;
					$message 	= sprintf(lang('error_validation'), 'API Error', 'Terdapat Kesalahan dari Request API', (isset($api_request['reason']) ? $api_request['reason'] : lang('message_400')));

		            $this->res['code'] 		= $code;
		            $this->res['message'] 	= $message;
				}
	        }
	        else
	        {
	        	$code 		= 400;
				$message 	= sprintf(lang('error_message'), lang('code_' . $code), lang('message_' . $code));

	            $this->res['code'] 		= $code;
	            $this->res['message'] 	= $message;
	        }

        	$this->print_output();
		}
		else
		{
			if ( ! empty($this->data['cart']['train']))
			{
				// Load View
	            $this->set_document('title', 'Checkout');
	            $this->set_document('setting', array('search' => FALSE));

	            $this->set_document('css', array(
	                $this->data['assets_url'] . 'css/page.checkout.css',
	                $this->data['assets_url'] . 'css/page.checkout.train.css'
	            ));

	            $this->set_document('js', array(
	                $this->data['assets_url'] . 'js/app/page.checkout.train.js'
	            ));

		        $this->print_layout('product/train/checkout_train');
			}
			else
			{
				redirect($this->data['app_url'] . 'product/train');
			}
		}
	}

	public function payment()
	{
		if ( ! empty($this->req))
		{
	        if ( ! empty($this->data['cart']['train']['fareReferenceId']))
	        {
	        	$this->load->library(array('form_validation'));
	        	$rules[] = array('cust_name', 'trim|required|max_length[100]', 'Nama Lengkap');
	            $rules[] = array('cust_email', 'trim|required|valid_email|max_length[100]', 'Email');
	            $rules[] = array('cust_phone', 'trim|required|numeric|max_length[15]', 'No. Telepon');

	            // Validation passenger
           		$pass = 1;

           		foreach ($this->req['passenger'] as $key => $value)
           		{
                    $rules[] = array('passenger['.$pass.'][type]', 'trim|required|regex_match[(adult|child|infant)]', 'Tipe Penumpang #' . $pass);
                    $rules[] = array('passenger['.$pass.'][name]', 'trim|required|max_length[100]', 'Nama Penumpang #' . $pass);
                    $rules[] = array('passenger['.$pass.'][id]', 'trim|required|max_length[50]', 'ID Penumpang #' . $pass);
                    $rules[] = array('passenger['.$pass.'][dob]', 'trim|required|max_length[10]|callback_validate_date', 'Tanggal Lahir Penumpang #' . $pass);
                    $rules[] = array('passenger['.$pass.'][phone]', 'trim|required|numeric|max_length[15]', 'No. Telepon #' . $pass);

                    $pass++;
           		}

	            set_rules($rules);

	            if (($this->form_validation->run() == TRUE))
	            {
	            	$params = $this->req;
	                $book = array();

	                $req_url    = $this->data['config']['api']['train_book']['url'];
		        	$req_params['api_key'] = $this->data['config']['api']['key'];
		        	$req_params['ws'] 	= $this->data['config']['api']['train_book']['service'];
					$req_params['call'] = $this->data['config']['api']['train_book']['call'];

					$req_params['fareReferenceId'] 	= $this->data['cart']['train']['fareReferenceId'];
					$req_params['customer_name'] 	= $params['cust_name'];
					$req_params['customer_email'] 	= $params['cust_email'];
					$req_params['customer_phone'] 	= $params['cust_phone'];

					$pass = 1;
					foreach ($this->req['passenger'] as $key => $value)
           			{
           				$req_params['pax_type_' . $pass]        = $params['passenger'][$pass]['type'];
                        $req_params['pax_name_' . $pass]  		= $params['passenger'][$pass]['name'];
                        $req_params['pax_id_no_' . $pass]       = $params['passenger'][$pass]['id'];
                        $req_params['pax_birthdate_' . $pass]	= date('Y-m-d', strtotime($params['passenger'][$pass]['dob']));
                        $req_params['pax_phone_' . $pass]       = $params['passenger'][$pass]['phone'];

                    	$pass++;
           			}

		            $this->load->library('curl');

					$api_request = $this->curl->post($req_url, $req_params, '', FALSE);
					$api_request = json_decode($api_request, 1);

					$now = date('Y-m-d H:i:s');
					$booking_limit = date('Y-m-d H:i:s', (strtotime($now) + (60*5)));

					$total = 0;
					if ( ! empty($this->data['cart']['train']['depart'])) $total++;
					if ( ! empty($this->data['cart']['train']['return'])) $total++;

					$success = 0;
					if (isset($api_request['rest_no']) && ($api_request['rest_no'] == 0) && ! empty($api_request['data']['book_id']))
					{
						$data = $api_request['data'];

						if (isset($data['depart']['book_details']['status']) && ($data['depart']['book_details']['status'] == 'book'))
						{
							$success++;

							if ( ! empty($data['depart']['book_details']['time_limit']))
	                        {
	                            $booking_limit = $data['depart']['book_details']['time_limit'];
	                        }
						}

						if (isset($data['return']['book_details']['status']) && ($data['return']['book_details']['status'] == 'book'))
						{
							$success++;

							if ( ! empty($data['return']['book_details']['time_limit']) 
								&& ((strtotime($booking_limit) > strtotime($data['return']['book_details']['time_limit']))))
                            {
                                $booking_limit = $data['return']['book_details']['time_limit'];
                            }
						}

						// Jika sukses semua booking
						if ($success == $total)
						{
							unset($this->data['cart']['train']);
							$this->session->set_userdata('cart-' . $this->config->item('encryption_key'), $this->data['cart']);

							$book['train'] = $data;
							$book['train']['booking_limit'] = $booking_limit;
							$this->session->set_userdata('book-' . $this->config->item('encryption_key'), $book);

							// Updating RESPONSE data
				            $this->res['code'] 		= 200;
				            $this->res['message'] 	= lang('message_' . $this->res['code']);
				            $this->res['redirect'] 	= $this->data['app_url'] . 'product/train/payment';
						}
						else
						{
							// Updating RESPONSE data
							$code 		= 400;
							$message 	= sprintf(lang('error_message'), 'Booking Gagal', 'Terjadi kegagalan proses booking');

				            $this->res['code'] 		= $code;
				            $this->res['message'] 	= $message;
						}
					}
					else
					{
						// Updating RESPONSE data
						$code 		= 400;
						$message 	= sprintf(lang('error_validation'), 'API Error', 'Terdapat Kesalahan dari Request API', (isset($api_request['reason']) ? $api_request['reason'] : lang('message_400')));

			            $this->res['code'] 		= $code;
			            $this->res['message'] 	= $message;
					}
            	}
				else
				{
					// Updating RESPONSE data
					$code 		= 400;
					$message 	= sprintf(lang('error_validation'), lang('code_' . $code), lang('message_' . $code), validation_errors());

		            $this->res['code'] 		= $code;
		            $this->res['message'] 	= $message;
		            $this->res['data'] 		= get_rules_error($rules);
				}
		    }
	        else
	        {
	        	$code 		= 400;
				$message 	= sprintf(lang('error_message'), lang('code_' . $code), lang('message_' . $code));

	            $this->res['code'] 		= $code;
	            $this->res['message'] 	= $message;
	        }

        	$this->print_output();
		}
		else
		{
			if ( ! empty($this->data['book']['train']))
			{
				// Load View
	            $this->set_document('title', 'Payment');
	            $this->set_document('setting', array('search' => FALSE));

	            $this->set_document('css', array(
	                $this->data['assets_url'] . 'css/page.checkout.css',
	                $this->data['assets_url'] . 'css/page.checkout.train.css'
	            ));

	            $this->set_document('js', array(
	                $this->data['assets_url'] . 'js/app/page.payment.train.js'
	            ));

		        $this->print_layout('product/train/payment_train');
			}
			else
			{
				redirect($this->data['app_url'] . 'product/train/checkout');
			}
		}
	}

	public function submit()
	{
		$this->load->library(array('form_validation'));

        $rules[] = array('from', 'trim|required', 'Stasiun Asal');
        $rules[] = array('to', 'trim|required', 'Stasiun Tujuan');
        $rules[] = array('depart', 'trim|required', 'Tanggal Pergi');
        $rules[] = array('return', 'trim', 'Tanggal Pulang');
        $rules[] = array('adult', 'trim|required|integer', 'Penumpang (Dewasa)');
        $rules[] = array('infant', 'trim|integer', 'Penumpang (Bayi)');

        set_rules($rules);

        if (($this->form_validation->run() == TRUE))
		{
			// Updating RESPONSE data
            $this->res['code'] 		= 200;
            $this->res['message'] 	= lang('message_' . $this->res['code']);
            $this->res['redirect'] 	= $this->data['app_url'] . 'product/train/search?' . http_build_query($this->req, '', "&");
		}
		else
		{
			// Updating RESPONSE data
			$code 		= 400;
			$message 	= sprintf(lang('error_validation'), lang('code_' . $code), lang('message_' . $code), validation_errors());

            $this->res['code'] 		= $code;
            $this->res['message'] 	= $message;
            $this->res['data'] 		= get_rules_error($rules);
		}

        $this->print_output();
	}

	public function remove($params = array())
	{
		// Default RESPONSE data
		$code 		= 400;
		$message 	= sprintf(lang('error_message'), lang('code_' . $code), lang('message_' . $code));

        $this->res['code'] 		= $code;
        $this->res['message'] 	= $message;

		if ( ! empty($params[0]))
		{
			switch ($params[0]) {
				case 'cart':
					if ( ! empty($this->data['cart']['train']))
					{
						unset($this->data['cart']['train']);
						$this->session->set_userdata('cart-' . $this->config->item('encryption_key'), $this->data['cart']);

						$this->res['code'] 		= 200;
            			$this->res['message'] 	= lang('message_' . $this->res['code']);
					}
				break;
				case 'book':
					if ( ! empty($this->data['book']['train']))
					{
						unset($this->data['book']['train']);
						$this->session->set_userdata('book-' . $this->config->item('encryption_key'), $this->data['book']);

						$this->res['code'] 		= 200;
            			$this->res['message'] 	= lang('message_' . $this->res['code']);
					}
				break;
			}
		}

        $this->print_output();
	}

	public function load()
	{
		$params = $this->input->get();

		if ( ! empty($params['from']) && ! empty($params['to']) && ! empty($params['adult']))
		{
			// Reconcile Parameters
			$params['depart'] = ( ! empty($params['depart']) ? date('Y-m-d', strtotime($params['depart'])) : date('Y-m-d', strtotime("+1 day")));
			$params['return'] = ( ! empty($params['return']) ? date('Y-m-d', strtotime($params['return'])) : '');

			$params['roundtrip'] = ( ! empty($params['return']) ? 'return' : 'oneway');

			$params['dep'] 			= $params['from'];
    		$params['arr'] 			= $params['to'];
    		$params['depart_date'] 	= $params['depart'];
    		$params['return_date'] 	= ( ! empty($params['return']) ? $params['return'] : $params['depart']);

			$this->res = $this->api_search($params);
		}
		else
		{
			$this->res['code']        = 400;
			$this->res['message']     = lang('message_' . $this->res['code']);
		}

		$this->print_output();
	}

	private function api_search($params = array())
	{
		$this->load->library('curl');

        $response['code'] 		= 404;
    	$response['message'] 	= lang('message_' . $response['code']);

		$req_url    = $this->data['config']['api']['train_search']['url'];
    	$req_params['api_key'] = $this->data['config']['api']['key'];
    	$req_params['ws'] 	= $this->data['config']['api']['train_search']['service'];
		$req_params['call'] = $this->data['config']['api']['train_search']['call'];

		$req_params['roundtrip'] = $params['roundtrip'];
		$req_params['dep'] 		= $params['dep'];
		$req_params['arr'] 		= $params['arr'];
		$req_params['depart_date'] = $params['depart_date'];
		$req_params['return_date'] = $params['return_date'];
		$req_params['adult'] = (((int) $params['adult'] < 7) ? (int) $params['adult'] : 7);
		$req_params['infant'] = 0;
		if ( ! empty($params['infant'])) $req_params['infant'] = (((int) $params['infant'] < 4) ? (int) $params['infant'] : 4);

    	$api_request = $this->curl->post($req_url, $req_params, '', FALSE);
		$api_request = json_decode($api_request, 1);

    	if ( ! empty($api_request['data']['scReferenceId']) &&  ! empty($api_request['data']['schedule']['depart']))
    	{
    		$this->data['search_reference'] = $api_request['data']['scReferenceId'];
			$this->data['search_result'] 	= $api_request['data']['schedule']['depart'];

    		$response['code'] 			= 200;
	    	$response['message'] 		= lang('message_' . $response['code']);
	    	$response['data']['res'] 	= $api_request['data']['schedule'];
	    	$response['data']['ref'] 	= $this->data['search_reference'];
    		$response['data']['depart']['content'] = $this->load->extview($this->theme . '/template/product/train/search_train_item', array_merge(array('search_schedule' => 'depart'), $this->data), TRUE);

    		if ( ! empty($api_request['data']['schedule']['return']))
    		{
				$this->data['search_result'] = $api_request['data']['schedule']['return'];
    			$response['data']['return']['content'] = $this->load->extview($this->theme . '/template/product/train/search_train_item', array_merge(array('search_schedule' => 'return'), $this->data), TRUE);
    		};
    	}

        return $response;
	}

    public function validate_date($date)
    {
        if ( ! empty($date))
        {
            if ($date == date('d-m-Y', strtotime($date)))
            {
                return TRUE;
            }
            else
            {
                $this->form_validation->set_message('validate_date', 'Format tanggal salah. Gunakan d-m-Y.');
                
                return FALSE;
            }
        }
        else
        {
            return TRUE;
        }
    }
}
