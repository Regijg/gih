<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Flight extends App_Controller {

	public function __construct()
    {
        parent::__construct();

        $this->res['redirect'] = ( ! empty($this->req['redirect']) ? $this->req['redirect'] : $this->data['app_url'] . 'product/flight');
        $this->data['cart'] = $this->session->userdata('cart-' . $this->config->item('encryption_key'));
        $this->data['book'] = $this->session->userdata('book-' . $this->config->item('encryption_key'));
    }

	public function index()
	{
		// DEFAULT AIRLINES
		$get_airlines = file_get_contents($this->data_dir . 'airline.json');
        $get_airlines = json_decode($get_airlines, 1);

        $this->data['airlines'] = $get_airlines['data']['airlines'];

		// Load View
		$this->set_document('setting', array('search' => 'flight'));
		$this->set_document('progress', array('step1' => 'active','step2'=>'','step3'=>'','step4'=>''));

		$this->set_document('css', array(
			$this->data['assets_url'] . 'css/page.campaign.css'
		));

		$this->set_document('js', array(
			$this->data['assets_url'] . 'js/app/page.campaign.js'
		));

        $this->print_layout('product/flight/campaign_flight');
	}

	public function search()
	{
		$params = $this->input->get();

		if (( ! empty($params['origin'])) && ( ! empty($params['origin_label']))
			&& ( ! empty($params['destination'])) && ( ! empty($params['destination_label']))
			&& ( ! empty($params['depart']))
			)
		{
			// DEFAULT AIRLINES
			$get_airlines = file_get_contents($this->data_dir . 'airline.json');
	        $get_airlines = json_decode($get_airlines, 1);

	        $this->data['airlines'] = $get_airlines['data']['airlines'];

			$this->data['roundtrip'] = (( ! empty($params['return']) && ! empty($params['return'])) ? 'return' : 'oneway');
			$this->data['search_url'] = $this->data['app_url'] . 'product/flight/load?' . http_build_query($params, '', "&");

			// Load View
			$this->set_document('progress', array('step1' => 'active','step2'=>'','step3'=>'','step4'=>''));
			$this->set_document('title', 'Pencarian Pesawat');
			$this->set_document('setting', array('search' => FALSE));

			$this->set_document('css', array(
				$this->data['assets_url'] . 'css/page.search.css',
				$this->data['assets_url'] . 'css/page.search.flight.css'
			));

			$this->set_document('js', array(
				$this->data['assets_url'] . 'js/app/page.search.flight.js'
			));

	        $this->print_layout('product/flight/search_flight');
		}
		else
		{
			redirect($this->data['app_url'] . 'product/flight');
		}
	}

	public function checkout()
	{
		if ( ! empty($this->req['data']))
		{
	        $data = $this->req['data'];

			if (empty($data['depart']['fno'])) {
				$code 		= 400;
				$message 	= sprintf(lang('error_message'), lang('code_' . $code), lang('message_' . $code));

				$this->res['code'] 		= $code;
				$this->res['message'] 	= $message;
				$this->res['post_data'] = $data;	
			}

			if (!empty($data['depart']['fno']))
	        {
	        	$req_url    = $this->data['config']['api']['flight_fare']['url'];
	        	$req_params['api_key'] = $this->data['config']['api']['key'];
	        	$req_params['ws'] 	= $this->data['config']['api']['flight_fare']['service'];
				$req_params['call'] = $this->data['config']['api']['flight_fare']['call'];

				// $req_params['from'] 	= $data['depart']['reference'];
				// $req_params['to'] 		= $data['depart']['class'];
				$req_params['from'] 		= $data['depart']['from'];
				$req_params['to'] 			= $data['depart']['to'];
				$req_params['date'] 		= $data['depart']['date'];
				$req_params['flight'] 		= $data['depart']['fno'];
				$req_params['adult'] 		= $data['depart']['passenger']['adult'];
				$req_params['child'] 		= $data['depart']['passenger']['child'];
				$req_params['infant'] 		= $data['depart']['passenger']['infant'];

				// if ( ! empty($data['return']['reference']) && ! empty($data['return']['fno']) && ! empty($data['return']['class']))
	        	// {
	        	// 	$req_params['scReferenceId_2'] 	= $data['return']['reference'];
				// 	$req_params['fno_2'] 	= $data['return']['fno'];
				// 	$req_params['class_id_2'] 	= $data['return']['class'];
	        	// }

	            $this->load->library('curl');

				$api_request = $this->curl->post($req_url, $req_params, '', FALSE);
				$api_request = json_decode($api_request, 1);

				if (isset($api_request['result']) && ($api_request['result'] == 'ok'))
				{
					$this->data['cart']['flight'] = $api_request;
					$this->session->set_userdata('cart-' . $this->config->item('encryption_key'), $this->data['cart']);

					// Updating RESPONSE data
		            $this->res['code'] 		= 200;
		            $this->res['message'] 	= lang('message_' . $this->res['code']);
		            $this->res['redirect'] 	= $this->data['app_url'] . 'product/flight/checkout';
				}
				else
				{
					// Updating RESPONSE data
					$code 		= 400;
					$message 	= sprintf(lang('error_validation'), 'API Error', 'Terdapat Kesalahan dari Request API', (isset($api_request['reason']) ? $api_request['reason'] : lang('message_400')));

		            $this->res['code'] 		= $code;
					$this->res['message'] 	= $message;
				}
	        }

        	$this->print_output();
		}
		else
		{
			if ( ! empty($this->data['cart']['flight']))
			{
				// Load View
				$this->set_document('progress', array('step1' => 'completed','step2'=>'active','step3'=>'','step4'=>''));
	            $this->set_document('title', 'Checkout');
	            $this->set_document('setting', array('search' => FALSE));

	            $this->set_document('css', array(
	                $this->data['assets_url'] . 'css/page.checkout.css',
	                $this->data['assets_url'] . 'css/page.checkout.flight.css'
	            ));

	            $this->set_document('js', array(
	                $this->data['assets_url'] . 'js/app/page.checkout.flight.js'
	            ));

		        $this->print_layout('product/flight/checkout_flight');
			}
			else
			{
				redirect($this->data['app_url'] . 'product/flight');
			}
		}
	}

	public function payment()
	{
		if ( ! empty($this->req))
		{
	        if ( ! empty($this->data['cart']['flight']))
	        {
	        	$this->load->library(array('form_validation'));
	        	$rules[] = array('cust_name', 'trim|required|max_length[100]', 'Nama Lengkap');
	            $rules[] = array('cust_email', 'trim|required|valid_email|max_length[100]', 'Email');
	            $rules[] = array('cust_phone', 'trim|required|numeric|max_length[15]', 'No. Telepon');

	            // Validation passenger
           		$pass = 1;

           		foreach ($this->req['passenger'] as $key => $value)
           		{
                    $rules[] = array('passenger['.$pass.'][type]', 'trim|required|regex_match[(adult|child|infant)]', 'Tipe Penumpang #' . $pass);
                    $rules[] = array('passenger['.$pass.'][title]', 'trim|required|regex_match[(mr|ms|mrs)]', 'Titel Penumpang #' . $pass);
                    $rules[] = array('passenger['.$pass.'][firstname]', 'trim|required|max_length[100]', 'Nama Depan Penumpang #' . $pass);
                    $rules[] = array('passenger['.$pass.'][lastname]', 'trim|required|max_length[100]', 'Nama Belakang Penumpang #' . $pass);
                    $rules[] = array('passenger['.$pass.'][id]', 'trim|max_length[50]', 'ID Penumpang #' . $pass);
                    $rules[] = array('passenger['.$pass.'][dob]', 'trim|max_length[10]|callback_validate_date', 'Tanggal Lahir Penumpang #' . $pass);

                    $pass++;
           		}

	            set_rules($rules);

	            if (($this->form_validation->run() == TRUE))
	            {
	            	$params = $this->req;
					$book = array();
					

	                $req_url    = $this->data['config']['api']['flight_book']['url'];
		        	$req_params['api_key'] = $this->data['config']['api']['key'];
		        	$req_params['ws'] 	= $this->data['config']['api']['flight_book']['service'];
					$req_params['call'] = $this->data['config']['api']['flight_book']['call'];

					$req_params['partner_id'] 	= 'Q1lvclBDVUUxV211dTYxaGZmL1dDQT09';
					$req_params['flight'] 		= $this->data['cart']['flight']['flight_code'];
					$req_params['from'] 		= $this->data['cart']['flight']['flight_from'];
					$req_params['to'] 			= $this->data['cart']['flight']['flight_to'];
					$req_params['date'] 		= $this->data['cart']['flight']['flight_date'];

					
					$req_params['adult'] 		= $this->data['cart']['flight']['adult'];
					$req_params['child'] 		= $this->data['cart']['flight']['child'];
					$req_params['infant'] 		= $this->data['cart']['flight']['infant'];

					$req_params['customer_name'] 	= $params['cust_name'];
					$req_params['customer_phone'] 	= $params['cust_phone'];
					$req_params['customer_email'] 	= $params['cust_email'];

					$pass = 1;
					foreach ($this->req['passenger'] as $key => $value)
           			{
           				$req_params['pax_type_' . $pass]        = $params['passenger'][$pass]['type'];
                        $req_params['pax_title_' . $pass]       = $params['passenger'][$pass]['title'];
                        $req_params['pax_name_' . $pass]  = $params['passenger'][$pass]['firstname'];
                        $req_params['pax_last_name_' . $pass]	= $params['passenger'][$pass]['lastname'];
                        $req_params['pax_id_no_' . $pass]       = $params['passenger'][$pass]['id'];
                        $req_params['pax_birthdate_' . $pass]	= date('Y-m-d', strtotime($params['passenger'][$pass]['dob']));
						$req_params['pax_baggage_'. $pass] = '';
						$req_params['pax_pasport_'. $pass] = '';
						$req_params['pax_expired_pasport_'. $pass] = '';
                    	$pass++;
           			}

		            $this->load->library('curl');

					$api_request = $this->curl->post($req_url, $req_params, '', FALSE);
					$api_request = json_decode($api_request, 1);

					$now = date('Y-m-d H:i:s');
					$booking_limit = date('Y-m-d H:i:s', (strtotime($now) + (60*5)));

					$total = 0;
					if ( ! empty($this->data['cart']['flight']['flight_departure'])) $total++;
					if ( ! empty($this->data['cart']['flight']['return'])) $total++;

					$success = 0;
					if (isset($api_request['depart']) && ($api_request['depart']['result'] == 'ok') && ! empty($api_request['depart']['tid']))
					{
						$data = $api_request['depart'];

						if (isset($data['flight_statusbooking']) && ($data['flight_statusbooking'] == 'waiting'))
						{
							$success++;

							if ( ! empty($data['flight_timelimit']))
	                        {
	                            $booking_limit = $data['flight_timelimit'];
	                        }
						}

						if (isset($data['return']['booking_status']) && ($data['return']['booking_status'] == 'waiting'))
						{
							$success++;

							if ( ! empty($data['return']['flight_timelimit']) 
								&& ((strtotime($booking_limit) > strtotime($data['return']['flight_timelimit']))))
                            {
                                $booking_limit = $data['return']['flight_timelimit'];
                            }
						}
						
						// Jika sukses semua booking
						if ($success == $total)
						{
							unset($this->data['cart']['flight']);
							$this->session->set_userdata('cart-' . $this->config->item('encryption_key'), $this->data['cart']);

							$book['flight'] = $data;
							$book['flight']['flight_timelimit'] = $booking_limit;
							$this->session->set_userdata('book-' . $this->config->item('encryption_key'), $book);

							// Updating RESPONSE data
				            $this->res['code'] 		= 200;
				            $this->res['message'] 	= lang('message_' . $this->res['code']);
				            $this->res['redirect'] 	= $this->data['app_url'] . 'product/flight/payment';
						}
						else
						{
							// Updating RESPONSE data
							$code 		= 400;
							$message 	= sprintf(lang('error_message'), 'Booking Gagal', 'Terjadi kegagalan proses booking');

				            $this->res['code'] 		= $code;
				            $this->res['message'] 	= $message;
						}
					}
					else
					{
						// Updating RESPONSE data
						$code 		= 400;
						$message 	= sprintf(lang('error_validation'), 'API Error', 'Terdapat Kesalahan dari Request API', (isset($api_request['reason']) ? $api_request['reason'] : lang('message_400')));

			            $this->res['code'] 		= $code;
						$this->res['messagee'] 	= $message;
						$this->res['req_params'] = $req_params;
						$this->res['req_result'] = $api_request;
					}
            	}
				else
				{
					// Updating RESPONSE data
					$code 		= 400;
					$message 	= sprintf(lang('error_validation'), lang('code_' . $code), lang('message_' . $code), validation_errors());

		            $this->res['code'] 		= $code;
		            $this->res['message'] 	= $message;
		            $this->res['data'] 		= get_rules_error($rules);
				}
		    }
	        else
	        {
	        	$code 		= 400;
				$message 	= sprintf(lang('error_message'), lang('code_' . $code), lang('message_' . $code));

	            $this->res['code'] 		= $code;
	            $this->res['message'] 	= $message;
	        }

        	$this->print_output();
		}
		else
		{
			if ( ! empty($this->data['book']['flight']))
			{
				// Load View
				$this->set_document('progress', array('step1' => 'completed','step2'=>'completed','step3'=>'active','step4'=>''));
	            $this->set_document('title', 'Payment');
	            $this->set_document('setting', array('search' => FALSE));

	            $this->set_document('css', array(
	                $this->data['assets_url'] . 'css/page.checkout.css',
	                $this->data['assets_url'] . 'css/page.checkout.flight.css'
	            ));

	            $this->set_document('js', array(
	                $this->data['assets_url'] . 'js/app/page.payment.flight.js'
	            ));

		        $this->print_layout('product/flight/payment_flight');
			}
			else
			{
				redirect($this->data['app_url'] . 'product/flight/checkout');
			}
		}
	}

	public function submit()
	{
		$this->load->library(array('form_validation'));

        $rules[] = array('origin', 'trim|required', 'Kota Asal');
        $rules[] = array('destination', 'trim|required', 'Kota Tujuan');
        $rules[] = array('depart', 'trim|required', 'Tanggal Pergi');

        set_rules($rules);

        if (($this->form_validation->run() == TRUE))
		{
			// Updating RESPONSE data
            $this->res['code'] 		= 200;
            $this->res['message'] 	= lang('message_' . $this->res['code']);
            $this->res['redirect'] 	= $this->data['app_url'] . 'product/flight/search?' . http_build_query($this->req, '', "&");
		}
		else
		{
			// Updating RESPONSE data
			$code 		= 400;
			$message 	= sprintf(lang('error_validation'), lang('code_' . $code), lang('message_' . $code), validation_errors());

            $this->res['code'] 		= $code;
            $this->res['message'] 	= $message;
            $this->res['data'] 		= get_rules_error($rules);
		}

        $this->print_output();
	}

	public function load()
	{
		$params = $this->input->get();

		// Jika sudah ada identification_id
		if ( ! empty($params['identification_id']))
		{
			$schedule = (( ! empty($params['schedule']) && in_array($params['schedule'], array('depart', 'return'))) ? $params['schedule'] : 'depart');

			$this->res = $this->api_search(array('identification_id' => $params['identification_id'], 'schedule' => $schedule));
		}
		else // Jika belum ada
		{
			if ( ! empty($params['origin']) && ! empty($params['destination']))
			{
				// Reconcile Parameters
				$params['depart'] = ( ! empty($params['depart']) ? date('Y-m-d', strtotime($params['depart'])) : date('Y-m-d', strtotime("+1 day")));
				$params['return'] = ( ! empty($params['return']) ? date('Y-m-d', strtotime($params['return'])) : '');

				if ( ! empty($params['airlines']))
				{
					$params['airlines'] = explode(',', $params['airlines']);
				}
				else
				{
					// DEFAULT AIRLINES
					$get_airlines = file_get_contents($this->data_dir . 'airline.json');
			        $get_airlines = json_decode($get_airlines, 1);

			        $params['airlines'] = array();
			        foreach ($get_airlines['data']['airlines'] as $key => $value)
			        {
			        	$params['airlines'][] = $value['airline_id'];
			        }
				}

				// API Process
				$result = array();

		    	// Depart
		    	if ( ! empty($params['depart']))
		    	{
					$params['from'] 		= $params['origin'];
					$params['to'] 			= $params['destination'];
					$params['date'] 		= $params['depart'];

					$request = $this->api_search($params);

					if (isset($request['code']) && ($request['code'] == 200))
					{
						$result['depart'][] = $request['data']['id'];
					}
				}
				
				// Return
		    	if ( ! empty($params['return']))
		    	{
					$params['airline_id'] 	= $airline;
					$params['from'] 			= $params['destination'];
					$params['to'] 			= $params['origin'];
					$params['date'] 		= $params['return'];

					$request = $this->api_search($params);

					if (isset($request['code']) && ($request['code'] == 200))
					{
						$result['return'][] = $request['data']['id'];
					}
		    	}

		    	if ( ! empty($result))
		    	{
			    	$this->res['code']        = 200;
					$this->res['message']     = lang('message_' . $this->res['code']);
					$this->res['data']['res'] = $result;
				}
				else
				{
					$this->res['code']        = 500;
					$this->res['message']     = lang('message_' . $this->res['code']);
				}
			}
			else
			{
				$this->res['code']        = 400;
				$this->res['message']     = lang('message_' . $this->res['code']);
			}
		}

		$this->print_output();
	}

	public function remove($params = array())
	{
		// Default RESPONSE data
		$code 		= 400;
		$message 	= sprintf(lang('error_message'), lang('code_' . $code), lang('message_' . $code));

        $this->res['code'] 		= $code;
        $this->res['message'] 	= $message;

		if ( ! empty($params[0]))
		{
			switch ($params[0]) {
				case 'cart':
					if ( ! empty($this->data['cart']['flight']))
					{
						unset($this->data['cart']['flight']);
						$this->session->set_userdata('cart-' . $this->config->item('encryption_key'), $this->data['cart']);

						$this->res['code'] 		= 200;
            			$this->res['message'] 	= lang('message_' . $this->res['code']);
					}
				break;
				case 'book':
					if ( ! empty($this->data['book']['flight']))
					{
						unset($this->data['book']['flight']);
						$this->session->set_userdata('book-' . $this->config->item('encryption_key'), $this->data['book']);

						$this->res['code'] 		= 200;
            			$this->res['message'] 	= lang('message_' . $this->res['code']);
					}
				break;
			}
		}

        $this->print_output();
	}

	private function api_search($params = array())
	{
		$this->load->library('curl');

        $response['code'] 		= 404;
    	$response['message'] 	= lang('message_' . $response['code']);
		if ( ! empty($params['identification_id']))
		{
			
        	$req_url     = $this->data['config']['api']['flight_search']['url'] . '/poolflight/getData/' . $params['identification_id'];
        	$api_request = $this->curl->get($req_url, '', '', FALSE);
			$api_request = json_decode($api_request, 1);

        	if ( ! empty($api_request['message']))
        	{
        		$response['code'] 		= 100;
		    	$response['message'] 	= lang('message_' . $response['code']);
		    	$response['data']['id'] = $params['identification_id'];
        	}
        	elseif (isset($api_request))
        	{
        		// if ( ! empty($api_request))
        		// {
        			$this->data['search_reference'] = $api_request['data']['scReferenceId'];
        			$this->data['search_result'] 	= $api_request;
        			$this->data['search_schedule'] 	= $params['schedule'];
        			
        			$response['code'] 		 = 200;
		    		$response['message'] 	 = lang('message_' . $response['code']);
		    		$response['data']['res'] = $api_request;
		    		$response['data']['content'] = $this->load->extview($this->theme . '/template/product/flight/search_flight_item', $this->data, TRUE);
		    	// }
        	}
        }
        else
        {
        	$req_url    = $this->data['config']['api']['flight_search']['url'] . '/poolflight';
        	$req_params['api_key'] = $this->data['config']['api']['key'];
        	$req_params['ws'] 	= $this->data['config']['api']['flight_search']['service'];
			$req_params['call'] = $this->data['config']['api']['flight_search']['call'];

			// $req_params['airline_id'] = $params['airline_id'];
			$req_params['from'] = $params['origin'];
			$req_params['to'] = $params['destination'];
			$req_params['date'] = $params['depart'];
			
			$api_request = $this->curl->post($req_url, $req_params, '', FALSE);
			$api_request = json_decode($api_request, 1);
			
        	if ( ! empty($api_request['identification_id']))
        	{
        		$response['code'] 		= 200;
		    	$response['message'] 	= lang('message_' . $response['code']);
		    	$response['data']['id']	= $api_request['identification_id'];
        	}
        }

        return $response;
	}

    public function validate_date($date)
    {
        if ( ! empty($date))
        {
            if ($date == date('d-m-Y', strtotime($date)))
            {
                return TRUE;
            }
            else
            {
                $this->form_validation->set_message('validate_date', 'Format tanggal salah. Gunakan d-m-Y.');
                
                return FALSE;
            }
        }
        else
        {
            return TRUE;
        }
    }
}