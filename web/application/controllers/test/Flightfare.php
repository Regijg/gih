<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Flightfare extends CI_Controller 
{

    public function get_faredetail()
    {
        $this->load->library('curl');
        $req_params['api_key'] = '2222';
        $req_params['ws'] = 'flight';
        $req_params['call'] = 'ws_faredetail';
        $req_params['scReferenceId'] = '-pQNVjpaqdvRGWxZWHjy_N4sKTlMDf-WohhRc_xvCNY';
        $req_params['fno'] 	= 'QG 684';
        $req_params['class_id'] = 'DQG1';
        $request_result = $this->curl->post('http://gih.appsku.xyz/api/ws', $req_params, '', FALSE);
        $result = json_decode($request_result, 1);
        echo json_encode($result);
    }
}