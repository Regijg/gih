<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Error_page extends Base_Controller {

	public function __construct()
    {
    	parent::__construct();
    }

	public function index($code = '404')
	{
		// Updating RESPONSE data
		$message = sprintf(lang('error_message'), lang('code_' . $code), lang('message_' . $code));

        $this->data['code'] 	= $code;
        $this->data['message'] 	= $message;

        $this->data['homepage_url'] = $this->data['app_url'];

		$this->print_layout('error');
	}
}
