<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Info extends App_Controller {

	public function __construct()
    {
        parent::__construct();
    }

	public function index($params = array())
	{
		$this->data['info_type'] = ( ! empty($params[0]) ? $params[0] : 'about');
		
		// Load View
		$this->set_document('setting', array('search' => FALSE));

		$this->set_document('css', array(
			$this->data['assets_url'] . 'css/page.info.css',
		));

		$this->set_document('js', array(
			$this->data['assets_url'] . 'js/app/page.info.js',
		));

        $this->print_layout('info');
	}
}
