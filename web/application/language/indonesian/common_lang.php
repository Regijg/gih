<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['lang_code']      = 'id';
$lang['lang_locale']    = 'id_ID';
$lang['lang_name']      = 'Indonesian';

// Currency
$lang['prefix_symbol']     	= 'Rp ';
$lang['suffix_symbol']     	= '';
$lang['decimal_symbol']     = ',';
$lang['thousands_symbol']   = '.';

$lang['error_message'] 		= '(%s) %s';
$lang['error_validation'] 	= '<small>(%s) %s</small> <br> %s';
$lang['error_common'] 		= 'Terjadi kesalahan.';
$lang['error_parameter'] 	= 'Parameter tidak valid.';
$lang['error_not_found'] 	= 'Data tidak ditemukan.';
$lang['error_captcha'] 		= 'Silahkan verifikasi captcha dengan benar.';
$lang['error_accept_terms'] = 'Silahkan setujui Syarat &amp; Ketentuan.';

$lang['success_save'] 		= 'Data berhasil disimpan.';
$lang['success_remove'] 	= 'Data berhasil dihapus.';
$lang['success_transaction'] = 'Transaksi berhasil dilakukan.';

$lang['message_empty_data'] = 'Tidak ada data tersedia.';
$lang['text_pending'] 	= 'Pending';
$lang['text_pipeline'] 	= 'Pipeline';
$lang['text_process'] 	= 'Process';
$lang['text_approve'] 	= 'Disetujui';
$lang['text_reject'] 	= 'Ditolak';
