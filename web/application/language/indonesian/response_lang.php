<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// HTTP Response
// 1XX Informational
$lang['code_100'] 	 = 'Continue';
$lang['message_100'] = 'Melanjutkan proses.';

// 2XX Success
$lang['code_200'] 	 = 'OK';
$lang['message_200'] = 'Sukses.';

// 3XX Redirection
$lang['code_300'] 	 = 'Multiple Choices';
$lang['message_300'] = 'Terdapat beberapa pilihan dari hasil.';

// 4XX Client Error
$lang['code_400'] 	 = 'Bad Request';
$lang['message_400'] = 'Parameter permintaan tidak valid.';
$lang['code_401'] 	 = 'Unauthorized';
$lang['message_401'] = 'Dibutuhkan otorisasi terlebih dahulu.';
$lang['code_402'] 	 = 'Payment Required';
$lang['message_402'] = 'Silahkan tentukan pembayaran terlebih dahulu.';
$lang['code_403'] 	 = 'Forbidden';
$lang['message_403'] = 'Dibutuhkan izin untuk melakukan ini.';
$lang['code_404'] 	 = 'Not Found';
$lang['message_404'] = 'Permintaan tidak ditemukan.';
$lang['code_498'] 	 = 'Invalid Token';
$lang['message_498'] = 'Token kadaluarsa atau tidak valid.';

// 5XX Server Error
$lang['code_500'] = 'Internal Server Error';
$lang['message_500'] = 'Terdapat kesalahan dalam sistem / server.';
