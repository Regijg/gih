<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * App Controller
 * @author Asep Fajar Nugraha <delve_brain@hotmail.com>
 */
class App_Controller extends Base_Controller {
    protected $req;
    protected $res;
    protected $ajax = FALSE;
    protected $data = array();
    protected $document = array();
    protected $theme = 'default';

	public function __construct()
    {
        parent::__construct();

        // Check Application
        $this->check_application();
    }

    public function _remap($method, $params = array())
    {
        if (method_exists($this, $method))
        {
            // Fetching router informations
            // Directory: $this->router->fetch_directory()
            // Class: $this->router->fetch_class()
            // Method: $this->router->fetch_method()

            return call_user_func(array($this, $method), $params);
        }
        else
        {
            if ($this->ajax)
            {
                $this->res['code']      = 404;
                $this->res['message']   = lang('message_' . $this->res['code']);

                $this->print_output();
            }
            else
            {
                redirect($this->data['app_url'] . 'error/404');
            }
        }
    }

    protected function check_application()
    {
        $valid_login = TRUE; // Do not net login to access App_Controller and childs

        if ( ! empty($this->data['login']))
        {
            $valid_login = TRUE;
        }

        if ($valid_login === FALSE)
        {
            $this->res['code']      = (isset($code) ? $code : 401);
            $this->res['message']   = (isset($message) ? $message : sprintf(lang('error_message'), lang('code_' . $this->res['code']), lang('message_' . $this->res['code'])));
            $this->res['redirect']  = $this->data['app_url'] . 'login';

            $this->print_output();
        }
    }

    protected function update_current_deposit()
    {
        $req_url = $this->data['config']['api']['check_deposit']['url'];
        $req_params['api_key']  = $this->data['config']['api']['key'];
        $req_params['ws']   = $this->data['config']['api']['check_deposit']['service'];
        $req_params['call'] = $this->data['config']['api']['check_deposit']['call'];

        $req_params['partner_id'] = $this->data['login']['id'];

        $this->load->library('curl');
        $api_request = $this->curl->post($req_url, $req_params, '', FALSE);
        $api_request = json_decode($api_request, 1);

        $current_deposit = (isset($api_request['rest_no']) && ($api_request['rest_no'] == 0) ? $api_request['data']['current_deposit'] : 0);

        $this->data['login']['current_deposit'] = $current_deposit;
        $this->session->set_userdata('login-' . $this->config->item('encryption_key'), $this->data['login']);
    }
}
