<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Base Model
 * @author Asep Fajar Nugraha <delve_brain@hotmail.com>
 */
class Base_Model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    protected function sanitize($data = array())
    {
        if (is_array($data))
        {
            foreach ($data as $key => $value)
            {
                unset($data[$key]);

                $data[$this->sanitize($key)] = $this->sanitize($value);
            }
        }
        else
        {
            if ( ! is_object($data))
            {
                $data = trim($this->db->escape_str(htmlspecialchars($data, ENT_QUOTES, 'UTF-8')));
            }
        }

        return $data;
    }

    protected function build_condition($args = array(), $table)
    {
        $conditions = array();

        $args = $this->sanitize($args);
        foreach ($args as $key => $value)
        {
            if (in_array($key, array('sort', 'page', 'length')) === FALSE)
            {
                /**
                 * Hasil: $condition_key (column) $condition_operator (=) $condition_value (nilai)
                 * Contoh: column = nilai
                 *
                 **/

                // Normalize
                $condition_key      = '';
                $condition_operator = '';
                $condition_value    = '';
                $selector = $operator = '';

                // Preparing condition_key
                $selector = explode('::', $key);
                if (isset($selector[1]))
                {
                    $key = explode('.', $selector[1]);
                    switch ($selector[0]) {
                        case 'ENCRYPTED':
                            $condition_key = "SHA1(CONCAT(" . (isset($key[1]) ? $key[0] . ".`" . $key[1] . "`" : $table . ".`" . $key[0] . "`") . ", '" . $this->config->item('encryption_key') . "'))";
                        break;
                        default:
                            $condition_key = (isset($key[1]) ? $key[0] . ".`" . $key[1] . "`" : $table . ".`" . $key[0] . "`");
                        break;
                    }
                }
                else
                {
                    $key = explode('.', $key);
                    $condition_key = (isset($key[1]) ? $key[0] . ".`" . $key[1] . "`" : $table . ".`" . $key[0] . "`");
                }

                // Preparing condition_operator
                if (is_array($value) === false)
                {
                    $operator = explode('::', $value);

                    if (count($operator) > 1)
                    {
                        $valid_operator = array("EQUAL", "NOTEQ", "LESS", "GREAT", "LESSEQ", "GREATEQ", "IN", "NOTIN", "LIKE", "NOTLIKE", "BETWEEN");

                        if (in_array($operator[0], $valid_operator))
                        {
                            switch ($operator[0]) {
                                case 'EQUAL':
                                    $condition_operator = "=";
                                break;
                                case 'NOTEQ':
                                    $condition_operator = "!=";
                                break;
                                case 'LESS':
                                    $condition_operator = "<";
                                break;
                                case 'GREAT':
                                    $condition_operator = ">";
                                break;
                                case 'LESSEQ':
                                    $condition_operator = "<=";
                                break;
                                case 'GREATEQ':
                                    $condition_operator = ">=";
                                break;
                                case 'IN':
                                    $condition_operator = "IN";
                                break;
                                case 'NOTIN':
                                    $condition_operator = "NOT IN";
                                break;
                                case 'LIKE':
                                    $condition_operator = "LIKE";
                                break;
                                case 'NOTLIKE':
                                    $condition_operator = "NOT LIKE";
                                break;
                                case 'BETWEEN':
                                    $condition_operator = "BETWEEN";
                                break;
                                default:
                                    $condition_operator = "=";
                                break;
                            }
                        }

                        $value = $operator[1];
                    }
                    else
                    {
                        $condition_operator = "=";
                    }
                }

                if ( ! empty($condition_operator))
                {
                    if (preg_match('/(.*?)\[\{(.*)\}\{(.*)\}\{(.*)\}\]/', $value, $match))
                    {
                        $value = explode(',', $match[1]);
                        $condition_value = "(SELECT `".$match[2]."` FROM `".$match[3]."` WHERE `".$match[4]."` IN ('".implode("', '", $value)."'))";
                    }
                    else
                    {
                        if ($condition_operator == 'BETWEEN')
                        {
                            if (preg_match('/\[\{(.*)\}\{(.*)\}\]/', $value, $match))
                            {
                                $condition_value = "'".$match[1]."' AND '".$match[2]."'";
                            }
                        }
                        else
                        {
                            $value = explode('##', $value);

                            if ((count($value) > 1) && ($value[1] == 'subquery'))
                            {
                                $condition_value = $value[0];
                            }
                            else
                            {
                                $condition_value = "'".$value[0]."'";
                            }
                        }
                    }
                }

                if ( ! empty($condition_key) && ! empty($condition_operator) && ! empty($condition_value))
                {
                    $conditions[] = "(" . $condition_key . " " . $condition_operator . " " . $condition_value . ")";
                }
            }
        }

        $result = (($conditions) ? " WHERE " : "");

        foreach ($conditions as $key => $cond)
        {
            $result .= $cond . (($key < (count($conditions) - 1)) ? " AND " : "");
        }

        return $result;
    }

    protected function build_order($args = array(), $table)
    {
        $args = $this->sanitize($args);

        $result = "";
        if ( ! empty($args['sort']))
        {
            $index = 0;
            foreach ($args['sort'] as $key => $sort)
            {
                if (! empty($sort['sort_by']) && ! empty($sort['sort_direction']))
                {
                    $result .= (($index == 0) ? " ORDER BY" : "");
                    $result .= (($index > 0) ? "," : "");

                    if ($sort['sort_direction'] != 'desc') $sort['sort_direction'] = 'asc';

                    $key = explode('.', $sort['sort_by']);
                    
                    if ($sort['sort_by'] == 'random_sort')
                    {
                        $result .= " RAND()";
                    }
                    else
                    {
                        $result .= 
                            " " . (isset($key[1]) ? $key[0] . ".`" . $key[1] . "`" : $table . ".`" . $key[0] . "`") . 
                            " ".strtoupper($sort['sort_direction']);
                    }

                    $index++;
                }
            }
        }

        return $result;
    }

    protected function build_limit($args = array())
    {
        $result = "";
        $default_length = 10;

        $page   = (isset($args['page']) ? (int) $args['page'] : '');
        $length = (isset($args['length']) ? $args['length'] : 'all');

        if (( ! empty($page)) OR ( ! empty($length)))
        {
            if (( ! empty($page)) && ( ! empty($length)))
            {
                if ($length !== 'all')
                {
                    $start = (((int)$page > 1) ? ((int)$page - 1) * (int)$length : 0);
                    $result = " LIMIT ".$start.",".$length;
                }
            }
            elseif (( ! empty($page)) && (empty($length)))
            {
                $start = (((int)$page > 1) ? ((int)$page - 1) * $default_length : 0);
                $result = " LIMIT ".$start.",".$default_length;
            }
            elseif ((empty($page)) && ( ! empty($length)))
            {
                if ($length !== 'all') $result = " LIMIT ".$length;
            }
        }

        return $result;
    }

    protected function build_field($table = '', $values = '')
    {
        $result = "";
        $columns = $this->describe_table($table);

        $i = 1;
        foreach ($values as $key => $value)
        {
            if (in_array($key, $columns))
            {
                $result .= (($i != 1) ? ', ' : '')."`".$key."` = '".((( is_array($value)) OR ( is_object($value))) ? json_encode($this->sanitize($value)) : $this->sanitize($value))."'";

                $i++;
            }
        }

        return $result;
    }

    protected function describe_table($table, $show_full = FALSE) {
        $query = $this->db->query("DESCRIBE `" . $table . "`")->result();

        $result = array();
        foreach($query as $row)
        {
            if ($show_full)
            {
                $result[] = array($row->Field, $row->Type, $row->Null, $row->Key, $row->Default, $row->Extra);
            }
            else
            {
                $result[] = $row->Field;
            }
        }

        return $result;
    }

    public function count_all($table)
    {
        $query = $this->db->query("SELECT COUNT(*) AS `total_rows` FROM `".$table."`");
        
        if ($query->num_rows() === 0)
        {
            return 0;
        }

        $query = $query->fetch();
        return (int) $query['total_rows'];
    }

    public function count_rows($table = '', $column_key = '', $params = array())
    {
        $response = 0;

        if ( ! empty($table) && ! empty($column_key) && ! empty($params))
        {
            $query = "SELECT `".$column_key."` FROM `".$table."`";

            $index = 0;
            foreach ($params as $key => $value)
            {
                $query .= (($index == 0) ? " WHERE" : " AND") . " " . $key . " = '".$value."'";
                $index++;
            }

            return $this->db->query($query)->num_rows();
        }
    }
}