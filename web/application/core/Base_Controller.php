<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Base Controller
 * @author Asep Fajar Nugraha <delve_brain@hotmail.com>
 */
class Base_Controller extends CI_Controller {
    protected $req;
    protected $res;
    protected $ajax = FALSE;
    protected $data = array();
    protected $document = array();
    protected $theme = 'default';

	public function __construct()
    {
        parent::__construct();

        // Init Application
        $this->init_application();
    }

    protected function init_application()
    {
        // Check Request Method
        if (strtolower(filter_input(INPUT_SERVER, 'HTTP_X_REQUESTED_WITH')) === 'xmlhttprequest') {
           $this->ajax = TRUE;
        }
        
        // Adding post to req
        if ($this->input->post())
        {
            $this->req = $this->input->post();
        }

        // Get Session
        $this->data['app'] = $this->session->userdata('app-' . $this->config->item('encryption_key'));
        $this->data['login'] = $this->session->userdata('login-' . $this->config->item('encryption_key'));

        // Get Config
        $app_list = file_get_contents(FCPATH . 'app.json');
        $app_list = json_decode($app_list, 1);

        $params = $this->input->get();
        if ( ! empty($params['access_app']) && in_array($params['access_app'], $app_list))
        {
            if ( ! empty($this->data['app']) && ($this->data['app']['name'] != strtolower($params['access_app'])))
            {
                // Unset session before
                $this->session->set_userdata('login-' . $this->config->item('encryption_key'), array());
                $this->session->set_userdata('cart-' . $this->config->item('encryption_key'), array());
                $this->session->set_userdata('book-' . $this->config->item('encryption_key'), array());

                // Set new app session
                $this->data['app']['name']    = strtolower($params['access_app']);
                $this->data['app']['type']    = (( ! empty($params['access_type']) && in_array($params['access_type'], array('full', 'webview'))) ? $params['access_type'] : 'full');

                // App session
                $this->session->set_userdata('app-' . $this->config->item('encryption_key'), $this->data['app']);
            }
            else
            {
                $this->data['app']['name']    = strtolower($params['access_app']);
                $this->data['app']['type']    = (( ! empty($params['access_type']) && in_array($params['access_type'], array('full', 'webview'))) ? $params['access_type'] : 'full');

                // App session
                $this->session->set_userdata('app-' . $this->config->item('encryption_key'), $this->data['app']);
            }

            $config_list = file_get_contents(FCPATH . 'config/' . $this->data['app']['name'] . '.json');
            $config_list = json_decode($config_list, 1);
        }
        else
        {
            if ( ! empty($this->data['app']))
            {
                $config_list = file_get_contents(FCPATH . 'config/' . $this->data['app']['name'] . '.json');
                $config_list = json_decode($config_list, 1);
            }
            else
            {
                $config_list = file_get_contents(FCPATH . 'config/' . 'paipai.json');
                $config_list = json_decode($config_list, 1);

                $this->data['app']['name']    = 'paipai';
                $this->data['app']['type']    = 'full';

                // App session
                $this->session->set_userdata('app-' . $this->config->item('encryption_key'), $this->data['app']);
            }
        }

        if (isset($config_list['app']))
        {
            $config = $config_list;

            // BEGIN: Config & Data Directory
            $this->data_dir = FCPATH . 'config/data/' . $config['app']['data_dir']['value'] . '/';

            // BEGIN: Document
            $this->set_document('app_layout', array('header', 'footer', 'content', 'search'));
            $this->set_document('app_title', ( ! empty($config['app']['title']['value']) ? $config['app']['title']['value'] : 'Untitled'));

            // BEGIN: Language
            if ( ! empty($config['app']['lang']['value']))
            {
                $language = $config['app']['lang']['value'];
            }
            else
            {
                $language = $this->config->item('language');
            }

            $lang = $this->input->get('lang');
            if ( ! empty($lang) &&  ! empty($config['lang']) && in_array($lang, $config['lang']))
            {
                $this->session->set_userdata('lang-' . $this->config->item('encryption_key'), $this->input->get('lang'));
            }

            $idiom = $this->session->userdata('lang-' . $this->config->item('encryption_key'));
            $idiom = ( ! empty($idiom) ? $idiom : $language);

            $this->lang->load('common', $idiom);
            $this->lang->load('response', $idiom);

            // BEGIN: Theme
            if ( ! empty($config['app']['theme']['value']))
            {
                $this->theme = $config['app']['theme']['value'];
            }

            // BEGIN: URL
            $this->data['config']       = $config;
            $this->data['app_url']      = base_url();
            $this->data['storage_url']  = $this->data['app_url'] . 'storage/';
            $this->data['storage_path'] = FCPATH . 'storage/';
            $this->data['assets_url']   = $this->data['app_url'] . 'public/theme/' . $this->theme . '/assets/';
        }
        else
        {
            if ($this->ajax)
            {
                $this->res['code']      = 500;
                $this->res['message']   = lang('message_' . $this->res['code']);

                $this->print_output();
            }
            else
            {
                redirect($this->data['app_url'] . 'error/404');
            }
        }
    }

    public function set_document($type, $value)
    {
        $this->document[$type] = $value;
    }

    public function get_document($type = '')
    {
        if ( ! empty($type))
        {
            return $this->document[$type];
        }
        else
        {
            return $this->document;
        }
    }

    protected function print_layout($content_layout = '')
    {
        // Document
        $this->data['document'] = $this->get_document();

        $template = array();
        foreach ($this->data['document']['app_layout'] as $key => $layout)
        {
            $template[$layout] = $this->load->extview(
                    $this->theme . '/template/' . (($layout === 'content') ? $content_layout : 'layout/' . $layout)
                    , $this->data
                    , true);
        }

        // PRINT
        $this->load->extview($this->theme . '/template/layout/container', $template);
    }

    protected function print_output($return_type = 'json', $data = array(), $exit = TRUE)
    {
        if (empty($data))
        {
            $data = $this->res;
        }

        switch ($return_type)
        {
            case 'json':
                if ( ! $this->ajax)
                {
                    // Set flashdata
                    if ( ! empty($this->res['code']) && ($this->res['code'] !== 200))
                    {
                        $this->session->set_flashdata('error', $this->res['message']);
                    }

                    // Check if has redirect
                    if ( ! empty($this->res['redirect']))
                    {
                        redirect($this->res['redirect']);
                    }
                }
                else
                {
                    $this->output
                        ->set_content_type('application/json', $this->config->item('charset'))
                        ->set_output(json_encode($data))
                        ->_display();
                    
                    if ($exit) exit;
                }
            break;

            case 'data':
                $res = json_encode($data);
                return json_decode($res, 1);
                if ($exit) exit;
            break;
            
            default:
                echo '<pre>'.print_r($data, 1).'</pre>';
                if ($exit) exit;
            break;
        }
    }
}
